## Requirement
- Download and install Nodejs v6.9.1 [Windows](https://nodejs.org/en/blog/release/v6.9.1/) or another OS.
- Install bower `npm install -g bower`
- Install gulp `npm install -g gulp-cli`

## How can I develop?
- Install dependencies with npm `npm install`
- Copy `src/config.js.example` to `src/config.js`
- Copy `src/app/config.js.example` to `src/app/config.js`
- Run development with `gulp serve`
- Run production with `gulp build`

## How can I code?
- [Create new page](http://akveo.github.io/blur-admin/articles/013-create-new-page/)
- [Customize sidebar](http://akveo.github.io/blur-admin/articles/051-sidebar/)

## View demo
- [blur-admin-mint](http://akveo.com/blur-admin-mint/#/dashboard)

