'use strict';

angular.module('BlurAdmin', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',
  'ngCookies',
  'pascalprecht.translate',
  'angular-ladda',
  'ngTagsInput',
  'ngFileUpload',
  'daterangepicker',
  'angular-svg-round-progressbar',
  'BlurAdmin.theme',
  'BlurAdmin.shared',
  'BlurAdmin.pages'
])
  .config(['$translateProvider', 'stConfig', function ($translateProvider, stConfig, $cookies) {
    $translateProvider.useStaticFilesLoader({
      prefix: 'assets/translations/locale-',
      suffix: '.json'
    });
    $translateProvider.useSanitizeValueStrategy('sanitize');
    $translateProvider.useCookieStorage();
    $translateProvider.useLoaderCache(true);
    $translateProvider.fallbackLanguage('vi');
    $translateProvider.preferredLanguage('vi');

    stConfig.pagination.template = 'app/shared/templates/pagination.custom.html';
  }])
  .run(function ($http, $cookies, permissionService) {
    if ($cookies.get('access_token')) {
      $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('access_token');
    }

    permissionService.loadRoutes();
  });