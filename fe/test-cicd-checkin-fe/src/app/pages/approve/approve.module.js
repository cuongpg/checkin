/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.approve', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('approve', {
        url: '/approve',
        templateUrl: 'app/pages/approve/approve.html',
        title: 'PENDING_RECORD',
        controller: 'ApproveCtrl',
        sidebarMeta: {
          icon: 'ion-funnel',
          order: 1,
        },
        permission: ['OverTime.GetOvertimeForManager']
      });
  }

})();
