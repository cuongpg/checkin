(function () {
  'use strict';

  angular.module('BlurAdmin.pages.approve')
    .controller('ApproveCtrl', ApproveCtrl);

  /** @ngInject */
  function ApproveCtrl($scope, apiService, $uibModal, popupService, $uibModalStack, notificationService, menuService) {
    $scope.employee = null;
    $scope.isForAdmin = false;
    if ($scope.$resolve.employee) {
      $scope.employee = $scope.$resolve.employee;
      $scope.isForAdmin = true;
    }
    $scope.approve = {
      refresh : function (data, isEvents) {
        var _this = data;
        if (_this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete _this.tableState["all"];
            }
          }
        }
        _this.selected = [];
        _this.isLoading = true;
        _this.tableState.all = { "checkbox": [{ "status": "0" }] }
        _this.tableState.pagination = { "start": 0, "totalItemCount": 1000, "number": 1000, "numberOfPages": 1000 }
        _this.tableState.groupEventIds = []
        if (!_this.urlGetAll == '') {
          apiService.get(_this.urlGetAll, _this.tableState, function (response) {
            _this.tableData = response.data.data;
            _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
            _this.tableState.pagination.totalItemCount = response.data.totalRecords;
            _this.isLoading = false;
            _this.isSelectAll = false;
          }, function () {
            _this.isLoading = false;
            _this.isSelectAll = false;
          });
        }
        menuService.updateMenu();
      }
    };

    $scope.selectAll = function (collection, data, type) {
      var _this = data;
      if (_this.selected.length === 0) {
        angular.forEach(collection, function (val) {
          if (type == 'un') {
            if (val.statusDestroy == 0) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          }
          else {
            if (val.status == 0) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          }
        });
        _this.isSelectAll = true;
      }
      else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length) {
        angular.forEach(collection, function (val) {
          var found = _this.selected.indexOf(val.id);
          if (found == -1) _this.selected.push(val.id);
          val.checked = true;
        });
        _this.isSelectAll = true;
      }
      else {
        _this.selected = [];
        angular.forEach(collection, function (val) {
          val.checked = false;
        });
        _this.isSelectAll = false;
      }
    }

    $scope.toggleSelection = function (id, data) {
      var idx = data.selected.indexOf(id);
      var bankTransfer = _.filter(data.tableData, function (o) { return o.id === id; })[0];
      if (idx > -1) {
        bankTransfer.checked = false;
        data.selected.splice(idx, 1);
        data.isSelectAll = false;
      } else {
        bankTransfer.checked = true;
        data.selected.push(id);
        if (data.selected.length === data.tableData.length) {
          data.isSelectAll = true;
        }
      }
    }

    $scope.approveRejectAll = function (type, data) {
      var ids = data.selected;
      if (ids.length == 0) {
        notificationService.warningTranslate('PLEASE_CHOOSE_ONE_RECORD');
      }
      else {
        var dataPost = {
          "ids": ids
        }
        if (type == 'approve') {
          popupService.openApprovePopup($scope, data.urlApproveAll, dataPost)
        }
        else {
          popupService.openRejectPopup($scope, data.urlRejectAll, dataPost)
        }
      }
      if (!$scope.isForAdmin)
        $uibModalStack.dismissAll();
    }

    $scope.approveReject = function (type, userId, data) {
      var dataPost = {
        "ids": [
          userId
        ]
      }
      if (type == 'approve') {
        popupService.openApprovePopup($scope, data.urlApprove, dataPost)
      }
      else {
        popupService.openRejectPopup($scope, data.urlReject, dataPost)
      }
    }

    $scope.refresh = function () {
      $scope.approveIrregular.refresh();
      $scope.approveOvertime.refresh();
      $scope.approveLeave.refresh();
      $scope.approveUnLeave.refresh();
      $scope.ChangeManagerInfoToApprove.refresh();
    }

    // For Irregular
    $scope.approveIrregular = {
      isSelectAll: false,
      isLoading: true,
      tableState: { all: {} },
      tablePageSize: 0,
      tableData: [],
      urlGetAll: $scope.isForAdmin ? 'TimeKeeping/AdminGetAbnormalTimeKeepingForManager?employeeCode=' + $scope.employee.id : 'TimeKeeping/GetAbnormalTimeKeepingForManager',
      urlApprove: $scope.isForAdmin ? 'TimeKeeping/AdminApproveAbnormalTimeKeeping?employeeCode=' + $scope.employee.id + '&isForManager=true': 'TimeKeeping/ApprovalAbnormalTimeKeeping',
      urlReject: $scope.isForAdmin ? 'TimeKeeping/AdminRejectAbnormalTimeKeeping?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'TimeKeeping/RejectAbnormalTimeKeeping',
      urlApproveAll: $scope.isForAdmin ? 'TimeKeeping/AdminApproveAbnormalTimeKeeping?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'TimeKeeping/ApprovalAbnormalTimeKeeping',
      urlRejectAll: $scope.isForAdmin ? 'TimeKeeping/AdminRejectAbnormalTimeKeeping?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'TimeKeeping/RejectAbnormalTimeKeeping',
      init: function (tableState) {
        $scope.approveIrregular.tableState = tableState;
        $scope.approveIrregular.refresh();
      },
      refresh: function () {
        $scope.approve.refresh(this, false)
      },
      selectAll: function (collection) {
        $scope.selectAll(collection, this, '')
      },
      toggleSelection: function (id) {
        $scope.toggleSelection(id, this)
      },
      approveReject: function (type, userId) {
        $scope.approveReject(type, userId, this)
      },
      approveRejectAll: function (type) {
        $scope.approveRejectAll(type, this);
      }
    };

    // For Overtime
    $scope.approveOvertime = {
      isSelectAll: false,
      isLoading: true,
      tableState: { all: {} },
      tablePageSize: 0,
      tableData: [],
      urlGetAll: $scope.isForAdmin ? 'OverTime/AdminGetOvertimeForManager?employeeCode=' + $scope.employee.id : 'OverTime/GetOvertimeForManager',
      urlApprove: $scope.isForAdmin ? 'OverTime/AdminApproveOverTime?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'OverTime/ApprovalOverTime',
      urlReject: $scope.isForAdmin ? 'OverTime/AdminRejectOverTime?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'OverTime/RejectOverTime',
      urlApproveAll: $scope.isForAdmin ? 'OverTime/AdminApproveOverTime?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'OverTime/ApprovalOverTime',
      urlRejectAll: $scope.isForAdmin ? 'OverTime/AdminRejectOverTime?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'OverTime/RejectOverTime',
      init: function (tableState) {
        $scope.approveOvertime.tableState = tableState;
        $scope.approveOvertime.refresh();
      },
      refresh: function () {
        $scope.approve.refresh(this, true)
      },
      selectAll: function (collection) {
        $scope.selectAll(collection, this, '')
      },
      toggleSelection: function (id) {
        $scope.toggleSelection(id, this)
      },
      approveReject: function (type, userId) {
        $scope.approveReject(type, userId, this)
      },
      approveRejectAll: function (type) {
        $scope.approveRejectAll(type, this);
      }
    };

    // For Leave
    $scope.approveLeave = {
      isSelectAll: false,
      isLoading: true,
      tableState: { all: {} },
      tablePageSize: 0,
      tableData: [],
      urlGetAll: $scope.isForAdmin ? 'ApprovalRecord/AdminGetApprovalRecordsForManager?employeeCode=' + $scope.employee.id : 'ApprovalRecord/GetApprovalRecordsForManager',
      urlApprove: $scope.isForAdmin ? 'ApprovalRecord/AdminApproveLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/ApprovalApprovalRecord',
      urlReject: $scope.isForAdmin ? 'ApprovalRecord/AdminRejectLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/RejectApprovalRecord',
      urlApproveAll: $scope.isForAdmin ? 'ApprovalRecord/AdminApproveLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/ApprovalApprovalRecord',
      urlRejectAll: $scope.isForAdmin ? 'ApprovalRecord/AdminRejectLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/RejectApprovalRecord',
      init: function (tableState) {
        $scope.approveLeave.tableState = tableState;
        $scope.approveLeave.refresh();
      },
      refresh: function () {
        $scope.approve.refresh(this, false)
      },
      selectAll: function (collection) {
        $scope.selectAll(collection, this, '')
      },
      toggleSelection: function (id) {
        $scope.toggleSelection(id, this)
      },
      approveReject: function (type, userId) {
        $scope.approveReject(type, userId, this)
      },
      approveRejectAll: function (type) {
        $scope.approveRejectAll(type, this);
      }
    };

    // For UnLeave
    $scope.approveUnLeave = {
      isSelectAll: false,
      isLoading: true,
      tableState: { all: {} },
      tablePageSize: 0,
      tableData: [],
      urlGetAll: $scope.isForAdmin ? 'ApprovalRecord/GetDestroyApprovalRecordsForAdmin?employeeCode=' + $scope.employee.id : 'ApprovalRecord/GetDestroyApprovalRecordsForManager',
      urlApprove: $scope.isForAdmin ? 'ApprovalRecord/AdminApproveDestroyLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/ApprovalDestroyApprovalRecord',
      urlReject: $scope.isForAdmin ? 'ApprovalRecord/AdminRejectDestroyLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/RejectDestroyApprovalRecord',
      urlApproveAll: $scope.isForAdmin ? 'ApprovalRecord/AdminApproveDestroyLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/ApprovalDestroyApprovalRecord',
      urlRejectAll: $scope.isForAdmin ? 'ApprovalRecord/AdminRejectDestroyLeaveRequest?employeeCode=' + $scope.employee.id + '&isForManager=true' : 'ApprovalRecord/RejectDestroyApprovalRecord',
      init: function (tableState) {
        $scope.approveUnLeave.tableState = tableState;
        $scope.approveUnLeave.refresh();
      },
      refresh: function () {
        $scope.approve.refresh(this, false)
      },
      selectAll: function (collection) {
        $scope.selectAll(collection, this, 'un')
      },
      toggleSelection: function (id) {
        $scope.toggleSelection(id, this)
      },
      approveReject: function (type, userId) {
        $scope.approveReject(type, userId, this)
      },
      approveRejectAll: function (type) {
        $scope.approveRejectAll(type, this);
      }
    };

    $scope.ChangeManagerInfoToApprove = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 5,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.ChangeManagerInfoToApprove.tableState = tableState;
        $scope.ChangeManagerInfoToApprove.refresh();
      },
      refresh: function () {
        var _this = $scope.ChangeManagerInfoToApprove;
        _this.isLoading = true;
        var changemanagerUrl = 'ChangeManagerInfo/GetChangeManagerInfoToApprove';
        if ($scope.isForAdmin) changemanagerUrl = 'ChangeManagerInfo/GetChangeManagerInfoToApprove' + '?managerId=' + $scope.employee.id;
        apiService.get(changemanagerUrl, _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },

      approveReject: function (item, a_r) {
        var _this = this;
        var linkApi = '';
        if (item.status != 0) {
          return notificationService.errorTranslate('CAN_NOT_FIX_THIS_FORM');
        }
        if ($scope.isForAdmin) {
          var idManager = 0;
          if (item.newManagerStatus == 0 && item.oldManagerStatus == 0) {
            return notificationService.errorTranslate('Cannot approved without any confirm of managers')
          }
          else {
            idManager = item.newManagerStatus == 0 ? item.newManagerId : item.oldManagerId
          }
          apiService.create('ChangeManagerInfo/AdminConfirmChangeManager' + '?id=' + item.id + '&isConfirm=' + (a_r == 'approve') + '&managerId=' + idManager, null, function (_response) {
            $scope.refresh();
          });
        }
        else {
          apiService.create('ChangeManagerInfo/ConfirmChangeManager' + '?id=' + item.id + '&isConfirm=' + (a_r == 'approve'), null, function (_response) {
            $scope.refresh();
          });
        }

      }

    };
  }
})();