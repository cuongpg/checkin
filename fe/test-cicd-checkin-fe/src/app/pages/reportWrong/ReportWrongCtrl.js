(function () {
    'use strict';

    angular.module('BlurAdmin.pages.reportwrong')
        .controller('ReportWrongCtrl', ReportWrongCtrl);

    /** @ngInject */
    function ReportWrongCtrl($scope, $window, $uibModal, apiService, notificationService, $filter, $translate, $cookies, $uibModalStack) {
        var vm = this;
        $scope.dataExport = {
            date: new Date(),
            startDate: null,
            endDate: null
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        }
        $scope.datePopup = {
            monthOpend: false,
            startDateOpend: false,
            endDateOpend: false
        };
        $scope.onDateClick = function () {
            $scope.datePopup.monthOpend = true;
        };
        $scope.changeMonth = function () {
            $scope.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date($scope.dataExport.date.getFullYear(), $scope.dataExport.date.getMonth() + 1, 0),
                minDate: new Date($scope.dataExport.date.getFullYear(), $scope.dataExport.date.getMonth(), 1),
                startingDay: 1
            }
            $scope.dataExport.startDate = $scope.dateOptions.minDate;
            $scope.dataExport.endDate = $scope.dateOptions.maxDate;
            vm.refresh();
            // vm.init();
        }
        vm.employee = null;
        vm.isForAdmin = false;
        if ($scope.$resolve) {
            vm.employee = $scope.$resolve.employee ? $scope.$resolve.employee : null;
            vm.isForAdmin = $scope.$resolve.employee ? $scope.$resolve.employee.isAdmin : false;
        }
        vm.isReport = false;
        vm.isGetDataOk = false;
        vm.language = $cookies.get('NG_TRANSLATE_LANG_KEY')
        vm.mechanisCurrKey = ''
        vm.mechanisCurrValue = ''
        if (vm.language === 'vi') {
            vm.mechanisCurrKey = 'Cơ chế KETRAPHAKY chuẩn'
            vm.mechanisCurrValue = 'Cơ chế làm thêm giờ'
        } else if (vm.language === 'en') {
            vm.mechanisCurrKey = 'Standard KETRAPHAKY mechanism'
            vm.mechanisCurrValue = 'Overtime mechanism'
        } else {
            vm.mechanisCurrKey = 'กลไก KETRAPHAKY มาตรฐาน'
            vm.mechanisCurrValue = 'กลไกแบบอิงหน่วย'
        }
        vm.width = $window.innerWidth;
        vm.height = $window.innerHeight;
        vm.firstDay = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        vm.endDay = new Date(new Date().getFullYear(), new Date().getMonth()+1, 0);
        vm.currentDate = new Date(new Date().toISOString().slice(0, 10));
        vm.timeNow = '';
        vm.isEdit = false;
        vm.oldProjectList = [];
        vm.oldResponse = {};
        vm.overtimeTime = 0;
        vm.infoData = [];
        vm.departmentData = [];
        vm.departmentEmpty = {
            isNew: true,
            lineDisable: false,
            ORG_STEXT: {
                isHide: false,
                title: {
                    class: "col-md-5",
                    titleBold: 'WHAT_CENTER'
                },
                autoKey: ['HILFM', 'HEAD_NAME'],
                disable: false,
                class: "col-md-5",
                value: {
                    key: 0,
                    value: ''
                },
                data: 'estimateOwnersSelected',
                type: 'select'
            },
            HEAD_NAME: {
                isHide: false,
                title: {
                    class: "col-md-3",
                    titleBold: 'REPORT_TO'
                },
                disable: true,
                class: "col-md-3",
                value: '',
                type: 'input'
            },
            HILFM: {
                isHide: false,
                title: {
                    class: "col-md-3",
                    titleBold: 'VOLUNTARY_MECHANISM'
                },
                disable: true,
                class: "col-md-3",
                value: {
                    key: 0,
                    value: ''
                },
                data: 'departmentMechanismSelected',
                type: 'select'
            },
            PROZT: {
                isHide: true,
                title: {
                    class: "col-md-0",
                    titleBold: ''
                },
                disable: false,
                class: "col-md-0",
                value: 100,
                type: 'number'
            },
            end: {
                isHide: false,
                title: {
                    class: "col-md-1",
                    titleBold: 'DELETE'
                },
                disable: false,
                class: "col-md-1",
                type: 'button'
            }
        }
        vm.projectData = []
        vm.projectEmpty = {
            isNew: true,
            lineDisable: false,
            name: {
                isHide: false,
                title: {
                    class: "col-md-2",
                    titleBold: 'THE_PROJECT_IS_PARTICIPATING'
                },
                autoKey: ['PROJ_TYPE_NAME', 'PERNR', 'startDate', 'endDate'],
                disable: false,
                class: "col-md-2",
                value: {
                    key: 0,
                    value: '',
                },
                data: 'projectInformationsSelected',
                type: 'select'
            },
            PERNR: {
                isHide: false,
                title: {
                    class: "col-md-2",
                    titleBold: 'REPORT_TO'
                },
                disable: true,
                class: "col-md-2",
                value: '',
                type: 'input'
            },
            PROJ_TYPE_NAME: {
                isHide: false,
                title: {
                    class: "col-md-2",
                    titleBold: 'VOLUNTARY_MECHANISM'
                },
                disable: true,
                class: "col-md-2",
                value: {
                    key: 0,
                    value: '',
                },
                data: [],
                type: 'select'
            },
            startDate: {
                isHide: false,
                title: {
                    class: "col-md-2",
                    titleBold: 'PARTICIPATING_TIME'
                },
                disable: false,
                class: "col-md-2",
                value: vm.currentDate,
                type: 'date',
                isOpen: false,
                validate: {
                    maxDate: vm.currentDate,
                    minDate: vm.currentDate
                }
            },
            endDate: {
                isHide: false,
                title: {
                    class: "col-md-2",
                    titleBold: 'END_TIME'
                },
                disable: false,
                class: "col-md-2",
                value: vm.currentDate,
                type: 'date',
                isOpen: false,
                validate: {
                    maxDate: vm.currentDate,
                    minDate: vm.currentDate
                }
            },
            perct: {
                isHide: false,
                title: {
                    class: "col-md-2",
                    titleBold: 'PERCENTAGE'
                },
                disable: false,
                class: "col-md-1",
                value: 100,
                type: 'number'
            },
            end: {

                title: {
                    class: "col-md-1",
                    titleBold: 'DELETE'
                },
                disable: false,
                class: "col-md-1",
                type: 'button'
            }
        }
        vm.salaryData = []
        vm.card = {}
        vm.departmentMechanismSelected = []
        vm.orgUnitLevel5Selected = []
        vm.orgUnitLevel3Selected = []
        vm.listLevelSelected = []
        vm.track2Selected = []
        vm.estimateOwnersSelected = []
        vm.projectInformationsSelected = []
        vm.COMMechanismListSelected = []
        vm.projectsListSelectedFullData = []
        vm.orgUnitLevel5SelectedSelectedFullData = []
        vm.COMMechanismListSelectedFullData = []
        vm.init = function () {
            var mechanismUrl = vm.isForAdmin ? 'Employee/MechanismInformationForAdmin/' + ($scope.dataExport.date.getMonth() + 1) + '/' + $scope.dataExport.date.getFullYear() + '?employeeCode=' + vm.employee.id : 'Employee/MechanismInformation';
            apiService.get(mechanismUrl, null, function (response) {
                apiService.get('Employee/IsNeedConfirmByEmployee', null, function (response) {
                    vm.isReport = response.data;
                }, function () {});
                var data = response.data;
                vm.isGetDataOk = true;
                vm.oldResponse = response.data;
                var departments = data.departments;
                var projects = data.projects;
                vm.overtimeTime = data.overtimeQuotas
                angular.forEach(departments, function (item) {
                    var departmentItem = {
                        isNew: false,
                        lineDisable: false,
                        ORG_STEXT: {
                            isHide: false,
                            title: {
                                class: "col-md-5",
                                titleBold: 'WHAT_CENTER'
                            },
                            autoKey: ['HILFM', 'HEAD_NAME'],
                            disable: false,
                            class: "col-md-5",
                            value: {
                                key: item.ORG_ID,
                                value: item.ORG_SHORT + " - " + item.ORG_STEXT
                            },
                            data: 'estimateOwnersSelected',
                            type: 'select'
                        },
                        HEAD_NAME: {
                            isHide: false,
                            title: {
                                class: "col-md-3",
                                titleBold: 'REPORT_TO'
                            },
                            disable: true,
                            class: "col-md-3",
                            value: item.HEAD_NAME,
                            type: 'input'
                        },
                        HILFM: {
                            isHide: false,
                            title: {
                                class: "col-md-3",
                                titleBold: 'VOLUNTARY_MECHANISM'
                            },
                            disable: true,
                            class: "col-md-3",
                            value: {
                                key: 0,
                                value: item.HILFM
                            },
                            data: 'departmentMechanismSelected',
                            type: 'select'
                        },
                        PROZT: {
                            isHide: true,
                            title: {
                                class: "col-md-0",
                                titleBold: ''
                            },
                            disable: false,
                            class: "col-md-0",
                            value: item.PROZT,
                            data: item.PROZT,
                            type: 'number'
                        },
                        end: {
                            isHide: false,
                            title: {
                                class: "col-md-1",
                                titleBold: 'DELETE'
                            },
                            disable: false,
                            class: "col-md-1",
                            type: 'button'
                        }
                    };
                    vm.departmentData.push(departmentItem);
                });
                angular.forEach(projects, function (item) {
                    var projectItem = {
                        isNew: false,
                        // lineDisable: vm.currentDate > new Date(item.endDate),
                        lineDisable: false,
                        name: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'THE_PROJECT_IS_PARTICIPATING'
                            },
                            autoKey: ['PROJ_TYPE_NAME', 'PERNR', 'startDate', 'endDate'],
                            disable: vm.currentDate > new Date(item.startDate),
                            class: "col-md-2",
                            value: {
                                key: item.id,
                                value: item.name,
                            },
                            data: 'projectInformationsSelected',
                            type: 'select'
                        },
                        PERNR: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'REPORT_TO'
                            },
                            disable: true,
                            class: "col-md-2",
                            value: item.PERNR ? item.PERNR : '',
                            type: 'input'
                        },
                        PROJ_TYPE_NAME: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'VOLUNTARY_MECHANISM'
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: item.PROJ_TYPE_NAME,
                            },
                            data: [],
                            type: 'select'
                        },
                        startDate: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'PARTICIPATING_TIME'
                            },
                            disable: vm.currentDate > new Date(item.startDate),
                            class: "col-md-2",
                            value: new Date(item.startDate),
                            type: 'date',
                            isOpen: false,
                            validate: {
                                maxDate: new Date(item.endDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.endDate) : vm.endDay,
                                minDate: new Date(item.startDate) > vm.firstDay && new Date(item.startDate) < vm.endDay ? new Date(item.startDate) : vm.firstDay
                            }
                        },
                        endDate: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'END_TIME'
                            },
                            disable: false,
                            class: "col-md-2",
                            value: new Date(item.endDate),
                            type: 'date',
                            isOpen: false,
                            validate: {
                                maxDate: new Date(item.endDate),
                                // minDate: new Date(item.startDate)
                                minDate: vm.firstDay
                            }
                        },
                        perct: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'PERCENTAGE'
                            },
                            disable: vm.currentDate > new Date(item.startDate),
                            class: "col-md-1",
                            value: item.perct,
                            type: 'number'
                        },
                        end: {
                            isHide: false,
                            title: {
                                class: "col-md-1",
                                titleBold: 'DELETE'
                            },
                            disable: vm.currentDate > new Date(item.startDate),
                            class: "col-md-1",
                            type: 'button'
                        }
                    };
                    vm.projectData.push(projectItem);
                });
                var infoItem = {
                    sp: {
                        isHide: false,
                        title: {
                            class: "col-md-3",
                            titleBold: 'ORG_UNIT_LEVEL_2'
                        },
                        disable: false,
                        class: "col-md-3",
                        value: {
                            key: 0,
                            value: data.statementCheckin.sp
                        },
                        data: 'orgUnitLevel3Selected',
                        type: 'select'
                    },
                    level: {
                        isHide: false,
                        title: {
                            class: "col-md-3",
                            titleBold: 'GRADE'
                        },
                        disable: false,
                        class: "col-md-3",
                        value: {
                            key: 0,
                            value: data.statementCheckin.level
                        },
                        data: 'listLevelSelected',
                        type: 'select'
                    },
                    track3: {
                        isHide: false,
                        title: {
                            class: "col-md-3",
                            titleBold: 'TRACK2'
                        },
                        disable: false,
                        class: "col-md-3",
                        value: {
                            key: 0,
                            value: data.statementCheckin.track2
                        },
                        data: 'track2Selected',
                        type: 'select'
                    },
                    cdt: {
                        isHide: false,
                        title: {
                            class: "col-md-3",
                            titleBold: 'ORG_UNIT_LEVEL_4'
                        },
                        disable: false,
                        class: "col-md-3",
                        value: {
                            key: 0,
                            value: data.statementCheckin.cdt
                        },
                        data: 'orgUnitLevel5Selected',
                        type: 'select'
                    }
                };
                vm.infoData.push(infoItem);
                var salaryItemDefault = {
                    comMechanisms: {
                        isHide: false,
                        title: {
                            class: "col-md-9",
                            titleBold: 'REMUNERATION_MECHANISM'
                        },
                        autoKey: ['grade'],
                        disable: false,
                        class: "col-md-9",
                        value: {
                            key: 0,
                            value: vm.mechanisCurrKey,
                            isTranslate: true
                        },
                        data: 'COMMechanismListSelected',
                        type: 'select'
                    },
                    grade: {
                        isHide: false,
                        title: {
                            class: "col-md-3",
                            titleBold: 'OVERTIME_MECHANISM'
                        },
                        disable: true,
                        class: "col-md-3",
                        value: {
                            key: 0,
                            value: vm.mechanisCurrValue,
                            isTranslate: true
                        },
                        data: [],
                        type: 'select'
                    }
                }
                if (data.statementCheckin.comMechanisms) {
                    var salaryItem = {
                        comMechanisms: {
                            isHide: false,
                            title: {
                                class: "col-md-9",
                                titleBold: 'REMUNERATION_MECHANISM'
                            },
                            autoKey: ['grade'],
                            disable: false,
                            class: "col-md-9",
                            value: {
                                key: data.statementCheckin.comMechanisms.ICNUM,
                                value: data.statementCheckin.comMechanisms.COMMechanismCode
                            },
                            data: 'COMMechanismListSelected',
                            type: 'select'
                        },
                        grade: {
                            isHide: false,
                            title: {
                                class: "col-md-3",
                                titleBold: 'OVERTIME_MECHANISM'
                            },
                            disable: true,
                            class: "col-md-3",
                            value: {
                                key: 0,
                                value: data.statementCheckin.comMechanisms.OTRegulation
                            },
                            data: '',
                            type: 'select'
                        }
                    }
                    vm.salaryData.push(salaryItem);
                } else {
                    vm.salaryData.push(salaryItemDefault);
                }
                // vm.initDataSelect();
                vm.renderCard();
            }, function () {});
        }
        vm.renderCard = function () {
            vm.card = {
                info: {
                    key: 'info',
                    data: vm.infoData,
                    footer: {
                        value: 'INFO_FOOTER',
                        type: 'text'
                    }
                },
                departmentList: {
                    key: 'departmentList',
                    data: vm.departmentData,
                    footer: {
                        value: 'DEPARTMENT_FOOTER',
                        type: 'button'
                    }
                },
                projectList: {
                    key: 'projectList',
                    data: vm.projectData,
                    footer: {
                        value: 'PROJECT_FOOTER',
                        type: 'button'
                    }
                },
                salary: {
                    key: 'salary',
                    data: vm.salaryData,
                    footer: {
                        value: 'SALARY_FOOTER',
                        type: 'text'
                    }
                }
            }
        }
        vm.refreshData = function (input) {}
        vm.onChangeValue = function (itemChangekey, lineChangeKey, groupChangeKey, valueInput) {
            if (itemChangekey && groupChangeKey && valueInput) {
                itemChangekey.forEach(function (value, i) {
                    var key = itemChangekey[i]
                    if (typeof vm.card[groupChangeKey]['data'][lineChangeKey][key].value === 'object') {
                        if (!vm.card[groupChangeKey]['data'][lineChangeKey][key].validate) {
                            if (valueInput.autoData[i]) {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].value.value = valueInput.autoData[i]
                            } else {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].value.value = ''
                            }
                        } else {
                            if (valueInput.autoData[i]) {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].validate = valueInput.autoData[i]
                            } else {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].value.value = {
                                    maxDate: vm.currentDate,
                                    minDate: vm.currentDate
                                }
                            }
                        }
                    } else {
                        if (valueInput.autoData[i]) {
                            vm.card[groupChangeKey]['data'][lineChangeKey][key].value = valueInput.autoData[i]
                        } else {
                            vm.card[groupChangeKey]['data'][lineChangeKey][key].value = ''
                        }
                    }
                });
                if(groupChangeKey == "projectList") {
                    vm.card.projectList.data[lineChangeKey].startDate.value = ''
                    vm.card.projectList.data[lineChangeKey].endDate.value = ''
                }
            }
        }
        vm.onReportForm = function () {
            vm.isEdit = !vm.isEdit;
            if (vm.isEdit) {
                vm.initDataSelect()
            } else {
                vm.refresh();
            }
        }
        vm.initDataSelect = function () {
            apiService.get('Employee/ProjectInformations', null, function (response) {
                vm.projectsListSelectedFullData = response.data;
                angular.forEach(vm.card.projectList.data, function (itemOld) {
                    var projectTgItem = response.data.filter(function (itemTg) {
                        return itemTg.PSPID === itemOld.name.value.key;
                    })[0];
                    if (projectTgItem) {
                        itemOld.startDate.validate = {
                            // maxDate: new Date(projectTgItem.ENDDA),
                            // minDate: new Date(projectTgItem.BEGDA)
                            maxDate: new Date(projectTgItem.endDate) > vm.firstDay && new Date(projectTgItem.endDate) < vm.endDay ? new Date(projectTgItem.endDate) : vm.endDay,
                            minDate: new Date(projectTgItem.startDate) > vm.firstDay && new Date(projectTgItem.endDate) < vm.endDay ? new Date(projectTgItem.startDate) : vm.firstDay
                        }
                        itemOld.endDate.validate = {
                            maxDate: new Date(projectTgItem.ENDDA),
                            minDate: vm.firstDay
                        }
                    }
                })
                angular.forEach(response.data, function (item) {
                    if (vm.currentDate <= new Date(item.ENDDA)) {
                        var validateEndDate = {
                            maxDate: new Date(item.ENDDA),
                            minDate: new Date(item.startDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.startDate) : vm.firstDay
                        }
                        var validateStartDate = {
                            // maxDate: new Date(item.ENDDA),
                            maxDate: new Date(item.endDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.endDate) : vm.endDay,
                            minDate: new Date(item.startDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.startDate) : vm.firstDay
                        }
                        vm.projectInformationsSelected.push({
                            key: item.PSPID,
                            value: item.PROJID,
                            autoData: [item.PROJ_TYPE_NAME, item.PERNR, validateStartDate, validateEndDate]
                        })
                    }
                });
                apiService.get('EstimateOwner/GetOrgUnit', null, function (response) {
                    vm.orgUnitLevel5SelectedSelectedFullData = response.data;
                    if (response.data != null) {
                        vm.estimateOwnersSelected = response.data.map(function (item) {
                            return {
                                key: item.OBJID,
                                value: item.SHORT + " ||| " + item.STEXT,
                                autoData: [item.HILFM, item.HEAD_P]
                            }
                        });
                    }
                    apiService.get('EstimateOwner/GetOrgUnit?level=4', null, function (response) {
                        vm.orgUnitLevel5Selected = response.data.map(function (item) {
                            return {
                                key: item.OBJID,
                                value: item.SHORT + " ||| " + item.STEXT
                            }
                        });
                    }, function () {});
                }, function () {});
                apiService.get('EstimateOwner/GetOrgUnit?level=2', null, function (response) {
                    vm.orgUnitLevel3Selected = response.data.map(function (item) {
                        return {
                            key: item.OBJID,
                            value: item.SHORT + " ||| " + item.STEXT
                        }
                    });
                    apiService.get('Employee/DepartmentMechanism', null, function (response) {
                        vm.departmentMechanismSelected = response.data.map(function (item) {
                            return {
                                key: item.id,
                                value: item.SHORT + " ||| " + item.name,
                                autoData: [item.PROJ_TYPE_NAME]
                            }
                        });
                    }, function () {});
                    apiService.get('Employee/GetListLevel', null, function (response) {
                        vm.listLevelSelected = response.data.map(function (item) {
                            return {
                                key: item.Code,
                                value: item.Desc_vi
                            }
                        });
                    }, function () {});
                    // apiService.get('Employee/GetListTrack2', null, function (response) {
                    //     vm.track2Selected = response.data.map(function (item) {
                    //         return {
                    //             key: item.Code,
                    //             value: item.Desc_vi
                    //         }
                    //     });
                    // }, function () {});
                    apiService.get('Employee/GetListTrack?level=2', null, function (response) {
                        vm.track2Selected = response.data.map(function (item) {
                            return {
                                key: item.id,
                                value: item.name + ' ||| ' + item.description
                            }
                        });
                    }, function () {});
                    apiService.get('Employee/COMMechanismList', null, function (response) {
                        vm.COMMechanismListSelectedFullData = response.data;
                        vm.COMMechanismListSelected = response.data.map(function (item) {
                            return {
                                key: item.HCMCode,
                                value: item.COMMechanismCode + " ||| " + item.Regulation,
                                autoData: [item.OTRegulation]
                            }
                        });
                        vm.COMMechanismListSelected.push({
                            key: 0,
                            value: vm.mechanisCurrKey,
                            autoData: [vm.mechanisCurrValue]
                        })
                    }, function () {});
                }, function () {});
            }, function () {});
        }
        vm.getEstimateOwners = function () {
            apiService.get('EstimateOwner/GetOrgUnit?level=5', null, function (response) {
                if (response.data != null) {
                    vm.estimateOwnersSelected = response.data.map(function (item) {
                        return {
                            key: item.OBJID,
                            value: item.STEXT,
                            autoData: [item.HILFM, item.HEAD_P]
                        }
                    });
                }
            })
        }
        vm.refresh = function () {
            vm.departmentMechanismSelected = []
            vm.orgUnitLevel5Selected = []
            vm.orgUnitLevel3Selected = []
            vm.listLevelSelected = []
            vm.track2Selected = []
            vm.estimateOwnersSelected = []
            vm.projectInformationsSelected = []
            vm.COMMechanismListSelected = []
            vm.oldProjectList = []
            vm.infoData = [];
            vm.departmentData = []
            vm.projectData = []
            vm.salaryData = []
            vm.card = {}
            vm.init();
        }
        vm.onConfirmForm = function () {
            if (vm.isEdit) {
                angular.forEach(vm.oldProjectList, function (item) {
                    vm.card.projectList.data.push(item)
                })
                vm.onApprove();
            } else {
                // vm.onApprove();
                apiService.get('Employee/ConfirmInformation', null, function (_response) {
                    $uibModalStack.dismissAll();
                    vm.isReport = !vm.isReport;
                });
            }
        }
        vm.formatDate = function (value) {
            var dateValue = new Date(value)
            dateValue.setSeconds(0);
            dateValue.setMinutes(0);
            dateValue.setHours(0);
            return dateValue;
        }
        vm.validatePrectInDay = function () {
            // Validate trong moi thoi diem khong duoc tham gia du an qua 100%
            var isPerctSmallerMax = true;
            var totalTimeDeparment = 0;
            // angular.forEach(vm.card.departmentList.data, function (item) {
            //     totalTimeDeparment = totalTimeDeparment + item.PROZT.value;
            // })
            angular.forEach(vm.card.projectList.data, function (item) {
                if (item.perct.value !== 0) {
                    var listTg = angular.copy(vm.card.projectList.data)
                    var index = vm.card.projectList.data.indexOf(item);
                    listTg.splice(index, 1);
                    var totalTimeProject = 0;
                    // if (!item.lineDisable) {
                    //     totalTimeProject = item.perct.value;
                    // }
                    totalTimeProject = item.perct.value
                    angular.forEach(listTg, function (mapItem) {
                        if (mapItem.perct.value != 0) {
                            // if (vm.formatDate(item.startDate.value) >= vm.formatDate(mapItem.startDate.value) && vm.formatDate(item.startDate.value) <= vm.formatDate(mapItem.endDate.value) && !mapItem.lineDisable) {
                            //     totalTimeProject = totalTimeProject + mapItem.perct.value;
                            // } else if (vm.formatDate(item.endDate.value) >= vm.formatDate(mapItem.startDate.value) && vm.formatDate(item.endDate.value) <= vm.formatDate(mapItem.endDate.value) && !mapItem.lineDisable) {
                            //     totalTimeProject = totalTimeProject + mapItem.perct.value;
                            // }
                            if (vm.formatDate(item.startDate.value) >= vm.formatDate(mapItem.startDate.value) && vm.formatDate(item.startDate.value) <= vm.formatDate(mapItem.endDate.value)) {
                                totalTimeProject = totalTimeProject + mapItem.perct.value;
                            } else if (vm.formatDate(item.endDate.value) >= vm.formatDate(mapItem.startDate.value) && vm.formatDate(item.endDate.value) <= vm.formatDate(mapItem.endDate.value)) {
                                totalTimeProject = totalTimeProject + mapItem.perct.value;
                            }
                        }
                    })
                    if (totalTimeProject + totalTimeDeparment > 100) {
                        isPerctSmallerMax = false;
                    }
                }
            })
            return isPerctSmallerMax;
        }
        vm.validateProjectName = function () {
            // Validate khong duoc chon du an giong nhau
            var isDuplicateName = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                var listTg = angular.copy(vm.card.projectList.data)
                var index = vm.card.projectList.data.indexOf(item);
                listTg.splice(index, 1);
                angular.forEach(listTg, function (mapItem) {
                    if (item.name.value.value === mapItem.name.value.value && item.perct.value !== 0 && mapItem.perct.value !== 0 && item.lineDisable && mapItem.lineDisable) {
                        isDuplicateName = false;
                    }
                })
            })
            return isDuplicateName;
        }
        vm.validateDateInProject = function () {
            // Validate khong duoc chon du an giong nhau
            var isNullDate = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                if ((!item.endDate.value || !item.startDate.value) && item.perct.value)
                {
                    isNullDate = false;
                    return false
                }
            })
            return isNullDate;
        }
        vm.validateNull = function () {
            // Validate khong duoc chon du an giong nhau
            var isNullDate = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                if ((!item.endDate.value || !item.startDate.value) && item.perct.value != 0) {
                    isNullDate = false;
                    return false;
                }
            })
            return isNullDate;
        }
        vm.validateTimeSmall = function () {
            var isValidateTime = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                // bắt đầu nhỏ hơn kết thúc. Kết thúc lớn hơn ngày đầu tháng
                if ((vm.formatDate(item.startDate.value) > vm.formatDate(item.endDate.value) || vm.firstDay > vm.formatDate(item.endDate.value)) && !item.lineDisable && item.perct.value > 0) {
                    isValidateTime = false;
                }
            })
            return isValidateTime;
        }
        vm.validateTime = function () {
            var isValidateTime = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                // endate thì ko được quá endate của dự án vaf startdate phai lon hon hoac bang
                if (item.perct.value > 0) {
                    var projectTgItem = vm.projectsListSelectedFullData.filter(function (itemTg) {
                        return itemTg.PSPID === item.name.value.key;
                    })[0];
                    if (projectTgItem) {
                        if (vm.formatDate(item.endDate.value) > vm.formatDate(projectTgItem.ENDDA)) {
                            isValidateTime = false;
                        } else if (vm.formatDate(item.startDate.value) < vm.formatDate(projectTgItem.BEGDA)) {
                            isValidateTime = false;
                        }
                    }
                }
            })
            return isValidateTime;
        }
        vm.onApprove = function () {
            if (vm.validateNull()) {
                if (vm.validateProjectName()) {
                    if (vm.validateTimeSmall()) {
                        if (vm.validateTime()) {
                            if (vm.validatePrectInDay()) {
                                var COMMechanismListSelectedItemPush = {}
                                var projectPush = []
                                var departmentsPush = []
                                angular.forEach(vm.card.projectList.data, function (projectItem) {
                                    angular.forEach(vm.projectsListSelectedFullData, function (projectSelectItem) {
                                        if (projectItem.name.value.key === projectSelectItem.PSPID && !(projectItem.perct.value === 0 && projectItem.isNew)) {
                                            projectPush.push({
                                                id: projectSelectItem.PSPID,
                                                name: projectSelectItem.SNAME,
                                                PERNR: projectSelectItem.PERNR,
                                                ch: projectSelectItem.PROJ_TYPE,
                                                perct: projectItem.perct.value ? projectItem.perct.value : 0,
                                                startDate: $filter('date')(projectItem.startDate.value, "yyyy-MM-dd"),
                                                endDate: $filter('date')(projectItem.endDate.value, "yyyy-MM-dd"),
                                                PROJ_TYPE_NAME: projectSelectItem.PROJ_TYPE_NAME,
                                            })
                                        }
                                    })
                                })
                                angular.forEach(vm.card.departmentList.data, function (departmentItem) {
                                    angular.forEach(vm.orgUnitLevel5SelectedSelectedFullData, function (departmentSelectItem) {
                                        if (departmentItem.ORG_STEXT.value.key == departmentSelectItem.OBJID) {
                                            departmentsPush.push({
                                                PERNR: departmentItem.ORG_STEXT.value.key,
                                                ORG_ID: departmentSelectItem.OBJID,
                                                ORG_SHORT: departmentSelectItem.SHORT,
                                                ORG_STEXT: departmentSelectItem.STEXT,
                                                // HEAD: '',
                                                HEAD_NAME: departmentItem.HEAD_NAME.value,
                                                HILFM: departmentSelectItem.HILFM,
                                                // PROZT: departmentItem.PROZT.value ? departmentItem.PROZT.value : 0
                                                PROZT: 0
                                                // BEGDA: '',
                                                // ENDDA: ''
                                            })
                                        }
                                    })
                                })
                                angular.forEach(vm.COMMechanismListSelectedFullData, function (item) {
                                    if (item.HCMCode === vm.card.salary.data[0].comMechanisms.value.key) {
                                        COMMechanismListSelectedItemPush = {
                                            ICNUM: item.HCMCode,
                                            COMMechanismCode: item.COMMechanismCode,
                                            Regulation: item.Regulation,
                                            OTRegulation: item.OTRegulation,
                                            BEGDA: item.BEGDA,
                                            ENDDA: item.ENDDA
                                        }
                                    }
                                })
                                vm.oldResponse.statementCheckin.cdt = vm.card.info.data[0].cdt.value.value
                                vm.oldResponse.statementCheckin.level = vm.card.info.data[0].level.value.value
                                vm.oldResponse.statementCheckin.track3 = vm.card.info.data[0].track3.value.value.replace('|||','-')
                                vm.oldResponse.statementCheckin.sp = vm.card.info.data[0].sp.value.value
                                vm.oldResponse.statementCheckin.comMechanisms = Object.keys(COMMechanismListSelectedItemPush).length !== 0 ? COMMechanismListSelectedItemPush : null
                                vm.oldResponse.overtimeQuotas = vm.overtimeTime
                                vm.oldResponse.projects = projectPush
                                vm.oldResponse.departments = departmentsPush
                                apiService.create('Employee/Order', vm.oldResponse, function (_response) {
                                    $uibModalStack.dismissAll();
                                    vm.isEdit = !vm.isEdit;
                                    vm.refresh()
                                });
                            } else {
                                notificationService.errorTranslate('PERCT_DEPARMENT_SMALLER');
                            }
                        } else {
                            notificationService.errorTranslate('TIME_IS_CENTER_START_AND_END');
                        }
                    } else {
                        notificationService.errorTranslate('START_DATE_SMALL_THAN_END_DATE');
                    }
                } else {
                    notificationService.errorTranslate('NOT_DUPLICATE_NAME');
                }
            } else {
                notificationService.errorTranslate('DATE_IS_NOT_NULL');
            }

        }
        vm.onAddItem = function (type) {
            if (type === 'projectList') {
                var item = angular.copy(vm.projectEmpty)
                vm.projectData.push(item);
            } else {
                var item = angular.copy(vm.departmentEmpty)
                vm.departmentData.push(item);
            }
        }
        vm.onRemoveItem = function (type, item) {
            if (type === 'projectList') {
                var index = vm.projectData.indexOf(item);
                if (item.isNew) {
                    vm.projectData.splice(index, 1);
                } else {
                    vm.card.projectList.data[index].perct.value = 0;
                }
            } else {
                var index = vm.departmentData.indexOf(item);
                vm.departmentData.splice(index, 1);
            }
        }
        vm.openDatePopup = function (item, lineData) {
            var count = 0;
            var oldDataItem = angular.copy(lineData)
            oldDataItem.perct.value = 0;
            angular.forEach(vm.oldProjectList, function (itemOld) {
                if (lineData.isNew) {
                    count++;
                } else if (lineData.name.value.value === itemOld.name.value.value) {
                    count++;
                }
            })
            if (count === 0) {
                vm.oldProjectList.push(oldDataItem)
            }
            item.isOpen = true;
        }
        vm.openDetails = function (type, data) {
            $uibModal.open({
                animation: true,
                templateUrl: type === 'projectList' ? 'app/pages/reportWrong/widgets/project/projectReport.html' : 'app/pages/reportWrong/widgets/department/departmentReport.html',
                size: 'lg',
                windowClass: 'app-modal-window',
                controller: type === 'projectList' ? 'ProjectReportCtrl' : 'DepartmentReportCtrl',
                resolve: {
                    data: function () {
                        return data;
                    },
                    type: function () {
                        return type;
                    }
                }
            });
        }
    }
})();