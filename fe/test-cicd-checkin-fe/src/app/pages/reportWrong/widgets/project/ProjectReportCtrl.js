(function () {
    'use strict';

    angular.module('BlurAdmin.pages.reportwrong')
        .controller('ProjectReportCtrl', ProjectReportCtrl);

    /** @ngInject */
    function ProjectReportCtrl($scope, apiService, type, data) {
        $scope.oldData = angular.copy(data)
        $scope.init = function () {
            angular.forEach($scope.oldData, function (value, key) {
                delete value.projectStartTime;
                delete value.projectEndTime;
                delete value.projectPercent;
                delete value.end;
                if (!value.isNew) {
                    delete value.isNew;
                    $scope.projectHistory.push(value)
                }
            });
        }
        $scope.data = [{
                key: 1,
                value: 'Select 1'
            },
            {
                key: 2,
                value: 'Select 2'
            }
        ];
        $scope.project = "Dự án ESS";
        $scope.projectHistory = [];
        $scope.refreshData = function (input) {
        }
        $scope.onChangeValue = function (value) {
        }
    }
})();