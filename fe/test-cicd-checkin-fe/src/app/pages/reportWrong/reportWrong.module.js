/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reportwrong', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('reportWrong', {
        url: '/reportWrong',
        title: 'REPORT_WRONG',
        sidebarMeta: {
          icon: 'fa fa-user',
          order: 1,
        },
        templateUrl: 'app/pages/reportWrong/reportWrong.html',
        controller: 'ReportWrongCtrl',
        'permission': 'Employee.GetMechanismInformation'
      });
  }

})();
