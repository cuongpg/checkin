/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages', [
    'ui.router',
    'ngCookies',
    'BlurAdmin.pages.dashboard',
    //'BlurAdmin.pages.property',
    'BlurAdmin.pages.checkin',
    'BlurAdmin.pages.approvalRecord',
    // 'BlurAdmin.pages.ui',
    // 'BlurAdmin.pages.components',
    // 'BlurAdmin.pages.form',
    // 'BlurAdmin.pages.tables',
    // 'BlurAdmin.pages.charts',
    // 'BlurAdmin.pages.maps',
    // 'BlurAdmin.pages.licenses',
    // 'BlurAdmin.pages.profile',
    'BlurAdmin.pages.system',
    'BlurAdmin.pages.attendance',
    'BlurAdmin.pages.myAttendance',
    'BlurAdmin.pages.report',
    'BlurAdmin.pages.timesheet',
    'BlurAdmin.pages.approve',
    'BlurAdmin.pages.feedback',
    'BlurAdmin.pages.changeManager',
    'BlurAdmin.pages.faq',
    // 'BlurAdmin.pages.profileWork',
    //'BlurAdmin.pages.reportwrong',
    'BlurAdmin.pages.reportwrong2'

  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    var $cookies;
    angular.injector(['ngCookies']).invoke(['$cookies', function(_$cookies_) {
      $cookies = _$cookies_;
    }]);
    if ($cookies.get('is_leader') == "leader" ) {
      $urlRouterProvider.otherwise('/approve');
    }
    else {
      $urlRouterProvider.otherwise('/checkin');
    }

    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Pages',
    //   icon: 'ion-document',
    //   subMenu: [{
    //     title: 'Sign In',
    //     fixedHref: 'auth.html',
    //     blank: true
    //   }, {
    //     title: 'Sign Up',
    //     fixedHref: 'reg.html',
    //     blank: true
    //   }, {
    //     title: 'User Profile',
    //     stateRef: 'profile'
    //   }, {
    //     title: '404 Page',
    //     fixedHref: '404.html',
    //     blank: true
    //   }]
    // });
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Menu Level 1',
    //   icon: 'ion-ios-more',
    //   subMenu: [{
    //     title: 'Menu Level 1.1',
    //     disabled: true
    //   }, {
    //     title: 'Menu Level 1.2',
    //     subMenu: [{
    //       title: 'Menu Level 1.2.1',
    //       disabled: true
    //     }]
    //   }]
    // });
  }

})();
