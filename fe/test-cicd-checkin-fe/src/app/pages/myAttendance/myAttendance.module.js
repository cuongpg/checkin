(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.myAttendance', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('myAttendance', {
          url: '/myAttendance',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'MY_TIMESHEET',
          sidebarMeta: {
            icon: 'ion-clipboard',
            order: 3,
          },
          permission: ['TimeKeeping.GetMyAbnormalTimeKeeping']
        })
        .state('myAttendance.myIrregular',
        {
          url: '/myIrregular',
          templateUrl: 'app/pages/myAttendance/myIrregular/myIrregular.html',
          controller: 'MyIrregularCtrl',
          controllerAs: 'vm',
          title: 'MY_IRREGULAR',
          sidebarMeta: {
            order: 2,
          },
        })
        .state('myAttendance.myovertime',
        {
          url: '/myOvertime',
          templateUrl: 'app/pages/myAttendance/myOvertime/myOvertime.html',
          controller: 'MyOvertimeCtrl',
          controllerAs: 'vm',
          title: 'MY_OVERTIME',
          sidebarMeta: {
            order: 3,
          },
        })
        .state('myAttendance.myLeave',
        {
          url: '/myLeave',
          templateUrl: 'app/pages/myAttendance/myLeave/myLeave.html',
          controller: 'MyLeaveCtrl',
          controllerAs: 'vm',
          title: 'MY_LEAVE',
          sidebarMeta: {
            order: 4,
          },
        })
        // .state('myAttendance.myTimeSheets',
        // {
        //   url: '/myTimeSheets',
        //   templateUrl: 'app/pages/myAttendance/myTimeSheets/myTimeSheets.html',
        //   controller: 'MyTimeSheetsCtrl',
        //   title: 'MY_TIMESHEET',
        //   sidebarMeta: {
        //     order: 1,
        //   },
        // })
        .state('myAttendance.unovertime',
        {
          url: '/myUnovertime',
          templateUrl: 'app/pages/myAttendance/unovertime/unovertime.html',
          controller: 'MyUnOvertimeCtrl',
          controllerAs: 'vm',
          title: 'UN_OVERTIME',
          sidebarMeta: {
            order: 3,
          },
        });
    }
  
  })();
  