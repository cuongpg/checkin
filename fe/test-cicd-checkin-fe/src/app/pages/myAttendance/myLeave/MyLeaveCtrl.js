(function () {
  'use strict';

  angular.module('BlurAdmin.pages.myAttendance')
    .controller('MyLeaveCtrl', MyLeaveCtrl);

  /** @ngInject */
  function MyLeaveCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, menuService) {

    $scope.openPopup = function (page, size) {
      $uibModal.open({
        animation: true,
        templateUrl: page,
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          datas: function () {
            return $scope.items;
          }
        }
      });
    };

    var ModalInstanceCtrl = function ($scope) {
      $scope.open = open;
      $scope.opened = [];
      for (var i = 0; i < 1; i++) {
        $scope.opened.push(false);
      }

      function open(i) {
        $scope.opened[i] = !$scope.opened[i];
      }

      $scope.no = function (page, size) {
        window.alert("no!");
        $uibModalStack.dismissAll();
      };

      $scope.yes = function (page, size) {
        window.alert("yes!");
        $uibModalStack.dismissAll();
      };

    };
    var action = false;
    $scope.myLeave = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      default: {
        status: 0
      },
      employee: null,

      init: function (tableState) {
        $scope.myLeave.tableState = tableState;
        $scope.myLeave.refresh();
      },
      refresh: function () {
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        _this.selected = [];
        _this.isLoading = true;
        var ApprovalUrl = $scope.myLeave.employee ? 'ApprovalRecord/GetApprovalRecordsForAdmin?employeeCode=' + $scope.myLeave.employee.id : 'ApprovalRecord/GetMyApprovalRecords'
        apiService.get(ApprovalUrl, _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      selectAll: function (collection) {
        var _this = this;
        if (_this.selected.length === 0) {
          angular.forEach(collection, function (val) {
            if (!val.existsExtractedSms) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          });
          _this.isSelectAll = true;
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length) {
          angular.forEach(collection, function (val) {
            var found = _this.selected.indexOf(val.id);
            if (found == -1) _this.selected.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        }
        else {
          _this.selected = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selected.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selected.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selected.push(id);
          if (this.selected.length === this.tableData.length) {
            this.isSelectAll = true;
          }
        }
      },
      cancel: function (userId) {
        action = true;
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_REJECT', function () {
          apiService.create('ApprovalRecord/DestroyApprovalRecord', dataPost, function (response) {
            _this.refresh();
            menuService.updateMenu();
          });
        });
      },
      openDetails: function (item) {
        if (action == true) {
          action = false;
        }
        else {
          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/myAttendance/myLeave/widgets/myLeaveDetails.html',
            size: 'md',
            controller: 'myLeaveDetailsModalCtrl',
            resolve: {
              item: function () {
                return item;
              }
            }
          });
        }
      },
      adminApprove: function (userId) {
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_APPROVE', function () {
          apiService.create('ApprovalRecord/AdminApproveLeaveRequest?employeeCode=' + $scope.myLeave.employee.id + '&isForManager=false', dataPost, function (response) {
            _this.refresh();
          });
        });
      },
      adminReject: function (userId) {
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        // confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_APPROVE', function () {
        //   apiService.create('ApprovalRecord/AdminRejectLeaveRequest?employeeCode=' + $scope.myLeave.employee.id + '&isForManager=false', dataPost, function (response) {
        //     _this.refresh();
        //   });
        // });
        _this.isSelectAll = false;
        var uibModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/approve/rejectModal.html',
          controller: 'RejectModalCtrl',
          size: 'sm',
          resolve: {
            details: function () {
              return _this;
            },
            urlReject: function () {
              return 'ApprovalRecord/AdminRejectLeaveRequest?employeeCode=' + $scope.myLeave.employee.id + '&isForManager=false';
            },
            dataPost: function () {
              return dataPost;
            }
          }
        });
      },


    };
  }
})();