(function () {
  'use strict';

  angular.module('BlurAdmin.pages.myAttendance')
    .controller('MyUnOvertimeCtrl', MyUnOvertimeCtrl);

  /** @ngInject */
  function MyUnOvertimeCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, notificationService) {
    
    $scope.eventsGroup = [];
    $scope.eventsChecked = [];
    $scope.getEventsGroup = function () {
      apiService.get('Event/GetGroupEvents',null, function (response) {
        $scope.eventsGroup = response.data;
        angular.forEach($scope.eventsGroup, function (val) {
          $scope.eventsChecked.push(val.id)
        });
      }, function () {
      });
    };  

    $scope.onChangeEventsGroup = function(eventsId)
    {
      if($scope.eventsChecked.indexOf(eventsId) == -1)
      {
        $scope.eventsChecked.push(eventsId)
      }
      else
      {
        $scope.eventsChecked.splice($scope.eventsChecked.indexOf(eventsId), 1);
      }
      $scope.unOvertime.refresh();
    }  

    $scope.openPopup = function (page, size) {
      $uibModal.open({
        animation: true,
        templateUrl: page,
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          datas: function () {
            return $scope;
          }
        }
      });
    };
    var action = false;
    var ModalInstanceCtrl = function ($scope) {
      $scope.no = function () {
        $uibModalStack.dismissAll();
      };

      $scope.yes = function (type) {
        var ids = $scope.$resolve.datas.unOvertime.selected;
        if (ids.length == 0) {
          notificationService.warning('PLEASE_CHOOSE_ONE_RECORD');
        }
        else {
          var dataPost = {
            "ids": ids
          }
          if (type == 'reject') {
            apiService.create('OverTime/RejectDestroyOverTime', dataPost, function (response) {
              $scope.$resolve.datas.unOvertime.refresh()
              $scope.$resolve.datas.unOvertime.isSelectAll= false;
            });
          }
          else {
            apiService.create('OverTime/ApprovalDestroyOverTime', dataPost, function (response) {
              $scope.$resolve.datas.unOvertime.refresh()
              $scope.$resolve.datas.unOvertime.isSelectAll= false;
            });
          }
        }
        $uibModalStack.dismissAll();
      };
    };

    $scope.unOvertime = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.unOvertime.tableState = tableState;
        $scope.unOvertime.refresh();
      },
      refresh: function () {
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        _this.tableState.groupEventIds = $scope.eventsChecked
        _this.selected = [];
        _this.isLoading = true;
        apiService.get('/OverTime/GetMyDestroyOvertime', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {

          _this.isLoading = false;
        });
      },
      selectAll: function (collection) {
        var _this = this;
        if (_this.selected.length === 0) {
          angular.forEach(collection, function (val) {
            if (val.statusDestroy == 0) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          });
          _this.isSelectAll = true;
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length) {
          angular.forEach(collection, function (val) {
            var found = _this.selected.indexOf(val.id);
            if (found == -1) _this.selected.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        }
        else {
          _this.selected = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selected.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selected.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selected.push(id);
          if (this.selected.length === this.tableData.length) {
            this.isSelectAll = true;
          }
        }
      },
      approve: function (userId) {
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_APPROVE', function () {
          apiService.create('OverTime/ApprovalDestroyOverTime', dataPost, function (response) {
            _this.refresh();
          });
        });
      },
      reject: function (userId) {
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        _this.isSelectAll = false;
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_REJECT', function () {
          apiService.create('OverTime/RejectDestroyOverTime', dataPost, function (response) {
            _this.refresh();
          });
        });
      },
      openDetails: function (item) {
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/attendance/unovertime/widgets/unovertimeDetails.html',
          size: 'md',
          controller: 'unovertimeDetailsModalCtrl',
          resolve: {
            item: function () {
              return item;
            }
          }
        });

      }
    };
  }
})();