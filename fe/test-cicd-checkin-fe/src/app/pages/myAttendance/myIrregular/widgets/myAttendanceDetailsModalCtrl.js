/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
    'use strict';


    angular.module('BlurAdmin.pages.myAttendance')
        .controller('myAttendanceDetailsModalCtrl', myAttendanceDetailsModalCtrl);


    /** @ngInject */
    function myAttendanceDetailsModalCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, item, confirmService, $state) {
        $scope.details = {
            checkInTime: item.checkInTime,
            checkOutTime: item.checkOutTime,
            email: item.email,
            id: item.id,
            imageUrl: item.imageUrl,
            managerEmail: item.managerEmail,
            managerName: item.managerName,
            name: item.name,
            note: item.note,
            status: item.status,
            timeSubmit: item.timeSubmit,
            workDate: item.workDate,
            rejectReason: item.rejectReason
        };

    }
})();