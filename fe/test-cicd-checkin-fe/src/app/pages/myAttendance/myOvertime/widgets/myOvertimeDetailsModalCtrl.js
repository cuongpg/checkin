/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
    'use strict';


    angular.module('BlurAdmin.pages.attendance')
        .controller('myOvertimeDetailsModalCtrl', myOvertimeDetailsModalCtrl);


    /** @ngInject */
    function myOvertimeDetailsModalCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, item, confirmService, $state) {
        $scope.details = {
            email: item.email,
            endTime: item.endTime,
            event: item.event,
            id: item.id,
            imageUrl: item.imageUrl,
            isDestroy: item.isDestroy,
            managerEmail: item.managerEmail,
            managerName: item.managerName,
            name: item.name,
            note: item.note,
            percentage: item.percentage,
            startTime: item.startTime,
            status: item.status,
            statusDestroy: item.statusDestroy,
            timeDestroy: item.timeDestroy,
            timeSubmit: item.timeSubmit,
            timeSubmitDestroy: item.timeSubmitDestroy,
            workDate: item.workDate,
            rejectReason: item.rejectReason,

            cancel: function (userId) {
                var dataPost = {
                    "ids": [
                        userId
                    ]
                }
                confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_REJECT', function () {
                    apiService.create('OverTime/DestroyOverTime', dataPost, function (response) {
                        $uibModalInstance.close();
                        $state.reload();
                    });
                });
            },


        };


    }
})();