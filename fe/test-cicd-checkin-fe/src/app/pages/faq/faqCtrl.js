(function () {
  'use strict';

  angular.module('BlurAdmin.pages.feedback')
    .controller('faqCtrl', faqCtrl);

  /** @ngInject */
  function faqCtrl($scope, $window, $cookies) {
    $scope.lang_cookie = $cookies.get('NG_TRANSLATE_LANG_KEY');
    if($scope.lang_cookie == 'th')
      $window.open('https://docs.google.com/spreadsheets/d/1pgcYwZVnXDR7MgwSnBz1zQksMbiyrzfBb4YfJdr9Tjg/edit#gid=46420486', '_blank');
    if($scope.lang_cookie == 'vi')
      $window.open('https://docs.google.com/spreadsheets/d/1pgcYwZVnXDR7MgwSnBz1zQksMbiyrzfBb4YfJdr9Tjg/edit#gid=0', '_blank');
    if($scope.lang_cookie == 'en')
      $window.open('https://docs.google.com/spreadsheets/d/1pgcYwZVnXDR7MgwSnBz1zQksMbiyrzfBb4YfJdr9Tjg/edit#gid=53651578', '_blank');
  }
})();