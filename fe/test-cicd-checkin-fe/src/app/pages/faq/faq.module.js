/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.faq', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('faq', {
        url: '/faq',
        templateUrl: 'app/pages/faq/faq.html',
        title: 'FAQ',
        controller: 'faqCtrl',
        sidebarMeta: {
          icon: 'ion-alert-circled',
          order: 11,
        },
      });
  }


})();