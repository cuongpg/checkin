(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('system.changeManagerInfo', {
          url: '/changeManagerInfo',
          title: 'CHANGE_MANAGER_INFO',
          templateUrl: 'app/pages/system/changeManagerInfo/changeManagerInfo.html',
          controller: 'changeManagerInfoCtrl',
          sidebarMeta: {
            order: 8,
          }
        });
    }
  
  })();