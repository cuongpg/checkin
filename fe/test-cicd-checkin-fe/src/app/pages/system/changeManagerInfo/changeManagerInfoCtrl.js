(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('changeManagerInfoCtrl', changeManagerInfoCtrl);

  /** @ngInject */
  function changeManagerInfoCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, $filter) {



    $scope.changeManagerInfo = {
      nations: [
        {
          key: 'VIETNAM',
          value: "Việt Nam"
        },
        {
          key: 'THAILAN',
          value: "Thái Lan"
        }
      ],
      nationalitySelected: null,
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      default: {
        status: 0
      },
      employeeSelected: null,
      emplyees: [],
      eployee: {
        status: [1],
        tableState: null,
      },
      allEmployee: {
        all: true,
        email: "All",
      },


      init: function (tableState) {
        $scope.changeManagerInfo.employeeSelected = $scope.changeManagerInfo.allEmployee
        $scope.changeManagerInfo.tableState = tableState;
        $scope.changeManagerInfo.initUser();
      },

      initUser: function () {
        var _this = this;
        apiService.get("users/GetMyUser", null, function (response) {
          angular.forEach(_this.nations, function(nation){
            if(nation.value == response.data.nationality)
            _this.nationalitySelected = nation;
          })
          // _this.nationalitySelected = response.data.nationality
          _this.refresh();
        });
      },

      refresh: function () {
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        _this.selected = [];
        _this.isLoading = true;
        var url = 'ChangeManagerInfo/GetChangeManagerInfos' + '?nationality=' + _this.nationalitySelected.value;
        if (!_this.employeeSelected.all) {
          url += '&employeeId=' + _this.employeeSelected.id;
        }
        apiService.get(url, _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          angular.forEach(_this.tableData, function (data) {
            data.createdAt = $filter('date')(data.createdAt, 'dd/MM/yyyy');
            data.updatedAt = $filter('date')(data.updatedAt, 'dd/MM/yyyy');
          })
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },

      refreshEmployee: function (input) {
        var _this = this
        if (input) _this.getEmployee(input);
        else _this.emplyees = [_this.allEmployee];
      },

      getEmployee: function (key) {
        var _this = this;
        _this.eployee.status = [1];
        _this.eployee.tableState = {
          all: {
            query: {
              fullName: key,
              email: key,
              employeeCode: key
            }
          },
          pagination: {
            number: 10,
            numberOfPages: 1,
            start: 0
          },
          search: {},
          sort: {}
        }
        apiService.get('users' + '?status=' + _this.eployee.status + '&scriptId=' + 0, _this.eployee.tableState, function (response) {
          _this.emplyees = response.data.data;
          _this.emplyees.push(_this.allEmployee);
        }, function () {
        });
      },
    };


  }
})();