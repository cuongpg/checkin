(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system', {
        url: '/system',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'SYSTEM',
        sidebarMeta: {
          icon: 'ion-settings',
          order: 6,
        },
        'permission': ['Users.GetAll', 'Groups.GetAll']
      });
  }

})();
