(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('OvertimeAllocationCtrl', OvertimeAllocationCtrl);

  /** @ngInject */
  function OvertimeAllocationCtrl($scope, apiService, confirmService, notificationService, $uibModal) {

    $scope.users = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 2500,
      tableData: [],
      scriptFilter: null,
      scripts: [],
      status: true,
      version: 'V2',
      initScripts: function () {
        var _this = this;
        apiService.get('Script', null, function (response) {
          _this.scripts = response.data;
          _this.scriptFilter = {
            id: 0,
            subScript: "All",
            all: true
          }
          _this.scripts.push(_this.scriptFilter);
          $scope.users.refresh();
        });
      },
      init: function (tableState) {
        $scope.users.tableState = tableState;
        $scope.users.initScripts();

      },
      refresh: function () {
        var _this = this;
        _this.selected = [];
        _this.isSelectAll = false;
        _this.isLoading = true;
        apiService.get('users' + '?isActive=' + $scope.users.status + '&scriptId=' + _this.scriptFilter.id, _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      report: function () {
        var _this = this;
        if (_this.selected.length !== 0) {
          confirmService.showTranslate('DO_YOU_WANT_SENT_OVERTIME_ALLOCATION', function () {
            var dataPost = {
              "userIds": _this.selected
            }
            var url = _this.version === 'V1' ? 'Employee/CreatedSessionAllocation' : 'Employee/CreatedSessionAllocationV2'
            apiService.create(url, _this.selected, function (response) {
              _this.refresh();
            });
          }, function () {});
        } else {
          notificationService.warningTranslate('PLEASE_CHOOSE_ONE_RECORD');
        }
      },
      selectAll: function (collection) {
        var _this = this;
        if (this.tableState.all) {
          if(_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        if (_this.selected.length === 0) {
          angular.forEach(collection, function (val) {
            _this.selected.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length && !_this.isSelectAll) {
          angular.forEach(collection, function (val) {
            var found = _this.selected.indexOf(val.id);
            if (found == -1) _this.selected.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length && _this.isSelectAll) {
          _this.selected = [];
          _this.isSelectAll = false;
        } else {
          _this.selected = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selected.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) {
          return o.id === id;
        })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selected.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selected.push(id);
          var count = 0;
          angular.forEach(this.tableData, function (val) {
            if (val.status == 0) {
              count++;
            }
          });
          if (this.selected.length === count) {
            this.isSelectAll = true;
          }
        }
      },
      delete: function (userId) {
        var _this = this;
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function () {
          apiService.delete('users/' + userId, function (response) {
            _this.refresh();
          });
        });
      },

      showAllocate: function(item){
        item.isAdmin = true;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/reportWrong2/reportWrong2.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'ReportWrong2Ctrl',
          resolve: {
            employee: function () {
              return item;
            }
          }
        });
      }
    };
  }

})();