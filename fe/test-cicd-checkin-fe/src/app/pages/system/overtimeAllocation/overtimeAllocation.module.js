(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.overtimeAllocation', {
        url: '/overtimeAllocation',
        title: 'OVERTIME_ALLOCATION_TITLE',
        templateUrl: 'app/pages/system/overtimeAllocation/overtimeAllocation.html',
        controller: 'OvertimeAllocationCtrl',
        sidebarMeta: {
          order: 0,
        },
        // 'permission': 'Users.GetAll'
      });
  }

})();
