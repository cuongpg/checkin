(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('UserGroupPageCtrl', UserGroupPageCtrl);

  /** @ngInject */
  function UserGroupPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate) {
    $scope.userGroup = {
      isCreateMode: _.isEmpty($stateParams.userGroupId),
      isEdit: !_.isEmpty($stateParams.userGroupId),
      isSave: _.isEmpty($stateParams.userGroupId),
      routes: [],
      selection: [],
      form: {},
      back: function () {
        $state.go('system.userGroups');
      },
      reload: function () {
        $state.reload();
      },
      showEdit: function() {
        var _this = this;
        _this.isEdit = false;
        $timeout(function() {
          _this.isSave = true;
        }, 200);
      },
      initRoutes: function() {
        var _this = this;
        apiService.get('routes', null, function (response) {
          _.forEach(response.data, function(route) {
            $translate(route.name).then(function (messageTranslated) {
              route.name = messageTranslated;
              _this.routes.push(route);
            }, function (translationId) {
              route.name = translationId;
              _this.routes.push(route);
            });
          });
        });
      },
      toggleSelection: function (routeId) {
        var idx = this.selection.indexOf(routeId);
        if (idx > -1) {
          this.selection.splice(idx, 1);
        } else {
          this.selection.push(routeId);
        }
      },
      init: function() {
        var _this = this;
        _this.initRoutes();
        if (!_this.isCreateMode) {
          apiService.get('groups/' + $stateParams.userGroupId, null, function (response) {
            _this.form = response.data;
            _this.selection = _.map(response.data.groupRoutes, function (o) {
              return o.route.id;
            });
          });
        }
      },
      save: function () {
        var _this = this;
        var data = _.clone(_this.form);
        data.groupRoutes = [];
        _.forEach(_this.selection, function (routeId) {
          data.groupRoutes.push({
            RouteId: routeId
          });
        })
        if (_this.isCreateMode) {
          apiService.create('groups', data, function (response) {
            _this.back();
          });
        } else {
          apiService.update('groups/' + $stateParams.userGroupId, data, function (response) {
            _this.reload();
          });
        }
      }
    };
  }

})();
