(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('UserGroupsPageCtrl', UserGroupsPageCtrl);

  /** @ngInject */
  function UserGroupsPageCtrl($scope, apiService, confirmService) {

    $scope.userGroups = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      init: function (tableState) {
        $scope.userGroups.tableState = tableState;
        $scope.userGroups.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        apiService.get('groups', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function() {
          _this.isLoading = false;
        });
      },
      delete: function (groupId) {
        var _this = this;
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function() {
          apiService.delete('groups/' + groupId, function (response) {
            _this.refresh();
          });
        });
      }
    };
  }

})();
