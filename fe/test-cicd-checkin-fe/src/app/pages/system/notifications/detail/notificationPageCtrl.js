(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .controller('notificationPageCtrl', notificationPageCtrl);
  
    /** @ngInject */
    function notificationPageCtrl($scope, apiService, $stateParams, notificationService, $timeout, $state, $uibModalStack) {
      $scope.itemId = null;
      if ($scope.$resolve.item) {
        $scope.itemId = $scope.$resolve.item;
      }
      $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
      };
      $scope.onReady = function () {
        // ...
      };
      $scope.flowsDetail = {
        isLoading: true,
        isCreateMode: !$scope.itemId,
        isEdit: !!$scope.itemId,
        isSave: !$scope.itemId,
        form: {},
        todoList: [],
        selectFlowStep: [],
  
        init: function () {
          var _this = this;
          // apiService.get('flows/getAllFlowStep', null, function (response) {
          //   _this.selectFlowStep = response.data;
          // });
          if (!_this.isSave) {
            apiService.get('notifications/' + $scope.itemId, null, function (response) {
              _this.form = response.data;
              // _.forEach(response.data, function (value) {
              //   _this.todoList.push(value.flowSteps);
              // });
            });
          }
        },
        back: function () {
          $uibModalStack.dismissAll();
          $state.go('system.notifications');
        },
        reload: function () {
          var _this = this;
          $uibModalStack.dismissAll();
          if(_this.isEdit)
          {
            $state.reload();
          }
        },
        deletedFlow: function (flow) {
          var _this = this;
          var index = _this.todoList.indexOf(flow);
          _this.todoList.splice(index, 1);
          _this.selectFlowStep.push(flow)
        },
        save: function () {
          var _this = this;
          var data = {};
          data = _this.form;
          if (_this.validate(data)) {
            if (_this.isCreateMode) {
              apiService.create('notifications/create', data, function (response) {
                _this.back();
              });
            } else {
              apiService.create('notifications/update', data, function (response) {
                _this.reload();
              });
            }
          }
  
        },
        showEdit: function () {
          var _this = this;
          _this.isEdit = false;
          $timeout(function () {
            _this.isSave = true;
          }, 200);
        },
        validate: function (data) {
          var _this = this;
          return true;
        }
  
      };
    }
  
  })();