(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('system.notification', {
          url: '/notification/{id}',
          title: 'CREATE_NOTIFICATION',
          templateUrl: 'app/pages/system/notifications/detail/notification.html',
          controller: 'notificationPageCtrl',
        });
    }
  
  })();