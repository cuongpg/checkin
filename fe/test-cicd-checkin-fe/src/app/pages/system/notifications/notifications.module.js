(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('system.notifications', {
          url: '/notification',
          title: 'LIST_NOTIFICATION',
          templateUrl: 'app/pages/system/notifications/notifications.html',
          controller: 'notificationsPageCtrl',
          sidebarMeta: {
            order: 8,
          }
        });
    }
  
  })();