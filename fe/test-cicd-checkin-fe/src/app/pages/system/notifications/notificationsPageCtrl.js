(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .controller('notificationsPageCtrl', notificationsPageCtrl);
  
    /** @ngInject */
    function notificationsPageCtrl($scope, apiService, $state, confirmService, $uibModal) {
  
      $scope.notifications = {
        isLoading: true,
        tableState: null,
        tablePageSize: 10,
        tableData: [],
        default: {
          search: {
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            // status:''                 
          }
        },
        init: function (tableState) {
          $scope.notifications.tableState = tableState;
          $scope.notifications.refresh();
        },
        refresh: function () {
          var _this = this;
          _this.isLoading = true;
          apiService.get('notifications/get-all', _this.tableState, function (response) {
            _this.tableData = response.data.data;
            _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
            _this.tableState.pagination.totalItemCount = response.data.totalRecords;
            _this.isLoading = false;
          }, function () {
            _this.isLoading = false;
          });
        },
        delete: function (Id) {
          var _this = this;
          confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function () {
            apiService.create('notifications/delete/' + Id, null, function (response) {
              $state.reload();
            });
          });
        },
        openDetails: function (itemId) {
          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/system/notifications/detail/notification.html',
            size: 'lg',
            windowClass: 'app-modal-window',
            controller: 'notificationPageCtrl',
            resolve: {
              item: function () {
                return itemId;
              }
            }
          });
        }
      }
    }
  
  })();