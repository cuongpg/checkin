(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('UserPageCtrl', UserPageCtrl);

  /** @ngInject */
  function UserPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, $uibModal, $filter, config, notificationService) {
    var uibModalInstance = null;
    $scope.user = {
      isCreateMode: _.isEmpty($stateParams.userId),
      isEdit: !_.isEmpty($stateParams.userId),
      isSave: _.isEmpty($stateParams.userId),
      routes: [],
      groups: [],
      selectionRoute: [],
      selectionGroup: [],
      hasBufferReasons: [],
      managers: [],
      managerSelected: null,
      form: {},
      scripts: [],
      scriptSelected: null,
      effectiveDate: null,
      listCalendarType: [],
      listCorporation: [],
      calendarSelected: null,
      corporations: [],
      corporationSelected: null,
      userGroups: [],
      dateOptions: {},
      activeDatePopup: false,
      disableDatePopup: false,
      activeDate: null,
      dateActive: null,
      estimateOwners: [],
      estimateOwnersSelected: null,
      status: null,
      statusHrb: 1,
      nationalities: [
        {
          key: 'VIETNAM',
          value: "Việt Nam"
        },
        {
          key: 'THAILAN',
          value: "Thái Lan"
        }
      ],
      nationalitySelected: null,
      headUnits: [],
      headUnitSelected: null,
      workProgressList: [],

      back: function () {
        $state.go('system.users');
      },
      reload: function () {
        $state.reload();
      },
      showEdit: function () {
        var _this = this;
        _this.isEdit = false;
        $timeout(function () {
          _this.isSave = true;
        }, 200);
      },
      initGroups: function () {
        var _this = this;
        apiService.get('groups/all', null, function (response) {
          _this.groups = response.data;
        });
      },
      initScripts: function () {
        var _this = this;
        apiService.get('Script', null, function (response) {
          _this.scripts = response.data;
        });
      },
      toggleSelectionGroup: function (groupId) {
        var idx = this.selectionGroup.indexOf(groupId);
        if (idx > -1) {
          this.selectionGroup.splice(idx, 1);
        } else {
          this.selectionGroup.push(groupId);
        }
      },
      initCalendar: function () {
        var _this = this;
        apiService.get('users/GetAllCalendarTypes', null, function (response) {
          _this.listCalendarType = response.data
        })
      },
      initWorkProgress: function () {
        var _this = this;
        apiService.get('users/WorkProgress/'+ $stateParams.userId, null, function (response) {
          _this.workProgressList = response.data;
          var nowWorkInfo = response.data.filter(function(w){ 
            var now =new Date(new Date().setHours(0,0,0,0)) ;
            var effectiveDate = new Date(w.effectiveDate.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
            var expirationDate = new Date(w.expirationDate.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
            return  effectiveDate <= now && expirationDate >= now 
          })[0];
          _this.effectiveDate = new Date(nowWorkInfo.effectiveDate.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
        })
      },

      init: function () {
        var _this = this;
        _this.initGroups();
        _this.initScripts();
        _this.initCalendar();
        _this.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };
        if (!_this.isCreateMode) {
          _this.initWorkProgress();
          apiService.get('users/' + $stateParams.userId, null, function (response) {
            _this.form = response.data;
            $timeout(function () {
              apiService.get('Script/GetScriptHistoryNow' + '?userId=' + _this.form.id, null, function (res) {
                if (!res.data) {
                  _this.status = 0;
                  _this.form.status = 0;
                }
                else {
                  _this.status = 1;
                  _this.form.status = 1;
                  angular.forEach(_this.scripts, function (script) {
                    if (res.data.scriptId == script.id) {
                      _this.scriptSelected = script;
                     
                    }
                  });
                }
              });
              apiService.get('Script/GetHrbHistoryNow' + '?userId=' + _this.form.id, null, function (res) {
                if (!res.data) {
                  _this.statusHrb = null;
                }
                else {
                  _this.statusHrb = res.data.hrbStatus;
                }
              });
              apiService.get('users/GetGroupsByUserId' + '?userId=' + _this.form.id, null, function (res) {
                _this.selectionGroup = _.map(res.data, function (o) {
                  return o.id;
                });
              });
              getManager(_this.form.managerId, true);
              getHeadUnit(_this.form.headOfOrgUnitId,true);
              getEstimateOwners(_this.form.department, true);
              //getCorporations(_this.form.corporation, true);
              angular.forEach(_this.listCalendarType, function (calendar) {
                if (_this.form.calendarType == calendar.code) {
                  _this.calendarSelected = calendar;
                }
              });
              angular.forEach(_this.nationalities, function(nation){
                if(_this.form.nationality == nation.value) _this.nationalitySelected = nation
              })

            }, 400);
          });
        }
      },
      save: function () {
        var _this = this;
        _this.form.id = _this.form.employeeCode;
        var data = _.clone(_this.form);
        data.groups = _this.selectionGroup;
        if (!_this.managerSelected || !_this.headUnitSelected || !_this.estimateOwnersSelected || !_this.corporationSelected
          || !_this.calendarSelected || !_this.nationalitySelected || !_this.form.employeeCode || !_this.form.email || !_this.form.fullName)
          {
            debugger;
            return notificationService.errorTranslate('MUST_FULLFILL_FORM');
          }

        data.managerId = _this.managerSelected.id;
        data.headOfOrgUnitId = _this.headUnitSelected.id;
        data.activeDate = $filter('date')(_this.form.activeDate, config.dateFormat);
        data.department = _this.estimateOwnersSelected.completeCode;
        data.corporation = _this.corporationSelected.shortCode;
        data.CalendarType = _this.calendarSelected.code;
        data.nationality = _this.nationalitySelected.value;
        if (_this.isCreateMode) {
          if (!_this.scriptSelected)
            return notificationService.errorTranslate('MUST_FULLFILL_FORM');
          data.scriptId = _this.scriptSelected.id;
          data.status = 1;
          apiService.create('users', data, function (response) {
            _this.back();
          });
        } else {
          data.createdAt = $filter('date')(_this.form.createdAt, config.dateFormat);
          data.updatedAt = $filter('date')(_this.form.updatedAt, config.dateFormat);
          apiService.update('users/' + $stateParams.userId, data, function (response) {
            _this.reload();
          });
        }
      },
      refreshManager: function (input) {
        if (input) getManager(input, false);
        else $scope.user.managers = [];
      },
      refreshEstimateOwners: function (input) {
        getEstimateOwners(input, false);
      },
      refreshCorporations: function (input) {
        getCorporations(input, false);
      },
      refreshHeadUnit: function (input) {
        if (input) getHeadUnit(input, false);
        else $scope.user.headUnits = [];
      },
      
      changeScript: function () {
        uibModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/system/users/detail/changeInfoModal.html',
          controller: 'ChangeInfoCtrl',
          size: 'md',
          resolve: {
            title: function () {
              return 'CHANGE_SCRIPT';
            },
            label: function () {
              return 'SCRIPT';
            },
            userId: function () {
              return $stateParams.userId;
            },
            listData: function () {
              return $scope.user.scripts.map(function(a) {return a.subScript;});
            },
            selected: function() {
              return $scope.user.scriptSelected.subScript;
            },
            effectiveDate:  function() {
              var result = new Date($scope.user.effectiveDate);
              result.setDate(result.getDate() + 1);
              return result;
            },
            saveChangeCallback: function() {
              return $scope.user.saveChangeScript;
            },
          }
        });
      },

      saveChangeScript: function(selected, effectiveDate, userId) {
      
        var selectedId = $scope.user.scripts.filter(function (s) {
          return s.subScript === selected;
        })[0].id;
        apiService.create('Script/UpdateScript' + '?scriptId=' + selectedId + '&effectiveDate=' + effectiveDate + '&userId=' + userId, null, function (response) {
          uibModalInstance.dismiss(top.key);
          $state.reload();
        });
      },

      changeLeaveCalenderType: function () {
        uibModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/system/users/detail/changeInfoModal.html',
          controller: 'ChangeInfoCtrl',
          size: 'md',
          resolve: {
            userId: function () {
              return $stateParams.userId;
            },
            title: function () {
              return 'CHANGE_CALENDAR_TYPE';
            },
            label: function () {
              return 'CALENDAR_TYPE';
            },
            listData: function () {
              return $scope.user.listCalendarType.map(function(a) {return a.name;});
            },
            selected: function() {
              return $scope.user.calendarSelected.name;
            },
            effectiveDate:  function() {
              var result = new Date($scope.user.effectiveDate);
              result.setDate(result.getDate() + 1);
              return result;
            },
            saveChangeCallback: function() {
              return $scope.user.saveChangeCalendarType;
            },
          }
        });
      },

      saveChangeCalendarType: function(selected, effectiveDate, userId) {
        var selectedCode = $scope.user.listCalendarType.filter(function (c) {
          return c.name === selected;
        })[0].code;
        apiService.create('users/UpdateLeaveCalendarType' + '?calendarType=' + selectedCode + '&effectiveDate=' + effectiveDate + '&userId=' + userId, null, function (response) {
          uibModalInstance.dismiss(top.key);
          $state.reload();
        });
      },

      onActiveOpenDate: function () {
        var _this = this;
        _this.activeDatePopup = true;
      },
      onDisableOpenDate: function () {
        var _this = this;
        _this.disableDatePopup = true;
      },

      dateChange: function () {
        var _this = this;
      },

      openTimeSheet: function (item) {
        item.isAdmin = true;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/myTimeSheets/myTimeSheets.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'MyTimeSheetsCtrl',
          resolve: {
            item: function () {
              return item;
            }
          }
        });
      },

      getLeaveOfEmployee: function (item) {
        var startDate = new Date();
        var endDate = new Date();
        startDate = $filter('date')(startDate, config.dateFormat);
        endDate = $filter('date')(endDate, config.dateFormat);
        apiService.get('ReasonType/LeaveUserReasons' + '?email=' + item.email + '&startDate=' + startDate + '&endDate=' + endDate, null, function (response) {
          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/system/users/detail/leaveUserReasonsModal.html',
            size: 'lg',
            windowClass: 'app-modal-window',
            controller: 'LeaveUserReasonsCtrl',
            resolve: {
              item: function () {
                return response.data;
              }
            }
          });
        });
      },

      getApprovalOfEmployee: function (item) {
        item.isAdmin = true;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/system/users/detail/employeeApproval.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'employeeApprovalCtrl',
          resolve: {
            employee: function () {
              return item;
            }
          }
        });
      },

      getPendingNeedApprove: function (item) {
        item.isAdmin = true;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/approve/approve.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'ApproveCtrl',
          resolve: {
            employee: function () {
              return item;
            }
          }
        });
      },

      changeUserStatus: function () {
        var _this = this;
        _this.form.status++;
        if (_this.form.status > 2) _this.form.status = 0;
      },

      createApproval: function (item) {
        item.isAdmin = true;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/approvalRecord/approvalRecord.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'ApprovalRecordCtrl',
          resolve: {
            employee: function () {
              return item;
            }
          }
        });
      },

      getLowerGradeOfEmployee: function (item) {
        item.isAdmin = true;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/attendance/lowerGrade/lowerGrade.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'LowerGradeCtrl',
          resolve: {
            employee: function () {
              return item;
            }
          }
        });
      },

      activeEmployee: function (item) {
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/system/users/detail/activeModal.html',
          size: 'sm',
          controller: activeEmployeeCtrl,
          resolve: {
            employee: function () {
              return item;
            }
          }
        });
      },
    };

    var activeEmployeeCtrl = function ($scope, employee) {
      $scope.employee = employee;
      $scope.note = '';

      $scope.date = {
        date: new Date(),
        datePopup: false,
        dateOptions: {},

        onOpenDate: function () {
          var _this = this;
          _this.datePopup = true;
        },
      }
      $scope.confirm = function () {
        var activeUrl;
        var data = _.clone($scope.employee)
        if ($scope.employee.status == 1) {
          data.disableDate = $filter('date')($scope.date.date, config.dateFormat);
          activeUrl = 'users/Deactive/' + data.id
        }
        else {
          data.activeDate = $filter('date')($scope.date.date, config.dateFormat);
          activeUrl = 'users/Active/' + data.id
        }
        apiService.update(activeUrl, data, function (response) {
          $scope.$dismiss();
          $state.reload();
        });

      }
      $scope.no = function () {
        $uibModalStack.dismissAll();
      };
    };

    var getManager = function (key, isEdit) {
      apiService.get('ChangeManagerInfo/GetAllManager' + '?keyWord=' + key + '&perPage=100', null, function (response) {
        if (response.data.data != null) {
          $scope.user.managers = response.data.data;
          if (isEdit) $scope.user.managerSelected = $scope.user.managers[0];
        }
      });
    };

    var getEstimateOwners = function (key, isEdit) {
      apiService.get('EstimateOwner/GetEstimateOwner' + '?keyWord=' + key, null, function (response) {
        if (response.data.estimateOwners != null)
          $scope.user.estimateOwners = response.data.estimateOwners;
        if (isEdit) $scope.user.estimateOwnersSelected = $scope.user.estimateOwners[0];
      })
    };

    var getCorporations = function (key, isEdit) {
      apiService.get('EstimateOwner/GetCorporations' + '?keyWord=' + key, null, function (response) {
        if (response.data.corporations != null)
          $scope.user.corporations = response.data.corporations;
        if (isEdit)
        {
          $scope.user.corporationSelected = $scope.user.corporations[0];
        }
        else
        {
          $timeout(angular.forEach($scope.user.corporations, function(corp){
            if($scope.user.form.corporation == corp.shortCode) $scope.user.corporationSelected = corp;
          }),100);
        }
      })
    };

    var getHeadUnit = function (key, isEdit) {
      apiService.get('ChangeManagerInfo/GetAllManager' + '?keyWord=' + key + '&perPage=100', null, function (response) {
        if (response.data.data != null) {
          $scope.user.headUnits = response.data.data;
          if (isEdit) $scope.user.headUnitSelected = $scope.user.headUnits[0];
        }
      });
    };
  }

})();
