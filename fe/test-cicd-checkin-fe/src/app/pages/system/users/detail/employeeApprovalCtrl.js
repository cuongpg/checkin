(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.approve')
      .controller('employeeApprovalCtrl', employeeApprovalCtrl);
  
    /** @ngInject */
    function employeeApprovalCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, notificationService, menuService, employee) {
      $scope.employee = employee;
      $scope.isForAdmin = false;
      
      $scope.screen = {
        screenShow: 'myLeave',
        chooseLeave: function () {
          $scope.screen.screenShow = 'myLeave'
          // $scope.leaveRequest.refresh();
        },
        chooseIrregular: function () {
          $scope.screen.screenShow = 'myIrregular'
          // $scope.manuallyRecord.refresh();
        },
        chooseOvertime: function () {
          $scope.screen.screenShow = 'myOvertime'
          // $scope.overtime.refresh();
        }
      }
    }
  })();