/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';
  angular.module('BlurAdmin.pages.system')
    .controller('ChangeInfoCtrl', ChangeInfoCtrl);
  /** @ngInject */
  function ChangeInfoCtrl($scope, $filter, userId, listData, selected, effectiveDate, title, label, saveChangeCallback, config) {
    $scope.userId = userId;
    $scope.changeInfo = {
      current: selected,
      listData: listData,
      selected: selected,
      title: title,
      label: label,
      mindate: effectiveDate,
      effectiveDate: effectiveDate,
      datePopup: false,
      dateOptions:  {
        formatYear: 'yy',
        minDate: new Date(effectiveDate),
        startingDay: 1
      },

      init: function () {
        var _this = this;
        _this.datePopup = false;
      },

      onOpenDate: function () {
        var _this = this;
        _this.datePopup = true;
      },

      dateChange: function () {
        var _this = this;
      },

      saveChange: function () {
        var _this = this;
        var effDate = $filter('date')(_this.effectiveDate, config.yyyyMMdd);
        saveChangeCallback(_this.selected, effDate, $scope.userId);
      }
    }
    $scope.changeInfo.init();

  }
})();