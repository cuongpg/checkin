/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.system')
    .controller('LeaveUserReasonsCtrl', LeaveUserReasonsCtrl);


  /** @ngInject */
  function LeaveUserReasonsCtrl($scope, apiService, $filter, notificationService, $window, $translate, $timeout, config, menuService, $state, item) {
    $scope.leaveReason = item;
    $scope.leaveShow = [];
    $scope.leave = function () {
      var fund = $scope.leaveReason
      angular.forEach($scope.leaveReason, function (item) {
        if (item.childs.length != 0) {
          $scope.leaveShow.push(
            {
              name: item.name,
              subName: "",
              hasPaidLeave: "",
              buffer: "",
              expirationDate: ""
            }
          );
          angular.forEach(item.childs, function (child) {
            if (child.buffer.length != 0) {
              angular.forEach(child.buffer, function (buf) {
                $scope.leaveShow.push(
                  {
                    name: "",
                    subName: child.name,
                    hasPaidLeave: child.hasPaidLeave ? "Yes" : "No",
                    buffer: buf.item1,
                    expirationDate: buf.item2
                  }
                );
              });
            }
            else {
              $scope.leaveShow.push(
                {
                  name: "",
                  subName: child.name,
                  hasPaidLeave: child.hasPaidLeave ? "Yes" : "No",
                  buffer: 0,
                  expirationDate: ""
                }
              );
            }
          });
        }
        else {
          angular.forEach(item.buffer, function (buf) {
            $scope.leaveShow.push(
              {
                name: item.name,
                subName: "",
                hasPaidLeave: item.hasPaidLeave ? "Yes" : "No",
                buffer: buf.item1,
                expirationDate: buf.item2
              }
            );
          })
        }
      });
    }
    $scope.leave();

  }
})();