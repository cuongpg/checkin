(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('UsersPageCtrl', UsersPageCtrl);

  /** @ngInject */
  function UsersPageCtrl($scope, apiService, confirmService) {

    $scope.users = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      scriptFilter: null,
      scripts: [],
      groups: [
        
        {
          id: 1,
          name: 'USER_ACTIVE'
        },
        {
          id: -1,
          name: 'USER_LEAVED'
        },
        {
          id: 0,
          name: 'UNPAID_LEAVE'
        },
        {
          id: null,
          name: 'ALL'
        },
      ],
      status: null,
      type: null,
      toggleSelectionStatus: function (groupId) {
        // var before = _.clone(this.selectionGroup);
        // if (this.status ==  groupId) {
        //   this.status = null;
        // } else {
        //   this.status = groupId;
        // }
        this.type = groupId
        this.refresh();
      },
      
      initScripts: function () {
        var _this = this;
        apiService.get('Script', null, function (response) {
          _this.scripts = response.data;
          _this.scriptFilter = {
            id: 0,
            subScript: "All",
            all: true
          }
          _this.scripts.push(_this.scriptFilter);
          $scope.users.refresh();
        });
      },
      init: function (tableState) {
        $scope.users.tableState = tableState;
        $scope.users.initScripts();
        
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        apiService.get('users/GetUsersByType' + '?type=' + $scope.users.type + '&scriptId=' + _this.scriptFilter.id, _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      delete: function (userId) {
        var _this = this;
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function () {
          apiService.delete('users/' + userId, function (response) {
            _this.refresh();
          });
        });
      }
    };
  }

})();
