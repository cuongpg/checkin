(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.OBTConfirm', {
        url: '/OBTConfirm',
        title: 'OBT_CONFIRM',
        templateUrl: 'app/pages/system/adminOBTConfirm/OBTConfirm.html',
        controller: 'OBTConfirmCtrl',
        sidebarMeta: {
          order: 1,
        },
        // 'permission': 'Users.GetAll'
      });
  }

})();
