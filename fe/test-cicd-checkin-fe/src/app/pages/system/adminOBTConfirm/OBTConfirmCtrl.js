(function () {
  'use strict';

  angular.module('BlurAdmin.pages.attendance')
    .controller('OBTConfirmCtrl', OBTConfirmCtrl);

  /** @ngInject */
  function OBTConfirmCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, notificationService,menuService) {

    $scope.openPopup = function (page, size) {
      $uibModal.open({
        animation: true,
        templateUrl: page,
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          datas: function () {
            return $scope;
          }
        }
      });
    };

    var ModalInstanceCtrl = function ($scope,menuService) {
      $scope.no = function () {
        $uibModalStack.dismissAll();
      };

      $scope.yes = function (type) {
        var ids = $scope.$resolve.datas.leave.selected;
        if (ids.length == 0) {
          notificationService.warningTranslate('PLEASE_CHOOSE_ONE_RECORD');
        }
        else {
          var dataPost = {
            "ids": ids
          }
          if (type == 'reject') {
            apiService.create('ApprovalRecord/RejectRecordNeedConfirmByAdmin', dataPost, function (response) {
              $scope.$resolve.datas.leave.refresh()
              $scope.$resolve.datas.leave.isSelectAll= false;
            });
          }
          else {
            apiService.create('ApprovalRecord/ApproveRecordNeedConfirmByAdmin', dataPost, function (response) {
              $scope.$resolve.datas.leave.refresh()
              $scope.$resolve.datas.leave.isSelectAll= false;
            });
          }
        }
        $uibModalStack.dismissAll();
      };
    };

    $scope.leave = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.leave.tableState = tableState;
        $scope.leave.refresh();
      },
      refresh: function () {
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        _this.selected = [];
        _this.isSelectAll = false;
        _this.isLoading = true;
        apiService.get('ApprovalRecord/GetApprovalRecordsNeedConfirmByAdmin', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {

          _this.isLoading = false;
        });
        menuService.updateMenu();
      },
      selectAll: function (collection) {
        var _this = this;
        if (_this.selected.length === 0) {
          angular.forEach(collection, function (val) {
            if (val.status == 0 || val.status == 5) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          });
          _this.isSelectAll = true;
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length && !_this.isSelectAll) {
          angular.forEach(collection, function (val) {
            var found = _this.selected.indexOf(val.id);
            if (found == -1) _this.selected.push(val.id);
            val.checked = true;
          });
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length && _this.isSelectAll) {
          _this.selected = [];
          _this.isSelectAll = false;
        }
        else {
          _this.selected = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selected.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selected.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selected.push(id);
          var count = 0;
          angular.forEach(this.tableData, function (val) {
            if (val.status == 0 || val.status == 5) {
              count++;
            }
          });
          if (this.selected.length === count) {
            this.isSelectAll = true;
          }
        }
      },
      approve: function (userId) {
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_APPROVE', function () {
          apiService.create('ApprovalRecord/ApproveRecordNeedConfirmByAdmin', dataPost, function (response) {
            _this.refresh();
          });
        });
      },
      reject: function (userId) {
        var _this = this;
        var dataPost = {
          "ids": [
            userId
          ]
        }
        _this.isSelectAll = false;
        confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_REJECT', function () {
          apiService.create('ApprovalRecord/RejectRecordNeedConfirmByAdmin', dataPost, function (response) {
            _this.refresh();
          });
        });
      },
      openDetails: function (item) {
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/attendance/leave/widgets/leaveDetails.html',
          size: 'md',
          controller: 'leaveDetailsModalCtrl',
          resolve: {
            item: function () {
              return item;
            },
            isForAdmin: function(){
              return true;
            }
          }
        });

      }
    };
  }
})();