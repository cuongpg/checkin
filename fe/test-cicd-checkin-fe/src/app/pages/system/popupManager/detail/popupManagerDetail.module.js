(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.popupManagerDetail', {
        url: '/popupManager/{popupManagerId}',
        title: 'POPUP_MANAGER_DETAIL',
        templateUrl: 'app/pages/system/popupManager/detail/popupManagerDetail.html',
        controller: 'PopupManagerDetailCtrl'
      });
  }

})();