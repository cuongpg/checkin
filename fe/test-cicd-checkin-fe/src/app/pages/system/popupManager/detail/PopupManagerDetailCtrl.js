(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('PopupManagerDetailCtrl', PopupManagerDetailCtrl);


  /** @ngInject */
  function PopupManagerDetailCtrl($scope, apiService, $timeout, confirmService, notificationService, $uibModal, $state, $stateParams) {
    var vm = this
    vm.isCreateMode = _.isEmpty($stateParams.popupManagerId)
    vm.isEdit = !_.isEmpty($stateParams.popupManagerId)
    vm.isSave = _.isEmpty($stateParams.popupManagerId)
    vm.datePopup = {
      startDateOpend: false,
      endDateOpend: false
    }
    vm.dateOptions = {}
    vm.dataExport = {
      startDate: new Date(new Date().toISOString().slice(0, 10)),
      endDate: new Date(new Date().toISOString().slice(0, 10))
    }
    vm.content = ''
    vm.isAllocation = true
    vm.isReport = false
    vm.status = true
    vm.currentDate = new Date(new Date().toISOString().slice(0, 10))
    vm.list = {}
    vm.listLevelSelected = []
    vm.listLevelSelectedFullData = []
    vm.orgUnitListLv2 = []
    vm.orgUnitListLv2FullData = []
    vm.orgUnitListLv3 = []
    vm.orgUnitListLv3FullData = []
    vm.orgUnitListLv4 = []
    vm.orgUnitListLv4FullData = []
    vm.orgUnitListLv5 = []
    vm.orgUnitListLv5FullData = []
    vm.track1SelectedFullData = []
    vm.track1Selected = []
    vm.ptListDataSelected = []
    vm.ptListDataFullData = []
    vm.locationDataSelected = []
    vm.locationDataFullData = []
    vm.popupActions = []
    vm.grades = {}
    vm.track1 = {}
    vm.track2 = {}
    vm.corporations = {}
    vm.locations = {}
    vm.orgUnitLv2s = {}
    vm.orgUnitLv3s = {}
    vm.orgUnitLv4s = {}
    vm.orgUnitLv5s = {}

    vm.getPopupActionsList = function () {
      apiService.get('Employee/PopupActions', null, function (response) {
        vm.popupActions = response.data.map(function (item) {
          return {
            key: item.name,
            value: item.name,
            isShow: false
          }
        })
      }, function () {})
    }
    vm.getListLevel = function () {
      apiService.get('Employee/GetListLevel', null, function (response) {
        vm.listLevelSelected = response.data.map(function (item) {
          return {
            key: item.Code,
            value: item.Code
          }
        })
      }, function () {})
    }
    vm.getListTrack = function () {
      var url = 'Employee/GetListTrack?level=1'
      apiService.get(url, null, function (response) {
        vm.track1SelectedFullData = response.data.map(function (item) {
          return {
            key: item.name,
            value: item.name
          }
        })
        vm.track1Selected = vm.track1SelectedFullData
      }, function () {})
    }
    vm.getOrgUnit = function (level, dataInput, dataFull) {
      apiService.get('EstimateOwner/GetListOrgUnit?level=' + level, null, function (response) {
        if (response.data != null) {
          vm[dataFull] = response.data.map(function (item) {
            return {
              key: item.code,
              value: item.code
            }
          })
          vm[dataInput] = vm[dataFull]
        }
      }, function () {})
    }
    vm.getCorporations = function () {
      apiService.get('EstimateOwner/GetCorporations', null, function (response) {
        vm.ptListDataSelected = response.data.corporations.map(function (item) {
          return {
            key: item.shortCode,
            value: item.shortCode
          }
        });
      }, function () {});
    }
    vm.getLocation = function () {
      apiService.get('TimeKeeping/GetAllLocation', null, function (response) {
        vm.locationDataSelected = response.data.locations.map(function (item) {
          return {
            key: item.address,
            value: item.address
          }
        });
      }, function () {});
    }
    vm.comvertStringToArray = function (input) {
      var data = {}
      if (input && input !== 'null') {
        data = JSON.parse(input).map(function (item) {
          return {
            key: item,
            value: item
          }
        })
      }
      return data;
    }
    vm.init = function () {
      vm.getListLevel()
      vm.getListTrack()
      vm.getPopupActionsList()
      vm.getOrgUnit(2, 'orgUnitListLv2', 'orgUnitListLv2FullData')
      vm.getOrgUnit(3, 'orgUnitListLv3', 'orgUnitListLv3FullData')
      vm.getOrgUnit(4, 'orgUnitListLv4', 'orgUnitListLv4FullData')
      vm.getOrgUnit(5, 'orgUnitListLv5', 'orgUnitListLv5FullData')
      vm.getCorporations()
      vm.getLocation()
      if ($stateParams.popupManagerId) {
        apiService.get('Employee/PopupConfig/' + $stateParams.popupManagerId, null, function (response) {
          if (response.data) {
            vm.grades = vm.comvertStringToArray(response.data.grades)
            vm.track1 = vm.comvertStringToArray(response.data.track1s)
            vm.track2 = vm.comvertStringToArray(response.data.track2s)
            vm.corporations = vm.comvertStringToArray(response.data.corporations)
            vm.locations = vm.comvertStringToArray(response.data.locations)
            vm.orgUnitLv2s = vm.comvertStringToArray(response.data.orgUnitLv2s)
            vm.orgUnitLv3s = vm.comvertStringToArray(response.data.orgUnitLv3s)
            vm.orgUnitLv4s = vm.comvertStringToArray(response.data.orgUnitLv4s)
            vm.orgUnitLv5s = vm.comvertStringToArray(response.data.orgUnitLv5s)
            vm.content = response.data.message
            vm.status = response.data.status === 0 ? false : true
            vm.dataExport.startDate = new Date(response.data.effectiveDate)
            vm.dataExport.endDate = new Date(response.data.expirationDate)
            var actionsData = vm.comvertStringToArray(response.data.actions)
            angular.forEach(vm.popupActions, function (item, index) {
              angular.forEach(actionsData, function (item2) {
                if (item2.key === item.key) {
                  vm.popupActions[index].isShow = true;
                }
              })
            })
          }
        }, function () {})
      }
      vm.list = {
        grade: {
          key: 'grade',
          title: 'GRADEV2',
          value: 'grades',
          data: 'listLevelSelected'
        },
        trach1: {
          key: 'trach1',
          title: 'TRACK1',
          value: 'track1',
          data: 'track1Selected'
        },
        corporration: {
          key: 'corporration',
          title: 'PERSONAL_AREA',
          value: 'corporations',
          data: 'ptListDataSelected'
        },
        location: {
          key: 'location',
          title: 'LOCATION',
          value: 'locations',
          data: 'locationDataSelected',
          isDisable: true
        },
        orgUnitLv2: {
          key: 'orgUnitLv2',
          title: 'ORG_UNIT_LV2_V2',
          value: 'orgUnitLv2s',
          data: 'orgUnitListLv2',
          refreshChild: function () {}
        },
        orgUnitLv3: {
          key: 'orgUnitLv3',
          title: 'ORG_UNIT_LV3_V2',
          value: 'orgUnitLv3s',
          data: 'orgUnitListLv3',
          refreshChild: function () {}
        },
        orgUnitLv4: {
          key: 'orgUnitLv4',
          title: 'ORG_UNIT_LV4_V2',
          value: 'orgUnitLv4s',
          data: 'orgUnitListLv4',
          refreshChild: function () {},
        },
        orgUnitLv5: {
          key: 'orgUnitLv5',
          title: 'ORG_UNIT_LV5_V2',
          value: 'orgUnitLv5s',
          data: 'orgUnitListLv5',
          refreshChild: function () {}
        }
      }
    }
    vm.init()
    vm.onDateClick = function (item) {
      if (item == 1)
        vm.datePopup.startDateOpend = true
      else if (item == 2)
        vm.datePopup.endDateOpend = true
    }
    vm.back = function () {
      $state.go('system.popupManager');
    }
    vm.reload = function () {
      $state.reload();
    }
    vm.showEdit = function () {
      vm.isEdit = false;
      $timeout(function () {
        vm.isSave = true;
      }, 200);
    }
    vm.formatDatePush = function (value) {
      var date = new Date(value)
      var month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
      var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
      if (month < 10) {
          return date.getFullYear() + '-' + month + '-' + day;
      } else {
          return date.getFullYear() + '-' + month + '-' + day;
      }
  }
    vm.save = function () {
      var actions = []
      angular.forEach(vm.popupActions, function (item) {
        if (item.isShow) {
          actions.push(item.key)
        }
      })
      if($stateParams.popupManagerId) {
        console.log(vm.dataExport.startDate)
        var data = {
          "id": $stateParams.popupManagerId,
          "grades": vm.parseArray(vm[vm.list.grade.value]),
          "track1s": vm.parseArray(vm[vm.list.trach1.value]),
          "track2s": null,
          "corporations": vm.parseArray(vm[vm.list.corporration.value]),
          "locations": vm.parseArray(vm[vm.list.location.value]),
          "orgUnitLv2s": vm.parseArray(vm[vm.list.orgUnitLv2.value]),
          "orgUnitLv3s": vm.parseArray(vm[vm.list.orgUnitLv3.value]),
          "orgUnitLv4s": vm.parseArray(vm[vm.list.orgUnitLv4.value]),
          "orgUnitLv5s": vm.parseArray(vm[vm.list.orgUnitLv5.value]),
          "actions": actions ? actions : '',
          "message": vm.content,
          "status": vm.status ? 1 : 0,
          "effectiveDate": vm.formatDatePush(vm.dataExport.startDate),
          "expirationDate": vm.formatDatePush(vm.dataExport.endDate),
        }
        apiService.update('Employee/PopupConfig', JSON.stringify(data), function (response) {
          $state.reload();
        });
      } else {
        var data = {
          "grades": vm.parseArray(vm[vm.list.grade.value]),
          "track1s": vm.parseArray(vm[vm.list.trach1.value]),
          "track2s": null,
          "corporations": vm.parseArray(vm[vm.list.corporration.value]),
          "locations": vm.parseArray(vm[vm.list.location.value]),
          "orgUnitLv2s": vm.parseArray(vm[vm.list.orgUnitLv2.value]),
          "orgUnitLv3s": vm.parseArray(vm[vm.list.orgUnitLv3.value]),
          "orgUnitLv4s": vm.parseArray(vm[vm.list.orgUnitLv4.value]),
          "orgUnitLv5s": vm.parseArray(vm[vm.list.orgUnitLv5.value]),
          "actions": actions ? actions : '',
          "message": vm.content,
          "status": vm.status ? 1 : 0,
          "effectiveDate": vm.formatDatePush(vm.dataExport.startDate),
          "expirationDate": vm.formatDatePush(vm.dataExport.endDate),
        }
        apiService.create('Employee/PopupConfig', JSON.stringify(data), function (response) {
          $state.reload();
        });
      }
    }
    vm.parseArray = function (data) {
      if (!data) {
        return null
      } else {
        var values = []
        angular.forEach(data, function (item) {
          values.push(item.value)
        })
        return values
        // return JSON.stringify(values).replace('[', '').replace(']', '').replace(/"/g, '')
      }
    }
  }
})();