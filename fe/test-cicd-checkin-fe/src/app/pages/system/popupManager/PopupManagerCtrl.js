(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('PopupManagerCtrl', PopupManagerCtrl);


  /** @ngInject */
  function PopupManagerCtrl($scope, apiService, $uibModal, $stateParams, confirmService) {
    $scope.popupManagers = {
      isCreateMode: _.isEmpty($stateParams.popupId),
      isEdit: !_.isEmpty($stateParams.popupId),
      isSave: _.isEmpty($stateParams.popupId),
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      init: function (tableState) {
        $scope.popupManagers.tableState = tableState;
        $scope.popupManagers.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        apiService.get('Employee/PopupConfigs', _this.tableState, function (response) {
          _this.tableData = response.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      parse: function (value) {
        return value.replace('[', '').replace(']', '').replace(/"/g, '');
      },
      parseTitle: function (value) {
        return value.replace('[', '').replace(']', '').replace(/"/g, '').replace(/,/g, '\n');
      }
    };
  }

})();
