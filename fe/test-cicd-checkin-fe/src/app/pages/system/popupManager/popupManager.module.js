(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.popupManager', {
        url: '/popupManager',
        title: 'POPUP_MANAGER',
        templateUrl: 'app/pages/system/popupManager/popupManager.html',
        controller: 'PopupManagerCtrl',
        sidebarMeta: {
          order: 0,
        }
      });
  }

})();
