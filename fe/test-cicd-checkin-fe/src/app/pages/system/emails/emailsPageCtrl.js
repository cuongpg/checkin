(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .controller('emailsPageCtrl', emailsPageCtrl);
  
    /** @ngInject */
    function emailsPageCtrl($scope, apiService, $state, confirmService, $uibModal) {
  
      $scope.emails = {
        isLoading: true,
        tableState: null,
        tablePageSize: 10,
        tableData: [],
        default: {
          search: {
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            // status:''                 
          }
        },
        init: function (tableState) {
          $scope.emails.tableState = tableState;
          $scope.emails.refresh();
        },
        refresh: function () {
          var _this = this;
          _this.isLoading = true;
          apiService.get('emails/get-all', _this.tableState, function (response) {
            _this.tableData = response.data.data;
            _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
            _this.tableState.pagination.totalItemCount = response.data.totalRecords;
            _this.isLoading = false;
          }, function () {
            _this.isLoading = false;
          });
        },
        delete: function (Id) {
          var _this = this;
          confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function () {
            apiService.create('emails/delete/' + Id, null, function (response) {
              $state.reload();
            });
          });
        },
        openDetails: function (itemId) {
          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/system/emails/detail/email.html',
            size: 'lg',
            windowClass: 'app-modal-window',
            controller: 'emailPageCtrl',
            resolve: {
              item: function () {
                return itemId;
              }
            }
          });
  
        }
      };
    }
  
  })();