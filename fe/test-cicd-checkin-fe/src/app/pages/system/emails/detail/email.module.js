(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('system.email', {
          url: '/email/{id}',
          title: 'CREATE_EMAIL',
          templateUrl: 'app/pages/system/emails/detail/email.html',
          controller: 'emailPageCtrl',
        });
    }
  
  })();