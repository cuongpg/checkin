(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.system')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('system.emails', {
          url: '/email',
          title: 'LIST_EMAIL',
          templateUrl: 'app/pages/system/emails/emails.html',
          controller: 'emailsPageCtrl',
          sidebarMeta: {
            order: 8,
          },
          permission: ['Emails.GetAllTemplate']
        });
    }
  
  })();