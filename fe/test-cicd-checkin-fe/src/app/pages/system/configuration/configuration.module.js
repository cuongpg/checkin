(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.configuration', {
        url: '/configuration',
        title: 'CONFIGURATION',
        templateUrl: 'app/pages/system/configuration/configuration.html',
        controller: 'ConfigurationCtrl',
        sidebarMeta: {
          order: 2,
        },
        // 'permission': 'Groups.GetAll'
      });
  }

})();