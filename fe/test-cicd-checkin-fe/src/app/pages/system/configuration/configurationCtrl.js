(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('ConfigurationCtrl', ConfigurationCtrl);

  /** @ngInject */
  function ConfigurationCtrl($scope, apiService, notificationService, confirmService, Upload, $filter, loaderService, $translate, $timeout, config, toastr, $uibModal) {
  
    $scope.leaveQuotasConfig = {
      employeeSelected: {},
      employees: [],
      reasonType:  {},
      reasonTypes: [],
      number: 0,
      effectiveDate: new Date(),
      expirationDate: new Date(),
      effectiveDatePopup: false,
      expirationDatePopup: false,
      dateOptions:  {
        formatYear: 'yy',
        startingDay: 1
      },

      onOpenEffectiveDate: function () {
        var _this = this;
        _this.effectiveDatePopup = true;
      },

      onOpenExpirationDate: function () {
        var _this = this;
        _this.expirationDatePopup = true;
      },

      dateChange: function () {
      },

      selectEmployee:  function () {
        var _this = this;
        _this.refreshReason( function() {
          _this.reasonType =  _this.reasonTypes[0];
        });
      },

      refreshReason: function(setReason)
      {
        var _this = this;
        var getReasonTypeUri=  'ReasonType/HasQuotasReasonsByUser?employeeCode='+ _this.employeeSelected.id;
        apiService.get(getReasonTypeUri, null, function (response) {
          _this.reasonTypes = [];
          response.data.forEach(function (reason) {
            _this.reasonTypes.push({
              value: reason.id,
              label: reason.name,
              buffer: reason.buffer
            })
          });
          if(setReason) setReason();
        }, function() {
        });
      },

      refreshEmployee: function (input) {
        if (input) {
          apiService.get('users/GetAllEmployee' + '?keyWord=' + input + '&perPage=10', null, function (response) {
            if (response.data.data != null) {
              $scope.leaveQuotasConfig.employees = response.data.data;
            }
          });
        }
      },

      submit:  function () {
        var _this = this;
        var submitUri=  'Configuration/AddLeaveQuotas';
        var body = {
          userId: _this.employeeSelected.id,
          reasonTypeId: _this.reasonType.value,
          number: _this.number,
          effectiveDate: $filter('date')(_this.effectiveDate, config.yyyyMMdd),
          expirationDate: $filter('date')(_this.expirationDate, config.yyyyMMdd)
        }
        apiService.create(submitUri, body, function (response) {
          _this.refreshReason(function(){
            var id = _this.reasonType.value;
            _this.reasonType =  $filter('filter')(_this.reasonTypes, {'value': id})[0]; 
          });
        
        }, function() {
        });

      },
      showResultPopup: function(response){
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/system/configuration/AbsenderQuotasPopup/AbsenderQuotasPopup.html',
          controller: 'AbsenderQuotasPopupCtrl',
          size: 'lg',
          resolve: {
            response: function () {
              return response;
            },
          }
        });
      },

      import: function (file, errFiles) {
        var _this = this;
        if ((errFiles && errFiles[0]) !== undefined) {
            toastr.error(errFiles[0].$error + ' ' + errFiles[0].$errorParam);
            return false;
        }
        if (file) {
          var uploadUrl = "Configuration/ImportAbsenderQuotas";
          file.upload = Upload.upload({
              url: config.apiUrl + uploadUrl,
              data: {
                  file: file
              }
          });
  
          file.upload.then(function (response) {
            _this.showResultPopup(response);
          }, function (response) {
            _this.showResultPopup(response);
          });
        }
      }


    };

    $scope.otQuotasConfig = {
      employeeSelected: {},
      employees: [],
      groupEventType:  {},
      groupEventTypes: [],
      number: 0,
      curQuotas: 0,
      month: new Date(),
      monthPopup: false,
      monthOptions:  {
        formatYear: 'yyyy',
        startingDay: 1,
        minMode: 'month'
      },

     
      onOpenMonth: function () {
        var _this = this;
        _this.monthPopup = true;
      },

      changeMonth: function () {
        var _this = this;
        _this.curQuotas =  0;
        if(_this.employeeSelected.id && _this.groupEventType.value)
        {
          var month = this.month.getMonth() + 1;
          var year = this.month.getFullYear();
          var getOTQuotasUri=  'OverTime/GetOTQuotas?employeeCode='+ _this.employeeSelected.id + '&groupEventId=' + _this.groupEventType.value + '&month=' + month + '&year=' + year;
          apiService.get(getOTQuotasUri, null, function (response) {
            if(response.data){
              _this.curQuotas = response.data.quotas;
            }
  
          }, function() {
          });
        }
      },

      selectEmployee:  function () {
        var _this = this;
        _this.refreshGroupEvent( function() {
          _this.groupEventType =  _this.groupEventTypes[0];
          _this.selectGroupEvent();
        });
      },

      refreshGroupEvent: function(setGroupEvent)
      {
        var _this = this;
        var getGroupEventTypeUri=  'Event/GetOTQuotasGroupEvents?employeeCode='+ _this.employeeSelected.id;
        apiService.get(getGroupEventTypeUri, null, function (response) {
          _this.groupEventTypes = [];
          response.data.forEach(function (group) {
            _this.groupEventTypes.push({
              value: group.id,
              label: group.name
            })
          });
          if(setGroupEvent) setGroupEvent();
        }, function() {
        });
      },

      selectGroupEvent: function()
      {
        var _this = this;
        _this.changeMonth();
      },

      refreshEmployee: function (input) {
        if (input) {
          apiService.get('users/GetAllEmployee' + '?keyWord=' + input + '&perPage=10', null, function (response) {
            if (response.data.data != null) {
              $scope.otQuotasConfig.employees = response.data.data;
            }
          });
        }
      },

      submit:  function () {
        var _this = this;
        var submitUri=  'Configuration/AddOTQuotas';
        var body = {
          userId: _this.employeeSelected.id,
          eventGroupId: _this.groupEventType.value,
          quotas: _this.number,
          startTime: $filter('date')(new Date(_this.month.getFullYear(), _this.month.getMonth(), 1), config.yyyyMMdd),
          endTime: $filter('date')(new Date(_this.month.getFullYear(), _this.month.getMonth() + 1, 0), config.yyyyMMdd),
        }
        apiService.create(submitUri, body, function (response) {
          _this.curQuotas = response.data.quotas;
        }, function() {
        });
      },

      showResultPopup: function(response){
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/system/configuration/OvertimeQuotasPopup/OvertimeQuotasPopup.html',
          controller: 'OvertimeQuotasPopupCtrl',
          size: 'lg',
          resolve: {
            response: function () {
              return response;
            },
          }
        });
      },

      import: function (file, errFiles) {
        var _this = this;
        if ((errFiles && errFiles[0]) !== undefined) {
            toastr.error(errFiles[0].$error + ' ' + errFiles[0].$errorParam);
            return false;
        }
        if (file) {
          var uploadUrl = "Configuration/ImportOTQuotas";
          file.upload = Upload.upload({
              url: config.apiUrl + uploadUrl,
              data: {
                  file: file
              }
          });
  
          file.upload.then(function (response) {
            _this.showResultPopup(response);
          }, function (response) {
            _this.showResultPopup(response);
          });
        }
      }


    };
    

    $scope.init= function () {
     
    };

  }
})();
