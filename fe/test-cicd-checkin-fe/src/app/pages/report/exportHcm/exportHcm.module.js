(function () {
  'use strict';

  angular.module('BlurAdmin.pages.report')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('report.exportHcm', {
        url: '/exportHcm',
        title: 'EXPORT_HCM',
        templateUrl: 'app/pages/report/exportHcm/exportHcm.html',
        controller: 'ExportHcmCtrl',
        sidebarMeta: {
          order: 2,
        },
        // 'permission': 'Groups.GetAll'
      });
  }

})();