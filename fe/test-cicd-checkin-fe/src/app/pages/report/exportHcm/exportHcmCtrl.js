(function () {
  'use strict';

  angular.module('BlurAdmin.pages.report')
    .controller('ExportHcmCtrl', ExportHcmCtrl);

  /** @ngInject */
  function ExportHcmCtrl($scope, apiService, notificationService, confirmService, Upload, $filter, loaderService, $translate, $timeout, config) {
    $scope.form = {
      fromDate: new Date(),
      toDate: new Date(),
      type: {
        key: 'Hcm/ExportPlannedWorkingTime',
        value : "Planned working time",
        fileName: "IMP_IT0007",
        infoType: "0007"
      },
      corporationType:  {},
      corporationTypes: [
          // {
          //   value: 'VI_CORP',
          //   label: 'VI_CORP'
          // },
          // {
          //   value: 'TH_CORP',
          //   label: 'TH_CORP'
          // }
      ],
      data: [],
      hcmType: [
        {
          key: 'Hcm/ExportPlannedWorkingTime',
          value : "Planned working time",
          fileName: "IMP_IT0007",
          infoType: "0007"
        },
        {
          key: 'Hcm/ExportAbsences',
          value : "Absence",
          fileName: "IMP_IT2001",
          infoType: "2001"
        },
        {
          key: 'Hcm/ExportAbsencesQuatas',
          value : "Absence quotas",
          fileName: "IMP_IT2006",
          infoType: "2006"
        },
        {
          key: 'Hcm/ExportOvertime',
          value : "Employee remuneration info",
          fileName: "IMP_IT2010",
          infoType: "2010"
        },
        {
          key: 'Hcm/ExportTimeKeepings',
          value : "Timekeepings",
          fileName: "IMP_IT0267",
          infoType: "0267"
        },
        {
          key: 'Hcm/ExportShiftEats',
          value : "Shift Eats",
          fileName: "IMP_IT0015",
          infoType: "0015"
        }
      ]
    };
    $scope.init= function () {
      var _this = this;
      _this.isLoading = true;
      var getCorpTypeUri=  'users/GetAllCorporationTypes';
      apiService.get(getCorpTypeUri, null, function (response) {
        _this.form.corporationTypes = [];
        response.data.forEach(function (corp) {
          _this.form.corporationTypes.push({
            value: corp.code,
            label: corp.code
          })
        });
        var length = _this.form.corporationTypes.length;
        _this.form.corporationType =  _this.form.corporationTypes[length-1];
        _this.isLoading = false;
      }, function() {
        _this.isLoading = false;
      });
      
    };
    $scope.open = function (isFrom) {
      if(isFrom)
      {
        $scope.popup.openedFrom = true;
      }
      else
      {
        $scope.popup.openedTo = true;
      }
    };

    $scope.popup = {
      openedFrom: false,
      openedTo: false
    };

    $scope.export = function () {
      var _this = this;
      var now = new Date();
      // now.setHours(now.getHours() + 7)
      var fromDate = $filter('date')($scope.form.fromDate, config.formatDate);
      var toDate = $filter('date')($scope.form.toDate, config.formatDate);
      var uri=  _this.form.type.key + '?corporationType='+_this.form.corporationType.value +'&startDate=' + fromDate + '&endDate=' + toDate;
      apiService.downloadCSV('GET', uri, null,  _this.form.type.fileName+'_'+now.getDate()+''+(now.getMonth() + 1)+''+now.getFullYear()+'_'+now.getHours()+''+now.getMinutes()+''+now.getMilliseconds(), true, function (response) {

      });
    };
    $scope.uploadToHCM = function () {
      var _this = this;
      var now = new Date();
      // now.setHours(now.getHours() + 7)
      var fromDate = $filter('date')($scope.form.fromDate, config.formatDate);
      var toDate = $filter('date')($scope.form.toDate, config.formatDate);
      var uri=  'Hcm/UploadToHCM' + '?infoType=' + _this.form.type.infoType + '&corporationType='+_this.form.corporationType.value +'&startDate=' + fromDate + '&endDate=' + toDate;
     
      confirmService.showTranslate('CONFIRM_UPLOAD_TO_HCM', function(){
        //alert(uri);
        apiService.get(uri, null, function (response) {
        }, function() {    
        });
      });
      // apiService.downloadCSV('GET', _this.form.type.key + '?startDate=' + fromDate + '&endDate=' + toDate, null,  _this.form.type.fileName+'_'+now.getDate()+''+(now.getMonth() + 1)+''+now.getFullYear()+'_'+now.getHours()+''+now.getMinutes()+''+now.getMilliseconds(), true, function (response) {
      // });
    };
    $scope.uploadFileToHCM = function() {
      var f = document.getElementById('file').files[0],
      r = new FileReader();
      r.onloadend = function(e) {
       debugger;
        var data = e.target.result;
        var uploadUrl = "Configuration/Import";
        var fd = new FormData();
        fd.append('file', data);
        
        apiService.create(uploadUrl, fd,  function() {
        }, 
        function() {
        });
      }
      r.readAsBinaryString(f);
    }

    $scope.import = function (file, errFiles) {
      debugger;
      if ((errFiles && errFiles[0]) !== undefined) {
          toastr.error(errFiles[0].$error + ' ' + errFiles[0].$errorParam);
          return false;
      }
      if (file) {
        var uploadUrl = "Configuration/Import";
        file.upload = Upload.upload({
            url: config.apiUrl + uploadUrl,
            data: {
                file: file
            }
        });

        file.upload.then(function (response) {

        }, function (response) {

        });
      }
    };
  }
})();
