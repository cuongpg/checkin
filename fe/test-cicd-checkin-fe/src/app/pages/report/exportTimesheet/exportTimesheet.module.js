(function () {
  'use strict';

  angular.module('BlurAdmin.pages.report')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('report.exportTimesheet', {
        url: '/exportTimesheet',
        title: 'EXPORT',
        templateUrl: 'app/pages/report/exportTimesheet/exportTimesheet.html',
        controller: 'ExportTimesheetCtrl',
        sidebarMeta: {
          order: 1,
        },
        // 'permission': 'Groups.GetAll'
      });
  }

})();