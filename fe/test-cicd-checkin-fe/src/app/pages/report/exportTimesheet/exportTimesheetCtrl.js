(function () {
  'use strict';

  angular.module('BlurAdmin.pages.report')
    .controller('ExportTimesheetCtrl', ExportTimesheetCtrl);

  /** @ngInject */
  function ExportTimesheetCtrl($scope, apiService, notificationService, confirmService, $filter, loaderService, $translate, $timeout, config) {
    $scope.dataExport = {
      date: null,
      startDate: null,
      endDate: null
    };

    $scope.form = {
      type: {
        key: 'timesheet',
        value: "Attendance Record"
      },
      scriptType: {
        key: 'D',
        value: "CTV"
      },
      exporttType: [
        {
          key: 'timesheet',
          value: "Attendance Record"
        },
        {
          key: 'overtime',
          value: "Overtime"
        },
        {
          key: 'pendingRecord',
          value: "Pending Record"
        }
      ],
      script: [],
      groupEvent: [],
      groupEventId: 1
    }

    $scope.monthOptions = {
      formatYear: 'yyyy',
      startingDay: 1,
      minMode: 'month'
    }
    $scope.dateOptions = {

    }
    $scope.changeMonth = function () {
      $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date($scope.dataExport.date.getFullYear(), $scope.dataExport.date.getMonth() + 1, 0),
        minDate: new Date($scope.dataExport.date.getFullYear(), $scope.dataExport.date.getMonth(), 1),
        startingDay: 1
      }
      $scope.dataExport.startDate = $scope.dateOptions.minDate;
      $scope.dataExport.endDate = $scope.dateOptions.maxDate;
    }
    $scope.getOvertime = function () {
      var _this = this;
      _this.isLoading = true;
      apiService.get('Event/GetGroupEvents', null, function (response) {
        $scope.form.groupEvent = response.data;
        apiService.get('Script', null, function (responseScript) {
          $scope.form.script = responseScript.data;
          $scope.form.scriptType = responseScript.data[0];
          _this.isLoading = false;
        },
          function () {
            _this.isLoading = false;
          });
      },
        function () {
          _this.isLoading = false;
        });

    }

    $scope.downloadFile = function (script, type, eventsId) {
      if (!$scope.dataExport.date || $scope.dataExport.date == undefined) {
        return notificationService.errorTranslate("WRONG_DATE_FORMAT");
      }
      if ($scope.dataExport.startDate > $scope.dataExport.endDate) {
        return notificationService.errorTranslate("EXPORT_TIME_INVALID")
      }
      var date = $scope.dataExport.date
      var startDate = $filter('date')($scope.dataExport.startDate, config.yyyyMMdd)
      var endDate = $filter('date')($scope.dataExport.endDate, config.yyyyMMdd)
      if (type == 'timesheet') {
        apiService.download('GET', 'TimeKeeping/ExportTimesheet/' + script + '?startTime=' + startDate + '&endTime=' + endDate, null, 'AttendanceRecord-' + (date.getMonth() + 1) + '-' + date.getFullYear(), function (response) {
        });
      }
      else if (type == 'overtime') {
        apiService.download('GET', 'Overtime/ExportOvertime/' + script + '?startTime=' + startDate + '&endTime=' + endDate, null, 'OVERTIME-' + (date.getMonth() + 1) + '-' + date.getFullYear(), function (response) {
        });
        // var eventsCode = '';
        // $scope.form.groupEvent.forEach(function (el) {
        //   if (el.id == eventsId) {
        //     eventsCode = el.code
        //   }
        // });
        // if (eventsCode) {
        //   apiService.download('GET', 'Overtime/ExportOvertime/' + (date.getMonth() + 1) + '/' + date.getFullYear(), null, 'TIMESHEET-' + (date.getMonth() + 1) + '-' + date.getFullYear(), function (response) {
        //   });
        // }
        // else {

        // }
      }
      else {
        // apiService.download('GET', 'TimeKeeping/ExportPendingRecord/' + script + '?startTime=' + startDate + '&endTime=' + endDate, null, 'PENDING-' + (date.getMonth() + 1) + '-' + date.getFullYear(), function (response) {
        apiService.download('GET', 'TimeKeeping/ExportPendingRecord/' + script + '?startTime=' + startDate + '&endTime=' + endDate, null, 'PENDING-' + (date.getMonth() + 1) + '-' + date.getFullYear(), function (response) {
        });
      }
    };

    $scope.onDateClick = function (item) {
      if (item == 1)
        $scope.datePopup.monthOpend = true;
      else if (item == 2)
        $scope.datePopup.startDateOpend = true;
      else
        $scope.datePopup.endDateOpend = true;
    };
    $scope.datePopup = {
      monthOpend: false,
      startDateOpend: false,
      endDateOpend: false
    };
  }

})();
