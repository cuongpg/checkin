/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.report', [])
    .config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
      .state('report', {
        url: '/report',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'REPORT',
        sidebarMeta: {
          icon: 'fa fa fa-bar-chart',
          order: 5,
        },
        permission: ['TimeKeeping.ExportTimesheet']
      });
  }
})();
