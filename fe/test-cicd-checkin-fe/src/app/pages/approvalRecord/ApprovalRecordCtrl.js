(function () {
  'use strict';

  angular.module('BlurAdmin.pages.approvalRecord')
    .controller('ApprovalRecordCtrl', ApprovalRecordCtrl);

  /** @ngInject */
  function ApprovalRecordCtrl($scope, apiService, confirmService, $filter, notificationService, $window, $translate, $timeout, config, menuService) {
    $scope.employee = null;
    $scope.isForAdmin = false;
    if ($scope.$resolve.employee) {
      $scope.employee = $scope.$resolve.employee;
      $scope.isForAdmin = true;
    }
    $scope.ismeridian = true;
    $scope.toggleMode = function () {
      $scope.ismeridian = !$scope.ismeridian;
    };
    $scope.minDate = null;

    $scope.getMinDate = function () {
      apiService.get('Script/GetMinDate', null, function (response) {
        $scope.minDate = new Date(response.data);
        if ($scope.isForAdmin) {
          $scope.minDate.setMonth($scope.minDate.getMonth() - 1);
        }
        $scope.leaveRequest.refresh();
      },
      function () {
      });
    }


    $scope.external = new Date();
    $scope.externalSettings = {
      showOnTap: false,
      showOnFocus: false
    };

    $scope.screen = {
      screenShow: 'leaveRequest',
      chooseLeaveRequest: function () {
        $scope.screen.screenShow = 'leaveRequest'
        $scope.leaveRequest.refresh();
      },
      chooseManuallyRecord: function () {
        $scope.screen.screenShow = 'manuallyRecord'
        $scope.manuallyRecord.refresh();
      },
      chooseOvertime: function () {
        $scope.screen.screenShow = 'overtime'
        $scope.overtime.refresh();
      },
    }

    $scope.manager = {
      managerName: "",
      managerEmail: "",
      userName: "",
      department: "",
    }

    $scope.loadManagerInfo = function () {
      var _this = this;
      _this.isLoading = true;
      var tkManagerUrl = null;
      if ($scope.isForAdmin) tkManagerUrl = 'Employee/AdminGetTimeKeepingManagerInfo?email=' + $scope.employee.email;
      else tkManagerUrl = 'Employee/TimeKeepingManagerInfo';
      apiService.get(tkManagerUrl, null, function (response) {
        // $scope.getCurrentEstimateOwner();
        $scope.manager.managerName = response.data.managerName;
        $scope.manager.managerEmail = response.data.managerEmail;
        $scope.manager.userName = response.data.userName;
        $scope.manager.department = response.data.department;
        _this.isLoading = false;
      },
        function () {
          _this.isLoading = false;
        });
    }

    //-----------Leave Request Controller------------
    $scope.leaveRequest = {
      isOneDay: true,
      selectedItem: {},
      leaveReasons: [],
      dayWithDrawn: {},
      dataLeaveRequest: {},
      datePopup: null,
      fromDatePopup: null,
      toDatePopup: null,
      dateOptions: {},

      refresh: function () {
        $scope.leaveRequest.dataLeaveRequest = {
          reason: "",
          morning: true,
          afternoon: true,
          leaveDate: new Date(),
          fromDate: new Date(),
          toDate: new Date(),
          tempdate: new Date(),
          startTime: new Date(),
          endTime: new Date(),
          holiday: 0,
          weekend: 0,
          leave: null
        };
        $scope.leaveRequest.leaveReasons = [];
        // if ($scope.leaveRequest.dataLeaveRequest.tempdate.getDate() < 4) {
        //   $scope.leaveRequest.dataLeaveRequest.tempdate.setMonth($scope.leaveRequest.dataLeaveRequest.tempdate.getMonth() - 1);
        // }
        // $scope.leaveRequest.dataLeaveRequest.tempdate.setDate(1);

        $scope.leaveRequest.dateOptions = {
          formatYear: 'yy',
          maxDate: new Date((new Date()).getFullYear() + 1, 12, 31),
          minDate: $scope.minDate,
          startingDay: 1
        };
        $scope.leaveRequest.dayWithDrawn.selected = null;
        $scope.loadManagerInfo();
        $scope.leaveRequest.getLeaveReason($scope.leaveRequest.dataLeaveRequest.leaveDate, $scope.leaveRequest.dataLeaveRequest.leaveDate);
        menuService.updateMenu();
        $scope.leaveRequest.datePopup = false;
        $scope.leaveRequest.fromDatePopup = false;
        $scope.leaveRequest.toDatePopup = false;
        $scope.leaveRequest.timeChange();
      },

      getLeaveReason: function (startTime, endTime) {
        var data = {
          startDate: $filter('date')(startTime, config.yyyyMMdd),
          endDate: $filter('date')(endTime, config.yyyyMMdd)
        }
        var LeaveReasonsUrl = null;
        if ($scope.isForAdmin) LeaveReasonsUrl = 'ReasonType/LeaveUserReasons?email=' + $scope.employee.email;
        else LeaveReasonsUrl = 'ReasonType/LeaveReasons';
        apiService.get(LeaveReasonsUrl, data, function (response) {
          if (response.data != null) {
            $scope.leaveRequest.leaveReasons = response.data;
            angular.forEach($scope.leaveRequest.leaveReasons, function (leaveReasonsItem) {
              if (!$scope.leaveRequest.dayWithDrawn.selected) {
                $scope.leaveRequest.dayWithDrawn.selected = leaveReasonsItem.id;
              }
              angular.forEach(leaveReasonsItem.buffer, function (bufItem) {
                if (leaveReasonsItem.id == 2 && bufItem.item1 != 0) {
                  $scope.leaveRequest.dayWithDrawn.selected = leaveReasonsItem.id;
                }
                bufItem.item3 = $filter('date')(bufItem.item2, config.ddMMyyyy);
              });
              if (leaveReasonsItem.childs.length != 0)
                angular.forEach(leaveReasonsItem.childs, function (child) {
                  angular.forEach(child.buffer, function (childbuf) {
                    childbuf.item3 = $filter('date')(childbuf.item2, config.ddMMyyyy);
                  })
                })
            });
          }
        });
      },

      timeChange: function () {
        var startTime = null;
        var endTime = null;
        var hasChilds = false;

        if ($scope.leaveRequest.isOneDay) {
          if ($scope.leaveRequest.dataLeaveRequest.leaveDate == undefined) {
            return notificationService.errorTranslate("WRONG_DATE_FORMAT");
          }
          if (!$scope.leaveRequest.dataLeaveRequest.morning && !$scope.leaveRequest.dataLeaveRequest.afternoon) {
            return notificationService.errorTranslate("LEAVEREQUEST_MUST_CHOOSE_MORNING_OR_AFTERNOON");
          }
          startTime = new Date($scope.leaveRequest.dataLeaveRequest.leaveDate);
          endTime = new Date($scope.leaveRequest.dataLeaveRequest.leaveDate);
          //afternoon = true
          if (!$scope.leaveRequest.dataLeaveRequest.morning) {
            startTime.setHours(12, 0, 0);
            endTime.setHours(23, 59, 0);
          }
          //morning = true
          else if (!$scope.leaveRequest.dataLeaveRequest.afternoon) {
            startTime.setHours(0, 0, 0);
            endTime.setHours(12, 0, 0);
          }
          else {
            startTime.setHours(0, 0, 0);
            endTime.setHours(23, 59, 0);
          }

        } else {
          if ($scope.leaveRequest.dataLeaveRequest.fromDate == undefined || $scope.leaveRequest.dataLeaveRequest.toDate == undefined) {
            return notificationService.errorTranslate("WRONG_DATE_FORMAT");
          }
          if ($scope.leaveRequest.dataLeaveRequest.fromDate > $scope.leaveRequest.dataLeaveRequest.toDate) {
            return notificationService.errorTranslate("LEAVEREQUEST_ERROR_STATTIME_GREATER_THAN_ENDTIME");
          }

          startTime = $scope.leaveRequest.dataLeaveRequest.fromDate;
          endTime = $scope.leaveRequest.dataLeaveRequest.toDate;
          startTime.setHours(0, 0, 0);
          endTime.setHours(23, 59, 0);
        }
        $scope.leaveRequest.dataLeaveRequest.startTime = $filter('date')(startTime, config.dateFormat);
        $scope.leaveRequest.dataLeaveRequest.endTime = $filter('date')(endTime, config.dateFormat);

        var calculateData = {
          isManyDate: !$scope.leaveRequest.isOneDay,
          isMorning: $scope.leaveRequest.dataLeaveRequest.morning,
          isAfternoon: $scope.leaveRequest.dataLeaveRequest.afternoon,
          startTime: $scope.leaveRequest.dataLeaveRequest.startTime,
          endTime: $scope.leaveRequest.dataLeaveRequest.endTime,
          note: $scope.leaveRequest.dataLeaveRequest.reason
        }

        var CalculationUrl = null;
        if ($scope.isForAdmin) CalculationUrl = 'ApprovalRecord/AdminCalculationDate?email=' + $scope.employee.email;
        else CalculationUrl = 'ApprovalRecord/CalculationDate';
        apiService.create(CalculationUrl, calculateData, function (response) {
          $scope.leaveRequest.dataLeaveRequest.holiday = response.data.numberHoliday;
          $scope.leaveRequest.dataLeaveRequest.weekend = response.data.numberWeekend;
          $scope.leaveRequest.dataLeaveRequest.leave = response.data.numberLeaveDate;
        }, function (response) { }, true)


        $scope.leaveRequest.getLeaveReason(startTime, endTime);
      },

      sendLeaveRequest: function () {
        var hasChilds = false;
        var hasBuffer = false;
        if (!$scope.leaveRequest.isOneDay && $scope.leaveRequest.dataLeaveRequest.fromDate.toLocaleDateString() == $scope.leaveRequest.dataLeaveRequest.toDate.toLocaleDateString()) {
          return notificationService.errorTranslate("CAN_NOT_CHOOSE_START_DATE_SAME_END_DATE");
        }
        else if ($scope.leaveRequest.dataLeaveRequest.leave == 0) {
          return notificationService.errorTranslate("LEAVE_DATE_INVALID");
        }
        if (!$scope.leaveRequest.dayWithDrawn.selected)
          return notificationService.errorTranslate("LEAVEREQUEST_MISSING_REASON");
        else {
          var dataDayWithDrawn = $scope.leaveRequest.leaveReasons.filter(function (obj) { return obj.id == $scope.leaveRequest.dayWithDrawn.selected; })[0];
        }
        if (dataDayWithDrawn.childs.length != 0) {
          hasChilds = true;
          if (!$scope.leaveRequest.selectedItem[dataDayWithDrawn.id]) {
            return notificationService.errorTranslate("LEAVEREQUEST_MISSING_REASON");
          }
        }
        if ($scope.leaveRequest.dataLeaveRequest.reason == null || $scope.leaveRequest.dataLeaveRequest.reason == "") {
          return notificationService.errorTranslate("REASON_CAN_NOT_NULL");
        }
        angular.forEach(dataDayWithDrawn.buffer, function (buffer) {
          if (buffer.item1 != 0) {
            hasBuffer = true;
          }
        })
        if (dataDayWithDrawn.childs.length == 0 && dataDayWithDrawn.hasPaidLeave && !hasBuffer) return notificationService.errorTranslate("OUT_OF_LEAVE_REASON");
        var dataGetValidCode = {
          isManyDate: !$scope.leaveRequest.isOneDay,
          isMorning: $scope.leaveRequest.dataLeaveRequest.morning,
          isAfternoon: $scope.leaveRequest.dataLeaveRequest.afternoon,
          startTime: $scope.leaveRequest.dataLeaveRequest.startTime,
          endTime: $scope.leaveRequest.dataLeaveRequest.endTime,
          reasonTypeId: hasChilds ? $scope.leaveRequest.selectedItem[dataDayWithDrawn.id].id : $scope.leaveRequest.dayWithDrawn.selected,
          note: $scope.leaveRequest.dataLeaveRequest.reason
        };

        if (!$scope.isForAdmin) {
          // var ValidCodeUrl = null;
          // if ($scope.isForAdmin) ValidCodeUrl = 'ApprovalRecord/AdminGetApprovalRecordValidCode?email=' + $scope.employee.email;
          // else ValidCodeUrl = 'ApprovalRecord/GetApprovalRecordValidCode';
          apiService.create('ApprovalRecord/GetApprovalRecordValidCode', dataGetValidCode, function (response) {
            var _data = response.data;
            // var ApprovalUrl = null;
            // if ($scope.isForAdmin) ApprovalUrl = 'ApprovalRecord/AdminCreateApprovalRecord?email=' + $scope.employee.email;
            // else ApprovalUrl = 'ApprovalRecord/ApprovalRecord';
            if (_data.length != 0) {
              var _body = "";
              angular.forEach(_data, function (value) {
                $translate(value).then(function (translated) {
                  _body += "- " + translated + "\n"
                });
              });
              $timeout(function () {
                confirmService.showTranslate(_body, function () {

                  apiService.create('ApprovalRecord/ApprovalRecord', dataGetValidCode, function (_response) {
                    $scope.leaveRequest.refresh();
                  });
                });
              }, 300);

            }
            else {
              apiService.create('ApprovalRecord/ApprovalRecord', dataGetValidCode, function (_response) {
                $scope.leaveRequest.refresh();
              });
            }
          }, function (response) { }, true)
        }
        else {
          apiService.create('ApprovalRecord/AdminCreateApprovalRecord?email=' + $scope.employee.email, dataGetValidCode, function (_response) {
            $scope.leaveRequest.refresh();
          });
        }

      },

      oneDayMode: function () {
        $scope.leaveRequest.isOneDay = true;
        $scope.leaveRequest.timeChange();
      },
      moreDayMode: function () {
        $scope.leaveRequest.isOneDay = false;
        $scope.leaveRequest.timeChange();
      },


      onLeaveDateClick: function () {
        $scope.leaveRequest.datePopup = true;
      },
      onFromDateClick: function () {
        $scope.leaveRequest.fromDatePopup = true;
      },
      onToDateClick: function () {
        $scope.leaveRequest.toDatePopup = true;
      }
    }

    //-----------Manually Record Controller----------------
    $scope.manuallyRecord = {
      time: {},
      dateOptions: {},
      data: {},
      datePopup: null,

      refresh: function () {
        var time = new Date();
        var timeEnd = new Date();
        time.setHours(8, 30, 0, 0);
        timeEnd.setHours(17, 30, 0, 0);
        $scope.manuallyRecord.time = {
          date: new Date(time),
          fromTime: new Date(time),
          toTime: new Date(timeEnd),
          tempdate: new Date(time)
        };

        // if ($scope.manuallyRecord.time.tempdate.getDate() < 4) {
        //   $scope.manuallyRecord.time.tempdate.setMonth($scope.manuallyRecord.time.tempdate.getMonth() - 1);
        // }
        // $scope.manuallyRecord.time.tempdate.setDate(1);

        $scope.manuallyRecord.dateOptions = {
          // dateDisabled: disabled,
          formatYear: 'yy',
          maxDate: new Date(),
          minDate: $scope.minDate,
          startingDay: 1
        };
        $scope.loadManagerInfo();

        $scope.manuallyRecord.data = {
          workDate: null,
          isManual: null,
          checkInTime: null,
          checkOutTime: null,
          note: ""
        };

        $scope.manuallyRecord.datePopup = false;

      },

      sendManuallyRecord: function () {
        $scope.manuallyRecord.data.workDate = $filter('date')($scope.manuallyRecord.time.date, config.dateFormat);
        $scope.manuallyRecord.data.isManual = true;
        var checkin = null;
        var checkout = null;

        if (!$scope.manuallyRecord.data.note) {
          return notificationService.errorTranslate("REASON_CAN_NOT_NULL");
        }

        if ($scope.manuallyRecord.time.fromTime != null) {
          checkin = new Date($scope.manuallyRecord.time.date);
          checkin.setHours($scope.manuallyRecord.time.fromTime.getHours());
          checkin.setMinutes($scope.manuallyRecord.time.fromTime.getMinutes());
          checkin.setSeconds(0);
        }
        if ($scope.manuallyRecord.time.toTime != null) {
          checkout = new Date($scope.manuallyRecord.time.date);
          checkout.setHours($scope.manuallyRecord.time.toTime.getHours());
          checkout.setMinutes($scope.manuallyRecord.time.toTime.getMinutes());
          checkout.setSeconds(0);
        }

        if ($scope.manuallyRecord.time.date == undefined) {
          return notificationService.errorTranslate('WRONG_DATE_FORMAT');
        }
        if (checkin == null) {
          return notificationService.errorTranslate('CHECKIN_CAN_NOT_NULL');
        }
        if (checkout != null) {
          var fromtime = $scope.manuallyRecord.time.fromTime.getHours() * 60 + $scope.manuallyRecord.time.fromTime.getMinutes();
          var totime = $scope.manuallyRecord.time.toTime.getHours() * 60 + $scope.manuallyRecord.time.toTime.getMinutes();
          if (fromtime >= totime) {
            return notificationService.errorTranslate('MANUALLYRECORD_INVALID_TIME');
          }
        }
        else if ($scope.manuallyRecord.time.date.toLocaleDateString() != (new Date()).toLocaleDateString()) {
          return notificationService.errorTranslate('CHECKOUT_CAN_NOT_NULL');
        }
        if (checkin > new Date()) {
          return notificationService.errorTranslate('CAN_NOT_MAKE_RECORD_FOR_FUTURE');
        }

        $scope.manuallyRecord.data.checkInTime = $filter('date')(checkin, config.dateFormat);
        $scope.manuallyRecord.data.checkOutTime = $filter('date')(checkout, config.dateFormat);
        // var manualValidCode = $scope.isForAdmin ?
        //   'TimeKeeping/AdminGetManualValidCode' + '?email=' + $scope.employee.email : 'TimeKeeping/GetManualValidCode';
        // var createManually = $scope.isForAdmin ?
        //   'TimeKeeping/AdminCreateTimeKeepingManually' + '?email=' + $scope.employee.email : 'TimeKeeping/TimeKeepingManually';
        if (!$scope.isForAdmin){
          apiService.create('TimeKeeping/GetManualValidCode', $scope.manuallyRecord.data, function (response) {
            var _data = response.data;
            if (_data.length != 0) {
              var _body = "";
              angular.forEach(_data, function (value) {
                $translate(value).then(function (translated) {
                  _body += "- " + translated + "\n"
                });
              });
              $timeout(function () {
                confirmService.showTranslate(_body, function () {
                  apiService.create('TimeKeeping/TimeKeepingManually', $scope.manuallyRecord.data, function (response) {
                    $scope.manuallyRecord.refresh();
                    menuService.updateMenu();
                  });
                });
              }, 300);
            }
            else {
              apiService.create('TimeKeeping/TimeKeepingManually', $scope.manuallyRecord.data, function (response) {
                $scope.manuallyRecord.refresh();
                menuService.updateMenu();
              });
            }
          }, function (response) {

          }, true)
        }
        else{
          apiService.create('TimeKeeping/AdminCreateTimeKeepingManually' + '?email=' + $scope.employee.email, $scope.manuallyRecord.data, function (response) {
            $scope.manuallyRecord.refresh();
            menuService.updateMenu();
          });
        }

      },

      opendate: function () {
        $scope.manuallyRecord.datePopup = true;
      }

    }

    //-----------Overtime Controller-----------------------
    $scope.overtime = {
      form: {},
      dateOptions: {},
      data: {},
      datePopup: null,

      refresh: function () {
        var time = new Date();
        time.setHours(0, 0, 0, 0);
        $scope.overtime.form = {
          date: new Date(time),
          fromTime: new Date(time),
          toTime: new Date(time),
          tempdate: new Date(time),
          managerUser: null,
          managerName: null,
          userName: null,
          department: null,
          reason: null,
          event: [],
          groupEvent: [],
          groupEventSelected: {},
          selectedEvent: null,
          tempEvent: []
        };

        // if ($scope.overtime.form.tempdate.getDate() < 4) {
        //   $scope.overtime.form.tempdate.setMonth($scope.overtime.form.tempdate.getMonth() - 1);
        // }
        // $scope.overtime.form.tempdate.setDate(1);

        $scope.overtime.dateOptions = {
          formatYear: 'yy',
          maxDate: new Date(),
          minDate: $scope.minDate,
          startingDay: 1
        };
        $scope.loadManagerInfo();

        $scope.overtime.datePopup = false;

        $scope.overtime.data = {
          workDate: null,
          startTime: null,
          endTime: null,
          eventId: null,
          note: null
        };
        var GetGroupUrl = $scope.isForAdmin ? 'Event/GetGroupEvents' + '?email=' + $scope.employee.email : 'Event/GetGroupEvents'
        apiService.get(GetGroupUrl, null, function (response) {
          if (response.data != null) {
            $scope.overtime.form.groupEvent = response.data;
            $scope.overtime.form.groupEventSelected = $scope.overtime.form.groupEvent[0];
          }
        });
      },

      sendOvertimeRequest: function () {
        $scope.overtime.data.workDate = $filter('date')($scope.overtime.form.date, config.dateFormat);
        if ($scope.overtime.form.fromTime) {
          $scope.overtime.data.startTime = new Date($scope.overtime.form.date);
          $scope.overtime.data.startTime.setHours($scope.overtime.form.fromTime.getHours());
          $scope.overtime.data.startTime.setMinutes($scope.overtime.form.fromTime.getMinutes());
          $scope.overtime.data.startTime.setSeconds(0);
        }
        else {
          return notificationService.errorTranslate("CHECKIN_CAN_NOT_NULL");
        }
        if ($scope.overtime.form.toTime) {
          $scope.overtime.data.endTime = new Date($scope.overtime.form.date);
          $scope.overtime.data.endTime.setHours($scope.overtime.form.toTime.getHours());
          $scope.overtime.data.endTime.setMinutes($scope.overtime.form.toTime.getMinutes());
          $scope.overtime.data.endTime.setSeconds(0);
        }
        else {
          return notificationService.errorTranslate("CHECKOUT_CAN_NOT_NULL");
        }
        if (!$scope.overtime.form.reason) {
          return notificationService.errorTranslate("REASON_CAN_NOT_NULL");
        }
        $scope.overtime.data.note = $scope.overtime.form.reason;
        if ($scope.overtime.form.selectedEvent == null) {
          if ($scope.overtime.form.groupEventSelected.inputEventCode) {
            return notificationService.errorTranslate("EVENT_CAN_NOT_NULL");
          }
          else {
            $scope.overtime.data.eventId = null;
          }
        }
        else {
          $scope.overtime.data.eventId = $scope.overtime.form.selectedEvent.id
        }
        var fromtime = $scope.overtime.form.fromTime.getHours() * 60 + $scope.overtime.form.fromTime.getMinutes();
        var totime = $scope.overtime.form.toTime.getHours() * 60 + $scope.overtime.form.toTime.getMinutes();

        if ($scope.overtime.form.date == undefined) {
          return notificationService.errorTranslate('WRONG_DATE_FORMAT');
        }
        else if ($scope.overtime.data.startTime > new Date() && $scope.overtime.data.endTime > new Date()) {
          return notificationService.errorTranslate('CAN_NOT_MAKE_RECORD_FOR_FUTURE');
        }
        else if (fromtime >= totime) {
          return notificationService.errorTranslate('LEAVEREQUEST_ERROR_STATTIME_GREATER_THAN_ENDTIME');
        }
        else {
          $scope.overtime.data.startTime = $filter('date')($scope.overtime.data.startTime, config.dateFormat);
          $scope.overtime.data.endTime = $filter('date')($scope.overtime.data.endTime, config.dateFormat);
          var otValidcodeUrl = $scope.isForAdmin ? 'OverTime/AdminGetOverTimeValidCode' + '?email=' + $scope.employee.email : 'OverTime/GetOverTimeValidCode'
          var createOTUrl = $scope.isForAdmin ? 'OverTime/AdminCreateManualOverTime' + '?email=' + $scope.employee.email : 'OverTime/ManualOverTime'
          if(!$scope.isForAdmin){
            apiService.create(otValidcodeUrl, $scope.overtime.data, function (response) {
              var _data = response.data;
              if (_data.length != 0) {
                var _body = "";
                angular.forEach(_data, function (value) {
                  $translate(value).then(function (translated) {
                    _body += "- " + translated + "\n"
                  });
                });
                $timeout(function () {
                  confirmService.showTranslate(_body, function () {
                    apiService.create(createOTUrl, $scope.overtime.data, function (response) {
                      $scope.overtime.refresh();
                    });
                  });
                }, 300);
              }
              else {
                apiService.create(createOTUrl, $scope.overtime.data, function (response) {
                  $scope.overtime.refresh();
                });
              }
            }, function (response) { }, true)
          }
          else{
            apiService.create(createOTUrl, $scope.overtime.data, function (response) {
              $scope.overtime.refresh();
            });
          }
        }
      },

      onChangeEventsGroup: function () {
        $scope.overtime.form.selectedEvent = null;
        if ($scope.overtime.form.groupEventSelected.inputEventCode)
          getEvent("");
      },

      refreshEvent: function (input) {
        if (input)
          getEvent(input);
        else $scope.overtime.form.event = $scope.overtime.form.tempEvent;
      },

      opendate: function () {
        $scope.overtime.datePopup = true;
      },

      openEventList: function () {
        $window.open('http://topi.ca/macoche', '_blank');
      }
    }

    var getEvent = function (key) {
      var getEventUrl = $scope.isForAdmin ? 'Event/GetEvents' + '?email=' + $scope.employee.email + '&groupId=' : 'Event/GetEvents' + '?groupId='
      apiService.get(getEventUrl + $scope.overtime.form.groupEventSelected.id + '&keyWord=' + key + '&perPage=100', null, function (response) {
        $scope.overtime.form.event = [];
        if (response.data.data != null) {
          angular.forEach(response.data.data, function (val) {
            var eventItem = { 'id': val.id, 'eventCode': val.eventCode, 'description': val.description, 'percentage': val.percentage, 'payPerBlock': val.payPerBlock, 'payPerHour': val.payPerHour * 1000 };
            $scope.overtime.form.event.push(eventItem);
          });
        }
        if (!key) {
          $scope.overtime.form.tempEvent = $scope.overtime.form.event;
        }
      });
    };
    $scope.getMinDate();
    $scope.send = function () {
      if ($scope.screen.screenShow == 'leaveRequest') $scope.leaveRequest.sendLeaveRequest();
      if ($scope.screen.screenShow == 'manuallyRecord') $scope.manuallyRecord.sendManuallyRecord();
      if ($scope.screen.screenShow == 'overtime') $scope.overtime.sendOvertimeRequest();
    }
  }

})();
