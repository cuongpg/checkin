/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.approvalRecord', [])
    .config(routeConfig);


  function routeConfig($stateProvider) {
    $stateProvider
      .state('approvalRecord', {
        url: '/approvalRecord',
        templateUrl: 'app/pages/approvalRecord/approvalRecord.html',
        title: 'APPROVAL_RECORD',
        controller: 'ApprovalRecordCtrl',
        sidebarMeta: {
          icon: 'ion-document-text',
          order: 2,
        },
        permission: ['Employee.TimeKeepingManagerInfo']
      });
  }
})();
