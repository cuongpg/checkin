/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.licenses', [])
        .config(routeConfig);
  function routeConfig($stateProvider) {
    $stateProvider
        .state('licenses', {
          url: '/licenses',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'LICENSES',
          sidebarMeta: {
            icon: 'ion-clipboard',
            order: 3,
          },
          // 'permission': ['Users.GetAll', 'Groups.GetAll']
        });
  }
  })();
  