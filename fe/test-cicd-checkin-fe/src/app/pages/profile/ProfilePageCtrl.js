(function () {
  'use strict';

  angular.module('BlurAdmin.pages.profile')
    .controller('ProfilePageCtrl', ProfilePageCtrl);

  /** @ngInject */
  function ProfilePageCtrl($scope, apiService) {
    
    $scope.profile = {
      data: null,
      init: function () {
        var _this = this;
        apiService.get('oauth/info', null, function (response) {
          _this.data = response.data;
        });
      },
    };
  }

})();
