(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.property')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('property.ListContracts', {
          url: '/list_contracts',
          title: 'List_Contracts',
          templateUrl: 'app/pages/property/contracts/listContracts.html',
          controller: 'ListContractPageCtrl',
          sidebarMeta: {
            order: 1,
          },
          // 'permission': 'Groups.GetAll'
        });
    }
  
  })();