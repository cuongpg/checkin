/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.property', [])
      .config(routeConfig);

  /** @ngInject 
  function routeConfig($stateProvider) {
    $stateProvider
        .state('property', {
          url: '/property',
          templateUrl: 'app/pages/property/property.html',
          title: 'PROPERTY',
          sidebarMeta: {
            icon: 'ion-android-home',
            order: 0,
          },
        });
  }
*/
function routeConfig($stateProvider) {
  $stateProvider
      .state('property', {
        url: '/property',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'PROPERTY',
        sidebarMeta: {
          icon: 'ion-android-home',
          order: 1,
        },
        // 'permission': ['Users.GetAll', 'Groups.GetAll']
      });
}
})();
