(function () {
  'use strict';

  angular.module('BlurAdmin.pages.timesheet')
    .controller('MyTimeSheetsCtrl', MyTimeSheetsCtrl);

  /** @ngInject */
  function MyTimeSheetsCtrl(baConfig, $scope, apiService, $uibModal, config, confirmService, $translate, dateService, $filter) {
    $scope.email = null;
    if ($scope.$resolve.item) {
      $scope.email = $scope.$resolve.item.email;
      $scope.isAdmin = $scope.$resolve.item.isAdmin
    }
    $scope.overtime = "";
    $scope.dateChange = new Date();
    $scope.timeSheetData = {
      totalMorningSeconds: null,
      totalAfternoonSeconds: null,
      totalRejects: null,
      totalShiftEat: null,
      totalWorkSeconds: null,
      totalWorkingDays: null,
      totalOverTime: [],
      holidayDatesInMonth: [],
      timeKeepingsInMonth: [],
      approvalDates: [],
      totalPendingOverTime: [],
      employeeType: "",
      totalNumberOfLeaveDay: null,
      quotasTimeKeepingInMonth: null,
      averageWorkSeconds: null,
      isLoading: true,
      subScript: "",
      listScript: [],
      scriptSelected: null,
      eventWorksApproval: [],
      eventWorksPending: [],
      hasEvent: false,


    };
    var renderFristTime = true;
    var timeKeepingsInMonth = [];
    var overTimes = [];
    var pendingOverTimes = [];
    var expiredOverTimes = [];
    var eventWorksExpired = [];

    var weekendInMonth = [];

    $scope.getDateFromBackendFormat = function (date) {
      var dateElements = date.split('/');
      return dateElements[2] + '-' + dateElements[1] + '-' + dateElements[0];
    }

    $scope.openInstruction = function () {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/myTimeSheets/timeSheetInstruction.html',
        size: 'lg',
      });
    }

    $scope.openEventBoard = function () {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/myTimeSheets/timeSheetDetails/eventDetailsModal.html',
        size: 'lg',
        controller: 'EventDetailsCtrl',
        resolve: {
          eventWorksApproval: function () {
            return $scope.timeSheetData.eventWorksApproval;
          },
          eventWorksPending: function () {
            return $scope.timeSheetData.eventWorksPending;
          },
        }
      })
    }

    $scope.refresh = function (month, year, scriptId) {
      $scope.timeSheetData.hasEventWork = false;
      // $scope.timeSheetData.timeKeepingsInMonth = [];
      $('#calendar').fullCalendar('removeEvents', function () {
        return $scope.timeSheetData.timeKeepingsInMonth
      })



      timeKeepingsInMonth = [];
      var myTimeSheetUrl = "TimeKeeping/GetTimeKeeping/" + month + "/" + year + "/" + scriptId;
      if ($scope.email != null) {
        if ($scope.isAdmin == true)
          myTimeSheetUrl = "TimeKeeping/GetTimeKeepingOfEmployee/" + month + "/" + year + "/" + scriptId + "/" + $scope.email;
        else
          myTimeSheetUrl = "TimeKeeping/GetTimeKeepingOfStaff/" + month + "/" + year + "/" + scriptId + "/" + $scope.email;
      }


      $scope.timeSheetData.isLoading = true;
      apiService.get(myTimeSheetUrl, null, function (response) {
        $scope.timeSheetData.totalMorningSeconds = dateService.getTotalHours(response.data.totalMorningSeconds * 1000);
        $scope.timeSheetData.totalAfternoonSeconds = dateService.getTotalHours(response.data.totalAffternoonSeconds * 1000);
        $scope.timeSheetData.totalRejects = response.data.totalRejects;
        $scope.timeSheetData.totalShiftEat = response.data.totalShiftEat;
        $scope.timeSheetData.totalNumberOfWorkDay = response.data.totalNumberOfWorkDay;
        $scope.timeSheetData.totalWorkSeconds = dateService.getTotalHours(response.data.totalWorkSeconds * 1000);
        $scope.timeSheetData.totalOverTime = response.data.totalOverTime;
        $scope.timeSheetData.totalPendingOverTime = response.data.totalPendingOverTime;
        $scope.timeSheetData.holidayDatesInMonth = response.data.holidayDatesInMonth;
        $scope.timeSheetData.approvalDates = response.data.approvalDates;
        $scope.timeSheetData.totalNumberOfLeaveDay = response.data.totalNumberOfLeaveDay;
        $scope.timeSheetData.employeeType = response.data.employeeType;
        $scope.timeSheetData.quotasTimeKeepingInMonth = response.data.quotasTimeKeepingInMonth;
        $scope.timeSheetData.averageWorkSeconds = response.data.totalNumberOfWorkDay != 0 ? dateService.getTotalHours(response.data.totalWorkSeconds / response.data.totalNumberOfWorkDay * 1000) : dateService.getTotalHours(0);
        $scope.timeSheetData.totalWorkingDays = response.data.totalNumberOfWorkDay + response.data.totalNumberOfLeaveDay;
        timeKeepingsInMonth = response.data.timeKeepingsInMonth;
        overTimes = response.data.overTimes;
        pendingOverTimes = response.data.pendingOverTimes;
        expiredOverTimes = response.data.expiredOverTimes;
        $scope.timeSheetData.subScript = response.data.subScript;
        weekendInMonth = response.data.weekendInMonth;
        $scope.timeSheetData.eventWorksApproval = response.data.eventWorksApproval;
        $scope.timeSheetData.eventWorksPending = response.data.eventWorksPending;
        eventWorksExpired = response.data.eventWorksExpired;
        $scope.timeSheetData.totalCheckinLate = response.data.totalCheckinLate;
        $scope.timeSheetData.totalCheckoutEarly = response.data.totalCheckoutEarly;
        var tempTimekeeping = [];

        angular.forEach(response.data.holidayDatesInMonth, function (value) {
          tempTimekeeping.push(
            {
              start: $scope.getDateFromBackendFormat($filter('date')(value.date, 'dd/MM/yyyy')),

              color: 'green',
              rendering: 'background'
            }
          );
          tempTimekeeping.push(
            {
              title: value.description,
              start: $scope.getDateFromBackendFormat($filter('date')(value.date, 'dd/MM/yyyy')),
              textColor: 'white',
              borderColor: '#00000000',
              backgroundColor: '#00000000',
              eventOrder: '1',
            }
          );

        });
        angular.forEach(response.data.approvalDates, function (value) {
          if (value.status == 0 || value.status == 1 || value.status == 4 || value.status == 5)
            tempTimekeeping.push(
              {
                title: (value.totalPaidLeave == 0 ? '' : value.totalPaidLeave + 'P')
                  + ((value.totalUnPaidLeave == 0 || value.totalPaidLeave == 0) ? '' : '-')
                  + (value.totalUnPaidLeave == 0 ? '' : value.totalUnPaidLeave + 'U') + ' : ' + value.note,
                start: $scope.getDateFromBackendFormat($filter('date')(value.workDate, 'dd/MM/yyyy')),
                workDate: value.workDate,
                textColor: (value.status == 1 || value.status == 4) ? 'white' : '#ff9900',
                borderColor: value.status == 4 ? 'red' : '#ff9900',
                backgroundColor: value.status == 1 ? '#ff9900' : (value.status == 4 ? 'red' : '#cccccc'),
                eventOrder: '2',
              }
            );
        });
        angular.forEach(response.data.timeKeepingsInMonth, function (value, key) {
          angular.forEach(value.details, function (detail, keyDetail) {
            if (detail.status == 0 || detail.status == 1 || detail.status == -1 || detail.status == 4)
              tempTimekeeping.push(
                {
                  title: detail.checkInTime + " - " + (detail.checkOutTime == null ? "" : detail.checkOutTime),
                  start: $scope.getDateFromBackendFormat(value.workDate),
                  workDate: value.workDate,
                  textColor: detail.status == 0 ? '#0795db' : 'white',
                  borderColor: detail.status == 4 ? 'red' : '#0795db',
                  backgroundColor: detail.status == 0 ? '#cccccc' : detail.status == 4 ? 'red' : '#0795db',
                  eventOrder: '2'
                }
              );
          });
        });

        angular.forEach(response.data.overTimes, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          tempTimekeeping.push(
            {
              title: $scope.overtime + " " + dateService.getTotalHours(value.item1 * 1000),
              start: $scope.getDateFromBackendFormat(value.item2),
              workDate: value.item2,
              borderColor: '#00c853',
              textColor: 'white',
              backgroundColor: '#00c853',
              eventOrder: '3',
            }
          );
          // }).catch(function (err) {
          // });

        });

        angular.forEach(response.data.pendingOverTimes, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          tempTimekeeping.push(
            {
              title: $scope.overtime + " " + dateService.getTotalHours(value.item1 * 1000),
              start: $scope.getDateFromBackendFormat(value.item2),
              workDate: value.item2,
              borderColor: '#00c853',
              textColor: '#00c853',
              backgroundColor: '#cccccc',
              eventOrder: '3',
            }
          );
          // }).catch(function (err) {
          // });

        });
        angular.forEach(response.data.expiredOverTimes, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          tempTimekeeping.push(
            {
              title: $scope.overtime + " " + dateService.getTotalHours(value.item1 * 1000),
              start: $scope.getDateFromBackendFormat(value.item2),
              workDate: value.item2,
              borderColor: 'red',
              textColor: 'white',
              backgroundColor: 'red',
              eventOrder: '3',
            }
          );
          // }).catch(function (err) {
          // });

        });

        angular.forEach(response.data.weekendInMonth, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          tempTimekeeping.push(
            {
              title: 'weekend',
              start: $scope.getDateFromBackendFormat($filter('date')(value.date, 'dd/MM/yyyy')),
              backgroundColor: '#bbbbbb',
              rendering: 'background'
            }
          );
          // }).catch(function (err) {
          // });

        });

        angular.forEach(response.data.eventWorksApproval, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          angular.forEach(value, function (group) {
            tempTimekeeping.push(
              {
                title: group.eventCode + " - " + dateService.getTotalHours(group.seconds * 1000),
                start: $scope.getDateFromBackendFormat(group.workDate),
                workDate: group.workDate,
                borderColor: '#00c853',
                textColor: 'white',
                backgroundColor: '#00c853',
                eventOrder: '3'
              }
            );
            // }).catch(function (err) {
            $scope.timeSheetData.hasEventWork = true;
          });
        });

        angular.forEach(response.data.eventWorksPending, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          angular.forEach(value, function (group) {
            tempTimekeeping.push(
              {
                title: group.eventCode + " - " + dateService.getTotalHours(group.seconds * 1000),
                start: $scope.getDateFromBackendFormat(group.workDate),
                workDate: group.workDate,
                borderColor: '#00c853',
                textColor: '#00c853',
                backgroundColor: '#cccccc',
                eventOrder: '3'
              }
            );
            $scope.timeSheetData.hasEventWork = true;
          });

        });

        angular.forEach(response.data.eventWorksExpired, function (value, key) {
          // $translate('OVERTIME').then(function (translated) {
          angular.forEach(value, function (group) {
            tempTimekeeping.push(
              {
                title: group.eventCode + " - " + dateService.getTotalHours(group.seconds * 1000),
                start: $scope.getDateFromBackendFormat(group.workDate),
                workDate: group.workDate,
                borderColor: 'red',
                textColor: 'white',
                backgroundColor: 'red',
                eventOrder: '3'
              }
            );
            $scope.timeSheetData.hasEventWork = true;
          });

        });

        angular.forEach($scope.timeSheetData.totalOverTime, function (value, key) {
          value.item1 = dateService.getTotalHours(value.item1 * 1000);
        });

        angular.forEach($scope.timeSheetData.totalPendingOverTime, function (value, key) {
          value.item1 = dateService.getTotalHours(value.item1 * 1000);
        });

        $scope.timeSheetData.timeKeepingsInMonth = tempTimekeeping;
        $scope.timeSheetData.isLoading = false;
        renderTimesheet();
        //show calendar

        renderFristTime = false;
        ;
      }, function () {
        $scope.timeSheetData.timeKeepingsInMonth = [];
        $scope.timeSheetData.isLoading = false;
        renderTimesheet();
        renderFristTime = false;
      })
    }

    var renderTimesheet = function () {
      $('#calendar').fullCalendar('addEventSource', $scope.timeSheetData.timeKeepingsInMonth);

      var $element = $('#calendar').fullCalendar({
        //height: 335,
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        eventBackgroundColor: 'white',
        eventTextColor: 'black',
        defaultDate: new Date(),
        selectable: true,
        eventOrder: "eventOrder",
        firstDay: 1,
        select: function (start, end) {
          var timeKeepingsInDay = timeKeepingsInMonth.filter(function (obj) { return obj.workDate == $filter('date')(start._d, 'dd/MM/yyyy'); })[0];
          var overtime = overTimes.filter(function (obj) { return obj.item2 == $filter('date')(start._d, 'dd/MM/yyyy'); })[0];
          var data = {
            attendedDay: $filter('date')(start._d, 'dd/MM/yyyy'),
            hours: 0,
            overtimes: 0,
            units: 0,
            details: []
          };
          if (timeKeepingsInDay) {
            data = {
              attendedDay: $filter('date')(start._d, 'dd/MM/yyyy'),
              hours: dateService.calculateDate(timeKeepingsInDay.totalSeconds * 1000),
              overtimes: 0,
              units: timeKeepingsInDay.numberOfWorkDay,
              details: timeKeepingsInDay.details
            };
          }
          if (overtime) {
            data.overtimes = dateService.calculateDate(overtime.item1 * 1000);
          }

          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/myTimeSheets/timeSheetDetails/timeSheetDetailsModal.html',
            size: 'lg',
            controller: 'TimeSheetDetailsCtrl',
            resolve: {
              details: function () {
                return data;
              }
            }
          });
        },
        editable: false, // canot drop event
        eventLimit: true, // allow "more" link when too many events
        events: $scope.timeSheetData.timeKeepingsInMonth,
        viewRender: function (view, element) {
          if (view.type == "month" && !renderFristTime) {
            $scope.dateChange = new Date(moment(view.title, "MMMM YYYY").local().toDate());
            $scope.resetScript($scope.dateChange.getMonth() + 1, $scope.dateChange.getFullYear());

          }
        },
        eventClick: function (event) {
          var timeKeepingsInDay = timeKeepingsInMonth.filter(function (obj) { return obj.workDate == event.workDate; })[0];
          var overtime = overTimes.filter(function (obj) { return obj.item2 == event.workDate; })[0];
          var pendingOT = pendingOverTimes.filter(function (obj) { return obj.item2 == event.workDate; });
          var expiredOT = expiredOverTimes.filter(function (obj) { return obj.item2 == event.workDate; });
          var leave = $scope.timeSheetData.approvalDates.filter(function (obj) { return obj.workDate == event.workDate; });
          var data = {
            attendedDay: event.workDate,
            hours: 0,
            overtimes: 0,
            units: 0,
            details: {
              timeKeeping: [],
              overTime: {
                approvedOT: [],
                pendingOT:[],
                expiredOT:[]
              },
              leaveRequest: []
            }
          };
          // console.log({
          //   "timeKeepingsInDay": timeKeepingsInDay,
          //   "overtime": overtime,
          //   "pendingOT": pendingOT,
          //   "expiredOT": expiredOT,
          //   "leave": leave
          // })
          if (timeKeepingsInDay) {
            data.attendedDay = event.workDate;
            data.hours = dateService.calculateDate(timeKeepingsInDay.totalSeconds * 1000);
            data.units = timeKeepingsInDay.numberOfWorkDay;
            data.details.timeKeeping = timeKeepingsInDay.details;
          }
          if (overtime) {
            data.overtimes = dateService.calculateDate(overtime.item1 * 1000);
            data.details.overTime.approvedOT.push(overtime)
          }
          if (pendingOT.length > 0) {
            data.details.overTime.pendingOT = pendingOT
          }
          if (expiredOT.length > 0) {
            data.details.overTime.expiredOT = expiredOT
          }
          if (leave.length > 0) {
            data.details.leaveRequest = leave;
          }

          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/myTimeSheets/timeSheetDetails/timeSheetDetailsModal.html',
            size: 'lg',
            controller: 'TimeSheetDetailsCtrl',
            resolve: {
              details: function () {
                return data;
              }
            }
          });
        },

      });

    }

    var dashboardColors = baConfig.colors.dashboard;

    $scope.changeScript = function () {
      $scope.refresh($scope.dateChange.getMonth() + 1, $scope.dateChange.getFullYear(), $scope.timeSheetData.scriptSelected.id);
    }
    $scope.resetScript = function (month, year) {
      var GetScriptUrl = "Script/GetScriptsInMonth/" + month + "/" + year;
      if ($scope.email != null) {
        if ($scope.isAdmin)
          GetScriptUrl = "Script/GetScriptsOfEmployeeInMonth/" + month + "/" + year + "/" + $scope.email;
        else
          GetScriptUrl = "Script/GetScriptsOfStaffInMonth/" + month + "/" + year + "/" + $scope.email;
      }
      apiService.get(GetScriptUrl, null, function (response) {
        $scope.timeSheetData.listScript = response.data;
        $scope.timeSheetData.scriptSelected = $scope.timeSheetData.listScript[0];
        $translate('OVERTIME').then(function (translated) {
          $scope.overtime = translated;
          $scope.refresh(month, year, $scope.timeSheetData.scriptSelected.id);
        }).catch(function (err) {
        });
      }, function (response) {
        $scope.refresh(month, year, 1);
      });
    }

    $scope.resetScript(new Date().getMonth() + 1, new Date().getFullYear());

  }
})();