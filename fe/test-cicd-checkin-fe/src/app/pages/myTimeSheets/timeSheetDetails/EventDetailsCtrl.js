/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.myAttendance')
    .controller('EventDetailsCtrl', EventDetailsCtrl);


  /** @ngInject */
  function EventDetailsCtrl($scope, apiService, $uibModalInstance, notificationService, config, dateService, eventWorksApproval, eventWorksPending) {
    $scope.eventWorksPending = eventWorksPending;
    $scope.eventWorksApproval = eventWorksApproval;

    $scope.refresh = function () {
      if ($scope.eventWorksApproval.length != 0)
        angular.forEach($scope.eventWorksApproval, function (event) {
          angular.forEach(event, function (group) {
            group.hours = dateService.getTotalHours(group.seconds * 1000)
          })
        });
      if ($scope.eventWorksPending.length != 0)
        angular.forEach($scope.eventWorksPending, function (event) {
          angular.forEach(event, function (group) {
            group.hours = dateService.getTotalHours(group.seconds * 1000)
          })
        });
    }

    $scope.refresh();


  }
})();