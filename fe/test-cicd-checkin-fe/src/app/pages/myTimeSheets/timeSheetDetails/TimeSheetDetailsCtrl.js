/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.myAttendance')
    .controller('TimeSheetDetailsCtrl', TimeSheetDetailsCtrl);


  /** @ngInject */
  function TimeSheetDetailsCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, details) {

    $scope.overTimesDetails = []

    $scope.noteData = {
      attendedDay: details.attendedDay,
      hours: details.hours,
      overtimes: details.overtimes,
      units: details.units,
      details: details.details
    };

    $scope.log = function () {
      var _this = this;
      if (_this.noteData.details.overTime) {
        if (_this.noteData.details.overTime.approvedOT) {
          angular.forEach(details.details.overTime.approvedOT, function (ot) {
            angular.forEach(ot.item3, function (item) {
              _this.overTimesDetails.push({
                startTime: item.item1,
                endTime: item.item2,
                status: 1
              })
            })
          })
        }
        if (_this.noteData.details.overTime.pendingOT) {
          angular.forEach(_this.noteData.details.overTime.pendingOT, function (ot) {
            angular.forEach(ot.item3, function (item) {
              _this.overTimesDetails.push({
                startTime: item.item1,
                endTime: item.item2,
                status: 0
              })
            })
          })
        }
        if (_this.noteData.details.overTime.expiredOT) {
          angular.forEach(_this.noteData.details.overTime.expiredOT, function (ot) {
            angular.forEach(ot.item3, function (item) {
              _this.overTimesDetails.push({
                startTime: item.item1,
                endTime: item.item2,
                status: 2
              })
            })
          })
        }
      }
      // console.log($scope.noteData);
      // console.log(details)
    }
    $scope.log();


  }
})();