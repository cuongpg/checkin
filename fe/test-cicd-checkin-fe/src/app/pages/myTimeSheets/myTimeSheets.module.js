/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.timesheet', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('timesheet', {
        url: '/timesheet',
        templateUrl: 'app/pages/myTimeSheets/myTimeSheets.html',
        title: 'TIMESHEET',
        controller: 'MyTimeSheetsCtrl',
        sidebarMeta: {
          icon: 'fa fa fa-briefcase',
          order: 1,
        }
      });
  }

})();
