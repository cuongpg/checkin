(function () {
    'use strict';

    var attendanceModule = angular.module('BlurAdmin.pages.attendance');

      /** @ngInject */
      attendanceModule.factory('irrecdanceService', function() {
        var factory = {};

        factory.getListPageSize = function() {
            return [
                {
                    value: 5,
                    text: '5'
                },
                {
                    value: 10,
                    text: '10'
                },
                {
                    value: 20,
                    text: '20'
                },
                {
                    value: 25,
                    text: '25'
                }
            ];
        };

        return factory;
    });
})();