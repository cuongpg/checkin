(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.attendance', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('attendance', {
          url: '/attendance',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'ATTENDANCE',
          sidebarMeta: {
            icon: 'fa fa fa-bullhorn',
            order: 4,
          },
          permission: ['OverTime.GetOvertimeForManager']
        })
        // .state('attendance.approve',
        // {
        //   url: '/approve',
        //   templateUrl: 'app/pages/attendance/approve/approve.html',
        //   controller: 'ApproveCtrl',
        //   controllerAs: 'vm',
        //   title: 'PENDING_RECORD',
        //   sidebarMeta: {
        //     order: 0,
        //   },
        // })
        .state('attendance.lowerGrade',
        {
          url: '/lowerGrade',
          templateUrl: 'app/pages/attendance/lowerGrade/lowerGrade.html',
          controller: 'LowerGradeCtrl',
          controllerAs: 'vm',
          title: 'MY_EMPLOYEE',
          sidebarMeta: {
            order: 5,
          },
        })
        .state('attendance.irregular',
        {
          url: '/irregular',
          templateUrl: 'app/pages/attendance/irregular/irregular.html',
          controller: 'IrregularCtrl',
          controllerAs: 'vm',
          title: 'MY_IRREGULAR',
          sidebarMeta: {
            order: 1,
          },
        })
        .state('attendance.overtime',
        {
          url: '/overtime',
          templateUrl: 'app/pages/attendance/overtime/overtime.html',
          controller: 'OvertimeCtrl',
          controllerAs: 'vm',
          title: 'MY_OVERTIME',
          sidebarMeta: {
            order: 2,
          },
        })
        // .state('attendance.unovertime',
        // {
        //   url: '/unovertime',
        //   templateUrl: 'app/pages/attendance/unovertime/unovertime.html',
        //   controller: 'UnOvertimeCtrl',
        //   controllerAs: 'vm',
        //   title: 'UN_OVERTIME',
        //   sidebarMeta: {
        //     order: 3,
        //   },
        // })
        .state('attendance.leave',
        {
          url: '/leave',
          templateUrl: 'app/pages/attendance/leave/leave.html',
          controller: 'LeaveCtrl',
          controllerAs: 'vm',
          title: 'MY_LEAVE',
          sidebarMeta: {
            order: 3,
          },
        })
        .state('attendance.unleave',
        {
          url: '/unleave',
          templateUrl: 'app/pages/attendance/unleave/unleave.html',
          controller: 'UnLeaveCtrl',
          controllerAs: 'vm',
          title: 'UN_LEAVE',
          sidebarMeta: {
            order: 4,
          },
        });
    }
  
  })();
  