/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
    'use strict';


    angular.module('BlurAdmin.pages.attendance')
        .controller('leaveDetailsModalCtrl', leaveDetailsModalCtrl);


    /** @ngInject */
    function leaveDetailsModalCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, item, confirmService, $state, isForAdmin) {
        $scope.isForAdmin = isForAdmin;
        $scope.details = {
            createdAt: item.createdAt,
            email: item.email,
            id: item.id,
            imageUrl: item.imageUrl,
            isDestroy: item.isDestroy,
            leaveTime: item.leaveTime,
            managerEmail: item.managerEmail,
            managerName: item.managerName,
            name: item.name,
            note: item.note,
            reasonType: item.reasonType,
            status: item.status,
            statusDestroy: item.statusDestroy,
            timeDestroy: item.timeDestroy,
            timeSubmit: item.timeSubmit,
            timeSubmitDestroy: item.timeSubmitDestroy,
            totalLeaveDate: item.totalLeaveDate,
            totalPaidLeave: item.totalPaidLeave,
            totalUnpaidLeave: item.totalUnpaidLeave,

            approve: function (userId) {
                var dataPost = {
                    "ids": [
                        userId
                    ]
                }
                confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_APPROVE', function () {
                    var urlApprove = $scope.isForAdmin? 'ApprovalRecord/ApproveRecordNeedConfirmByAdmin':'ApprovalRecord/ApprovalApprovalRecord'
                    apiService.create(urlApprove, dataPost, function (response) {
                        $uibModalInstance.close();
                        $state.reload();
                    });
                });
            },
            reject: function (userId) {
                var dataPost = {
                    "ids": [
                        userId
                    ]
                }
                confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_REJECT', function () {
                    var urlReject = $scope.isForAdmin? 'ApprovalRecord/RejectRecordNeedConfirmByAdmin':'ApprovalRecord/RejectApprovalRecord'
                    apiService.create(urlReject, dataPost, function (response) {
                        $uibModalInstance.close();
                        $state.reload();
                    });
                });
            },
        };


    }
})();