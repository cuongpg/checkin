(function () {
  'use strict';

  angular.module('BlurAdmin.pages.attendance')
    .controller('LowerGradeCtrl', LowerGradeCtrl);

  /** @ngInject */
  function LowerGradeCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, notificationService, menuService) {

    $scope.employee = null;
    $scope.isForAdmin = false;
    if ($scope.$resolve.employee) {
      $scope.employee = $scope.$resolve.employee;
      $scope.isForAdmin = true;
    }

    $scope.lowerGrade = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.lowerGrade.tableState = tableState;
        $scope.lowerGrade.refresh();
      },
      refresh: function () {
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        _this.selected = [];
        _this.isLoading = true;
        var getStaffUrl = '';
        if ($scope.isForAdmin) getStaffUrl = 'users/GetStaffsOfEmployee' + '?managerId=' + $scope.employee.id
        else getStaffUrl = 'users/MyStaffs';
        apiService.get(getStaffUrl, _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {

          _this.isLoading = false;
        });
        menuService.updateMenu();
      },
      selectAll: function (collection) {
        var _this = this;
        if (_this.selected.length === 0) {
          angular.forEach(collection, function (val) {
            if (val.statusDestroy == 0) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          });
          _this.isSelectAll = true;
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length) {
          angular.forEach(collection, function (val) {
            var found = _this.selected.indexOf(val.id);
            if (found == -1) _this.selected.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        }
        else {
          _this.selected = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selected.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selected.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selected.push(id);
          if (this.selected.length === this.tableData.length) {
            this.isSelectAll = true;
          }
        }
      },
      openDetails: function (item) {
        item.isAdmin = false;
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/myTimeSheets/myTimeSheets.html',
          size: 'lg',
          windowClass: 'app-modal-window',
          controller: 'MyTimeSheetsCtrl',
          resolve: {
            item: function () {
              return item;
            }
          }
        });
      }
    };
  }
})();