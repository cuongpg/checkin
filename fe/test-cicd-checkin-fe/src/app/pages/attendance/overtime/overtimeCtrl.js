(function () {
  'use strict';

  angular.module('BlurAdmin.pages.attendance')
    .controller('OvertimeCtrl', OvertimeCtrl);

  /** @ngInject */
  function OvertimeCtrl($scope, apiService, $uibModal, confirmService, $uibModalStack, popupService, notificationService, menuService) {

    $scope.eventsGroup = [];
    $scope.eventsChecked = [];
    $scope.getEventsGroup = function () {
      apiService.get('Event/GetGroupEvents',null, function (response) {
        $scope.eventsGroup = response.data;
        angular.forEach($scope.eventsGroup, function (val) {
          $scope.eventsChecked.push(val.id)
        });
      }, function () {
      });
    };  

    $scope.onChangeEventsGroup = function(eventsId)
    {
      if($scope.eventsChecked.indexOf(eventsId) == -1)
      {
        $scope.eventsChecked.push(eventsId)
      }
      else
      {
        $scope.eventsChecked.splice($scope.eventsChecked.indexOf(eventsId), 1);
      }
      $scope.overtime.refresh();
    }  

    $scope.overtime = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.overtime.tableState = tableState;
        $scope.overtime.refresh();
      },
      refresh: function () {
        console.log("refresh ot")
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox) {
            if (_this.tableState.all.checkbox[0].status == "") {
              delete this.tableState["all"];
            }
          }
        }
        _this.tableState.groupEventIds = $scope.eventsChecked
        _this.selected = [];
        _this.isSelectAll = false;
        _this.isLoading = true;
        apiService.get('OverTime/GetOvertimeForManager', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
        menuService.updateMenu();
      },
      selectAll: function (collection) {
        var _this = this;
        if (this.tableState.all) {
          if (_this.tableState.all.checkbox[0].status == "") {
            delete this.tableState["all"];
          }
        }
        if (_this.selected.length === 0) {
          angular.forEach(collection, function (val) {
            if (val.status == 0) {
              _this.selected.push(val.id);
              val.checked = true;
            }
          });
          _this.isSelectAll = true;
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length && !_this.isSelectAll) {
          angular.forEach(collection, function (val) {
            var found = _this.selected.indexOf(val.id);
            if (found == -1) _this.selected.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        }
        else if (_this.selected.length > 0 && _this.selected.length != _this.tableData.length && _this.isSelectAll) {
          _this.selected = [];
          _this.isSelectAll = false;
        }
        else {
          _this.selected = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selected.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selected.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selected.push(id);
          var count = 0;
          angular.forEach(this.tableData, function (val) {
            if (val.status == 0) {
              count++;
            }
          });
          if (this.selected.length === count) {
            this.isSelectAll = true;
          }
        }
      },
      approve: function (userId) {
        var dataPost = {
          "ids": [
            userId
          ]
        }
        popupService.openApprovePopup(this, 'OverTime/ApprovalOverTime', dataPost)
      },
      reject: function (userId) {
        var dataPost = {
          "ids": [
            userId
          ]
        }
        popupService.openRejectPopup(this, 'OverTime/RejectOverTime', dataPost)
      },
      approveRejectAll: function (type) {
        var ids = this.selected;
        if (ids.length == 0) {
          notificationService.warningTranslate('PLEASE_CHOOSE_ONE_RECORD');
        }
        else {
          var dataPost = {
            "ids": ids
          }
          if (type == 'approve') {
            popupService.openApprovePopup(this, 'OverTime/ApprovalOverTime', dataPost)
          }
          else {
            popupService.openRejectPopup(this, 'OverTime/RejectOverTime', dataPost)
          }
        }
      },
      openDetails: function (item) {
        $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/attendance/overtime/widgets/overtimeDetail.html',
          size: 'md',
          controller: 'overtimeDetailsModalCtrl',
          resolve: {
            item: function () {
              return item;
            }
          }
        });

      }
    };
  }
})();