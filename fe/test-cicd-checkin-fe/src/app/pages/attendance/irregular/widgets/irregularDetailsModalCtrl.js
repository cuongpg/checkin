/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
    'use strict';


    angular.module('BlurAdmin.pages.attendance')
        .controller('irregularDetailsModalCtrl', irregularDetailsModalCtrl);


    /** @ngInject */
    function irregularDetailsModalCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, item, confirmService, $state) {
        $scope.details = {
            checkInTime: item.checkInTime,
            checkOutTime: item.checkOutTime,
            email: item.email,
            id: item.id,
            imageUrl: item.imageUrl,
            managerEmail: item.managerEmail,
            managerName: item.managerName,
            name: item.name,
            note: item.note,
            status: item.status,
            timeSubmit: item.timeSubmit,
            workDate: item.workDate,

            approve: function (userId) {
                var dataPost = {
                    "ids": [
                        userId
                    ]
                }
                confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_APPROVE', function () {
                    apiService.create('TimeKeeping/ApprovalAbnormalTimeKeeping', dataPost, function (response) {
                        $uibModalInstance.close();
                        $state.reload();
                    });
                });
            },
            reject: function (userId) {
                var dataPost = {
                    "ids": [
                        userId
                    ]
                }
                confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_REJECT', function () {
                    apiService.create('TimeKeeping/RejectAbnormalTimeKeeping', dataPost, function (response) {
                        $uibModalInstance.close();
                        $state.reload();
                    });
                });
            },
        };


    }
})();