/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.checkin')
    .controller('addFaceBookCtrl', addFaceBookCtrl);


  /** @ngInject */
  function addFaceBookCtrl($scope, apiService, $uibModalInstance, notificationService, $translate, config, loaderService, datas) {
    $scope.faceBookLink = "";
    $scope.message = $scope.$resolve.message;
    $scope.Save = function () {
      if (!$scope.faceBookLink) return notificationService.errorTranslate('FACEBOOK_CANNOT_NULL');
      else if ($scope.faceBookLink.indexOf('facebook.com') < 0) return notificationService.errorTranslate('WRONG_FORMAT_LINK_FACEBOOK')
      else {
        // notificationService.successTranslate('OK');
        var dataPost = {
          "value":  $scope.faceBookLink
        }
        apiService.create('Employee/DeclareFacebook', dataPost , function (response) {
            $uibModalInstance.close();
          }, function() {
           $uibModalInstance.close();
          });
        $uibModalInstance.close();
        datas.refresh();
      }
    }
    $scope.Cancel = function () {
      $translate("NOT_JOIN_FACEBOOK").then(function (messageTranslated) {
        var dataPost = {
          "value":  messageTranslated
        }
        apiService.create('Employee/DeclareFacebook', dataPost , function (response) {
          $uibModalInstance.close();
        }, function() {
         $uibModalInstance.close();
        });
      }, 
      function (translationId) {
       
      });
     
    }
  }
})();