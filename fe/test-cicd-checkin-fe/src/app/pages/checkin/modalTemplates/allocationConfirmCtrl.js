/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.checkin')
    .controller('allocationConfirmCtrl', allocationConfirmCtrl);


  /** @ngInject */
  function allocationConfirmCtrl($scope, apiService, $uibModalInstance, $interval, $uibModalStack) {
    $scope.isWarning = $scope.$resolve.isWarning;
    $scope.contact = $scope.$resolve.contact;
    $scope.email = $scope.$resolve.email.split("@")[0];
    $scope.time = $scope.$resolve.time;
    $scope.timeS = $scope.time * 1000;

    $scope.circleDasharray = ""
    $scope.pathFill = ""
    $scope.transform = ""
    $scope.getMonday = function (d) {
      d = new Date(d);
      var day = d.getDay(),
          diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
      return new Date(d.setDate(diff));
    }
    $scope.capitalizeFirstLetter = function (string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
  }
    $scope.firstDay = $scope.getMonday(new Date());
    $scope.interval = $interval(function () {
      // var data = {
      //   size: 200,
      //   sectors: [{
      //       percentage: (15 - $scope.time) / 15
      //     }
      //   ]
      // }
      // $scope.calculateSectors(data);
      $scope.time = $scope.time - 1;
      if ($scope.time === 0) {
        $scope.cancelInterval($scope.interval);
        $uibModalStack.dismissAll();
        $scope.$resolve.reportFunction.checkReportWrong();
      }
    }, 1000);
    $scope.cancelInterval = function (interval) {
      if (interval != undefined) {
        $interval.cancel(interval);
        interval = undefined;
      }
    }
    $scope.calculateSectors = function (data) {
      var l = data.size / 2
      var a = 0 // Angle
      var aRad = 0 // Angle in Rad
      var z = 0 // Size z
      var x = 0 // Side x
      var y = 0 // Side y
      var X = 0 // SVG X coordinate
      var Y = 0 // SVG Y coordinate
      var R = 0 // Rotation
      var sector = {}
      data.sectors.map(function (item, key) {
        a = 360 * item.percentage;
        var aCalc = (a > 180) ? 360 - a : a;
        var arcSweep = 0;
        aRad = aCalc * Math.PI / 180;
        z = Math.sqrt(2 * l * l - (2 * l * l * Math.cos(aRad)));
        if (aCalc <= 90) {
          x = l * Math.sin(aRad);
        } else {
          x = l * Math.sin((180 - aCalc) * Math.PI / 180);
        }
        y = Math.sqrt(z * z - x * x);
        Y = y;

        if (a <= 180) {
          X = l + x;
          arcSweep = 0;
        } else {
          X = l - x;
          arcSweep = 1;
        }
        sector = {
          percentage: item.percentage,
          arcSweep: arcSweep,
          L: l,
          X: X,
          Y: Y,
          R: R,
          K: a < 180 ? 0 : 1
        }
        R = R + a;
      })
      $scope.circleDasharray = "15,3"
      $scope.pathFill = 'M' + sector.L + ',' + sector.L + ' L' + sector.L + ',0 A' + sector.L + ',' + sector.L + ' 1 ' + sector.K +' ,1 ' + sector.X + ', ' + sector.Y + ' z';
      $scope.transform = 'rotate(' + sector.R + ', '+ sector.L+', '+ sector.L+')';
      // console.log(a)
    }
  }
})();