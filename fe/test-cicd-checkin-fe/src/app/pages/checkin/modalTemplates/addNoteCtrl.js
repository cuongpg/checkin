/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.checkin')
    .controller('AddNoteCtrl', AddNoteCtrl);


  /** @ngInject */
  function AddNoteCtrl($scope ,apiService, $uibModalInstance, notificationService, config, loaderService) {
    $scope.isSavingNote = false;
    $scope.noteData = {
      note: ""
    };
    $scope.saveNote = function () {
      $scope.isSavingNote = true;
      apiService.create('TimeKeeping/AddNote', $scope.noteData, function (response) {
        $scope.isSavingNote = false;
        notificationService.successTranslate(response.data);
        $uibModalInstance.close();
      }, function() {
        $scope.isSavingNote = false;
      }, true);
    }
    
  }
})();