/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.checkin')
    .controller('notifiesCheckinCtrl', notifiesCheckinCtrl);


  /** @ngInject */
  function notifiesCheckinCtrl($scope, $state, apiService, $uibModalInstance, notificationService, details, loaderService, notifies, hasOverTime, currentLocaltion) {
    $scope.isSavingNote = false;
    $scope.noteData = {
      note: ""
    };

    $scope.noti = {
      selectedEvent: null,
      reason: "",
      groupEventSelected: null,
    }
    $scope.event = [];
    $scope.groupEvent = [];
    $scope.showNotify = notifies;
    $scope.hasOverTime = hasOverTime;
    $scope.tempEvent = [];

    $scope.refresh = function () {
      if($scope.hasOverTime)
      {
        apiService.get('Event/GetGroupEvents', null, function (response) {
          if (response.data != null) {
            $scope.groupEvent = response.data;
              $scope.noti.groupEventSelected = $scope.groupEvent[0];
          }
        });
      }
    };

    var getEvent = function (key) {
      apiService.get('Event/GetEvents' + '?groupId=' + $scope.noti.groupEventSelected.id + '&keyWord=' + key + '&perPage=100', null, function (response) {
        $scope.event = [];
        if (response.data.data != null) {
          angular.forEach(response.data.data, function (val) {
            var eventItem = { 'id': val.id, 'eventCode': val.eventCode, 'description': val.description, 'percentage': val.percentage, 'payPerBlock': val.payPerBlock, 'payPerHour': val.payPerHour };
            $scope.event.push(eventItem);
          });
        }
        if(!key)
        {
          $scope.tempEvent = $scope.event;
        }
      });
    };

    $scope.onChangeEventsGroup = function () {
      $scope.noti.selectedEvent = null;
      if ($scope.noti.groupEventSelected.inputEventCode) {
        getEvent("");
      }
    };

    $scope.confirm = function () {
      if($scope.hasOverTime)
      {
        if (!$scope.noti.selectedEvent) {
          if ($scope.noti.groupEventSelected.inputEventCode) {
            return notificationService.errorTranslate("EVENT_CAN_NOT_NULL");
          }
          else {
            apiService.create('TimeKeeping/CheckOut' + '?eventId=""' + '&overTimeNode=' + $scope.noti.reason, currentLocaltion, function (response) {
              details.checkable = false;
              details.getStatusTimeKeeping();
              $scope.$dismiss();
            });
          }
        }
        else {
          apiService.create('TimeKeeping/CheckOut' + '?eventId=' + $scope.noti.selectedEvent.id + '&overTimeNode=' + $scope.noti.reason, currentLocaltion, function (response) {
            details.checkable = false;
            details.getStatusTimeKeeping();
            $scope.$dismiss();
          });
        }
      }
      else
      {
        apiService.create('TimeKeeping/CheckOut'+'?overTimeNode=' + $scope.noti.reason, currentLocaltion, function (response) {
          details.checkable = false;
          details.getStatusTimeKeeping();
          details.refresh();
          $scope.$dismiss();
        });
      }

    };



    $scope.refreshEvent = function (input) {
      if (input)
        getEvent(input);
      else $scope.event = $scope.tempEvent;
    };
    $scope.refresh();

  }
})();