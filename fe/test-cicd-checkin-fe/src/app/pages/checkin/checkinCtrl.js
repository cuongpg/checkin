(function () {
  'use strict';

  angular.module('BlurAdmin.pages.checkin')
    .controller('CheckinCtrl', CheckinCtrl);

  /** @ngInject */
  function CheckinCtrl($scope, apiService, confirmService, $interval, $filter, notificationService, $translate, $q, $timeout, $uibModal, loaderService, menuService) {
    $scope.click = true
    $scope.enableClick = false;
    $scope.showCheckIn = true;
    $scope.checkText = "CHECK IN";
    $scope.internetStatus = $translate('CONNECTING_VALID');
    $translate('CONNECTING_VALID').then(function (translated) {
      $scope.internetStatus = translated;
    }).catch(function (err) { });
    $scope.internetStatusStyle = {
      "color": '#3d5170'
    };
    $scope.checkinStyle = {
      "color": "#DBA147"
    };
    $scope.distanceToCompany = "";
    $scope.currentLocaltion = {
      longitude: 0,
      latitude: 0,
    };
    $scope.checkable = true;
    $scope.verifyClient = true;
    $scope.checkAllocationPopup = false;
    $scope.checkReportWrongPopup = false;
    $scope.checkFaceBookPopup = false;
    $scope.popupMessage = "";
    var topicaPositionConfig = {
      radius: 0,
      location: {
        longitude: 0,
        latitude: 0
      },
      address: ""
    };
    var statusTimeKeeping = {
      checkin: 0,
      checkout: 1,
      done: 2
    }

    $scope.openPopup = function (page, size) {
      $uibModal.open({
        animation: true,
        templateUrl: page,
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          datas: function () {
            return $scope;
          }
        }
      });
    };

    var ModalInstanceCtrl = function ($scope) {
      $scope.no = function () {
        $uibModalStack.dismissAll();
      };
    };

    $scope.checkMessagePopup = function () {
      apiService.get('Employee/CheckPopUpStatus?Longitude=' + $scope.currentLocaltion.longitude + '&Latitude=' + $scope.currentLocaltion.latitude, null, function (response) {
        if (response.data.result) {
          var results = response.data.result;
          angular.forEach(results, function (item) {
            if (item == "Allocation") {
              $scope.checkAllocationPopup = true;
            }
            if (item == "OT") {
              $scope.checkReportWrongPopup = true;
            }
            if(item == "Facebook"){
              $scope.checkFaceBookPopup = true;
            }
            if(item == "ThongBao"){
              $scope.checkThongBaoPopup = true;
              $scope.popupMessage = response.data.message;
              $scope.checkThongBao();
            }
          });
          $scope.popupMessage = response.data.message
          $scope.getStatusTimeKeeping();
        } else {
          scope.getStatusTimeKeeping();
        }
      },
        function () { });
    }
    $scope.checkReportWrong = function () {
      if ($scope.checkReportWrongPopup) {
        apiService.get('Employee/IsNeedConfirmByEmployeeV2', null, function (response) {
          if (response.data) {
            loaderService.show();
            var item = {
              isAdmin: false
            }
            $uibModal.open({
              animation: true,
              backdrop: 'static',
              templateUrl: 'app/pages/reportWrong2/reportWrong2.html',
              controller: 'ReportWrong2Ctrl',
              size: 'lg',
              windowClass: 'app-modal-window',
              resolve: {
                employee: function () {
                  return item;
                },
                parent: function () {
                  return $scope;
                }
              }
            });
          }
          else $scope.checkFacebook();
        }, function () {
          $scope.checkFacebook(); 
        });
      }
      else {
        $scope.checkFacebook();
      }
    }
    $scope.checkAllocation = function () {
      if ($scope.checkAllocationPopup) {
        apiService.get('Employee/Allocation', null, function (response) {
          if (response.data) {
            if (response.data.type === "Warning" || response.data.type === "Error") {
              $scope.modalAllocation = $uibModal.open({
                animation: true,
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'app/pages/checkin/modalTemplates/allocationConfirmModal.html',
                controller: 'allocationConfirmCtrl',
                size: 'lg',
                resolve: {
                  time: function () {
                    return response.data.times ? response.data.times : 0;
                  },
                  isWarning: function () {
                    return response.data.type === "Warning";
                  },
                  contact: function () {
                    return response.data.contact;
                  },
                  email: function () {
                    return response.data.userEmail;
                  },
                  reportFunction: function () {
                    return $scope;
                  }
                }
              });
            } else {
              $scope.checkReportWrong();
            }
          } else {
            $scope.checkReportWrong();
          }
        },
        function () {
          $scope.checkReportWrong();
        })
      }
      else {
        $scope.checkReportWrong();
      }
    }
    $scope.refresh = function () {
      loaderService.show();
      $scope.cancelInterval($scope.interval);
      if (navigator.geolocation) {
        function success(position) {
          $scope.confirmPermision = true;
          $scope.currentLocaltion.longitude = position.coords.longitude;
          $scope.currentLocaltion.latitude = position.coords.latitude;
          $scope.checkMessagePopup();
        }

        function error() {
          $scope.confirmPermision = true;
          $scope.currentLocaltion.longitude = 0;
          $scope.currentLocaltion.latitude = 0;
          $scope.checkMessagePopup();
        }

        var options = {
          enableHighAccuracy: true,
          timeout: 2000
        };

        setTimeout(function () {
          navigator.geolocation.getCurrentPosition(success, error, options);
        }, 1000);

      } else {
        $scope.cancelInterval($scope.intervalGetTimkeeping);
        $scope.currentLocaltion.longitude = 0;
        $scope.currentLocaltion.latitude = 0;
        $scope.checkMessagePopup();
      }
      menuService.updateMenu();
    }

    $scope.watchPosition = function () {
      if (navigator.geolocation) {
        //watch position
        navigator.geolocation.watchPosition(function (position) {
          $scope.currentLocaltion.longitude = position.coords.longitude;
          $scope.currentLocaltion.latitude = position.coords.latitude;
          if (topicaPositionConfig.radius != 0) {
            var isInTopica = $scope.arePointsNear($scope.currentLocaltion, topicaPositionConfig.location, topicaPositionConfig.radius);
            if (!$scope.verifyClient) {
              //nằm ngoài phạm vi 200m
              if (!isInTopica) {
                $scope.cancelInterval($scope.intervalGetTimkeeping);
                $scope.internetStatus = $translate('POSSITION_NOT_VALID');
                $translate('POSSITION_NOT_VALID').then(function (translated) {
                  $scope.internetStatus = translated;
                }).catch(function (err) { });
                $scope.internetStatusStyle = {
                  "color": "red"
                };
                $scope.checkinStyle = {
                  "color": "red"
                };
              }
              //nằm trong phạm vi 
              else {
                //nếu mạng ko hợp lệ và interval không tồn tại => chạy interval getStatusTimekeeping
                if ($scope.intervalGetTimkeeping == undefined) {
                  $scope.intervalGetTimkeeping = $interval(function () {
                    $scope.getStatusTimeKeeping();
                  }, 10000);
                }
              }
            } else {
              $scope.cancelInterval($scope.intervalGetTimkeeping);
            }
          }
        }, function () {
          $scope.currentLocaltion.longitude = 0;
          $scope.currentLocaltion.latitude = 0;
          $scope.cancelInterval($scope.intervalGetTimkeeping);
        });
      }
    }

    $scope.getStatusTimeKeeping = function () {
      apiService.create('TimeKeeping/GetStatusTimeKeeping', $scope.currentLocaltion, function (response) {
        $scope.enableClick = true
        // $scope.getCurrentEstimateOwner();
        $scope.scriptName = response.data.scriptName;
        $scope.script = "Kịch bản " + response.data.subScript + " - " + response.data.scriptDescription;
        $translate('SCRIPT').then(function (translated) {
          $scope.script = translated + " " + response.data.subScript + " - " + response.data.scriptDescription;
        }).catch(function (err) { });
        topicaPositionConfig.location.longitude = response.data.location.longitude;
        topicaPositionConfig.location.latitude = response.data.location.latitude;
        topicaPositionConfig.address = response.data.location.address;
        topicaPositionConfig.radius = response.data.radius;
        $scope.arePointsNear($scope.currentLocaltion, topicaPositionConfig.location, topicaPositionConfig.radius);
        if (response.data.verifyClient) {
          $scope.verifyClient = true;
          $scope.cancelInterval($scope.intervalGetTimkeeping);
          $scope.internetStatus = $translate('CONNECTING_VALID');
          $translate('CONNECTING_VALID').then(function (translated) {
            $scope.internetStatus = translated;
          }).catch(function (err) { });
          $scope.internetStatusStyle = {
            "color": "#333333"
          };
          $scope.checkinStyle = {
            "color": "#DBA147"
          };
        } else {
          $scope.verifyClient = false;
          $scope.internetStatus = $translate('POSSITION_NOT_VALID');
          $translate('POSSITION_NOT_VALID').then(function (translated) {
            $scope.internetStatus = translated;
          }).catch(function (err) { });
          $scope.internetStatusStyle = {
            "color": "red"
          };
          $scope.checkinStyle = {
            "color": "red"
          };
        }
        //chua cham cong hoac con luot cham cong
        if (response.data.statusTimeKeeping.status == statusTimeKeeping.checkin) {
          $scope.checkText = "CHECK IN";
          $scope.countTime = null;
          $scope.checkable = true;
          $scope.showCheckIn = true;
          $scope.checkAllocation();
        }
        // da checkin, chua checkout
        else if (response.data.statusTimeKeeping.status == statusTimeKeeping.checkout) {
          $scope.checkReportWrong();
          $scope.startCountTime(response.data.statusTimeKeeping.substractTime * 1000);
          $scope.checkable = true;
          $scope.showCheckIn = true;
        }
        // hoan thanh cham cong
        else {
          $scope.checkReportWrong();
          $scope.checkable = false;
          $scope.showCheckIn = false;
          $scope.checkResult = $scope.CalculateDate(response.data.statusTimeKeeping.substractTime * 1000);
        }
      }, function () {
        $scope.enableClick = true
        $scope.checkAllocation();
        $scope.verifyClient = false;
        topicaPositionConfig.location.longitude = 0;
        topicaPositionConfig.location.latitude = 0;
        topicaPositionConfig.address = "";
        topicaPositionConfig.radius = 0;
      }, true);
    }

    $scope.arePointsNear = function (checkPoint, centerPoint, met) {
      var ky = 40000 / 360;
      var kx = Math.cos(Math.PI * centerPoint.latitude / 180.0) * ky;
      var dx = Math.abs(centerPoint.longitude - checkPoint.longitude) * kx;
      var dy = Math.abs(centerPoint.latitude - checkPoint.latitude) * ky;
      var distance = Math.sqrt(dx * dx + dy * dy) * 1000;
      $scope.distanceToCompany = "Bạn đang cách Topica " + topicaPositionConfig.address + " " + $filter('number')(distance, 2) + "m";
      return distance <= met;
    }

    $scope.cancelInterval = function (interval) {
      if (interval != undefined) {
        $interval.cancel(interval);
        interval = undefined;
      }
    }

    $scope.CalculateDate = function (opreation) {
      var diff = Math.floor(opreation);
      var secs = Math.floor(diff / 1000);
      var mins = Math.floor(secs / 60);
      var hours = Math.floor(mins / 60);
      var days = Math.floor(hours / 24);
      var months = Math.floor(days / 31);
      var years = Math.floor(months / 12);
      months = Math.floor(months % 12);
      days = Math.floor(days % 31);
      hours = Math.floor(hours % 24);
      mins = Math.floor(mins % 60);
      secs = Math.floor(secs % 60);
      var message = "";
      if (days <= 0) {
        message += (hours < 10 && hours >= 0 ? "0" + hours : hours) + ":";
        message += (mins < 10 && mins >= 0 ? "0" + mins : mins) + ":";
        message += (secs < 10 && secs >= 0 ? "0" + secs : secs) + "";
      } else {
        if (years > 0) {
          message += years + " years ";
        }
        if (months > 0 || years > 0) {
          message += months + " months ";
        }
        message += days + " days";
      }
      return message
    };

    $scope.startCountTime = function (timeStamp) {
      $scope.checkText = "CHECK OUT";
      var markDate = new Date($filter('date')(new Date(), "yyyy/MM/dd HH:mm:ss"));
      var currentDate = new Date($filter('date')(new Date(), "yyyy/MM/dd HH:mm:ss"));
      $scope.countTime = $scope.CalculateDate(timeStamp + currentDate.getTime() - markDate.getTime());
      $scope.interval = $interval(function () {
        currentDate = new Date($filter('date')(new Date(), "yyyy/MM/dd HH:mm:ss"));
        $scope.countTime = $scope.CalculateDate(timeStamp + currentDate.getTime() - markDate.getTime());
      }, 1000);
    }

    $scope.indexOfbyKey = function (obj_list, key, value) {
      for (index in obj_list) {
        if (obj_list[index][key] === value) return index;
      }
      return -1;
    }

    $scope.onCheck = function () {
      if ($scope.enableClick) {
        if ($scope.checkable) {
          $scope.enableClick = false
          //dang check in, an check out
          if ($scope.checkText == "CHECK OUT") {
            //confirm box check out
            apiService.create('TimeKeeping/GetCheckOutValidCode', $scope.currentLocaltion, function (response) {
              $scope.enableClick = true
              var _data = response.data;
              var _body = [];
              var loopPromises = [];
              if (_data.statusAllowSkips.length != 0) {
                angular.forEach(_data.statusAllowSkips, function (value) {
                  var deferred = $q.defer();
                  loopPromises.push(deferred.promise);
                  $translate(value).then(function (translated) {
                    var notify = "- " + translated;
                    if (value == 'TIMEKEEPING_HAS_OVER_TIME') {
                      notify += " " + $scope.CalculateDate(new Date(_data.overTimeSeconds * 1000).getTime())
                    }
                    _body.push(notify);
                    deferred.resolve(translated);
                  }).catch(function (err) {
                    deferred.reject(err);
                  });
                });
              } else {
                var deferred = $q.defer();
                loopPromises.push(deferred.promise);
                deferred.resolve();
              }
              $q.all(loopPromises).then(function (data) {
                var uibModalInstance = $uibModal.open({
                  animation: true,
                  templateUrl: 'app/pages/checkin/modalTemplates/notifiesCheckinModal.html',
                  controller: 'notifiesCheckinCtrl',
                  size: 'md',
                  resolve: {
                    details: function () {
                      return $scope;
                    },
                    notifies: function () {
                      return _body;
                    },
                    hasOverTime: function () {
                      return _data.hasOverTime;
                    },
                    currentLocaltion: function () {
                      return $scope.currentLocaltion;
                    }
                  }
                });
              });
            }, function (response) {
              $scope.getStatusTimeKeeping();
            }, true)
          } else {
            apiService.create('TimeKeeping/GetCheckInValidCode', $scope.currentLocaltion, function (response) {
              $scope.enableClick = true
              var _data = response.data;
              var loopPromises = [];
              var _body = [];
              if (_data.length != 0) {
                angular.forEach(_data, function (value) {
                  var deferred = $q.defer();
                  loopPromises.push(deferred.promise);
                  $translate(value).then(function (translated) {
                    var notify = "- " + translated;
                    _body.push(notify)
                    deferred.resolve(translated);
                  }).catch(function (err) {
                    deferred.reject(err);
                  });
                });
                $q.all(loopPromises).then(function (data) {
                  $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/checkin/modalTemplates/reasonModal.html',
                    controller: reasonCtrl,
                    size: 'md',
                    resolve: {
                      datas: function () {
                        return $scope;
                      },
                      body: function () {
                        return _body;
                      }
                    }
                  });
                });
              } else {
                apiService.create('TimeKeeping/CheckIn', $scope.currentLocaltion, function (response) {
                  $scope.checkText = "CHECK OUT";
                  $scope.getStatusTimeKeeping();
                });
              }
            }, function (response) {
              $scope.getStatusTimeKeeping();
            }, true);
          }
        } else {
          notificationService.error('COMPLETE_TIME');
          $scope.enableClick = true
        }
      } else {
        notificationService.error('DISABLE');
      }
    }

    var reasonCtrl = function ($scope, datas, body) {
      $scope.datas = datas;
      $scope.body = body;
      $scope.note = '';
      $scope.checkin = function () {
        var note = encodeURIComponent($scope.note)
        apiService.create('TimeKeeping/CheckIn' + '?checkinNote=' + note, datas.currentLocaltion, function (response) {
          datas.checkText = "CHECK OUT";
          datas.getStatusTimeKeeping();
          $scope.$dismiss();
        });
      }
      $scope.no = function () {
        $uibModalStack.dismissAll();
      };
    };
    $scope.showNoteModal = function () {
      var uibModalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/checkin/modalTemplates/addNoteModal.html',
        controller: 'AddNoteCtrl',
        size: 'md',

      });
    }

    $scope.checkFacebook = function () {
      if ($scope.checkFaceBookPopup) {
        //API gọi check thêm facebook start
        apiService.get('Employee/IsNeedDeclareFacebook', null, function (response) {
          if (response.data) {

            var uibModalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'app/pages/checkin/modalTemplates/addFaceBookModal.html',
              backdrop: 'static',
              keyboard: false,
              controller: 'addFaceBookCtrl',
              size: 'md',
              resolve: {
                datas: function () {
                  return $scope;
                }
              }
            });
            //API gọi check thêm facebook end
          }
        })
      }
    }
    $scope.checkThongBao = function () {
      if ($scope.checkThongBaoPopup) {
        //API gọi check thêm facebook start
            var uibModalInstance = $uibModal.open({
              animation: true,
              templateUrl: 'app/pages/checkin/modalTemplates/notifyUT.html',
              backdrop: 'static',
              keyboard: false,
              controller: 'addFaceBookCtrl',
              size: 'md',
              resolve: {
                message: function(){
                    return $scope.popupMessage;
                  },
                datas: function () {
                  return $scope;
                }
              }
            });
            //API gọi check thêm facebook end
      }
    }
    $scope.refresh();
    $scope.watchPosition();
  }
})();