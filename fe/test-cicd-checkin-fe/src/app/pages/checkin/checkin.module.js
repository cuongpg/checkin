/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.checkin', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('checkin', {
        url: '/checkin',
        templateUrl: 'app/pages/checkin/checkin.html',
        title: 'TIMEKEEPING',
        controller: 'CheckinCtrl',
        sidebarMeta: {
          icon: 'fa fa fa-bank',
          order: 1,
        },
        permission: ['TimeKeeping.GetStatusTimeKeeping']
      });
  }

})();
