(function () {
  'use strict';

  angular.module('BlurAdmin.pages.feedback')
    .controller('feedbackCtrl', feedbackCtrl);

  /** @ngInject */
  function feedbackCtrl($scope, $window, $cookies) {
    // $window.open('https://docs.google.com/forms/d/e/1FAIpQLSeRCn28wQQxkbMSRacj2DJx7WpFbiI6qQfG_Lvjkm3o0lTqig/viewform', '_blank');
    $scope.lang_cookie = $cookies.get('NG_TRANSLATE_LANG_KEY');
  }
})();