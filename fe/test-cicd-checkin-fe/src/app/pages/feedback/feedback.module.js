/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.feedback', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('feedback', {
        url: '/feedback',
        templateUrl: 'app/pages/feedback/feedback.html',
        title: 'FEEDBACK',
        controller: 'feedbackCtrl',
        sidebarMeta: {
          icon: 'ion-star',
          order: 10,
        },
      });
  }


})();