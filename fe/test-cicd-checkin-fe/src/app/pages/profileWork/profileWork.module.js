/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.profileWork', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('profileWork', {
        url: '/profileWork',
        title: 'PROFILE_WORK',
        sidebarMeta: {
          icon: 'fa fa-user',
          order: 1,
        },
        templateUrl: 'app/pages/profileWork/profileWork.html',
        controller: 'ProfileWorkPageCtrl',
        // 'permission': 'Flows.GetAll'
      });
  }

})();
