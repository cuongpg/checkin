(function () {
  'use strict';

  angular.module('BlurAdmin.pages.profileWork')
    .controller('ProfileWorkPageCtrl', ProfileWorkPageCtrl);

  /** @ngInject */
  function ProfileWorkPageCtrl($scope, apiService) {
    $scope.profile = {
      responseData : {},
      personalInformation : [
        {
          title: 'USER_INFO',
          icon: 'face',
          data: [
            {
              title: 'EMPLOYEE_DOB',
              key: 'employee_dob'
            },
            {
              title: 'EMPLOYEE_GENDER',
              key: 'employee_gender'
            }
          ]
        },
        {
          title: 'USER_CONTACT_INFO',
          icon: 'cellphone-link',
          data: [
            {
              title: 'EMPLOYEE_EMAIL',
              key: 'employee_email'
            },
            {
              title: 'EMPLOYEE_NOTIFY_TEL',
              key: 'employee_notify_tel'
            }
          ]
        },
        {
          title: 'USER_ADDRESS_INFO',
          icon: 'earth-box',
          data: [
            {
              title: 'EMPLOYEE_ADDRESS',
              key: 'employee_address'
            },
            {
              title: 'EMPLOYEE_CURRENT_ADDRESS',
              key: 'employee_current_address'
            }
          ]
        },
        {
          title: 'USER_OTHER_INFO',
          icon: 'layers-outline',
          data: [
            {
              title: 'EMPLOYEE_IDENTIFICATION',
              key: 'employee_identification'
            }
          ]
        }
      ],
      jobInformation : [
        {
          title: 'USER_CONTACT_INFO',
          icon: 'cellphone-link',
          data: [
            {
              title: 'EMPLOYEE_PHONE',
              key: 'employee_phone_number'
            },
            {
              title: 'EMAIL',
              key: 'employee_work_email'
            }
          ]
        },
        {
          title: 'LEVEL_INFO',
          icon: 'google-analytics',
          data: [
            {
              title: 'EMPLOYEE_LEVEL',
              key: ['statements', 'employee_level']
            }
          ]
        },
        {
          title: 'SUPERVISOR_INFO',
          icon: 'incognito',
          data: [
            {
              title: 'EMPLOYEE_SUPERVISOR',
              key: ['statements', 'employee_supervisor']
            }
          ]
        },
        {
          title: 'GROUP_INFO',
          icon: 'lan-connect',
          data: [
            {
              title: 'EMPLOYEE_GROUP',
              key: ['statements', 'estimation_owner']
            },
            {
              title: 'PB',
              key: ['statements', 'department']
            }
          ]
        },
        {
          title: 'OTHER_INFO',
          icon: 'layers-outline',
          data: [
            {
              title: 'HRB_STATUS',
              key: ['statements', 'hrb_status']
            },
            {
              title: 'EMPLOYEE_CORPORATION',
              key: ['statements', 'employee_corporation']
            }
          ]
        }
      ],
      init: function () {
        var _this = this;
        apiService.get('Employee', null, function (response) {
          _this.responseData = response.data;
        });
      },
      checkTypeKey : function (key)
      {
        return typeof key === 'string'
      }
    }
  }

})();
