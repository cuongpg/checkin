/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reportwrong2', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('reportWrong2', {
        url: '/reportWrong2',
        title: 'REPORT_WRONG',
        sidebarMeta: {
          icon: 'fa fa-user',
          order: 1,
        },
        templateUrl: 'app/pages/reportWrong2/reportWrong2.html',
        controller: 'ReportWrong2Ctrl',
        'permission': 'Employee.GetMechanismInformationV2'
      });
  }

})();
