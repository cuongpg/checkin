(function () {
    'use strict';

    angular.module('BlurAdmin.pages.reportwrong2')
        .controller('ReportWrong2Ctrl', ReportWrong2Ctrl);

    /** @ngInject */
    function ReportWrong2Ctrl($scope, $window, $uibModal, config, apiService, popupService, $timeout, notificationService, $filter, $translate, $cookies, $uibModalStack) {
      
        var vm = this;
        $scope.dataExport = {
            date: new Date(),
            startDate: null,
            endDate: null
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        }
        $scope.datePopup = {
            monthOpend: false,
            startDateOpend: false,
            endDateOpend: false
        };
        $scope.onDateClick = function () {
            $scope.datePopup.monthOpend = true;
        };
        $scope.changeMonth = function () {
            $scope.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date($scope.dataExport.date.getFullYear(), $scope.dataExport.date.getMonth() + 1, 0),
                minDate: new Date($scope.dataExport.date.getFullYear(), $scope.dataExport.date.getMonth(), 1),
                startingDay: 1
            }
            $scope.dataExport.startDate = $scope.dateOptions.minDate;
            $scope.dataExport.endDate = $scope.dateOptions.maxDate;
            vm.refresh();
        }
       
        vm.init = function () {
            apiService.get('Script/GetCutoffTimeKeepingDay', null, function (response) {
                var cutoffTimeKeepingDay = 4;
                vm.recordId = "";
                vm.userName = "";
                vm.employeeCode = "";
                vm.email = "";
                vm.employee = null;
                vm.longTermPerct = 100;
                vm.isForAdmin = false;
                if ($scope.$resolve) {
                    vm.employee = $scope.$resolve.employee ? $scope.$resolve.employee : null;
                    vm.isForAdmin = $scope.$resolve.employee ? $scope.$resolve.employee.isAdmin : false;
                }
                vm.isReport = false;
                vm.isGetDataOk = false;
                vm.language = $cookies.get('NG_TRANSLATE_LANG_KEY')
                vm.mechanisCurrKey = ''
                vm.mechanisCurrValue = ''
                if (vm.language === 'vi') {
                    vm.mechanisCurrKey = 'Cơ chế KETRAPHAKY chuẩn'
                    vm.mechanisCurrValue = 'Có OT'
                } else if (vm.language === 'en') {
                    vm.mechanisCurrKey = 'Standard KETRAPHAKY mechanism'
                    vm.mechanisCurrValue = 'Overtime mechanism'
                } else {
                    vm.mechanisCurrKey = 'กลไก KETRAPHAKY มาตรฐาน'
                    vm.mechanisCurrValue = 'กลไกแบบอิงหน่วย'
                }
                vm.width = $window.innerWidth;
                vm.height = $window.innerHeight;
                vm.firstDay = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
                if(new Date().getDate() < cutoffTimeKeepingDay){
                    vm.firstDay.setMonth(vm.firstDay.getMonth() - 1);
                    vm.currentDate  =  new Date(vm.firstDay.getFullYear(), vm.firstDay.getMonth() + 1, 0);
                }else{
                    vm.currentDate = new Date(new Date().toISOString().slice(0, 10));
                }
                vm.endDay  = new Date(vm.firstDay.getFullYear(), vm.firstDay.getMonth() + 1, 0);
                vm.timeNow = '';
                vm.isEdit = false;
                vm.oldProjectList = [];
                vm.projectListFullDataOld = [];
                vm.oldResponse = {};
                vm.overtimeTime = 0;
                vm.infoData = [];
                vm.longtermData = [];
                vm.projectData = []
                vm.salaryData = []
                vm.card = {}
                vm.listLevelSelected = []
                vm.track2Selected = []
                vm.track1Selected = []
                vm.track2SelectedFullData = []
                vm.track1SelectedFullData = []
                vm.orgUnitListLv2 = []
                vm.orgUnitListLv3 = []
                vm.orgUnitListLv4 = []
                vm.orgUnitListLv5 = []
                vm.orgUnitListLv2FullData = []
                vm.orgUnitListLv3FullData = []
                vm.orgUnitListLv4FullData = []
                vm.orgUnitListLv5FullData = []
                vm.projectInformationsSelected = []
                vm.COMMechanismListSelected = []
                vm.projectsListSelectedFullData = []
                vm.COMMechanismListSelectedFullData = []
                vm.departmentRoleDataSelected = []
                vm.ptListDataSelected = []
                vm.ptListDataFullData = []
                vm.projectEmpty = {
                    isNew: false,
                    lineDisable: false,
                    name: {
                        isHide: false,
                        title: {
                            class: "col-md-2",
                            titleBold: 'THE_PROJECT_IS_PARTICIPATING',
                            titleTranslate: $translate.instant('THE_PROJECT_IS_PARTICIPATING').replace('<br/>', ''),
                        },
                        autoKey: ['PROJ_TYPE_NAME', 'PERNR', 'startDate', 'endDate', 'projectType'],
                        disable: false,
                        class: "col-md-2",
                        value: {
                            key: 0,
                            value: ''
                        },
                        data: 'projectInformationsSelected',
                        type: 'select'
                    },
                    projectType: {
                        isHide: false,
                        title: {
                            class: "col-md-2",
                            titleBold: 'PROJECT_TYPE',
                            titleTranslate: $translate.instant('PROJECT_TYPE').replace('<br/>', ''),
                        },
                        disable: true,
                        class: "col-md-2",
                        value: {
                            key: 0,
                            value: ''
                        },
                        data: '[]',
                        type: 'select'
                    },
                    PERNR: {
                        isHide: false,
                        title: {
                            class: "col-md-1",
                            titleBold: 'REPORT_TO',
                            titleTranslate: $translate.instant('REPORT_TO').replace('<br/>', ''),
                        },
                        disable: true,
                        class: "col-md-1",
                        value: '',
                        type: 'input'
                    },
                    DEPARTMENT_ROLE: {
                        isHide: false,
                        title: {
                            class: "col-md-1",
                            titleBold: 'DEPARTMENT_ROLE',
                            titleTranslate: $translate.instant('DEPARTMENT_ROLE').replace('<br/>', ''),
                        },
                        disable: false,
                        class: "col-md-1",
                        value: {
                            key: 0,
                            value: '',
                        },
                        data: 'departmentRoleDataSelected',
                        type: 'select'
                    },
                    perct: {
                        isHide: false,
                        title: {
                            class: "col-md-1",
                            titleBold: 'PERCENTAGE',
                            titleTranslate: $translate.instant('PERCENTAGE').replace('<br/>', ''),
                        },
                        disable: false,
                        class: "col-md-1",
                        value: 0,
                        type: 'number'
                    },
                    startDate: {
                        isHide: false,
                        title: {
                            class: "col-md-1",
                            titleBold: 'PARTICIPATING_TIME',
                            titleTranslate: $translate.instant('PARTICIPATING_TIME').replace('<br/>', ''),
                        },
                        disable: false,
                        class: "col-md-1",
                        value: vm.currentDate,
                        type: 'date',
                        isOpen: false,
                        validate: {
                            maxDate: vm.currentDate,
                            minDate: vm.currentDate
                        }
                    },
                    endDate: {
                        isHide: false,
                        title: {
                            class: "col-md-1",
                            titleBold: 'END_TIME',
                            titleTranslate: $translate.instant('END_TIME').replace('<br/>', ''),
                        },
                        disable: false,
                        class: "col-md-1",
                        value: vm.currentDate,
                        type: 'date',
                        isOpen: false,
                        validate: {
                            maxDate: vm.currentDate,
                            minDate: vm.currentDate
                        }
                    },
                    PROJ_TYPE_NAME: {
                        isHide: false,
                        title: {
                            class: "col-md-2",
                            titleBold: 'VOLUNTARY_MECHANISM',
                            titleTranslate: $translate.instant('VOLUNTARY_MECHANISM').replace('<br/>', ''),
                        },
                        disable: true,
                        class: "col-md-2",
                        value: {
                            key: 0,
                            value: '',
                        },
                        data: [],
                        type: 'select',
                        isColor: true
                    },
                    end: {
                        isHide: false,
                        title: {
                            class: "col-md-1",
                            titleBold: 'DELETE',
                            titleTranslate: $translate.instant('DELETE').replace('<br/>', ''),
                        },
                        disable: false,
                        class: "col-md-1",
                        type: 'button'
                    }
                };
                var mechanismUrl = vm.isForAdmin ? 'Employee/MechanismInformationForAdminV2/' + ($scope.dataExport.date.getMonth() + 1) + '/' + $scope.dataExport.date.getFullYear() + '?employeeCode=' + vm.employee.id : 'Employee/MechanismInformationV2';
                apiService.get(mechanismUrl, null, function (response) {
                    apiService.get('Employee/IsNeedConfirmByEmployeeV2', null, function (response) {
                        vm.isReport = response.data;
                    }, function () {});
                    var data = response.data;
                    vm.recordId = data.recordId;
                    vm.userName = data.userName;
                    vm.employeeCode = data.employeeCode;
                    vm.email = data.email;
                    vm.isGetDataOk = true;
                    vm.oldResponse = response.data;
                    var projects = data.sorttermAllocation.all;
                    var longtermAllocation = data.longtermAllocation;
                    vm.overtimeTime = data.overtimeQuotas
                    vm.longTermPerct = 100;
                    angular.forEach(projects, function (item) {
                        vm.longTermPerct = vm.longTermPerct - item.percent;
                        var projectItem = {
                            isNew: false,
                            lineDisable: false,
                            name: {
                                isHide: false,
                                title: {
                                    class: "col-md-2",
                                    titleBold: 'THE_PROJECT_IS_PARTICIPATING',
                                    titleTranslate: $translate.instant('THE_PROJECT_IS_PARTICIPATING').replace('<br/>', ''),
                                },
                                autoKey: ['PROJ_TYPE_NAME', 'PERNR', 'startDate', 'endDate', 'projectType'],
                                disable: false,
                                class: "col-md-2",
                                value: {
                                    key: item.id,
                                    value: item.code,
                                },
                                data: 'projectInformationsSelected',
                                type: 'select'
                            },
                            projectType: {
                                isHide: false,
                                title: {
                                    class: "col-md-2",
                                    titleBold: 'PROJECT_TYPE',
                                    titleTranslate: $translate.instant('PROJECT_TYPE').replace('<br/>', ''),
                                },
                                disable: true,
                                class: "col-md-2",
                                value: {
                                    key: item.id,
                                    value: item.departmentType,
                                },
                                data: '[]',
                                type: 'select'
                            },
                            PERNR: {
                                isHide: false,
                                title: {
                                    class: "col-md-1",
                                    titleBold: 'REPORT_TO',
                                    titleTranslate: $translate.instant('REPORT_TO').replace('<br/>', ''),
                                },
                                disable: true,
                                class: "col-md-1",
                                value: item.headOfUnit ? item.headOfUnit : '',
                                type: 'input'
                            },
                            DEPARTMENT_ROLE: {
                                isHide: false,
                                title: {
                                    class: "col-md-1",
                                    titleBold: 'DEPARTMENT_ROLE',
                                    titleTranslate: $translate.instant('DEPARTMENT_ROLE').replace('<br/>', ''),
                                },
                                disable: false,
                                class: "col-md-1",
                                value: {
                                    key: item.hcmRole,
                                    value: item.departmentRole,
                                },
                                data: 'departmentRoleDataSelected',
                                type: 'select'
                            },
                            perct: {
                                isHide: false,
                                title: {
                                    class: "col-md-1",
                                    titleBold: 'PERCENTAGE',
                                    titleTranslate: $translate.instant('PERCENTAGE').replace('<br/>', ''),
                                },
                                disable: false,
                                class: "col-md-1",
                                value: item.percent,
                                type: 'number'
                            },
                            startDate: {
                                isHide: false,
                                title: {
                                    class: "col-md-1",
                                    titleBold: 'PARTICIPATING_TIME',
                                    titleTranslate: $translate.instant('PARTICIPATING_TIME').replace('<br/>', ''),
                                },
                                disable: false,
                                class: "col-md-1",
                                value: new Date(item.startDate),
                                type: 'date',
                                isOpen: false,
                                validate: {
                                    maxDate: new Date(item.endDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.endDate) : vm.endDay,
                                    minDate: new Date(item.startDate) > vm.firstDay && new Date(item.startDate) < vm.endDay ? new Date(item.startDate) : vm.firstDay
                                }
                            },
                            endDate: {
                                isHide: false,
                                title: {
                                    class: "col-md-1",
                                    titleBold: 'END_TIME',
                                    titleTranslate: $translate.instant('END_TIME').replace('<br/>', ''),
                                },
                                disable: false,
                                class: "col-md-1",
                                value: new Date(item.endDate),
                                type: 'date',
                                isOpen: false,
                                validate: {
                                    maxDate: new Date(item.endDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.endDate) : vm.endDay,
                                    minDate: new Date(item.startDate) > vm.firstDay && new Date(item.startDate) < vm.endDay ? new Date(item.startDate) : vm.firstDay
                                }
                            },
                            PROJ_TYPE_NAME: {
                                isHide: false,
                                title: {
                                    class: "col-md-2",
                                    titleBold: 'VOLUNTARY_MECHANISM',
                                    titleTranslate: $translate.instant('VOLUNTARY_MECHANISM').replace('<br/>', ''),
                                },
                                disable: true,
                                class: "col-md-2",
                                value: {
                                    key: 0,
                                    value: item.ch,
                                },
                                data: [],
                                type: 'select',
                                isColor: true
                            },
                            end: {
                                isHide: false,
                                title: {
                                    class: "col-md-1",
                                    titleBold: 'DELETE',
                                    titleTranslate: $translate.instant('DELETE').replace('<br/>', ''),
                                },
                                disable: false,
                                class: "col-md-1",
                                type: 'button'
                            }
                        };
                        vm.projectData.push(projectItem);
                    });
                    var longterm = {
                        orgUnitLv2: {
                            autoKey: ['headOfOrg2'],
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'ORG_UNIT_LV2',
                                titleTranslate: $translate.instant('ORG_UNIT_LV2').replace('<br/>', ''),
                            },
                            refreshChild: function () {
                                if (vm.card.longterm.data[0].orgUnitLv2.value) {
                                    vm.onChangeLongterm2();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(2);
                                } else {
                                    vm.card.longterm.data[0].orgUnitLv2.value = {
                                        key: 0,
                                        value: ''
                                    }
                                    vm.card.longterm.data[0].headOfOrg2.value.value = ''
                                    vm.onChangeLongterm2();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(2);
                                }
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.orgUnitLv2
                            },
                            data: 'orgUnitListLv2',
                            type: 'select'
                        },
                        orgUnitLv3: {
                            autoKey: ['headOfOrg3'],
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'ORG_UNIT_LV3',
                                titleTranslate: $translate.instant('ORG_UNIT_LV3').replace('<br/>', ''),
                            },
                            refreshChild: function () {
                                if (vm.card.longterm.data[0].orgUnitLv3.value) {
                                    vm.setOrgValueToNull(3);
                                    vm.onChangeLongterm3();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(3);
                                } else {
                                    vm.card.longterm.data[0].orgUnitLv3.value = {
                                        key: 0,
                                        value: ''
                                    }
                                    vm.card.longterm.data[0].headOfOrg3.value.value = ''
                                    vm.setOrgValueToNull(3);
                                    vm.onChangeLongterm3();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(3);
                                }
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.orgUnitLv3
                            },
                            data: 'orgUnitListLv3',
                            type: 'select'
                        },
                        orgUnitLv4: {
                            autoKey: ['headOfOrg4'],
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'ORG_UNIT_LV4',
                                titleTranslate: $translate.instant('ORG_UNIT_LV4').replace('<br/>', ''),
                            },
                            refreshChild: function () {
                                if (vm.card.longterm.data[0].orgUnitLv4.value) {
                                    vm.filterPorject(vm.card.longterm.data[0].orgUnitLv4.value.code);
                                    vm.onChangeLongterm4();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(4);
                                } else {
                                    vm.card.longterm.data[0].orgUnitLv4.value = {
                                        key: 0,
                                        value: '',
                                        code: 0
                                    }
                                    vm.card.longterm.data[0].headOfOrg4.value.value = ''
                                    vm.filterPorject(vm.card.longterm.data[0].orgUnitLv4.value.code);
                                    vm.onChangeLongterm4();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(4);
                                }
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                code: 0,
                                value: longtermAllocation.orgUnitLv4
                            },
                            data: 'orgUnitListLv4',
                            type: 'select'
                        },
                        orgUnitLv5: {
                            autoKey: ['headOfOrg5'],
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'ORG_UNIT_LV5',
                                titleTranslate: $translate.instant('ORG_UNIT_LV5').replace('<br/>', ''),
                            },
                            refreshChild: function () {
                                if (vm.card.longterm.data[0].orgUnitLv5.value) {
                                    vm.onChangeLongterm5();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(5);
                                } else {
                                    vm.card.longterm.data[0].orgUnitLv5.value = {
                                        key: 0,
                                        value: ''
                                    }
                                    vm.card.longterm.data[0].headOfOrg5.value.value = ''
                                    vm.onChangeLongterm5();
                                    vm.setChangeLongterm();
                                    vm.setOrgValueToNull(5);
                                }
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.orgUnitLv5
                            },
                            data: 'orgUnitListLv5',
                            type: 'select'
                        },
                        departmentType: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'DEPARTMENT_TYPE',
                                titleTranslate: $translate.instant('DEPARTMENT_TYPE').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.departmentType
                            },
                            data: [],
                            type: 'select'
                        },
                        longtermMechnism: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'LONGTERM_MECHINISM',
                                titleTranslate: $translate.instant('LONGTERM_MECHINISM').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.ch
                            },
                            data: [],
                            type: 'select',
                            isColor: true
                        },
                        headOfOrg2: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'HEAD_OF_ORG1',
                                titleTranslate: $translate.instant('HEAD_OF_ORG1').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.headOfOrgLv2
                            },
                            data: [],
                            type: 'select'
                        },
                        headOfOrg3: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'HEAD_OF_ORG2',
                                titleTranslate: $translate.instant('HEAD_OF_ORG2').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.headOfOrgLv3
                            },
                            data: [],
                            type: 'select'
                        },
                        headOfOrg4: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'HEAD_OF_ORG3',
                                titleTranslate: $translate.instant('HEAD_OF_ORG3').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.headOfOrgLv4
                            },
                            data: [],
                            type: 'select'
                        },
                        headOfOrg5: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'HEAD_OF_ORG4',
                                titleTranslate: $translate.instant('HEAD_OF_ORG4').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.headOfOrgLv5
                            },
                            data: [],
                            type: 'select'
                        },
                        // departmentRole: {
                        //     isHide: true,
                        //     title: {
                        //         class: "col-md-2",
                        //         titleBold: 'DEPARTMENT_ROLE',
                        // titleTranslate: $translate.instant('DEPARTMENT_ROLE').replace('<br/>', '')
                        //     },
                        //     disable: true,
                        //     class: "col-md-2",
                        //     value: {
                        //         key: 0,
                        //         value: longtermAllocation.headOfOrgLv5
                        //     },
                        //     data: [],
                        //     type: 'select'
                        // },
                        longTermPerct: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'LONGTERM_PERCT',
                                titleTranslate: $translate.instant('LONGTERM_PERCT').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: vm.longTermPerct,
                            type: 'number'
                        },
                        unitLeader: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'UNIT_LEADER',
                                titleTranslate: $translate.instant('UNIT_LEADER').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: longtermAllocation.kH200
                            },
                            data: [],
                            type: 'select',
                            isColor: true
                        }
                    }
                    vm.longtermData.push(longterm);
                    var infoItem = {
                        level: {
                            isHide: false,
                            title: {
                                class: "col-md-2",
                                titleBold: 'GRADEV2',
                                titleTranslate: $translate.instant('GRADEV2').replace('<br/>', ''),
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: data.positionsInformation.grade
                            },
                            data: 'listLevelSelected',
                            type: 'select'
                        },
                        track1: {
                            isHide: false,
                            refreshChild: function () {
                                if (!vm.card.info.data[0].track1.value) {
                                    vm.card.info.data[0].track1.value = {
                                        key: 0,
                                        value: '',
                                        partenId: 0
                                    }
                                }
                                vm.getTrackFilter(2, vm.card.info.data[0].track1.value.key)
                            },
                            title: {
                                class: "col-md-2",
                                titleBold: 'TRACK1',
                                titleTranslate: $translate.instant('TRACK1').replace('<br/>', ''),
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: data.positionsInformation.track1,
                                partenId: 0
                            },
                            data: 'track1Selected',
                            type: 'select'
                        },
                        track2: {
                            isHide: false,
                            refreshChild: function () {
                                if (!vm.card.info.data[0].track2.value) {
                                    vm.card.info.data[0].track2.value = {
                                        key: 0,
                                        value: '',
                                        partenId: 0
                                    }
                                }
                                vm.getTrackFilter(1, vm.card.info.data[0].track2.value.parentId)
                            },
                            title: {
                                class: "col-md-2",
                                titleBold: 'TRACK2V2',
                                titleTranslate: $translate.instant('TRACK2V2').replace('<br/>', ''),
                            },
                            disable: false,
                            class: "col-md-2",
                            value: {
                                key: 0,
                                value: data.positionsInformation.track2,
                                partenId: 0
                            },
                            data: 'track2Selected',
                            type: 'select'
                        },
                        overtime: {
                            isHide: false,
                            title: {
                                class: "col-md-4",
                                titleBold: 'OVERTIME_NORMS',
                                titleTranslate: $translate.instant('OVERTIME_NORMS').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-4",
                            value: data.overtimeQuotas,
                            type: 'number',
                            isColor: true
                        }
                    };
                    vm.infoData.push(infoItem);
                    var salaryItemDefault = {
                        comMechanisms: {
                            isHide: false,
                            title: {
                                class: "col-md-9",
                                titleBold: 'REMUNERATION_MECHANISM',
                                titleTranslate: $translate.instant('REMUNERATION_MECHANISM').replace('<br/>', ''),
                            },
                            autoKey: ['grade'],
                            disable: false,
                            class: "col-md-9",
                            value: {
                                key: 0,
                                value: vm.mechanisCurrKey,
                                isTranslate: true
                            },
                            data: 'COMMechanismListSelected',
                            type: 'select'
                        },
                        grade: {
                            isHide: false,
                            title: {
                                class: "col-md-3",
                                titleBold: 'OVERTIME_MECHANISM',
                                titleTranslate: $translate.instant('OVERTIME_MECHANISM').replace('<br/>', ''),
                            },
                            disable: true,
                            class: "col-md-3",
                            value: {
                                key: 0,
                                value: vm.mechanisCurrValue,
                                isTranslate: true
                            },
                            data: [],
                            type: 'select'
                        }
                    }
                    if (data.comMechanisms) {
                        var salaryItem = {
                            comMechanisms: {
                                isHide: false,
                                title: {
                                    class: "col-md-9",
                                    titleBold: 'REMUNERATION_MECHANISM',
                                    titleTranslate: $translate.instant('REMUNERATION_MECHANISM').replace('<br/>', ''),
                                },
                                autoKey: ['grade'],
                                disable: false,
                                class: "col-md-9",
                                value: {
                                    key: data.comMechanisms.ICNUM,
                                    value: data.comMechanisms.COMMechanismCode
                                },
                                data: 'COMMechanismListSelected',
                                type: 'select'
                            },
                            grade: {
                                isHide: false,
                                title: {
                                    class: "col-md-3",
                                    titleBold: 'OVERTIME_MECHANISM',
                                    titleTranslate: $translate.instant('OVERTIME_MECHANISM').replace('<br/>', ''),
                                },
                                disable: true,
                                class: "col-md-3",
                                value: {
                                    key: 0,
                                    value: data.comMechanisms.OTRegulation
                                },
                                data: '',
                                type: 'select'
                            }
                        }
                        vm.salaryData.push(salaryItem);
                    } else {
                        vm.salaryData.push(salaryItemDefault);
                    }
                    vm.renderCard();
                }, function () {});
            },
            function () {
            });
        }
        vm.renderCard = function () {
            vm.card = {
                info: {
                    header: {
                        value: 'POSITIONS_INFORMATION',
                        type: 'text',
                        style: {
                            "background": "#7597D6",
                            "padding": "12px 30px 13px 5px",
                            "font-size": "14px",
                            "font-weight": "500",
                            "color": "#ffffff"
                        }
                    },
                    key: 'info',
                    data: vm.infoData,
                    footer: {
                        value: '',
                        type: 'text'
                    }
                },
                longterm: {
                    header: {
                        value: 'LONGTERM_ALLOCATION',
                        type: 'text',
                        style: {
                            "background": "#7597D6",
                            "padding": "12px 30px 13px 5px",
                            "font-size": "14px",
                            "font-weight": "500",
                            "color": "#ffffff"
                        }
                    },
                    key: 'longterm',
                    data: vm.longtermData,
                    footer: {
                        value: '',
                        type: 'text'
                    }
                },
                projectList: {
                    header: {
                        value: 'SHORT_TERM_ALLOCATION',
                        type: 'text',
                        style: {
                            "background": "#DBA147",
                            "padding": "12px 30px 13px 5px",
                            "font-size": "14px",
                            "font-weight": "500",
                            "color": "#ffffff"
                        }
                    },
                    key: 'projectList',
                    data: vm.projectData,
                    footer: {
                        value: 'PROJECT_FOOTER',
                        type: 'link'
                    },
                    footer2: {
                        value: 'PROJECT_FOOTER2',
                        type: 'button'
                    }
                },
                salary: {
                    header: {
                        value: 'REMUNERATION_MECHANISM',
                        type: 'text',
                        style: {
                            "background": "#DBA147",
                            "padding": "12px 30px 13px 5px",
                            "font-size": "14px",
                            "font-weight": "500",
                            "color": "#ffffff",
                            "margin-top": "10px"
                        }
                    },
                    key: 'salary',
                    data: vm.salaryData,
                    footer: {
                        value: 'SALARY_FOOTER',
                        type: 'text'
                    }
                }
            }
        }
        vm.onAddItem = function () {
            var item = angular.copy(vm.projectEmpty)
            vm.projectData.push(item);
        }
        vm.onChangeValue = function (refreshChild, itemChangekey, lineChangeKey, groupChangeKey, valueInput) {
            if (itemChangekey && groupChangeKey && valueInput) {
                itemChangekey.forEach(function (value, i) {
                    var key = itemChangekey[i]
                    if (typeof vm.card[groupChangeKey]['data'][lineChangeKey][key].value === 'object') {
                        if (!vm.card[groupChangeKey]['data'][lineChangeKey][key].validate) {
                            if (valueInput.autoData[i]) {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].value.value = valueInput.autoData[i]
                            } else {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].value.value = ''
                            }
                        } else {
                            if (valueInput.autoData[i]) {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].validate = valueInput.autoData[i]
                            } else {
                                vm.card[groupChangeKey]['data'][lineChangeKey][key].value.value = {
                                    maxDate: vm.currentDate,
                                    minDate: vm.currentDate
                                }
                            }
                        }
                    } else {
                        if (valueInput.autoData[i]) {
                            vm.card[groupChangeKey]['data'][lineChangeKey][key].value = valueInput.autoData[i]
                        } else {
                            vm.card[groupChangeKey]['data'][lineChangeKey][key].value = ''
                        }
                    }
                });
                if (groupChangeKey == "projectList") {
                    vm.card.projectList.data[lineChangeKey].startDate.value = vm.firstDay
                    vm.card.projectList.data[lineChangeKey].endDate.value = vm.endDay
                }
            }
            if (refreshChild) {
                refreshChild();
            }
        }
        vm.onReportForm = function () {
            vm.isEdit = !vm.isEdit;
            if (vm.isEdit) {
                vm.initDataSelect()
            } else {
                vm.refresh();
            }
        }
        vm.initDataSelect = function () {
            vm.getProjectInfomation();
            vm.getOrgUnit(2, 'orgUnitListLv2', 'orgUnitListLv2FullData');
            vm.getOrgUnit(3, 'orgUnitListLv3', 'orgUnitListLv3FullData');
            vm.getOrgUnit(4, 'orgUnitListLv4', 'orgUnitListLv4FullData');
            vm.getOrgUnit(5, 'orgUnitListLv5', 'orgUnitListLv5FullData');
            vm.getListLevel();
            vm.getListTrack(1, null);
            vm.getListTrack(2, null);
            vm.getComMechnism();
            vm.getListDepartmentRole();
            // vm.getCorporations();
        }
        vm.refresh = function () {
            vm.departmentRoleDataSelected = []
            vm.longtermData = [];
            vm.listLevelSelected = []
            vm.track2Selected = []
            vm.projectInformationsSelected = []
            vm.COMMechanismListSelected = []
            vm.oldProjectList = []
            vm.projectListFullDataOld = []
            vm.infoData = [];
            vm.projectData = []
            vm.salaryData = []
            vm.card = {}
            vm.init();
        }
        vm.onConfirmForm = function () {
            if (vm.isEdit) {
                angular.forEach(vm.oldProjectList, function (item) {
                    vm.card.projectList.data.push(item)
                })
                vm.onApprove();
            } else {
                if($scope.$resolve.parent){
                    popupService.openLoadingPopup();
                }
                apiService.create('Employee/ConfirmInformationV2', null, function (_response) {
                    $uibModalStack.dismissAll();
                    vm.isReport = !vm.isReport;
                    popupService.closeLoadingPopup();
                    if ($scope.$resolve.parent) {
                        $scope.$resolve.parent.checkFacebook();
                    }
                }, function(err){
                    popupService.closeLoadingPopup();
                    if ($scope.$resolve.parent) {
                        $scope.$resolve.parent.checkFacebook();
                    }
                });
            }
        }
        vm.formatDataPost = function (value) {
            if (value) {
                if (value.split(" ||| ").length > 1) {
                    return value.split(" ||| ")[1];
                }
                return value;
            }
            return value;
        }
        vm.calulLongTerm = function () {
            vm.longTermPerct = 100;
            angular.forEach(vm.projectData, function (item) {
                if (item.perct.value) {
                    vm.longTermPerct = vm.longTermPerct - item.perct.value;
                    vm.card.longterm.data[0].longTermPerct.value = vm.longTermPerct;
                }
            });
        }
        vm.validateNull = function () {
            // Validate khong duoc de trong thong tin
            var isNullDate = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                if ((!item.endDate.value || !item.startDate.value || !item.DEPARTMENT_ROLE.value.value || !item.name.value.value)) {
                    isNullDate = false;
                    return false;
                }
            })
            return isNullDate;
        }
        vm.validateProjectName = function () {
            // Validate khong duoc chon du an giong nhau
            var isDuplicateName = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                var listTg = angular.copy(vm.card.projectList.data)
                var index = vm.card.projectList.data.indexOf(item);
                listTg.splice(index, 1);
                angular.forEach(listTg, function (mapItem) {
                    if (item.name.value.value === mapItem.name.value.value && item.perct.value !== 0 && mapItem.perct.value !== 0) {
                        isDuplicateName = false;
                    }
                })
            })
            return isDuplicateName;
        }
        vm.validateTimeSmall = function () {
            var isValidateTime = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                // bắt đầu nhỏ hơn kết thúc. Kết thúc lớn hơn ngày đầu tháng
                if ((vm.formatDate(item.startDate.value) > vm.formatDate(item.endDate.value)) && item.perct.value > 0) {
                    isValidateTime = false;
                }
            })
            return isValidateTime;
        }
        vm.formatDatePush = function (value) {
            var date = new Date(value)
            var month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
            var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
            if (month < 10) {
                return date.getFullYear() + '-' + month + '-' + day;
            } else {
                return date.getFullYear() + '-' + month + '-' + day;
            }
        }
        vm.validatePerctMin = function () {
            var isPerctMin = true;
            angular.forEach(vm.card.projectList.data, function (item) {
                // bắt đầu nhỏ hơn kết thúc. Kết thúc lớn hơn ngày đầu tháng
                if (!item.perct.value || item.perct.value === 0) {
                    isPerctMin = false;
                }
            })
            return isPerctMin;
        }
        vm.validateSortTermAndOrgLv4 = function () {
            var status = true;
            var value = vm.card.longterm.data[0].orgUnitLv4.value.value
            if (value) {
                var orgLevel4Code = value.split(".")[value.split(".").length - 1];
                angular.forEach(vm.card.projectList.data, function (item) {
                    if (item.name.value.shortCode == orgLevel4Code) {
                        status = false;
                    }
                });
            } else {
                return status;
            }
            return status;
        }
        vm.onApprove = function () {
            if (vm.validateSortTermAndOrgLv4()) {
                if (vm.validatePerctMin()) {
                    if (vm.validateProjectName()) {
                        if (vm.validateNull()) {
                            if (vm.validateTimeSmall()) {
                                // Xử lý update giá trị cho old response từ vm.card
                                var projects = [];
                                var COMMechanismListSelectedItemPush = {}
                                angular.forEach(vm.card.projectList.data, function (item) {
                                    angular.forEach(vm.projectListFullDataOld, function (itemOld) {
                                        if (item.name.value.key == itemOld.id) {
                                            var value = {
                                                percent: item.perct.value ? item.perct.value : 0,
                                                hcmId: itemOld.hcmId,
                                                id: itemOld.id,
                                                name: itemOld.name,
                                                code: itemOld.code,
                                                ch: itemOld.ch,
                                                startDate: vm.formatDatePush(item.startDate.value),
                                                endDate: vm.formatDatePush(item.endDate.value),
                                                departmentType: itemOld.departmentType,
                                                headOfUnit: itemOld.headOfUnit,
                                                level: itemOld.level,
                                                parentId: itemOld.parentId,
                                                departmentRole: vm.formatDataPost(item.DEPARTMENT_ROLE.value.value),
                                                hcmRole: item.DEPARTMENT_ROLE.value.key,
                                                shortCode: itemOld.shortCode
                                            }
                                            projects.push(value)
                                        }
                                    });
                                });
                                angular.forEach(vm.COMMechanismListSelectedFullData, function (item) {
                                    if (item.HCMCode === vm.card.salary.data[0].comMechanisms.value.key) {
                                        COMMechanismListSelectedItemPush = {
                                            ICNUM: item.HCMCode,
                                            COMMechanismCode: item.COMMechanismCode,
                                            Regulation: item.Regulation,
                                            OTRegulation: item.OTRegulation,
                                            BEGDA: item.BEGDA,
                                            ENDDA: item.ENDDA
                                        }
                                    }
                                })
                                var data = {
                                    recordId: vm.recordId,
                                    userName: vm.userName,
                                    employeeCode: vm.employeeCode,
                                    email: vm.email,
                                    positionsInformation: {
                                        grade: vm.formatDataPost(vm.card.info.data[0].level.value.value),
                                        track1: vm.formatDataPost(vm.card.info.data[0].track1.value.value),
                                        track2: vm.formatDataPost(vm.card.info.data[0].track2.value.value),
                                    },
                                    longtermAllocation: {
                                        orgUnitLv2: vm.formatDataPost(vm.card.longterm.data[0].orgUnitLv2.value.value),
                                        orgUnitLv3: vm.formatDataPost(vm.card.longterm.data[0].orgUnitLv3.value.value),
                                        orgUnitLv4: vm.formatDataPost(vm.card.longterm.data[0].orgUnitLv4.value.value),
                                        orgUnitLv5: vm.formatDataPost(vm.card.longterm.data[0].orgUnitLv5.value.value),
                                        headOfOrgLv2: vm.formatDataPost(vm.card.longterm.data[0].headOfOrg2.value.value),
                                        headOfOrgLv3: vm.formatDataPost(vm.card.longterm.data[0].headOfOrg3.value.value),
                                        headOfOrgLv4: vm.formatDataPost(vm.card.longterm.data[0].headOfOrg4.value.value),
                                        headOfOrgLv5: vm.formatDataPost(vm.card.longterm.data[0].headOfOrg5.value.value),
                                        departmentType: vm.formatDataPost(vm.card.longterm.data[0].departmentType.value.value),
                                        ch: vm.formatDataPost(vm.card.longterm.data[0].longtermMechnism.value.value),
                                        kH200: vm.formatDataPost(vm.card.longterm.data[0].unitLeader.value.value)
                                    },
                                    sorttermAllocation: {
                                        all: projects,
                                        projects: null,
                                        departments: null,
                                    },
                                    comMechanisms: Object.keys(COMMechanismListSelectedItemPush).length !== 0 ? COMMechanismListSelectedItemPush : null,
                                    overtimeQuotas: vm.overtimeTime
                                }
                                if (vm.longTermPerct <= 0) {
                                    notificationService.errorTranslate('SHORTTERM_VALIDATE');
                                } else {
                                    if($scope.$resolve.parent){
                                        popupService.openLoadingPopup();
                                    }
                                    apiService.create('Employee/OrderV2', data, function(response) {
                                        popupService.closeLoadingPopup();
                                        $uibModalStack.dismissAll();
                                        vm.isEdit = !vm.isEdit;
                                        vm.refresh()
                                        notificationService.successTranslate('SUCCESS_SENT_DATA_HROPER');
                                        if ($scope.$resolve.parent) {
                                            $scope.$resolve.parent.checkFacebook();
                                        }
                                    }, function(err){
                                        popupService.closeLoadingPopup();
                                        if ($scope.$resolve.parent) {
                                            $scope.$resolve.parent.checkFacebook();
                                        }
                                    });
                                   
                                }
                            } else {
                                notificationService.errorTranslate('START_DATE_SMALL_THAN_END_DATE');
                            }
                        } else {
                            notificationService.errorTranslate('VALUE_IS_NOT_NULL');
                        }
                    } else {
                        notificationService.errorTranslate('NOT_DUPLICATE_NAME');
                    }
                } else {
                    notificationService.errorTranslate('VALIDATE_PERCT_VALUE');
                }
            } else {
                notificationService.errorTranslate('VALIDATE_SORTTERM_ORG');
            }
        }
        vm.onRemoveItem = function (type, item) {
            if (type === 'projectList') {
                var index = vm.projectData.indexOf(item);
                vm.projectData.splice(index, 1);
            }
        }
        vm.setChangeLongterm = function () {
            if (vm.card.longterm.data[0].orgUnitLv5.value.value) {
                vm.setDataToGroupLongterm(vm.card.longterm.data[0].orgUnitLv5.value);
            } else if (vm.card.longterm.data[0].orgUnitLv4.value.value) {
                vm.setDataToGroupLongterm(vm.card.longterm.data[0].orgUnitLv4.value);
            } else if (vm.card.longterm.data[0].orgUnitLv3.value.value) {
                vm.setDataToGroupLongterm(vm.card.longterm.data[0].orgUnitLv3.value);
            } else if (vm.card.longterm.data[0].orgUnitLv2.value.value) {
                vm.setDataToGroupLongterm(vm.card.longterm.data[0].orgUnitLv2.value);
            }
        }
        vm.setDataToGroupLongterm = function (data) {
            if (data.departmentType) {
                vm.card.longterm.data[0].departmentType.value.value = data.departmentType;
            }
            if (data.ch) {
                vm.card.longterm.data[0].longtermMechnism.value.value = data.ch;
            }
            if (data.headOfUnit) {
                vm.card.longterm.data[0].unitLeader.value.value = data.headOfUnit;
            }
        }
        vm.onChangeLongterm2 = function () {
            angular.forEach(vm.orgUnitListLv2, function (item) {
                if (item.key == vm.card.longterm.data[0].orgUnitLv2.value.key) {
                    var data = item;
                    vm.card.longterm.data[0].headOfOrg2.value.value = data.autoData[0]
                    vm.onFilterDataOrgUnit2(data.key)
                }
            });
        }
        vm.onChangeLongterm3 = function () {
            angular.forEach(vm.orgUnitListLv3, function (item) {
                if (item.key == vm.card.longterm.data[0].orgUnitLv3.value.key) {
                    var data = item;
                    vm.card.longterm.data[0].headOfOrg3.value.value = data.autoData[0]
                    angular.forEach(vm.orgUnitListLv2, function (item2) {
                        if (item2.key == data.parentId) {
                            vm.card.longterm.data[0].orgUnitLv2.value.key = item2.key;
                            vm.card.longterm.data[0].orgUnitLv2.value.value = item2.value
                            vm.card.longterm.data[0].headOfOrg2.value.value = item2.autoData[0]
                        }
                    });
                    vm.onFilterDataOrgUnit3(data.key)
                }
            });
        }
        vm.onChangeLongterm4 = function () {
            angular.forEach(vm.orgUnitListLv4, function (item) {
                if (item.key == vm.card.longterm.data[0].orgUnitLv4.value.key) {
                    var data = item;
                    vm.card.longterm.data[0].headOfOrg4.value.value = data.autoData[0]
                    angular.forEach(vm.orgUnitListLv3, function (item2) {
                        if (item2.key == data.parentId) {
                            vm.card.longterm.data[0].orgUnitLv3.value.key = item2.key;
                            vm.card.longterm.data[0].orgUnitLv3.value.value = item2.value
                        }
                    });
                    vm.onFilterDataOrgUnit4(data.key)
                }
            });
            vm.onChangeLongterm3();
        }
        vm.onChangeLongterm5 = function () {
            angular.forEach(vm.orgUnitListLv5, function (item) {
                if (item.key == vm.card.longterm.data[0].orgUnitLv5.value.key) {
                    var data = item;
                    vm.card.longterm.data[0].headOfOrg5.value.value = data.autoData[0]
                    angular.forEach(vm.orgUnitListLv4, function (item2) {
                        if (item2.key == data.parentId) {
                            vm.card.longterm.data[0].orgUnitLv4.value.key = item2.key;
                            vm.card.longterm.data[0].orgUnitLv4.value.value = item2.value
                        }
                    });
                }
            });
            vm.onChangeLongterm4();
            vm.onChangeLongterm3();
        }
        vm.onFilterDataOrgUnit4 = function (id) {
            vm.orgUnitListLv5 = [];
            angular.forEach(vm.orgUnitListLv5FullData, function (item) {
                if (id == item.parentId) {
                    vm.orgUnitListLv5.push(item);
                }
            });
        }
        vm.onFilterDataOrgUnit3 = function (id) {
            vm.orgUnitListLv4 = [];
            angular.forEach(vm.orgUnitListLv4FullData, function (item) {
                if (id == item.parentId) {
                    vm.orgUnitListLv4.push(item);
                }
            });
            vm.orgUnitListLv5 = [];
            if (!vm.card.longterm.data[0].orgUnitLv4.value.value) {
                angular.forEach(vm.orgUnitListLv4, function (item1) {
                    angular.forEach(vm.orgUnitListLv5FullData, function (item2) {
                        if (item1.key == item2.parentId) {
                            vm.orgUnitListLv5.push(item2);
                        }
                    });
                });
            } else {
                angular.forEach(vm.orgUnitListLv5FullData, function (item2) {
                    if (vm.card.longterm.data[0].orgUnitLv4.value.key == item2.parentId) {
                        vm.orgUnitListLv5.push(item2);
                    }
                });
            }
        }
        vm.onFilterDataOrgUnit2 = function (id) {
            vm.orgUnitListLv3 = [];
            angular.forEach(vm.orgUnitListLv3FullData, function (item) {
                if (id == item.parentId) {
                    vm.orgUnitListLv3.push(item);
                }
            });
            vm.orgUnitListLv4 = [];
            angular.forEach(vm.orgUnitListLv3, function (item1) {
                angular.forEach(vm.orgUnitListLv4FullData, function (item2) {
                    if (item1.key == item2.parentId) {
                        vm.orgUnitListLv4.push(item2);
                    }
                });
            });
            vm.orgUnitListLv5 = [];
            if (!vm.card.longterm.data[0].orgUnitLv4.value.value) {
                angular.forEach(vm.orgUnitListLv4, function (item1) {
                    angular.forEach(vm.orgUnitListLv5FullData, function (item2) {
                        if (item1.key == item2.parentId) {
                            vm.orgUnitListLv5.push(item2);
                        }
                    });
                });
            } else {
                angular.forEach(vm.orgUnitListLv5FullData, function (item2) {
                    if (vm.card.longterm.data[0].orgUnitLv4.value.key == item2.parentId) {
                        vm.orgUnitListLv5.push(item2);
                    }
                });
            }
        }
        vm.setOrgValueToNull = function (level) {
            if (level == 2) {
                vm.card.longterm.data[0].orgUnitLv3.value.key = 0;
                vm.card.longterm.data[0].orgUnitLv3.value.value = ''
                vm.card.longterm.data[0].headOfOrg3.value.value = ''
                vm.card.longterm.data[0].orgUnitLv4.value.key = 0;
                vm.card.longterm.data[0].orgUnitLv4.value.value = ''
                vm.card.longterm.data[0].headOfOrg4.value.value = ''
                vm.card.longterm.data[0].orgUnitLv5.value.key = 0;
                vm.card.longterm.data[0].orgUnitLv5.value.value = ''
                vm.card.longterm.data[0].headOfOrg5.value.value = ''
            }
            if (level == 3) {
                vm.card.longterm.data[0].orgUnitLv4.value.key = 0;
                vm.card.longterm.data[0].orgUnitLv4.value.value = ''
                vm.card.longterm.data[0].headOfOrg4.value.value = ''
                vm.card.longterm.data[0].orgUnitLv5.value.key = 0;
                vm.card.longterm.data[0].orgUnitLv5.value.value = ''
                vm.card.longterm.data[0].headOfOrg5.value.value = ''
            }
            if (level == 4) {
                vm.card.longterm.data[0].orgUnitLv5.value.key = 0;
                vm.card.longterm.data[0].orgUnitLv5.value.value = ''
                vm.card.longterm.data[0].headOfOrg5.value.value = ''
            }
        }
        vm.filterPorject = function (value) {
            vm.projectInformationsSelected = []
            var orgLevel4Code = value;
            // var orgLevel4Code = value.split(".")[value.split(".").length - 1];
            angular.forEach(vm.projectsListSelectedFullData, function (item) {
                if (item.shortCode != orgLevel4Code) {
                    vm.projectInformationsSelected.push(item)
                }
            });
        }
        vm.getProjectInfomation = function () {
            apiService.get('/EstimateOwner/GetListSorttermUnit?time=' + $filter('date')(vm.currentDate, config.yyyyMMdd), null, function (response) {
                vm.projectListFullDataOld = response.data;
                angular.forEach(vm.card.projectList.data, function (itemOld) {
                    var projectTgItem = response.data.filter(function (itemTg) {
                        return itemTg.id === itemOld.name.value.key;
                    })[0];
                    if (projectTgItem) {
                        itemOld.startDate.validate = {
                            maxDate: new Date(projectTgItem.endDate) > vm.firstDay && new Date(projectTgItem.endDate) < vm.endDay ? new Date(projectTgItem.endDate) : vm.endDay,
                            minDate: new Date(projectTgItem.startDate) > vm.firstDay && new Date(projectTgItem.startDate) < vm.endDay ? new Date(projectTgItem.startDate) : vm.firstDay
                        }
                        itemOld.endDate.validate = {
                            maxDate: new Date(projectTgItem.endDate) > vm.firstDay && new Date(projectTgItem.endDate) < vm.endDay ? new Date(projectTgItem.endDate) : vm.endDay,
                            minDate: new Date(projectTgItem.startDate) > vm.firstDay && new Date(projectTgItem.startDate) < vm.endDay ? new Date(projectTgItem.startDate) : vm.firstDay
                        }
                    }
                })
                angular.forEach(response.data, function (item) {
                    if (true) {
                        var validate = {
                            maxDate: new Date(item.endDate) > vm.firstDay && new Date(item.endDate) < vm.endDay ? new Date(item.endDate) : vm.endDay,
                            minDate: new Date(item.startDate) > vm.firstDay && new Date(item.startDate) < vm.endDay ? new Date(item.startDate) : vm.firstDay
                        }
                        vm.projectInformationsSelected.push({
                            key: item.id,
                            code: item.code,
                            value: item.code,
                            shortCode: item.shortCode,
                            autoData: [item.ch, item.headOfUnit, validate, validate, item.departmentType, item.departmentRole]
                        })
                        vm.projectsListSelectedFullData.push({
                            key: item.id,
                            code: item.code,
                            value: item.code,
                            shortCode: item.shortCode,
                            autoData: [item.ch, item.headOfUnit, validate, validate, item.departmentType, item.departmentRole]
                        })
                    }
                });
            }, function () {});
        }
        vm.getOrgUnit = function (level, dataInput, dataFull) {
            apiService.get('EstimateOwner/GetListOrgUnit?level=' + level, null, function (response) {
                if (response.data != null) {
                    vm[dataFull] = response.data.map(function (item) {
                        return {
                            key: item.id,
                            code: item.code,
                            value: item.code,
                            autoData: [item.headOfUnit],
                            departmentType: item.departmentType,
                            ch: item.ch,
                            headOfUnit: item.headOfUnit,
                            parentId: item.parentId
                        }
                    });
                    vm[dataInput] = vm[dataFull]
                }
            }, function () {});
        }
        vm.getListLevel = function () {
            apiService.get('Employee/GetListLevel', null, function (response) {
                vm.listLevelSelected = response.data.map(function (item) {
                    return {
                        key: item.Code,
                        value: item.Desc_vi
                    }
                });
            }, function () {});
        }
        vm.getListDepartmentRole = function () {
            apiService.get('Employee/GetUnitRoles', null, function (response) {
                vm.departmentRoleDataSelected = response.data.map(function (item) {
                    return {
                        key: item.hcmCode,
                        value: item.name
                    }
                });
            }, function () {});
        }
        vm.getComMechnism = function () {
            apiService.get('Employee/COMMechanismList', null, function (response) {
                vm.COMMechanismListSelectedFullData = response.data;
                vm.COMMechanismListSelected = response.data.map(function (item) {
                    return {
                        key: item.HCMCode,
                        value: item.COMMechanismCode + " ||| " + item.Regulation,
                        autoData: [item.OTRegulation]
                    }
                });
                vm.COMMechanismListSelected.push({
                    key: 0,
                    value: vm.mechanisCurrKey,
                    autoData: [vm.mechanisCurrValue]
                })
            }, function () {});
        }
        vm.getListTrack = function (level, partenId) {
            var url = 'Employee/GetListTrack?level=' + level + (partenId ? ('&parentId=' + partenId) : "");
            apiService.get(url, null, function (response) {
                if (level == 1) {
                    vm.track1SelectedFullData = response.data.map(function (item) {
                        return {
                            key: item.id,
                            value: item.name + " ||| " + item.description,
                            parentId: item.parentId
                        }
                    });
                    vm.track1Selected = vm.track1SelectedFullData;
                }
                if (level == 2) {
                    vm.track2SelectedFullData = response.data.map(function (item) {
                        return {
                            key: item.id,
                            value: item.name + " ||| " + item.description,
                            parentId: item.parentId
                        }
                    });
                    vm.track2Selected = vm.track2SelectedFullData;
                }
            }, function () {});
        }
        vm.getTrackFilter = function (level, id) {
            if (level = 2) {
                vm.track2Selected = [];
                angular.forEach(vm.track2SelectedFullData, function (item) {
                    if (item.parentId == id) {
                        vm.track2Selected.push(item)
                    }
                });
            }
            if (level = 1) {
                angular.forEach(vm.track1SelectedFullData, function (item) {
                    if (item.key == id) {
                        vm.card.info.data[0].track1.value.key = item.key
                        vm.card.info.data[0].track1.value.value = item.value,
                            vm.card.info.data[0].track1.value.parentId = item.parentId
                    }
                });
            }
        }
        vm.getCorporations = function () {
            apiService.get('EstimateOwner/GetCorporations', null, function (response) {
                vm.ptListDataSelected = response.data.corporations.map(function (item) {
                    return {
                        key: item.shortCode,
                        value: item.shortCode
                    }
                });
            }, function () {});
        }

        vm.openDatePopup = function (item, lineData) {
            // var count = 0;
            // var oldDataItem = angular.copy(lineData)
            // oldDataItem.perct.value = 0;
            // angular.forEach(vm.oldProjectList, function (itemOld) {
            //     if (lineData.isNew) {
            //         count++;
            //     } else if (lineData.name.value.value === itemOld.name.value.value) {
            //         count++;
            //     }
            // })
            // if (count === 0) {
            //     vm.oldProjectList.push(oldDataItem)
            // }
            item.isOpen = true;
        }
        vm.openLink = function () {
            $window.open('https://sdoc.topica.vn', '_blank');
        }
        vm.formatDate = function (value) {
            var dateValue = new Date(value)
            dateValue.setSeconds(0);
            dateValue.setMinutes(0);
            dateValue.setHours(0);
            return dateValue;
        }
        vm.openDetails = function (type, data) {
            $uibModal.open({
                animation: true,
                templateUrl: type === 'projectList' ? 'app/pages/reportWrong/widgets/project/projectReport.html' : 'app/pages/reportWrong/widgets/department/departmentReport.html',
                size: 'lg',
                windowClass: 'app-modal-window',
                controller: type === 'projectList' ? 'ProjectReportCtrl' : 'DepartmentReportCtrl',
                resolve: {
                    data: function () {
                        return data;
                    },
                    type: function () {
                        return type;
                    }
                }
            });
        }
    }
})();