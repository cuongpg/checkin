(function () {
  'use strict';

  angular.module('BlurAdmin.pages.approvalRecord')
    .controller('changeManagerCtrl', changeManagerCtrl);

  /** @ngInject */
  function changeManagerCtrl($scope, apiService, confirmService, $filter, notificationService, loaderService, $translate, $timeout, config, menuService) {

    $scope.changeManager = {
      currentManager: {},
      newManagerSelected: {},
      newManager: [],

      refresh: function () {
        $scope.changeManager.currentManager = {
          managerName: "",
          managerEmail: "",
          userName: "",
          department: "",
          headOfOrgUnitEmail: "",
          headOfOrgUnitName: ""
        }
        $scope.changeManager.newManagerSelected = {};
        $scope.changeManager.newManager = [];
        $scope.changeManager.loadManagerInfo();
      },

      loadManagerInfo: function () {
        var _this = this;
        _this.isLoading = true;
        apiService.get('Employee/TimeKeepingManagerInfo', null, function (response) {
          // $scope.changeManager.getCurrentEstimateOwner();
          $scope.changeManager.currentManager.managerName = response.data.managerName;
          $scope.changeManager.currentManager.managerEmail = response.data.managerEmail;
          $scope.changeManager.currentManager.userName = response.data.userName;
          $scope.changeManager.currentManager.department = response.data.department;
          $scope.changeManager.currentManager.headOfOrgUnitName = response.data.headOfOrgUnitName;
          $scope.changeManager.currentManager.headOfOrgUnitEmail = response.data.headOfOrgUnitEmail;
          _this.isLoading = false;
        },
          function () {
            _this.isLoading = false;
          });
      },

      refreshNewManager: function (input) {
        if (input) getManagerByEmail(input);
        else $scope.changeManager.newManager = [];
      },

      ChangeManager: function () {

        if(!$scope.changeManager.currentManager.headOfOrgUnitEmail){
          return notificationService.errorTranslate('CANNOT_CHANGE_MANAGER')
          
        }
        if (!$scope.changeManager.newManagerSelected.email) {
          return notificationService.errorTranslate('NEW_MANAGER_IS_NULL');
        }
        else if ($scope.changeManager.newManagerSelected.email == $scope.changeManager.currentManager.managerEmail) {
          return notificationService.errorTranslate('NEW_MANAGER_IS_CURRENT_MANAGER')
        }
        else {
          var newManagerEmail = $scope.changeManager.newManagerSelected.email;
          apiService.create('ChangeManagerInfo/ChangeManager' + '?newManagerEmail=' + newManagerEmail, null, function (_response) {
            $scope.refresh();
          });
        }
      }
    }

    var getManagerByEmail = function (key) {
      apiService.get('ChangeManagerInfo/GetAllManager' + '?keyWord=' + key + '&perPage=10', null, function (response) {
        if (response.data.data != null) {
          $scope.changeManager.newManager = response.data.data;
        }
      });
    };

    $scope.refresh = function () {
      $scope.changeManager.refresh();
      $scope.myChangeManagerInfo.refresh();
      $scope.ChangeManagerInfoToApprove.refresh();
    }





    $scope.ChangeManagerInfoToApprove = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 5,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.ChangeManagerInfoToApprove.tableState = tableState;
        $scope.ChangeManagerInfoToApprove.refresh();
      },
      refresh: function () {
        var _this = $scope.ChangeManagerInfoToApprove;
        _this.isLoading = $scope.ChangeManagerInfoToApprove;

        apiService.get('ChangeManagerInfo/GetChangeManagerInfoToApprove', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },

      approveReject: function (item, a_r) {
        if (item.status != 0) {
          return notificationService.errorTranslate('CAN_NOT_FIX_THIS_FORM');
        }
        apiService.create('ChangeManagerInfo/ConfirmChangeManager' + '?id=' + item.id + '&isConfirm=' + (a_r == 'approve'), null, function (_response) {
          $scope.refresh();
        });
      }

    };


    $scope.myChangeManagerInfo = {
      isSelectAll: false,
      isLoading: true,
      tableState: null,
      tablePageSize: 5,
      tableData: [],
      default: {
        status: 0
      },
      init: function (tableState) {
        $scope.myChangeManagerInfo.tableState = tableState;
        $scope.myChangeManagerInfo.refresh();
      },
      refresh: function () {
        var _this = $scope.myChangeManagerInfo;
        _this.isLoading = true;
        apiService.get('ChangeManagerInfo/GetMyChangeManagerInfo', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      cancel: function (item) {
        if (item.status == 0) {
          apiService.create('ChangeManagerInfo/DestroyChangeManager' + '?id=' + item.id, null, function (_response) {
            $scope.refresh();
          });
        }
      }


    };

    $scope.refresh();

  }

})();

