/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.changeManager', [])
    .config(routeConfig);


  function routeConfig($stateProvider) {
    $stateProvider
      .state('changeManager', {
        url: '/changeManager',
        templateUrl: 'app/pages/changeManager/changeManager.html',
        title: 'CHANGE_MANAGER',
        controller: 'changeManagerCtrl',
        sidebarMeta: {
          icon: 'ion-document-text',
          order: 2,
        },
        permission: ['Employee.TimeKeepingManagerInfo']
      });
  }
})();
