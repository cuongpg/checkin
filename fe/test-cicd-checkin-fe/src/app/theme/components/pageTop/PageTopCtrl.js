
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
    .controller('PageTopCtrl', PageTopCtrl);

  function PageTopCtrl($scope, $window, $cookies, $translate, $uibModal, $uibModalStack) {

    $scope.openPopup = function (page, size) {
      $uibModal.open({
        animation: true,
        templateUrl: page,
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          datas: function () {
            return $scope;
          }
        }
      });
    };
    var ModalInstanceCtrl = function ($scope) {
      $scope.ok = function () {
        $window.open('https://hrpro.topica.vn', '_blank');
        $uibModalStack.dismissAll();
      };
      $scope.cancel = function () {
        $uibModalStack.dismissAll();
      };
    };

    $scope.userImageUrl = $cookies.get('user_image_url');
    $scope.userName = $cookies.get('user_name');

    $scope.flags = {
      selected: $cookies.get('NG_TRANSLATE_LANG_KEY'),
      en: 'en',
      vi: 'vi',
      th: 'th',
    };

    $scope.changeLanguage = function (language) {
      $translate.use(language);
      $scope.flags.selected = language;
    };

    $scope.signout = function () {
      var clearAllCookies = function (){
        _.forEach($cookies.getAll(), function (value, key) {
          $cookies.remove(key);
        });
      }
      var ssoClient = new SSOClient(configApp.configSso);
      try
      {
        clearAllCookies()
        ssoClient.refreshTokenSilent().then(function (user) {
          ssoClient.signoutRedirect();
        }).catch(function(error) {
          ssoClient.removeUserInfo()
          ssoClient.signin()
        });
        // gapi.load('auth2', function () {
        //   auth2 = gapi.auth2.init({
        //     client_id: configApp.clientId,
        //     scope: 'profile'
        //   });
        // });
        // var auth2 = gapi.auth2.getAuthInstance();
        // var profile = auth2.currentUser.get().getBasicProfile();
        // auth2.signOut().then(function () {
        //   auth2.disconnect();
        //   // _.forEach($cookies.getAll(), function (value, key) {
        //   //   $cookies.remove(key);
        //   // });
        //   clearAllCookies()
        //   $window.location.href = 'auth.html';
        // });
        // auth2.disconnect();
      }
      catch(ex)
      {
        // _.forEach($cookies.getAll(), function (value, key) {
        //   $cookies.remove(key);
        // });
        clearAllCookies()
        ssoClient.removeUserInfo()
        ssoClient.signin();
        // $window.location.href = 'auth.html';
      }
    };
  }

})();