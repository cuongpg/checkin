/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.theme.components')
        .controller('LangCenterCtrl', LangCenterCtrl);
  
    /** @ngInject */
    function LangCenterCtrl($scope, $sce, $translate, $cookies, $state) {     
  
      $scope.languages = [
        {
          langId: 0,
          image: '././././assets/img/lang/vi.svg',
          text: 'Vietnamese',
          name: 'vi'
        },
        {
          langId: 1,
          image: '././././assets/img/lang/en.svg',
          text: 'English',
          name: 'en'
        },
        {
          langId: 2,
          image: '././././assets/img/lang/th.svg',
          text: 'Thailand',
          name: 'th'
        }
      ];

      $scope.curLang = $scope.languages[0];
      var lang_cookie = $cookies.get('NG_TRANSLATE_LANG_KEY');
      angular.forEach($scope.languages, function(value, key) {
        if (value.name == lang_cookie) {
          $scope.curLang = value;
        }
      })
      $scope.selectLanguage = function (lang) {
        $scope.curLang = lang;
        $translate.use(lang.name);
        $state.go($state.current, {}, {reload: true});
      }

      $scope.getCurLang = function(lang) {
        var text = lang.text;
        return $sce.trustAsHtml(text);
      };
    }
  })();