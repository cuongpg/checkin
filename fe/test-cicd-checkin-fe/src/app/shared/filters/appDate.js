(function () {
    'use strict';

    angular.module('BlurAdmin.shared')
        .filter('appDate', appDate);

    /** @ngInject */
    function appDate($filter, $cookies) {
        return function (input) {
            if (!input) {
                return '';
            }
            return $filter('date')(moment.utc(input).local().toDate(), 
                ($cookies.get('NG_TRANSLATE_LANG_KEY') === 'en' ? 'MM/dd/yyyy' : 'dd/MM/yyyy'));
        };
    }

})();
