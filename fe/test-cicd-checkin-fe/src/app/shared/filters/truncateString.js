(function () {
  'use strict';

  angular.module('BlurAdmin.shared')
    .filter('truncateString', truncateString);

  /** @ngInject */
  function truncateString() {
    return function (input, length) {
      return _.truncate(input, {
        'length': length || 20
      });
    };
  }

})();
