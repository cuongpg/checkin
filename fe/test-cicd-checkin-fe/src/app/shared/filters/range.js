(function () {
  'use strict';

  angular.module('BlurAdmin.shared')
    .filter('range', range);

  /** @ngInject */
  function range() {
    return function (input, total) {
      input = _.range(1, total + 1)
      return input;
    };
  }

})();
