(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('popupService', popupService);

  /** @ngInject */
  function popupService($uibModal) {
    var loadingPopup;

    function openPopup(templateUrl, controller, details, urlConfirm, dataPost) {
      return $uibModal.open({
        animation: true,
        templateUrl:templateUrl,
        controller: controller,
        size: 'md',
        resolve: {
          details: function () {
            return details;
          },
          urlConfirm: function () {
            return urlConfirm;
          },
          dataPost: function () {
            return dataPost;
          }
        }
      });
    }

    function openLoadingPopup() {
      loadingPopup = $uibModal.open({
        animation: true,
        backdrop: 'static',
        keyboard: false,
        templateUrl: 'app/shared/services/popup/loadingModal.html',
        size: 'md',
        resolve: {
        }
      });
    }

    function closeLoadingPopup() {
      if(loadingPopup){
        loadingPopup.close();
      }
    }

    function openApprovePopup(details, urlConfirm, dataPost) {
      return openPopup('app/shared/services/popup/approvalModal.html', 'ApprovalModalCtrl', details, urlConfirm, dataPost)
    }

    function openRejectPopup(details, urlConfirm, dataPost) {
      return openPopup('app/shared/services/popup/rejectModal.html', 'RejectModalCtrl', details, urlConfirm, dataPost)
    }

    return {
      openLoadingPopup: openLoadingPopup,
      closeLoadingPopup: closeLoadingPopup,
      openApprovePopup: openApprovePopup,
      openRejectPopup: openRejectPopup
    };
  }
})();