(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('dateService', dateService);

  /** @ngInject */
  function dateService() {


    function calculateDate(opreation) {
      var diff = Math.floor(opreation);
      var secs = Math.floor(diff / 1000);
      var mins = Math.floor(secs / 60);
      var hours = Math.floor(mins / 60);
      var days = Math.floor(hours / 24);
      var months = Math.floor(days / 31);
      var years = Math.floor(months / 12);
      months = Math.floor(months % 12);
      days = Math.floor(days % 31);
      hours = Math.floor(hours % 24);
      mins = Math.floor(mins % 60);
      secs = Math.floor(secs % 60);
      var message = "";
      if (days <= 0) {
        message += (hours < 10 && hours >= 0 ? "0" + hours : hours) + ":";
        message += (mins < 10 && mins >= 0 ? "0" + mins : mins) + ":";
        message += (secs < 10 && secs >= 0 ? "0" + secs : secs) + "";
      } else {
        if (years > 0) {
          message += years + " years ";
        }
        if (months > 0 || years > 0) {
          message += months + " months ";
        }
        message += days + " days";
      }
      return message;
    };

    function getTotalHours(timeStamp) {
      var duration = moment.duration(timeStamp);
      var hours =  parseInt(duration.asHours());
      var minutes = (parseInt(duration.asMinutes()) - hours*60);
      var seconds = (parseInt(duration.asSeconds()) - parseInt(duration.asMinutes())*60);
      return (hours < 10 && hours >= 0 ? "0" + hours : hours) + ":" +
        (minutes < 10 && minutes >= 0 ? "0" + minutes : minutes) + ":" +
        (seconds < 10 && seconds >= 0 ? "0" + seconds : seconds);
    };

    function getYear(timeStamp) {
      return moment.duration(timeStamp);
      // return moment(timeStamp,"DD/MM/YYYY").year();
    }

    return {
      calculateDate: calculateDate,
      getTotalHours: getTotalHours,
      getYear: getYear
    };

  }
})();