(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('apiService', apiService);

  /** @ngInject */
  function apiService($http, $window, $cookies, $timeout,
    config, notificationService, loaderService) {

    var onErrorRequest = function (res) {
      if (res.status === 401) {
        var clearCookies = function(){
          _.forEach($cookies.getAll(), function (value, key) {
            $cookies.remove(key);
          });
        }
        var saveDataToCookies = function(key , value , dayExpired) {
          var expireDate = new Date();
          expireDate.setDate(expireDate.getDate() + dayExpired);
          $cookies.put(key, value, {
            expires: expireDate
          });
        }
        var signInAgain = function() {
          loaderService.hide()
          clearCookies()
          ssoClient.removeUserInfo();
          ssoClient.signin();
        }
        var ssoClient = new SSOClient(configApp.configSso);
        loaderService.show()
        ssoClient.refreshTokenSilent().then(function successCallback(ssoResponse) {
          if (ssoResponse) {
            var access_token = ssoResponse.access_token;
            var options = {
              method: "POST",
              url: configApp.apiUrl + "oauth/ssotoken",
              data: { AccessToken: access_token }
            };
            $http(options).then(function success(response) {
              if (response.data.data) {
                var routesOption = {
                    method: 'GET',
                    url: configApp.apiUrl + 'oauth/routes',
                    headers: {
                      'Authorization': 'Bearer ' + response.data.data,
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    }
                };
                $http(routesOption).then(function success(routes) {
                  loaderService.hide()
                  if (routes.data.data) {
                      var isLeader = !(routes.data.data.indexOf('OverTime.GetOvertimeForManager') === -1);
                      if (isLeader) {
                        $cookies.put('is_leader', "leader");
                      } else {
                        $cookies.put('is_leader', "employee");
                      }
                      saveDataToCookies('user_image_url',ssoResponse.profile.employee_profile_picture,30)
                      saveDataToCookies('access_token',response.data.data,30)
                      saveDataToCookies('user_name',((ssoResponse.profile.given_name || "") + " " + (ssoResponse.profile.family_name || "")),30)
                      $window.location = '/';
                    }
                }, function error(response) {
                  signInAgain()
                });
              } else{
                signInAgain()
              }
            }, function error(response) {
              signInAgain()
            });
          }
        }, function errorCallback(response) {
          signInAgain()
          // $window.location.href = 'auth.html';
        });
        // _.forEach($cookies.getAll(), function (value, key) {
        //   $cookies.remove(key);
        // });
        // $window.location.href = 'auth.html';
      } else if (res.status === -1) {
        notificationService.errorTranslate('ERR_CONNECTION_REFUSED');
      } else {
        if (_.has(res, 'data.message')) {
          notificationService.errorTranslate(res.data.message);
        } else {
          try{
            var enc = new TextDecoder("utf-8");
            var data =  JSON.parse(enc.decode(res.data))
            if(data.message) {
              notificationService.errorTranslate(data.message);
            } else {
              notificationService.errorTranslate('ERROR_OCCURRED');
            }
          }
          catch(err){
            notificationService.errorTranslate('ERROR_OCCURRED');
          }
        }
      }

      // if (res.status === 401) {
      //   _.forEach($cookies.getAll(), function (value, key) {
      //     $cookies.remove(key);
      //   });
      //   $window.location.href = 'auth.html';
      // } else if (res.status === -1) {
      //   notificationService.errorTranslate('ERR_CONNECTION_REFUSED');
      // } else {
      //   if (_.has(res, 'data.message')) {
      //     notificationService.errorTranslate(res.data.message);
      //   } else {
      //     var enc = new TextDecoder("utf-8");
      //     var data =  JSON.parse(enc.decode(res.data))
      //     if(data.message) {
      //       notificationService.errorTranslate(data.message);
      //     } else {
      //       notificationService.errorTranslate('ERROR_OCCURRED');
      //     }
      //   }
      // }
    };

    var request = function (url, method, data, successCallback, errorCallback, options, hideNotif, fileDownloadName, isCSV) {
      if (url.indexOf('?') != -1) {
        url = url + "&language=" + $cookies.get('NG_TRANSLATE_LANG_KEY');
      } else {
        url = url + "?language=" + $cookies.get('NG_TRANSLATE_LANG_KEY');
      }
      loaderService.show();
      var isGet = (method.toLowerCase() === 'get');
      //options.timeout = 15000;
      options = _.merge(options, {
        url: config.apiUrl + url,
        method: method,
        params: (isGet ? data : null),
        data: (!isGet ? (data || null) : null),
        timeout: !fileDownloadName ? 150000 : 8000000
      });

      $http(options)
        .then(function success(response) {
          if (!isGet && !hideNotif) {
            notificationService.successTranslate('SUCCESS');
          }
          // show loading a moment
          $timeout(function () {
            if(response)
            {
              loaderService.hide();
            }
            // check if is download
            if (fileDownloadName && !isCSV) {
              var blob = new Blob([response.data], { type: response.headers("content-type") });
              saveAs(blob, fileDownloadName);
            }
            if (fileDownloadName && isCSV) {
              var blob = new Blob([response.data], { type: "text/csv;charset=utf-8;" });
              saveAs(blob, fileDownloadName+'.csv');
            }
            if (successCallback) {
              successCallback(response.data, response.status);
            }
          }, 400);
        }, function error(errorResponse) {
          loaderService.hide();
          onErrorRequest(errorResponse);
          if (errorCallback) {
            errorCallback(errorResponse, errorResponse.status);
          }
        }
        );
    };

    function get(url, data, successCallback, errorCallback, hideNotif, options) {
      request(url, 'get', data, successCallback, errorCallback, options, hideNotif);
    };

    function create(url, data, successCallback, errorCallback, hideNotif, options) {
      request(url, 'post', data, successCallback, errorCallback, options, hideNotif);
    };

    function update(url, data, successCallback, errorCallback, hideNotif, options) {
      request(url, 'put', data, successCallback, errorCallback, options, hideNotif);
    };

    function remove(url, successCallback, errorCallback, data, hideNotif, options) {
      request(url, 'delete', data, successCallback, errorCallback, options, hideNotif);
    };

    function patch(url, data, successCallback, errorCallback, hideNotif, options) {
      request(url, 'patch', data, successCallback, errorCallback, options, hideNotif);
    };

    function download(method, url, data, fileDownloadName, successCallback, errorCallback, options) {
      options = _.merge(options, {
        responseType: 'arraybuffer'
      });
      request(url, method, data, successCallback, errorCallback, options, false, fileDownloadName);
    };

    function downloadCSV(method, url, data, fileDownloadName, isCSV, successCallback, errorCallback, options) {
      options = _.merge(options, {
        responseType: 'arraybuffer'
      });
      request(url, method, data, successCallback, errorCallback, options, false, fileDownloadName, isCSV);
    };

    return {
      request: request,
      get: get,
      create: create,
      update: update,
      delete: remove,
      patch: patch,
      download: download,
      downloadCSV: downloadCSV
    };

  }
})();