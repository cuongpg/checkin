(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('notificationService', notificationService);

  /** @ngInject */
  function notificationService(toastr, $translate) {

    function success(message) {
      show('success', message);
    }

    function info(message) {
      show('info', message);
    }

    function error(message) {
      show('error', message);
    }

    function warning(message) {
      show('warning', message);
    }

    function successTranslate(message) {
      show('success', message, true);
    }

    function infoTranslate(message) {
      show('info', message, true);
    }

    function errorTranslate(message) {
      show('error', message, true);
    }

    function warningTranslate(message) {
      show('warning', message, true);
    }

    var show = function(type, message, isTranslate) {
      if (!isTranslate) {
        toastr[type](message);
      } else {
        $translate(message).then(function (messageTranslated) {
          toastr[type](messageTranslated);
        }, function (translationId) {
          toastr[type](translationId);
        });
      }
    };

    return {
      success: success,
      info: info,
      error: error,
      warning: warning,
      successTranslate: successTranslate,
      infoTranslate: infoTranslate,
      errorTranslate: errorTranslate,
      warningTranslate: warningTranslate,
    };
  }
})();