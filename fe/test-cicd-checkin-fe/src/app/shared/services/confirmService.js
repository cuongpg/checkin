(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('confirmService', confirmService);

  /** @ngInject */
  function confirmService($translate) {

    function show(message, yesCallback, noCallback) {
      var cf = confirm(message);
      if (cf) {
        if (yesCallback) {
          yesCallback();
        }
      } else {
        if (noCallback) {
          noCallback();
        }
      }
    }

    function showTranslate(message, yesCallback, noCallback) {
      $translate(message).then(function (messageTranslated) {
        show(messageTranslated, yesCallback, noCallback);
      }, function (translationId) {
        show(translationId, yesCallback, noCallback);
      });
    }

    return {
      show: show,
      showTranslate: showTranslate,
    };
  }
})();