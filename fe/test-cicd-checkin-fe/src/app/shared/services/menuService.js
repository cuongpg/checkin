'use strict';

angular.module('BlurAdmin.shared.services')
  .factory('menuService', menuService);

function menuService($cookies, $rootScope, $http, $q, $timeout, $state, config, apiService) {
  function initialize() {
    updateMenu();
    // var url = config.apiUrl+'../signalr';
    // var options = {
    //   transport: signalR.HttpTransportType.WebSockets,
    //   accessTokenFactory: function () { return $cookies.get('access_token'); }
    // };
    // var connection = new signalR.HubConnectionBuilder().withUrl(url,options).configureLogging(signalR.LogLevel.Information).build();
    // connection.serverTimeoutInMilliseconds = 100000000; // 1 second
    // connection.start().catch(function (err) {
    //   return console.error(err.toString());
    // }).finally(function(){
    //   updateMenu();
    // });
    // connection.on('UpdateMenuInfo',function () {
    //   updateMenu();
    // });
    // $rootScope.connection = connection;
  }
  function updateMenu() {
    var menuItems = $rootScope.currentMenuItems;
    var promises = [];
    apiService.get('TimeKeeping/CountPendingRecords', null, function (response) {
      var total = response.data;
      angular.forEach(menuItems, function (item) {
        // if (item.name == 'approve') {
        //   item.total = total.numberPendingTimeKeepingForManager + total.numberPendingApprovalForManager + total.numberPendingDestroyApprovalForManager + total.numberPendingOverTimeForManager;
        // }
        if (item.subMenu) {
          angular.forEach(item.subMenu, function (itemSub) {
            if (itemSub.name == 'attendance.irregular') {
              itemSub.total = total.numberPendingTimeKeepingForManager;
            }
            if (itemSub.name == 'attendance.overtime') {
              itemSub.total = total.numberPendingOverTimeForManager;
            }
            if (itemSub.name == 'attendance.leave') {
              itemSub.total = total.numberPendingApprovalForManager;
            }
            if (itemSub.name == 'attendance.unleave') {
              itemSub.total = total.numberPendingDestroyApprovalForManager;
            }
            if (itemSub.name == 'myAttendance.myIrregular') {
              itemSub.total = total.numberPendingTimeKeeping;
            }
            if (itemSub.name == 'myAttendance.myOvertime') {
              itemSub.total = total.numberPendingOverTime;
            }
            if (itemSub.name == 'myAttendance.myUnovertime') {
              itemSub.total = total.numberPendingUnOverTime;
            }
            if (itemSub.name == 'myAttendance.myLeave') {
              itemSub.total = total.numberPendingApproval;
            }
            if (itemSub.name == 'system.OBTConfirm') {
              itemSub.total = total.numberPendingApprovalForAdmin;
            }
          });
        }
        else {
          if (item.name == 'approve') {
            item.total = total.numberPendingTimeKeepingForManager + total.numberPendingApprovalForManager + total.numberPendingDestroyApprovalForManager + total.numberPendingOverTimeForManager + total.numberPendingChangeManagerForManager;
          }
          if (item.name == 'changeManager') {
            item.total = total.numberPendingChangeManagerForManager;
          }
        }
      });
      $rootScope.$broadcast('updateMenuItems', menuItems);
    }, function () {
      _this.isLoading = false;
    });
  }
  return {
    initialize: initialize,
    updateMenu: updateMenu
  };

}