/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.checkin')
    .controller('ApprovalModalCtrl', ApprovalModalCtrl);


  /** @ngInject */
  function ApprovalModalCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, details, urlConfirm, dataPost) {
    $scope.details = details;
    $scope.urlConfirm = urlConfirm;
    $scope.dataPost = dataPost;

    $scope.approve = function () {
      apiService.create($scope.urlConfirm, $scope.dataPost, function (response) {
        $scope.$dismiss();
        details.refresh();
      });
    }
  }
})();