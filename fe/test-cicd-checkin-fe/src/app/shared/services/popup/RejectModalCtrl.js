/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';


  angular.module('BlurAdmin.pages.checkin')
    .controller('RejectModalCtrl', RejectModalCtrl);


  /** @ngInject */
  function RejectModalCtrl($scope, apiService, $uibModalInstance, notificationService, config, loaderService, details, urlConfirm, dataPost) {
    $scope.isSavingNote = false;
    $scope.noteData = {
      note: ""
    };
    $scope.details = details;
    $scope.urlConfirm = urlConfirm;
    $scope.dataPost = dataPost;

    $scope.reject = function () {
      var note;
      if (!$scope.noteData.note) {
        return notificationService.errorTranslate('REASON_CAN_NOT_NULL');
      }
      else {
        note = encodeURIComponent($scope.noteData.note)
      }
      
      if ($scope.urlConfirm.indexOf('?') != -1) {
        $scope.urlConfirm = $scope.urlConfirm + "&rejectReason=" + note;
      }
      else {
        $scope.urlConfirm = $scope.urlConfirm + "?rejectReason=" + note;
      }
      apiService.create($scope.urlConfirm, $scope.dataPost, function (response) {
        $scope.$dismiss();
        details.refresh();
      });
    }
  }
})();