(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('permissionService', permissionService);

  /** @ngInject */
  function permissionService($rootScope, $timeout, apiService, $state, baSidebarService, menuService) {

    function loadRoutes() {
      var _this = this;
      apiService.get('oauth/routes', null, function (response) {
        _this.routes = response.data;
        var menuItems = baSidebarService.getMenuItems();
        _.forEach(_.filter($state.get(), function (o) { return _.has(o, 'permission'); }),
          function (value) {
            if (_this.isDenied(value.permission)) {
              var denied = $state.get(value.name);
              denied.abstract = true;
              $state.reload(denied);

              var indexPull = [];
              var indexSubPull = [];
              _.forEach(menuItems, function (item, index) {
                if (denied.name == item.name) {
                  indexPull.push(index);
                }
                if (item.subMenu) {
                  _.forEach(item.subMenu, function (itemSub, indexSub) {
                    if (denied.name == itemSub.name) {
                      indexSubPull.push(indexSub);
                    }
                  });
                  if (indexSubPull.length > 0) {
                    _.pullAt(item.subMenu, indexSubPull)
                  }
                }
              });
              if (indexPull.length > 0) {
                _.pullAt(menuItems, indexPull)
              }
            }
          });
          $rootScope.currentMenuItems = menuItems;
          menuService.initialize();
        $timeout(function () {
          $rootScope.$broadcast('updateMenuItems', menuItems);
        });
        $rootScope.$pageFinishedLoading = true;
      });
    }

    function isAllowed(route) {
      var _this = this;
      if (_.isArray(route)) {
        var found = false;
        _.forEach(route, function (value) {
          if (!found && (_.indexOf(_this.routes, value) !== -1)) {
            found = true;
          }
        });
        return found;
      }
      return (_.indexOf(_this.routes, route) !== -1);
    }

    function isDenied(route) {
      return !this.isAllowed(route);
    }

    return {
      loadRoutes: loadRoutes,
      isAllowed: isAllowed,
      isDenied: isDenied,
    };
  }
})();