(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('loaderService', loaderService);

  /** @ngInject */
  function loaderService($rootScope) {

    function show() {
      $rootScope.$showLoading = true;
    }

    function hide() {
      $rootScope.$showLoading = false;
    }

    return {
      show: show,
      hide: hide,
    };
  }
})();