(function () {
  'use strict';

  angular.module('BlurAdmin.shared.directives')
    .directive('searchDate', searchDate);

  /** @ngInject */
  function searchDate(config,  $filter) {

    return {
      restrict: 'E',
      scope: {
        type: '@',
        label: '@',
        column: '@',
        tableState: '=',
        refresh: '&',
      },
      templateUrl: 'app/shared/directives/search/date/searchDate.html',
      link: function (scope) {
        scope.openedDatePicker = [];
        for (var i = 0; i <= 1; i++) {
          scope.openedDatePicker.push(false);
        }
        scope.format = config.formatDate;

        scope.options = {
          showWeeks: false
        };
        scope.openDatePicker = function (i) {
          scope.openedDatePicker[i] = !scope.openedDatePicker[i];
        }
        scope.onChange = function () {
          if (_.isUndefined(scope.tableState.all)) {
            scope.tableState.all = {};
          }
          if (_.isUndefined(scope.tableState.all['date'])) {
            scope.tableState.all['date'] = [];
          }
          var indexed = _.findIndex(scope.tableState.all['date'], function (o) {
            return o.type === scope.type && _.keys(o.data)[0] === scope.column;
          });
          var data = {};
          data[scope.column] = scope.date;
          
          var datescope= $filter('date')(scope.date, 'dd/MM/yyyy');
          data[scope.column] = datescope;
          var search = { type: scope.type, data: data };
          if (indexed === -1) {
            
            scope.tableState.all['date'].push(search);
          } else {
            scope.tableState.all['date'].splice(indexed, 1, search);
          }
          scope.tableState.pagination.start = 0;
          scope.refresh();
        };
      }

    };
  }

})();