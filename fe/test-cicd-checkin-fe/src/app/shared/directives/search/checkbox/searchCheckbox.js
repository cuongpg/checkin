(function () {
  'use strict';

  angular.module('BlurAdmin.shared.directives')
    .directive('searchCheckbox', searchCheckbox);

  /** @ngInject */
  function searchCheckbox() {
    return {
      restrict: 'E',
      scope: {
        label: '@',
        data: '=',
        default: '=',
        column: '@',
        tableState: '=',
        refresh: '&',
        onChanged: '&',
      },
      templateUrl: 'app/shared/directives/search/checkbox/searchCheckbox.html',
      link: function (scope) {
        scope.onChange = function() {
          if (_.isUndefined(scope.tableState.all)) {
            scope.tableState.all = {};
          }
          if (_.isUndefined(scope.tableState.all['checkbox'])) {
            scope.tableState.all['checkbox'] = [];
          }
          var indexed = _.findIndex(scope.tableState.all['checkbox'], function (o) {
            return _.keys(o)[0] === scope.column;
          });
          var search = {};
          search[scope.column] = scope.checkbox;
          if (indexed === -1) {
            scope.tableState.all['checkbox'].push(search);
          } else {
            scope.tableState.all['checkbox'].splice(indexed, 1, search);
          }
          scope.tableState.pagination.start = 0;
          scope.refresh();
          scope.onChanged({ value: scope.checkbox });
        };
      }
    };
  }

})();