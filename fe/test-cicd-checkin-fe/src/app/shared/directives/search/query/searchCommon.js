(function () {
  'use strict';

  angular.module('BlurAdmin.shared.directives')
    .directive('searchCommon', searchCommon);

  /** @ngInject */
  function searchCommon() {
    return {
      restrict: 'E',
      scope: {
        columns: '=',
        tableState: '=',
        refresh: '&',
      },
      templateUrl: 'app/shared/directives/search/query/searchCommon.html',
      link : function (scope) {
        scope.$watch('query', function (val) {
          if (!_.isUndefined(val)) {
            var query = {};
            _.forEach(scope.columns, function(column) {
              query[column] = val;
            });
            if (_.isUndefined(scope.tableState.all)) {
              scope.tableState.all = {};
            }
            scope.tableState.all['query'] = query;
            scope.tableState.pagination.start = 0;
            scope.refresh();
          }
        });
      }
    };
  }

})();