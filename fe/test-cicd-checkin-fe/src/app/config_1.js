'use strict';

angular.module('BlurAdmin')
  .constant('config', {
    //apiUrl: 'http://aitask.net:58478/api/',
	  apiUrl: 'http://localhost:10161/api/',
    dateFormat: 'dd/MM/yyyy HH:mm:ss',
    topicaPosition: {latitude: 21.0033379, longitude: 105.83620289999999},
    distanceInsideTopica : 200
  });