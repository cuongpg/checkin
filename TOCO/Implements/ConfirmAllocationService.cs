﻿//using MongoDB.Driver;
//using TOCO.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace TOCO.Implements
//{
//    public class ConfirmAllocationService : BaseService
//    {
//        public ConfirmAllocationService(ApplicationDbContext context) : base(context)
//        {
//        }

//        public ConfirmAllocation Create(ConfirmAllocation confirmAllocation)
//        {

//            confirmAllocation.CreatedAt = DateTime.Now;
//            confirmAllocation.UpdatedAt = DateTime.Now;
//            context.ConfirmAllocations.Add(confirmAllocation);
//            context.SaveChanges();
//            return confirmAllocation;
//        }

//        public int Update(ConfirmAllocation confirmAllocation)
//        {
//            confirmAllocation.UpdatedAt = DateTime.Now;
//            context.ConfirmAllocations.Update(confirmAllocation);
//            return context.SaveChanges();
//        }

//        public ConfirmAllocation GetLateReportByUserId(int userId)
//        {
//            return context.ConfirmAllocations
//                .Where(c => c.Status == ConfirmAllocationStatus.ApprovalReport)
//                .Where(c => c.UserId == userId)
//                .OrderByDescending(c => c.UpdatedAt)
//                .FirstOrDefault();
//        }

//        public ConfirmAllocation GetLateReportById(int recordId)
//        {
//            return context.ConfirmAllocations
//                .Where(c => c.Id == recordId)
//                .FirstOrDefault();
//        }

//        public List<ConfirmAllocation> GetReportByUserAndStatus(int userId, ConfirmAllocationStatus status)
//        {
//            return context.ConfirmAllocations
//                .Where(c => c.UserId == userId)
//                .Where(c => c.Status == status)
//                .ToList();
//        }

//        public List<ConfirmAllocation> GetActiveReports(DateTime startDate, DateTime endDate)
//        {
//            return context.ConfirmAllocations
//                .Where(c => c.CreatedAt <= endDate && c.CreatedAt >= startDate)
//                .Where(c => c.Status != ConfirmAllocationStatus.Destroy && c.Status != ConfirmAllocationStatus.Expired)
//                .GroupBy(c => c.UserId)
//                .Select(g => g.OrderByDescending(c => c.CreatedAt).First())
//                .ToList();
//        }

//        public ConfirmAllocation GetWaitReportByUserId(int userId)
//        {
//            return context.ConfirmAllocations
//               .Where(c => c.Status == ConfirmAllocationStatus.Waiting)
//               .Where(c => c.UserId == userId)
//               .OrderByDescending(c => c.UpdatedAt)
//               .FirstOrDefault();
//        }

//        public List<ConfirmAllocation> GetErrorReport()
//        {
//            return context.ConfirmAllocations
//              .Where(c => c.Status == ConfirmAllocationStatus.Reporting)
//              .Where(c => c.HrOperResult != "OK")
//              .OrderByDescending(c => c.UpdatedAt)
//              .ToList();
//        }
//    }
//}
