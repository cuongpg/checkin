﻿using TOCO.API;
using System.Collections.Generic;
using TOCO.Models;

namespace TOCO.Implements
{
    public interface IFireBaseMesageService
    {
        bool SendPushNotification(List<string> listToken, Notification notification, int badge);
    }

    public class FireBaseMessageService : IFireBaseMesageService
    {
        public FireBaseMessageService()
        {
        }

        public bool SendPushNotification(List<string> listToken, Notification notification, int badge)
        {
            bool bOk = true;
            if (listToken.Count > 0)
            {
                bOk = FcmAPI.Connection.SendPushNotification(listToken, notification, badge);
            }

            return bOk;
        }
    }
}
