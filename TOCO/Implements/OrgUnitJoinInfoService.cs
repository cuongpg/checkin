﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Helpers;
using TOCO.Models.Pagination;
using TOCO.Common;

namespace TOCO.Implements
{
    public class OrgUnitJoinInfoService : BaseService
    {
        public OrgUnitJoinInfoService(ApplicationDbContext context) : base(context)
        {
        }


        public OrgUnitJoinInfo GetOrgUnitJoinInfo(int id)
        {
            return context.OrgUnitJoinInfos.FirstOrDefault(e => e.Id == id);
        }

        public List<OrgUnitJoinInfo> GetOrgUnitJoinInfo(int userId, string shortCode, DateTime start, DateTime end)
        {
            return context.OrgUnitJoinInfos
                .Where(org => org.UserId == userId)
                .Where(org => org.ShortCode == shortCode)
                .Where(org => org.StartDate <= end && org.EndDate >= start)
                .Where(org => org.Status == OrgUnitJoinInfoStatus.Active)
                .ToList();
        }

        public OrgUnitJoinInfo Create(OrgUnitJoinInfo orgUnitJoinInfo)
        {
            orgUnitJoinInfo.CreatedAt = DateTime.Now;
            orgUnitJoinInfo.UpdatedAt = DateTime.Now;
            context.OrgUnitJoinInfos.Add(orgUnitJoinInfo);
            context.SaveChanges();
            return orgUnitJoinInfo;
        }

        public OrgUnitJoinInfo Update(OrgUnitJoinInfo orgUnitJoinInfo)
        {
            orgUnitJoinInfo.UpdatedAt = DateTime.Now;
            context.OrgUnitJoinInfos.Update(orgUnitJoinInfo);
            context.SaveChanges();
            return orgUnitJoinInfo;
        }
        
    }
}
