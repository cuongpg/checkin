﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Helpers;
using TOCO.Models.Pagination;
using TOCO.Common;

namespace TOCO.Implements
{
    public class EstimateOwnerService : BaseService
    {
        public EstimateOwnerService(ApplicationDbContext context) : base(context)
        {
        }

        public List<EstimateOwner> SearchEstimateOwner(string keyWord)
        {
            
            int limit = int.Parse(AppSetting.Configuration.GetValue("Meta:Limit"));
            IQueryable<EstimateOwner> estimateOwners = context.EstimateOwners.Where(e =>e.Active == 1);
            if (!string.IsNullOrEmpty(keyWord))
                estimateOwners = estimateOwners.Where(e => e.CompleteCode.Contains(keyWord.Trim()));
            return estimateOwners.Take(limit).ToList();
        }

        public EstimateOwner GetById(string id)
        {
            return context.EstimateOwners.FirstOrDefault(e => e.Id == id);
        }

        public EstimateOwner GetByCode(string code)
        {
            return context.EstimateOwners.FirstOrDefault(e => e.CompleteCode == code);
        }

        public EstimateOwner GetByShortCode(string code)
        {
            return context.EstimateOwners.FirstOrDefault(e => e.CompleteCode.EndsWith(code));
        }

        public EstimateOwner Create(EstimateOwner estimateOwner)
        {
            estimateOwner.CreatedAt = DateTime.Now;
            estimateOwner.UpdatedAt = DateTime.Now;
            context.EstimateOwners.Add(estimateOwner);
            context.SaveChanges();
            return estimateOwner;
        }

        public EstimateOwner Update(EstimateOwner estimateOwner)
        {
            estimateOwner.UpdatedAt = DateTime.Now;
            context.EstimateOwners.Update(estimateOwner);
            context.SaveChanges();
            return estimateOwner;
        }

        public bool ValidateEstimateOwerId(string estimateOwnerId)
        {
            return GetById(estimateOwnerId) != null;
        }
    }
}
