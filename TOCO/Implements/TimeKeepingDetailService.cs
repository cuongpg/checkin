﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Models;
using TOCO.Models.ResultModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Helpers;
using TOCO.Models.Pagination;

namespace TOCO.Implements
{


    public class TimeKeepingDetailService : BaseService
    {

        public TimeKeepingDetailService(ApplicationDbContext context) : base(context)
        {
        }

        public List<TimeKeepingDetail> GetForgetCheckOut()
        {
            return context.TimeKeepingDetails
                .Include(td => td.TimeKeeping)
                .ThenInclude(d => d.User)
                .Where(t => t.CheckOutTime == null
                && t.TimeKeeping.WorkDate < DateTime.Now.Date
                && t.Status != StatusTimeKeepingApproval.Destroy
                && t.Status != StatusTimeKeepingApproval.Rejected)
                .ToList();
        }

        public TimeKeepingDetail GetTimekeepingWaitCheckout(int timeKeepingId)
        {
            return context.TimeKeepingDetails.Where(t => t.CheckOutTime == null && t.TimeKeepingId == timeKeepingId && t.Status != StatusTimeKeepingApproval.Destroy && t.Status != StatusTimeKeepingApproval.Rejected).FirstOrDefault();
        }

        public (bool, string) AddNote(string email, string note, TimeKeeping timeKeeping)
        {
            bool bOK = false;
            string status = string.Empty;
            //TimeKeeping timeKeeping = timeKeepingService.GetByWorkDate(email, workDate);
            if (timeKeeping != null)
            {
                // Lấy danh sách chấm công tạm thời hợp lệ cuối cùng trong ngày
                TimeKeepingDetail timeKeepingDetail = timeKeeping.TimeKeepingDetails.Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy).OrderByDescending(t => t.CreatedAt).FirstOrDefault();
                if(timeKeepingDetail != null)
                {
                    timeKeepingDetail.Note += string.IsNullOrEmpty(timeKeepingDetail.Note) ? note : "\n" + note;
                    bOK = Update(timeKeepingDetail) != null;
                    status = ResultCode.ADDNOTE_SUCCESS;
                }
                else
                {
                    bOK = true;
                    status = ResultCode.CHECKIN_NOT_YET;
                }
            }
            else
            {
                bOK = true;
                status = ResultCode.CHECKIN_NOT_YET;
            }
            return (bOK, status);
        }

        public TimeKeepingDetail GetById(int id)
        {
            return context.TimeKeepingDetails.FirstOrDefault(t => t.Id == id);
        }

        public TimeKeepingDetail Create(TimeKeepingDetail timeKeepingDetail)
        {
            timeKeepingDetail.CreatedAt = DateTime.Now;
            timeKeepingDetail.UpdatedAt = DateTime.Now;
            context.TimeKeepingDetails.Add(timeKeepingDetail);
            context.SaveChanges();
            return timeKeepingDetail;
        }

        public TimeKeepingDetail Update(TimeKeepingDetail timeKeepingDetail)
        {
            timeKeepingDetail.UpdatedAt = DateTime.Now;
            context.TimeKeepingDetails.Update(timeKeepingDetail);
            context.SaveChanges();
            return timeKeepingDetail;
        }

        public bool Delete(int id)
        {
            var model = GetById(id);

            if (model == null)
            {
                return false;
            }

            context.TimeKeepingDetails.Remove(model);
            context.SaveChanges();

            return true;
        }

        public (bool bOk, string resultCode, TimeKeepingDetail[] timeKeepingDetails) ConfirmAbnormalTimeKeeping(StatusTimeKeepingApproval status, int[] ids, User user, string rejectReason)
        {
            string message = string.Empty;
            bool bRes = false;
            TimeKeepingDetail[] details = context.TimeKeepingDetails
                .Include(td => td.TimeKeeping)
                .ThenInclude(t => t.User)
                .Where(t => t.TimeKeeping.User.ManagerId == user.Id && ids.Contains(t.Id) && t.Status == StatusTimeKeepingApproval.Pending).ToArray();

            if (details.Count() == 0)
            {
                message = ResultCode.TIME_KEEPING_NOT_FOUND;
            }
            else
            {
                try
                {
                    foreach (TimeKeepingDetail detail in details)
                    {
                        bRes = true;
                        if (DateTime.Now > detail.TimeKeeping.WorkDate)
                        {
                            bRes = false;
                            message = ResultCode.TIME_KEEPING_NOT_FOUND;
                        }
                        else
                        {
                            detail.ApprovalTime = DateTime.Now;
                            detail.ApproverEmail = user.Email;
                            detail.Status = status;
                            if (!string.IsNullOrEmpty(rejectReason))
                            {
                                detail.RejectReason = rejectReason;
                            }
                            Update(detail);
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    message = ex is DbUpdateException || ex is DbUpdateConcurrencyException ? ResultCode.DB_EXCEPTION : ResultCode.OTHER_EXCEPTION;
                }
            }

            return (bRes, message, details);
        }

        public List<TimeKeepingDetail> GetTimeKeepingDetailInMonth(User user, int month, int year)
        {
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
            List<TimeKeepingDetail> timeKeepingDetails = context.TimeKeepingDetails.Include(td => td.TimeKeeping)
                .Where(td => td.TimeKeeping.UserId == user.Id && td.TimeKeeping.WorkDate.Date >= startDate && td.TimeKeeping.WorkDate.Date <= endDate)
                .Where(td => td.Status != StatusTimeKeepingApproval.Destroy && td.Status != StatusTimeKeepingApproval.Rejected)
                .ToList();

            return timeKeepingDetails;
        }

        public IEnumerable<TimeKeepingDetail> GetPendingTimeKeepingLastMonth(DateTime startDate, DateTime endDate, string corporationType)
        {
            return context.TimeKeepingDetails
                .Where(tkd => tkd.Status == StatusTimeKeepingApproval.Pending)
                .Join(context.TimeKeepings, tkd => tkd.TimeKeepingId, tk => tk.Id, (tkd, tk) => new
                {
                    Detail = tkd,
                    tk.ScriptId,
                    tk.WorkDate
                })
                .Where(tkd_tk_s => tkd_tk_s.WorkDate >= startDate && tkd_tk_s.WorkDate <= endDate)
                .Join(context.Scripts, tkd_tk => tkd_tk.ScriptId, s => s.Id, (tkd_tk, s) => new
                {
                    tkd_tk.Detail,
                    tkd_tk.WorkDate,
                    s.CorporationType
                })
                .Where(tkd_tk_s => tkd_tk_s.CorporationType == corporationType)
                .Select(tkd_tk_s => tkd_tk_s.Detail).ToList();
        }

        public int CountPendingTimeKeeping(User user, bool isForManager)
        {
            IQueryable<TimeKeepingDetail> source = context.TimeKeepingDetails
                .Include(td => td.TimeKeeping)
                .ThenInclude(t => t.User)
                .Where(t => t.Status == StatusTimeKeepingApproval.Pending);
            source = isForManager ? source.Where(t => t.TimeKeeping.User.ManagerId == user.Id) : source.Where(t => t.TimeKeeping.UserId == user.Id);
            return source.Count();
        }

        public int CountRejectTimeKeepingDetailsInMonth(int userId, DateTime startDate, DateTime endDate)
        {
            int count = context.TimeKeepingDetails.Include(td => td.TimeKeeping)
                .Where(td => td.TimeKeeping.UserId == userId && td.TimeKeeping.WorkDate.Date >= startDate && td.TimeKeeping.WorkDate.Date <= endDate)
                .Where(td => td.Status == StatusTimeKeepingApproval.Rejected)
                .Count();

            return count;
        }

        public List<TimeKeepingDetail> GetFinishTimeKeepingByWorkDate(int timeKeepingId)
        {
            return context.TimeKeepingDetails
                .Where(td => td.TimeKeepingId == timeKeepingId && td.CheckOutTime != null)
                .Where(td => td.Status != StatusTimeKeepingApproval.Destroy && td.Status != StatusTimeKeepingApproval.Rejected)
                .ToList();
        }

        public List<TimeKeepingDetail> GetTimeKeepingDetailInDay(int timeKeepingId)
        {
            return context.TimeKeepingDetails
                .Where(td => td.TimeKeepingId == timeKeepingId)
                .Where(td => td.Status != StatusTimeKeepingApproval.Destroy && td.Status != StatusTimeKeepingApproval.Rejected)
                .ToList();
        }

        public List<TimeKeepingDetail> GetApprovedTimeKeepingDetailInDay(int timeKeepingId)
        {
            return context.TimeKeepingDetails
                .Where(td => td.TimeKeepingId == timeKeepingId)
                .Where(td => td.Status == StatusTimeKeepingApproval.Done || td.Status == StatusTimeKeepingApproval.Approved)
                .ToList();
        }

        public List<TimeKeepingDetail> GetAbnormalTimeKeepingDetail(out int numberOfPages, out int totalRecords, STRequestModel request, User user, bool isForManager)
        {
            numberOfPages = 1;
            totalRecords = 0;

            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();

            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();


            IQueryable<TimeKeepingDetail> source = context.TimeKeepingDetails.Include(td => td.TimeKeeping).ThenInclude(t => t.User);

            source = isForManager ? source.Where(t => t.TimeKeeping.User.ManagerId == user.Id) : source.Where(t => t.TimeKeeping.UserId == user.Id);

            if (statusFilters.Count > 0)
            {
                source = source.Where(t => (int)t.Status == statusFilters[0].Status);
            }
            else
            {
                source = source.Where(t => t.Status != StatusTimeKeepingApproval.Done);
            }

            foreach (DateSearch date in listDateSearch)
            {
                SearchDateObject searchDate = date.GetData<SearchDateObject>();
                if (!string.IsNullOrEmpty(searchDate.StartAt))
                {
                    source = source.Where(t => t.TimeKeeping.WorkDate.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
                if (!string.IsNullOrEmpty(searchDate.EndAt))
                {
                    source = source.Where(t => t.TimeKeeping.WorkDate.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
            }

            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo trạng thái Pending => Approval => Rejected
            //source = source.OrderBy(ar => ar.Status);
            // Sort theo thời gian tạo
            source = source.OrderByDescending(tk => tk.UpdatedAt.HasValue ? tk.UpdatedAt : tk.CreatedAt);
            if (sorts != null)
            {
                foreach ((string key, bool value) in sorts)
                {
                    switch (key)
                    {
                        case "status":
                            source = value ? source.OrderBy(ar => ar.Status) : source.OrderByDescending(ar => ar.Status);
                            break;
                        case "name":
                            source = value ? source.OrderBy(t => t.TimeKeeping.User.FullName) : source.OrderByDescending(t => t.TimeKeeping.User.FullName);
                            break;
                        case "email":
                            source = value ? source.OrderBy(t => t.TimeKeeping.User.Email) : source.OrderByDescending(t => t.TimeKeeping.User.Email);
                            break;
                        case "date":
                            source = value ? source.OrderBy(t => t.TimeKeeping.WorkDate) : source.OrderByDescending(t => t.TimeKeeping.WorkDate);
                            break;
                        default:
                            break;
                    }
                }
            }
            source = source.Skip(pagination.Start).Take(pagination.Number);
            var a = source.ToList();
            return source.ToList();
        }

        public IEnumerable<(int UserId, TimeKeepingDetail Detail, DateTime WorkDate, string HCMWorkScheduleRule)> GetTimeKeepingByTime(string corporationType, int employeeTypeId, DateTime start, DateTime end)
        {
            IEnumerable<dynamic> result = context.TimeKeepingDetails
                .Where(tkd => tkd.Status == StatusTimeKeepingApproval.Approved || tkd.Status == StatusTimeKeepingApproval.Done)
                .Join(context.TimeKeepings, tkd => tkd.TimeKeepingId, tk => tk.Id, (tkd, tk) => new
                {
                    Detail = tkd,
                    tk.WorkDate,
                    tk.UserId,
                    ScriptIdTimeKeeping = tk.ScriptId
                })
                .Where(tkd_tk => tkd_tk.WorkDate >= start && tkd_tk.WorkDate <= end)
                .Join(context.HrbHistories, tkd_tk => tkd_tk.UserId, hrb => hrb.UserId, (tkd_tk, hrb) => new
                {
                    tkd_tk.Detail,
                    tkd_tk.UserId,
                    tkd_tk.WorkDate,
                    tkd_tk.ScriptIdTimeKeeping,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(tkd_tk_hrb => !(tkd_tk_hrb.EffectiveDate > end || tkd_tk_hrb.ExpirationDate < start))
                .Where(tkd_tk_hrb => tkd_tk_hrb.HrbStatus == HrbStatus.Active)
                .Join(context.ScriptHistorys, tkd_tk => tkd_tk.UserId, sh => sh.UserId, (tkd_tk, sh) => new
                {
                    tkd_tk.Detail,
                    tkd_tk.UserId,
                    tkd_tk.WorkDate,
                    tkd_tk.ScriptIdTimeKeeping,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(tkd_tk_sh => tkd_tk_sh.WorkDate >= tkd_tk_sh.EffectiveDate && tkd_tk_sh.WorkDate <= tkd_tk_sh.ExpirationDate)
                .Where(tkd_tk_sh => tkd_tk_sh.ScriptId == tkd_tk_sh.ScriptIdTimeKeeping)
                .Where(tkd_tk_sh => !(tkd_tk_sh.EffectiveDate > end || tkd_tk_sh.ExpirationDate < start))
                .Join(context.Scripts, tkd_tk_sh => tkd_tk_sh.ScriptId, s => s.Id, (tkd_tk_sh, s) => new
                {
                    tkd_tk_sh.UserId,
                    tkd_tk_sh.Detail,
                    tkd_tk_sh.WorkDate,
                    tkd_tk_sh.ScriptId,
                    s.EmployeeTypeId,
                    s.HCMWorkScheduleRule,
                    s.NoNeedTimeKeeping,
                    s.CorporationType
                })
                .Where(tkd_tk_sh_s => tkd_tk_sh_s.CorporationType == corporationType)
                .Where(tkd_tk_sh_s => tkd_tk_sh_s.EmployeeTypeId == employeeTypeId)
                .Where(tkd_tk_sh_s => tkd_tk_sh_s.NoNeedTimeKeeping == false);
            foreach (dynamic item in result)
            {
                yield return (item.UserId, item.Detail, item.WorkDate, item.HCMWorkScheduleRule);
            }
        }
    }
}
