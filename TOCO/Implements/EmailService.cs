﻿using MongoDB.Driver;
using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class EmailService : BaseService
    {
        public EmailService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Email> GetEmailByEmailTo(string email)
        {
            return context.Emails.Where(e => e.EmailTo == email).ToList();
        }

        public int Create(Email email)
        {

            email.CreatedAt = DateTime.Now;
            email.UpdatedAt = DateTime.Now;
            context.Emails.Add(email);

            return context.SaveChanges();
        }

        public int Update(Email email)
        {
            email.UpdatedAt = DateTime.Now;
            context.Emails.Update(email);
            return context.SaveChanges();
        }

        public List<Email> GetEmailsByStatus(EmailStatus status)
        {
            return context.Emails.Where(e => e.Status == status).ToList();
        }

        public Email GetById(int id)
        {
            return context.Emails.Where(e => e.Id == id).FirstOrDefault();
        }
    }
}
