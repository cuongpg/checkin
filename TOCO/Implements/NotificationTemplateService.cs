﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;

namespace TOCO.Implements
{
    public class NotificationTemplateService : BaseService
    {
        public NotificationTemplateService(ApplicationDbContext context) : base(context)
        {

        }

        public IEnumerable<NotificationTemplate> GetAllNotificationTemplate(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;

            IQueryable<NotificationTemplate> query = QueryableHelper.Default(context.NotificationTemplates);
            STHelper.Process<NotificationTemplate, NotificationPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public NotificationTemplate GetTemplateByCode(string templateCode)
        {
            return context.NotificationTemplates.Where(et => et.Code == templateCode).FirstOrDefault();
        }

        public void Delete(int id)
        {
            var template = context.NotificationTemplates.Find(id);
            context.NotificationTemplates.Remove(template);
            context.SaveChanges();
        }

        public NotificationTemplate GetTemplateById(int id)
        {
            return context.NotificationTemplates.Find(id);
        }

        public NotificationTemplate Update(NotificationTemplate template)
        {
            template.UpdatedAt = DateTime.Now;
            context.SaveChanges();
            return template;
        }

        public NotificationTemplate Create(NotificationTemplate template)
        {
            var data = new NotificationTemplate
            {
                Name = template.Name,
                Title = template.Title,
                Code = template.Code,
                Message = template.Message,
                Parameters = template.Parameters,
                DocumentType = template.DocumentType,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            context.NotificationTemplates.Add(data);
            context.SaveChanges();
            return data; 
        }
    }
}
