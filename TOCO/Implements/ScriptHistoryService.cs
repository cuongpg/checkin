﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class ScriptHistoryService : BaseService
    {
        public ScriptHistoryService(ApplicationDbContext context) : base(context)
        {
        }

        public ScriptHistory GetById(int id)
        {
            return context.ScriptHistorys.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<ScriptHistory> GetAllByUser(int userId)
        {
            return context.ScriptHistorys
                .Where(s => s.UserId == userId).OrderBy(a => a.ExpirationDate);
        }

        public IEnumerable<ScriptHistory> GetByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        {
            return context.ScriptHistorys
                .Where(s => s.UserId == userId)
                .Where(s => !(s.EffectiveDate > endTime || s.ExpirationDate < startTime));
        }

        public IEnumerable<ScriptHistory> GetFutureHistories(int userId, DateTime time)
        {
            return context.ScriptHistorys
                .Where(s => s.UserId == userId)
                .Where(s => s.EffectiveDate > time);
        }

        public ScriptHistory Create(ScriptHistory history)
        {
            history.CreatedAt = DateTime.Now;
            history.UpdatedAt = DateTime.Now;
            context.ScriptHistorys.Add(history);
            context.SaveChanges();
            return history;
        }

        public ScriptHistory Update(ScriptHistory history)
        {
            history.UpdatedAt = DateTime.Now;
            context.ScriptHistorys.Update(history);
            context.SaveChanges();
            return history;
        }

        public bool Delete(int id)
        {
            var history = GetById(id);

            if (history == null)
            {
                return false;
            }

            context.ScriptHistorys.Remove(history);
            context.SaveChanges();

            return true;
        }

        public IEnumerable<int> GetUserIdsByScript(int scriptId, DateTime startTime, DateTime endTime)
        {
            return context.ScriptHistorys
                .Where(s => s.ScriptId == scriptId)
                .Where(s => !(s.EffectiveDate > endTime || s.ExpirationDate < startTime))
                .Select(s => s.UserId)
                .Distinct();
        }

        public ScriptHistory GetScriptInDay(int userId, DateTime day)
        {
            return context.ScriptHistorys
                 .Where(s => s.UserId == userId)
                 .Where(s => s.EffectiveDate <= day.Date && s.ExpirationDate >= day.Date)
                 .FirstOrDefault();
        }

        public IEnumerable<(int UserId, int ScriptId, DateTime EffectiveDate, DateTime ExpirationDate, string HCMWorkScheduleRule, int LunchBreak, bool HasHoliday, bool HasLeaveWeekend, bool CheckInOnly, bool NoNeedTimeKeeping, int? HourOfWorkDay)> GetByTime(DateTime start, DateTime end)
        {
            IQueryable<dynamic> result = context.ScriptHistorys
                .Where(sh => !(sh.ExpirationDate < start || sh.EffectiveDate > end))
                .Join(context.Scripts, sh => sh.ScriptId, s => s.Id, (sh, s) => new
                {
                    sh.UserId,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate,
                    s.HCMWorkScheduleRule,
                    s.LunchBreak,
                    s.HasHoliday,
                    s.HasLeaveWeekend,
                    s.CheckInOnly,
                    s.HourOfWorkDay,
                    s.NoNeedTimeKeeping
                });
            foreach (dynamic item in result)
            {
                yield return (item.UserId, item.ScriptId, item.EffectiveDate, item.ExpirationDate, item.HCMWorkScheduleRule, item.LunchBreak, item.HasHoliday, item.HasLeaveWeekend, item.CheckInOnly, item.NoNeedTimeKeeping, item.HourOfWorkDay);
            }
        }

    }
}
