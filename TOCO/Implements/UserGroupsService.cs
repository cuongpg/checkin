﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Implements
{
    public class UserGroupsService : BaseService
    {
        public UserGroupsService(ApplicationDbContext context) : base(context)
        {
        }

        public List<User> GetAll(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;
            IQueryable<User> query = QueryableHelper.Default(context.Users);
            STHelper.Process<User, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public UserGroup Create(UserGroup userGroup)
        {
            context.UserGroup.Add(userGroup);
            context.SaveChanges();
            return userGroup;
        }

        public UserGroup Update(UserGroup userGroup)
        {
            context.UserGroup.Update(userGroup);
            context.SaveChanges();
            return userGroup;
        }

        

        public bool Delete(UserGroup userGroup)
        {
            context.UserGroup.Remove(userGroup);
            context.SaveChanges();
            return true;
        }

        public List<UserGroup> GetByUserId(int userId)
        {
            IQueryable<UserGroup> userGroups = context.UserGroup.Where(ug => ug.UserId == userId);
            return userGroups.ToList();
        }

        public bool RemoveAllGroupByUserId(int userId)
        {
            IQueryable<UserGroup> userGroups = context.UserGroup.Where(ug => ug.UserId == userId);
            context.UserGroup.RemoveRange(userGroups.ToList());
            context.SaveChanges();
            return true;
        }

        
        
    }
}
