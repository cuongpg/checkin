﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class MechanismService : BaseService
    {
        public MechanismService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Mechanism> GetAll()
        {
            return context.Mechanisms.ToList();
        }

       
        public Mechanism GetByName(string name)
        {
            return context.Mechanisms.Where(l => l.MechanismName == name).FirstOrDefault();
        }

        public Mechanism GetByHCMCode(string hcmCode)
        {
            return context.Mechanisms.Where(l => l.MechanismHCMCode == hcmCode).FirstOrDefault();
        }

        public Mechanism GetByHCMId(string hILFM)
        {
            return context.Mechanisms.Where(l => l.MechanismHCMId == hILFM).FirstOrDefault();
        }
    }
}
