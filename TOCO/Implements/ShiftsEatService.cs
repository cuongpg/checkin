﻿using TOCO.Common;
using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Implements
{
    public class ShiftsEatService : BaseService
    {
        public ShiftsEatService(ApplicationDbContext context) : base(context)
        {
        }

        public ShiftsEat GetById(int id)
        {
            return context.ShiftsEats.FirstOrDefault(s => s.Id == id);
        }

        public ShiftsEat GetByWorkDate(string email, DateTime workDate)
        {
            return context.ShiftsEats.FirstOrDefault(s => s.WorkDate == workDate && s.User.Email == email);
        }

        public ShiftsEat Create(ShiftsEat shiftsEat)
        {
            shiftsEat.CreatedAt = DateTime.Now;
            context.ShiftsEats.Add(shiftsEat);
            context.SaveChanges();

            return shiftsEat;
        }

        public List<ShiftsEat> GetShiftEatsInMonth(User user, DateTime startDate, DateTime endDate)
        {
            List<ShiftsEat> shiftsEats = null;
            shiftsEats = context.ShiftsEats
                .Where(s => s.UserId == user.Id && s.WorkDate.Date >= startDate && s.WorkDate.Date <= endDate)
                .ToList();

            return shiftsEats;
        }
    }
}
