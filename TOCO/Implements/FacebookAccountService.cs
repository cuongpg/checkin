﻿using Microsoft.EntityFrameworkCore;
using NetTools;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;


namespace TOCO.Implements
{
    public class FacebookAccountService : BaseService
    {

        public FacebookAccountService(ApplicationDbContext context) : base(context)
        {
        }

        public FacebookAccount GetByUserId(int userId)
        {
            return context.FacebookAccounts.FirstOrDefault(f => f.UserId == userId);
        }


        public List<FacebookAccount> GetAll()
        {
            return context.FacebookAccounts.ToList();
        }

        public FacebookAccount Create(FacebookAccount config)
        {
            config.CreatedAt = DateTime.Now;
            config.UpdatedAt = DateTime.Now;
            context.FacebookAccounts.Add(config);
            context.SaveChanges();
            return config;
        }

        public FacebookAccount Update(FacebookAccount config)
        {
            config.UpdatedAt = DateTime.Now;
            context.FacebookAccounts.Update(config);
            context.SaveChanges();
            return config;
        }

        public bool Delete(int id)
        {
            var config = GetByUserId(id);

            if (config == null)
            {
                return false;
            }

            context.FacebookAccounts.Remove(config);
            context.SaveChanges();

            return true;
        }

      
    }
}
