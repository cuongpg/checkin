﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class UnitRoleService : BaseService
    {
        public UnitRoleService(ApplicationDbContext context) : base(context)
        {
        }

        public List<UnitRole> GetAll()
        {
            return context.UnitRoles.ToList();
        }

       
        public UnitRole GetByName(string name)
        {
            return context.UnitRoles.Where(l => l.Name == name).FirstOrDefault();
        }

        public UnitRole GetByHCMCode(string hcmCode)
        {
            return context.UnitRoles.Where(l => l.HCMCode == hcmCode).FirstOrDefault();
        }
    }
}
