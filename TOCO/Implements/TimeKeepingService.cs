﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Implements
{
    public class TimeKeepingService : BaseService
    {
        public TimeKeepingService(ApplicationDbContext context) : base(context)
        {
        }

        public TimeKeeping GetByEmailAndWorkDate(string email, DateTime workDate)
        {
            return context.TimeKeepings.Include(t => t.TimeKeepingDetails)
                .FirstOrDefault(s => s.WorkDate.Date == workDate.Date && s.User.Email == email);
        }

        public TimeKeeping Create(TimeKeeping timeKeeping)
        {
            timeKeeping.CreatedAt = DateTime.Now;
            timeKeeping.UpdatedAt = DateTime.Now;
            context.TimeKeepings.Add(timeKeeping);
            context.SaveChanges();

            return timeKeeping;
        }

        public bool Delete(string email, DateTime workDate)
        {
            var model = GetByEmailAndWorkDate(email, workDate);

            if (model == null)
            {
                return false;
            }

            context.TimeKeepings.Remove(model);
            context.SaveChanges();

            return true;
        }
       
        public TimeKeeping Create(User user, int scriptId, DateTime workDate)
        {
            TimeKeeping timeKeeping = GetTimeKeepingOfEmployeeByWorkDate(user.Id, workDate);
            if (user == null)
            {
                return null;
            }
            if (timeKeeping == null)
            {
                timeKeeping = new TimeKeeping()
                {
                    WorkDate = workDate,
                    UserId = user.Id,
                    ScriptId = scriptId
                };
                return Create(timeKeeping);
            }
            else
            {
                return timeKeeping;
            }
        }

        public TimeKeeping GetTimeKeepingOfEmployeeByWorkDate(int userId, DateTime workDate)
        {
            return context.TimeKeepings.Where(t => t.UserId == userId && t.WorkDate.Date == workDate.Date).FirstOrDefault();
        }

        public List<TimeKeeping> GetTimeKeepingInMonth(User user, Script script, DateTime startDate, DateTime endDate)
        {
            List<TimeKeeping> timeKeepings = context.TimeKeepings.Include(tk => tk.TimeKeepingDetails)
                .Where(tk => tk.UserId == user.Id && tk.ScriptId == script.Id && tk.WorkDate.Date >= startDate && tk.WorkDate.Date <= endDate)
                .ToList();

            return timeKeepings;
        }

        public TimeKeeping Update(TimeKeeping timeKeeping)
        {
            if (timeKeeping == null)
            {
                return null;
            }
            timeKeeping.UpdatedAt = DateTime.Now;
            context.TimeKeepings.Update(timeKeeping);
            context.SaveChanges();
            return timeKeeping;
        }
    }
}
