﻿using MongoDB.Driver;
using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace TOCO.Implements
{
    public class FirebaseTokenService : BaseService
    {
        public FirebaseTokenService(ApplicationDbContext context) : base(context)
        {
        }

        public List<FirebaseToken> GetFirebaseTokensByEmail(string email)
        {
            return context.FirebaseTokens.AsNoTracking().Where(ft => ft.Email == email).ToList();
        }

        public int InsertFireBaseToken(string email, string token, string deviceId)
        {
            int result = 0;
            try
            {
                FirebaseToken firebaseToken = context.FirebaseTokens.AsNoTracking().FirstOrDefault(ft => ft.DeviceId == deviceId);
                if (firebaseToken != null)
                {
                    firebaseToken.Email = email;
                    firebaseToken.Token = token;
                    firebaseToken.UpdatedAt = DateTime.Now;
                    context.FirebaseTokens.Update(firebaseToken);
                    //context.FirebaseTokens.Remove(firebaseToken);
                }
                else
                {
                    firebaseToken = new FirebaseToken
                    {
                        Token = token,
                        DeviceId = deviceId,
                        Email = email,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };
                    context.FirebaseTokens.Add(firebaseToken);
                }
                result = context.SaveChanges();
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("Violation of UNIQUE KEY constraint"))
                {
                    throw ex;
                }
            }

            return result;
        }

        public int DeleteFireBaseToken(string deviceId)
        {

            FirebaseToken firebaseToken = GetByDeviceId(deviceId);

            if (firebaseToken == null)
            {
                return -1;
            }

            context.FirebaseTokens.Remove(firebaseToken);
            return context.SaveChanges();
        }

        public FirebaseToken GetByDeviceId(string deviceId)
        {
            return context.FirebaseTokens.FirstOrDefault(ft => ft.DeviceId == deviceId);
        }
    }
}
