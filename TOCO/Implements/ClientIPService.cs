﻿using Microsoft.EntityFrameworkCore;
using NetTools;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;


namespace TOCO.Implements
{
    public class ClientIPService : BaseService
    {

        private readonly string[] privateIpAddress = {
            "10.0.0.0/8",
            "172.16.0.1/12",
            "192.168.0.0/16",
            //"127.0.0.0/8"
        };


        public ClientIPService(ApplicationDbContext context) : base(context)
        {
        }

        public List<ClientIP> GetAll(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;

            IQueryable<ClientIP> query = QueryableHelper.Default(context.ClientIPs);

            STHelper.Process<ClientIP, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);

            return query.ToList();
        }

        public List<ClientIP> GetAll(STRequestModel request = null)
        {
            IQueryable<ClientIP> query = QueryableHelper.Default(context.ClientIPs);
            return query.ToList();
        }

        public ClientIP GetById(int id)
        {
            return context.ClientIPs.FirstOrDefault(u => u.Id == id);
        }

        public ClientIP GetByIPAddress(string ipAddress)
        {
            return context.ClientIPs.FirstOrDefault(u => u.IPAddress == ipAddress);
        }

        public ClientIP Create(ClientIP ipClient)
        {
            ipClient.CreatedAt = DateTime.Now;
            ipClient.UpdatedAt = DateTime.Now;
            context.ClientIPs.Add(ipClient);
            context.SaveChanges();

            return ipClient;
        }

        public bool CheckClientIP(string ip)
        {
            //bool bOk = ip == "::1";
            bool bOk = false;

            if (!bOk)
            {
                foreach (string privateIpAddress in privateIpAddress)
                {
                    IPAddressRange addressRanger = IPAddressRange.Parse(privateIpAddress);
                    if (bOk = addressRanger.Contains(IPAddressRange.Parse(ip)))
                    {
                        break;
                    }
                }
            }
            if (!bOk)
            {
               List<ClientIP> listIpAddress = context.ClientIPs.ToList();
                foreach(ClientIP ipAddress in listIpAddress)
                {
                    IPAddressRange addressRanger = IPAddressRange.Parse(ipAddress.IPAddress);
                    if (bOk = addressRanger.Contains(IPAddressRange.Parse(ip)))
                    {
                        break;
                    }
                }
            }
            
            return bOk;
        }




        // todo: fix update user routes
        public ClientIP Update(int id, ClientIP script)
        {
            ClientIP model = GetById(id);

            if (model == null)
            {
                return null;
            }

            model.IPAddress = script.IPAddress;
           
            model.UpdatedAt = DateTime.Now;

            context.ClientIPs.Update(model);
            context.SaveChanges();
            return model;
        }

        public bool Delete(int id)
        {
            var ipClient = GetById(id);

            if (ipClient == null)
            {
                return false;
            }

            context.ClientIPs.Remove(ipClient);
            context.SaveChanges();

            return true;
        }
    }
}
