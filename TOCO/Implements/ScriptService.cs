﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class ScriptService : BaseService
    {
        public ScriptService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Script> GetAll()
        {
            IQueryable<Script> query = QueryableHelper.Default(context.Scripts).OrderBy(s =>s.SubScript);
            return query.ToList();
        }

        public List<Script> GetAll(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;
            IQueryable<Script> query = QueryableHelper.Default(context.Scripts);
            STHelper.Process<Script, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public Script GetById(int id)
        {
            return context.Scripts.FirstOrDefault(s => s.Id == id);
        }

        public Script GetByName(string name)
        {
            return context.Scripts.FirstOrDefault(s => s.SubScript == name);
        }

        public Script Create(Script script)
        {
            script.CreatedAt = DateTime.Now;
            script.UpdatedAt = DateTime.Now;
            context.Scripts.Add(script);
            context.SaveChanges();
            return script;
        }

        public Script Update(Script script)
        {
            script.UpdatedAt = DateTime.Now;
            context.Scripts.Update(script);
            context.SaveChanges();
            return script;
        }

        public bool Delete(string name)
        {
            var script = GetByName(name);

            if (script == null)
            {
                return false;
            }

            context.Scripts.Remove(script);
            context.SaveChanges();

            return true;
        }
    }
}
