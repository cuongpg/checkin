﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class MechanismQuotasService : BaseService
    {
        public MechanismQuotasService(ApplicationDbContext context) : base(context)
        {
        }

        public List<MechanismQuotas> GetAll()
        {
            return context.MechanismQuotas.ToList();
        }

        public double GetQuotas(string mechanismName, string levelName)
        {
            double quotas = 0;
            if (!string.IsNullOrEmpty(levelName) && !string.IsNullOrEmpty(mechanismName))
            {
                quotas = context.MechanismQuotas
                       .Join(context.Levels, mq => mq.LevelId, l => l.Id, (mq, l) => new
                       {
                           mq.Quotas,
                           l.Name,
                           mq.MechanismId
                       })
                       .Join(context.Mechanisms, mq_l => mq_l.MechanismId, m => m.Id, (mq_l, m) => new
                       {
                           mq_l.Quotas,
                           mq_l.Name,
                           m.MechanismName
                       })
                       .Where(mq_l_m => mq_l_m.MechanismName.Trim().ToLower() == mechanismName.Trim().ToLower() && mq_l_m.Name.Trim().ToLower() == levelName.Trim().ToLower())
                       .Select(mq_l_m => mq_l_m.Quotas)
                       .FirstOrDefault();
            }

            return quotas;
        }

    }
}
