﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Helpers;
using TOCO.Models.Pagination;
using TOCO.Common;

namespace TOCO.Implements
{
    public class CorporationService : BaseService
    {
        public CorporationService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Corporation> SearchCorporationService(string keyWord)
        {

            int limit = int.Parse(AppSetting.Configuration.GetValue("Meta:Limit"));
            IQueryable < Corporation > corporations = context.Corporations.Where(c => c.Active == CorporationStatus.Actived).OrderBy(c => c.ShortCode);
            if (!string.IsNullOrEmpty(keyWord))
                corporations = corporations.Where(c => c.ShortCode.Contains(keyWord.Trim()));
            return corporations.Take(limit).ToList();
        }

        public Corporation GetById(int id)
        {
            return context.Corporations.FirstOrDefault(e => e.Id == id);
        }

        public List<Corporation> GetCorporation(out int numberOfPages, out int totalRecords, int page, int perPage)
        {
            numberOfPages = 1;
            totalRecords = 0;
            IQueryable<Corporation> source = context.Corporations.Where(c => c.Active == CorporationStatus.Actived);
            int skip = (page - 1) * perPage;
            skip = skip < 0 ? 0 : skip;
            totalRecords = source.Count();
            numberOfPages = totalRecords / perPage;
            if (totalRecords % perPage != 0)
            {
                numberOfPages++;
            }
            source = source.Skip(skip).Take(perPage);
            return source.ToList();
        }

        public Corporation GetCorporationByCompleteCode(string corporation)
        {
            return context.Corporations.Where(c => c.CompleteCode == corporation).FirstOrDefault();
        }

        public Corporation GetByCompleteCode(string code)
        {
            return context.Corporations.FirstOrDefault(e => e.CompleteCode == code);
        }

        public Corporation GetByShortCode(string code)
        {
            return context.Corporations.FirstOrDefault(e => e.ShortCode == code);
        }

        public Corporation Create(Corporation corporation)
        {
            corporation.CreatedAt = DateTime.Now;
            corporation.UpdatedAt = DateTime.Now;
            context.Corporations.Add(corporation);
            context.SaveChanges();
            return corporation;
        }

        public Corporation Update(Corporation corporation)
        {
            corporation.UpdatedAt = DateTime.Now;
            context.Corporations.Update(corporation);
            context.SaveChanges();
            return corporation;
        }

        public bool ValidateCorporationId(int corporationId)
        {
            return GetById(corporationId) != null;
        }
    }
}
