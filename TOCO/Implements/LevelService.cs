﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class LevelService : BaseService
    {
        public LevelService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Level> GetAll()
        {
            return context.Levels.ToList();
        }

       
        public Level GetByName(string name)
        {
            return context.Levels.Where(l => l.Name == name).FirstOrDefault();
        }

        public Level GetByCode(string code)
        {
            return context.Levels.Where(l => l.Code == code).FirstOrDefault();
        }

        public Level GetById(int id)
        {
            return context.Levels.Where(l => l.Id == id).FirstOrDefault();
        }
    }
}
