﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Helpers;
using TOCO.Models.Pagination;
using TOCO.Common;

namespace TOCO.Implements
{
    public class CommonItemService : BaseService
    {
        public CommonItemService(ApplicationDbContext context) : base(context)
        {
        }


        public CommonItem GetById(int id)
        {
            return context.Commons.FirstOrDefault(e => e.Id == id);
        }

        public CommonItem GetByName(string name)
        {
            return context.Commons.FirstOrDefault(e => e.Name == name);
        }

        public CommonItem Create(CommonItem commonItem)
        {
            commonItem.CreatedAt = DateTime.Now;
            commonItem.UpdatedAt = DateTime.Now;
            context.Commons.Add(commonItem);
            context.SaveChanges();
            return commonItem;
        }

        public CommonItem Update(CommonItem commonItem)
        {
            commonItem.UpdatedAt = DateTime.Now;
            context.Commons.Update(commonItem);
            context.SaveChanges();
            return commonItem;
        }

        public List<CommonItem> GetChilds(int parentId)
        {
            return context.Commons.Where(c => c.ParentId == parentId && c.Active == StatusCommonItem.Active).ToList();
        }
    }
}
