﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Implements
{
    public class OvertimeQuotasService : BaseService
    {
        public OvertimeQuotasService(ApplicationDbContext context) : base(context)
        {
        }

        public OvertimeQuotas Update(OvertimeQuotas oq)
        {
            oq.UpdatedAt = DateTime.Now;
            context.OvertimeQuotas.Update(oq);
            context.SaveChanges();
            return oq;
        }

        public OvertimeQuotas Create(OvertimeQuotas oq)
        {
            oq.CreatedAt = DateTime.Now;
            oq.UpdatedAt = DateTime.Now;
            context.OvertimeQuotas.Add(oq);
            context.SaveChanges();
            return oq;
        }

        public OvertimeQuotas GetByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        {
            return context.OvertimeQuotas
                .Where(oq => oq.UserId == userId)
                .Where(oq=> oq.StartTime == startTime)
                .Where(oq => oq.EndTime == endTime)
                .FirstOrDefault();
        }

        public OvertimeQuotas GetByUserAndGroup(int userId, int groupEventId, DateTime startDate, DateTime endDate)
        {
            return context.OvertimeQuotas.Where(oq => oq.UserId == userId 
                                                    && oq.EventGroupId == groupEventId 
                                                    && oq.StartTime.Date == startDate.Date  
                                                    && oq.EndTime.Date == endDate.Date).FirstOrDefault();
        }
    }
}
