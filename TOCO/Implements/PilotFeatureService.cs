﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TOCO.Implements
{
    public class PilotFeatureService : BaseService
    {
        public PilotFeatureService(ApplicationDbContext context) : base(context)
        {
        }
      
        public PilotFeature GetByCode(string code)
        {
            return context.PilotFeatures.FirstOrDefault(p => p.Code == code);
        }
    }
}
