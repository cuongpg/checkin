﻿using Microsoft.EntityFrameworkCore;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Implements
{
    public class OverTimeService : BaseService
    {

        public OverTimeService(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Lấy thông tin làm thêm giờ theo ngày
        /// </summary>
        /// <param name="email"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        public OverTime GetByWorkDate(string email, DateTime workDate)
        {
            return context.OverTimes.Include(o => o.OverTimeDetails).FirstOrDefault(s => s.WorkDate.Date == workDate.Date && s.User.Email == email);
        }

        /// <summary>
        /// Tạo đơn xin làm thêm giờ
        /// </summary>
        /// <param name="overTime"></param>
        /// <returns></returns>
        public OverTime Create(OverTime overTime)
        {
            overTime.CreatedAt = DateTime.Now;
            overTime.UpdatedAt = DateTime.Now;
            context.OverTimes.Add(overTime);
            context.SaveChanges();
            return overTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeKeeping"></param>
        /// <returns></returns>
        public OverTime Update(OverTime overTime)
        {
            overTime.UpdatedAt = DateTime.Now;
            context.OverTimes.Update(overTime);
            context.SaveChanges();
            return overTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        public bool Delete(string email, DateTime workDate)
        {
            var model = GetByWorkDate(email, workDate);

            if (model == null)
            {
                return false;
            }

            context.OverTimes.Remove(model);
            context.SaveChanges();

            return true;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        public OverTime Create(User user, Script script, DateTime workDate)
        {
            //User user = context.Users.Where(u => u.Email == email).FirstOrDefault();
            //Script script = context.Scripts.Where(s => s.Id == user.ScriptId).FirstOrDefault();
            OverTime overTime = GetByWorkDate(user.Email, workDate);
            if (user == null)
            {
                return null;
            }

            if (overTime == null)
            {
                overTime = new OverTime()
                {
                    WorkDate = workDate,
                    UserId = user.Id,
                    ScriptId = script.Id,
                };
                return Create(overTime);
            }
            else
            {
                return overTime;
            }
        }

    }
}
