﻿using TOCO.Models;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class RouteService : BaseService
    {
        public RouteService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Route> GetAll(string query)
        {
            IQueryable<Route> routes = context.Routes;
            if (!string.IsNullOrEmpty(query))
            {
                routes = routes.Where(r => r.Name.Contains(query));
            }

            return routes.ToList();
        }
    }
}
