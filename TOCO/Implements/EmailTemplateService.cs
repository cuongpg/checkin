﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;

namespace TOCO.Implements
{
    public class EmailTemplateService : BaseService
    {
        public EmailTemplateService(ApplicationDbContext context) : base(context)
        {

        }


        public IEnumerable<EmailTemplate> GetAllEmailTemplate(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;

            IQueryable<EmailTemplate> query = QueryableHelper.Default(context.EmailTemplates);
            STHelper.Process<EmailTemplate, EmailPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public EmailTemplate GetTemplateByCode(string templateCode)
        {
            return context.EmailTemplates.Where(et => et.Code == templateCode).FirstOrDefault();
        }

        public void Delete(int id)
        {
            var template = context.EmailTemplates.Find(id);
            context.EmailTemplates.Remove(template);
            context.SaveChanges();
        }

        public EmailTemplate GetTemplateById(int id)
        {
            return context.EmailTemplates.Find(id);
        }

        public EmailTemplate Update(EmailTemplate emailTemplates)
        {
            emailTemplates.UpdatedAt = DateTime.Now;
            context.SaveChanges();
            return emailTemplates;
        }

        public  EmailTemplate Create(EmailTemplate emailTemplates)
        {
            var data = new EmailTemplate
            {
                Name = emailTemplates.Name,
                Subject = emailTemplates.Subject,
                Code = emailTemplates.Code,
                Body = emailTemplates.Body,
                Parameters = emailTemplates.Parameters,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            context.EmailTemplates.Add(data);
            context.SaveChanges();
            return data;
        }
    }
}
