﻿using MongoDB.Driver;
using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class ConfirmAllocationV2Service : BaseService
    {
        public ConfirmAllocationV2Service(ApplicationDbContext context) : base(context)
        {
        }

        public ConfirmAllocationV2 Create(ConfirmAllocationV2 confirmAllocation)
        {

            confirmAllocation.CreatedAt = DateTime.Now;
            confirmAllocation.UpdatedAt = DateTime.Now;
            context.ConfirmAllocationV2s.Add(confirmAllocation);
            context.SaveChanges();
            return confirmAllocation;
        }

        public int Update(ConfirmAllocationV2 confirmAllocation)
        {
            confirmAllocation.UpdatedAt = DateTime.Now;
            context.ConfirmAllocationV2s.Update(confirmAllocation);
            return context.SaveChanges();
        }

        public ConfirmAllocationV2 GetLateReportByUserId(int userId)
        {
            return context.ConfirmAllocationV2s
                .Where(c => c.Status == ConfirmAllocationStatusV2.ApprovalReport)
                .Where(c => c.UserId == userId)
                .OrderByDescending(c => c.UpdatedAt)
                .FirstOrDefault();
        }

        public ConfirmAllocationV2 GetLateReportById(int recordId)
        {
            return context.ConfirmAllocationV2s
                .Where(c => c.Id == recordId)
                .FirstOrDefault();
        }

        public List<ConfirmAllocationV2> GetReportByUserAndStatus(int userId, ConfirmAllocationStatusV2 status)
        {
            return context.ConfirmAllocationV2s
                .Where(c => c.UserId == userId)
                .Where(c => c.Status == status)
                .ToList();
        }

        public ConfirmAllocationV2 GetWaitReportByUserId(int userId, DateTime startMonth, DateTime endMonth)
        {
            return context.ConfirmAllocationV2s
               .Where(c => c.Status == ConfirmAllocationStatusV2.Waiting)
               .Where(c => c.UserId == userId)
               .Where(c => c.StartDate.Date == startMonth.Date && c.EndDate.Date == endMonth.Date)
               .OrderByDescending(c => c.UpdatedAt)
               .FirstOrDefault();
        }

        public List<ConfirmAllocationV2> GetErrorReport()
        {
            return context.ConfirmAllocationV2s
              .Where(c => c.Status == ConfirmAllocationStatusV2.Reporting)
              .Where(c => c.HrOperResult != "OK")
              .OrderByDescending(c => c.UpdatedAt)
              .ToList();
        }

        //public List<ConfirmAllocationV2> GetReportByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        //{
        //    return context.ConfirmAllocationV2s
        //      .Where(c => c.UserId == userId)
        //      .Where(c => c.Status != ConfirmAllocationStatusV2.Waiting)
        //      .Where(c => c.CreatedAt.Date >= startTime && c.CreatedAt.Date <= endTime)
        //      .OrderByDescending(c => c.UpdatedAt)
        //      .ToList();
        //}
    }
}
