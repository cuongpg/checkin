﻿using Microsoft.EntityFrameworkCore;
using NetTools;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;


namespace TOCO.Implements
{
    public class LocationService : BaseService
    {

        public LocationService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Location> GetAll(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;

            IQueryable<Location> query = QueryableHelper.Default(context.Locations);

            STHelper.Process<Location, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);

            return query.ToList();
        }

        public List<Location> GetAll(STRequestModel request = null)
        {
            IQueryable<Location> query = QueryableHelper.Default(context.Locations);
            return query.ToList();
        }

        public Location GetById(int id)
        {
            return context.Locations.FirstOrDefault(u => u.Id == id);
        }


        public Location Create(Location location)
        {
            location.CreatedAt = DateTime.Now;
            location.UpdatedAt = DateTime.Now;
            context.Locations.Add(location);
            context.SaveChanges();

            return location;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <returns></returns>
        public bool CheckLocation(double longitude, double latitude)
        {
            bool bOk = false;
            float radius = float.Parse(AppSetting.Configuration.GetValue("TopicaRadius"));
            List<Location> locations = context.Locations.ToList();
            foreach (Location location in locations)
            {
                double distance = CalcDistance(longitude, latitude, location);
                bOk = distance <= radius;
                if (bOk)
                {
                    break;
                }
            }
            return bOk;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        private double CalcDistance(double longitude, double latitude, Location location)
        {
            var ky = 40000 / 360;
            var kx = Math.Cos(Math.PI * (double)location.Latitude / 180.0) * ky;
            var dx = Math.Abs((double)location.Longitude - longitude) * kx;
            var dy = Math.Abs((double)location.Latitude - latitude) * ky;
            double distance = Math.Sqrt(dx * dx + dy * dy) * 1000;
            return distance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <returns></returns>
        public (Location, double) GetTopicaLocation(float longitude, float latitude)
        {
            List<Location> locations = context.Locations.ToList();
            double distanceMin = 0;
            Location result = null;
            foreach (Location location in locations)
            {
                double distance = CalcDistance(longitude, latitude, location);
                if (distanceMin == 0)
                {
                    result = location;
                    distanceMin = distance;
                }
                else
                {
                    if(distance < distanceMin)
                    {
                        result = location;
                        distanceMin = distance;
                    }
                }
          
            }
            return (result, distanceMin);
        }
    }
}
