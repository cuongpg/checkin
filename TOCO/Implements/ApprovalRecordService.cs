﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Models.ResultModels.WisamiResultModels;
using System.Globalization;

namespace TOCO.Implements
{
    public class ApprovalRecordService : BaseService
    {
        public ApprovalRecordService(ApplicationDbContext context) : base(context)
        {
        }

        public ApprovalRecord Create(ApprovalRecord approvalRecord)
        {
            approvalRecord.CreatedAt = DateTime.Now;
            approvalRecord.UpdatedAt = DateTime.Now;
            context.ApprovalRecords.Add(approvalRecord);
            context.SaveChanges();
            return approvalRecord;
        }

        public ApprovalRecord Update(ApprovalRecord detail)
        {
            if (detail == null)
            {
                return null;
            }
            detail.UpdatedAt = DateTime.Now;
            context.ApprovalRecords.Update(detail);
            context.SaveChanges();
            return detail;
        }

        // Lấy danh sách Pending của tháng trước
        // Để chuyển trạng thái Expired cho đơn nghỉ phép, hủy nghỉ phép
        public IEnumerable<ApprovalRecord> GetPendingApprovalLastMonth(DateTime startDate, DateTime endDate, string corporationType)
        {
            return context.ApprovalRecords
                .Where(ar => ar.Status == StatusApprovalRecord.Pending || ar.Status == StatusApprovalRecord.AdminPending || ar.StatusDestroy == StatusApprovalRecord.Pending)
                .Where(ar => ar.StartTime <= endDate && ar.EndTime >= startDate)
                .Join(context.ScriptHistorys, ar => ar.UserId, sh => sh.UserId, (ar, sh) => new
                {
                    Detail = ar,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(ar_sh => ar_sh.EffectiveDate <= startDate && ar_sh.ExpirationDate >= startDate)
                .Join(context.Scripts, ar_sh => ar_sh.ScriptId, s => s.Id, (ar_sh, s) => new
                {
                    ar_sh.Detail,
                    s.CorporationType
                })
                .Where(a_u_sh_s => a_u_sh_s.CorporationType == corporationType)
                .Select(a_u_sh_s => a_u_sh_s.Detail)
                .Distinct()
                .ToList();
        }

        #region [Hiển thị đơn nghỉ phép và hủy nghỉ phép]

        // Lấy danh sách đơn nghỉ phép, hủy nghỉ phép 
        // Để hiển thị lên đơn của tôi và đơn cần duyệt
        public List<ApprovalRecord> GetApprovalRecords(out int numberOfPages, out int totalRecords, STRequestModel request, User user, bool isForManager, bool isDestroy)
        {
            numberOfPages = 1;
            totalRecords = 0;

            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();

            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();

            IQueryable<ApprovalRecord> source = context.ApprovalRecords.Include(ar => ar.User).Include(ar => ar.ReasonType);

            // Lấy đơn xin nghỉ đợi xác nhận hoặc đã xác nhận hủy
            if (isDestroy)
            {
                source = source.Where(ar => ar.StatusDestroy == StatusApprovalRecord.Pending
                || ar.StatusDestroy == StatusApprovalRecord.Approved);
            }
            // Lấy đơn xin nghỉ còn lại
            else
            {
                source = source.Where(ar => ar.StatusDestroy == StatusApprovalRecord.NotYet
                || ar.StatusDestroy == StatusApprovalRecord.Rejected
                || ar.StatusDestroy == StatusApprovalRecord.Expired);
            }
            source = isForManager ? source.Where(ar => ar.User.ManagerId == user.Id) : source.Where(ar => ar.UserId == user.Id);

            if (statusFilters.Count > 0)
            {
                source = isDestroy ? source.Where(ar => (int)ar.StatusDestroy == statusFilters[0].Status) : source.Where(ar => (int)ar.Status == statusFilters[0].Status);
            }

            foreach (DateSearch date in listDateSearch)
            {
                SearchDateObject searchDate = date.GetData<SearchDateObject>();
                if (!string.IsNullOrEmpty(searchDate.StartAt))
                {
                    source = source.Where(ar => ar.StartTime.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture)
                    || ar.EndTime.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
                if (!string.IsNullOrEmpty(searchDate.EndAt))
                {
                    source = source.Where(ar => ar.StartTime.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture)
                    || ar.EndTime.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
            }

            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo trạng thái Pending => Approval => Rejected
            //source = isDestroy ? source.OrderBy(ar => ar.StatusDestroy) : source.OrderBy(ar => ar.Status);
            // Sort theo thời gian tạo
            source = source.OrderByDescending(ar => ar.UpdatedAt.HasValue ? ar.UpdatedAt : ar.CreatedAt);
            if (sorts != null)
            {
                foreach ((string key, bool value) in sorts)
                {
                    switch (key)
                    {
                        case "status":
                            source = value ? (isDestroy ? source.OrderBy(ar => ar.StatusDestroy) : source.OrderBy(ar => ar.Status)) : (isDestroy ? source.OrderByDescending(ar => ar.StatusDestroy) : source.OrderByDescending(ar => ar.Status));
                            break;
                        case "name":
                            source = value ? source.OrderBy(ar => ar.User.FullName) : source.OrderByDescending(ar => ar.User.FullName);
                            break;
                        case "email":
                            source = value ? source.OrderBy(ar => ar.User.Email) : source.OrderByDescending(ar => ar.User.Email);
                            break;
                        case "startDate":
                            source = value ? source.OrderBy(ar => ar.StartTime) : source.OrderByDescending(ar => ar.StartTime);
                            break;
                        case "endDate":
                            source = value ? source.OrderBy(ar => ar.EndTime) : source.OrderByDescending(ar => ar.EndTime);
                            break;
                        default:
                            break;
                    }
                }
            }
            source = source.Skip(pagination.Start).Take(pagination.Number);
            return source.ToList();
        }

        // Lấy danh sách đơn nghỉ phép cần Admin duyệt trước
        // Để hiển thị lên danh sách đơn cần Admin duyệt
        public List<ApprovalRecord> GetApprovalRecordsNeedConfirmByAdmin(out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            numberOfPages = 1;
            totalRecords = 0;

            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();

            IQueryable<ApprovalRecord> source = context.ApprovalRecords
                .Include(ar => ar.User)
                .Include(ar => ar.ReasonType)
                .Where(ar => ar.Status == StatusApprovalRecord.AdminPending)
                .Where(ar => ar.StatusDestroy != StatusApprovalRecord.Pending)
                .Where(ar => ar.StatusDestroy != StatusApprovalRecord.Approved);

            foreach (DateSearch date in listDateSearch)
            {
                SearchDateObject searchDate = date.GetData<SearchDateObject>();
                if (!string.IsNullOrEmpty(searchDate.StartAt))
                {
                    source = source.Where(ar => ar.StartTime.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture)
                    || ar.EndTime.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
                if (!string.IsNullOrEmpty(searchDate.EndAt))
                {
                    source = source.Where(ar => ar.StartTime.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture)
                    || ar.EndTime.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
            }

            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo ngày tháng
            source = source.OrderBy(ar => ar.CreatedAt);
            if (sorts != null)
            {
                foreach ((string key, bool value) in sorts)
                {
                    switch (key)
                    {
                        case "name":
                            source = value ? source.OrderBy(ar => ar.User.FullName) : source.OrderByDescending(ar => ar.User.FullName);
                            break;
                        case "email":
                            source = value ? source.OrderBy(ar => ar.User.Email) : source.OrderByDescending(ar => ar.User.Email);
                            break;
                        case "startDate":
                            source = value ? source.OrderBy(ar => ar.StartTime) : source.OrderByDescending(ar => ar.StartTime);
                            break;
                        case "endDate":
                            source = value ? source.OrderBy(ar => ar.EndTime) : source.OrderByDescending(ar => ar.EndTime);
                            break;
                        default:
                            break;
                    }
                }
            }
            source = source.Skip(pagination.Start).Take(pagination.Number);
            return source.ToList();
        }



        // Đếm số đơn nghỉ phép, hủy nghỉ phép pending
        // Hiện thông báo số lượng đơn cần duyệt
        public int CountPendingApprovalRecord(User user, bool isForManager, bool isAdmin, bool isDestroy)
        {
            IQueryable<ApprovalRecord> source = context.ApprovalRecords.Include(ar => ar.User).Include(ar => ar.ReasonType);
            if (isAdmin)
            {
                source = source
                       .Where(ar => ar.Status == StatusApprovalRecord.AdminPending)
                       .Where(ar => ar.StatusDestroy != StatusApprovalRecord.Pending)
                       .Where(ar => ar.StatusDestroy != StatusApprovalRecord.Approved);
            }
            else
            {
                source = isForManager ? source.Where(ar => ar.User.ManagerId == user.Id) : source.Where(ar => ar.UserId == user.Id);
                if (isDestroy)
                {
                    source = source.Where(ar => ar.StatusDestroy == StatusApprovalRecord.Pending);
                }
                else
                {
                    source = source
                        .Where(ar => isForManager ? ar.Status == StatusApprovalRecord.Pending : (ar.Status == StatusApprovalRecord.Pending || ar.Status == StatusApprovalRecord.AdminPending))
                        .Where(ar => ar.StatusDestroy != StatusApprovalRecord.Pending)
                        .Where(ar => ar.StatusDestroy != StatusApprovalRecord.Approved);
                }
            }
            return source.Count();
        }
        #endregion

        #region [Xử lý Conflict]
        // Lấy danh sách nghỉ phép bị conflict với đơn nghỉ phép nhiều ngày
        // Dùng để xử lý conflict
        public List<ApprovalRecord> GetByDate(string email, DateTime startTime, DateTime endTime)
        {
            return context.ApprovalRecords.Include(a => a.User)
                // Lọc ra danh sách đơn xin nghỉ phép chưa bị reject, quá hạn, đang đợi hủy hoặc đã được duyệt hủy
                .Where(a => a.User.Email == email)
                .Where(a => a.Status != StatusApprovalRecord.Rejected
                            && a.Status != StatusApprovalRecord.Expired
                            && a.StatusDestroy != StatusApprovalRecord.Pending
                            && a.StatusDestroy != StatusApprovalRecord.Approved)
                // Lọc ra danh sách các đơn hợp lệ có ngày nghỉ trùng
                .Where(a => !(startTime.Date > a.EndTime.Date || endTime.Date < a.StartTime.Date)).ToList();
        }

        // Lấy danh sách nghỉ phép bị conflict với đơn nghỉ phép 1 ngày
        // Dùng để xử lý conflict
        public List<ApprovalRecord> GetByDate(string email, DateTime date, bool isAfternoon, bool isMorning)
        {
            return context.ApprovalRecords.Include(a => a.User)
                // Lọc ra danh sách đơn xin nghỉ phép chưa bị reject, quá hạn, đang đợi hủy hoặc đã được duyệt hủy
                .Where(a => a.User.Email == email)
                .Where(a => a.Status != StatusApprovalRecord.Rejected
                        && a.Status != StatusApprovalRecord.Expired
                        && a.StatusDestroy != StatusApprovalRecord.Pending
                        && a.StatusDestroy != StatusApprovalRecord.Approved)
                // Lọc ra danh sách các đơn hợp lệ có ngày nghỉ trùng
                .Where(a => (a.IsManyDate && a.StartTime <= date && a.EndTime >= date)
                        || (!a.IsManyDate && a.StartTime == date && (a.IsAfternoon == isAfternoon || a.IsMorning == isMorning))).ToList();
        }

        // Lấy danh sách đơn hủy nghỉ phép đợi duyệt do conflict với đơn nghỉ phép vừa được duyệt
        // Dùng xử lý tự động approval các đơn hủy do conflict với đơn nghỉ phép vừa được duyệt
        public IEnumerable<ApprovalRecord> GetDestroyPendingByDate(string email, DateTime date, bool isAfternoon, bool isMorning)
        {
            return context.ApprovalRecords.Include(a => a.User)
                // Lọc ra danh sách đơn xin hủy nghỉ phép đang đợi duyệt
                .Where(a => a.User.Email == email)
                .Where(a => a.StatusDestroy == StatusApprovalRecord.Pending)
                // Lọc ra danh sách các đơn hợp lệ có ngày nghỉ trùng
                .Where(a => (a.IsManyDate && a.StartTime <= date && a.EndTime >= date)
                        || (!a.IsManyDate && a.StartTime == date && (a.IsAfternoon == isAfternoon || a.IsMorning == isMorning))).ToList();
        }

        // Lấy danh sách đơn hủy nghỉ phép đợi duyệt do conflict với đơn nghỉ phép vừa được duyệt
        // Dùng xử lý tự động approval các đơn hủy do conflict với đơn nghỉ phép vừa được duyệt
        public IEnumerable<ApprovalRecord> GetDestroyPendingByDate(string email, DateTime startTime, DateTime endTime)
        {
            return context.ApprovalRecords.Include(a => a.User)
                // Lọc ra danh sách đơn xin hủy nghỉ phép đang đợi duyệt
                .Where(a => a.User.Email == email)
                .Where(a => a.StatusDestroy == StatusApprovalRecord.Pending)
                // Lọc ra danh sách các đơn hợp lệ có ngày nghỉ trùng
                .Where(a => !(startTime.Date > a.EndTime.Date || endTime.Date < a.StartTime.Date)).ToList();
        }
        #endregion

        #region [Approval, Reject, Destroy đơn]
        // Confirm đơn xin nghỉ, hủy xin nghỉ
        public (bool bOk, string code, ApprovalRecord[] records) ConfirmApprovalReacord(StatusApprovalRecord status, int[] ids, User manager, bool isDestroy, string rejectReason)
        {
            string resultCode = string.Empty;
            bool bRes = false;
            IQueryable<ApprovalRecord> source = context.ApprovalRecords
                                                .Include(a => a.User)
                                                .Where(a => a.User.ManagerId == manager.Id && ids.Contains(a.Id));
            if (isDestroy)
            {
                source = source.Where(a => a.StatusDestroy == StatusApprovalRecord.Pending);
            }
            else
            {
                source = source.Where(a => a.Status == StatusApprovalRecord.Pending
                            && a.StatusDestroy != StatusApprovalRecord.Pending
                            && a.StatusDestroy != StatusApprovalRecord.Approved);
            }
            ApprovalRecord[] details = source.ToArray();
            if (details.Count() == 0)
            {
                resultCode = ResultCode.APPROVAL_NOT_FOUND;
                bRes = false;
            }
            else
            {
                foreach (ApprovalRecord detail in details)
                {
                    if (!isDestroy)
                    {
                        detail.ApprovalTime = DateTime.Now;
                        detail.ApproverEmail = manager.Email;
                        detail.Status = status;
                    }
                    else
                    {
                        detail.ApprovalDestroyTime = DateTime.Now;
                        detail.ApproverEmail = manager.Email;
                        detail.StatusDestroy = status;
                    }
                    if (!string.IsNullOrEmpty(rejectReason))
                    {
                        detail.RejectReason = rejectReason;
                    }
                    Update(detail);
                    context.SaveChanges();
                }
                bRes = true;
            }
            return (bRes, resultCode, details);
        }

        // Confirm đơn xin nghỉ từ admin
        public (bool bOk, string code, ApprovalRecord[] records) ConfirmRecordByAdmin(StatusApprovalRecord status, int[] ids, string adminEmail, string rejectReason)
        {
            string resultCode = string.Empty;
            bool bRes = false;
            IQueryable<ApprovalRecord> source = context.ApprovalRecords
                                                .Include(a => a.User)
                                                .Include(a => a.ReasonType)
                                                .Where(a => ids.Contains(a.Id))
                                                .Where(a => a.Status == StatusApprovalRecord.AdminPending)
                                                .Where(a => a.StatusDestroy != StatusApprovalRecord.Pending)
                                                .Where(a => a.StatusDestroy != StatusApprovalRecord.Approved);
            ApprovalRecord[] details = source.ToArray();
            if (details.Count() == 0)
            {
                resultCode = ResultCode.APPROVAL_NOT_FOUND;
                bRes = false;
            }
            else
            {
                foreach (ApprovalRecord detail in details)
                {
                    detail.AdminConfirmTime = DateTime.Now;
                    detail.AdminConfirmEmail = adminEmail;
                    detail.Status = status;

                    if (!string.IsNullOrEmpty(rejectReason))
                    {
                        detail.RejectReason = rejectReason;
                    }
                    Update(detail);
                    context.SaveChanges();
                }
                bRes = true;
            }
            return (bRes, resultCode, details);
        }

        // Hủy nghỉ phép
        public (bool bOk, string code, ApprovalRecord[] records) DestroyApprovalRecord(bool byManager, bool isAdmin, int[] ids, int userId)
        {
            string resultCode = string.Empty;
            bool bRes = false;
            IQueryable<ApprovalRecord> source = context.ApprovalRecords.Where(a => a.UserId == userId && ids.Contains(a.Id));
            ApprovalRecord[] details = source.ToArray();
            if (details.Count() == 0)
            {
                resultCode = ResultCode.APPROVAL_NOT_FOUND;
            }
            else
            {
                int destroyNumber = 0;
                // Kiểm tra đăng ký ot trong tương lai và cho tháng trước sau mồng 03 hàng tháng
                DateTime startCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                DateTime startLastMonth = startCurrentMonth.AddMonths(-1).Date;

                foreach (ApprovalRecord detail in details)
                {
                    ScriptHistory scriptHistory = context.ScriptHistorys
                        .Where(sh => sh.UserId == detail.UserId && sh.EffectiveDate <= detail.StartTime.Date && sh.ExpirationDate >= detail.StartTime.Date).FirstOrDefault();
                    Script script = context.Scripts.Where(s => s.Id == scriptHistory.ScriptId).FirstOrDefault();
                    // Nếu validate theo ngày cutoff
                    if (((detail.StartTime.Date < startCurrentMonth && DateTime.Now.Day >= script.CutoffTimeKeepingDay)
                        || (detail.StartTime.Date < startLastMonth && DateTime.Now.Day < script.CutoffTimeKeepingDay)) && !isAdmin)
                    {
                        continue;
                    }
                    if (detail.IsDestroy && !byManager)
                    {
                        continue;
                    }

                    detail.IsDestroy = true;
                    if (((detail.Status == StatusApprovalRecord.Pending || detail.Status == StatusApprovalRecord.AdminPending) && detail.StartTime > DateTime.Now) || byManager || isAdmin)
                    {
                        detail.StatusDestroy = StatusApprovalRecord.Approved;
                        detail.DestroyTime = DateTime.Now;
                        detail.ApprovalDestroyTime = DateTime.Now;
                    }
                    else
                    {
                        detail.StatusDestroy = StatusApprovalRecord.Pending;
                        detail.DestroyTime = DateTime.Now;
                    }
                    Update(detail);
                    context.SaveChanges();
                    destroyNumber++;
                }
                if (destroyNumber > 0)
                {
                    bRes = true;
                }
                else
                {
                    resultCode = ResultCode.CAN_NOT_DESTROY;
                }
            }
            return (bRes, resultCode, details);
        }
        #endregion

        #region [Tính công nghỉ phép cho bảng công]
        // Lấy tất cả đơn xin nghỉ phép còn hiệu lực theo User và Ngày nghỉ
        // Sử dụng để hiển thị bảng công
        public List<ApprovalRecord> GetApprovalRecordByDate(User user, DateTime date)
        {
            return context.ApprovalRecords
                .Include(ar => ar.ReasonType)
                .Where(a => a.UserId == user.Id && (a.StartTime.Date <= date && a.EndTime.Date >= date))
                // Lấy tất cả các đơn xin nghỉ phép có hiệu lực hoặc đợi duyệt (Không bị quá hạn, reject và chưa được approval hủy)
                .Where(a => a.Status != StatusApprovalRecord.Rejected
                            && a.StatusDestroy != StatusApprovalRecord.Pending
                            && a.StatusDestroy != StatusApprovalRecord.Approved)
                .ToList();
        }

        // Lấy tất cả đơn xin nghỉ phép còn hiệu lực theo User và Ngày nghỉ
        // Sử dụng để xuất bảng công
        public List<ApprovalRecord> GetApprovalApprovalRecordByDate(User user, DateTime date)
        {
            return context.ApprovalRecords
                .Include(ar => ar.ReasonType)
                .Where(a => a.UserId == user.Id && (a.StartTime.Date <= date && a.EndTime.Date >= date))
                // Lấy tất cả các đơn xin nghỉ phép có hiệu lực (Đã được duyệt và chưa được approval hủy)
                .Where(a => a.Status == StatusApprovalRecord.Approved && a.StatusDestroy != StatusApprovalRecord.Approved)
                .ToList();
        }
        #endregion

        #region [Tính số phép còn lại]
        // Lấy tất cả đơn xin nghỉ phép còn hiệu lực theo User và Loại nghỉ phép
        // Sử dụng để tính số ngày phép còn lại
        public List<ApprovalRecord> GetApprovalRecordByUserAndReasonType(int userId, int reasonTypeId)
        {
            return context.ApprovalRecords
                .Where(a => a.UserId == userId)
                .Where(a => a.ReasonTypeId == reasonTypeId)
                // Lấy tất cả các đơn xin nghỉ phép có hiệu lực hoặc đợi duyệt (Không bị quá hạn, reject và chưa được approval hủy)
                .Where(a => a.Status != StatusApprovalRecord.Rejected
                            && a.Status != StatusApprovalRecord.Expired
                            && a.StatusDestroy != StatusApprovalRecord.Approved)
                .ToList();
        }
        #endregion

        // Lấy tất cả danh sách nghỉ phép có hiệu lực
        // Để export phép cho HCM
        public IEnumerable<(ApprovalRecord Record, int ReasonId, string HCMCode)> GetApprovalRecordForHcm(string corporationType, DateTime startDate, DateTime endDate)
        {
            IEnumerable<dynamic> result = context.ApprovalRecords
                .Include(ar => ar.ReasonType)
                // Lấy tất cả các đơn xin nghỉ phép có hiệu lực (Đã được duyệt và chưa được approval hủy)
                .Where(ar => ar.Status == StatusApprovalRecord.Approved
                            && ar.StatusDestroy != StatusApprovalRecord.Approved)
                .Where(ar => !(ar.StartTime > endDate || ar.EndTime < startDate))
                .Join(context.ReasonTypes, ar => ar.ReasonTypeId, r => r.Id, (ar, r) => new
                {
                    Detail = ar,
                    r.HCMCode,
                    ReasonId = r.Id,
                    r.CorporationType,
                })
                .Where(ar_r => ar_r.CorporationType == corporationType);

            foreach (dynamic item in result)
            {
                yield return (item.Detail, item.ReasonId, item.HCMCode);
            }
        }
    }
}
