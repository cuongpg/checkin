﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Implements
{
    public class AbsenderQuotasService : BaseService
    {
        public AbsenderQuotasService(ApplicationDbContext context) : base(context)
        {
        }


        public List<(double Number, DateTime ExpirationDate, int)> GetByExpirationDate(int userId, int reasonId)
        {
            IQueryable<AbsenderQuotas> listAbsenderQuotas = context.AbsenderQuotas.Where(aq => aq.UserId == userId && aq.ReasonTypeId == reasonId);

            List<(double Number, DateTime ExpirationDate, int GenerationYear)> absenderGroups = new List<(double Number, DateTime ExpirationDate, int GenerationYear)>();

            foreach (IGrouping<dynamic, AbsenderQuotas> group in listAbsenderQuotas.GroupBy(aq => new { aq.ExpirationDate, aq.EffectiveDate.Year }))
            {
                double number = 0;
                foreach (AbsenderQuotas detail in group)
                {
                    number += detail.Number;
                }
                absenderGroups.Add((number, group.Key.ExpirationDate, group.Key.Year));
            }
            return absenderGroups;
        }

        public IEnumerable<(AbsenderQuotas Quotas, string HCMQuotasCode)> GetAbsenderQuotas(string corporationType, DateTime startDate, DateTime endDate)
        {
            IEnumerable<dynamic> result = context.AbsenderQuotas.Join(context.ReasonTypes, ab => ab.ReasonTypeId, rt => rt.Id, (ab, rt) => new
            {
                Quotas = ab,
                rt.HCMQuotasCode,
                rt.CorporationType
            })
            .Where(ab_rt => ab_rt.CorporationType == corporationType)
            .Where(ab_rt => (ab_rt.Quotas.CreatedAt >= startDate && ab_rt.Quotas.CreatedAt <= endDate));
            foreach (dynamic item in result)
            {
                yield return (item.Quotas, item.HCMQuotasCode);
            }
        }

        //public IEnumerable<(AbsenderQuotas Quotas, string HCMQuotasCode)> GetAbsenderQuotas(DateTime time)
        //{
        //    IEnumerable<dynamic> result = context.AbsenderQuotas.Join(context.ReasonTypes, ab => ab.ReasonTypeId, rt => rt.Id, (ab, rt) => new
        //    {
        //        Quotas = ab,
        //        rt.HCMQuotasCode
        //    })
        //    .Where(ab_rt => (ab_rt.Quotas.CreatedAt >= time.AddDays(-1) && ab_rt.Quotas.CreatedAt < time));
        //    foreach (dynamic item in result)
        //    {
        //        yield return (item.Quotas, item.HCMQuotasCode);
        //    }
        //}

        public AbsenderQuotas Create(AbsenderQuotas ab)
        {
            ab.CreatedAt = DateTime.Now;
            ab.UpdatedAt = DateTime.Now;
            context.AbsenderQuotas.Add(ab);
            context.SaveChanges();
            return ab;
        }
    }
}
