﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TOCO.Implements
{
    public class EventService: BaseService
    {
        public EventService(ApplicationDbContext context) : base(context)
        {
        }

        public Event GetById(int id)
        {
            return context.Events.FirstOrDefault(e => e.Id == id);
        }

        public Event GetByCode(string code)
        {
            return context.Events.FirstOrDefault(e => e.EventCode == code);
        }

        public List<Event> GetEventsByEmailAndGroup(int? groupId, int employeeTypeId, int page, int perPage, string keyWord, out int totalPages)
        {
            IQueryable<Event> source = context.Events.Include(e => e.GroupEvent);
            source = groupId.HasValue ? source.Where(e => e.GroupEventId == groupId) : source;
            source = source.Where(e => e.EmployeeTypeId == employeeTypeId);
            source = source.Where(e => e.EventCode.Contains(keyWord));
            
            int skip = page * perPage;
            int totalRecords = source.Count();
            totalPages = totalRecords / perPage;
            if (totalRecords % perPage != 0)
            {
                totalPages++;
            }
            source = source.OrderBy(e => e.EventCode);
            source = source.Skip(skip).Take(perPage);
            return source.ToList();
        }

        public Event GetByEmployeeTypeAndEventId(string corporationType, int employeeTypeId, int eventId)
        {
            return context.Events.Include(ev => ev.GroupEvent)
                .Where(ev => ev.EmployeeTypeId == employeeTypeId && ev.Id == eventId && ev.GroupEvent.CorporationType == corporationType)
                .FirstOrDefault();
        }
    }
}
