﻿using TOCO.Common;
using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TOCO.Models.Pagination;
using System.Globalization;

namespace TOCO.Implements
{
    public class OverTimeDetailService : BaseService
    {
        public OverTimeDetailService(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Lay thong tin lam them theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OverTimeDetail GetById(int id)
        {
            return context.OverTimeDetails.FirstOrDefault(t => t.Id == id);
        }

        /// <summary>
        /// Tao don xin lam them gio
        /// </summary>
        /// <param name="overTimeDetail"></param>
        /// <returns></returns>
        public OverTimeDetail Create(OverTimeDetail overTimeDetail)
        {
            overTimeDetail.CreatedAt = DateTime.Now;
            overTimeDetail.UpdatedAt = DateTime.Now;
            context.OverTimeDetails.Add(overTimeDetail);
            context.SaveChanges();

            return overTimeDetail;
        }

        /// <summary>
        /// Cap nhat don xin lam them gio
        /// </summary>
        /// <param name="overTimeDetail"></param>
        /// <returns></returns>
        public OverTimeDetail Update(OverTimeDetail overTimeDetail)
        {
            overTimeDetail.UpdatedAt = DateTime.Now;
            context.OverTimeDetails.Update(overTimeDetail);
            context.SaveChanges();
            return overTimeDetail;
        }

        /// <summary>
        /// Xoa don xin lam them
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            var model = GetById(id);

            if (model == null)
            {
                return false;
            }

            context.OverTimeDetails.Remove(model);
            context.SaveChanges();

            return true;
        }

        public IEnumerable<(int? Percentage,
            double? PayPerHour,
            double? PayPerBlock,
            string EventCode,
            DateTime WorkDate,
            DateTime StartTime,
            DateTime EndTime,
            bool IsManual)> GetOverTimeDetailsInMonthByStatus(int groupEventId, StatusOverTime status, int userId, int scriptId, DateTime startDate, DateTime endDate)
        {
            IQueryable<dynamic> result = context.OverTimeDetails
                .Where(otd => (otd.IsDestroy == false || otd.StatusDestroy == StatusOverTime.Rejected) && (otd.Status == status))
                .Join(context.OverTimes, otd => otd.OverTimeId, ot => ot.Id, (otd, ot) => new
                {
                    otd.StartTime,
                    otd.EndTime,
                    otd.EventId,
                    otd.IsManual,
                    ot.UserId,
                    ot.WorkDate,
                    ot.ScriptId
                })
                .Where(otd_ot => otd_ot.ScriptId == scriptId)
                .Where(otd_ot => otd_ot.UserId == userId)
                .Where(otd_ot => otd_ot.WorkDate >= startDate && otd_ot.WorkDate <= endDate)
                .Join(context.Events, otd_ot => otd_ot.EventId, e => e.Id, (otd_ot, e) => new
                {
                    otd_ot.StartTime,
                    otd_ot.EndTime,
                    otd_ot.EventId,
                    otd_ot.IsManual,
                    otd_ot.UserId,
                    otd_ot.WorkDate,
                    otd_ot.ScriptId,
                    e.PayPerBlock,
                    e.PayPerHour,
                    e.Percentage,
                    e.EventCode,
                    e.GroupEventId
                })
                .Where(otd_ot_e => otd_ot_e.GroupEventId == groupEventId);
            foreach (dynamic item in result)
            {
                yield return (item.Percentage, item.PayPerHour, item.PayPerBlock, item.EventCode, item.WorkDate, item.StartTime, item.EndTime, item.IsManual);
            }
        }


        public List<OverTimeDetail> GetValidOverTimeDetailInDay(int overtimeId, int? id)
        {
            IEnumerable<OverTimeDetail> query = context.OverTimeDetails.Include(otd => otd.Event)
                .Where(t => t.OverTimeId == overtimeId && t.Status != StatusOverTime.Rejected && !(t.IsDestroy == true && t.StatusDestroy != StatusOverTime.Rejected));

            if (id.HasValue)
            {
                query = query.Where(t => t.Id != id);
            }
            return query.ToList();
        }

        public List<OverTimeDetail> GetOvertime(int[] groupEventIds, out int numberOfPages, out int totalRecords, STRequestModel request, User user, bool isForManager, bool isDestroy)
        {
            numberOfPages = 1;
            totalRecords = 0;
            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();
            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();
            IQueryable<OverTimeDetail> source = context.OverTimeDetails.Include(otd => otd.Event).ThenInclude(e => e.GroupEvent).Include(otd => otd.OverTime).ThenInclude(ot => ot.User);
            if (groupEventIds.Count() > 0)
            {
                source = source.Where(otd => groupEventIds.Contains(otd.Event.GroupEventId));
            }
            if (groupEventIds.Count() > 0)
            {
                source = source.Where(otd => groupEventIds.Contains(otd.Event.GroupEventId));
            }

            // Nếu lấy đơn hủy
            if (isDestroy)
            {
                source = source.Where(ot => ot.IsDestroy && ot.StatusDestroy != StatusOverTime.Rejected);
            }
            else // Nếu lấy đơn không hủy
            {
                source = source.Where(ot => !ot.IsDestroy || ot.StatusDestroy == StatusOverTime.Rejected);
            }

            source = isForManager ? source.Where(otd => otd.OverTime.User.ManagerId == user.Id) : source.Where(otd => otd.OverTime.UserId == user.Id);
            if (statusFilters.Count > 0)
            {
                source = isDestroy ? source.Where(t => (int)t.StatusDestroy == statusFilters[0].Status) : source.Where(t => (int)t.Status == statusFilters[0].Status);
            }
            else
            {
                source = source.Where(t => t.Status != StatusOverTime.NotYet);
            }
            foreach (DateSearch date in listDateSearch)
            {
                SearchDateObject searchDate = date.GetData<SearchDateObject>();
                if (!string.IsNullOrEmpty(searchDate.StartAt))
                {
                    source = source.Where(t => t.OverTime.WorkDate.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
                if (!string.IsNullOrEmpty(searchDate.EndAt))
                {
                    source = source.Where(t => t.OverTime.WorkDate.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
            }
            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo trạng thái Pending => Approval => Rejected
            //source = isDestroy ? source.OrderBy(ar => ar.StatusDestroy) : source.OrderBy(ar => ar.Status);
            // Sort theo thời gian tạo
            source = source.OrderByDescending(ot => ot.UpdatedAt.HasValue ? ot.UpdatedAt : ot.CreatedAt);
            if (sorts != null)
            {
                foreach ((string key, bool value) in sorts)
                {
                    switch (key)
                    {
                        case "status":
                            source = value ? (isDestroy ? source.OrderBy(ar => ar.StatusDestroy) : source.OrderBy(ar => ar.Status)) : (isDestroy ? source.OrderByDescending(ar => ar.StatusDestroy) : source.OrderByDescending(ar => ar.Status));
                            break;
                        case "name":
                            source = value ? source.OrderBy(otd => otd.OverTime.User.FullName) : source.OrderByDescending(otd => otd.OverTime.User.FullName);
                            break;
                        case "email":
                            source = value ? source.OrderBy(otd => otd.OverTime.User.Email) : source.OrderByDescending(otd => otd.OverTime.User.Email);
                            break;
                        case "date":
                            source = value ? source.OrderBy(otd => otd.OverTime.WorkDate) : source.OrderByDescending(otd => otd.OverTime.WorkDate);
                            break;
                        default:
                            break;
                    }
                }
            }
            source = source.Skip(pagination.Start).Take(pagination.Number);
            return source.ToList();
        }

        public IEnumerable<OverTimeDetail> GetPendingOverTimeLastMonth(DateTime startDate, DateTime endDate, string corporationType)
        {
            return context.OverTimeDetails
                .Where(otd => otd.Status == StatusOverTime.Pending)
                .Join(context.OverTimes, otd => otd.OverTimeId, ot => ot.Id, (otd, ot) => new
                {
                    Detail = otd,
                    ot.ScriptId,
                    ot.WorkDate
                })
                .Where(otd_ot_s => otd_ot_s.WorkDate >= startDate && otd_ot_s.WorkDate <= endDate)
                .Join(context.Scripts, otd_ot => otd_ot.ScriptId, s => s.Id, (otd_ot, s) => new
                {
                    otd_ot.Detail,
                    otd_ot.WorkDate,
                    s.CorporationType
                })
                .Where(otd_ot_s => otd_ot_s.CorporationType == corporationType)
                .Select(otd_ot_s => otd_ot_s.Detail).ToList();
        }

        public int CountPendingOverTime(User user, bool isForManager, bool isDestroy)
        {
            IQueryable<OverTimeDetail> source = context.OverTimeDetails.Include(otd => otd.OverTime).ThenInclude(ot => ot.User);

            if (isDestroy)
            {
                source = source.Where(otd => otd.IsDestroy && otd.StatusDestroy != StatusOverTime.Rejected);
            }
            else
            {
                source = source.Where(otd => !otd.IsDestroy || otd.StatusDestroy == StatusOverTime.Rejected);
            }
            source = isForManager ? source.Where(otd => otd.OverTime.User.ManagerId == user.Id) : source.Where(otd => otd.OverTime.UserId == user.Id);

            source = isDestroy ? source.Where(otd => otd.StatusDestroy == StatusOverTime.Pending) : source.Where(otd => otd.Status == StatusOverTime.Pending);

            return source.Count();
        }

        public (bool, string, OverTimeDetail[]) ConfirmOverTime(StatusOverTime status, int[] ids, User manager, bool isDestroy, string rejectReason)
        {
            string resultCode = string.Empty;
            bool bRes = false;
            IQueryable<OverTimeDetail> source = context.OverTimeDetails
                .Include(td => td.OverTime).ThenInclude(t => t.User)
                .Where(t => t.OverTime.User.ManagerId == manager.Id && ids.Contains(t.Id) && t.IsDestroy == isDestroy);
            source = isDestroy ? source.Where(a => a.StatusDestroy == StatusOverTime.Pending) : source.Where(a => a.Status == StatusOverTime.Pending);

            OverTimeDetail[] details = source.ToArray();
            if (details.Count() == 0)
            {
                resultCode = ResultCode.OVERTIME_NOT_FOUND;
            }
            else
            {
                foreach (OverTimeDetail detail in details)
                {
                    if (!isDestroy)
                    {
                        detail.ApprovalTime = DateTime.Now;
                        detail.ApproverEmail = manager.Email;
                        if (!string.IsNullOrEmpty(rejectReason))
                        {
                            detail.RejectReason = rejectReason;
                        }
                        detail.Status = status;
                    }
                    else
                    {
                        detail.ApprovalDestroyTime = DateTime.Now;
                        detail.StatusDestroy = status;
                    }
                    context.OverTimeDetails.Update(detail);
                    context.SaveChanges();
                }
                bRes = true;
            }

            return (bRes, resultCode, details);
        }

        public (bool bOk, string code) DestroyOverTime(int[] ids, User user)
        {
            string resultCode = string.Empty;
            bool bRes = false;
            IQueryable<OverTimeDetail> source = context.OverTimeDetails
                .Include(td => td.OverTime)
                .Where(t => t.OverTime.UserId == user.Id && ids.Contains(t.Id) && t.IsDestroy == false);
            OverTimeDetail[] details = source.ToArray();
            if (details.Count() == 0)
            {
                resultCode = ResultCode.OVERTIME_NOT_FOUND;
            }
            else
            {
                int destroyNumber = 0;
                // Kiểm tra đăng ký ot trong tương lai và cho tháng trước sau mồng 03 hàng tháng
                DateTime startCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                DateTime startLastMonth = startCurrentMonth.AddMonths(-1).Date;

                foreach (OverTimeDetail detail in details)
                {
                    Script script = context.Scripts.Where(s => s.Id == detail.OverTime.ScriptId).FirstOrDefault();
                    // Nếu validate theo ngày cutoff
                    if (((detail.OverTime.WorkDate < startCurrentMonth) && DateTime.Now.Day >= script.CutoffTimeKeepingDay)
                        || ((detail.OverTime.WorkDate < startLastMonth) && DateTime.Now.Day < script.CutoffTimeKeepingDay))
                    {
                        continue;
                    }

                    detail.IsDestroy = true;
                    detail.StatusDestroy = StatusOverTime.Approved;
                    detail.DestroyTime = DateTime.Now;
                    detail.ApprovalDestroyTime = DateTime.Now;
                    context.OverTimeDetails.Update(detail);
                    context.SaveChanges();
                    destroyNumber++;
                }
                if (destroyNumber > 0)
                {
                    bRes = true;
                }
                else
                {
                    resultCode = ResultCode.CAN_NOT_DESTROY;
                }
            }
            return (bRes, resultCode);
        }

        public int[] GetAutoOverTimeDetails(User user, DateTime workDate)
        {
            return context.OverTimeDetails.Include(otd => otd.OverTime)
                .Where(otd => otd.OverTime.WorkDate == workDate && otd.OverTime.UserId == user.Id && !otd.IsManual).Select(otd => otd.Id).ToArray();
        }

        public List<OverTimeDetail> GetAllOvertimeInMonthByUser(int userId, int scriptId, DateTime startDate, DateTime endDate, string groupEventCode)
        {
            return context.OverTimeDetails.Include(otd => otd.OverTime)
                .Include(otd => otd.Event)
                .ThenInclude(e => e.GroupEvent)
                .Where(otd => otd.Event.GroupEvent.Code == groupEventCode)
                .Where(otd => otd.IsDestroy == false || otd.StatusDestroy == StatusOverTime.Rejected)
                .Where(otd => otd.OverTime.ScriptId == scriptId)
                .Where(otd => otd.OverTime.UserId == userId && otd.OverTime.WorkDate >= startDate && otd.OverTime.WorkDate <= endDate).ToList();
        }

        public IEnumerable<(int UserId,
            DateTime StartTime,
            DateTime EndTime,
            bool IsManual,
            int EventId,
            DateTime WorkDate,
            int GroupEventId,
            string HcmEventId,
            double? Percentage,
            double? PerPayHour,
            int LunchBreak)> GetOvertimeHcm(string corporationType, int? employeeTypeId, DateTime startDate, DateTime endDate)
        {
            IEnumerable<dynamic> result = context.OverTimeDetails
                .Join(context.OverTimes, otd => otd.OverTimeId, ot => ot.Id, (otd, ot) => new
                {
                    otd.StartTime,
                    otd.EndTime,
                    otd.IsDestroy,
                    otd.Status,
                    otd.StatusDestroy,
                    otd.IsManual,
                    otd.EventId,
                    ot.UserId,
                    ot.WorkDate,
                    ot.ScriptId
                })
                .Join(context.HrbHistories, otd_ot =>  otd_ot.UserId, hrb => hrb.UserId, (otd_ot, hrb) => new
                {
                    otd_ot.StartTime,
                    otd_ot.EndTime,
                    otd_ot.IsDestroy,
                    otd_ot.Status,
                    otd_ot.StatusDestroy,
                    otd_ot.IsManual,
                    otd_ot.EventId,
                    otd_ot.UserId,
                    otd_ot.WorkDate,
                    otd_ot.ScriptId,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(otd_ot_hrb => otd_ot_hrb.ExpirationDate >= otd_ot_hrb.WorkDate && otd_ot_hrb.EffectiveDate <= otd_ot_hrb.WorkDate)
                .Where(otd_ot_hrb => otd_ot_hrb.HrbStatus == HrbStatus.Active)
                .Join(context.ScriptHistorys, otd_ot => new { otd_ot.UserId, otd_ot.ScriptId }, sh => new { sh.UserId, sh.ScriptId }, (otd_ot, sh) => new
                {
                    otd_ot.StartTime,
                    otd_ot.EndTime,
                    otd_ot.IsDestroy,
                    otd_ot.Status,
                    otd_ot.StatusDestroy,
                    otd_ot.IsManual,
                    otd_ot.EventId,
                    otd_ot.UserId,
                    otd_ot.WorkDate,
                    otd_ot.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(otd_ot_sh => otd_ot_sh.ExpirationDate >= otd_ot_sh.WorkDate && otd_ot_sh.EffectiveDate <= otd_ot_sh.WorkDate)
                .Join(context.Scripts, otd_ot => otd_ot.ScriptId, s => s.Id, (otd_ot, s) => new
                {
                    otd_ot.StartTime,
                    otd_ot.EndTime,
                    otd_ot.IsDestroy,
                    otd_ot.Status,
                    otd_ot.StatusDestroy,
                    otd_ot.IsManual,
                    otd_ot.EventId,
                    otd_ot.WorkDate,
                    otd_ot.UserId,
                    s.EmployeeTypeId,
                    s.LunchBreak,
                    s.CorporationType
                })
                .Where(otd_ot_s => otd_ot_s.CorporationType == corporationType)
                .Join(context.Events, otd_ot_s => otd_ot_s.EventId, e => e.Id, (otd_ot_s, e) => new
                {
                    otd_ot_s.UserId,
                    otd_ot_s.StartTime,
                    otd_ot_s.EndTime,
                    otd_ot_s.IsDestroy,
                    otd_ot_s.Status,
                    otd_ot_s.StatusDestroy,
                    otd_ot_s.IsManual,
                    otd_ot_s.EventId,
                    otd_ot_s.WorkDate,
                    otd_ot_s.EmployeeTypeId,
                    otd_ot_s.LunchBreak,
                    e.HCMEventId,
                    e.Percentage,
                    e.PayPerHour,
                    e.PayPerBlock,
                    e.GroupEventId
                })
                .Where(otd_ot_s_e => !otd_ot_s_e.PayPerBlock.HasValue)
                .Where(otd_ot_s_e => otd_ot_s_e.Status == StatusOverTime.Approved && (otd_ot_s_e.IsDestroy == false || otd_ot_s_e.StatusDestroy == StatusOverTime.Rejected))
                .Where(otd_ot_s_e => otd_ot_s_e.WorkDate >= startDate && otd_ot_s_e.WorkDate <= endDate);
            if (employeeTypeId.HasValue)
            {
                result = result.Where(otd_ot_s_e => otd_ot_s_e.EmployeeTypeId == employeeTypeId);
            }
            foreach (dynamic item in result)
            {
                yield return (item.UserId, item.StartTime, item.EndTime, item.IsManual, item.EventId, item.WorkDate, item.GroupEventId, item.HCMEventId, item.Percentage, item.PayPerHour, item.LunchBreak);
            }
        }

        // Old version
        public IEnumerable<(int UserId, string Email, string FullName, string Department, string CalendarType, DateTime WorkDate, OverTimeDetail Detail, string GroupEventName, string EventCode, int? Percentage, double? PerPayHour, int LunchBreak)> GetOvertimeForExport(DateTime startDate, DateTime endDate)
        {
            IEnumerable<dynamic> result = context.OverTimeDetails
                    .Where(otd => otd.IsDestroy == false || otd.StatusDestroy == StatusOverTime.Rejected)
                    .Join(context.OverTimes, otd => otd.OverTimeId, ot => ot.Id, (otd, ot) => new
                    {
                        Detail = otd,
                        ot.UserId,
                        ot.WorkDate,
                        ot.ScriptId
                    })
                    .Where(otd_ot => otd_ot.WorkDate >= startDate && otd_ot.WorkDate <= endDate)
                    .Join(context.Users, otd_ot => otd_ot.UserId, u => u.Id, (otd_ot, u) => new
                    {
                        otd_ot.Detail,
                        otd_ot.WorkDate,
                        otd_ot.UserId,
                        otd_ot.ScriptId,
                        u.FullName,
                        u.Department,
                        u.Email,
                    })
                    .Join(context.LeaveCalendarTypeHistories, otd_ot_u => otd_ot_u.UserId, ch => ch.UserId, (otd_ot_u, ch) => new {
                        otd_ot_u.Detail,
                        otd_ot_u.WorkDate,
                        otd_ot_u.UserId,
                        otd_ot_u.ScriptId,
                        otd_ot_u.FullName,
                        otd_ot_u.Department,
                        otd_ot_u.Email,
                        CalendarType = ch.Code,
                        ch.EffectiveDate,
                        ch.ExpirationDate
                    })
                    .Where(otd_ot_u => otd_ot_u.WorkDate >= otd_ot_u.EffectiveDate && otd_ot_u.WorkDate <= otd_ot_u.ExpirationDate)
                    .Join(context.Scripts, otd_ot_u => otd_ot_u.ScriptId, s => s.Id, (otd_ot_u, s) => new
                    {
                        otd_ot_u.Detail,
                        otd_ot_u.WorkDate,
                        otd_ot_u.UserId,
                        otd_ot_u.FullName,
                        otd_ot_u.Department,
                        otd_ot_u.Email,
                        otd_ot_u.CalendarType,
                        s.EmployeeTypeId,
                        s.LunchBreak
                    })
                    .Join(context.Events, otd_ot_u_s => otd_ot_u_s.Detail.EventId, e => e.Id, (otd_ot_u_s, e) => new
                    {
                        otd_ot_u_s.UserId,
                        otd_ot_u_s.Detail,
                        otd_ot_u_s.WorkDate,
                        otd_ot_u_s.FullName,
                        otd_ot_u_s.Email,
                        otd_ot_u_s.Department,
                        otd_ot_u_s.CalendarType,
                        otd_ot_u_s.EmployeeTypeId,
                        otd_ot_u_s.LunchBreak,
                        e.EventCode,
                        e.Percentage,
                        e.PayPerHour,
                        e.GroupEventId
                    })
                    .Join(context.GroupEvents, otd_ot_u_s_e => otd_ot_u_s_e.GroupEventId, ge => ge.Id, (otd_ot_u_s_e, ge) => new
                    {
                        otd_ot_u_s_e.UserId,
                        otd_ot_u_s_e.Detail,
                        otd_ot_u_s_e.WorkDate,
                        otd_ot_u_s_e.FullName,
                        otd_ot_u_s_e.Email,
                        otd_ot_u_s_e.Department,
                        otd_ot_u_s_e.CalendarType,
                        otd_ot_u_s_e.EmployeeTypeId,
                        otd_ot_u_s_e.EventCode,
                        otd_ot_u_s_e.Percentage,
                        otd_ot_u_s_e.PayPerHour,
                        otd_ot_u_s_e.LunchBreak,
                        GroupEventName = ge.Name
                    });

            foreach (dynamic item in result)
            {
                yield return (item.UserId, item.Email, item.FullName, item.Department, item.CalendarType, item.WorkDate, item.Detail, item.GroupEventName, item.EventCode, item.Percentage, item.PayPerHour, item.LunchBreak);
            }
        }

        // New version
        public IEnumerable<(int UserId, string Email, string FullName, string Department, string CalendarType, DateTime WorkDate, OverTimeDetail Detail, string GroupEventName, string EventCode, int? Percentage, double? PerPayHour, int LunchBreak)> GetOvertimeForExport(DateTime startDate, DateTime endDate, int scriptId)
        {
            IEnumerable<dynamic> result = context.OverTimeDetails
                    .Where(otd => otd.IsDestroy == false || otd.StatusDestroy == StatusOverTime.Rejected)
                    .Join(context.OverTimes, otd => otd.OverTimeId, ot => ot.Id, (otd, ot) => new
                    {
                        Detail = otd,
                        ot.UserId,
                        ot.WorkDate,
                        ot.ScriptId
                    })
                    .Where(otd_ot => otd_ot.WorkDate >= startDate && otd_ot.WorkDate <= endDate)
                    .Where(otd_ot => otd_ot.ScriptId == scriptId)
                    .Join(context.HrbHistories, otd_ot => otd_ot.UserId, hrb => hrb.UserId, (otd_ot, hrb) => new
                    {
                        otd_ot.Detail,
                        otd_ot.UserId,
                        otd_ot.WorkDate,
                        otd_ot.ScriptId,
                        hrb.HrbStatus,
                        hrb.EffectiveDate,
                        hrb.ExpirationDate
                    })
                    .Where(otd_ot_hrb => otd_ot_hrb.ExpirationDate >= otd_ot_hrb.WorkDate && otd_ot_hrb.EffectiveDate <= otd_ot_hrb.WorkDate)
                    .Where(otd_ot_hrb => otd_ot_hrb.HrbStatus == HrbStatus.Active)
                    .Join(context.Users, otd_ot => otd_ot.UserId, u => u.Id, (otd_ot, u) => new
                    {
                        otd_ot.Detail,
                        otd_ot.WorkDate,
                        otd_ot.UserId,
                        otd_ot.ScriptId,
                        u.FullName,
                        u.Department,
                        u.Email,
                    })
                    .Join(context.LeaveCalendarTypeHistories, otd_ot_u => otd_ot_u.UserId, ch => ch.UserId, (otd_ot_u, ch) => new {
                        otd_ot_u.Detail,
                        otd_ot_u.WorkDate,
                        otd_ot_u.UserId,
                        otd_ot_u.ScriptId,
                        otd_ot_u.FullName,
                        otd_ot_u.Department,
                        otd_ot_u.Email,
                        CalendarType = ch.Code,
                        ch.EffectiveDate,
                        ch.ExpirationDate
                    })
                    .Where(otd_ot_u => otd_ot_u.WorkDate >= otd_ot_u.EffectiveDate && otd_ot_u.WorkDate <= otd_ot_u.ExpirationDate)
                    .Join(context.Scripts, otd_ot_u => otd_ot_u.ScriptId, s => s.Id, (otd_ot_u, s) => new
                    {
                        otd_ot_u.Detail,
                        otd_ot_u.WorkDate,
                        otd_ot_u.UserId,
                        otd_ot_u.FullName,
                        otd_ot_u.Department,
                        otd_ot_u.Email,
                        otd_ot_u.CalendarType,
                        s.EmployeeTypeId,
                        s.LunchBreak
                    })
                    .Join(context.Events, otd_ot_u_s => otd_ot_u_s.Detail.EventId, e => e.Id, (otd_ot_u_s, e) => new
                    {
                        otd_ot_u_s.UserId,
                        otd_ot_u_s.Detail,
                        otd_ot_u_s.WorkDate,
                        otd_ot_u_s.FullName,
                        otd_ot_u_s.Email,
                        otd_ot_u_s.Department,
                        otd_ot_u_s.CalendarType,
                        otd_ot_u_s.EmployeeTypeId,
                        otd_ot_u_s.LunchBreak,
                        e.EventCode,
                        e.Percentage,
                        e.PayPerHour,
                        e.GroupEventId
                    })
                    .Join(context.GroupEvents, otd_ot_u_s_e => otd_ot_u_s_e.GroupEventId, ge => ge.Id, (otd_ot_u_s_e, ge) => new
                    {
                        otd_ot_u_s_e.UserId,
                        otd_ot_u_s_e.Detail,
                        otd_ot_u_s_e.WorkDate,
                        otd_ot_u_s_e.FullName,
                        otd_ot_u_s_e.Email,
                        otd_ot_u_s_e.Department,
                        otd_ot_u_s_e.CalendarType,
                        otd_ot_u_s_e.EmployeeTypeId,
                        otd_ot_u_s_e.EventCode,
                        otd_ot_u_s_e.Percentage,
                        otd_ot_u_s_e.PayPerHour,
                        otd_ot_u_s_e.LunchBreak,
                        GroupEventName = ge.Name
                    });

            foreach (dynamic item in result)
            {
                yield return (item.UserId, item.Email, item.FullName, item.Department, item.CalendarType, item.WorkDate, item.Detail, item.GroupEventName, item.EventCode, item.Percentage, item.PayPerHour, item.LunchBreak);
            }
        }
    }
}
