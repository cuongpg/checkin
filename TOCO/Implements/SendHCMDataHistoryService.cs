﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class SendHCMDataHistoryService : BaseService
    {
        public SendHCMDataHistoryService(ApplicationDbContext context) : base(context)
        {
        }

        public SendHCMDataHistory GetById(int id)
        {
            return context.HCMDataHistories.Where(s => s.Id == id).FirstOrDefault();
        }
        public SendHCMDataHistory Create(SendHCMDataHistory history)
        {
            history.CreatedAt = DateTime.Now;
            history.UpdatedAt = DateTime.Now;
            context.HCMDataHistories.Add(history);
            context.SaveChanges();
            return history;
        }

        public SendHCMDataHistory Update(SendHCMDataHistory history)
        {
            history.UpdatedAt = DateTime.Now;
            context.HCMDataHistories.Update(history);
            context.SaveChanges();
            return history;
        }

        public bool Delete(int id)
        {
            var history = GetById(id);

            if (history == null)
            {
                return false;
            }

            context.HCMDataHistories.Remove(history);
            context.SaveChanges();

            return true;
        }

    }
}
