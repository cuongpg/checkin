﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class CorporationHistoryService : BaseService
    {
        public CorporationHistoryService(ApplicationDbContext context) : base(context)
        {
        }

        public CorporationHistory GetById(int id)
        {
            return context.CorporationHistories.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<CorporationHistory> GetAllByUser(int userId)
        {
            return context.CorporationHistories
                .Where(s => s.UserId == userId)
                .OrderBy(s => s.ExpirationDate);
        }

        public IEnumerable<CorporationHistory> GetByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        {
            return context.CorporationHistories
                .Where(s => s.UserId == userId)
                .Where(s => !(s.EffectiveDate > endTime || s.ExpirationDate < startTime));
        }


        public CorporationHistory Create(CorporationHistory history)
        {
            history.CreatedAt = DateTime.Now;
            history.UpdatedAt = DateTime.Now;
            context.CorporationHistories.Add(history);
            context.SaveChanges();
            return history;
        }

        public CorporationHistory Update(CorporationHistory history)
        {
            history.UpdatedAt = DateTime.Now;
            context.CorporationHistories.Update(history);
            context.SaveChanges();
            return history;
        }

        public bool Delete(int id)
        {
            var history = GetById(id);

            if (history == null)
            {
                return false;
            }

            context.CorporationHistories.Remove(history);
            context.SaveChanges();

            return true;
        }

        public CorporationHistory GetCorporationInDay(int userId, DateTime day)
        {
            return context.CorporationHistories
                 .Where(s => s.UserId == userId)
                 .Where(s => s.EffectiveDate <= day && s.ExpirationDate >= day)
                 .FirstOrDefault();
        }

        public CorporationHistory GetLastCorporation(int userId)
        {
            return context.CorporationHistories
                 .Where(s => s.UserId == userId)
                 .OrderByDescending(s => s.EffectiveDate)
                 .FirstOrDefault();
        }
    }
}
