﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Implements
{
    public class ReasonTypeService : BaseService
    {

        public ReasonTypeService(ApplicationDbContext context) : base(context)
        {
        }

        public List<ReasonType> GetReasonTypes(bool hasPaidLeave, string corporationType)
        {
            return context.ReasonTypes
                .Where(r => (!r.HasPaidLeave || hasPaidLeave) && r.CorporationType == corporationType)
                .Where(r => !r.IsHide)
                .OrderBy(r => r.OrderBy)
                .ToList();
        }


        public ReasonType GetById(int id)
        {
            return context.ReasonTypes.FirstOrDefault(e => e.Id == id);
        }

        public ReasonType GetReasonById(int reasonTypeId)
        {
            return context.ReasonTypes.Where(r => r.Id == reasonTypeId).FirstOrDefault();
        }

        public List<ReasonType> GetHasBufferReasonTypes()
        {
            return context.ReasonTypes.Where(r => r.HasBufferDate).ToList();
        }

        public List<ReasonType> GetHasQuotasReasonsByUser(string corporationType)
        {
            return context.ReasonTypes
                .Where(r => r.HasPaidLeave)
                .Where(r => r.HasBufferDate)
                .Where(r => r.CorporationType == corporationType)
                .Where(r => !r.IsHide)
                .OrderBy(r => r.OrderBy)
                .ToList();
        }
    }

}
