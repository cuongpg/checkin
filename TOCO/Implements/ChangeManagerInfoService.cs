﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Models.ResultModels.WisamiResultModels;
using System.Globalization;


namespace TOCO.Implements
{
    public class ChangeManagerInfoService : BaseService
    {
        public ChangeManagerInfoService(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Create a manager change
        /// </summary>
        /// <param name="changeManager"></param>
        /// <returns></returns>
        public ChangeManagerInfo Create(ChangeManagerInfo changeManager)
        {
            changeManager.CreatedAt = DateTime.Now;
            changeManager.UpdatedAt = DateTime.Now;
            context.ChangeManagerInfo.Add(changeManager);
            context.SaveChanges();
            return changeManager;
        }


        /// <summary>
        /// Confirm change manager
        /// </summary>
        /// <param name="status"></param>
        /// <param name="id"></param>
        /// <param name="manager"></param>
        /// <returns></returns>
        public (bool bOk, string code, ChangeManagerInfo records) ConfirmChangeManager(StatusChangeManagerInfo status, int id, User manager, string rejectReason)
        {
            string resultCode = string.Empty;
            bool bRes = false;
            IQueryable<ChangeManagerInfo> source = context.ChangeManagerInfo.Where(cm => cm.Id == id);

            ChangeManagerInfo changeManager = source.FirstOrDefault();
            if (changeManager == null)
            {
                resultCode = ResultCode.CHANGE_MANAGER_NOT_FOUND;
                bRes = false;
            }
            else
            {
                if (manager.Id != changeManager.OldManagerId && manager.Id != changeManager.NewManagerId)
                {
                    resultCode = ResultCode.MANAGER_NOT_FOUND;
                    bRes = false;
                }
                else
                {
                    if (status == StatusChangeManagerInfo.Approved)
                    {
                        if (changeManager.OldManagerId == manager.Id)
                            changeManager.OldManagerStatus = StatusChangeManagerInfo.Approved;
                        if (changeManager.NewManagerId == manager.Id)
                            changeManager.NewManagerStatus = StatusChangeManagerInfo.Approved;
                        if (changeManager.NewManagerStatus == StatusChangeManagerInfo.Approved
                            && changeManager.OldManagerStatus == StatusChangeManagerInfo.Approved)
                        {
                            changeManager.Status = StatusChangeManagerInfo.Approved;
                            int userId = changeManager.UserId;
                            User user = context.Users
                                .Include(u => u.UserGroups)
                                .ThenInclude(ug => ug.Group).FirstOrDefault(u => u.Id == userId);
                            user.ManagerId = changeManager.NewManagerId;
                            context.Users.Update(user);
                        }
                        changeManager.UpdatedAt = DateTime.Now;
                    }
                    else
                    {
                        if (changeManager.OldManagerId == manager.Id)
                            changeManager.OldManagerStatus = StatusChangeManagerInfo.Rejected;
                        if (changeManager.NewManagerId == manager.Id)
                            changeManager.NewManagerStatus = StatusChangeManagerInfo.Rejected;
                        changeManager.Status = StatusChangeManagerInfo.Rejected;
                        changeManager.UpdatedAt = DateTime.Now;
                    }
                    if (!string.IsNullOrEmpty(rejectReason))
                    {
                        changeManager.RejectReason = rejectReason;
                    }
                    context.ChangeManagerInfo.Update(changeManager);
                    context.SaveChanges();
                    bRes = true;
                }
            }
            return (bRes, resultCode, changeManager);
        }

        /// <summary>
        /// get my change manager info
        /// </summary>
        /// <param name="user"></param>
        /// <param name="numberOfPages"></param>
        /// <param name="totalRecords"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<ChangeManagerInfo> GetMyChangeManagerInfo(User user, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            numberOfPages = 1;
            totalRecords = 0;
            DateTime fiveDayBefore = DateTime.Now.AddDays(-7);
            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();
            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();
            IQueryable<ChangeManagerInfo> source = context.ChangeManagerInfo
                .Where(cm => cm.UserId == user.Id && (cm.Status == StatusChangeManagerInfo.Pending || cm.UpdatedAt >= fiveDayBefore))
                .OrderBy(cm => cm.Status);

            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo trạng thái Pending => Approval => Rejected

            source = source.Skip(pagination.Start).Take(pagination.Number);
            return source.ToList();
        }


        public ChangeManagerInfo GetLastApprovalChangeManagerInfo(int userId, DateTime date)
        {
            ChangeManagerInfo source = context
                 .ChangeManagerInfo
                 .Where(ch => ch.UserId == userId)
                 .Where(ch => ch.Status == StatusChangeManagerInfo.Approved)
                 .Where(ch => ch.UpdatedAt <= date)
                 .OrderByDescending(ch => ch.UpdatedAt)
                 .FirstOrDefault();

            return source;
        }



        public int CountPendingChangeManager(User user, bool isManager)
        {
            IQueryable<ChangeManagerInfo> source = context.ChangeManagerInfo
                .Where(cm => isManager ? (cm.Status == StatusChangeManagerInfo.Pending && ((cm.NewManagerId == user.Id && cm.NewManagerStatus == StatusChangeManagerInfo.Pending) || (cm.OldManagerId == user.Id && cm.OldManagerStatus == StatusChangeManagerInfo.Pending))) : (cm.UserId == user.Id && cm.Status == StatusChangeManagerInfo.Pending) );
            return source.Count();
        }

        /// <summary>
        /// get change manager information need to approve
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="numberOfPages"></param>
        /// <param name="totalRecords"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<ChangeManagerInfo> GetChangeManagerInfoToApprove(User manager, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            DateTime fiveDayBefore = DateTime.Now.AddDays(-7);
            numberOfPages = 1;
            totalRecords = 0;
            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();
            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();
            IQueryable<ChangeManagerInfo> source = context.ChangeManagerInfo
                .Where(cm => (cm.NewManagerId == manager.Id || cm.OldManagerId == manager.Id) && (cm.Status == StatusChangeManagerInfo.Pending || cm.UpdatedAt >= fiveDayBefore))
                .OrderBy(cm => cm.Status);

            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo trạng thái Pending => Approval => Rejected

            source = source.Skip(pagination.Start).Take(pagination.Number);
            var a = source.ToList();
            return source.ToList();
        }

        /// <summary>
        /// Get my pending change manager infomation
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ChangeManagerInfo GetMyPendingChangeManagerInfo(User user)
        {
            return context.ChangeManagerInfo.Where(cm => cm.UserId == user.Id && cm.Status == StatusChangeManagerInfo.Pending).FirstOrDefault();
        }

        /// <summary>
        /// destroy the change manager info
        /// </summary>
        /// <param name="changeForm"></param>
        /// <returns></returns>
        public (bool bOk, string message, ChangeManagerInfo records) DestroyChangeManagerInfo(int id)
        {
            bool bRes = false;
            string resultCode = string.Empty;
            IQueryable<ChangeManagerInfo> source = context.ChangeManagerInfo.Where(cm => cm.Id == id);
            ChangeManagerInfo changeForm = source.FirstOrDefault();
            if (changeForm == null)
            {
                resultCode = ResultCode.CHANGE_MANAGER_NOT_FOUND;
                bRes = false;
            }
            else
            {
                changeForm.Status = StatusChangeManagerInfo.Destroyed;
                changeForm.UpdatedAt = DateTime.Now;
                context.ChangeManagerInfo.Update(changeForm);
                context.SaveChanges();
                bRes = true;
            }
            return (bRes, resultCode, changeForm);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ChangeManagerInfo GetChangeManagerInfoById(int id)
        {
            return context.ChangeManagerInfo.Where(cm => cm.Id == id).FirstOrDefault();
        }

        public List<ChangeManagerInfo> GetChangeManagerInfos(string nationality, User employee, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            numberOfPages = 1;
            totalRecords = 0;
            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();
            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();
            IQueryable<ChangeManagerInfo> source = context.ChangeManagerInfo;

            source = source.Join(context.Users, cmi => cmi.UserId, u => u.Id, (cmi, u) => new
            {
                ChangeManagerInfo = cmi,
                u.Nationality
            }).Where(cmi_u => cmi_u.Nationality == nationality).Select(cmi_u => cmi_u.ChangeManagerInfo);

            if (!(employee is null))
            {
                source = source.Where(cmi => cmi.UserId == employee.Id);
            }
            if (statusFilters.Count > 0)
            {
                source = source.Where(cmi => (int)cmi.Status == statusFilters[0].Status);
            }
            foreach (DateSearch date in listDateSearch)
            {
                SearchDateObject searchDate = date.GetData<SearchDateObject>();
                if (!string.IsNullOrEmpty(searchDate.StartAt))
                {
                    source = source.Where(cmi => cmi.CreatedAt.Date >= DateTime.ParseExact(searchDate.StartAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
                if (!string.IsNullOrEmpty(searchDate.EndAt))
                {
                    source = source.Where(cmi => cmi.CreatedAt.Date <= DateTime.ParseExact(searchDate.EndAt, Constants.FORMAT_DATE_DDMMYYYY, CultureInfo.InvariantCulture));
                }
            }
            source = source.OrderByDescending(cmi => cmi.CreatedAt);
            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            // Sort theo trạng thái Pending => Approval => Rejected

            source = source.Skip(pagination.Start).Take(pagination.Number);
            var a = source.ToList();
            return source.ToList();
        }

    }
}
