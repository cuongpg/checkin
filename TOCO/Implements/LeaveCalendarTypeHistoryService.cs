﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class LeaveCalendarTypeHistoryService : BaseService
    {
        public LeaveCalendarTypeHistoryService(ApplicationDbContext context) : base(context)
        {
        }

        public LeaveCalendarTypeHistory GetById(int id)
        {
            return context.LeaveCalendarTypeHistories.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<LeaveCalendarTypeHistory> GetAllByUser(int userId)
        {
            return context.LeaveCalendarTypeHistories
                .Where(s => s.UserId == userId)
                .OrderBy(s => s.ExpirationDate);
        }

        public IEnumerable<LeaveCalendarTypeHistory> GetByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        {
            return context.LeaveCalendarTypeHistories
                .Where(s => s.UserId == userId)
                .Where(s => !(s.EffectiveDate > endTime || s.ExpirationDate < startTime));
        }


        public LeaveCalendarTypeHistory Create(LeaveCalendarTypeHistory history)
        {
            history.CreatedAt = DateTime.Now;
            history.UpdatedAt = DateTime.Now;
            context.LeaveCalendarTypeHistories.Add(history);
            context.SaveChanges();
            return history;
        }

        public LeaveCalendarTypeHistory Update(LeaveCalendarTypeHistory history)
        {
            history.UpdatedAt = DateTime.Now;
            context.LeaveCalendarTypeHistories.Update(history);
            context.SaveChanges();
            return history;
        }

        public bool Delete(int id)
        {
            var history = GetById(id);

            if (history == null)
            {
                return false;
            }

            context.LeaveCalendarTypeHistories.Remove(history);
            context.SaveChanges();

            return true;
        }

        public LeaveCalendarTypeHistory GetLeaveCalendarTypeHistoryInDay(int userId, DateTime day)
        {
            return context.LeaveCalendarTypeHistories
                 .Where(s => s.UserId == userId)
                 .Where(s => s.EffectiveDate <= day && s.ExpirationDate >= day)
                 .FirstOrDefault();
        }

        public IEnumerable<LeaveCalendarTypeHistory> GetFutureHistories(int userId, DateTime time)
        {
            return context.LeaveCalendarTypeHistories
               .Where(s => s.UserId == userId)
               .Where(s => s.EffectiveDate > time);
        }
    }
}
