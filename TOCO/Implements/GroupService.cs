﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class GroupService : BaseService
    {
        private readonly UserService _userService;

        public GroupService(ApplicationDbContext context) : base(context)
        {
            _userService = new UserService(context);
        }

        public List<Group> GetAll(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;

            IQueryable<Group> query = QueryableHelper.Default(context.Groups);

            STHelper.Process<Group, GroupPagination>(request, ref numberOfPages, ref totalRecords, ref query);

            return query.ToList();
        }

        public List<Group> GetAllNotPagination()
        {
            return context.Groups.Include(u => u.GroupRoutes).ToList();
        }

        public Group GetById(int id)
        {
            return context.Groups.Include(u => u.GroupRoutes).ThenInclude(u => u.Route).FirstOrDefault(g => g.Id == id);
        }

        public Group GetByIdWithUsers(int id)
        {
            return context.Groups.Include(u => u.UserGroups).ThenInclude(u => u.User)
                .Include(r => r.GroupRoutes).ThenInclude(r => r.Route).FirstOrDefault(g => g.Id == id);
        }

        public Group Create(Group group)
        {
            group.CreatedAt = DateTime.Now;
            group.UpdatedAt = DateTime.Now;

            context.Groups.Add(group);
            context.SaveChanges();

            return group;
        }

        public bool Update(int id, Group group)
        {
            var model = GetByIdWithUsers(id);

            if (model == null)
            {
                return false;
            }

            model.Name = group.Name;
            model.Description = group.Description;
            model.UpdatedAt = DateTime.Now;

            var oldRoutes = model.GroupRoutes.Select(ur => ur.RouteId).ToList();
            var newRoutes = group.GroupRoutes.Select(ur => ur.RouteId).ToList();
            var intersectRoutes = oldRoutes.Intersect(newRoutes).ToList();
            var listRouteRemove = new List<int>();

            foreach (var item in model.GroupRoutes)
            {
                if (intersectRoutes.IndexOf(item.RouteId) != -1)
                {
                    context.Entry(item).State = EntityState.Unchanged;
                    group.GroupRoutes.Remove(group.GroupRoutes.First(ur => ur.RouteId == item.RouteId));
                }
                else if (oldRoutes.IndexOf(item.RouteId) != -1)
                {
                    context.Entry(item).State = EntityState.Deleted;
                    listRouteRemove.Add(item.RouteId);
                }
            }

            model.GroupRoutes = model.GroupRoutes.Concat(group.GroupRoutes).ToList();

            context.Groups.Update(model);

            context.SaveChanges();

            return true;
        }

        public bool Delete(int id)
        {
            var group = GetById(id);

            if (group == null)
            {
                return false;
            }

            context.Groups.Remove(group);
            context.SaveChanges();

            return true;
        }

        public List<Group> GetByUserId(User user)
        {
            IQueryable<Group> query = context.UserGroup.Where(ug => ug.UserId == user.Id)
                .Join(context.Groups, ug => ug.GroupId, g => g.Id, (ug, g) => new
                {
                    Group = g
                })
                .Select(ug_g => ug_g.Group);
            return query.ToList();
        }
    }
}
