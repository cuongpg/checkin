﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class EmployeeTypeService : BaseService
    {
        public EmployeeTypeService(ApplicationDbContext context) : base(context)
        {
        }

        public EmployeeType GetById(int id)
        {
            return context.EmployeeTypes.FirstOrDefault(e => e.Id == id);
        }

        public EmployeeType GetByName(string code)
        {
            return context.EmployeeTypes.FirstOrDefault(e => e.Name == code);
        }
    }
}
