﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class LeaveDateService : BaseService
    {
        public LeaveDateService(ApplicationDbContext context) : base(context)
        {
        }

        public List<LeaveDate> GetAll(out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;

            IQueryable<LeaveDate> query = QueryableHelper.Default(context.LeaveDates /*.Include(ld => ld.LeaveDateType)*/);

            STHelper.Process<LeaveDate, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);

            return query.ToList();
        }

        public List<LeaveDate> GetByDate(DateTime date)
        {
            return context.LeaveDates.Where(l => l.Date == date).ToList();
        }

        public List<LeaveDate> GetByType(string type)
        {
            return context.LeaveDates.Where(l => l.Type == type).ToList();
        }

        public LeaveDate GetByDateAndType(DateTime date, string type)
        {
            return context.LeaveDates.Where(l => l.Date == date && l.Type == type).FirstOrDefault();
        }

        public LeaveDate Create(LeaveDate leaveDate)
        {
            leaveDate.CreatedAt = DateTime.Now;
            leaveDate.UpdatedAt = DateTime.Now;
            context.LeaveDates.Add(leaveDate);
            context.SaveChanges();

            return leaveDate;
        }

        public List<LeaveDate> GetDateInMonthByUser(int month, int year, int userId)
        {
            List<LeaveDate> holidayDates = new List<LeaveDate>();
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
            holidayDates = context.LeaveDates
                .Join(context.LeaveCalendarTypeHistories, ld => ld.Type, ch => ch.Code, (ld, ch) => new {
                    Detail = ld,
                    History = ch
                })
                .Where(ld_ch => ld_ch.History.UserId == userId)
                .Where(ld_ch => ld_ch.Detail.Date >= ld_ch.History.EffectiveDate && ld_ch.Detail.Date <= ld_ch.History.ExpirationDate)
                .Where(ld_ch => ld_ch.Detail.Date >= startDate && ld_ch.Detail.Date <= endDate)
                .Select(ld_ch => ld_ch.Detail)
                .ToList();
            return holidayDates;
        }

        public List<LeaveDate> GetLeaveDateByUserAndTime(DateTime startDate, DateTime endDate, int userId)
        {
            List<LeaveDate> holidayDates = new List<LeaveDate>();;
            holidayDates = context.LeaveDates
               .Join(context.LeaveCalendarTypeHistories, ld => ld.Type, ch => ch.Code, (ld, ch) => new {
                   Detail = ld,
                   History = ch
               })
               .Where(ld_ch => ld_ch.History.UserId == userId)
               .Where(ld_ch => ld_ch.Detail.Date >= ld_ch.History.EffectiveDate && ld_ch.Detail.Date <= ld_ch.History.ExpirationDate)
               .Where(ld_ch => ld_ch.Detail.Date >= startDate && ld_ch.Detail.Date <= endDate)
               .Select(ld_ch => ld_ch.Detail)
               .ToList();
            return holidayDates;
        }
    }
}
