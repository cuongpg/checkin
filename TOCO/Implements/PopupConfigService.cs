﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class PopupConfigService : BaseService
    {
        public PopupConfigService(ApplicationDbContext context) : base(context)
        {
        }

        public PopupConfig GetById(int id)
        {
            return context.PopupConfigs.FirstOrDefault(e => e.Id == id);
        }


        public List<PopupConfig> GetAll()
        {
            return context.PopupConfigs.ToList();
        }

        public PopupConfig Create(PopupConfig config)
        {
            config.CreatedAt = DateTime.Now;
            config.UpdatedAt = DateTime.Now;
            context.PopupConfigs.Add(config);
            context.SaveChanges();
            return config;
        }

        public PopupConfig Update(PopupConfig config)
        {
            config.UpdatedAt = DateTime.Now;
            context.PopupConfigs.Update(config);
            context.SaveChanges();
            return config;
        }

        public bool Delete(int id)
        {
            var config = GetById(id);

            if (config == null)
            {
                return false;
            }

            context.PopupConfigs.Remove(config);
            context.SaveChanges();

            return true;
        }

        public PopupConfig GetConfigByDate(DateTime timeNow)
        {
            return context.PopupConfigs
                .Where(p => p.Status == PopupConfigStatus.Active)
                .Where(p => p.EffectiveDate <= timeNow && p.ExpirationDate >= timeNow).FirstOrDefault();
        }
    }
}
