﻿using MongoDB.Driver;
using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Models.Pagination;

namespace TOCO.Implements
{
    public class NotificationService : BaseService
    {
        public NotificationService(ApplicationDbContext context) : base(context)
        {
        }

        public List<Notification> GetNotificationsByEmail(string email)
        {
            return context.Notifications.Where(nt => nt.Email == email).ToList();
        }

        public int Create(Notification notification)
        {

            notification.CreatedAt = DateTime.Now;
            notification.UpdatedAt = DateTime.Now;
            context.Notifications.Add(notification);

            return context.SaveChanges();
        }

        public int Update(Notification notification)
        {
            notification.UpdatedAt = DateTime.Now;
            context.Notifications.Update(notification);
            return context.SaveChanges();
        }

        public List<Notification> GetNotificationsByStatus(NotificationStatus status)
        {
            return context.Notifications.Where(nt => nt.Status == status).ToList();
        }

        public Notification GetById(int id)
        {
            return context.Notifications.Where(nt => nt.Id == id).FirstOrDefault();
        }

        public List<Notification> GetNotification(out int numberOfPages, out int totalRecords, STRequestModel request, string email)
        {
            numberOfPages = 1;
            totalRecords = 0;
            Dictionary<string, bool> sorts = request.GetSort<Dictionary<string, bool>>();
            PaginationModel pagination = request.GetPagination();
            List<StatusFilter> statusFilters = request.GetCheckboxs<StatusFilter>();
            List<DateSearch> listDateSearch = request.GetDates<DateSearch>();
            IQueryable<Notification> source = context.Notifications.Where(n => n.Email == email)
                .OrderByDescending(n => n.CreatedAt);
            totalRecords = source.Count();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            source = source.Skip(pagination.Start).Take(pagination.Number);
            return source.ToList();
        }

        public int CountNotificationsByStatusAndEmail(NotificationStatus status, string email)
        {
            return context.Notifications.Where(n => n.Email == email && n.Status == status).Count();
        }

        public int UpdateStatus(int id, NotificationStatus status)
        {
            Notification notification = context.Notifications.FirstOrDefault(n => n.Id == id);

            notification.Status = status;

            return Update(notification);
        }

        public IEnumerable<int> GetWaitingSeeNotificationIds(string email)
        {
            return context.Notifications.Where(n => n.Email == email
            && n.Status != NotificationStatus.WATCHED
            && n.Status != NotificationStatus.WAITING_READ).Select(n => n.Id);
        }
    }
}
