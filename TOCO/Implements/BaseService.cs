﻿using TOCO.Models;

namespace TOCO.Implements
{
    public class BaseService
    {
        protected readonly ApplicationDbContext context;

        public BaseService(ApplicationDbContext context)
        {
            this.context = context;
        }
    }
}
