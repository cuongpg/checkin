﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class HrbHistoryService : BaseService
    {
        public HrbHistoryService(ApplicationDbContext context) : base(context)
        {
        }

        public HrbHistory GetById(int id)
        {
            return context.HrbHistories.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<HrbHistory> GetAllByUser(int userId)
        {
            return context.HrbHistories
                .Where(s => s.UserId == userId).OrderBy(a => a.ExpirationDate);
        }

        public IEnumerable<HrbHistory> GetByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        {
            return context.HrbHistories
                .Where(s => s.UserId == userId)
                .Where(s => !(s.EffectiveDate > endTime || s.ExpirationDate < startTime));
        }


        public HrbHistory Create(HrbHistory history)
        {
            history.CreatedAt = DateTime.Now;
            history.UpdatedAt = DateTime.Now;
            context.HrbHistories.Add(history);
            context.SaveChanges();
            return history;
        }

        public HrbHistory Update(HrbHistory history)
        {
            history.UpdatedAt = DateTime.Now;
            context.HrbHistories.Update(history);
            context.SaveChanges();
            return history;
        }

        public bool Delete(int id)
        {
            var history = GetById(id);

            if (history == null)
            {
                return false;
            }

            context.HrbHistories.Remove(history);
            context.SaveChanges();

            return true;
        }

       
        public HrbHistory GetHrbInDay(int userId, DateTime day)
        {
            return context.HrbHistories
                 .Where(s => s.UserId == userId)
                 .Where(s => s.EffectiveDate <= day && s.ExpirationDate >= day)
                 .FirstOrDefault();
        }

        public HrbHistory GetHrbInDay(string email, DateTime day)
        {
            return context.HrbHistories
                 .Join(context.Users, hrb => hrb.UserId, u => u.Id, (hrb,u) => new {
                     Hrb = hrb,
                     u.Email
                 })
                 .Where(hrb_u => hrb_u.Email == email)
                 .Where(s => s.Hrb.EffectiveDate <= day && s.Hrb.ExpirationDate >= day)
                 .Select(hrb_u => hrb_u.Hrb)
                 .FirstOrDefault();
        }
    }
}
