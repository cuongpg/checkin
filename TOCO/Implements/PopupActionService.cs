﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Implements
{
    public class PopupActionService : BaseService
    {
        public PopupActionService(ApplicationDbContext context) : base(context)
        {
        }

        public List<PopupAction> GetAll()
        {
            return context.PopupActions.ToList();
        }

    }
}
