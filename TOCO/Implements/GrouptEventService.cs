﻿using TOCO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Implements
{
    public class GroupEventService : BaseService
    {
        public GroupEventService(ApplicationDbContext context) : base(context)
        {
        }

        public List<GroupEvent> GetGroupEvents(string corporationType)
        {
            return context.GroupEvents
                .Where(gr => gr.CorporationType == corporationType)
                .Where(gr => !gr.IsHide)
                .OrderBy(gr => gr.OrderBy).ToList();
        }

        public List<GroupEvent> GetOTQuotasGroupEvents(string corporationType)
        {
            return context.GroupEvents
                .Where(gr => gr.CorporationType == corporationType)
                .Where(gr => gr.HasQuotas)
                .Where(gr => gr.QuotasDefault == null)
                .Where(gr => !gr.IsHide)
                .OrderBy(gr => gr.OrderBy).ToList();
        }

        public GroupEvent GetGroupEventByCode(string code)
        {
            return context.GroupEvents.Where(ge => ge.Code == code).FirstOrDefault();
        }

        public GroupEvent GetGroupEventById(int id)
        {
            return context.GroupEvents.Where(ge => ge.Id == id).FirstOrDefault();
        }
    }
}
