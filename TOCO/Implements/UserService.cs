﻿using Microsoft.EntityFrameworkCore;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Implements
{
    public class UserService : BaseService
    {
        public UserService(ApplicationDbContext context) : base(context)
        {
        }

        // Old
        public List<User> GetAll(bool? isActive, int scriptId, out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;
            IQueryable<User> query = context.Users;

            if (isActive.HasValue && isActive.Value)
            {
                query = query.Where(u => context.HrbHistories.Any(sh => sh.UserId == u.Id && sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now));
            }
            else if (isActive.HasValue && !isActive.Value)
            {
                query = query.Where(u => context.HrbHistories.Where(sh => sh.UserId == u.Id).All(sh => !(sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now)));
            }
            if (scriptId != 0)
            {
                query = query.Where(u => context.ScriptHistorys.Any(sh => sh.UserId == u.Id && sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now && sh.ScriptId == scriptId));
            }
            query = query.GroupBy(u => u.Id, (key, group) => group.First());
            var a = query.Count();
            STHelper.Process<User, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public List<User> GetAll(int? type, int scriptId, out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;
            IQueryable<User> query = context.Users;
            if (type.HasValue && type.Value == (int)HrbStatus.Active)
            {
                query = query.Where(u => context.HrbHistories.Any(sh => sh.UserId == u.Id && sh.HrbStatus == HrbStatus.Active && sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now));
            }
            else if(type.HasValue && type.Value == (int)HrbStatus.Pending)
            {
                query = query.Where(u => context.HrbHistories.Any(sh => sh.UserId == u.Id && sh.HrbStatus == HrbStatus.Pending && sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now));
            }
            else if (type.HasValue)
            {
                query = query.Where(u => context.HrbHistories.Where(sh => sh.UserId == u.Id).All(sh => !(sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now)));
            }
            if (scriptId != 0)
            {
                query = query.Where(u => context.ScriptHistorys.Any(sh => sh.UserId == u.Id && sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now && sh.ScriptId == scriptId));
            }
            query = query.GroupBy(u => u.Id, (key, group) => group.First());
            var a = query.Count();
            STHelper.Process<User, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public List<int> GetAll()
        {
            return context.Users.Select(u => u.Id).ToList();
        }


        public List<User> GetUsersByCorporationType(string corporationType)
        {
            List<User> result = context.Users
                 .Join(context.HrbHistories, u => u.Id, hrb => hrb.UserId, (u, hrb) => new
                 {
                     User = u,
                     hrb.HrbStatus,
                     hrb.EffectiveDate,
                     hrb.ExpirationDate
                 })
                .Where(u_hrb => u_hrb.ExpirationDate >= DateTime.Now.Date && u_hrb.EffectiveDate <= DateTime.Now.Date)
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.Id, sh => sh.UserId, (u, sh) => new
                {
                    User = u,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => u_sh.ExpirationDate >= DateTime.Now.Date && u_sh.EffectiveDate <= DateTime.Now.Date)
                .Join(context.Scripts, u_sh => u_sh.ScriptId, s => s.Id, (u_sh, s) => new
                {
                    u_sh.User,
                    s.CorporationType
                })
                .Where(u_sh_s => u_sh_s.CorporationType == corporationType)
                .Select(u_sh_s => u_sh_s.User)
                .GroupBy(u => u.Id, (key, group) => group.First())
                .ToList();

            return result;
        }

        public List<int> GetListEmployeeHasOvertimeQuotas(string corporationType, DateTime startTime, DateTime endTime)
        {
            IEnumerable<int> result = context.Users
                .Join(context.HrbHistories, u => u.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    User = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => !(u_hrb.ExpirationDate < startTime || u_hrb.EffectiveDate > endTime))
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.Id, sh => sh.UserId, (u, sh) => new
                {
                    User = u,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => !(u_sh.ExpirationDate < startTime || u_sh.EffectiveDate > endTime))
                .Join(context.Scripts, u_sh => u_sh.ScriptId, s => s.Id, (u_sh, s) => new
                {
                    u_sh.User,
                    s.CorporationType,
                    s.HasOverTime
                })
                .Where(u_sh_s => u_sh_s.HasOverTime)
                .Where(u_sh_s => u_sh_s.CorporationType == corporationType)
                .Select(u_sh_s => u_sh_s.User)
                .GroupBy(u => u.Id, (key, group) => group.First())
                .Select(u => u.Id);

            return result.ToList();
        }

        public User GetById(int id)
        {
            return context.Users
                .Include(u => u.UserGroups)
                .ThenInclude(ug => ug.Group)
                .FirstOrDefault(u => u.Id == id);
        }

        public User GetByEmployeeCode(int employeeCode)
        {
            return context.Users
                .Include(u => u.UserGroups)
                .ThenInclude(ug => ug.Group)
                .FirstOrDefault(u => u.EmployeeCode == employeeCode);
        }

        public User GetByEmail(string email)
        {
            return context.Users.FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
        }

        public User GetByUserName(string userName)
        {
            return context.Users.FirstOrDefault(u => u.Email.Substring(0, u.Email.IndexOf("@")).ToLower() == userName.ToLower());
        }

        public List<string> GetRoutesByEmail(string email)
        {
            List<string> routers = new List<string>();
            User user = context.Users
                .Join(context.HrbHistories, u => u.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    User = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => u_hrb.ExpirationDate >= DateTime.Now.Date && u_hrb.EffectiveDate <= DateTime.Now.Date)
                .Where(u_hrb => u_hrb.HrbStatus == HrbStatus.Active)
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.Id, sh => sh.UserId, (u, sh) => new
                {
                    User = u,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => u_sh.ExpirationDate >= DateTime.Now.Date && u_sh.EffectiveDate <= DateTime.Now.Date)
                .Select(u_sh => u_sh.User)
                .Include(u => u.UserGroups)
                .ThenInclude(ug => ug.Group)
                .ThenInclude(g => g.GroupRoutes)
                .ThenInclude(gr => gr.Route)
                .Where(u => u.Email == email).FirstOrDefault();

            if (user != null)
            {
                routers.AddRange(context.Routes.Where(r => r.SkipCheckPermision == Constants.SKIP_CHECK_PERMISION).Select(r => r.Code).ToList());
                foreach (UserGroup userGroup in user.UserGroups)
                {
                    foreach (GroupRoute groupRouter in userGroup.Group.GroupRoutes)
                    {
                        if (!routers.Contains(groupRouter.Route.Code))
                        {
                            routers.Add(groupRouter.Route.Code);
                        }
                    }
                }

            }
            return routers;
        }

        public IEnumerable<(int UserId,
            string HCMWorkScheduleRule,
            int LunchBreak,
            bool HasHoliday,
            bool HasLeaveWeekend,
            bool CheckInOnly,
            bool NoNeedTimeKeeping,
            int? HourOfWorkDay,
            DateTime StartWork,
            DateTime EndWork)> GetNewUserIds(string corporationType, int employeeTypeId, DateTime startTime, DateTime endTime)
        {
            IQueryable<dynamic> result = context.Users
                .Where(u => u.CreatedAt >= startTime && u.CreatedAt <= endTime)
                .Join(context.ScriptHistorys, u => u.Id, sh => sh.UserId, (u, sh) => new
                {
                    u.Id,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => !(u_sh.EffectiveDate > endTime || u_sh.ExpirationDate < startTime))
                .Join(context.Scripts, u_sh => u_sh.ScriptId, s => s.Id, (u_sh, s) => new
                {
                    UserId = u_sh.Id,
                    u_sh.EffectiveDate,
                    u_sh.ExpirationDate,
                    s.EmployeeTypeId,
                    s.HCMWorkScheduleRule,
                    s.LunchBreak,
                    s.HasHoliday,
                    s.HasLeaveWeekend,
                    s.CheckInOnly,
                    s.NoNeedTimeKeeping,
                    s.HourOfWorkDay,
                    s.CorporationType
                })
                .Where(u_sh_s => u_sh_s.CorporationType == corporationType)
                .Where(u_sh_s => u_sh_s.EmployeeTypeId == employeeTypeId);
            foreach (dynamic item in result)
            {
                DateTime startWorkDate = startTime > item.EffectiveDate ? startTime : item.EffectiveDate;
                DateTime endWorkDate = endTime < item.ExpirationDate ? endTime : item.ExpirationDate;
                yield return (item.UserId,
                    item.HCMWorkScheduleRule,
                    item.LunchBreak,
                    item.HasHoliday,
                    item.HasLeaveWeekend,
                    item.CheckInOnly,
                    item.NoNeedTimeKeeping,
                    item.HourOfWorkDay,
                    startWorkDate,
                    endWorkDate);
            }
        }

        public List<User> GetActiveUsersByManagerId(int id, out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            numberOfPages = 1;
            totalRecords = 0;
            IQueryable<User> query = context.Users
                .Join(context.HrbHistories, u => u.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    User = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => u_hrb.ExpirationDate >= DateTime.Now.Date && u_hrb.EffectiveDate <= DateTime.Now.Date)
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.Id, sh => sh.UserId, (u, sh) => new
                {
                    User = u,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => u_sh.ExpirationDate >= DateTime.Now.Date && u_sh.EffectiveDate <= DateTime.Now.Date)
                .Select(u_sh => u_sh.User)
                .Where(u => u.ManagerId == id);
            STHelper.Process<User, UserPagination>(request, ref numberOfPages, ref totalRecords, ref query);
            return query.ToList();
        }

        public User Create(User user)
        {
            user.Status = UserStatus.Active;
            user.CreatedAt = DateTime.Now;
            user.UpdatedAt = DateTime.Now;
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }

        public User Update(User user)
        {
            user.UpdatedAt = DateTime.Now;
            context.Users.Update(user);
            context.SaveChanges();
            return user;
        }

        public void UpdateDepartment(string completeCodeOld, string completeCodeNew)
        {
            List<User> users = context.Users.Where(u => u.Department == completeCodeOld).ToList();
            foreach (User user in users)
            {
                user.Department = completeCodeNew;
                Update(user);
            }
        }

        public void UpdateCorporation(string completeCodeOld, string completeCodeNew)
        {
            List<User> users = context.Users.Where(u => u.Corporation == completeCodeOld).ToList();
            foreach (User user in users)
            {
                user.Corporation = completeCodeNew;
                Update(user);
            }
        }

        public bool Delete(int id)
        {
            User user = GetById(id);
            if (user == null)
            {
                return false;
            }
            context.Users.Remove(user);
            context.SaveChanges();
            return true;
        }

        public List<User> GetAllUsers()
        {
            return context.Users.ToList();
        }

        public IEnumerable<(int UserId,
            HrbStatus HrbStatus,
            string HCMWorkScheduleRule,
            int LunchBreak,
            bool HasHoliday,
            bool HasLeaveWeekend,
            bool CheckInOnly,
            bool NoNeedTimeKeeping,
            int? HourOfWorkDay,
            DateTime StartWork,
            DateTime EndWork)> GetAllUserIds(string corporationType, int employeeTypeId, DateTime startTime, DateTime endTime)
        {
            IQueryable<dynamic> result = context.Users
                .Join(context.HrbHistories, u => u.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    UserId = u.Id,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => !(u_hrb.EffectiveDate > endTime || u_hrb.ExpirationDate < startTime))
                .Select(u_hrb => new
                {
                    u_hrb.UserId,
                    u_hrb.HrbStatus,
                    EffectiveDate = u_hrb.EffectiveDate < startTime ? startTime : u_hrb.EffectiveDate,
                    ExpirationDate = u_hrb.ExpirationDate < endTime ? u_hrb.ExpirationDate : endTime
                })
                .Join(context.ScriptHistorys, u_hrb => u_hrb.UserId, sh => sh.UserId, (u_hrb, sh) => new
                {
                    u_hrb.UserId,
                    u_hrb.HrbStatus,
                    u_hrb.EffectiveDate,
                    u_hrb.ExpirationDate,
                    sh.ScriptId,
                    ScriptEffectiveDate = sh.EffectiveDate,
                    ScriptExpirationDate = sh.ExpirationDate
                })
                .Where(u_sh => !(u_sh.ScriptEffectiveDate > u_sh.ExpirationDate || u_sh.ScriptExpirationDate < u_sh.EffectiveDate))
                .Select(u_sh => new
                {
                    u_sh.UserId,
                    u_sh.HrbStatus,
                    EffectiveDate = u_sh.EffectiveDate < u_sh.ScriptEffectiveDate ? u_sh.ScriptEffectiveDate : u_sh.EffectiveDate,
                    ExpirationDate = u_sh.ExpirationDate < u_sh.ScriptExpirationDate ? u_sh.ExpirationDate : u_sh.ScriptExpirationDate,
                    u_sh.ScriptId,
                })
                .Join(context.Scripts, u_sh => u_sh.ScriptId, s => s.Id, (u_sh, s) => new
                {
                    u_sh.UserId,
                    u_sh.HrbStatus,
                    u_sh.EffectiveDate,
                    u_sh.ExpirationDate,
                    s.EmployeeTypeId,
                    s.HCMWorkScheduleRule,
                    s.LunchBreak,
                    s.HasHoliday,
                    s.HasLeaveWeekend,
                    s.CheckInOnly,
                    s.NoNeedTimeKeeping,
                    s.HourOfWorkDay,
                    s.CorporationType
                })
                .Where(u_sh_s => u_sh_s.CorporationType == corporationType)
                .Where(u_sh_s => u_sh_s.EmployeeTypeId == employeeTypeId);
            foreach (dynamic item in result)
            {
                yield return (item.UserId,
                    item.HrbStatus,
                    item.HCMWorkScheduleRule,
                    item.LunchBreak,
                    item.HasHoliday,
                    item.HasLeaveWeekend,
                    item.CheckInOnly,
                    item.NoNeedTimeKeeping,
                    item.HourOfWorkDay,
                    item.EffectiveDate,
                    item.ExpirationDate);
            }
        }

        public IEnumerable<int> GetUserIds(string corporationType, int employeeTypeId, DateTime startTime, DateTime endTime)
        {
            return context.Users
                .Join(context.HrbHistories, u => u.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    User = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => !(u_hrb.EffectiveDate > endTime || u_hrb.ExpirationDate < startTime))
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.Id, sh => sh.UserId, (u, sh) => new
                {
                    u.Id,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => !(u_sh.EffectiveDate > endTime || u_sh.ExpirationDate < startTime))
                .Join(context.Scripts, u_sh => u_sh.ScriptId, s => s.Id, (u_sh, s) => new
                {
                    UserId = u_sh.Id,
                    s.EmployeeTypeId,
                    s.CorporationType
                })
                .Where(u_sh_s => u_sh_s.CorporationType == corporationType)
                .Where(u_sh_s => u_sh_s.EmployeeTypeId == employeeTypeId)
                .Select(u_sh_s => u_sh_s.UserId)
                .Distinct();
        }

        public bool IsUserCheckinInDay(int userId, DateTime workDay)
        {
            List<User> users = context.Users
                .Where(u => u.Id == userId && u.CreatedAt == workDay)
               .ToList();
            return (users.Count() != 0);
        }

        public List<User> GetUserByGroups(int groups, int page, int perPage, string keyWord, out int totalPages)
        {
            IQueryable<User> query = context.UserGroup
                .Where(ug => ug.GroupId == groups)
                .Join(context.Users, ug => ug.UserId, u => u.Id, (ug, u) => new
                {
                    User = u
                })
                .Join(context.HrbHistories, u => u.User.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    User = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => u_hrb.ExpirationDate >= DateTime.Now.Date && u_hrb.EffectiveDate <= DateTime.Now.Date)
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.User.Id, sh => sh.UserId, (u, sh) => new
                {
                    u.User,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => u_sh.ExpirationDate >= DateTime.Now.Date && u_sh.EffectiveDate <= DateTime.Now.Date)
                .Select(ug_u => ug_u.User);

            query = query.Where(u => u.Email.Contains(keyWord) || u.Id.ToString().Contains(keyWord));

            int skip = page * perPage;
            int totalRecords = query.Count();
            totalPages = totalRecords / perPage;
            if (totalRecords % perPage != 0)
            {
                totalPages++;
            }
            query = query.Skip(skip).Take(perPage);
            var a = query.ToList();
            return query.ToList();
        }

        public List<User> GetAllUserByGroups(int groups)
        {
            IQueryable<User> query = context.UserGroup
                .Where(ug => ug.GroupId == groups)
                .Join(context.Users, ug => ug.UserId, u => u.Id, (ug, u) => new
                {
                    User = u
                })
                .Join(context.HrbHistories, u => u.User.Id, hrb => hrb.UserId, (u, hrb) => new
                {
                    User = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => u_hrb.ExpirationDate >= DateTime.Now.Date && u_hrb.EffectiveDate <= DateTime.Now.Date)
                .Select(u_hrb => u_hrb.User)
                .Join(context.ScriptHistorys, u => u.User.Id, sh => sh.UserId, (u, sh) => new
                {
                    u.User,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => u_sh.ExpirationDate >= DateTime.Now.Date && u_sh.EffectiveDate <= DateTime.Now.Date)
                .Select(ug_u => ug_u.User);
            return query.ToList();
        }

        public List<int> GetAllManagerByCorporationType(string corporationType)
        {
            IQueryable<int> query = context.Users
                .Select(u => u.ManagerId)
                .Distinct()
                .Join(context.HrbHistories, u => u, hrb => hrb.UserId, (u, hrb) => new
                {
                    Id = u,
                    hrb.HrbStatus,
                    hrb.EffectiveDate,
                    hrb.ExpirationDate
                })
                .Where(u_hrb => u_hrb.ExpirationDate >= DateTime.Now.Date && u_hrb.EffectiveDate <= DateTime.Now.Date)
                .Select(u_hrb => u_hrb.Id)
                .Join(context.ScriptHistorys, u => u, sh => sh.UserId, (u, sh) => new
                {
                    Id = u,
                    sh.ScriptId,
                    sh.EffectiveDate,
                    sh.ExpirationDate
                })
                .Where(u_sh => u_sh.ExpirationDate >= DateTime.Now.Date && u_sh.EffectiveDate <= DateTime.Now.Date)
                .Join(context.Scripts, u_sh => u_sh.ScriptId, s => s.Id, (u_sh, s) => new
                {
                    u_sh.Id,
                    s.CorporationType
                })
                .Where(u_sh_s => u_sh_s.CorporationType == corporationType)
                .Select(u_sh => u_sh.Id);
            return query.ToList();
        }

        public List<LeaveCalendarType> GetAllCalendarTypes()
        {
            return context.LeaveCalendarType.ToList();
        }

        public List<CorporationType> GetAllCorporationTypes()
        {
            return context.CorporationTypes.ToList();
        }
    }
}
