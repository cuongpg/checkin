﻿using System;
using System.Collections.Generic;

namespace TOCO
{
    public class Constants
    {
        public const string POST_METHOD = "Post";
        public const string APPLICATION_JSON_CONTENT_TYPE = "application/json";
        public const string IMAGE_URL = "image_url";
        public const string EMPLOYEE_CODE = "employee_code";
        public const string AUTHOR_FORMAT = "Authorization: key={0}";

        public const string NA = "N/A";

        public const int TOTAL_NUMBER_DATE = 10;
        public const string FORMAT_DATE_DDMMYYYY = "dd/MM/yyyy";
        public const string FORMAT_DATE_MMDDYYYY = "MM/dd/yyyy";
        public const string FORMAT_DATE_YYYYMMDD = "yyyy-MM-dd";
        public const string FORMAT_DATE_DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";
        public const string FORMAT_DATE_DDDDMMDDYYYYHHMMSS = "dddd, MM/dd/yyyy HH:mm:ss";
        public const string FORMAT_TIME_HHMMSS = "HH:mm:ss";
        public const string FORMAT_TIME_HHMM = "HH:mm";
        public const string FORMAT_DATE_DDMM = "dd/MM";
        public const string FORMAT_DATE_MMDD = "MM/dd";
        public const string FORMAT_DATE_MMYYYY = "MM/yyyy";

        public const string FORMAT_DATE_HCM_DDMMYYYY = "yyyy-MM-dd";
        public const string FORMAT_DATE_HCM_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
        public const string FORMAT_DATE_HKT_YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";
        public const string TRUE = "true";
        public const string FALSE = "false";

        public const string SUBMITED = "submitted";
        public const string REVIEWING = "reviewing";

        public const int SKIP_CHECK_PERMISION = 1;

        public const string APPROVAL = "APPROVAL";
        public const string REJECT = "REJECT";
        public const string SCRIPT = "SCRIPT";
        public const string FROM_TO_DATE = "FROM_TO_DATE";

        //Allocation status
        public const string ALLOCATION_TITLE = "ALLOCATION_TITLE";
        public const string ALLOCATION_MESSAGE_WARNING = "ALLOCATION_MESSAGE_WARNING";
        public const string ALLOCATION_MESSAGE_ERROR = "ALLOCATION_MESSAGE_ERROR";

        public const string UNIT_TYPE_ORG = "Dự án trọng điểm - Org Unit";
        public const string UNIT_TYPE_SMALL_PROJECT = "Dự án nhỏ";

        public struct TimekeepingChannel
        {
            public const string WEB = "WEB";
            public const string MOBLIE = "MOBLIE";
            public const string FINGERPRINT = "FINGERPRINT";
        }

        public struct Device
        {
            public const string IOS = "IOS";
            public const string ANDROID = "ANDROID";
            public const string FINGERPRINT_MACHINE = "FINGERPRINT_MACHINE";
        }

        public struct UserAgent
        {
            public const string IOS = "ios";
            public const string ANDROID = "android";
            public const string CHECKIN_SERVICE = "Checkin Service";
        }

        public struct HandleForgetCheckout
        {
            public const string Auto = "Auto";
            public const string Destroy = "Destroy";
            public const string DoNothing = "DoNothing";
        }

        public struct User
        {
            public struct Status
            {
                public const int Pending = 0;
                public const int Active = 1;
                public const int Blocked = 2;
            }
            public struct Groups
            {
                public const int Manager = 1;
                public const int Admin = 2;
                public const int Employee = 3;
                public const int ViewTimeSheet = 4;
                public const int Allocation = 5;
            }
        }
        public struct Paging
        {
            public const int PageSize = 10;
            public const int MaxRecord = 1000;
        }

        public struct MongoPropertyName
        {
            public const string ListContract = "ListContract";
            public const string ContractDetailHeader = "ContractDetailHeader";
            public const string ContractDetailBody = "ContractDetailBody";
            public const string ListProcess = "ListProcess";
        }

        public struct Salary
        {
            public const string INCOME_INFO_TITLE = "INCOME_INFO_TITLE";
            public const string INCOME_TOTAL_NOT_TAXABLE_TITLE = "INCOME_TOTAL_NOT_TAXABLE_TITLE";
            public const string INCOME_TOTAL_TAXABLE_DEDUCTION_TITLE = "INCOME_TOTAL_TAXABLE_DEDUCTION_TITLE";
            public const string INCOME_PERSONAL_TAX_TITLE = "INCOME_PERSONAL_TAX_TITLE";
            public const string INCOME_TAX_DEDUCTIBLE_TITLE = "INCOME_TAX_DEDUCTIBLE_TITLE";
            public const string INCOME_TAX_DIFFERENCE_TITLE = "INCOME_TAX_DIFFERENCE_TITLE";
            public const string INCOME_TOTAL_DEDUCTION_TITLE = "INCOME_TOTAL_DEDUCTION_TITLE";
            public const string INCOME_TOTAL_RECEIVED_TITLE = "INCOME_TOTAL_RECEIVED_TITLE";
            public const string INCOME_PAID_TITLE = "INCOME_PAID_TITLE";
            public const string INCOME_DETAIL_TITLE = "INCOME_DETAIL_TITLE";
            public const string INCOME_TOTAL_REALITY_WORK_SALARY_TITLE = "INCOME_TOTAL_REALITY_WORK_SALARY_TITLE";
            public const string INCOME_TOTAL_OT_SALARY_TITLE = "INCOME_TOTAL_OT_SALARY_TITLE";
            public const string INCOME_OT_ALLOWANCE_TITLE = "INCOME_OT_ALLOWANCE_TITLE";
            public const string INCOME_LUNCH_ALLOWANCE_TITLE = "INCOME_LUNCH_ALLOWANCE_TITLE";
            public const string INCOME_PARKING_ALLOWANCE_TITLE = "INCOME_PARKING_ALLOWANCE_TITLE";
            public const string INCOME_MOBILE_ALLOWANCE_TITLE = "INCOME_MOBILE_ALLOWANCE_TITLE";
            public const string INCOME_STOCK_ALLOWANCE_TITLE = "INCOME_STOCK_ALLOWANCE_TITLE";
            public const string INCOME_HOLIDAY_BONUS_TITLE = "INCOME_HOLIDAY_BONUS_TITLE";
            public const string INCOME_WORKING_FEE_TITLE = "INCOME_WORKING_FEE_TITLE";
            public const string INCOME_OTHER_ALLOWANCE_TITLE = "INCOME_OTHER_ALLOWANCE_TITLE";
            public const string INCOME_SHIFTEAT_ALLOWANCE_TITLE = "INCOME_SHIFTEAT_ALLOWANCE_TITLE";
            public const string INCOME_LEAVE_RETRIEVAL_TITLE = "INCOME_LEAVE_RETRIEVAL_TITLE";
            public const string INCOME_SALARY_RETRIEVAL_TITLE = "INCOME_SALARY_RETRIEVAL_TITLE";
            public const string INCOME_HOLIDAY_RETRIEVAL_TITLE = "INCOME_HOLIDAY_RETRIEVAL_TITLE";
            public const string INCOME_COM_TITLE = "INCOME_COM_TITLE";
            public const string INCOME_BONUS_COM_TITLE = "INCOME_BONUS_COM_TITLE";
            public const string INCOME_OTHER_BONUS_TITLE = "INCOME_OTHER_BONUS_TITLE";
            public const string INCOME_SALARY_REIMBURSEMENT_TITLE = "INCOME_SALARY_REIMBURSEMENT_TITLE";
            public const string INCOME_TAX_REIMBURSEMENT_TITLE = "INCOME_TAX_REIMBURSEMENT_TITLE";
            public const string INCOME_PARKING_REIMBURSEMENT_TITLE = "INCOME_PARKING_REIMBURSEMENT_TITLE";
            public const string INCOME_REIMBURSEMENT_TITLE = "INCOME_REIMBURSEMENT_TITLE";
            public const string INCOME_EXCEED_TEMPORARILY_TITLE = "INCOME_EXCEED_TEMPORARILY_TITLE";
            public const string INCOME_TOTAL_INSURRANCE_TITLE = "INCOME_TOTAL_INSURRANCE_TITLE";
            public const string INCOME_DETAIL_2_TITLE = "INCOME_DETAIL_2_TITLE";
            public const string INCOME_BASE_SALARY_TITLE = "INCOME_BASE_SALARY_TITLE";
            public const string INCOME_PRODUCTIVITY_SALARY_TITLE = "INCOME_PRODUCTIVITY_SALARY_TITLE";
            public const string INCOME_WORK_DATE_QUOTAS_TITLE = "INCOME_WORK_DATE_QUOTAS_TITLE";
            public const string INCOME_WORK_DATE_NUMBER_TITLE = "INCOME_WORK_DATE_NUMBER_TITLE";
            public const string INCOME_OT_150_TITLE = "INCOME_OT_150_TITLE";
            public const string INCOME_OT_200_TITLE = "INCOME_OT_200_TITLE";
            public const string INCOME_OT_300_TITLE = "INCOME_OT_300_TITLE";
            public const string INCOME_PERSONAL_DEDUCTION_TITLE = "INCOME_PERSONAL_DEDUCTION_TITLE";
            public const string INCOME_FAMILY_DEDUCTION_TITLE = "INCOME_FAMILY_DEDUCTION_TITLE";

        }


        public const string ATTENDENCE_MANAGEMENT_CHECKIN = "ATTENDENCE_MANAGEMENT_CHECKIN";
        public const string ATTENDENCE_MANAGEMENT_CHECKOUT = "ATTENDENCE_MANAGEMENT_CHECKOUT";

        public static readonly Dictionary<DayOfWeek, string> DAYOFWEEK = new Dictionary<DayOfWeek, string>
            {
                { DayOfWeek.Monday, "Thứ hai" },
                { DayOfWeek.Tuesday, "Thứ ba" },
                { DayOfWeek.Wednesday, "Thứ tư" },
                { DayOfWeek.Thursday, "Thứ năm" },
                { DayOfWeek.Friday, "Thứ sáu" },
                { DayOfWeek.Saturday, "Thứ bẩy" },
                { DayOfWeek.Sunday, "Chủ nhật" }

            };
    }
}
