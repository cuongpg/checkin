﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace TOCO.Filters
{
    public class LogFilter : IActionFilter
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly ILogger _logger;

        private readonly string MESSAGE_INFO_EXECUTING = "EXECUTING FROM_EMAIL {FROM_EMAIL} - FROM_IP {FROM_IP} - FROM_USER_AGENT {FROM_USER_AGENT} " +
            "- REQUEST_METHOD {REQUEST_METHOD} - REQUEST_PATH {REQUEST_PATH} - REQUEST_QUERY {REQUEST_QUERY} - REQUEST_PARAM {REQUEST_PARAM}";
        private readonly string MESSAGE_INFO_EXECUTED = "EXECUTED FROM_EMAIL {FROM_EMAIL} - FROM_IP {FROM_IP} - FROM_USER_AGENT {FROM_USER_AGENT} " +
            "- REQUEST_METHOD {REQUEST_METHOD} - REQUEST_PATH {REQUEST_PATH} - RESPONSE_STATUS_CODE {RESPONSE_STATUS_CODE} - RESPONSE_DATA {RESPONSE_DATA}";

        public LogFilter(IHttpContextAccessor accessor, ILoggerFactory logger)
        {
            _accessor = accessor;
            _logger = logger.CreateLogger<LogFilter>();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            _logger.LogInformation(
                          8282,
                          MESSAGE_INFO_EXECUTING,
                          context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value ?? "NOT_AUTHENTICATED",
                          _accessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                          _accessor.HttpContext.Request.Headers["User-Agent"],
                          context.HttpContext.Request.Method,
                          context.HttpContext.Request.Scheme + "://" + context.HttpContext.Request.Host + context.HttpContext.Request.Path,
                          System.Net.WebUtility.UrlDecode(context.HttpContext.Request.QueryString.ToString()),
                          context.ActionArguments.ToDictionary(p => p.Key, p => JsonConvert.SerializeObject(p.Value))
                      );
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
           IActionResult result =  context.Result;
            string data = string.Empty;
            if (result is ObjectResult obj)
            {
                JsonSerializerSettings option = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };
                data = JsonConvert.SerializeObject(obj.Value, option);
            }
            
            _logger.LogInformation(
                8282,
                MESSAGE_INFO_EXECUTED,
                context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value ?? "NOT_AUTHENTICATED",
                _accessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                _accessor.HttpContext.Request.Headers["User-Agent"],
                context.HttpContext.Request.Method,
                context.HttpContext.Request.Scheme + "://" + context.HttpContext.Request.Host + context.HttpContext.Request.Path,
                (context.Result as ObjectResult)?.StatusCode,
                data
            );
        }
    }
}
