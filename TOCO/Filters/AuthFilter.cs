﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TOCO.Models;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace TOCO.Filters
{
    public class AuthFilter : IActionFilter
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _accessor;
        private readonly IDistributedCache _cache;

        public AuthFilter(IHttpContextAccessor accessor, IDistributedCache cache, IConfiguration configuration)
        {
            _configuration = configuration;
            _accessor = accessor;
            _cache = cache;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
