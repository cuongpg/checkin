﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TOCO.Implements;
using TOCO.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System;
using TOCO.Common;

namespace TOCO.Filters
{
    public class PermissionFilter : IActionFilter
    {
        private const string Format = @"{{""message"":""{0}"",""data"":null}}";
        private readonly UserService userService;
        private readonly HrbHistoryService hrbHistoryService;

        private readonly List<string> _publicRoutes = new List<string>()
        {

        };

        public PermissionFilter(ApplicationDbContext context)
        {
            userService = new UserService(context);
            hrbHistoryService = new HrbHistoryService(context);
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                var email = context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;

                // Check trạng thái làm việc
                HrbHistory hrbHistory = hrbHistoryService.GetHrbInDay(email, DateTime.Now.Date);

                if (hrbHistory is null)
                {
                    context.Result = new ContentResult()
                    {
                        Content = string.Format(Format, ResultCode.DEACTIVE_USER_MESSAGE),
                        StatusCode = 403,
                        ContentType = "application/json; charset=utf-8"
                    };
                }
                else if (hrbHistory.HrbStatus == HrbStatus.Pending)
                {
                    context.Result = new ContentResult()
                    {
                        Content = string.Format(Format, ResultCode.PENDING_USER_MESSAGE),
                        StatusCode = 403,
                        ContentType = "application/json; charset=utf-8"
                    };
                }
                else
                {
                    var currentRouteName = $"{context.RouteData.Values["controller"]}.{context.RouteData.Values["action"]}";
                    if ((_publicRoutes.IndexOf(currentRouteName) == -1) && (userService.GetRoutesByEmail(email).IndexOf(currentRouteName) == -1))
                    {
                        context.Result = new ContentResult()
                        {
                            Content = string.Format(Format, ResultCode.YOU_DONT_HAVE_PERMISSION),
                            StatusCode = 403,
                            ContentType = "application/json; charset=utf-8"
                        };
                    }
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // do something after the action executes
        }
    }
}
