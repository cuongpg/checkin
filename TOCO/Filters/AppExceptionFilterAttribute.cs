﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Security.Claims;

namespace TOCO.Filters
{
    public class AppExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpContextAccessor _accessor;
        private readonly ILogger _logger;

        private readonly string MESSAGE_INFO_EXCEPTION = "EXCEPTION FROM_EMAIL {FROM_EMAIL} - FROM_IP {FROM_IP} - FROM_USER_AGENT {FROM_USER_AGENT} " +
            "- REQUEST_METHOD {REQUEST_METHOD} - REQUEST_PATH {REQUEST_PATH} - REQUEST_QUERY {REQUEST_QUERY} - MESSAGE_EXCEPTION {MESSAGE_EXCEPTION}";

        public AppExceptionFilterAttribute(IHostingEnvironment hostingEnvironment, IHttpContextAccessor accessor, ILoggerFactory logger)
        {
            _hostingEnvironment = hostingEnvironment;
            _accessor = accessor;
            _logger = logger.CreateLogger<AppExceptionFilterAttribute>();
        }

        public override void OnException(ExceptionContext context)
        {
            string message = @"{""message"":""";

            if (context.Exception.InnerException != null)
            {
                message += context.Exception.InnerException.Message;
            }
            else
            {
                message += context.Exception.Message;
            }

            var messageException = message;
            //if (_hostingEnvironment.IsDevelopment())
            //{
            //    message += @""",""trace"":""" + context.Exception.StackTrace.Replace("\\", "/");
            //}
            messageException += @""",""trace"":""" + context.Exception.StackTrace.Replace("\\", "/");
            messageException += @""",""data"":null}";

            message += @""",""data"":null}";

            _logger.LogError(
                8282,
                MESSAGE_INFO_EXCEPTION,
                context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value ?? "NOT_AUTHENTICATED",
                _accessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                _accessor.HttpContext.Request.Headers["User-Agent"],
                context.HttpContext.Request.Method,
                context.HttpContext.Request.Scheme + "://" + context.HttpContext.Request.Host + context.HttpContext.Request.Path,
                System.Net.WebUtility.UrlDecode(context.HttpContext.Request.QueryString.ToString()),
                messageException
            );

            context.Result = new ContentResult()
            {
                Content = message,
                StatusCode = 500,
                ContentType = "application/json; charset=utf-8"
            };
        }
    }
}
