﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Prometheus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Middleware
{
    public class RequestMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public RequestMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this._next = next;
            // Khởi tạo logger cho chắc cú, không cần thì bỏ qua dòng này cũng được
            this._logger = loggerFactory.CreateLogger<RequestMiddleware>();
        }


        public async Task Invoke(HttpContext httpContext)
        {
            // Lấy dữ liệu truy cấn: link, method
            var path = httpContext.Request.Path.Value;
            var method = httpContext.Request.Method;

            // Khởi tạo các metric cần thiết, có nhu cầu hoặc mong muốn ghi nhận
            var processGauge = Metrics.CreateGauge("current_processing", "Number of request current processing");
            var processPath = Metrics.CreateGauge("current_processing_path", "Number of current processing by path",
                new GaugeConfiguration
                {
                    LabelNames = new[] { "path", "method" }
                });
            var counter = Metrics.CreateCounter("request_total", "HTTP Requests Total",
                new CounterConfiguration
                {
                    LabelNames = new[] { "path", "method", "status" }
                });

            var histogram =
                Metrics
                    .CreateHistogram(
                        "api_response_time_seconds",
                        "API Response Time in seconds",
                        new HistogramConfiguration
                        {
                            Buckets = new[]
                                {0.1, 0.15, 0.2, 0.5, 1, 3, 5, 7, 10, 15, 30, 45, 60, 180, 300, 600, 1800},
                            LabelNames = new[] { "path", "method", "status" }
                        });

            int statusCode;

            // Tạo timer để count thời gian xử lý, cái này để kết hợp với histogram buckets.
            var sw = Stopwatch.StartNew();


            // Nếu gặp những link không chứa substring này thì mới ghi nhận
            if (!path.Contains("metrics") && !path.Contains("log") && !path.Contains("favicon") &&
                !path.Contains("/job") &&
                !path.Contains("swagger"))
            {
                processGauge.Inc();
                processPath.Labels(path, method).Inc();
            }

            try
            {
                // Tiến hành process request như bình thường
                await _next.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                // Có lỗi thì log lại
                _logger.LogDebug($"Request failed: {ex.Message}");

                // Dừng timer
                sw.Stop();
                // Nếu gặp những link không chứa substring này thì mới ghi nhận
                if (!path.Contains("metrics") && !path.Contains("log") && !path.Contains("favicon") &&
                    !path.Contains("/job") &&
                    !path.Contains("swagger"))
                {
                    statusCode = 500;

                    // Cập nhật metrics theo từng loại
                    counter.Labels(path, method, statusCode.ToString()).Inc();
                    histogram.Labels(path, method, statusCode.ToString()).Observe(sw.Elapsed.TotalSeconds);
                    processGauge.Dec();
                    processPath.Labels(path, method).Dec();
                }
                throw;
            }

            sw.Stop();
            // Nếu gặp những link không chứa substring này thì mới ghi nhận
            if (!path.Contains("metrics") && !path.Contains("log") && !path.Contains("favicon") &&
                !path.Contains("/job") &&
                !path.Contains("swagger"))
            {
                // Cập nhật metrics theo từng loại
                statusCode = httpContext.Response.StatusCode;
                counter.Labels(path, method, statusCode.ToString()).Inc();
                histogram.Labels(path, method, statusCode.ToString()).Observe(sw.Elapsed.TotalSeconds);
                processGauge.Dec();
                processPath.Labels(path, method).Dec();
            }
        }
    }

    public static class RequestMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestMiddleware(this IApplicationBuilder builder)
        {
            // Tạo Middleware Extension để sử dụng ở Startup
            return builder.UseMiddleware<RequestMiddleware>();
        }
    }
}
