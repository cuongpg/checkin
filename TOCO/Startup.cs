﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Prometheus;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Filters;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Middleware;
using TOCO.Models;
using TOCO.Service;

namespace TOCO
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            CultureInfo customCulture = new System.Globalization.CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);

            Thread.CurrentThread.CurrentCulture = customCulture;
            Thread.CurrentThread.CurrentUICulture = customCulture;

            // Create Serilog Elasticsearch logger
            var isController = Serilog.Filters.Matching.FromSource(this.GetType().Namespace);
            Log.Logger = new LoggerConfiguration()
               .Enrich.FromLogContext()
               .MinimumLevel.Debug()
               .Filter.ByIncludingOnly(isController)
               .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(Configuration["Logging:HostUri"]))
               {
                   MinimumLogEventLevel = LogEventLevel.Information,
                   IndexFormat = Configuration["Logging:Type"] + "-{0:yyyy.MM.dd}",
                   InlineFields = true,
                   AutoRegisterTemplate = true,
                   ModifyConnectionSettings = (c) => c.GlobalHeaders(new NameValueCollection { { "Authorization", Configuration["Logging:Authentication"] } })
               })
               .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = Configuration["Redis:Configuration"];
                options.InstanceName = Configuration["Redis:InstanceName"];
            });
            services.AddDbContext<ApplicationDbContext>(opt => opt.UseSqlServer(Configuration["ConnectionString"]));
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));

            services.AddCors();

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(LogFilter));
                options.Filters.Add(typeof(AuthFilter));
                options.Filters.Add(typeof(PermissionFilter));
                options.Filters.Add(typeof(AppExceptionFilterAttribute));
                options.MaxModelValidationErrors = 50;
                options.ModelBindingMessageProvider.SetValueMustNotBeNullAccessor(
                    (_) => "The field is required.");
            }).AddJsonOptions(
                options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            );

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "TOCO API", Version = "v1" });
                c.OperationFilter<AddAuthorizationHeader>();
            });

            services.AddSingleton(provider => Configuration);
            services.AddScoped<IMailProcess, MailProcess>();
            services.AddScoped<IFireBaseMesageService, FireBaseMessageService>();

            ////Server chạy bắn notify
            //services.AddSingleton<IHostedService, SendNotificationService>();
            //Server chạy gửi mail
            //services.AddSingleton<IHostedService, SendEmailService>();
            ////Service chạy tự động checkout
            //services.AddSingleton<IHostedService, AutoCheckoutService>();

            ////Service chạy đồng bộ dữ liệu listmaster
            ////services.AddSingleton<IHostedService, ListmasterServicecs>();
            ////Service chạy bắn notify nhắc chấm công
            //services.AddSingleton<IHostedService, PromptCheckinService>();
            //services.AddSingleton<IHostedService, PromptForgetCheckinService>();
            //services.AddSingleton<IHostedService, PromptCheckoutService>();
            ////Service đổi trạng thái quá hạn đơn từ
            //services.AddSingleton<IHostedService, SetExpiredRecordServiceVi>();
            //services.AddSingleton<IHostedService, SetExpiredRecordServiceTh>();

            ////Service thêm quỹ phép
            ////services.AddSingleton<IHostedService, GenerateQuotasServiceVi>();
            ////services.AddSingleton<IHostedService, GenerateQuotasServiceTh>();

            //// Service chạy nhắc duyệt đơn, check bảng công
            ////services.AddSingleton<IHostedService, PromptApproveCutoffTempVi>();
            ////services.AddSingleton<IHostedService, PromptApprovalCutoffVi>();

            //services.AddSingleton<IHostedService, PromptApproveCutoffTempTh>();
            //services.AddSingleton<IHostedService, PromptManagerApprovalCutoffTh>();

            //services.AddSingleton<IHostedService, ExportHCMDayService>();
            ////services.AddSingleton<IHostedService, ExportHCMMonthService>();
            ////services.AddSingleton<IHostedService, ExportHCMCutoffService>();

            ////Service chạy đồng bộ định mức OT cho nhân sự VN
            ////services.AddSingleton<IHostedService, SyncOvertimeQuotasService>();

            ////services.AddSingleton<IHostedService, SyncHKTDataService>();
            //services.AddSingleton<IHostedService, SendHrOperService>();
            //services.AddSingleton<IHostedService, SyncGlobalVariable>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            // Enable metric server by prometheus
            app.UseMetricServer();
            // Enable metric monitoring middleware
            app.UseRequestMiddleware();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "TOCO API V1");
            });

            app.UseMvc();
        }
    }
}
