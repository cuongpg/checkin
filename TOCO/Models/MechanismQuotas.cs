﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class MechanismQuotas
    {
        public int MechanismId { get; set; }
        public int LevelId { get; set; }
        public double Quotas { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
