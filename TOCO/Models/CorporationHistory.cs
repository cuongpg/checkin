﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class CorporationHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string HcmPersionalId { get; set; }
        public string Corporation { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public bool CompareTo(CorporationHistory obj)
        {
            if (obj == null)
            {
                return false;
            }

            return this.UserId == obj.UserId
                && this.Corporation == obj.Corporation
                && this.HcmPersionalId == obj.HcmPersionalId
                && this.EffectiveDate == obj.EffectiveDate
                && this.ExpirationDate == obj.ExpirationDate;
        }
    }
}
