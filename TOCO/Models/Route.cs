﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class Route
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int SkipCheckPermision { get; set; }
        [JsonIgnore]
        public List<GroupRoute> GroupRoutes { get; set; }
    }
}
