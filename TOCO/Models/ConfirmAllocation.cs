﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public enum ConfirmAllocationStatus : int
    {
        Destroy = -1,
        Waiting = 0,
        ConfirmByEmployee = 1,
        Reporting = 2,
        ApprovalReport = 3,
        RejectReport = 4,
        Expired = 5
    }

    public partial class ConfirmAllocation
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public ConfirmAllocationStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string HrOperResult { get; set; }
        public string RejectReason { get; set; }
    }
}
