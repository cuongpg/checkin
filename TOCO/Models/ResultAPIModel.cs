﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class ResultAPIModel : ResultModel
    {
        public int Total { get; set; }
    }
}
