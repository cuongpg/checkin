﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{

    public enum StatusChangeManagerInfo : int
    {
        Pending = 0,
        Approved = 1,
        Rejected = 2,
        Destroyed = 3
    }
    [JsonObject(IsReference = true)]
    public class ChangeManagerInfo
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int OldManagerId { get; set; }
        public StatusChangeManagerInfo OldManagerStatus { get; set; }
        public int NewManagerId { get; set; }
        public StatusChangeManagerInfo NewManagerStatus { get; set; }
        public StatusChangeManagerInfo Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string RejectReason { get; set; }
        public int? IsManual { get; set; }
        public ChangeManagerInfo()
        {
            OldManagerStatus = StatusChangeManagerInfo.Pending;
            NewManagerStatus = StatusChangeManagerInfo.Pending;
            Status = StatusChangeManagerInfo.Pending;
        }
        
    }
}