﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class OverTime
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime WorkDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int ScriptId { get; set; }
        [JsonIgnore]
        public User User { get; set; }
        [JsonIgnore]
        public List<OverTimeDetail> OverTimeDetails { get; set; }

    }
}