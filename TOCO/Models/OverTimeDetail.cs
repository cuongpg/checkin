﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models
{
    public enum StatusOverTime : int
    {
        NotYet = -1,
        Pending = 0,
        Approved = 1,
        Rejected = 2,
        Expired = 4,
    }
    [JsonObject(IsReference = true)]
    public class OverTimeDetail
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int OverTimeId { get; set; }
        public StatusOverTime Status { get; set; }
        public DateTime? ApprovalTime { get; set; }
        public string Note { get; set; }
        public bool IsManual { get; set; }
        public int EventId { get; set; }

        public bool IsDestroy { get; set; }
        public StatusOverTime StatusDestroy { get; set; }
        public DateTime? ApprovalDestroyTime { get; set; }
        public DateTime? DestroyTime { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public string ApproverEmail { get; set; }
        public string RejectReason { get; set; }

        [JsonIgnore]
        public OverTime OverTime { get; set; }
        [JsonIgnore]
        public Event Event { get; set; }

        public OverTimeDetail()
        {
            Status = StatusOverTime.Pending;
            StatusDestroy = StatusOverTime.NotYet;
        }
    }
}