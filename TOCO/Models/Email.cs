﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public enum EmailStatus : int
    {
        UNSENT = 0,
        SENT = 1
    }
    public partial class Email
    {
        public int Id { get; set; }
        public string EmailTo { get; set; }
        public string CCTo { get; set; }
        public string FileAttach { get; set; }
        //public string Subject { get; set; }
        //public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public EmailStatus Status { get; set; }
        public string EmailType { get; set; }
        public string ParameterValues { get; set; }
    }
}
