﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class Group
    {
        public const int AdminGroupId = 2;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        [JsonIgnore]
        public List<UserGroup> UserGroups { get; set; }
        public List<GroupRoute> GroupRoutes { get; set; }
    }
}
