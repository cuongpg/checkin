﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public enum ScriptId : int
    {
        B = 1,
        D = 2,
        T = 9
    }


    [JsonObject(IsReference = true)]
    public class Script
    {
        public static readonly Dictionary<string, (string Start, string End)> TimeWork = new Dictionary<string, (string Start, string End)>
        {
            { "HC01", ("08:00:00", "17:30:00") },
            { "HC02", ("08:30:00", "18:00:00") },
            { "HC03", ("09:00:00", "18:30:00") }
        };

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SubScript { get; set; }
        public string CheckInTimeDefault { get; set; }
        public string CheckOutTimeDefault { get; set; }
        public int? EnoughHour { get; set; }
        public bool HasOverTime { get; set; }
        public bool HasShiftsEat { get; set; }
        public bool CheckInOnly { get; set; }
        public string OverTimeLine { get; set; }
        public string ShiftsEatTimeLine { get; set; }
        public int NumberCheckInMax { get; set; }
        public bool HasLeaveWeekend { get; set; }
        public bool HasHoliday { get; set; }
        public int LunchBreak { get; set; }
        public bool HasPaidLeave { get; set; }
        public int? HourOfWorkDay { get; set; }
        public int CutoffTimeKeepingDay { get; set; }
        public string AutoCheckoutTime { get; set; }
        public string HandleForgetCheckout { get; set; }
        public int? MinOverTimeMinutes { get; set; }
        public bool NoNeedTimeKeeping { get; set; }
        public int EmployeeTypeId { get; set; }
        public string HCMWorkScheduleRule { get; set; }
        public int? HourOfHalfWorkDay { get; set; }
        public string CorporationType { get; set; }

        [JsonIgnore]
        public EmployeeType EmployeeType { get; set; }
        [JsonIgnore]
        public List<TimeKeeping> TimeKeepings { get; set; }
        [JsonIgnore]
        public DateTime CreatedAt { get; set; }
        [JsonIgnore]
        public DateTime? UpdatedAt { get; set; }
    }
}
