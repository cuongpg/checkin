﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class Event
    {
        public const string NV_OTWK = "NV_OTWK";
        public const string NV_OTDL = "NV_OTDL";

        public const string NT_OT150 = "NT_OT150";
        public const string NT_OT200_300 = "NT_OT200_300";

        public int Id { get; set; }
        public string EventCode { get; set; }
        public int? Percentage { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int EmployeeTypeId { get; set; }
        public int GroupEventId { get; set; }
        public double? PayPerHour { get; set; }
        public double? PayPerBlock { get; set; }
        public string HCMEventId { get; set; }
        public bool ShowCode { get; set; }
        [JsonIgnore]
        public GroupEvent GroupEvent { get; set; }
        [JsonIgnore]
        public EmployeeType EmployeeType { get; set; }
        [JsonIgnore]
        public List<LeaveDate> LeaveDates { get; set; }
        [JsonIgnore]
        public List<OverTimeDetail> OverTimeDetails { get; set; }
    }
}