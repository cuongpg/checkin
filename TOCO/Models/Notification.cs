﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public enum NotificationStatus : int
    {
        ERROR = -1,
        UNSENT = 0,
        WAITING_SEE = 1,
        WAITING_READ = 2,
        WATCHED = 3,
    }

    public class Notification
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string DocumentId { get; set; }
        public string DocumentType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Avatar { get; set; }
        public string ParameterValues { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public NotificationStatus Status { get; set; }
    }
}
