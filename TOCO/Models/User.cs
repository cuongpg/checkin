﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TOCO.Models
{
    public enum UserStatus : int
    {
        Pending = 0,
        Active = 1,
        Deactive = 2,
    }

    [JsonObject(IsReference = true)]
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string ImageUrl { get; set; }
        public UserStatus Status { get; set; }
        public int ManagerId { get; set; }
        public int EmployeeCode { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Department { get; set; }
        public string Corporation { get; set; }
        public string Nationality { get; set; }
        public int? HeadOfOrgUnitId { get; set; }
        public bool IsPilot { get; set; }
        [JsonIgnore]
        public List<TimeKeeping> TimeKeepings { get; set; }
        [JsonIgnore]
        public List<OverTime> OverTimes { get; set; }
        [JsonIgnore]
        public List<ApprovalRecord> ApprovalRecords { get; set; }
        [JsonIgnore]
        public List<ShiftsEat> ShiftsEats { get; set; }
        [JsonIgnore]
        public List<UserGroup> UserGroups { get; set; }
    }
}
