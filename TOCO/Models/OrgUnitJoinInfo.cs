﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public enum OrgUnitJoinInfoStatus
    {
        Deactive = 0,
        Active = 1
    }

    public class OrgUnitJoinInfo
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string OrgUnitId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Percentage { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public OrgUnitJoinInfoStatus Status { get; set; }
        public string HcmRole { get; set; }
        public string ShortCode { get; set; }
    }
}