﻿namespace TOCO.Models
{
    public class Auth
    {
        public string IP { get; set; }
        public string UserAgent { get; set; }
    }
}
