﻿using System;

namespace TOCO.Models.Pagination
{
    public class EmailPagination
    {
        //public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Parameters { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string EmailType { get; set; }
    }
}
