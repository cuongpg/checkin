﻿namespace TOCO.Models.Pagination
{
    public class Sort
    {
        public string Predicate { get; set; }
        public bool Reverse { get; set; }
    }
}
