﻿using Newtonsoft.Json;

namespace TOCO.Models.Pagination
{
    public class DateSearch
    {
        public object Type { get; set; }
        public object Data { get; set; }

        public T GetData<T>()
        {
            if (Data == null)
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(Data.ToString());
        }
    }
}
