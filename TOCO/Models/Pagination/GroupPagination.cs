﻿namespace TOCO.Models.Pagination
{
    public class GroupPagination
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
    }
}
