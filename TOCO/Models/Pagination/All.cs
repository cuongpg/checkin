﻿namespace TOCO.Models.Pagination
{
    public class All
    {
        public object Query { get; set; }
        public object Checkbox { get; set; }
        public object Date { get; set; }
    }
}
