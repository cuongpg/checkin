﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.Pagination
{
    public class StatusFilter
    {
        public int Status { get; set; }
    }
}
