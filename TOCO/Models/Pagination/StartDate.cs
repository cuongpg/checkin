﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.Pagination
{
    public class SearchDateObject
    {
        public string StartAt { get; set; }
        public string EndAt { get; set; }
    }
}
