﻿using System;

namespace TOCO.Models.Pagination
{
    public class NotificationPagination
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string DocumentType { get; set; }
        public string Parameters { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
