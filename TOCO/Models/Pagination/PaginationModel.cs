﻿namespace TOCO.Models.Pagination
{
    public class PaginationModel
    {
        public int Start { get; set; }
        public int TotalItemCount { get; set; }
        public int Number { get; set; }
        public int NumberOfPages { get; set; }

        public PaginationModel()
        {
            Number = Constants.Paging.PageSize;
        }
    }
}
