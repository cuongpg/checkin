﻿namespace TOCO.Models.Pagination
{
    public class UserPagination
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Status { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public string EmployeeCode { get; set; }
    }
}
