﻿using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public enum CorporationStatus: int
    {
        Deactive = 0,
        Actived = 1
    }

    public class Corporation
    {
        public int Id { get; set; }

        public int ParentId { get; set; }

        public int Level { get; set; }

        public string NameNative { get; set; }

        public string ShortName { get; set; }

        public string NameEn { get; set; }

        public string CompleteCode { get; set; }

        public string ShortCode { get; set; }

        public string TaxCode { get; set; }

        public string Location { get; set; }

        public string AddressInCountry { get; set; }

        public string AddressInEnglish { get; set; }

        public int Status { get; set; }

        public CorporationStatus Active { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string Version { get; set; }
    }
}