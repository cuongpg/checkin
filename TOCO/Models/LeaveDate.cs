﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class LeaveDate
    {
        public const string WK = "WK";

        public string Type { get; set; }
        public DateTime Date { get; set; }
        public int EventId { get; set; }
        public string Description { get; set; }
        public string ExportTimeKeepingCode { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        [JsonIgnore]
        public Event Event { get; set; }
    }
}