﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class GroupRoute
    {
        public int GroupId { get; set; }
        public int RouteId { get; set; }
        [JsonIgnore]
        public Group Group { get; set; }
        [JsonIgnore]
        public Route Route { get; set; }
    }
}
