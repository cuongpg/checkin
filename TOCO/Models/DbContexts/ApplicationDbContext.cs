﻿using Microsoft.EntityFrameworkCore;
using System;
using TOCO.Common;

namespace TOCO.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<ClientIP> ClientIPs { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Script> Scripts { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
        public DbSet<CorporationType> CorporationTypes { get; set; }
        public DbSet<TimeKeeping> TimeKeepings { get; set; }
        public DbSet<TimeKeepingDetail> TimeKeepingDetails { get; set; }
        public DbSet<OverTime> OverTimes { get; set; }
        public DbSet<OverTimeDetail> OverTimeDetails { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EstimateOwner> EstimateOwners { get; set; }
        public DbSet<Corporation> Corporations { get; set; }
        public DbSet<ShiftsEat> ShiftsEats { get; set; }
        public DbSet<LeaveDate> LeaveDates { get; set; }
        public DbSet<ApprovalRecord> ApprovalRecords { get; set; }
        public DbSet<ReasonType> ReasonTypes { get; set; }
        public DbSet<GroupEvent> GroupEvents { get; set; }
        public DbSet<EmployeeType> EmployeeTypes { get; set; }
        public DbSet<NotificationTemplate> NotificationTemplates { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<FirebaseToken> FirebaseTokens { get; set; }
        public DbSet<AbsenderQuotas> AbsenderQuotas { get; set; }
        public DbSet<ChangeManagerInfo> ChangeManagerInfo { get; set; }
        public DbSet<ScriptHistory> ScriptHistorys { get; set; }
        public DbSet<HrbHistory> HrbHistories { get; set; }
        public DbSet<LeaveCalendarType> LeaveCalendarType { get; set; }
        public DbSet<LeaveCalendarTypeHistory> LeaveCalendarTypeHistories { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Mechanism> Mechanisms { get; set; }
        public DbSet<UnitRole> UnitRoles { get; set; }
        public DbSet<SendHCMDataHistory> HCMDataHistories { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<MechanismQuotas> MechanismQuotas { get; set; }
        public DbSet<OvertimeQuotas> OvertimeQuotas { get; set; }
        public DbSet<CorporationHistory> CorporationHistories { get; set; }
        public DbSet<FacebookAccount> FacebookAccounts { get; set; }
        public DbSet<OrgUnitJoinInfo> OrgUnitJoinInfos { get; set; }
        public DbSet<ConfirmAllocationV2> ConfirmAllocationV2s { get; set; }
        public DbSet<PopupAction> PopupActions { get; set; }
        public DbSet<PopupConfig> PopupConfigs { get; set; }
        public DbSet<PilotFeature> PilotFeatures { get; set; }
        public DbSet<CommonItem> Commons { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public static ApplicationDbContext CreateInstance()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(AppSetting.Configuration.GetValue("ConnectionString"));
            return new ApplicationDbContext(optionsBuilder.Options);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Script>()
                .HasOne(s => s.EmployeeType)
                .WithMany(e => e.Scripts)
                .HasForeignKey(s => s.EmployeeTypeId);

            modelBuilder.Entity<Event>()
                .HasOne(e => e.EmployeeType)
                .WithMany(et => et.Events)
                .HasForeignKey(e => e.EmployeeTypeId);

            modelBuilder.Entity<Event>()
                .HasOne(e => e.GroupEvent)
                .WithMany(ge => ge.Events)
                .HasForeignKey(e => e.GroupEventId);

            modelBuilder.Entity<ShiftsEat>()
                .HasOne(s => s.User)
                .WithMany(u => u.ShiftsEats)
                .HasForeignKey(s => s.UserId);

            modelBuilder.Entity<TimeKeeping>()
              .HasOne(t => t.User)
              .WithMany(u => u.TimeKeepings)
              .HasForeignKey(t => t.UserId);

            modelBuilder.Entity<TimeKeeping>()
             .HasOne(t => t.Script)
             .WithMany(u => u.TimeKeepings)
             .HasForeignKey(t => t.ScriptId);

            modelBuilder.Entity<TimeKeepingDetail>()
             .HasOne(t => t.TimeKeeping)
             .WithMany(td => td.TimeKeepingDetails)
             .HasForeignKey(t => t.TimeKeepingId);

            modelBuilder.Entity<OverTime>()
               .HasOne(ot => ot.User)
               .WithMany(u => u.OverTimes)
               .HasForeignKey(ot => ot.UserId);

            modelBuilder.Entity<OverTimeDetail>()
               .HasOne(otd => otd.OverTime)
               .WithMany(ot => ot.OverTimeDetails)
               .HasForeignKey(otd => otd.OverTimeId);

            modelBuilder.Entity<OverTimeDetail>()
              .HasOne(otd => otd.Event)
              .WithMany(e => e.OverTimeDetails)
              .HasForeignKey(otd => otd.EventId);

            modelBuilder.Entity<LeaveDate>()
                .HasKey(ld => new { ld.Type, ld.Date });

            modelBuilder.Entity<LeaveDate>()
             .HasOne(ld => ld.Event)
             .WithMany(e => e.LeaveDates)
             .HasForeignKey(ld => ld.EventId);

            modelBuilder.Entity<ApprovalRecord>()
               .HasOne(a => a.User)
               .WithMany(u => u.ApprovalRecords)
               .HasForeignKey(a => a.UserId);

            modelBuilder.Entity<ApprovalRecord>()
              .HasOne(a => a.ReasonType)
              .WithMany(r => r.ApprovalRecords)
              .HasForeignKey(a => a.ReasonTypeId);

            modelBuilder.Entity<UserGroup>()
                .HasKey(ur => new { ur.UserId, ur.GroupId });

            modelBuilder.Entity<UserGroup>()
               .HasOne(ur => ur.User)
               .WithMany(u => u.UserGroups)
               .HasForeignKey(ur => ur.UserId);

            modelBuilder.Entity<UserGroup>()
                .HasOne(ur => ur.Group)
                .WithMany(r => r.UserGroups)
                .HasForeignKey(ur => ur.GroupId);

            modelBuilder.Entity<GroupRoute>()
                .HasKey(ur => new { ur.GroupId, ur.RouteId });

            modelBuilder.Entity<MechanismQuotas>()
             .HasKey(mq => new { mq.MechanismId, mq.LevelId });


            modelBuilder.Entity<GroupRoute>()
               .HasOne(ur => ur.Group)
               .WithMany(u => u.GroupRoutes)
               .HasForeignKey(ur => ur.GroupId);

            modelBuilder.Entity<GroupRoute>()
                .HasOne(ur => ur.Route)
                .WithMany(r => r.GroupRoutes)
                .HasForeignKey(ur => ur.RouteId);
        }
    }
}
