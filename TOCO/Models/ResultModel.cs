﻿namespace TOCO.Models
{
    public class ResultModel
    {
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
