﻿using Newtonsoft.Json.Serialization;
using System.Text.RegularExpressions;

namespace TOCO.Models.APIModels
{
    public class SnakeCaseNamingStrategy : CamelCaseNamingStrategy
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            string pattern = @"[A-Z]";
            string newPropertyName = Regex.Replace(propertyName, pattern, delegate (Match match)
            {
                string v = match.ToString();
                return "_" + char.ToLower(v[0]);
            });
            if(newPropertyName[0] == '_')
            {
                newPropertyName = newPropertyName.Remove(0,1);
            }
            return newPropertyName;
        }
    }
}
