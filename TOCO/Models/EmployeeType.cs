﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class EmployeeType
    {
        public const string CTV = "CTV";
        public const string NV = "NV";

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        [JsonIgnore]
        public List<Script> Scripts { get; set; }
        [JsonIgnore]
        public List<Event> Events { get; set; }
    }
}