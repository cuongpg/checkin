﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{

    public enum StatusApprovalRecord : int
    {
        NotYet = -1,
        Pending = 0,
        Approved = 1,
        Rejected = 2,
        Expired = 4,
        AdminPending = 5,
    }
    [JsonObject(IsReference = true)]
    public class ApprovalRecord
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool IsManyDate { get; set; }
        public int ReasonTypeId { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Reason { get; set; }
        public StatusApprovalRecord Status { get; set; }
        public double TotalPaidLeave { get; set; }
        public double TotalUnpaidLeave { get; set; }
        public DateTime? ApprovalTime { get; set; }
        public bool IsMorning { get; set; }
        public bool IsAfternoon { get; set; }

        public bool IsDestroy { get; set; }
        public StatusApprovalRecord StatusDestroy { get; set; }
        public DateTime? ApprovalDestroyTime { get; set; }
        public DateTime? DestroyTime { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public string ApproverEmail { get; set; }
        public string RejectReason { get; set; }

        public DateTime? AdminConfirmTime { get; set; }
        public string AdminConfirmEmail { get; set; }


        [JsonIgnore]
        public ReasonType ReasonType { get; set; }
        [JsonIgnore]
        public User User { get; set; }


        public ApprovalRecord()
        {
            Status = StatusApprovalRecord.Pending;
            StatusDestroy = StatusApprovalRecord.NotYet;
        }
    }


}