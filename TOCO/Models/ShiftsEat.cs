﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class ShiftsEat
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime WorkDate { get; set; }
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public User User { get; set; }
    }
}