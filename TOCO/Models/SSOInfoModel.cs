using Newtonsoft.Json;

namespace TOCO.Models
{
    public class SSOInfoModel
    {
        public string AccessToken { get; set; }
        
        [JsonProperty("client_id")]
        public string ClientId { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("given_name")]
        public string GivenName { get; set; }
        [JsonProperty("family_name")]
        public string FamilyName { get; set; }
        [JsonProperty("employee_profile_picture")]
        public string Picture { get; set; }
        [JsonProperty("exp")]
        public long Expired { get; set; }
        
        [JsonProperty("active")]
        public bool IsActive { get; set; }

        public string toFullName()
        {
            return $"{GivenName ?? ""} {FamilyName ?? ""}";
        }
    }
}