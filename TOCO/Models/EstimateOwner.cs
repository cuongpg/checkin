﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class EstimateOwner
    {
        [Key]
        public string Id { get; set; }
        public string CompleteCode { get; set; }
        public string ParentId { get; set; }
        public int Level { get; set; }
        public int Active { get; set; }
        public string DivisionNameVn { get; set; }
        public string DivisionNameEn { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}