﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public enum PilotStatus : int
    {
        PILOT = 1,
        ALL = 2,
        DESTROY = 3
    }


    public class PilotFeature
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
