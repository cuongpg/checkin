﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public enum PopupConfigStatus: int
    {
        Deactive = 0,
        Active = 1
    }

    public class PopupConfig
    {
        public int Id { get; set; }
        public string Grades { get; set; }
        public string Track1s { get; set; }
        public string Corporations { get; set; }
        public string Locations { get; set; }
        public string OrgUnitLv2s { get; set; }
        public string OrgUnitLv3s { get; set; }
        public string OrgUnitLv4s { get; set; }
        public string OrgUnitLv5s { get; set; }
        public string Actions { get; set; }
        public string Message { get; set; }
        public PopupConfigStatus Status { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }
}
