﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class FirebaseToken
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string DeviceId { get; set; }
        public string Token { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
