﻿namespace TOCO.Models
{
    public class STResponseModel
    {
        public int NumberOfPages { get; set; }
        public int TotalRecords { get; set; }
        public object Data { get; set; }
    }
}
