﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class UserGroup
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }

        [JsonIgnore]
        public User User { get; set; }
        [JsonIgnore]
        public Group Group { get; set; }
    }
}
