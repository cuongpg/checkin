﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public enum SendHCMDataStatus : int
    {
        Fail = 0,
        Success = 1
    }


    [JsonObject(IsReference = true)]
    public class SendHCMDataHistory
    {
        public int Id { get; set; }
        public string InfoType { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string FilePath { get; set; }
        public SendHCMDataStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int Count { get; set; }
    }
}
