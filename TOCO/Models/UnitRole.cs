﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class UnitRole
    {
        public const string HCMCodeDefault = "MEMB";
        public int Id { get; set; }
        public string Name { get; set; }
        public string HCMCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
