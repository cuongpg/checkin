﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class LeaveCalendarType
    {
        public const string VI = "VI";
        public const string TH = "TH";

        [Key]
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
