﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class FacebookAccount
    {
        [Key]
        public int UserId { get; set; }
        public string Facebook { get; set; }
       
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
