﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}