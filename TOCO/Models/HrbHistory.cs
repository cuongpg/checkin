﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public enum HrbStatus : int
    {
        Pending = 0,
        Active = 1
    }

    [JsonObject(IsReference = true)]
    public class HrbHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public HrbStatus HrbStatus { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }


        public bool CompareTo(HrbHistory obj)
        {
            if (obj == null)
            {
                return false;
            }

            return this.UserId == obj.UserId
                && this.HrbStatus.Equals(obj.HrbStatus)
                && this.EffectiveDate == obj.EffectiveDate
                && this.ExpirationDate == obj.ExpirationDate;

        }
    }
}
