﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class ConfirmAllocationRequestModel
    {
        public int RecordId { get; set; }
        public string RejectReason { get; set; }
    }
}
