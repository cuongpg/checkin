﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class ApprovalRecordRequestModel
    {
        public bool IsManyDate { get; set; }
        public bool IsMorning { get; set; }
        public bool IsAfternoon { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ReasonTypeId { get; set; }
        public string Note { get; set; }
    }
}
