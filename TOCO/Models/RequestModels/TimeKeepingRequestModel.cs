﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class TimeKeepingRequestModel
    {
        public string WorkDate { get; set; }
        public bool IsManual { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string Note { get; set; }
        //public int EstimateOwnerId { get; set; }
    }
}
