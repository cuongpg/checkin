﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class NotificationRequestModel
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string DeviceId { get; set; }
        public string DocumentId { get; set; }
        public string DocumentType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Avatar { get; set; }
        public string[] ParameterValues { get; set; }
    }
}
