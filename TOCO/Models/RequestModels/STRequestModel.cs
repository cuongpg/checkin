﻿using Newtonsoft.Json;
using TOCO.Models.Pagination;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class STRequestModel
    {
        public string Pagination { get; set; }
        public string Search { get; set; }
        public string Sort { get; set; }
        public string All { get; set; }

        public PaginationModel GetPagination()
        {
            if (IsEmptyParam(Pagination))
            {
                return new PaginationModel();
            }

            return JsonConvert.DeserializeObject<PaginationModel>(Pagination);
        }

        public T GetSearch<T>()
        {
            if (IsEmptyParam(Search))
            {
                return default(T);
            }

            Search data = JsonConvert.DeserializeObject<Search>(Search);
            return JsonConvert.DeserializeObject<T>(data.PredicateObject.ToString());
        }

        public T GetSort<T>()
        {
            if (IsEmptyParam(Sort))
            {
                return default(T);
            }

            Sort data = JsonConvert.DeserializeObject<Sort>(Sort);
            var x = new Dictionary<string, bool>
            {
                [data.Predicate] = data.Reverse
            };
            var json = JsonConvert.SerializeObject(x);

            return JsonConvert.DeserializeObject<T>(json);
        }

        public T GetQueries<T>()
        {
            var query = GetSearchAll().Query;
            if (query ==  null)
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(query.ToString());
        }

        public List<T> GetCheckboxs<T>()
        {
            var list = new List<T>();
            var checkboxs = GetSearchAll().Checkbox;
            if (checkboxs == null)
            {
                return list;
            }

            return JsonConvert.DeserializeObject<List<T>>(checkboxs.ToString());
        }

        public List<DateSearch> GetDates<T>()
        {
            var list = new List<DateSearch>();
            var dates = GetSearchAll().Date;
            if (dates == null)
            {
                return list;
            }

            return JsonConvert.DeserializeObject<List<DateSearch>>(dates.ToString());
        }

        private All GetSearchAll()
        {
            if (All == null)
            {
                return new All();
            }
            return JsonConvert.DeserializeObject<All>(All);
        }

        private bool IsEmptyParam(string param)
        {
            return string.IsNullOrEmpty(param) || "{}".Equals(param);
        }
    }
}
