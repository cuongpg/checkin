﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class GeographicalLocationRequest
    {
        [JsonProperty("latitude")]

        public float Latitude { get; set; }

        [JsonProperty("longitude")]

        public float Longitude { get; set; }

    }
}
