﻿using Newtonsoft.Json;
using TOCO.Models.Pagination;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class RejectRequestModel : ApprovalRequestModel
    {
        [JsonProperty("reason")]
        public string Reason { get; set; }
    }
}
