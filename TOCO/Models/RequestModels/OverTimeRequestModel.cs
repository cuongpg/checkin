﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class OverTimeRequestModel
    {
        public string WorkDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int? EventId { get; set; }
        public string Note { get; set; }
    }
}
