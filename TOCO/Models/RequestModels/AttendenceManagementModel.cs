﻿using Newtonsoft.Json;
using TOCO.Models.Pagination;
using System.Collections.Generic;
using System;

namespace TOCO.Models
{
    public class AMRecordModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }
    }

    public class AttendenceManagementModel
    {
        [JsonProperty("data")]
        public AMRecordModel[] Data { get; set; }
    }
}
