﻿using Newtonsoft.Json;
using TOCO.Models.Pagination;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class ConfirmRequestModel
    {
        [JsonProperty("ids")]
        public int[] Ids { get; set; }
    }
}
