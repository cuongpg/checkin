﻿using Newtonsoft.Json;
using TOCO.Models.Pagination;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class ApprovalRequestModel
    {
        [JsonProperty("approvalId")]
        public string ApprovalId { get; set; }
    }
}
