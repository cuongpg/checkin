﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class UserRequestModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string ImageUrl { get; set; }
        public int Status { get; set; }
        public int ScriptId { get; set; }
        public int ManagerId { get; set; }
        public int EmployeeCode { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public string CalendarType { get; set; }
        public string Department { get; set; }
        public string Corporation { get; set; }
        public string Nationality { get; set; }
        public string CorporationType { get; set; }
        public string ActiveDate { get; set; }
        public string DisableDate { get; set; }
        public int? HeadOfOrgUnitId { get; set; }

        public int[] Groups { get; set; }

    }
}
