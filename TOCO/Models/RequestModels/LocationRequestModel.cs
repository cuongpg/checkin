﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.RequestModels
{
    public class LocationRequestModel
    {
        public float Longitude { get; set; }
        public float Latitude { get; set; }
    }
}
