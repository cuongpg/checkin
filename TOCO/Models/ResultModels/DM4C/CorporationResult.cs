﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models
{
    public class CorporationResult
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("name_native")]
        public string NameNative { get; set; }
        
        [JsonProperty("short_code")]
        public string ShortCode { get; set; }
    }
}