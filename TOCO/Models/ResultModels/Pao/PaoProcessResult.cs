﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;

namespace TOCO.Models
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class PaoProcessesResult
    {
        public string Id { get; set; }
        public string Avatar { get; set; }
        public string CreatorEmail { get; set; }
        public string CreatorMobile { get; set; }
        public string CreatorName { get; set; }
        public string ProcessName { get; set; }
        public string Description { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public string Status { get; set; }
        public string Deadline { get; set; }
        public string MyApproveDate { get; set; }
    }
}
