﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class ListPaoProcessesResult
    {
        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("processes")]
        public List<PaoProcessesResult> Processes { get; set; }
    }
}
