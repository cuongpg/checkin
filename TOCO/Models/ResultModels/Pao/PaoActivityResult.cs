﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    public class PaoActivityResult
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }

        [JsonProperty("update_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }

        [JsonProperty("doc_link")]
        public string DocLink { get; set; }

    }
}