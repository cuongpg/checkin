﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    public class PaoReplyResult
    {
        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("deleted")]
        public string Deleted { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }
    }
}