﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{

    public class FTTInfo
    {
        [JsonProperty("documents")]
        public string Documents { get; set; }
        [JsonProperty("explain")]
        public string Explain { get; set; }
        [JsonProperty("payment_date")]
        public string PaymentDate { get; set; }
        [JsonProperty("taxable_income")]
        public string TaxableIncome { get; set; }
        [JsonProperty("tax")]
        public string TaxTempDeductible { get; set; }
        [JsonProperty("total_received")]
        public string TotalReceived { get; set; }
    }

    public class FTTInfoResult
    {
        [JsonProperty("com")]
        public List<FTTInfo> Com { get; set; }

        [JsonProperty("bonus")]
        public List<FTTInfo> Bonus { get; set; }

        [JsonProperty("other")]
        public List<FTTInfo> Other { get; set; }
    }
}
