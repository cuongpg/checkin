﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class IncomeInforResult
    {
        [JsonProperty("month")]
        public int Month { get; set; }

        [JsonProperty("year")]
        public int Year { get; set; }

        [JsonProperty("corporation")]
        public string Corporation { get; set; }

        [JsonProperty("actually_received")]
        public int? ActuallyReceived { get; set; }
    }
}
