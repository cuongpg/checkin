﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class TimesheetResult
    {
        public User User { get; set; }
        public int QuotasTimeKeepingInMonth { get; set; }
        public double TotalNumberOfWorkDay { get; set; }
        public double TotalNumberOfLeaveDay { get; set; }
        public int TotalShiftEat { get; set; }
        public List<TimeKeepingResult> TimeKeepingsInMonth { get; set; }
        public List<(double TotalSeconds, string Description, int Percentage)> TotalOverTime { get; set; }
        public List<ApprovalDateResult> ApprovalDates { get; set; }
        public Script Script { get; set; }
        public double TotalWorkSeconds { get; set; }
        public List<LeaveDate> LeaveDates { get; set; }
        public double[] ThailandRemainLeave { get; set; }
        public double[] VietnamRemainLeave { get; set; }
    }
}
