﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class ApprovalDateResult
    {
        public string WorkDate { get; set; }
        public string Reason { get; set; }
        public string Note { get; set; }
        public bool IsMorning { get; set; }
        public bool IsAfternoon { get; set; }
        public double TotalPaidLeave { get; set; }
        public double TotalUnPaidLeave { get; set; }
        public int Status { get; set; }
        public string ReasonExportCode { get; set; }
    }
}
