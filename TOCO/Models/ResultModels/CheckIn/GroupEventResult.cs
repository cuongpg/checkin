﻿using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class GroupEventResult
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? QuotasDefault { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool HasQuotas { get; set; }
        public bool PayByPercentage { get; set; }
        public bool PayByHour { get; set; }
        public bool PayByBlock { get; set; }
        public bool InputEventCode { get; set; }
        public string CorporationType { get; set; }
    }
}