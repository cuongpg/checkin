﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.ResultModels
{
    public class TimeKeepingDetailResult
    {
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public string SystemNote { get; set; }
    }
}
