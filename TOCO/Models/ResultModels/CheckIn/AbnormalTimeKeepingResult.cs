﻿using System;

namespace TOCO.Models
{
   

    public class AbnormalTimeKeepingResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string WorkDate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }       
        public string Note { get; set; }
        public string ImageUrl { get; set; }
        public string SystemNote { get; set; }
        public string ManagerName { get; set; }
        public string ManagerEmail { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public string RejectReason { get; set; }
    }
}