﻿using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class UserResult
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string ImageUrl { get; set; }
        public string ScriptName { get; set; }
        public int ManagerId { get; set; }
        public int EmployeeCode { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string CalendarType { get; set; }
        public string Department { get; set; }
    }
}
