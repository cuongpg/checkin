﻿using System;

namespace TOCO.Models
{
    public enum StatusTimeKeeping : int
    {
        Checkin = 0,
        Checkout = 1,
        Done = 2
    }


    public class StatusTimeKeepingResult
    {
        public double SubstractTime { get; set; }
        public StatusTimeKeeping Status { get; set; }
    }
}