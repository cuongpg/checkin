﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class PendingRecordResult
    {
        public User User { get; set; }
        public User Manager { get; set; }
        public int NumberPendingTimeKeeping { get; set; }
        public int NumberPendingApproval { get; set; }
        public int NumberPendingDestroyApproval { get; set; }
        public int NumberPendingOverTime { get; set; }
        public int NumberPendingChangeManager { get; set; }
    }
}
