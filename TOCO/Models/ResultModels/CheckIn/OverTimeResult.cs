﻿using System;

namespace TOCO.Models
{
    public class OverTimeResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string WorkDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }       
        public string Note { get; set; }
        public string ImageUrl { get; set; }
        public string Event { get; set; }
        public string GroupEventName { get; set; }
        public string Remuneration { get; set; }
        public string ManagerName { get; set; }
        public string ManagerEmail { get; set; }
        public string CreatedAt { get; set; }
        public string ApprovalTime { get; set; }

        public string IsDestroy { get; set; }
        public int StatusDestroy { get; set; }
        public string UpdatedAt { get; set; }
        //public string TimeApprovalDestroy { get; set; }
        //public string TimeDestroy { get; set; }
        public string RejectReason { get; set; }
    }
}