﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class WorkProgressResult
    {
        public string EffectiveDate { get; set; }
        public string ExpirationDate { get; set; }
        public string Script { get; set; }
        public int Status { get; set; }
        public string CalendarType { get; set; }
      
    }
}
