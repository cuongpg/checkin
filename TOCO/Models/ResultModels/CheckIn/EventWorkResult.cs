﻿using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class EventWorkResult
    {
        public string GroupEvent { get; set; }
        public string EventCode { get; set; }
        public string WorkDate { get; set; }
        public double Seconds { get; set; }
        public string Percentage { get; set; }
        public string PayByHour { get; set; }
        public string PayByBlock { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}