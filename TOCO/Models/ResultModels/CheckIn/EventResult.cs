﻿using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class EventResult
    {
        public int Id { get; set; }
        public string EventCode { get; set; }
        public int? Percentage { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int EmployeeTypeId { get; set; }
        public int GroupEventId { get; set; }
        public double? PayPerHour { get; set; }
        public double? PayPerBlock { get; set; }
    }
}