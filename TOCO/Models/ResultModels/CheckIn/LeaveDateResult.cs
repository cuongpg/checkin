﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class LeaveDateResult
    {
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public int EventId { get; set; }
        public string Description { get; set; }
        public int LeaveDateTypeId { get; set; }
        
        public DateTime CreatedAt { get;  set; }
    }
}