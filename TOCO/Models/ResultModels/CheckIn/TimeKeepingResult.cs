﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class TimeKeepingResult
    {
        public double NumberOfWorkDay { get; set; }
        public bool IsMorning { get; set; }
        public bool IsAfternoon { get; set; }
        public double TotalSeconds { get; set; }
        public string WorkDate { get; set; }
        public List<TimeKeepingDetailResult> Details { get; set; }
    }
}
