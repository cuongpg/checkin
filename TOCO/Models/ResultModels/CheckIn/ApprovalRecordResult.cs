﻿using System;

namespace TOCO.Models
{
    public class ApprovalRecordResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string LeaveTime { get; set; }
        public string TotalLeaveDate { get; set; }
        public string TotalPaidLeave { get; set; }
        public string TotalUnpaidLeave { get; set; }
        public string Note { get; set; }
        public string ReasonType { get; set; }
        public string ImageUrl { get; set; }
        public string CreatedAt { get; set; }
        public string ManagerName { get; set; }
        public string ManagerEmail { get; set; }
        public string ApprovalTime { get; set; }
        public string IsDestroy { get; set; }
        public int StatusDestroy { get; set; }
        public string UpdatedAt { get; set; }
        public string RejectReason { get; set; }
        //public string TimeDestroy { get; set; }
    }
}