﻿using System;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class ChangeManagerResult
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public int OldManagerId { get; set; }
        public string OldManagerEmail { get; set; }
        public int OldManagerStatus { get; set; }
        public int NewManagerId { get; set; }
        public string NewManagerEmail { get; set; }
        public int NewManagerStatus { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int MyStatus { get; set; }
        public string RejectReason { get; set; }
    }
}
