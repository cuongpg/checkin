﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class OvertimeDetailResult
    {
        public User User { get; set; }
        public Script Script { get; set; }
        public List<OverTimeDetail> OverTimeDetails { get; set; }
    }
}
