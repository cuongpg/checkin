﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.ResultModels.WisamiResultModels
{
    public class ReasonTypeResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool HasPaidLeave { get; set; }
        public bool NeedAdminConfirm { get; set; }
        public List<(double Number, string ExpirationDate, int GenerationYear)> Buffer { get; set; }
        public List<ReasonTypeResult> Childs { get; set; }
    }
}
