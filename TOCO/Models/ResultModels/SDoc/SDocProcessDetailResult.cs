﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class SDocProcessDetailResult
    {
        [JsonProperty("process_name")]
        public string ProcessName { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("approval_id")]
        public string ApprovalId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("documents")]
        public List<SDocDocumentResult> Documents { get; set; }

        [JsonProperty("activities")]
        public List<SDocActivityResult> Activities { get; set; }

        [JsonProperty("groups")]
        public List<SDocReviewGroupResult> Groups { get; set; }

        [JsonProperty("comments")]
        public List<SDocCommnentResult> Comments { get; set; }

        [JsonProperty("rejected_reasons")]
        public List<SDocRejectedReasonResult> RejectedReasons { get; set; }

        [JsonProperty("reviewing")]
        public string IsReviewing { get; set; }
    }
}
