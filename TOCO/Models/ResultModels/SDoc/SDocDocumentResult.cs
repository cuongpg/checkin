﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    public class SDocDocumentResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("file_id")]
        public string FileId { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("doc_link")]
        public string DocLink { get; set; }
    }
}