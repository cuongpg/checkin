﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    public class SDocRejectedReasonResult
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
    }
}