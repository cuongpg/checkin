﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class SDocReviewGroupResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("reviewers")]
        public List<SDocReviewerResult> Reviewers { get; set; }
    }
}