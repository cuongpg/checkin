﻿using Newtonsoft.Json;

namespace TOCO.Models
{
    public class SDocReviewerResult
    {
        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }
}