﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class SDocCommnentResult
    {
        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("deleted")]
        public string Deleted { get; set; }

        [JsonProperty("replies")]
        public List<SDocReplyResult> Replies { get; set; }
    }
}