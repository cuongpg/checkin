﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class StatementCheckin
    {
        [JsonProperty("cdt")]
        public string EstimationOwner { get; set; }

        [JsonProperty("ch")]
        public string CH { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }

        [JsonProperty("track2")]
        public string Track2 { get; set; }

        [JsonProperty("track3")]
        public string Track3 { get; set; }

        [JsonProperty("sp")]
        public string Product { get; set; }

        [JsonProperty("comMechanisms")]
        public JoinCOMMechanism COMMechanisms { get; set; }
    }
    
    public class ProjectCheckin
    {
        public ProjectCheckin ShallowCopy()
        {
            return (ProjectCheckin)this.MemberwiseClone();
        }

        [Required]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("PERNR")]
        public string PERNR { get; set; }

        [JsonProperty("ch")]
        public string CH { get; set; }

        [Required]
        [JsonProperty("perct")]
        public double PERCT { get; set; }

        [Required]
        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [Required]
        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("PROJ_TYPE_NAME")]
        public string PROJ_TYPE_NAME { get; set; }

        [JsonProperty("HcmId")]
        public string HcmId { get; set; }
    }


    public class DepartmentCheckin
    {
        [JsonProperty("PERNR")]
        public string PERNR { get; set; }

        [Required]
        [JsonProperty("ORG_ID")]
        public string ORG_ID { get; set; }

        [JsonProperty("ORG_SHORT")]
        public string ORG_SHORT { get; set; }

        [JsonProperty("ORG_STEXT")]
        public string ORG_STEXT { get; set; }

        [JsonProperty("HEAD")]
        public string HEAD { get; set; }

        [JsonProperty("HEAD_NAME")]
        public string HEAD_NAME { get; set; }

        [JsonProperty("HILFM")]
        public string HILFM { get; set; }

        [Required]
        [JsonProperty("PROZT")]
        public double PROZT { get; set; }

        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }

        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }
    }

    public class MechanismInformation
    {
        [JsonProperty("recordId")]
        public int RecordId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("employeeCode")]
        public string EmployeeCode { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("statementCheckin")]
        public StatementCheckin StatementCheckin { get; set; }

        [Required]
        [JsonProperty("projects")]
        public List<ProjectCheckin> Projects { get; set; }

        [Required]
        [JsonProperty("departments")]
        public List<DepartmentCheckin> Departments { get; set; }

        [JsonProperty("overtimeQuotas")]
        public double OvertimeQuotas { get; set; }
    }

    public class OrderMechanismInformation
    {
        [JsonProperty("oldValue")]
        public MechanismInformation OldValue { get; set; }

        [JsonProperty("newValue")]
        public MechanismInformation NewValue { get; set; }
    }
}
