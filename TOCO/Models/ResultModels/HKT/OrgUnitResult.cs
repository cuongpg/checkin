﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class OrgUnitResult
    {
        [JsonProperty("OBJID")]
        public string OBJID { get; set; }
        [JsonProperty("SHORT")]
        public string SHORT { get; set; }
        [JsonProperty("FULL_CODE")]
        public string FULL_CODE { get; set; }
        [JsonProperty("STEXT")]
        public string STEXT { get; set; }
        [JsonProperty("HEAD_P")]
        public string HEAD_P { get; set; }
        [JsonProperty("HILFM")]
        public string HILFM { get; set; }
        [JsonProperty("PARID")]
        public string PARID { get; set; }
        [JsonProperty("UNIT_TYPE")]
        public string UNIT_TYPE { get; set; }
        [JsonIgnore]
        public int LEVEL { get; set; }
    }
}
