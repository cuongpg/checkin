﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class PositionsInformation
    {
        public string Grade { get; set; }
        public string Track1 { get; set; }
        public string Track2 { get; set; }
        //public string Corporation { get; set; }
    }

    public class LongtermAllocation
    {
        public string OrgUnitLv2 { get; set; }
        public string OrgUnitLv3 { get; set; }
        public string OrgUnitLv4 { get; set; }
        public string OrgUnitLv5 { get; set; }
        public string HeadOfOrgLv2 { get; set; }
        public string HeadOfOrgLv3 { get; set; }
        public string HeadOfOrgLv4 { get; set; }
        public string HeadOfOrgLv5 { get; set; }
        public string DepartmentType { get; set; }
        public string CH { get; set; }
        public string KH200 { get; set; }
    }

    public class SorttermAllocation
    {
        public List<JoinUnit> All { get; set; }
        public List<JoinUnit> Projects { get; set; }
        public List<JoinUnit> Departments { get; set; }
    }

    public class Unit
    {
        [Required]
        public string Id { get; set; }
        public string Name { get; set; }
        [Required]
        public string ShortCode { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string CH { get; set; }
        [Required]
        public string StartDate { get; set; }
        [Required]
        public string EndDate { get; set; }
        public string DepartmentType { get; set; }
        public string HeadOfUnit { get; set; }
        public int Level { get; set; }
        public string ParentId { get; set; }
    }

    public class JoinUnit : Unit
    {
        [Required]
        public double Percent { get; set; }
        public string HcmId { get; set; }
        public string DepartmentRole { get; set; }
        public string HcmRole { get; set; }
        public JoinUnit()
        {
        }

        public JoinUnit(Unit unit, UnitRole unitRole,  string start, string end, double percent)
        {
            this.Id = unit.Id;
            this.Name = unit.Name;
            this.Code = unit.Code;
            this.ShortCode = unit.ShortCode;
            this.CH = unit.CH;
            this.DepartmentType = unit.DepartmentType;
            this.HeadOfUnit = unit.HeadOfUnit;
            this.StartDate = start;
            this.EndDate = end;
            this.Percent = percent;

            this.DepartmentRole = unitRole.Name;
            this.HcmRole = unitRole.HCMCode;
        }

        public JoinUnit ShallowCopy()
        {
            return (JoinUnit)this.MemberwiseClone();
        }
    }
    
    public class MechanismInformationV2
    {
        public int RecordId { get; set; }
        public string UserName { get; set; }
        public string EmployeeCode { get; set; }
        public string Email { get; set; }
        [Required]
        public PositionsInformation PositionsInformation { get; set; }
        [Required]
        public LongtermAllocation LongtermAllocation { get; set; }
        [Required]
        public SorttermAllocation SorttermAllocation { get; set; }
        public JoinCOMMechanism ComMechanisms { get; set; }
        public double OvertimeQuotas { get; set; }
    }

    public class OrderMechanismInformationV2
    {
        public MechanismInformationV2 OldValue { get; set; }
        public MechanismInformationV2 NewValue { get; set; }
    }
}
