﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class HrDataEmployeeResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("employee_avatar")]
        public string EmployeeAvatar { get; set; }

        [JsonProperty("employee_code")]
        public string EmployeeCode { get; set; }

        [JsonProperty("employee_notify_name")]
        public string EmployeeNotifyName { get; set; }

        [JsonProperty("employee_notify_tel")]
        public string EmployeeNotifyTel { get; set; }

        [JsonProperty("employee_work_email")]
        public string EmployeeWorkEmail { get; set; }

        [JsonProperty("employee_phone_number")]
        public string EmployeePhoneNumber { get; set; }

        [JsonProperty("employee_first_name")]
        public string EmployeeFirstName { get; set; }

        [JsonProperty("employee_last_name")]
        public string EmployeeLastName { get; set; }

        [JsonProperty("employee_dob")]
        public string EmployeeDob { get; set; }

        [JsonProperty("employee_gender")]
        public string EmployeeGender { get; set; }

        [JsonProperty("facebook_account")]
        public string EmployeeFacebookAccount { get; set; }

        [JsonProperty("employee_address")]
        public string EmployeeAddress { get; set; }

        [JsonProperty("employee_current_address")]
        public string EmployeeCurrentAddress { get; set; }

        [JsonProperty("employee_identification")]
        public string EmployeeIdentification { get; set; }

        [JsonProperty("employe_marriage_status")]
        public string EmployeeMarriageStatus { get; set; }

        [JsonProperty("employee_email")]
        public string EmployeeEmail { get; set; }

        [JsonProperty("statements")]
        public List<HrDataStatementResult> Statements { get; set; }

        [JsonProperty("firebase_tokens")]
        public List<string> FireBaseTokens { get; set; }
    }
}
