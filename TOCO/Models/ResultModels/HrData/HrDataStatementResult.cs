﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class HrDataStatementResult
    {
        [JsonProperty("employee_level")]
        public string CB { get; set; }

        [JsonProperty("department")]
        public string DV { get; set; }

        [JsonProperty("estimation_owner")]
        public string CDT { get; set; }

        [JsonProperty("hrb_status")]
        public string HRB { get; set; }

        [JsonProperty("employee_supervisor")]
        public string KH200 { get; set; }

        [JsonProperty("employee_corporation")]
        public string PT { get; set; }

        [JsonProperty("rank")]
        public string HAM { get; set; }
    }
}
