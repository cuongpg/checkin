﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    public class OvertimeQuotas
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int EventGroupId { get; set; }
        public double Quotas { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}