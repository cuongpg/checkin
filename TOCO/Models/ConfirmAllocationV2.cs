﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public enum ConfirmAllocationStatusV2 : int
    {
        Destroy = -1,
        Waiting = 0,
        ConfirmByEmployee = 1,
        Reporting = 2,
        ApprovalReport = 3,
        RejectReport = 4,
        Expired = 5
    }

    public partial class ConfirmAllocationV2
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public ConfirmAllocationStatusV2 Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string HrOperResult { get; set; }
        public string RejectReason { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
