﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public struct NotificationTemplateCode
    {
        /// <summary>
        /// Notify_NhanSu_LamThemGio 
        /// </summary>
        public const string ON001 = "ON001";
        /// <summary>
        /// Notify_QuanLy_LamThemGio 
        /// </summary>
        public const string ON002 = "ON002";
        /// <summary>
        /// Notify_NhanSu_NhacNhoCheckOut 
        /// </summary>
        public const string ON003 = "ON003";
        /// <summary>
        /// Notify_NhanSu_ChamCongBatThuong 
        /// </summary>
        public const string ON004 = "ON004";
        /// <summary>
        /// Notify_QuanLy_ChamCongBatThuong 
        /// </summary>
        public const string ON005 = "ON005";
        /// <summary>
        /// Notify_NhanSu_DonXinNghiPhep 
        /// </summary>
        public const string ON006 = "ON006";
        /// <summary>
        /// Notify_QuanLy_DonXinNghiPhep 
        /// </summary>
        public const string ON007 = "ON007";
        /// <summary>
        /// Notify_NhanSu_HuyXinNghiPhep 
        /// </summary>
        public const string ON008 = "ON008";
        /// <summary>
        /// Notify_NhanSu_Welcome 
        /// </summary>
        public const string ON009 = "ON009";
        /// <summary>
        /// Notify_NhanSu_ThongBaoBangChamCong 
        /// </summary>
        public const string ON011 = "ON011";
        /// <summary>
        /// Notify_Quanly_nhac_duyet_don 
        /// </summary>
        public const string ON015 = "ON015";
        /// <summary>
        /// Notify_NhanSu_ThongBaoBangChamCongChot 
        /// </summary>
        public const string ON016 = "ON016";
        /// <summary>
        /// Notify_NhacCheckin 
        /// </summary>
        public const string ON019 = "ON019";
        /// <summary>
        /// Notify_NhacCheckin 
        /// </summary>
        public const string ON020 = "ON020";
        /// <summary>
        /// Notify_NhacCheckin 
        /// </summary>
        public const string ON021 = "ON021";
        /// <summary>
        /// Notify_Reviewer_DuocProcess 
        /// </summary>
        public const string OS001 = "OS001";
        /// <summary>
        /// Notify_Submitter_ThongBaoReject 
        /// </summary>
        public const string OS002 = "OS002";
        /// <summary>
        /// Notify_Reviewer_ThongBaoHuyTuSubmitter 
        /// </summary>
        public const string OS003 = "OS003";
        /// <summary>
        /// Notify_Submitter_ThongBaoHuyTuAdmin 
        /// </summary>
        public const string OS004 = "OS004";
        /// <summary>
        /// Notify_Reviewer_ThongBaoHuyTuAdmin 
        /// </summary>
        public const string OS005 = "OS005";
        /// <summary>
        /// Notify_Submitter_HoanThanhDonHang 
        /// </summary>
        public const string OS006 = "OS006";
        /// <summary>
        /// Notify_Submitter_ThongBaoRejectProcess 
        /// </summary>
        public const string OS007 = "OS007";
        /// <summary>
        /// Notify_Reviewer_NhanDuocReProcess 
        /// </summary>
        public const string OS008 = "OS008";
        /// <summary>
        /// Notify_Rejector_NhanDuocReProcess 
        /// </summary>
        public const string OS009 = "OS009";
        /// <summary>
        /// Notify_Reviewer_HoanThanhDonHang 
        /// </summary>
        public const string OS010 = "OS010";
        /// <summary>
        /// Notify_Submitter_HoanThanhDonHang 
        /// </summary>
        public const string OS011 = "OS011";
        /// <summary>
        /// Notify_Submitter_ThongBaoRejectReporcess 
        /// </summary>
        public const string OS012 = "OS012";

    }

    public partial class NotificationTemplate
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string DocumentType { get; set; }
        public string Parameters { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
