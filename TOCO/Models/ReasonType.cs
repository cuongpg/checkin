﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public enum ReasonThailandId : int
    {
        AnnualLeaveThId = 12,
        SickLeaveThId = 13,
        RenewVisaLeaveThId = 14,
        BereavementLeaveThId = 15,
        MilitaryLeaveThId = 18,
        MaternityLeaveThId = 19,
        NecessaryBusinessLeaveThId =20,
        UnPaidLeaveThId = 21,
    }

    public enum ReasonVietnamId : int
    {
        AnnualLeaveViId = 2,
    }

    [JsonObject(IsReference = true)]
    public class ReasonType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? BufferDefault { get; set; }
        public bool HasPaidLeave { get; set; }
        public bool HasBufferDate { get; set; }
        public string ReasonExportCode { get; set; }
        public string HCMCode { get; set; }
        public int ParentId { get; set; }
        public string CorporationType { get; set; }
        public bool IsHide { get; set; }
        public int OrderBy { get; set; }
        public string HCMQuotasCode { get; set; }
        public bool NeedAdminConfirm { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        [JsonIgnore]
        public List<ApprovalRecord> ApprovalRecords { get; set; }
    }
}