﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models
{
    [JsonObject(IsReference = true)]
    public class ScriptHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ScriptId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }


        public bool CompareTo(ScriptHistory obj)
        {
            if(obj == null)
            {
                return false;
            }

            return this.UserId == obj.UserId
                && this.ScriptId == obj.ScriptId
                && this.EffectiveDate == obj.EffectiveDate
                && this.ExpirationDate == obj.ExpirationDate;
             
        }
    }
}
