﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocProcessResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public SDocProcessDocumentModel Data { get; set; }
    }
}
