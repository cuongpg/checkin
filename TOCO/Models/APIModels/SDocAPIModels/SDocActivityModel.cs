﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocActivityModel : SDocBaseModel
    {
        public string Content { get; set; }
        public long? ProcessDocumentId { get; set; }
        public int? CreatorId { get; set; }
        public SDocUserModel Creator { get; set; }
    }
}