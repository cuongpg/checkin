﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace TOCO.Models.APIModels.SDocAPIModels
{

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocProcessDocumentModel : SDocBaseModel
    {
        public int? Status { get; set; }
        public long? CreatorId { get; set; }
        public long? ApproverId { get; set; }
        public string MainFileId { get; set; }
        public int? CurrentGroup { get; set; }
        public int? TemplateId { get; set; }
        public string ProcessName { get; set; }
        public int? SendingMethod { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public string OrderCode { get; set; }
        public int? GroupChainId { get; set; }
        public string CanceledReason { get; set; }
        public string OsscarCode { get; set; }
        public string BudgetOwner { get; set; }
        public string Project { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public long? EntityId { get; set; }
        public long? SubCategoryId { get; set; }
        public string PartnerName { get; set; }
        public string ProductName { get; set; }
        public string StatusLabel { get; set; }
        public string MyApproveDate { get; set; }
        public List<SDocActivityModel> Activities { get; set; }
        public List<SDocDocumentModel> Documents { get; set; }
        public SDocUserModel Creator { get; set; }
        public List<SDocReviewerModel> Reviewers { get; set; }
        public SDocUserModel Approver { get; set; }
        public List<SDocRejectedReasonModel> RejectedReasons { get; set; }
        public SDocEntityModel Entity { get; set; }
        public SDocSubCategoryModel SubCategory { get; set; }
        public SDocGroupChainModel GroupChains { get; set; }
        public List<SDocCommentModel> Comments { get; set; }
    }
}