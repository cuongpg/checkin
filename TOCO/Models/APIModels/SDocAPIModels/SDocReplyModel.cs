﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class SDocReplyModel
    {
        public SDocAuthorModel Author { get; set; }
        public string Content { get; set; }
        public string CreatedDate { get; set; }
        public string Deleted { get; set; }
        public string HtmlContent { get; set; }
        public string Kind { get; set; }
        public string ModifiedDate { get; set; }
        public string ReplyId { get; set; }
    }
}