﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels.SDocAPIModels.InputModels
{
    public class SDocProcessAction
    {
        public const int APPROVAL = 1;
        public const int REJECT = 2;

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("approval_id")]
        public string ApprovalId { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }
    }
}
