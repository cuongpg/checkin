﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace TOCO.Models.APIModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class SDocCommentModel
    {
        public string Anchor { get; set; }
        public SDocAuthorModel Author { get; set; }
        public string CommentId { get; set; }
        public string Content { get; set; }
        public string CreatedDate { get; set; }
        public string Deleted { get; set; }
        public string FileId { get; set; }
        public string FileTitle { get; set; }
        public string HtmlContent { get; set; }
        public string Kind { get; set; }
        public string ModifiedDate { get; set; }
        public List<SDocReplyModel> Replies { get; set; }
        public string Status { get; set; }
    }
}