using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocGroupChainModel : SDocBaseModel
    {
        public string Name { get; set; }
        public int? SendingMethod { get; set; }
        public string InspectorEmail { get; set; }
        public int? Status { get; set; }
        public string ApprovedBy { get; set; }
        public string RejectedBy { get; set; }
        public int? SendWhenReprocess { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int? DeadlineRange { get; set; }
        public string GuideLink { get; set; }
    }
}