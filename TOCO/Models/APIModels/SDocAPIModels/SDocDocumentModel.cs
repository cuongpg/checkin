﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocDocumentModel : SDocBaseModel
    {
        public string Name { get; set; }
        public string FileId { get; set; }
        public string Icon { get; set; }
        public string DocLink { get; set; }
        public string MimeType { get; set; }
        public long? LastModifierId { get; set; }
        public long? CreatorId { get; set; }
        public string EmbedLink { get; set; }
        public string ThumbLink { get; set; }
    }
}