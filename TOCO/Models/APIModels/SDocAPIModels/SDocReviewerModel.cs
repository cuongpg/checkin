﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    public enum ProcessStatus
    {
        reviewing = 0,
        approved = 1,
        rejected = 2,
        canceled = 3
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocReviewerModel : SDocBaseModel
    {
        public const int SDOC_STATUS_REVIEWING = 0;

        public int? GroupNum { get; set; }
        public string Email { get; set; }
        public int? Permission { get; set; }
        public ProcessStatus? Status { get; set; }
        public string ApprovalId { get; set; }
        public DateTime? ApprovedAt { get; set; }
        public bool? IsCurrentReviewerOnProcess { get; set; }
        public string Avatar { get; set; }
        public long? GroupChainId { get; set; }
        public string SmallGroupName { get; set; }
    }
}