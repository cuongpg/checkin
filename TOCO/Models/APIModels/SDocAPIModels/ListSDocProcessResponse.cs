﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListSDocProcessResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public SDocProcessPagination Data { get; set; }
    }
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocProcessPagination
    {
        public List<SDocProcessDocumentModel> Items { get; set; }
        public int Page { get; set; }
        public int TotalPage { get; set; }
    }
}
