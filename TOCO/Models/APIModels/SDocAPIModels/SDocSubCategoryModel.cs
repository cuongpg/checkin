using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocSubCategoryModel
    {
        public long Id { get; set; }
        public long? CategoryId { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
    }
}