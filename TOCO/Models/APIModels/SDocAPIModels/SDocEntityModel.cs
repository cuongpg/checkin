using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocEntityModel: SDocBaseModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}