﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocRejectedReasonModel : SDocBaseModel
    {
        public string Content { get; set; }
        public long? ReviewerId { get; set; }
        public long? ProcessDocumentId { get; set; }
        
        public SDocReviewerModel Reviewer { get; set; }
    }
}