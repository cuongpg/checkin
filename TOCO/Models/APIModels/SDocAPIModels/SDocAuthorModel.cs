﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TOCO.Models.APIModels
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class SDocAuthorModel
    {
        public string DisplayName { get; set; }
        public string IsAuthenticatedUser { get; set; }
        public string Kind { get; set; }
        public SDocPictureModel Picture { get; set; }
    }
}