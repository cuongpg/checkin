using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace TOCO.Models.APIModels.SDocAPIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SDocKindOfDocModel : SDocBaseModel
    {
        public List<SDocKindOfDocModel> SubCategories { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string IsVisible { get; set; }
    }
}