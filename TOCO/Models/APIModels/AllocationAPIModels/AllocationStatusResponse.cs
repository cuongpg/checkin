﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public struct ReportType
    {
        public const string Warning = "Warning";
        public const string Info = "Info";
        public const string Error = "Error";
    }

    public class ReportContact
    {
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
    }

    public class AllocationStatusResponse
    {
        public string UserEmail { get; set; }

        public string Type { get; set; }

        public int? Times { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public ReportContact Contact {get; set;}
    }
}
