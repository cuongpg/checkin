﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public enum TN18FTTType : int
    {
        LuongNV = 1,
        LuongCTV = 2,
        TKCM = 3,
        ComCTV = 4,
        ComNV = 5,
        ThuongNV = 6,
        GiangVienAuMy = 7,
        GiangVienThaiLan = 8,
        GiangVienVietNam = 9,
        LaiVay = 10,
        ThuongCTV = 11,
        ChiaSeDoanhThu = 12,
        GVNNCoHDLD = 13,
        GiangVienPhilipine = 14,
    }

    public class Tn18TypeName
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }

    public class Tn18FTTInfo
    {
        [JsonProperty("phap_nhan")]
        public string Corporation { get; set; }
        [JsonProperty("type")]
        public TN18FTTType Type { get; set; }
        [JsonProperty("bo_chung_tu")]
        public string Documents { get; set; }
        [JsonProperty("trang_thai")]
        public string Status { get; set; }
        [JsonProperty("dien_giai")]
        public string Explain { get; set; }
        [JsonProperty("ngay_thanh_toan")]
        public string PaymentDate { get; set; }
        [JsonProperty("tong_thu_nhap_chiu_thue")]
        public int? TaxableIncome { get; set; }
        [JsonProperty("thue_tam_trich")]
        public int? TaxTempDeductible { get; set; }
        [JsonProperty("thuc_nhan")]
        public int? TotalReceived { get; set; }
        [JsonProperty("thang")]
        public int? Month { get; set; }
        [JsonProperty("nam")]
        public int? Year { get; set; }
        [JsonProperty("type_name")]
        public Tn18TypeName TypeName { get; set; }
    }
}
