﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class Tn18SalaryInfoQuery
    {
        [JsonProperty("ma_nhan_vien")]
        public double EmployeeCodeVN { get; set; }
        [JsonProperty("thang")]
        public double MonthVN { get; set; }
        [JsonProperty("nam")]
        public double YearVN { get; set; }
        [JsonProperty("employee_code")]
        public double EmployeeCodeEN { get; set; }
        [JsonProperty("month")]
        public double MonthEN { get; set; }
        [JsonProperty("year")]
        public double YearEN { get; set; }
    }
    public class Tn18EmloyeeInfo
    {
        [JsonProperty("Mã NV HR 20")]
        public double EmployeeCode { get; set; }
        [JsonProperty("Họ")]
        public string FirstName { get; set; }
        [JsonProperty("Tên")]
        public string LastName { get; set; }
        [JsonProperty("Chủ dự toán")]
        public string EstimateOwner { get; set; }
        [JsonProperty("Sản phẩm")]
        public string Product { get; set; }
        [JsonProperty("Loại cơ chế")]
        public string MechanismType { get; set; }
        [JsonProperty("Trạng thái lao động")]
        public string WorkStatus { get; set; }
        [JsonProperty("Bậc")]
        public string Rank { get; set; }
        [JsonProperty("Cấp")]
        public string Level { get; set; }
        [JsonProperty("Đơn vị")]
        public string Unit { get; set; }
        [JsonProperty("Phòng")]
        public string Department { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
    }

    public class Tn18SalaryStructure
    {
        [JsonProperty("Lương cơ sở")]
        public double? BaseSalary { get; set; }
        [JsonProperty("Lương năng suất")]
        public double? ProductivitySalary { get; set; }
        [JsonProperty("Lương hoạt động")]
        public double? TotalSalary { get; set; }
    }

    public class Tn18WorkDate
    {
        [JsonProperty("Công định mức")]
        public double? WorkDateQuotas { get; set; }
        [JsonProperty("Công thực tế")]
        public double? WorkDateNumber { get; set; }
        [JsonProperty("Làm thêm giờ 150%")]
        public double? OT150 { get; set; }
        [JsonProperty("Làm thêm giờ 200%")]
        public double? OT200 { get; set; }
        [JsonProperty("Làm thêm giờ 300%")]
        public double? OT300 { get; set; }
        [JsonProperty("Tổng số giờ làm thêm")]
        public double? TotalOTHour { get; set; }
    }

    public class Tn18Salary
    {
        [JsonProperty("Lương theo ngày công thực tế")]
        public double? RealityWorkSalary { get; set; }
        [JsonProperty("Lương làm thêm giờ 150%")]
        public double? OT150Salary { get; set; }
        [JsonProperty("Lương làm thêm giờ 200%")]
        public double? OT200Salary { get; set; }
        [JsonProperty("Lương làm thêm giờ 300%")]
        public double? OT300Salary { get; set; }
        [JsonProperty("Tổng lương làm thêm giờ")]
        public double? TotalOTSalary { get; set; }
        [JsonProperty("TỔNG LƯƠNG")]
        public double? TotalSalary { get; set; }
    }

    public class Tn18Allowance
    {
        [JsonProperty("Phụ cấp ăn trưa")]
        public double? LunchAllowance { get; set; }
        [JsonProperty("vé tháng")]
        public string MonthlyTicket { get; set; }
        [JsonProperty("Phụ cấp gửi xe")]
        public double? ParkingAllowance { get; set; }
        [JsonProperty("Công tác phí")]
        public double? WorkingFee { get; set; }
        [JsonProperty("Phụ cấp làm thêm giờ theo công việc")]
        public double? OvertimeAllowance { get; set; }
        [JsonProperty("Phụ cấp điện thoại")]
        public double? MobileAllowance { get; set; }
        [JsonProperty("Phụ cấp đào tạo")]
        public double? EduAllowance { get; set; }
        [JsonProperty("Phụ cấp cổ phần")]
        public double? StockAllowance { get; set; }
        [JsonProperty("Phụ cấp khác")]
        public double? OtherAllowance { get; set; }
        [JsonProperty("TỔNG PHỤ CẤP")]
        public double? TotalAllowance { get; set; }
    }

    public class Tn18OtherRevenues
    {
        [JsonProperty("Thưởng lễ (Thưởng kỳ H1/2018)")]
        public double? HolidayBonus { get; set; }
        [JsonProperty("Hoàn thuế")]
        public double? TaxRefund { get; set; }
        [JsonProperty("Phụ cấp ăn ca")]
        public double? ShiftEatAllowance { get; set; }
        [JsonProperty("Truy lĩnh phép")]
        public double? LeaveRetrieval { get; set; }
        [JsonProperty("Truy lĩnh lương")]
        public double? SalaryRetrieval { get; set; }
        [JsonProperty("Truy lĩnh thưởng kỳ H1.2017")]
        public double? HolidayRetrieval { get; set; }
        [JsonProperty("Khác")]
        public double? Other { get; set; }
        [JsonProperty("Com")]
        public double? Com { get; set; }
        [JsonProperty("Thưởng")]
        public double? Bonus { get; set; }
        [JsonProperty("TỔNG CÁC KHOẢN THU KHÁC")]
        public double? TotalOtherRevenues { get; set; }
    }

    public class Tn18TotalIncome
    {
        [JsonProperty("TỔNG LƯƠNG + PHỤ CẤP + CÁC KHOẢN KHÁC")]
        public double? Total { get; set; }
    }

    public class Tn18Insurrance
    {
        [JsonProperty("Tổng BH trích lương Nhân viên (10.5%)")]
        public double? TotalInsurrance { get; set; }
    }

    public class Tn18Deductions
    {
        [JsonProperty("Hoàn ứng công tác phí")]
        public double? WorkFeeReimbursement { get; set; }
        [JsonProperty("Truy thu thuế")]
        public double? TaxReimbursement { get; set; }
        [JsonProperty("Truy thu lương")]
        public double? SalaryReimbursement { get; set; }
        [JsonProperty("Hoàn ứng chi phí gửi xe")]
        public double? ParkingReimbursement { get; set; }
        [JsonProperty("Hoàn Tạm ứng")]
        public double? Reimbursement { get; set; }
        [JsonProperty("Trích tiền gửi xe")]
        public double? QuoteCarDeposit { get; set; }
        [JsonProperty("Giảm trừ khác")]
        public double? OtherDeductions { get; set; }
        [JsonProperty("TỔNG CÁC KHOẢN GIẢM TRỪ")]
        public double? TotalDeduction { get; set; }
    }

    public class Tn18PersonalIncomeTax
    {
        [JsonProperty("Giảm trừ bản thân")]
        public double? PersonalDeduction { get; set; }
        [JsonProperty("Giảm trừ gia cảnh")]
        public double? FamilyDeduction { get; set; }
        [JsonProperty("Thu nhập không chịu thuế")]
        public double? NotTaxableIncome { get; set; }
        [JsonProperty("Tổng giảm trừ tính thuế")]
        public double? TotalTaxableDeduction { get; set; }
        [JsonProperty("Thu nhập tính thuế")]
        public double? TaxelIncome { get; set; }
        [JsonProperty("Thuế TNCN")]
        public double? PersonalIncomeTax { get; set; }
        [JsonProperty("Thuế đã trích")]
        public double? TaxDeductible { get; set; }
    }

    public class Tn18TotalReceived
    {
        [JsonProperty("Tổng thực nhận")]
        public double? TotalReceived { get; set; }
        [JsonProperty("Đã thanh toán")]
        public double? Paid { get; set; }
        [JsonProperty("Còn lại cần thanh toán")]
        public double? RemainingPayment { get; set; }
    }


    public class Tn18SalaryInfoData
    {
        [JsonProperty("1.Thông tin nhân sự")]
        public Tn18EmloyeeInfo EmloyeeInfo { get; set; }

        [JsonProperty("2.Cơ cấu lương")]
        public Tn18SalaryStructure SalaryStructure { get; set; }

        [JsonProperty("3.Ngày công")]
        public Tn18WorkDate WorkDate { get; set; }

        [JsonProperty("4.Lương")]
        public Tn18Salary Salary { get; set; }

        [JsonProperty("5.Phụ Cấp")]
        public Tn18Allowance Allowance { get; set; }

        [JsonProperty("6.Các khoản thu khác")]
        public Tn18OtherRevenues OtherRevenues { get; set; }

        [JsonProperty("7.Tổng thu nhập ( 7=4+5+6 )")]
        public Tn18TotalIncome TotalIncome { get; set; }

        [JsonProperty("8.Bảo hiểm ")]
        public Tn18Insurrance Insurrance { get; set; }

        [JsonProperty("9.Các khoản trừ ")]
        public Tn18Deductions Deductions { get; set; }

        [JsonProperty("10.Thuế TNCN")]
        public Tn18PersonalIncomeTax PersonalIncomeTax { get; set; }

        [JsonProperty("11.Thực nhận (11=7-8-10)")]
        public Tn18TotalReceived TotalReceived { get; set; }
    }

    public class Tn18SalaryInfoResult
    {
        [JsonProperty("status")]
        public double Status { get; set; }

        [JsonProperty("data")]
        public Tn18SalaryInfoData Data { get; set; }

        [JsonProperty("query")]
        public Tn18SalaryInfoQuery Query { get; set; }


    }
}
