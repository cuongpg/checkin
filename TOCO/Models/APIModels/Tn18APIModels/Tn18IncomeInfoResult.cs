﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class Tn18IncomeInfo
    {
        [JsonProperty("phap_nhan")]
        public string Corporation { get; set; }

        [JsonProperty("tong_thu_nhap_truoc_thue")]
        public int? TotalIncomeBeforeTax { get; set; }

        [JsonProperty("tong_thu_nhap_khong_chiu_thue")]
        public int? TotalIncomeNotTaxable { get; set; }

        [JsonProperty("tong_thu_nhap_chiu_thue")]
        public int? TotalTaxableIncome { get; set; }

        [JsonProperty("bao_hiem_xa_hoi")]
        public int? SocialInsurance { get; set; }

        [JsonProperty("thue_tam_trich")]
        public int? Tax { get; set; }

        [JsonProperty("thuc_nhan")]
        public int? ActuallyReceived { get; set; }

        [JsonProperty("thang")]
        public int Month { get; set; }

        [JsonProperty("nam")]
        public int Year { get; set; }

    }

    public class Tn18IncomeInfoResult
    {
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("data")]
        public List<Tn18IncomeInfo> Data { get; set; }

        [JsonProperty("first_page_url")]
        public string FirstPageUrl { get; set; }

        [JsonProperty("from")]
        public int From { get; set; }

        [JsonProperty("last_page")]
        public int LastPage { get; set; }

        [JsonProperty("last_page_url")]
        public string LastPageUrl { get; set; }

        [JsonProperty("next_page_url")]
        public string NextPageUrl { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }

        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }

        [JsonProperty("to")]
        public int To { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }

    }
}
