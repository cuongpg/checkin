﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class ResponseZohoDataContracts
    {
        public List<ZohoDataContracts> Data_HD_new { get; set; }
    }

    public class ZohoDataContracts
    {
        public string Loai_hop_dong { get; set; }
        public int? Nam_hieu_luc { get; set; }
        public string Ngay_hoan_thanh { get; set; }
        public string Hinh_thuc_lam_viec { get; set; }
        public string Hieu_luc_hop_dong { get; set; }
        public string Ngay_nhap { get; set; }
        public string Quan_ly { get; set; }
        public string Ho_va_ten_dem { get; set; }
        public string Don_vi { get; set; }
        public string Trang_thai_HRB { get; set; }
        public string So_DTDD { get; set; }
        public string ID { get; set; }
        public string Level_gia_han { get; set; }
        public string Ngay_nhap_bang_khai_code { get; set; }
        public string Link_scan { get; set; }
        public string Hieu_luc_hop_dong_code { get; set; }
        public string Co_huy_khong { get; set; }
        public string Luong_co_so { get; set; }
        public string Deadline { get; set; }
        public string Chu_du_toan { get; set; }
        public string Ma_nhan_vien { get; set; }
        public string Gui_mail_cho_NLD { get; set; }
        public string Level { get; set; }
        public string Thoi_gian_lam_viec { get; set; }
        public string Noi_cap { get; set; }
        public string Email_cong_ty { get; set; }
        public string Dia_chi_thuong_tru { get; set; }
        public string Trang_thai_deadlines { get; set; }
        public int? Ngay_hieu_luc { get; set; }
        public string Dung_Con { get; set; }
        public string Noi_dung_cong_viec { get; set; }
        public string Ngay_nhap_bang_khai { get; set; }
        public string Trang_thai_in { get; set; }
        public string Ngay_het_hieu_luc_code { get; set; }
        public int? Thang_hieu_luc { get; set; }
        public string Trang_thai_giay_to_Note { get; set; }
        public string Check_hieu_luc { get; set; }
        public string Ngay_hoan_thanh_code { get; set; }
        public string Cap_bac { get; set; }
        public string So_CMND { get; set; }
        public string Luong_nang_suat { get; set; }
        public string Chuc_vu { get; set; }
        public string Ham { get; set; }
        public string Ngay_het_hieu_luc { get; set; }
        public string Ngay_thang_nam_sinh { get; set; }
        public string Luong_hoat_dong { get; set; }
        public string Ten { get; set; }
        public string Ngay_nhap_thong_tin { get; set; }
        public string Ngay_cap { get; set; }
        public string Upload_file_scan { get; set; }
        public string Gia_han { get; set; }
    }
}
