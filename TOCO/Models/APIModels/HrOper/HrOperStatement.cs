﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class HrOperStatementAPIResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public HrOperStatement Data { get; set; }
    }


    public class HrOperStatement
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty("cb")]
        public string Level { get; set; }
        [JsonProperty("ng")]
        public string Track2 { get; set; }
        [JsonProperty("ng2")]
        public string Track3 { get; set; }
        [JsonProperty("sp")]
        public string Product { get; set; }
        [JsonProperty("cdt")]
        public string EstimateOwner { get; set; }
        [JsonProperty("statementposition")]
        public string StatementPosition { get; set; }
    }
}
