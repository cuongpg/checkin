﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class HrOperAPIResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public List<HrOperObject> Data { get; set; }
    }


    public class HrOperObject
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("columnId")]
        public string ColumnId { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("listmasterId")]
        public string ListmasterId { get; set; }
        [JsonProperty("listmasterParentId")]
        public string ListmasterParentId { get; set; }
        [JsonProperty("listmasterLevel")]
        public string ListmasterLevel { get; set; }
        [JsonProperty("listmasterProposalName")]
        public string ListmasterProposalName { get; set; }
        [JsonProperty("listmasterApprovedName")]
        public string ListmasterApprovedName { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedAt { get; set; }
        [JsonProperty("updatedAt")]
        public string UpdatedAt { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
    }
}
