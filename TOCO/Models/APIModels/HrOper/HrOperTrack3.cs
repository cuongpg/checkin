﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class HrOperTrack3APIResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public List<string> Data { get; set; }
    }
}
