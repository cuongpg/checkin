﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class EmployeeDetailResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("manv")]
        public string EmployeeCode { get; set; }
        [JsonProperty("tennv")]
        public string FullName { get; set; }
        [JsonProperty("anhdaidien")]
        public string Avatar { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("ngaysinh")]
        public string BirthDay { get; set; }
        [JsonProperty("headcount")]
        public string Headcount { get; set; }
        [JsonProperty("capbac")]
        public string Level { get; set; }
        [JsonProperty("ngach")]
        public string Rank { get; set; }
        [JsonProperty("gioitinh")]
        public string Gender { get; set; }
        [JsonProperty("madonvi")]
        public string Department { get; set; }
        [JsonProperty("matrungtam")]
        public string Center { get; set; }
        [JsonProperty("Facebook")]
        public string Facebook { get; set; }
        [JsonProperty("loaicoche")]
        public string Mechanism { get; set; }
        [JsonProperty("loaiquyetdinh")]
        public string StatementType { get; set; }
        [JsonProperty("maquyetdinh")]
        public string StatementCode { get; set; }
        [JsonProperty("ngaylapphieu")]
        public string CreatedDate { get; set; }
        [JsonProperty("phapnhan")]
        public string Corporation { get; set; }
        [JsonProperty("sanpham")]
        public string Product { get; set; }
        [JsonProperty("chudutoan")]
        public string EstimateOwner { get; set; }
        [JsonProperty("skype")]
        public string Skype { get; set; }
        [JsonProperty("hoatdong")]
        public string Action { get; set; }
        [JsonProperty("trangthaihrb")]
        public string Hrb { get; set; }
        [JsonProperty("quatrinhlamviec")]
        public string WorkHistory { get; set; }
        [JsonProperty("diadiemlamviec")]
        public string Location { get; set; }
        [JsonProperty("ham")]
        public string Ham { get; set; }
        [JsonProperty("ngachphu")]
        public string Track2 { get; set; }
        [JsonProperty("updatedat")]
        public string UpdatedAt { get; set; }
        [JsonProperty("createdat")]
        public string CreatedAt { get; set; }
    }
}
