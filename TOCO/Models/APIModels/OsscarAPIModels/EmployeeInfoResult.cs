﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class EmployeeInfoResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("urlBase")]
        public string UrlBase { get; set; }
        [JsonProperty("tennv")]
        public string FullName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("anhdaidien")]
        public string Avatar { get; set; }
        [JsonProperty("ngaysinh")]
        public string Birthday { get; set; }
        [JsonProperty("phapnhan")]
        public string Corporation { get; set; }
    }
}
