﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
   
    public class HrDataEmployee
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("employee_code")]
        public string EmployeeCode { get; set; }

        [JsonProperty("employee_first_name")]
        public string EmployeeFirstName { get; set; }

        [JsonProperty("employee_last_name")]
        public string EmployeeLastName { get; set; }

        [JsonProperty("employee_gender")]
        public string EmployeeGender { get; set; }

        [JsonProperty("employee_dob")]
        public DateTime? EmployeeDob { get; set; }

        [JsonProperty("employee_pob")]
        public string EmployeePob { get; set; }

        [JsonProperty("employee_home_town")]
        public string EmployeeHomeTown { get; set; }

        [JsonProperty("employee_identification")]
        public string EmployeeIdentification { get; set; }

        [JsonProperty("employee_identification_date")]
        public DateTime? EmployeeIdentificationDate { get; set; }

        [JsonProperty("employee_identification_place")]
        public string EmployeeIdentificationPlace { get; set; }

        [JsonProperty("employee_passport_number")]
        public string EmployeePassportNumber { get; set; }

        [JsonProperty("employee_passport_valid_date")]
        public DateTime? EmployeePassportValidDate { get; set; }

        [JsonProperty("employee_passport_expired_date")]
        public DateTime? EmployeePassportExpiredDate { get; set; }

        [JsonProperty("employee_email")]
        public string EmployeeEmail { get; set; }

        [JsonProperty("employee_phone_number")]
        public string EmployeePhoneNumber { get; set; }

        [JsonProperty("employee_nationality")]
        public string EmployeeNationality { get; set; }

        [JsonProperty("employee_facebook_account")]
        public string EmployeeFacebookAccount { get; set; }

        [JsonProperty("employee_google_plus_account")]
        public string EmployeeGooglePlusAccount { get; set; }

        [JsonProperty("employee_center")]
        public string EmployeeCenter { get; set; }

        [JsonProperty("employee_group")]
        public string EmployeeGroup { get; set; }

        [JsonProperty("employee_project")]
        public string EmployeeProject { get; set; }

        [JsonProperty("employee_position")]
        public string EmployeePosition { get; set; }

        [JsonProperty("employee_level")]
        public string EmployeeLevel { get; set; }

        [JsonProperty("employee_work_date")]
        public DateTime? EmployeeWorkDate { get; set; }

        [JsonProperty("employee_supervisor")]
        public string EmployeeSupervisor { get; set; }

        [JsonProperty("employee_username")]
        public string EmployeeUsername { get; set; }

        [JsonProperty("employee_work_email")]
        public string EmployeeWorkEmail { get; set; }

        [JsonProperty("employee_headcount")]
        public string EmployeeHeadcount { get; set; }

        [JsonProperty("employee_corporation")]
        public string EmployeeCorporation { get; set; }

        [JsonProperty("employee_document_code")]
        public string EmployeeDocumentCode { get; set; }

        [JsonProperty("employee_note")]
        public string EmployeeNote { get; set; }

        [JsonProperty("employee_home_tel")]
        public string EmployeeHomeTel { get; set; }

        [JsonProperty("employee_bank_branch")]
        public string EmployeeBankBranch { get; set; }

        [JsonProperty("employee_bank")]
        public string EmployeeBank { get; set; }

        [JsonProperty("employee_tax_number")]
        public string EmployeeTaxNumber { get; set; }

        [JsonProperty("employee_address")]
        public string EmployeeAddress { get; set; }

        [JsonProperty("employee_current_address")]
        public string EmployeeCurrentAddress { get; set; }

        [JsonProperty("employee_school")]
        public string EmployeeSchool { get; set; }

        [JsonProperty("employee_degree")]
        public string EmployeeDegree { get; set; }

        [JsonProperty("employee_highest_degree")]
        public string EmployeeHighestDegree { get; set; }

        [JsonProperty("employee_graduate_year")]
        public string EmployeeGraduateYear { get; set; }

        [JsonProperty("employee_major")]
        public string EmployeeMajor { get; set; }

        [JsonProperty("employee_major_type")]
        public string EmployeeMajorType { get; set; }

        [JsonProperty("employee_assurance_number")]
        public string EmployeeAssuranceNumber { get; set; }

        [JsonProperty("employee_assurance_status")]
        public string EmployeeAssuranceStatus { get; set; }

        [JsonProperty("employee_assurance_month")]
        public string EmployeeAssuranceMonth { get; set; }

        [JsonProperty("employee_bank_account")]
        public string EmployeeBankAccount { get; set; }

        [JsonProperty("employe_marriage_status")]
        public string EmployeMarriageStatus { get; set; }

        [JsonProperty("employee_father_name")]
        public string EmployeeFatherName { get; set; }

        [JsonProperty("employee_father_dob")]
        public string EmployeeFatherDob { get; set; }

        [JsonProperty("employee_father_job")]
        public string EmployeeFatherJob { get; set; }

        [JsonProperty("employee_mother_name")]
        public string EmployeeMotherName { get; set; }

        [JsonProperty("employee_mother_dob")]
        public string Employee_mother_dob { get; set; }

        [JsonProperty("employee_mother_job")]
        public string EmployeeMotherJob { get; set; }

        [JsonProperty("employe_husband_name")]
        public string EmployeHusbandName { get; set; }

        [JsonProperty("employee_husband_dob")]
        public string EmployeeHusbandDob { get; set; }

        [JsonProperty("employee_husband_job")]
        public string EmployeeHusbandJob { get; set; }

        [JsonProperty("employee_child_name")]
        public string EmployeeChildName { get; set; }

        [JsonProperty("employee_child_dob")]
        public string EmployeeChildDob { get; set; }

        [JsonProperty("employee_child_job")]
        public string Employee_child_job { get; set; }

        [JsonProperty("employee_child_name_2")]
        public string EmployeeChildName2 { get; set; }

        [JsonProperty("employee_child_dob_2")]
        public string EmployeeChildDob2 { get; set; }

        [JsonProperty("employee_child_job_2")]
        public string EmployeeChildJob2 { get; set; }

        [JsonProperty("employee_child_name_3")]
        public string EmployeeChildName3 { get; set; }

        [JsonProperty("employee_child_dob_3")]
        public string EmployeeChildDob3 { get; set; }

        [JsonProperty("employee_child_job_3")]
        public string EmployeeChildJob3 { get; set; }

        [JsonProperty("employee_notfiy_name")]
        public string EmployeeNotfiyName { get; set; }

        [JsonProperty("employee_notify_relation")]
        public string EmployeeNotifyRelation { get; set; }

        [JsonProperty("employee_notify_tel")]
        public string EmployeeNotifyTel { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("modified_at")]
        public DateTime? ModifiedAt { get; set; }

        [JsonProperty("Statements")]
        public List<HrDataStatement> Statements { get; set; }
    }
}
