﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class HrDataStatement
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("statement_employee_code")]
        public string StatementEmployeeCode { get; set; }

        [JsonProperty("statement_type")]
        public string StatementType { get; set; }

        [JsonProperty("statement_code")]
        public string StatementCode { get; set; }

        [JsonProperty("statement_valid_date")]
        public DateTime? StatementValidDate { get; set; }

        [JsonProperty("statement_expired_date")]
        public DateTime? StatementExpiredDate { get; set; }

        [JsonProperty("statement_created_date")]
        public DateTime? StatementCreatedDate { get; set; }

        [JsonProperty("statement_status")]
        public string StatementStatus { get; set; }

        [JsonProperty("statement_note")]
        public string StatementNote { get; set; }

        [JsonProperty("statement_file_scan")]
        public string StatementFileScan { get; set; }

        [JsonProperty("statement_name")]
        public string StatementName { get; set; }

        [JsonProperty("HRC")]
        public string HRC { get; set; }

        [JsonProperty("PT")]
        public string EmployeeCorporation { get; set; }

        [JsonProperty("CDT")]
        public string EstimationOwner { get; set; }

        [JsonProperty("HD")]
        public string HD { get; set; }

        [JsonProperty("SP")]
        public string SP { get; set; }

        [JsonProperty("CH")]
        public string CH { get; set; }

        [JsonProperty("HRB")]
        public string HrbStatus { get; set; }

        [JsonProperty("DV")]
        public string DV { get; set; }

        [JsonProperty("CB")]
        public string EmployeeLevel { get; set; }

        [JsonProperty("NG")]
        public string NG { get; set; }

        [JsonProperty("LO")]
        public string LO { get; set; }

        [JsonProperty("KN")]
        public string KN { get; set; }

        [JsonProperty("SA100")]
        public string SA100 { get; set; }

        [JsonProperty("SA200")]
        public string SA200 { get; set; }

        [JsonProperty("SA300")]
        public string SA300 { get; set; }

        [JsonProperty("DVC")]
        public string DVC { get; set; }

        [JsonProperty("PC100")]
        public string PC100 { get; set; }

        [JsonProperty("PC200")]
        public string PC200 { get; set; }

        [JsonProperty("PC300")]
        public string PC300 { get; set; }

        [JsonProperty("PC400")]
        public string PC400 { get; set; }

        [JsonProperty("PC900")]
        public string PC900 { get; set; }

        [JsonProperty("KH100")]
        public string KH100 { get; set; }

        [JsonProperty("KH200")]
        public string EmployeeSupervisor { get; set; }

        [JsonProperty("KH300")]
        public string KH300 { get; set; }

        [JsonProperty("NG_PHU")]
        public string ExtraBranch { get; set; }

        [JsonProperty("HAM")]
        public string Rank { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("modified_at")]
        public DateTime? ModifiedAt { get; set; }

        [JsonProperty("PB")]
        public string Department { get; set; }
    }
}
