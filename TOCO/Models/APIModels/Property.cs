﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class Property
    {
        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("default")]
        public string Default { get; set; }

        [BsonElement("visible")]
        public bool Visible { get; set; }

        [BsonElement("bodyColor")]
        public string BodyColor { get; set; }

        [BsonElement("bodyFontSize")]
        public int BodyFontSize { get; set; }

        [BsonElement("bodyIsBold")]
        public bool BodyIsBold { get; set; }

        [BsonElement("bodyIsItalic")]
        public bool BodyIsItalic { get; set; }

        [BsonElement("mergeCell")]
        public int MergeCell { get; set; }

        [BsonElement("startCell")]
        public int StartCell { get; set; }

        [BsonElement("justifyContent")]
        public string JustifyContent { get; set; }

        [BsonElement("alignItems")]
        public string AlignItems { get; set; }

        [BsonElement("titleColor")]
        public string TitleColor { get; set; }

        [BsonElement("titleFontSize")]
        public int TitleFontSize { get; set; }

        [BsonElement("titleIsBold")]
        public bool TitleIsBold { get; set; }

        [BsonElement("titleIsItalic")]
        public bool TitleIsItalic { get; set; }
        
    }
}
