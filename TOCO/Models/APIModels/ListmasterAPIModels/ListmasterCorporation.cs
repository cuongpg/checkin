﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models.APIModels
{
    public class ListmasterCorporation
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("parent_id")]
        public int ParentId { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("name_native")]
        public string NameNative { get; set; }

        [JsonProperty("short_name")]
        public string ShortName { get; set; }

        [JsonProperty("name_en")]
        public string NameEn { get; set; }

        [JsonProperty("complete_code")]
        public string CompleteCode { get; set; }

        [JsonProperty("short_code")]
        public string ShortCode { get; set; }

        [JsonProperty("tax_code")]
        public string TaxCode { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("address_in_country")]
        public string AddressInCountry { get; set; }

        [JsonProperty("address_in_english")]
        public string AddressInEnglish { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("active")]
        public int Active { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }
}