﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models.APIModels
{
    public class ListmasterMeta
    {
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("pageCount")]
        public int PageCount { get; set; }
        [JsonProperty("currentPage")]
        public int CurrentPage { get; set; }
        [JsonProperty("perPage")]
        public int PerPage { get; set; }
    }
}