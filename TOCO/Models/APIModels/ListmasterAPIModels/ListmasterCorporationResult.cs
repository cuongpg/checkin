﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models.APIModels
{
    public class ListmasterCorporationResult
    {
        [JsonProperty("items")]
        public List<ListmasterCorporation> Items { get; set; }
        [JsonProperty("_links")]
        public object Link { get; set; }
        [JsonProperty("_meta")]
        public ListmasterMeta Meta { get; set; }
    }
}