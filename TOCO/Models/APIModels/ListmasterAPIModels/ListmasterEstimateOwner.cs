﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models.APIModels
{
    public class ListmasterEstimateOwner
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }
        [JsonProperty("level")]
        public int Level { get; set; }
        [JsonProperty("division_name_vn")]
        public string DivisionNameVn { get; set; }
        [JsonProperty("division_name_en")]
        public string DivisionNameEn { get; set; }
        [JsonProperty("complete_code")]
        public string CompleteCode { get; set; }
        [JsonProperty("shortened_code")]
        public string ShortenedCode { get; set; }
        [JsonProperty("sp_id")]
        public string SpId { get; set; }
        [JsonProperty("proposal")]
        public string Proposal { get; set; }
        [JsonProperty("approved")]
        public string Approved { get; set; }
        [JsonProperty("proposal_name")]
        public string ProposalName { get; set; }
        [JsonProperty("approved_name")]
        public string ApprovedName { get; set; }
        [JsonProperty("user_create_id")]
        public string UserCreateId { get; set; }
        [JsonProperty("user_create_name")]
        public string UserCreateName { get; set; }
        [JsonProperty("decision_number")]
        public string DecisionNumber { get; set; }
        [JsonProperty("upgrade_note")]
        public string UpgradeNote { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("active")]
        public int Active { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
    }
}