﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterTpData
    {
        public string SmdmId { get; set; }
        public int TpLevel { get; set; }
        public string TpOrgCodeOld { get; set; }
        public string TpOrgNameOld { get; set; }
        public string TpShortCodeOld { get; set; }
        public string TpOrgCodeNew { get; set; }
        public string TpShortCodeNew { get; set; }
        public string TpProduct { get; set; }
        public string TpOrgNameVi { get; set; }
        public string TpOrgNameEn { get; set; }
        public string TpParentOrgId { get; set; }
        public string TpParentOrgName { get; set; }
        public string TpOrgHeadUserId { get; set; }
        public string TpOrgHeadUsername { get; set; }
        public string TpOrgType { get; set; }
        public string TpOrgPolicy { get; set; }
        public string TpBeginDate { get; set; }
        public string TpEndDate { get; set; }
        public string TpStatementLink { get; set; }
        public int TpCurrentStatus { get; set; }
        public int TpStatus { get; set; }
    }

    //[JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    //public class ListmasterHCM
    //{
    //    public string HcmOrgUnitCode { get; set; }
    //    public string HcmOrgUnitName { get; set; }
    //    public string HcmLongCode { get; set; }
    //    public string HcmShortCode { get; set; }
    //    public string HcmHeadOfOrgUserid { get; set; }
    //    public string HcmHeadOfOrgUsername { get; set; }
    //    public string HcmOrgPolicy { get; set; }
    //    public string HcmBeginDate { get; set; }
    //    public string HcmEndDate { get; set; }
    //}

    //[JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    //public class ListmasterProject
    //{
    //    public string PrProjectCode { get; set; }
    //    public string PrProjectName { get; set; }
    //    public string PrCompanyCode { get; set; }
    //    public string PrHeadOfOrgUserid { get; set; }
    //    public string PrHeadOfOrgUsername { get; set; }
    //    public string PrProjectPolicy { get; set; }
    //    public string PrBeginDate { get; set; }
    //    public string PrEndDate { get; set; }
    //    public string PrProjectStatus { get; set; }
    //    public string ProAreaByCompanyCode { get; set; }
    //}

    //[JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    //public class ListmasterFico
    //{
    //    public string FiProfitCenter { get; set; }
    //    public string FiGroupOfFundCenter { get; set; }
    //    public string FiFundCenter { get; set; }
    //    public string FiLevelOfGroupFund { get; set; }
    //    public string FiFundOwner { get; set; }
    //    public string CompanyCodeItems { get; set; }
    //}

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterMST
    {
        public string Id { get; set; }
        public string SmdmId { get; set; }
        public ListmasterTpData Tpdata { get; set; }
        public object Hcm { get; set; }
        public object Project { get; set; }
        public object Fico { get; set; }
        public string CommonStatus { get; set; }
        public string CommonCreatedAt { get; set; }
        public string CommonUpdatedAt { get; set; }
        public string CommonCreatedBy { get; set; }
        public string CommonUpdatedBy { get; set; }
        public string CommonOwner { get; set; }
        public ListmasterDateType CreatedAt { get; set; }
        public ListmasterDateType UpdatedAt { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterMSTMeta
    {
        public ListmasterMSTPagination Pagination { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterMSTPagination
    {
        public int Total { get; set; }
        public int Count { get; set; }
        public int PerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public object Links { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterMSTResult
    {
        public List<ListmasterMST> Data { get; set; }
        public ListmasterMSTMeta Meta { get; set; }
    }
}
