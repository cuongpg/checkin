﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models.APIModels
{
    public class ListmasterEstimateOwnerResult
    {
        [JsonProperty("items")]
        public List<ListmasterEstimateOwner> Items { get; set; }
        [JsonProperty("_links")]
        public object Link { get; set; }
        [JsonProperty("_meta")]
        public ListmasterMeta Meta { get; set; }
    }
}