﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace TOCO.Models.APIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterDateType
    {
        public string Date { get; set; }
        public int TimezoneType { get; set; }
        public string Timezone { get; set; }
    }
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterCommon
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int ListtypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DescriptionEn { get; set; }
        public string Owner { get; set; }
        public string Cdt { get; set; }
        public string Sp { get; set; }
        public string Pt { get; set; }
        public string Hd { get; set; }
        public StatusCommonItem Active { get; set; }
        public ListmasterDateType CreatedAt { get; set; }
        public ListmasterDateType UpdatedAt { get; set; }
    }
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ListmasterCommonResult
    {
        public List<ListmasterCommon> Data { get; set; }
    }
}