﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TOCO.Models.APIModels
{
    public class PaoComments
    {
        [JsonProperty("anchor")]
        public string Anchor { get; set; }

        [JsonProperty("author")]
        public PaoAuthor Author { get; set; }

        [JsonProperty("comment_id")]
        public string CommentId { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("deleted")]
        public string Deleted { get; set; }

        [JsonProperty("file_id")]
        public string FileId { get; set; }

        [JsonProperty("file_title")]
        public string FileTitle { get; set; }

        [JsonProperty("html_content")]
        public string HtmlContent { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("modified_date")]
        public string ModifiedDate { get; set; }

        [JsonProperty("replies")]
        public List<PaoReply> Replies { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}