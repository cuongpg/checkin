﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoReviewer
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("group_num")]
        public string GroupNum { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("permission")]
        public string Permission { get; set; }

        [JsonProperty("process_document_id")]
        public string ProcessDocumentId { get; set; }

        [JsonProperty("approval_id")]
        public string ApprovalId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("approved_at")]
        public string ApprovedAt { get; set; }

        [JsonProperty("template_id")]
        public string TemplateId { get; set; }

        [JsonProperty("html_content")]
        public string IsCurrentReviewerOnProcess { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("group_chain_id")]
        public string GroupChainId { get; set; }

        [JsonProperty("small_group_name")]
        public string SmallGroupName { get; set; }
    }
}