﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoPicture
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}