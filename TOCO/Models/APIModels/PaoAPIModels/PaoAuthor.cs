﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoAuthor
    {
        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("is_authenticated_user")]
        public string IsAuthenticatedUser { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("picture")]
        public PaoPicture Picture { get; set; }
    }
}