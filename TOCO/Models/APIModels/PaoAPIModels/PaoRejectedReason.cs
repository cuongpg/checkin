﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoRejectedReason
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
    }
}