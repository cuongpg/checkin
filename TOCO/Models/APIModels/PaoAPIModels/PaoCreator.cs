﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoCreator
    {
        [JsonProperty("full_name")]
        public string CreatorName { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}