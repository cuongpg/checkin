﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models.APIModels
{
    public class PaoActivity
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("content")]
        public string Action { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }
}