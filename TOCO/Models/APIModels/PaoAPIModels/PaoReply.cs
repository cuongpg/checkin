﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoReply
    {
        [JsonProperty("author")]
        public PaoAuthor Author { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("deleted")]
        public string Deleted { get; set; }

        [JsonProperty("html_content")]
        public string HtmlContent { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("modified_date")]
        public string ModifiedDate { get; set; }

        [JsonProperty("reply_id")]
        public string ReplyId { get; set; }
    }
}