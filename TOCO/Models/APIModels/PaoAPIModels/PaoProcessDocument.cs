﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoProcessDocument
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("move_to_topica_folder")]
        public string MoveToTopicaFolder { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("creator_id")]
        public string CreatorId { get; set; }

        [JsonProperty("approver_id")]
        public string ApproverId { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("main_file_id")]
        public string MainFileId { get; set; }

        [JsonProperty("current_group")]
        public string CurrentGroup { get; set; }

        [JsonProperty(" template_id")]
        public string TemplateId { get; set; }

        [JsonProperty("process_name")]
        public string ProcessName { get; set; }

        [JsonProperty("move_to_subfolder")]
        public string MoveToSubfolder { get; set; }

        [JsonProperty("sending_method")]
        public string SendingMethod { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("send_when_reprocess")]
        public string SendWhenReprocess { get; set; }

        [JsonProperty("reprocess_number")]
        public string ReprocessNumber { get; set; }

        [JsonProperty("deadline")]
        public string Deadline { get; set; }

        [JsonProperty("order_code")]
        public string OrderCode { get; set; }

        [JsonProperty("group_chain_id")]
        public string GroupChainId { get; set; }
    }
}