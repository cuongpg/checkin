﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class PaoProcess
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("creator")]
        public PaoCreator Creator { get; set; }

        [JsonProperty("move_to_topica_folder")]
        public bool? MoveToTopicaFolder { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("creator_id")]
        public int? CreatorId { get; set; }

        [JsonProperty("approver_id")]
        public int? ApproverId { get; set; }

        [JsonProperty("created_at")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? UpdatedAt { get; set; }

        [JsonProperty("main_file_id")]
        public string MainFileId { get; set; }

        [JsonProperty("current_group")]
        public int? CurrentGroup { get; set; }

        [JsonProperty("template_id")]
        public int? TemplateId { get; set; }

        [JsonProperty("process_name")]
        public string ProcessName { get; set; }

        [JsonProperty("move_to_subfolder")]
        public bool? MoveToSubfolder { get; set; }

        [JsonProperty("sending_method")]
        public string SendingMethod { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("send_when_reprocess")]
        public string SendWhenReprocess { get; set; }

        [JsonProperty("reprocess_number")]
        public int? ReprocessNumber { get; set; }

        [JsonProperty("deadline")]
        public string Deadline { get; set; }

        [JsonProperty("order_code")]
        public string OrderCode { get; set; }

        [JsonProperty("group_chain_id")]
        public string GroupChainId { get; set; }

        [JsonProperty("my_approve_date")]
        public string MyApproveDate { get; set; }
    }
}
