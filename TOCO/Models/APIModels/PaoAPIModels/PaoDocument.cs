﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class PaoDocument
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("file_id")]
        public string FileId { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("doc_link")]
        public string DocLink { get; set; }

        [JsonProperty("mime_type")]
        public string MimeType { get; set; }

        [JsonProperty("process_document_id")]
        public string ProcessDocumentId { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("last_modifier_id")]
        public string LastModifierId { get; set; }

        [JsonProperty("creator_id")]
        public string CreatorId { get; set; }

        [JsonProperty("embed_link")]
        public string EmbedLink { get; set; }

        [JsonProperty("thumb_link")]
        public string ThumbLink { get; set; }

        [JsonProperty("template_id")]
        public string TemplateId { get; set; }

        [JsonProperty("template_main_file")]
        public string TemplateMainFile { get; set; }
    }
}