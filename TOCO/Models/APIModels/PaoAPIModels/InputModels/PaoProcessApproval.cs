﻿using TOCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels.PaoAPIModels.InputModels
{
    public class PaoProcessApproval : PaoProcessAction
    {
        public PaoProcessApproval()
        {
            Process = AppSetting.Configuration.GetValue("PAO:Approve");
        }
    }
}
