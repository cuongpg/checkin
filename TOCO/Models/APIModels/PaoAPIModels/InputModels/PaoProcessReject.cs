﻿using Newtonsoft.Json;
using TOCO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels.PaoAPIModels.InputModels
{
    public class PaoProcessReject : PaoProcessAction
    {
        [JsonProperty("reason")]
        public string Reason { get; set; }

        public PaoProcessReject()
        {
            Process = AppSetting.Configuration.GetValue("PAO:Reject");
        }
    }
}
