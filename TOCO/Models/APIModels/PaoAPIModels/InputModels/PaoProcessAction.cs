﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels.PaoAPIModels.InputModels
{
    public class PaoProcessAction
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("process")]
        public string Process { get; set; }

        [JsonProperty("approval_id")]
        public string ApprovalId { get; set; }
    }
}
