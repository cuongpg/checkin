﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class PaoProcessDetail
    {
        [JsonProperty("process_document")]
        public PaoProcessDocument ProcessDocument { get; set; }

        [JsonProperty("documents")]
        public List<PaoDocument> Documents { get; set; }

        [JsonProperty("activities")]
        public Dictionary<string, PaoActivity> Activities { get; set; }

        [JsonProperty("reviewers")]
        public List<PaoReviewer> Reviewers { get; set; }

        [JsonProperty("comments")]
        public List<PaoComments> Comments { get; set; }

        [JsonProperty("rejected_reasons")]
        public Dictionary<string, PaoRejectedReason> RejectedReasons { get; set; }
    }
}
