﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TOCO.Models.APIModels
{
    public class JoinCOMMechanism
    {
        [Required]
        [JsonProperty("ICNUM")]
        public string ICNUM { get; set; }

        [Required]
        [JsonProperty("COMMechanismCode")]
        public string COMMechanismCode { get; set; }

        [JsonProperty("Regulation")]
        public string Regulation { get; set; }

        [JsonProperty("OTRegulation")]
        public string OTRegulation { get; set; }

        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }

        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }

        [JsonProperty("HcmId")]
        public string HcmId { get; set; }
    }
}
