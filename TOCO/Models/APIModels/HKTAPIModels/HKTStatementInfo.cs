﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class HKTStatementInfo
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("CB")]
        public string CB { get; set; }
        [JsonProperty("CDT")]
        public string CDT { get; set; }
        [JsonProperty("CH")]
        public string CH { get; set; }
        [JsonProperty("COM")]
        public string COM { get; set; }
        [JsonProperty("DV")]
        public string DV { get; set; }
        [JsonProperty("HRB")]
        public string HRB { get; set; }
        [JsonProperty("HRC")]
        public string HRC { get; set; }
        [JsonProperty("KH200")]
        public string KH200 { get; set; }
        [JsonProperty("KN")]
        public string KN { get; set; }
        [JsonProperty("LO")]
        public string LO { get; set; }
        [JsonProperty("NG")]
        public string NG { get; set; }
        [JsonProperty("PB")]
        public string PB { get; set; }
        [JsonProperty("PC900")]
        public string PC900 { get; set; }
        [JsonProperty("PT")]
        public string PT { get; set; }
        [JsonProperty("SA100")]
        public string SA100 { get; set; }
        [JsonProperty("SA200")]
        public string SA200 { get; set; }
        [JsonProperty("SA300")]
        public string SA300 { get; set; }
        [JsonProperty("SP")]
        public string SP { get; set; }
        [JsonProperty("statement_code")]
        public string StatementCode { get; set; }
        [JsonProperty("statement_created_date")]
        public string StatementCreatedDate { get; set; }
        [JsonProperty("statement_employee_code")]
        public string StatementEmployeeCode { get; set; }
        [JsonProperty("statement_employee_pernr")]
        public string StatementEmployeePernr { get; set; }
        [JsonProperty("statement_expired_date")]
        public string StatementExpiredDate { get; set; }
        [JsonProperty("statement_type")]
        public string StatementType { get; set; }
        [JsonProperty("statement_valid_date")]
        public string StatementValidDate { get; set; }
        [JsonProperty("statement_name")]
        public string StatementName { get; set; }


        [JsonProperty("ORG_3_HEAD")]
        public string Org3Head { get; set; }
        [JsonProperty("ORG_3")]
        public string Org3 { get; set; }
        [JsonProperty("CDT_HEAD")]
        public string CdtHead { get; set; }
        [JsonProperty("SP_HEAD")]
        public string SpHead { get; set; }
        [JsonProperty("PB_HEAD")]
        public string PbHead { get; set; }
        [JsonProperty("NG1")]
        public string NG1 { get; set; }
        [JsonProperty("NG2")]
        public string NG2 { get; set; }

    }
}
