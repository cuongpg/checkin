﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class COMMechanism
    {
        public const string CO_OT = "Có OT";
        public const string KHONG_OT = "Không OT";

        [JsonProperty("COMMechanismCode")]
        public string COMMechanismCode { get; set; }

        [JsonProperty("Regulation")]
        public string Regulation { get; set; }

        [JsonProperty("RegulationFull")]
        public string RegulationFull { get; set; }

        [JsonProperty("OTRegulation")]
        public string OTRegulation { get; set; }

        [JsonProperty("CDT")]
        public string CDT { get; set; }

        [JsonProperty("SP")]
        public string SP { get; set; }

        [JsonProperty("NG")]
        public string NG { get; set; }

        [JsonProperty("CB")]
        public string CB { get; set; }

        [JsonProperty("HCMCode")]
        public string HCMCode { get; set; }

        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }

        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }

    }
}
