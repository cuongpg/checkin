﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class OrgUnit
    {
        [JsonProperty("children")]
        public List<OrgUnit> Children { get; set; }
        [JsonProperty("INFTY")]
        public string INFTY { get; set; }
        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }
        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }
        [JsonProperty("SEQNR")]
        public string SEQNR { get; set; }
        [JsonProperty("AEDTM")]
        public string AEDTM { get; set; }
        [JsonProperty("UNAME")]
        public string UNAME { get; set; }
        [JsonProperty("OTYPE")]
        public string OTYPE { get; set; }
        [JsonProperty("OBJID")]
        public string OBJID { get; set; }
        [JsonProperty("LANGU")]
        public string LANGU { get; set; }
        [JsonProperty("ITXNR")]
        public string ITXNR { get; set; }
        [JsonProperty("SHORT")]
        public string SHORT { get; set; }
        [JsonProperty("STEXT")]
        public string STEXT { get; set; }
        [JsonProperty("HEAD_P")]
        public string HEAD_P { get; set; }
        [JsonProperty("HILFM")]
        public string HILFM { get; set; }
        [JsonProperty("FULL_CODE")]
        public string FULL_CODE { get; set; }
        [JsonIgnore]
        public string PARID { get; set; }
    }
}
