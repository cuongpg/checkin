﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class PersonalSyncLog
    {
        [JsonProperty("timeStamp")]
        public string TimeStamp { get; set; }
        [JsonProperty("personalNumber")]
        public string PersonalNumber { get; set; }
        [JsonProperty("infoType")]
        public string InfoType { get; set; }
        [JsonProperty("TPID")]
        public string TPID { get; set; }
        [JsonProperty("MASSN")]
        public string MASSN { get; set; }
        [JsonProperty("hist")]
        public string Hist { get; set; }
    }
}
