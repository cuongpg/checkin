﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class ProjectInfo
    {
        [JsonProperty("ID")]
        public string ID { get; set; }

        [JsonProperty("MANDT")]
        public string MANDT { get; set; }

        [JsonProperty("PSPID")]
        public string PSPID { get; set; }

        [JsonProperty("PROJID")]
        public string PROJID { get; set; }

        [JsonProperty("PERNR")]
        public string PERNR { get; set; }

        [JsonProperty("WERKS")]
        public string WERKS { get; set; }

        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }

        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }

        [JsonProperty("SNAME")]
        public string SNAME { get; set; }

        [JsonProperty("ZZURL")]
        public string ZZURL { get; set; }

        [JsonProperty("PROJ_TYPE")]
        public string PROJTYPE { get; set; }

        [JsonProperty("PROJ_STATUS")]
        public string PROJSTATUS { get; set; }

        [JsonProperty("AEDAT")]
        public string AEDAT { get; set; }

        [JsonProperty("AEZET")]
        public string AEZET { get; set; }

        [JsonProperty("AENAM")]
        public string AENAM { get; set; }

        [JsonProperty("PROJ_TYPE_NAME")]
        public string PROJ_TYPE_NAME { get; set; }

        [JsonProperty("UNIT_TYPE")]
        public string UNIT_TYPE { get; set; }
    }
}
