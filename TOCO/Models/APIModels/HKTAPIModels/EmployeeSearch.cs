﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class EmployeeSearch
    {
        [JsonProperty("employee_code")]
        public string EmployeeCode { get; set; }
        [JsonProperty("employee_full_name")]
        public string FullName { get; set; }
        [JsonProperty("employee_work_email")]
        public string Email { get; set; }
        [JsonProperty("employee_phone_number")]
        public string Mobile { get; set; }
        [JsonProperty("employee_dob")]
        public string Birthday { get; set; }
        [JsonProperty("PT")]
        public string Corporation { get; set; }
    }
}
