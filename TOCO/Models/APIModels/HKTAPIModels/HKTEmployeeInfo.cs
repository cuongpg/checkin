﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class HKTEmployeeInfo
    {
        public string EmployeeCode { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeGender { get; set; }
        public string EmployeeDob { get; set; }
        public string EmployeePob { get; set; }
        public string EmployeeCurrentAddress { get; set; }
        public string EmployeeAddress { get; set; }
        public string EmployeeNationality { get; set; }
        public string EmployeeIdentification { get; set; }
        public string EmployeeIdentificationDate { get; set; }
        public string EmployeeIdentificationPlace { get; set; }
        public string EmployeeTaxNumber { get; set; }
        public string EmployeeEmail { get; set; }
        public string EmployeeWorkEmail { get; set; }
        public string EmployeeNotifyTel { get; set; }
        public string EmployeePhoneNumber { get; set; }
        public string EmployeeUsername { get; set; }
        public string EmployeeHomeTel { get; set; }
        public string EmployeeMarriageStatus { get; set; }
        public string EmployeeBank { get; set; }
        public string EmployeeWorkDate { get; set; }
        public string CreatedAt { get; set; }
        public string ModifiedAt { get; set; }

    }
}
