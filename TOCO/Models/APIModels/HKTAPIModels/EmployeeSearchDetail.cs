﻿using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class EmployeeSearchDetail
    {
        [JsonProperty("employee_code")]
        public string EmployeeCode { get; set; }
        [JsonProperty("employee_full_name")]
        public string FullName { get; set; }
        [JsonProperty("employee_first_name")]
        public string EmployeeFirstName { get; set; }
        [JsonProperty("employee_last_name")]
        public string EmployeeLastName { get; set; }
        [JsonProperty("employee_work_email")]
        public string Email { get; set; }
        [JsonProperty("employee_email")]
        public string PersonalEmail { get; set; }
        [JsonProperty("employee_gender")]
        public string Gender { get; set; }
        [JsonProperty("employee_address")]
        public string Address { get; set; }
        [JsonProperty("employee_phone_number")]
        public string Mobile { get; set; }
        [JsonProperty("employee_dob")]
        public string BirthDay { get; set; }
        [JsonProperty("department")]
        public string Department { get; set; }
        [JsonProperty("CDT")]
        public string Center { get; set; }
        [JsonProperty("hr_code")]
        public string HrCode { get; set; }
        [JsonProperty("position")]
        public string Level { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("KH200")]
        public string KH200 { get; set; }
        [JsonProperty("PB")]
        public string PB { get; set; }
        [JsonProperty("NG")]
        public string Rank { get; set; }
        [JsonProperty("HRB")]
        public string Hrb { get; set; }
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }
}
