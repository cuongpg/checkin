﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class JoinProjectInfo
    {
        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }

        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }

        [JsonProperty("PSPID")]
        public string PSPID { get; set; }

        [JsonProperty("PERCT")]
        public double PERCT { get; set; }

        [JsonProperty("POLICY")]
        public string POLICY { get; set; }

        [JsonProperty("TITLE")]
        public string TITLE { get; set; }

        [JsonProperty("SNAME")]
        public string SNAME { get; set; }

        [JsonProperty("PERNR")]
        public string PERNR { get; set; }
    }
}
