﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class KDSObject
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("InfoType")]
        public string InfoType { get; set; }
        [JsonProperty("Field")]
        public string Field { get; set; }
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Priority")]
        public string Priority { get; set; }
        [JsonProperty("Desc_vi")]
        public string Desc_vi { get; set; }
        [JsonProperty("Desc_en")]
        public string Desc_en { get; set; }
    }
}
