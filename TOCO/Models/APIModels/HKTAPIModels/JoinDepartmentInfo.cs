﻿using Newtonsoft.Json;

namespace TOCO.Models.APIModels
{
    public class JoinDepartmentInfo
    {
        [JsonProperty("PERNR")]
        public string PERNR { get; set; }
        [JsonProperty("PERNR_S")]
        public string PERNR_S { get; set; }
        [JsonProperty("PERNR_SHORT")]
        public string PERNR_SHORT { get; set; }
        [JsonProperty("PERNR_STEXT")]
        public string PERNR_STEXT { get; set; }
        [JsonProperty("ORG_ID")]
        public string ORG_ID { get; set; }
        [JsonProperty("ORG_SHORT")]
        public string ORG_SHORT { get; set; }
        [JsonProperty("ORG_STEXT")]
        public string ORG_STEXT { get; set; }
        [JsonProperty("HEAD")]
        public string HEAD { get; set; }
        [JsonProperty("HEAD_S")]
        public string HEAD_S { get; set; }
        [JsonProperty("HEAD_SHORT")]
        public string HEAD_SHORT { get; set; }
        [JsonProperty("HEAD_STEXT")]
        public string HEAD_STEXT { get; set; }
        [JsonProperty("HILFM")]
        public string HILFM { get; set; }
        [JsonProperty("PROZT")]
        public double PROZT { get; set; }
        [JsonProperty("BEGDA")]
        public string BEGDA { get; set; }
        [JsonProperty("ENDDA")]
        public string ENDDA { get; set; }
    }
}
