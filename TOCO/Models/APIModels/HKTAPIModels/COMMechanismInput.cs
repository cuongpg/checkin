﻿using Newtonsoft.Json;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models.APIModels
{
    public class COMMechanismInput
    {
        [JsonProperty("COMMechanismCode")]
        public string COMMechanismCode { get; set; }

        [JsonProperty("CDT")]
        public string CDT { get; set; }

        [JsonProperty("SP")]
        public string SP { get; set; }

        [JsonProperty("NG")]
        public string NG { get; set; }

        [JsonProperty("CB")]
        public string CB { get; set; }
    }
}
