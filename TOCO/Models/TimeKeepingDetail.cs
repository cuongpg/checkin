﻿using Newtonsoft.Json;
using System;

namespace TOCO.Models
{
    public enum StatusTimeKeepingApproval : int
    {
        Done = -1,
        Pending = 0,
        Approved = 1,
        Rejected = 2,
        Destroy = 3,
        Expired = 4,
    }
    [JsonObject(IsReference = true)]
    public class TimeKeepingDetail
    {
        public int Id { get; set; }
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public int TimeKeepingId { get; set; }
        public StatusTimeKeepingApproval Status { get; set; }
        public DateTime? ApprovalTime { get; set; }
        public string Note { get; set; }
        public bool IsManual { get; set; }
        public bool InvalidCheckIn { get; set; }
        public bool InvalidCheckOut { get; set; }
        public int EstimateOwnerId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string SystemNote { get; set; }

        public string ApproverEmail { get; set; }
        public string RejectReason { get; set; }

        public bool IsCheckinFinger { get; set; }
        public bool IsCheckoutFinger { get; set; }
        public string CheckinDevice { get; set; }
        public string CheckoutDevice { get; set; }
        public string CheckinPosition { get; set; }
        public string CheckoutPosition { get; set; }

        [JsonIgnore]
        public TimeKeeping TimeKeeping { get; set; }

        public TimeKeepingDetail()
        {
            Status = StatusTimeKeepingApproval.Done;
        }
    }
}