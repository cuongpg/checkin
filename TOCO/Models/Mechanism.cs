﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public class Mechanism
    {
        public int Id { get; set; }
        public string MechanismName { get; set; }
        public string MechanismHCMCode { get; set; }
        public string MechanismHCMId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
