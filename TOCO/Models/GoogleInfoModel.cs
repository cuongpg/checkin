﻿namespace TOCO.Models
{
    public class GoogleInfoModel
    {
        public string IdToken { get; set; }
        public string aud { get; set; }
        public string hd { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
    }
}
