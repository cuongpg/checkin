﻿using System;
using System.Collections.Generic;

namespace TOCO.Models
{
    public class ClientIP
    {
        public int Id { get; set; }
        public string IPAddress { get; set; }
        public string Branch { get; set; }
       
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
