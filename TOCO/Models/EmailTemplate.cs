﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Models
{
    public struct EmailTemplateCode
    {
        public const string OC001 = "OC001";
        public const string OC002 = "OC002";
        public const string OC003 = "OC003";
        public const string OC004 = "OC004";
        public const string OC005 = "OC005";
        public const string OC006 = "OC006";
        public const string OC007 = "OC007";
        public const string OC008 = "OC008";
        public const string OC009 = "OC009";
        public const string OC010 = "OC010";
        public const string OC011 = "OC011";
        public const string OC012 = "OC012";
        public const string OC013 = "OC013";
        public const string OC014 = "OC014";
        public const string OC015 = "OC015";
        public const string OC016 = "OC016";
        public const string OC017 = "OC017";
        public const string OC018 = "OC018";
        public const string OC020 = "OC020";

        public const string OC030 = "OC030";
        public const string OC031 = "OC031";
        public const string OC032 = "OC032";
        public const string OC033 = "OC033";
    }

    public partial class EmailTemplate
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Parameters { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string EmailType { get; set; }
    }
}
