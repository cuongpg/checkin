﻿using TOCO.Models;
using System.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using TOCO.Common;
using System.Globalization;
using System.Threading.Tasks;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace TOCO.Helpers
{
    public class STHelper
    {
        public static string RemoveHtmlTag(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        /// <summary>
        /// Lay ra danh sach ngay trong khoang thoi gian
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime to)
        {
            for (var day = from.Date; day.Date <= to.Date; day = day.AddDays(1))
                yield return day;
        }


        public static (double morningSeconds, double afternoonSeconds) CalculationWorkSeconds(DateTime checkOutTime, DateTime checkInTime, int lunchBreak)
        {
            // Tính thời gian làm việc
            double morningSeconds = 0;
            double afternoonSeconds = 0;
            string workDate = checkInTime.ToString(Constants.FORMAT_DATE_DDMMYYYY);
            CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
            DateTime startLunch = DateTime.ParseExact(workDate + " " + AppSetting.Configuration.GetValue("LunchBreakStart"), Constants.FORMAT_DATE_DDMMYYYYHHMMSS, customCulture);
            DateTime endLunch = startLunch.AddMinutes(lunchBreak);
            
            // Thời gian làm buổi sáng
            morningSeconds = checkOutTime < startLunch ? checkOutTime.Subtract(checkInTime).TotalSeconds : startLunch.Subtract(checkInTime).TotalSeconds;
            morningSeconds = morningSeconds > 0 ? morningSeconds : 0;
            afternoonSeconds = checkInTime < endLunch ? checkOutTime.Subtract(endLunch).TotalSeconds : checkOutTime.Subtract(checkInTime).TotalSeconds;
            afternoonSeconds = afternoonSeconds > 0 ? afternoonSeconds : 0;
            return (morningSeconds, afternoonSeconds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime? ConvertDateTimeToLocal(string dateTime)
        {
            DateTime? dt = null;
            if (dateTime != null)
            {
                dt = DateTime.ParseExact(dateTime, Constants.FORMAT_DATE_DDMMYYYYHHMMSS, CultureInfo.InvariantCulture);
            }

            return dt;
        }

        /// <summary>
        /// Check effectiveDate of token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>True/False</returns>
        public static bool CheckEffectiveToken(string token)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken tokenSecure = handler.ReadToken(token) as JwtSecurityToken;
            DateTime effectiveDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds((long)tokenSecure.Payload.Exp);
            return DateTime.Now < effectiveDate;
        }

        public static string GenerateJWT(string email, string imageUrl, string employee_code)
        {
            return GenerateJWT(email, imageUrl, employee_code,
                DateTime.Now.AddMinutes(Double.Parse(AppSetting.Configuration.GetValue("Jwt:Expires"))));
        }
        
        public static string GenerateJWT(string email, string imageUrl, string employee_code, DateTime expiredTime)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSetting.Configuration.GetValue("Jwt:Key")));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Email, email),
                new Claim(Constants.IMAGE_URL, imageUrl),
                new Claim(Constants.EMPLOYEE_CODE, employee_code)
            };

            JwtSecurityToken token = new JwtSecurityToken(
                AppSetting.Configuration.GetValue("Jwt:Issuer"),
                AppSetting.Configuration.GetValue("Jwt:Issuer"),
                claims,
                expires: expiredTime,
                signingCredentials: creds
            );

            string jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

        public static GoogleInfoModel GetTokenInfo(string json)
        {
            GoogleInfoModel tokenInfo = new GoogleInfoModel();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(tokenInfo.GetType());
            tokenInfo = ser.ReadObject(ms) as GoogleInfoModel;
            ms.Close();
            return tokenInfo;
        }

        public static async Task<HttpResponseMessage> VerifiedIdToken(string id_token)
        {
            string url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + id_token;

            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync(url);
            return response;
        }
        
        public static async Task<HttpResponseMessage> VerifiedAccessToken(string accessToken)
        {
            var dict = new Dictionary<string, string>();
            dict.Add("client_id", AppSetting.Configuration.GetValue("SSO:ApiResource"));
            dict.Add("client_secret", AppSetting.Configuration.GetValue("SSO:ApiResourceSecret"));
            dict.Add("token", accessToken);
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, AppSetting.Configuration.GetValue("SSO:IntrospectEndPoint")) { Content = new FormUrlEncodedContent(dict) };
            var response = await client.SendAsync(req);
            return response;
        }

        public static T ConvertObject<T, K>(K objectFrom) where T : new()
        {
            T objectTo = new T();
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo propTo in properties)
            {
                PropertyInfo propFrom = typeof(K).GetProperty(propTo.Name);
                if (propFrom != null)
                {
                    var typeName = propTo.PropertyType.Name;
                    if (typeName.Contains("List"))
                    {
                        Type typeItemTo = propTo.PropertyType.GetGenericArguments().Single();
                        Type listTypeTo = typeof(List<>).MakeGenericType(typeItemTo);
                        object valueTo = Activator.CreateInstance(listTypeTo);

                        Type typeItemFrom = propFrom.PropertyType.GetGenericArguments().Single();
                        Type listTypeFrom = typeof(List<>).MakeGenericType(typeItemFrom);
                        IEnumerable valueFrom = (IEnumerable)propFrom.GetValue(objectFrom);
                        Type[] types = new Type[2] { typeItemTo, typeItemFrom };

                        foreach (var itemFrom in valueFrom)
                        {
                            MethodInfo method = typeof(STHelper).GetMethod("ConvertObject");
                            MethodInfo genericMethod = method.MakeGenericMethod(types);
                            object itemTo = genericMethod.Invoke(null, new object[1] { itemFrom });
                            valueTo.GetType().GetMethod("Add").Invoke(valueTo, new[] { itemTo });
                        }
                        propTo.SetValue(objectTo, valueTo);
                    }
                    else
                    {
                        string nameSpaceOfType = propTo.PropertyType.Namespace;
                        byte[] token = propTo.PropertyType.Assembly.GetName().GetPublicKeyToken();
                        bool isSystemType = (nameSpaceOfType == "System") || nameSpaceOfType.StartsWith("System.");
                        object valueFrom = propFrom.GetValue(objectFrom);
                        if (isSystemType && valueFrom != null)
                        {
                            Type typeTo = propTo.PropertyType;
                            Type typeFrom = propFrom.PropertyType;
                            if (typeTo == Type.GetType("System.String"))
                            {
                                string value = (valueFrom is DateTime?) ? DateTime.Parse(valueFrom.ToString()).ToString(Constants.FORMAT_DATE_DDMMYYYY) : valueFrom.ToString();
                                propTo.SetValue(objectTo, value);
                            }
                            else if(typeTo == typeFrom)
                            {
                                propTo.SetValue(objectTo, valueFrom);
                            }
                            
                        }
                        else if (valueFrom != null)
                        {
                            Type[] types = new Type[2] { propTo.PropertyType, propFrom.PropertyType };
                            MethodInfo method = typeof(STHelper).GetMethod("ConvertObject");
                            MethodInfo genericMethod = method.MakeGenericMethod(types);
                            object itemTo = genericMethod.Invoke(null, new object[1] { valueFrom });
                            propTo.SetValue(objectTo, itemTo);
                        }
                        else
                        {
                            propTo.SetValue(objectTo, null);
                        }
                    }
                }
            }
            return objectTo;
        }

        public static T ConvertObjectByLanguage<T, K>(K objectFrom, string language) where T : new()
        {
            T objectTo = new T();
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo propTo in properties)
            {
                PropertyInfo propFrom = typeof(K).GetProperty(propTo.Name);
                if (propFrom != null)
                {
                    var typeName = propTo.PropertyType.Name;
                    if (typeName.Contains("List"))
                    {
                        Type typeItemTo = propTo.PropertyType.GetGenericArguments().Single();
                        Type listTypeTo = typeof(List<>).MakeGenericType(typeItemTo);
                        object valueTo = Activator.CreateInstance(listTypeTo);

                        Type typeItemFrom = propFrom.PropertyType.GetGenericArguments().Single();
                        Type listTypeFrom = typeof(List<>).MakeGenericType(typeItemFrom);
                        IEnumerable valueFrom = (IEnumerable)propFrom.GetValue(objectFrom);
                        Type[] types = new Type[2] { typeItemTo, typeItemFrom };

                        foreach (var itemFrom in valueFrom)
                        {
                            MethodInfo method = typeof(STHelper).GetMethod("ConvertObjectByLanguage");
                            MethodInfo genericMethod = method.MakeGenericMethod(types);
                            object itemTo = genericMethod.Invoke(null, new object[2] { itemFrom, language });
                            valueTo.GetType().GetMethod("Add").Invoke(valueTo, new[] { itemTo });
                        }
                        propTo.SetValue(objectTo, valueTo);
                    }
                    else
                    {
                        string nameSpaceOfType = propTo.PropertyType.Namespace;
                        byte[] token = propTo.PropertyType.Assembly.GetName().GetPublicKeyToken();
                        bool isSystemType = (nameSpaceOfType == "System") || nameSpaceOfType.StartsWith("System.");
                        object valueFrom = propFrom.GetValue(objectFrom);
                        if (isSystemType && valueFrom != null)
                        {
                            Type typeTo = propTo.PropertyType;
                            Type typeFrom = propFrom.PropertyType;
                            if (typeTo == Type.GetType("System.String"))
                            {
                                string value = (valueFrom is DateTime?) ? DateTime.Parse(valueFrom.ToString()).ToString(Constants.FORMAT_DATE_DDMMYYYY) :
                                    MessageManager.GetMessageManager(language).GetValue(valueFrom.ToString());
                                propTo.SetValue(objectTo, value);
                            }
                            else if (typeTo == typeFrom)
                            {
                                propTo.SetValue(objectTo, valueFrom);
                            }

                        }
                        else if (valueFrom != null)
                        {
                            Type[] types = new Type[2] { propTo.PropertyType, propFrom.PropertyType };
                            MethodInfo method = typeof(STHelper).GetMethod("ConvertObjectByLanguage");
                            MethodInfo genericMethod = method.MakeGenericMethod(types);
                            object itemTo = genericMethod.Invoke(null, new object[2] { valueFrom, language });
                            propTo.SetValue(objectTo, itemTo);
                        }
                        else
                        {
                            propTo.SetValue(objectTo, null);
                        }
                    }
                }
            }
            return objectTo;
        }

        public static void Process<V, U>(STRequestModel request, ref int numberOfPages, ref int totalRecords, ref IQueryable<V> query)
        {
            if (request != null)
            {
                query = ProcessSearchAll<V, U>(request, query);

                query = ProcessSearch<V, U>(request, query);

                query = ProcessSort<V, U>(request, query);

                totalRecords = query.Count();

                numberOfPages = ProcessPaging(request, ref query, totalRecords);
            }
        }

        private static int ProcessPaging<V>(STRequestModel request, ref IQueryable<V> query, int totalRecords)
        {
            int numberOfPages;
            var pagination = request.GetPagination();
            numberOfPages = totalRecords / pagination.Number;
            if (totalRecords % pagination.Number != 0)
            {
                numberOfPages++;
            }
            query = query.Skip(pagination.Start).Take(pagination.Number);
            return numberOfPages;
        }

        private static IQueryable<V> ProcessSort<V, U>(STRequestModel request, IQueryable<V> query)
        {
            var sort = request.GetSort<U>();
            if (sort != null)
            {
                foreach (var property in sort.GetType().GetProperties())
                {
                    var name = property.Name;
                    var order = sort.GetType().GetProperty(name).GetValue(sort);
                    if (order != null)
                    {
                        if (!bool.Parse(order.ToString()))
                        {
                            query = query.OrderBy(x => x.GetType().GetProperty(name).GetValue(x));
                        }
                        else
                        {
                            query = query.OrderByDescending(x => x.GetType().GetProperty(name).GetValue(x));
                        }
                        break;
                    }
                }
            }

            return query;
        }

        private static IQueryable<V> ProcessSearch<V, U>(STRequestModel request, IQueryable<V> query)
        {
            var search = request.GetSearch<U>();
            if (search != null)
            {
                foreach (var property in search.GetType().GetProperties())
                {
                    var name = property.Name;
                    var input = search.GetType().GetProperty(name).GetValue(search);
                    if (input != null)
                    {
                        query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null
                            && x.GetType().GetProperty(name).GetValue(x).ToString().ToLower().Contains(input.ToString().ToLower()));
                    }
                }
            }

            return query;
        }

        private static IQueryable<V> ProcessSearchAll<V, U>(STRequestModel request, IQueryable<V> query)
        {
            query = ProcessSearchAllQueries<V, U>(request, query);

            query = ProcessSearchAllCheckbox<V, U>(request, query);

            query = ProcessSearchAllDate<V, U>(request, query);

            return query;
        }

        private static IQueryable<V> ProcessSearchAllDate<V, U>(STRequestModel request, IQueryable<V> query)
        {
            var dates = request.GetDates<U>();
            if (dates.Count > 0)
            {
                foreach (var date in dates)
                {
                    var data = date.GetData<U>();
                    foreach (var property in data.GetType().GetProperties())
                    {
                        var name = property.Name;
                        var input = data.GetType().GetProperty(name).GetValue(data);
                        if (input != null && input.ToString() != "")
                        {
                            DateTime dateInput = DateTime.Parse(input.ToString()).ToLocalTime().Date;
                            if (date.Type.ToString() == "greater")
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null
                                    && DateTime.Parse(x.GetType().GetProperty(name).GetValue(x).ToString()).ToLocalTime().Date >= dateInput);
                            }
                            else if (date.Type.ToString() == "lesser")
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null
                                    && DateTime.Parse(x.GetType().GetProperty(name).GetValue(x).ToString()).ToLocalTime().Date <= dateInput);
                            }
                            else
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null
                                    && DateTime.Parse(x.GetType().GetProperty(name).GetValue(x).ToString()).ToLocalTime().Date == dateInput);
                            }
                        }
                    }
                }
            }

            return query;
        }

        private static IQueryable<V> ProcessSearchAllCheckbox<V, U>(STRequestModel request, IQueryable<V> query)
        {
            var checkboxs = request.GetCheckboxs<U>();
            if (checkboxs.Count > 0)
            {
                foreach (var checkbox in checkboxs)
                {
                    foreach (var property in checkbox.GetType().GetProperties())
                    {
                        var name = property.Name;
                        var input = checkbox.GetType().GetProperty(name).GetValue(checkbox);
                        if (input != null && input.ToString() != "")
                        {
                            if (input.ToString().Equals("IS_NULL"))
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) == null);
                            }
                            else if (input.ToString().Equals("IS_NOT_NULL"))
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null);
                            }
                            else if (typeof(V).GetProperty(name).PropertyType == typeof(Int32) && Int32.TryParse(input.ToString(), out int number))
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null
                                    && Int32.Parse(x.GetType().GetProperty(name).GetValue(x).ToString()) == number);
                            }
                            else if (typeof(V).GetProperty(name).PropertyType == typeof(string))
                            {
                                query = query.Where(x => x.GetType().GetProperty(name).GetValue(x) != null
                                    && x.GetType().GetProperty(name).GetValue(x).ToString() == input.ToString());
                            }
                        }
                    }
                }
            }

            return query;
        }

        private static IQueryable<V> ProcessSearchAllQueries<V, U>(STRequestModel request, IQueryable<V> query)
        {
            var queries = request.GetQueries<U>();
            if (queries != null)
            {
                var predicate = PredicateBuilder.False<V>();
                var oldPredicate = predicate;
                foreach (var property in queries.GetType().GetProperties())
                {
                    var name = property.Name;
                    var input = queries.GetType().GetProperty(name).GetValue(queries);
                    if (input != null && input.ToString() != "")
                    {
                        predicate = predicate.Or(x => x.GetType().GetProperty(name).GetValue(x) != null
                            && x.GetType().GetProperty(name).GetValue(x).ToString().ToLower().Contains(input.ToString().ToLower()));
                    }
                }
                if (predicate != oldPredicate)
                {
                    query = query.Where(predicate);
                }
            }

            return query;
        }

        public static string TimeToString(DateTime dateTime, string language)
        {
            string time = string.Empty;
            switch (language)
            {
                case "vi-VN":
                case "vi":
                case "VN":
                case "Việt Nam":
                    time = string.Format("{0}, {1}", Constants.DAYOFWEEK[dateTime.DayOfWeek], dateTime.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS));
                    break;

                default:
                    time = dateTime.ToString(Constants.FORMAT_DATE_DDDDMMDDYYYYHHMMSS);
                    break;
            }
            return time;
        }
    }
}
