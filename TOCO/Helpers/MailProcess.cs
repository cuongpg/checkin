﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TOCO.Common;

namespace TOCO.Helpers
{
    public interface IMailProcess
    {
        Task Send(string subject, string body, string addressTo, string ccTo, params string[] pathFileAttach);
        Task RunService(string message);
        Task ErrorService(string message);
    }

    [Serializable]
    public class MailProcess : IMailProcess
    {
        private readonly SmtpClient smtpClient;
        private string AddressMailServer { get; set; }
        private string EmailSend { get; set; }
        private string EmailName { get; set; }
        private string Subject { get; set; }
        private string Body { get; set; }

        public MailProcess()
        {
            AddressMailServer = AppSetting.Configuration.GetValue("MailConfig:AddressMailServer");
            bool enableSSL = AppSetting.Configuration.GetValue("MailConfig:EnableSsl").ToUpper() == "true".ToUpper();
            EmailSend = AppSetting.Configuration.GetValue("MailConfig:EmailSend");
            EmailName = AppSetting.Configuration.GetValue("MailConfig:EmailName");
            smtpClient = new SmtpClient(AddressMailServer)
            {
                Port = int.Parse(AppSetting.Configuration.GetValue("MailConfig:Port")),
                Credentials = new System.Net.NetworkCredential(AppSetting.Configuration.GetValue("MailConfig:EmailUsername"), AppSetting.Configuration.GetValue("MailConfig:EmailPassword")),
                EnableSsl = enableSSL,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
        }

        //public async Task PostMail(string EmailTo, string body, string subject)
        //{
        //    PayLoad payLoad = new PayLoad
        //    {
        //        emailId = 7189128292
        //    };
        //    var message = new Message
        //    {
        //        to = EmailTo
        //    };
        //    payLoad.message = message;
        //    var contactProperties = new List<ContactProperty>
        //    {
        //        new ContactProperty() { name = "message", value = body },
        //        new ContactProperty() { name = "marital_status", value = subject }
        //    };
        //    payLoad.contactProperties = contactProperties;
        //    var content = new StringContent(JsonConvert.SerializeObject(payLoad), Encoding.UTF8, "application/json");
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
        //        "Basic",
        //        Convert.ToBase64String(
        //            System.Text.ASCIIEncoding.ASCII.GetBytes(
        //                string.Format("{0}:{1}", EmailUsername, EmailPassword))));
        //            var response = await client.PostAsync("https://api.hubapi.com/email/public/v1/singleEmail/send?hapikey=" + ApiKey, content);
        //}

        public async Task Send(string subject, string body, string addressTo, string ccTo, params string[] pathFileAttach)
        {
            try
            {
                Subject = subject;
                Body = body;
                var mail = new MailMessage
                {
                    From = new MailAddress(EmailSend, EmailName),
                    BodyEncoding = Encoding.UTF8,
                    IsBodyHtml = true,
                    SubjectEncoding = Encoding.UTF8
                };
                mail.To.Add(addressTo);
                if (!string.IsNullOrEmpty(ccTo))
                {
                    mail.CC.Add(addressTo);
                }
                mail.Subject = Subject;
                mail.Body = Body;
                if (pathFileAttach?.Length > 0)
                {
                    foreach (var pathFile in pathFileAttach)
                    {
                        mail.Attachments.Add(new Attachment(pathFile));
                    }
                }
                await Task.Run(() => smtpClient.Send(mail));
            }
            catch (Exception)
            {

            }
        }

        public async Task RunService(string message)
        {
            string emailTo = AppSetting.Configuration.GetValue("OperationEmail");
            string subject = AppSetting.Configuration.GetValue("MailConfig:Subject") + " - Thông báo chạy hệ thống";

            await Send(subject, message, emailTo, null, null);
        }

        public async Task ErrorService(string message)
        {
            string emailTo = AppSetting.Configuration.GetValue("OperationEmail");
            string subject = AppSetting.Configuration.GetValue("MailConfig:Subject") + " - Báo Lỗi Hệ Thống";

            await Send(subject, message, emailTo, null, null);
        }
    }
}
