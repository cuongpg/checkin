﻿using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOCO.Helpers
{
    class ExcelReader : IDisposable
    {
        private readonly IWorkbook hssfwb;
        private readonly string[] sheetNames;

      
        public ExcelReader(IFormFile file)
        {
            string folderName = "Upload";
            string newPath = Path.Combine(@"Exported\", folderName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                string fullPath = Path.Combine(newPath, file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;

                    if (sFileExtension == ".xls")
                    {
                        hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                    }
                    else
                    {
                        hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                    }

                    List<string> sheets = new List<string>();
                    for (int i = 0; i < hssfwb.NumberOfSheets; i++)
                    {
                        string SheetName = hssfwb.GetSheetName(i).Trim().Replace(" ", "");

                        if (!string.IsNullOrEmpty(SheetName))
                        {
                            sheets.Add(SheetName);
                        }
                    }
                    this.sheetNames = sheets.ToArray();
                }
            }
        }

  
        private string GetCellValue(ICell cell)
        {
            var dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);

            // If this is not part of a merge cell,
            // just get this cell's value like normal.
            if (!cell.IsMergedCell)
            {
                return dataFormatter.FormatCellValue(cell);
            }

            // Otherwise, we need to find the value of this merged cell.
            else
            {
                // Get current sheet.
                var currentSheet = cell.Sheet;

                // Loop through all merge regions in this sheet.
                for (int i = 0; i < currentSheet.NumMergedRegions; i++)
                {
                    var mergeRegion = currentSheet.GetMergedRegion(i);

                    // If this merged region contains this cell.
                    if (mergeRegion.FirstRow <= cell.RowIndex && cell.RowIndex <= mergeRegion.LastRow &&
                        mergeRegion.FirstColumn <= cell.ColumnIndex && cell.ColumnIndex <= mergeRegion.LastColumn)
                    {
                        // Find the top-most and left-most cell in this region.
                        var firstRegionCell = currentSheet.GetRow(mergeRegion.FirstRow)
                                                .GetCell(mergeRegion.FirstColumn);

                        // And return its value.
                        return dataFormatter.FormatCellValue(firstRegionCell);
                    }
                }
                // This should never happen.
                throw new Exception("Cannot find this cell in any merged region");
            }
        }

        public string[] GetSheetName()
        {
            return sheetNames;
        }

        public string[][] ReadFile(string sheetName, int startRow)
        {
            List<string[]> data = new List<string[]>();

            // Lấy sheet đầu tiên
            ISheet sheet = hssfwb.GetSheetAt(Array.IndexOf(sheetNames, sheetName));

            int rowIdx = startRow;

            IRow row;

            while ((row = sheet.GetRow(rowIdx)) != null)
            {
                if (row.GetCell(0) == null || string.IsNullOrEmpty(GetCellValue(row.GetCell(0))))
                {
                    break;
                }
                List<string> info = new List<string>();
                int collumIdx = 0;
                ICell cell;
                while ((cell = row.GetCell(collumIdx)) != null)
                {
                    if (cell != null)
                    {
                        info.Add(GetCellValue(cell));
                    }
                    else
                    {
                        info.Add(string.Empty);
                    }
                    collumIdx++;
                }
                data.Add(info.ToArray());
                rowIdx++;
            }

            return data.ToArray();
        }

        public void Dispose()
        {
            if(hssfwb != null)
            {
                hssfwb.Close();
            }
        }
    }
}
