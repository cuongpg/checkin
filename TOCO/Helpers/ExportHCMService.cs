﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Helpers
{
    public class ExportHCMService
    {
        protected readonly string urlFormat;
        protected readonly string userName;
        protected readonly string password;

        public ExportHCMService()
        {
            urlFormat = AppSetting.Configuration.GetValue("HCM:ExportSchedule:UlrFormat");
            userName = AppSetting.Configuration.GetValue("HCM:ExportSchedule:UserName");
            password = AppSetting.Configuration.GetValue("HCM:ExportSchedule:Password");
        }

        public SendHCMDataHistory ExportHCMData(string corporationType, string infoType, DateTime timeRun, DateTime startTime, DateTime endTime, ExportFunction function)
        {
            bool bOk = true;
            string filePath = string.Empty;
            int count = 0;
            try
            {
                filePath = function(corporationType, timeRun, startTime, endTime);
                count = File.ReadAllLines(filePath).Count();
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(userName, password);
                    string nameFile = filePath.Substring(filePath.LastIndexOf("\\") + 1);
                    string url = string.Format(urlFormat, nameFile);
                    client.UploadFile(url, WebRequestMethods.Ftp.UploadFile, filePath);
                }
            }
            catch (Exception ex)
            {
                bOk = false;
            }

            SendHCMDataHistory sendHCMDataHistory = new SendHCMDataHistory
            {
                InfoType = infoType,
                StartTime = startTime,
                EndTime = endTime,
                FilePath = filePath,
                Status = bOk ? SendHCMDataStatus.Success : SendHCMDataStatus.Fail,
                Count = count
            };
            return sendHCMDataHistory;
        }
    }
}

