﻿using System.Linq;

namespace TOCO.Helpers
{
    public class QueryableHelper
    {
        public static IQueryable<T> Default<T>(IQueryable<T> source, string propertyOrderName = "Id")
        {
            return source.Take(Constants.Paging.MaxRecord).OrderByDescending(x => x.GetType().GetProperty(propertyOrderName).GetValue(x));
        }
    }
}
