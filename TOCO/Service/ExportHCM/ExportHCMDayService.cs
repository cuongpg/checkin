﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class ExportHCMDayService : BackgroundService
    {
        public ExportHCMDayService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("HCM:ExportSchedule:Day"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    HcmManager hcmManager = new HcmManager(db);
                    SendHCMDataHistoryService sendHCMDataHistoryService = new SendHCMDataHistoryService(db);
                    ExportHCMService exportHCMService = new ExportHCMService();
                    DateTime startTime = timeRun.AddDays(-1).Date; ;
                    DateTime endTime = timeRun.Date;
                    // Export IT0007
                    sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT0007, timeRun, startTime, endTime, hcmManager.ExportPlannedWorkingTimeEveryDay));
                    // Export IT2006
                    sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT2006, timeRun, startTime, endTime, hcmManager.ExportAbsencesQuotasEveryDay));
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

