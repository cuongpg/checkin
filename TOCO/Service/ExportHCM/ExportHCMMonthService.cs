﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class ExportHCMMonthService : BackgroundService
    {
        public ExportHCMMonthService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("HCM:ExportSchedule:Month"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);

                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    HcmManager hcmManager = new HcmManager(db);
                    SendHCMDataHistoryService sendHCMDataHistoryService = new SendHCMDataHistoryService(db);
                    int month = timeRun.AddMonths(-1).Month;
                    int year = timeRun.AddMonths(-1).Year; ;
                    DateTime startTime = new DateTime(year, month, 1).Date;
                    DateTime endTime = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
                    ExportHCMService exportHCMService = new ExportHCMService();
                    // Export IT2001
                    sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT2001, timeRun, startTime, endTime, hcmManager.ExportAbsencesEveryMonth));
                    // Export IT2010
                    sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT2010, timeRun, startTime, endTime, hcmManager.ExportOvertimeEveryMonth));
                    // Export IT0015
                    sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT0015, timeRun, startTime, endTime, hcmManager.ExportShiftEatsEveryMonth));
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

