﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class ExportHCMCutoffService : BackgroundService
    {
        public ExportHCMCutoffService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int numberCutoff = int.Parse(AppSetting.Configuration.GetValue("HCM:ExportSchedule:Cutoff:Number"));
            int hour = int.Parse(AppSetting.Configuration.GetValue("HCM:ExportSchedule:Cutoff:Hour"));
            int minute = int.Parse(AppSetting.Configuration.GetValue("HCM:ExportSchedule:Cutoff:Minute"));
            int second = int.Parse(AppSetting.Configuration.GetValue("HCM:ExportSchedule:Cutoff:Second"));
            // Run something
            double period;
            DateTime timeRun = GetRunTime(false, numberCutoff, hour, minute, second, LeaveCalendarType.VI);
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : GetRunTime(true, numberCutoff, hour, minute, second, LeaveCalendarType.VI);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    HcmManager hcmManager = new HcmManager(db);
                    SendHCMDataHistoryService sendHCMDataHistoryService = new SendHCMDataHistoryService(db);
                    int month = timeRun.Month;
                    int year = timeRun.Year;
                    DateTime startTime = new DateTime(year, month, 1).Date;
                    DateTime endTime = GetRunTime(false, numberCutoff + 1, hour, minute, second, LeaveCalendarType.VI).Date;
                    ExportHCMService exportHCMService = new ExportHCMService();
                    // Export IT0267
                    sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT0267, timeRun, startTime, endTime, hcmManager.ExportTimeKeepingsEveryMonth));
                }

                // Tính thời gian delay
                timeRun = GetRunTime(true, numberCutoff, hour, minute, second, LeaveCalendarType.VI);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                // Delay tới tháng sau
                await Delay((long)period);
            }
        }
    }
}

