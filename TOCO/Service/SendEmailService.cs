﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class SendEmailService : BackgroundService
    {
        public SendEmailService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await mailProcess.RunService(this.GetType().Name);
            while (!stoppingToken.IsCancellationRequested)
            {
                int id = 0;
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        EmailManager emailManager = new EmailManager(db);

                        List<Email> emails = emailManager.GetEmailsByStatus(EmailStatus.UNSENT);
                        foreach (Email email in emails)
                        {
                            id = email.Id;
                            (string subject, string body) = emailManager.GetContentByEmailType(email);
                            await mailProcess.Send(subject, body, email.EmailTo, null, null);
                            emailManager.UpdateStatus(email.Id, EmailStatus.SENT);
                        }
                    }
                }
                catch (Exception ex)
                {
                    await mailProcess.ErrorService(this.GetType().Name + " - Id:" + id + ". <br>Error: " + ex.Message + "<br>" + ex.StackTrace);
                }
                int period = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:SendEmail"));
                await Task.Delay(period);
            }
        }
    }
}
