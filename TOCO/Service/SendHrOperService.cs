﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Service
{
    public class SendHrOperService : BackgroundService
    {
        public SendHrOperService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:SendHrOper"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                List<(int, string)> err = new List<(int, string)>();

                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    ConfirmAllocationManager confirmAllocationManager = new ConfirmAllocationManager(db);
                    //List<ConfirmAllocation> confirmAllocations = confirmAllocationManager.GetErrorReport();
                    List<ConfirmAllocationV2> confirmAllocationV2s = confirmAllocationManager.GetErrorReportV2();
                    //foreach (ConfirmAllocation confirmAllocation in confirmAllocations)
                    //{
                    //    try
                    //    {
                    //        MechanismInformation oldValue = JsonConvert.DeserializeObject<MechanismInformation>(confirmAllocation.OldValue);
                    //        MechanismInformation newValue = JsonConvert.DeserializeObject<MechanismInformation>(confirmAllocation.NewValue);
                    //        newValue.RecordId = confirmAllocation.Id;
                    //        (string response, HttpStatusCode status, string message) = await HrOperAPI.Connection.Order(new OrderMechanismInformation
                    //        {
                    //            OldValue = oldValue,
                    //            NewValue = newValue
                    //        });

                    //        // Update trạng thái order HrOper
                    //        confirmAllocation.HrOperResult = status.ToString();
                    //        confirmAllocationManager.Update(confirmAllocation);
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        err.Add((confirmAllocation.Id, e.Message));
                    //        continue;
                    //    }
                    //}
                    foreach (ConfirmAllocationV2 confirmAllocationV2 in confirmAllocationV2s)
                    {
                        try
                        {
                            MechanismInformationV2 oldValue = JsonConvert.DeserializeObject<MechanismInformationV2>(confirmAllocationV2.OldValue);
                            MechanismInformationV2 newValue = JsonConvert.DeserializeObject<MechanismInformationV2>(confirmAllocationV2.NewValue);
                            newValue.RecordId = confirmAllocationV2.Id;
                            (string response, HttpStatusCode status, string message) = await HrOperAPI.Connection.OrderV2(new OrderMechanismInformationV2
                            {
                                OldValue = oldValue,
                                NewValue = newValue
                            });

                            // Update trạng thái order HrOper
                            confirmAllocationV2.HrOperResult = status.ToString();
                            confirmAllocationManager.UpdateV2(confirmAllocationV2);
                        }
                        catch (Exception e)
                        {
                            err.Add((confirmAllocationV2.Id, e.Message));
                            continue;
                        }
                    }
                }

                if (err.Count > 0)
                {
                    StringBuilder errorMessage = new StringBuilder();
                    foreach ((int userId, string message) in err)
                    {
                        errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
                    }
                    await mailProcess.ErrorService(this.GetType().Name + ".<br>Id: " + errorMessage.ToString());
                }

                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}
