﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public abstract class SetExpiredRecordService : BackgroundService
    {
        public SetExpiredRecordService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected async Task SetExpiredRecordAsync(DateTime timeRun, ScheduleType type, string corporationType, CancellationToken stoppingToken)
        {
            int month = DateTime.Now.AddMonths(-1).Month;
            int year = DateTime.Now.AddMonths(-1).Year;
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                            OvertimeManager overtimeManager = new OvertimeManager(db);
                            ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);

                            IEnumerable<TimeKeepingDetail> pendingTimeKeepings = timeKeepingManager.GetPendingTimeKeepingLastMonth(startDate, endDate, corporationType);
                            IEnumerable<OverTimeDetail> pendingOverTimes = overtimeManager.GetPendingOverTimeLastMonth(startDate, endDate, corporationType);
                            IEnumerable<ApprovalRecord> pendingApprovals = approvalRecordManager.GetPendingApprovalLastMonth(startDate, endDate, corporationType);

                            foreach (TimeKeepingDetail detail in pendingTimeKeepings)
                            {
                                detail.Status = StatusTimeKeepingApproval.Expired;
                                timeKeepingManager.Update(detail);
                            }
                            foreach (OverTimeDetail detail in pendingOverTimes)
                            {
                                detail.Status = StatusOverTime.Expired;
                                overtimeManager.Update(detail);
                            }
                            foreach (ApprovalRecord detail in pendingApprovals)
                            {
                                if (detail.Status == StatusApprovalRecord.Pending || detail.Status == StatusApprovalRecord.AdminPending)
                                {
                                    detail.Status = StatusApprovalRecord.Expired;
                                }
                                if (detail.StatusDestroy == StatusApprovalRecord.Pending || detail.Status == StatusApprovalRecord.AdminPending)
                                {
                                    detail.StatusDestroy = StatusApprovalRecord.Expired;
                                }
                                approvalRecordManager.Update(detail);
                            }
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            await mailProcess.ErrorService(this.GetType().Name + ". <br>Error: " + ex.Message + "<br>" + ex.StackTrace);
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }

    public class SetExpiredRecordServiceVi : SetExpiredRecordService
    {
        public SetExpiredRecordServiceVi(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Run something
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:vi:SetExpiredRecord"));
            await SetExpiredRecordAsync(timeRun, type, CorporationType.CorporationVI, stoppingToken);
        }
    }

    public class SetExpiredRecordServiceTh : SetExpiredRecordService
    {
        public SetExpiredRecordServiceTh(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Run something
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:th:SetExpiredRecord"));
            await SetExpiredRecordAsync(timeRun, type, CorporationType.CorporationTH, stoppingToken);
        }
    }

}

