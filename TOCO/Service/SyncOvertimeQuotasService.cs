﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Service
{
    public class SyncOvertimeQuotasService : BackgroundService
    {
        private readonly (int, int)[] TOPI_GOD = { (10016, 40) };

        public SyncOvertimeQuotasService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:SyncOvertimeQuotasTime"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                int month = timeRun.AddMonths(-1).Month;
                int year = timeRun.AddMonths(-1).Year;
                DateTime startMonth = new DateTime(year, month, 1);
                DateTime endMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                List<int> userIds = new List<int>();
                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    UserManager userManager = new UserManager(db);
                    userIds = userManager.GetListEmployeeHasOvertimeQuotas(CorporationType.CorporationVI, startMonth, endMonth).ToList();
                }
                try
                {
                    int numberInTask = 500;
                    int count = userIds.Count();
                    int taskNumber = count / numberInTask;
                    if (count % numberInTask != 0)
                    {
                        taskNumber++;
                    }
                    Task[] process = new Task[taskNumber];
                    List<(int, string)> err = new List<(int, string)>();
                    for (int indTask = 0; indTask < taskNumber; indTask++)
                    {
                        int i = indTask;
                        List<int> listUserInTask = new List<int>();
                        listUserInTask.AddRange(userIds.Skip(i * numberInTask).Take(numberInTask).ToList());
                        process[i] = Task.Run(async () =>
                        {
                            using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                            {
                                ConfirmAllocationManager confirmAllocationManager = new ConfirmAllocationManager(db);
                                OvertimeQuotasService overtimeQuotasService = new OvertimeQuotasService(db);
                                UserManager userManager = new UserManager(db);

                                foreach (int userId in listUserInTask)
                                {
                                    if (TOPI_GOD.Any(t => t.Item1 == userId))
                                    {
                                        OvertimeQuotas overtimeQuotas = overtimeQuotasService.GetByUserAndTime(userId, startMonth, endMonth);
                                        if (overtimeQuotas == null)
                                        {
                                            overtimeQuotasService.Create(new OvertimeQuotas
                                            {
                                                UserId = userId,
                                                EventGroupId = GroupEvent.OVER_TIME_VN_ID,
                                                Quotas = TOPI_GOD.Where(t => t.Item1 == userId).First().Item2,
                                                StartTime = startMonth,
                                                EndTime = endMonth
                                            });
                                        }
                                        continue;
                                    }
                                    try
                                    {
                                        double quotas = 0;
                                        User user = userManager.GetUserById(userId);
                                        MechanismInformationV2 result = await confirmAllocationManager.GetMechanismInformationV2ByEmail(user, startMonth, endMonth);
                                        if (result != null)
                                        {
                                            quotas = result.OvertimeQuotas;
                                        }
                                        else
                                        {
                                            quotas = -1;
                                            err.Add((userId, string.Empty));
                                        }
                                        OvertimeQuotas overtimeQuotas = overtimeQuotasService.GetByUserAndTime(userId, startMonth, endMonth);
                                        if (overtimeQuotas == null)
                                        {
                                            overtimeQuotasService.Create(new OvertimeQuotas
                                            {
                                                UserId = user.Id,
                                                EventGroupId = GroupEvent.OVER_TIME_VN_ID,
                                                Quotas = quotas,
                                                StartTime = startMonth,
                                                EndTime = endMonth
                                            });
                                        }
                                        else if (overtimeQuotas.Quotas != quotas && quotas != -1)
                                        {
                                            overtimeQuotas.Quotas = quotas;
                                            overtimeQuotasService.Update(overtimeQuotas);
                                        }
                                    }
                                    catch (Exception exx)
                                    {
                                        err.Add((userId, exx.Message));
                                        continue;
                                    }
                                }
                            }
                        });
                    }
                    Task.WaitAll(process);
                    if (err.Count > 0)
                    {
                        StringBuilder errorMessage = new StringBuilder();
                        foreach ((int userId, string message) in err)
                        {
                            errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
                        }
                        await mailProcess.ErrorService(this.GetType().Name + ".<br>UserIds: " + errorMessage.ToString());
                    }
                }
                catch (Exception e)
                {
                    string a = e.Message;
                }

                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}
