﻿//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
//using Newtonsoft.Json;
//using NPOI.HSSF.UserModel;
//using NPOI.SS.UserModel;
//using NPOI.XSSF.UserModel;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using TOCO.API;
//using TOCO.Common;
//using TOCO.Helpers;
//using TOCO.Implements;
//using TOCO.Logic;
//using TOCO.Models;
//using TOCO.Models.APIModels;

//namespace TOCO.Service
//{
//    public class SyncHrDataService : BackgroundService
//    {

//        // Trạng thái HRB là nhân viên nghỉ việc
//        //private const string HRB9000 = "HRB9000";

//        public SyncHrDataService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
//        {
//        }

//        private string GetCellValue(ICell cell)
//        {
//            var dataFormatter = new DataFormatter(CultureInfo.CurrentCulture);

//            // If this is not part of a merge cell,
//            // just get this cell's value like normal.
//            if (!cell.IsMergedCell)
//            {
//                return dataFormatter.FormatCellValue(cell);
//            }

//            // Otherwise, we need to find the value of this merged cell.
//            else
//            {
//                // Get current sheet.
//                var currentSheet = cell.Sheet;

//                // Loop through all merge regions in this sheet.
//                for (int i = 0; i < currentSheet.NumMergedRegions; i++)
//                {
//                    var mergeRegion = currentSheet.GetMergedRegion(i);

//                    // If this merged region contains this cell.
//                    if (mergeRegion.FirstRow <= cell.RowIndex && cell.RowIndex <= mergeRegion.LastRow &&
//                        mergeRegion.FirstColumn <= cell.ColumnIndex && cell.ColumnIndex <= mergeRegion.LastColumn)
//                    {
//                        // Find the top-most and left-most cell in this region.
//                        var firstRegionCell = currentSheet.GetRow(mergeRegion.FirstRow)
//                                                .GetCell(mergeRegion.FirstColumn);

//                        // And return its value.
//                        return dataFormatter.FormatCellValue(firstRegionCell);
//                    }
//                }
//                // This should never happen.
//                throw new Exception("Cannot find this cell in any merged region");
//            }
//        }

//        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
//        {
//            var cellNumberFormat = HSSFDataFormat.GetBuiltinFormat("#,##0");
//            XSSFWorkbook hssfwb;
//            using (FileStream file = new FileStream(@"Templates\EmployeeInfoThailand.xlsx", FileMode.Open, FileAccess.Read))
//            {
//                hssfwb = new XSSFWorkbook(file);
//                file.Close();
//            }
//            // Lấy sheet Template bảng công - sheet đầu tiên
//            ISheet sheet = hssfwb.GetSheetAt(0);
//            // Styling
//            ICellStyle cellStyle = hssfwb.CreateCellStyle();
//            cellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.Green.Index;
//            List<string[]> employees = new List<string[]>();
//            int curRow = 1;
//            IRow row = null;
//            while ((row = sheet.GetRow(curRow)) != null)
//            {
//                if (row.GetCell(0) == null || string.IsNullOrEmpty(GetCellValue(row.GetCell(0))))
//                {
//                    break;
//                }
//                string[] info = new string[11];
//                for (int collumIndx = 0; collumIndx < 11; collumIndx++)
//                {
//                    ICell cell = row.GetCell(collumIndx);
//                    if (cell != null)
//                    {
//                        info[collumIndx] = GetCellValue(cell);
//                    }
//                    else
//                    {
//                        info[collumIndx] = "";
//                    }
//                }
//                employees.Add(info);
//                curRow++;
//            }

//            // Created HttpClient
//            var handler = new HttpClientHandler
//            {
//                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
//            };
//            HttpClient client = new HttpClient(handler);
//            client.DefaultRequestHeaders.Add("User-Agent", "Toco App");
//            string token = "rabbit_consumer.J2PK8K5a1Mi4qrpsnNLdrbNKDfbn3f2Z1blY6l1EROBeSqGkGsF9MvoFjq3ujAbK";
//            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
//            string getUrl = "http://hrm.topica.asia:9090/api/v1.0/classes/5/objects?employee_code~={0}&limit=25&offset=0";
//            string updateUrl = "http://hrm.topica.asia:9090/api/v2.0/objects/{0}";
//            StringBuilder error = new StringBuilder();
//            foreach (string[] info in employees)
//            {
//                try
//                {
//                    // Get id object
//                    var httpGetRequestMessage = new HttpRequestMessage
//                    {
//                        Method = HttpMethod.Get,
//                        RequestUri = new Uri(string.Format(getUrl, info[0])),
//                    };

//                    int timeout = 5000;
//                    CancellationTokenSource cts = new CancellationTokenSource(timeout);

//                    HttpResponseMessage responseGet = await client.SendAsync(httpGetRequestMessage, cts.Token);

//                    string data = await responseGet.Content.ReadAsStringAsync();
//                    dynamic result = JsonConvert.DeserializeObject<dynamic>(data);
//                    string id = result.data[0].id;

//                    // edit
//                    var httpPostRequestMessage = new HttpRequestMessage
//                    {
//                        Method = HttpMethod.Put,
//                        RequestUri = new Uri(string.Format(updateUrl, id)),
//                    };
//                    dynamic body = new
//                    {
//                        id,
//                        employee_code = new
//                        {
//                            value = info[0],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_first_name = new
//                        {
//                            value = info[1],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_last_name = new
//                        {
//                            value = info[2],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_phone_number = new
//                        {
//                            value = info[3],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_work_email = new
//                        {
//                            value = info[4],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_dob = new
//                        {
//                            value = info[5],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_gender = new
//                        {
//                            value = info[6],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_email = new
//                        {
//                            value = info[7],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_current_address = new
//                        {
//                            value = info[8],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_address = new
//                        {
//                            value = info[9],
//                            status_data = "A",
//                            flag_data = "Green"
//                        },

//                        employee_work_direct_management = new
//                        {
//                            value = info[10],
//                            status_data = "A",
//                            flag_data = "Green"
//                        }

//                    };
//                    string bodyValue = JsonConvert.SerializeObject(body);
//                    httpPostRequestMessage.Content = new StringContent(bodyValue, Encoding.UTF8, "application/json");

//                    HttpResponseMessage responsePost = await client.SendAsync(httpPostRequestMessage, cts.Token);

//                    string updateData = await responsePost.Content.ReadAsStringAsync();
//                }
//                catch (Exception ex)
//                {
//                    error.AppendLine(info[0]);
//                    continue;
//                }
//                await Task.Delay(1000);
//            }





//            //dynamic newEmployee = new
//            //{

//            //}










//            //await mailProcess.RunService(this.GetType().Name);
//            ////(DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:SyncHrDataService"));
//            ////double period;
//            ////if (DateTime.Now != timeRun)
//            ////{
//            ////    timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
//            ////    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//            ////    period = period > 0 ? period : 0;
//            ////    await Task.Delay((int)period, stoppingToken);
//            ////}
//            ////while (!stoppingToken.IsCancellationRequested)
//            ////{
//            ////using (var transaction = context.Database.BeginTransaction())
//            ////{
//            ////string NAME_LEAVE_WEEKEND = "NAME_LEAVE_WEEKEND";
//            //DateTime startDate = new DateTime(2019, 4, 27).Date;
//            //DateTime endDate = new DateTime(2019, 4, 30).Date;
//            //try
//            //{
//            //    TimeKeepingManager timeKeepingManager = new TimeKeepingManager(context);
//            //    OvertimeManager overtimeManager = new OvertimeManager(context);
//            //    EventManager eventManager = new EventManager(context);
//            //    EmailManager emailManager = new EmailManager(context);
//            //    NotificationManager notificationManager = new NotificationManager(context);

//            //    List<TimeKeepingDetail> timeKeepingResult = context.TimeKeepingDetails
//            //    .Where(tkd => tkd.CheckInTime.Value.Date >= startDate)
//            //    .Where(tkd => tkd.CheckInTime.Value.Date <= endDate)
//            //    .Where(tkd => tkd.Status == StatusTimeKeepingApproval.Approved || tkd.Status == StatusTimeKeepingApproval.Done)
//            //    .Where(tkd => tkd.CheckOutTime.HasValue)
//            //    .ToList()
//            //    .Join(context.TimeKeepings, tkd => tkd.TimeKeepingId, tk => tk.Id, (tkd, tk) => new
//            //    {
//            //        Detail = tkd,
//            //        tk.ScriptId,
//            //        tk.WorkDate
//            //    })
//            //    .Where(tkd_tk => tkd_tk.ScriptId == 3 || tkd_tk.ScriptId == 5 || tkd_tk.ScriptId == 6)
//            //    //.Where(tkd_tk => tkd_tk.ScriptId == 3 ? String.Compare(tkd_tk.Detail.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMMSS), "18:30:00") >= 0
//            //    //: (
//            //    //    tkd_tk.ScriptId == 5 ? String.Compare(tkd_tk.Detail.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMMSS), "19:30:00") >= 0
//            //    //    : String.Compare(tkd_tk.Detail.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMMSS), "19:00:00") >= 0
//            //    //))
//            //    .Select(tkd_tk => tkd_tk.Detail)
//            //    .ToList();

//            //    foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingResult)
//            //    {

//            //        TimeKeeping timeKeeping = context.TimeKeepings.Where(tk => tk.Id == timeKeepingDetail.TimeKeepingId).FirstOrDefault();
//            //        DateTime workDate = timeKeeping.WorkDate;
//            //        User user = context.Users.Where(u => u.Id == timeKeeping.UserId).FirstOrDefault();
//            //        OverTime overTime = context.OverTimes.Where(ot => ot.WorkDate == workDate && ot.UserId == user.Id).FirstOrDefault();
//            //        if (overTime != null)
//            //        {
//            //            continue;
//            //        }

//            //        User manager = context.Users.Where(u => u.Id == user.ManagerId).FirstOrDefault();
//            //        Script script = context.Scripts.Where(s => s.Id == timeKeeping.ScriptId).FirstOrDefault();
//            //        // Get thời gian làm thêm giờ
//            //        (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(user, script, timeKeepingDetail.CheckInTime.Value, timeKeepingDetail.CheckOutTime.Value, false, timeKeepingDetail.Id, null);
//            //        if (hasOverTime)
//            //        {
//            //            // Xử lý ăn ca
//            //            timeKeepingManager.ShiftsEatProcess(timeKeeping.WorkDate, user, script, timeKeepingDetail.CheckOutTime);

//            //            // Kiểm tra conflict thời gian
//            //            if (overtimeManager.CheckConflictTime(user.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
//            //            {
//            //                overtimeManager.DestroyConflictOverTime(user.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null);
//            //            }
//            //            overtimeManager.CreateOverTime(user, timeKeepingDetail.TimeKeeping.WorkDate, overTimeDetail, false, string.Empty, StatusOverTime.Pending);
//            //            // Gửi mail thông báo làm thêm giờ cho quản lý
//            //            Event eventOvertime = eventManager.GetEventById(overTimeDetail.EventId);
//            //            emailManager.PushEmail(
//            //                manager.Email,
//            //                null,
//            //                null,
//            //                EmailTemplateCode.OC002,
//            //                new string[]
//            //                {
//            //                            manager.FullName,
//            //                            user.FullName,
//            //                            string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
//            //                            (string)eventOvertime.EventCode,
//            //                            workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
//            //                            overTimeDetail.StartTime.ToString(Constants.FORMAT_TIME_HHMM) + " - " + overTimeDetail.EndTime.ToString(Constants.FORMAT_TIME_HHMM),
//            //                            overTimeDetail.Note,
//            //                            AppSetting.Configuration.GetValue("MailConfig:UrlFrontEnd") + "/#/attendance/overtime"
//            //                },
//            //                manager.Nationality
//            //                );
//            //            // Gửi notify thông báo làm thêm giờ cho quản lý
//            //            notificationManager.PushNotification(
//            //                manager.Email,
//            //                null,
//            //                user.ImageUrl,
//            //                NotificationTemplateCode.ON002,
//            //                new string[]
//            //                {
//            //                            workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
//            //                            user.FullName
//            //                },
//            //                manager.Nationality);
//            //        }
//            //    }





//            //    context.SaveChanges();
//            //    //    transaction.Commit();
//            //}
//            //catch (Exception ex)
//            //{
//            //    //transaction.Rollback();
//            //}

//            //int employeeCode = 0;
//            //try
//            //{
//            //    IEnumerable<User> users = context.Users;
//            //    foreach (User user in users)
//            //    {
//            //        employeeCode = user.Id;
//            //        string email = user.Email;
//            //        bool bEdit = false;
//            //        (HrDataEmployeeAPIResponse data, HttpStatusCode status, string message) = await HrDataApi.Connection.GetEmployeeInfo(email);
//            //        if (data.Message == ResultCode.OK && data.Data.Count != 0)
//            //        {
//            //            if (data.Data[0].EmployeeWorkDate.HasValue)
//            //            {
//            //                user.ActiveDate = data.Data[0].EmployeeWorkDate.Value;
//            //                bEdit = true;
//            //            }

//            //            if (data.Data[0].Statements != null && data.Data[0].Statements.Count > 0)
//            //            {
//            //                if (data.Data[0].Statements[0].HrbStatus.Contains(HRB9000))
//            //                {
//            //                    user.Status = UserStatus.Deactive;
//            //                    user.DisableDate = data.Data[0].Statements[0].StatementValidDate ?? DateTime.Now;
//            //                    bEdit = true;
//            //                }
//            //            }
//            //        }
//            //        if (bEdit)
//            //        {
//            //            user.UpdatedAt = DateTime.Now;
//            //            context.Users.Update(user);
//            //            context.SaveChanges();
//            //        }
//            //    }
//            //    transaction.Commit();
//            //}
//            //catch (Exception ex)
//            //{
//            //    transaction.Rollback();
//            //    await mailProcess.ErrorService(this.GetType().Name + " - EmployeeCode:" + employeeCode + ". \nError: " + ex.Message + "\n" + ex.StackTrace);
//            //}
//            //}
//            //if (type != ScheduleType.NONE)
//            //{
//            //    timeRun = AddScheduleTime(timeRun, type);
//            //    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//            //    period = period > 0 ? period : 0;
//            //    await Task.Delay((int)period, stoppingToken);
//            //}
//            //else
//            //{
//            //    break;
//            //}
//        }
//    }
//}
