﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Service
{
    public struct SyncConfig
    {
        public bool SyncHrb { get; set; }
        public bool SyncCorporation { get; set; }
        public bool SyncHeadOfOrg { get; set; }
        public bool SyncLeaveType { get; set; }
        public bool SyncOtherInfo { get; set; }
    }


    public class SyncHKTDataService : BackgroundService
    {
        private const string HRB = "HRB";
        private const string KH200 = "KH200";

        private const string ZA = "ZA";
        private const string ZB = "ZB";
        private const string ZG = "ZG";
        private const string ZC = "ZC";
        private const string ZF = "ZF";
        private const string FB = "FB";
        private const string FA = "FA";

        private readonly string THX = AppSetting.Configuration.GetValue("HKT:THX");

        private const string HRB9000 = "HRB9000";
        private const string HRB1200 = "HRB1200";

        private readonly string[] corporationVN = AppSetting.Configuration.GetArrayValue<string>("HKT:Corporations:Vi");

        private readonly string[] corporationTL = AppSetting.Configuration.GetArrayValue<string>("HKT:Corporations:Th");

        private readonly string[] productionsTL = AppSetting.Configuration.GetArrayValue<string>("HKT:Productions:Th");

        private readonly string[] departmentsTL = AppSetting.Configuration.GetArrayValue<string>("HKT:Deparments:Th");

        private readonly int[] ignoreSyncCorporation = AppSetting.Configuration.GetArrayValue<int>("HKT:IgnoreSyncCorporation");

        private readonly SyncConfig syncConfigVi = AppSetting.Configuration.GetValue<SyncConfig>("HKT:SyncConfig:Vi");

        private readonly SyncConfig syncConfigTh = AppSetting.Configuration.GetValue<SyncConfig>("HKT:SyncConfig:Th");

        private readonly int[] debugIds = AppSetting.Configuration.GetArrayValue<int>("HKT:DebugIds");

        public SyncHKTDataService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:SyncHKTData"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                List<(string, string)> err = new List<(string, string)>();

                try
                {
                    string path = string.Format(@"Exported\LogSyncHKT\statement_{0}{1}{2}{3}{4}{5}.csv",
                        DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour,
                        DateTime.Now.Minute, DateTime.Now.Second);
                    int skip = 0;
                    int take = 500;
                    string[] header = { "Id", "CB", "CDT", "CH", "COM", "DV", "HRB",
                        "HRC", "KH200", "KN", "LO", "NG", "PB", "PC900", "PT", "SA100",
                        "SA200", "SA300", "SP", "StatementCode", "StatementCreatedDate",
                        "StatementEmployeeCode", "StatementEmployeePernr", "StatementExpiredDate",
                        "StatementType", "StatementValidDate" };
                    using (StreamWriter sw = new StreamWriter(path, false))
                    {
                        sw.NewLine = "\n";
                        WriteLogHeader(sw, header);
                        (int ManagerIdDefault, string CalendarType, string Nationality) dataDefaultNTL = (20136, "NTL", "Việt Nam");
                        (int ManagerIdDefault, string CalendarType, string Nationality) dataDefaultVN = (20136, "VI", "Việt Nam");
                        (int ManagerIdDefault, string CalendarType, string Nationality) dataDefaultTL = (20136, "TH", "Thái Lan");

                        DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                        while (true)
                        {
                            (List<HKTStatementInfo>, HttpStatusCode, string) data = await HKTDataAPI.Connection.GetListStatementsAsync(skip, take);

                            if (data.Item2 != HttpStatusCode.OK)
                            {
                                err.Add((string.Format("STATEMENT_LAST skip: {0} - take: {1}", skip, take), string.Format("{0}({1})", data.Item2.ToString(), data.Item3)));
                            }
                            else
                            {
                                foreach (HKTStatementInfo statementInfo in data.Item1)
                                {
                                    (int ManagerIdDefault, string CalendarType, string Nationality) dataDefault = default;
                                    string corporationType = string.Empty;
                                    SyncConfig syncConfig = default;
                                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                                    {
                                        DateTime statementValidDate = DateTime.ParseExact(statementInfo.StatementValidDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, CultureInfo.InvariantCulture);
                                        int userId = int.Parse(statementInfo.StatementEmployeeCode);
                                        if (debugIds.Count() != 0 && !debugIds.Contains(userId))
                                        {
                                            continue;
                                        }
                                        string corporationCode = statementInfo.PT.Split("-")[0].Trim();
                                        if (corporationVN.Contains(corporationCode))
                                        {
                                            if ((!string.IsNullOrEmpty(statementInfo.SP) && productionsTL.Contains(statementInfo.SP))
                                                || (!string.IsNullOrEmpty(statementInfo.DV) && departmentsTL.Contains(statementInfo.DV)))
                                            {
                                                dataDefault = dataDefaultNTL;
                                            }
                                            else
                                            {
                                                dataDefault = dataDefaultVN;
                                            }
                                            corporationType = CorporationType.CorporationVI;
                                            syncConfig = syncConfigVi;
                                        }
                                        else if (corporationTL.Contains(corporationCode))
                                        {
                                            dataDefault = dataDefaultTL;
                                            corporationType = CorporationType.CorporationTH;
                                            syncConfig = syncConfigTh;
                                        }

                                        UserManager userManager = new UserManager(db);
                                        UserGroupsService userGroupsService = new UserGroupsService(db);
                                        ScriptHistoryService scriptHistoryService = new ScriptHistoryService(db);
                                        //ScriptManager scriptManager = new ScriptManager(db);
                                        //EstimateOwnerService estimateOwnerService = new EstimateOwnerService(db);
                                        CorporationHistoryService corporationHistoryService = new CorporationHistoryService(db);
                                        ChangeManagerInfoService changeManagerInfoService = new ChangeManagerInfoService(db);
                                        HrbHistoryService hrbHistoryService = new HrbHistoryService(db);
                                        LeaveCalendarTypeHistoryService leaveCalendarTypeHistoryService = new LeaveCalendarTypeHistoryService(db);
                                        using (var transaction = db.Database.BeginTransaction())
                                        {
                                            try
                                            {
                                                // Kiểu tra xem user có trên checkin không?
                                                // Nếu chưa có trên checkin thì thêm mới
                                                // Kiểm tra xem user có làm việc trong tháng không validDate?
                                                User user = userManager.GetUserById(userId);
                                                if (user == null
                                                    && (!statementInfo.StatementType.Contains(ZG) || statementValidDate >= startMonth)
                                                    && !string.IsNullOrEmpty(corporationType))
                                                {
                                                    // Thêm vào bảng User
                                                    (HKTEmployeeInfo, HttpStatusCode, string) userInfo = await HKTDataAPI.Connection.GetEmployeeInfoAsync(userId);
                                                    if (userInfo.Item2 != HttpStatusCode.OK)
                                                    {
                                                        err.Add((string.Format("EMPLOYEE_BY_USER {0}", statementInfo.StatementEmployeeCode), string.Format("{0}({1})", data.Item2.ToString(), data.Item3)));
                                                        continue;
                                                    }
                                                    string managerName = statementInfo.KH200;
                                                    int? headOfOrgId = null;
                                                    if (!string.IsNullOrEmpty(managerName))
                                                    {
                                                        User headOfOrg = userManager.GetUserByUserName(managerName);
                                                        if (headOfOrg != null)
                                                        {
                                                            headOfOrgId = headOfOrg.Id;
                                                            List<UserGroup> headOfOrgPermisions = userGroupsService.GetByUserId(headOfOrgId.Value);
                                                            // Nếu quản lý chưa có quyền thì thêm quyền
                                                            if (headOfOrgPermisions.Find(h => h.GroupId == Constants.User.Groups.Manager) == null)
                                                            {
                                                                UserGroup managerPermision = new UserGroup
                                                                {
                                                                    UserId = headOfOrgId.Value,
                                                                    GroupId = Constants.User.Groups.Manager
                                                                };
                                                                userGroupsService.Create(managerPermision);
                                                            }
                                                        }
                                                    }
                                                    user = new User
                                                    {
                                                        Id = userId,
                                                        Email = userInfo.Item1.EmployeeWorkEmail.ToLower(),
                                                        FullName = userInfo.Item1.EmployeeFirstName + " " + userInfo.Item1.EmployeeLastName,
                                                        Status = UserStatus.Active,
                                                        ManagerId = headOfOrgId ?? dataDefault.ManagerIdDefault,
                                                        EmployeeCode = userId,
                                                        //CalendarType = dataDefault.CalendarType,
                                                        Nationality = dataDefault.Nationality,
                                                        HeadOfOrgUnitId = headOfOrgId,
                                                        Department = statementInfo.DV,
                                                        Corporation = corporationCode
                                                    };
                                                    if (userManager.Create(user))
                                                    {
                                                        // Tạo bản ghi lịch sử quản lý 
                                                        ChangeManagerInfo changeManagerInfo = new ChangeManagerInfo
                                                        {
                                                            UserId = user.Id,
                                                            OldManagerId = headOfOrgId ?? dataDefault.ManagerIdDefault,
                                                            OldManagerStatus = StatusChangeManagerInfo.Approved,
                                                            NewManagerId = headOfOrgId ?? dataDefault.ManagerIdDefault,
                                                            NewManagerStatus = StatusChangeManagerInfo.Approved,
                                                            Status = StatusChangeManagerInfo.Approved,
                                                            IsManual = 2
                                                        };
                                                        changeManagerInfoService.Create(changeManagerInfo);

                                                        // Them vao bang user group quyen employee
                                                        UserGroup employeePermision = new UserGroup
                                                        {
                                                            UserId = userId,
                                                            GroupId = Constants.User.Groups.Employee
                                                        };
                                                        UserGroup allocationPermision = new UserGroup
                                                        {
                                                            UserId = userId,
                                                            GroupId = Constants.User.Groups.Allocation
                                                        };
                                                        userGroupsService.Create(employeePermision);
                                                        userGroupsService.Create(allocationPermision);

                                                        // Lay danh sach thay doi quyet dinh
                                                        (List<HKTStatementInfo>, HttpStatusCode, string) result = await HKTDataAPI.Connection.GetStatementInfoAsync(userId);
                                                        if (result.Item2 != HttpStatusCode.OK)
                                                        {
                                                            err.Add((string.Format("STATEMENT_BY_USER {0}", statementInfo.StatementEmployeeCode), string.Format("{0}({1})", data.Item2.ToString(), data.Item3)));
                                                            continue;
                                                        }
                                                        // Lấy danh sách ScriptHistory thêm mới
                                                        List<Tuple<DateTime, DateTime, int>> scriptHistories = GetScriptHistories(result.Item1, corporationType);
                                                        // Tạo danh sách CorporationHistory thêm mới
                                                        List<Tuple<DateTime, DateTime, string, string>> corporationHistories = GetCorporationHistories(result.Item1);
                                                        // Tạo danh sách HrbHistory thêm mới
                                                        List<Tuple<DateTime, DateTime, HrbStatus>> hrbHistories = GetHrbHistories(result.Item1);
                                                        // Thêm script history vào db
                                                        foreach (Tuple<DateTime, DateTime, int> history in scriptHistories)
                                                        {
                                                            // Insert them ban ghi lịch sử kịch bản moi
                                                            ScriptHistory scriptHistory = new ScriptHistory
                                                            {
                                                                UserId = userId,
                                                                ScriptId = history.Item3,
                                                                EffectiveDate = history.Item1,
                                                                ExpirationDate = history.Item2
                                                            };
                                                            scriptHistoryService.Create(scriptHistory);
                                                        }
                                                        // Insert thêm bản ghi lịch sử pháp nhân mới vào db
                                                        foreach (Tuple<DateTime, DateTime, string, string> history in corporationHistories)
                                                        {
                                                            // Insert them ban ghi lịch sử pháp nhân moi
                                                            CorporationHistory corporationHistory = new CorporationHistory
                                                            {
                                                                UserId = userId,
                                                                Corporation = history.Item4,
                                                                HcmPersionalId = history.Item3,
                                                                EffectiveDate = history.Item1,
                                                                ExpirationDate = history.Item2
                                                            };
                                                            corporationHistoryService.Create(corporationHistory);
                                                        }
                                                        // Thêm bản ghi lịch sử hrb vào db
                                                        foreach (Tuple<DateTime, DateTime, HrbStatus> history in hrbHistories)
                                                        {
                                                            // Insert them ban ghi lịch sử hrb moi
                                                            HrbHistory hrbHistory = new HrbHistory
                                                            {
                                                                UserId = userId,
                                                                HrbStatus = history.Item3,
                                                                EffectiveDate = history.Item1,
                                                                ExpirationDate = history.Item2
                                                            };
                                                            hrbHistoryService.Create(hrbHistory);
                                                        }

                                                        // Thêm vào bảng LeaveCalenderTypeHistories
                                                        LeaveCalendarTypeHistory leaveCalendarTypeHistory = new LeaveCalendarTypeHistory
                                                        {
                                                            UserId = userId,
                                                            Code = dataDefault.CalendarType,
                                                            EffectiveDate = hrbHistories.First().Item1,
                                                            ExpirationDate = DateTime.MaxValue.Date
                                                        };
                                                        leaveCalendarTypeHistoryService.Create(leaveCalendarTypeHistory);
                                                    }
                                                    WriteLogBody(sw, header, statementInfo, "Insert User");
                                                }
                                                // Nếu có trên checkin rồi thì kiểm tra và sửa lại cho đúng với HCM
                                                // Thuộc pháp nhân Việt hoặc Thái
                                                else if (user != null && !string.IsNullOrEmpty(corporationType))
                                                {
                                                    // Check thay đổi thông tin user
                                                    #region Change HeadOfOrg
                                                    // Thay đổi head of org
                                                    bool bEditHeadOfOrg = false;
                                                    if (syncConfig.SyncHeadOfOrg)
                                                    {
                                                        // Check thay đổi thông tin HeadOfOrg
                                                        string managerName = statementInfo.KH200;
                                                        int? headOfOrgId = null;
                                                        if (!string.IsNullOrEmpty(managerName))
                                                        {
                                                            headOfOrgId = userManager.GetUserByUserName(managerName)?.Id ?? null;
                                                        }
                                                        // Nếu thay đổi HeadOfOrg
                                                        if (headOfOrgId.HasValue && headOfOrgId != user.HeadOfOrgUnitId)
                                                        {
                                                            // Thay đổi HeadOfOrg của nhân sự
                                                            // Thay đổi quản lý trực tiếp về HeadOfOrg và tạo đơn đổi quản lý
                                                            user.HeadOfOrgUnitId = headOfOrgId;
                                                            user.ManagerId = headOfOrgId.Value;
                                                            userManager.Update(user);
                                                            // Hủy đơn đang đợi duyệt
                                                            ChangeManagerInfo pendingChangeManager = changeManagerInfoService.GetMyPendingChangeManagerInfo(user);
                                                            if (pendingChangeManager != null)
                                                            {
                                                                changeManagerInfoService.DestroyChangeManagerInfo(pendingChangeManager.Id);
                                                            }
                                                            ChangeManagerInfo changeManagerInfo = new ChangeManagerInfo
                                                            {
                                                                UserId = user.Id,
                                                                OldManagerId = headOfOrgId.Value,
                                                                OldManagerStatus = StatusChangeManagerInfo.Approved,
                                                                NewManagerId = headOfOrgId.Value,
                                                                NewManagerStatus = StatusChangeManagerInfo.Approved,
                                                                Status = StatusChangeManagerInfo.Approved,
                                                                IsManual = 2
                                                            };
                                                            changeManagerInfoService.Create(changeManagerInfo);
                                                            bEditHeadOfOrg = true;
                                                        }
                                                    }
                                                    #endregion

                                                    #region Change Leave Type
                                                    // Thay đổi lịch làm việc
                                                    bool bEditLeaveType = false;
                                                    if (syncConfig.SyncLeaveType)
                                                    {
                                                        // Lấy ra lịch nghỉ phép hiện tại
                                                        LeaveCalendarTypeHistory leaveCalendarTypeHistoryNow = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, statementValidDate.Date);
                                                        if (leaveCalendarTypeHistoryNow != null && dataDefault.CalendarType != leaveCalendarTypeHistoryNow.Code)
                                                        {
                                                            // Xóa bỏ các lịch sử trong tương lai
                                                            List<LeaveCalendarTypeHistory> futureHistories = leaveCalendarTypeHistoryService.GetFutureHistories(userId, statementValidDate.Date).ToList();
                                                            foreach (LeaveCalendarTypeHistory history in futureHistories)
                                                            {
                                                                leaveCalendarTypeHistoryService.Delete(history.Id);
                                                            }
                                                            leaveCalendarTypeHistoryNow.ExpirationDate = statementValidDate.Date.AddDays(-1);
                                                            if (leaveCalendarTypeHistoryNow.EffectiveDate < leaveCalendarTypeHistoryNow.ExpirationDate)
                                                            {
                                                                leaveCalendarTypeHistoryService.Update(leaveCalendarTypeHistoryNow);
                                                            }
                                                            else
                                                            {
                                                                leaveCalendarTypeHistoryService.Delete(leaveCalendarTypeHistoryNow.Id);
                                                            }

                                                            // Thêm vào bảng LeaveCalenderTypeHistories
                                                            LeaveCalendarTypeHistory newLeaveCalendarTypeHistory = new LeaveCalendarTypeHistory
                                                            {
                                                                UserId = userId,
                                                                Code = dataDefault.CalendarType,
                                                                EffectiveDate = statementValidDate.Date,
                                                                ExpirationDate = DateTime.MaxValue.Date
                                                            };
                                                            leaveCalendarTypeHistoryService.Create(newLeaveCalendarTypeHistory);
                                                            bEditLeaveType = true;
                                                        }
                                                    }
                                                    #endregion

                                                    #region Change Other Info
                                                    // Thay đổi thông tin khác
                                                    bool bEditOtherInfo = false;
                                                    if (syncConfig.SyncOtherInfo)
                                                    {
                                                        if (user.Department != statementInfo.DV || user.Corporation != corporationCode)
                                                        {
                                                            // Thay đổi thông tin đơn vị, phòng ban của nhân sự
                                                            user.Department = statementInfo.DV;
                                                            user.Corporation = corporationCode;
                                                            userManager.Update(user);
                                                            bEditOtherInfo = true;
                                                        }
                                                    }
                                                    #endregion

                                                    (List<HKTStatementInfo>, HttpStatusCode, string) result = await HKTDataAPI.Connection.GetStatementInfoAsync(userId);
                                                    if (result.Item2 != HttpStatusCode.OK)
                                                    {
                                                        err.Add((string.Format("STATEMENT_BY_USER {0}", statementInfo.StatementEmployeeCode), string.Format("{0}({1})", data.Item2.ToString(), data.Item3)));
                                                        continue;
                                                    }

                                                    #region Check thay đổi Corporation
                                                    bool bEditCorporation = false;
                                                    if (syncConfig.SyncCorporation)
                                                    {
                                                        // Check thay đổi thông tin thời gian pháp nhân hiện tại
                                                        List<CorporationHistory> arrCorporationHistory = corporationHistoryService.GetAllByUser(userId).ToList();
                                                        // Khai báo mảng corporation history sẽ add và db
                                                        List<CorporationHistory> arrCorporationHistoryAdd = new List<CorporationHistory>();
                                                        if (!ignoreSyncCorporation.Contains(userId))
                                                        {
                                                            // Tạo danh sách CorporationHistory thêm mới
                                                            List<Tuple<DateTime, DateTime, string, string>> corporationHistories = GetCorporationHistories(result.Item1);
                                                            // Duyệt hết các khoảng thời gian của lịch sử pháp nhân
                                                            foreach (Tuple<DateTime, DateTime, string, string> history in corporationHistories)
                                                            {
                                                                // Insert them ban ghi lịch sử pháp nhân moi
                                                                CorporationHistory corporationHistory = new CorporationHistory
                                                                {
                                                                    UserId = userId,
                                                                    Corporation = history.Item4,
                                                                    HcmPersionalId = history.Item3,
                                                                    EffectiveDate = history.Item1,
                                                                    ExpirationDate = history.Item2
                                                                };
                                                                arrCorporationHistoryAdd.Add(corporationHistory);
                                                            }
                                                            bEditCorporation = arrCorporationHistory.Count != arrCorporationHistoryAdd.Count;
                                                            if (!bEditCorporation)
                                                            {
                                                                for (int indx = 0; indx < arrCorporationHistoryAdd.Count; indx++)
                                                                {
                                                                    if (!arrCorporationHistory[indx].CompareTo(arrCorporationHistoryAdd[indx]))
                                                                    {
                                                                        bEditCorporation = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (bEditCorporation)
                                                        {
                                                            // Remove hết corporation history của nhân sự trong bảng
                                                            foreach (CorporationHistory item in arrCorporationHistory)
                                                            {
                                                                corporationHistoryService.Delete(item.Id);
                                                            }
                                                            // Add lại các item trong arrScriptHistoryAdd vào script history
                                                            foreach (CorporationHistory item in arrCorporationHistoryAdd)
                                                            {
                                                                corporationHistoryService.Create(item);
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                    #region Check thay đổi HRB
                                                    bool bEditHrb = false;
                                                    if (syncConfig.SyncHrb)
                                                    {
                                                        // Check thay đổi thông tin thời gian hrn
                                                        List<HrbHistory> arrHrbHistory = hrbHistoryService.GetAllByUser(userId).ToList();
                                                        // Tạo danh sách HrbHistory thêm mới
                                                        List<Tuple<DateTime, DateTime, HrbStatus>> hrbHistories = GetHrbHistories(result.Item1);
                                                        // Khai báo mảng hrb history sẽ add và db
                                                        List<HrbHistory> arrHrbHistoryAdd = new List<HrbHistory>();
                                                        // Duyệt hết các khoảng thời gian của lịch sử hrb
                                                        foreach (Tuple<DateTime, DateTime, HrbStatus> history in hrbHistories)
                                                        {
                                                            // Insert them ban ghi lịch sử Hrb moi
                                                            HrbHistory hrbHistory = new HrbHistory
                                                            {
                                                                UserId = userId,
                                                                HrbStatus = history.Item3,
                                                                EffectiveDate = history.Item1,
                                                                ExpirationDate = history.Item2
                                                            };
                                                            arrHrbHistoryAdd.Add(hrbHistory);
                                                        }
                                                        bEditHrb = arrHrbHistory.Count != arrHrbHistoryAdd.Count;
                                                        if (!bEditHrb)
                                                        {
                                                            for (int indx = 0; indx < arrHrbHistory.Count; indx++)
                                                            {
                                                                if (!arrHrbHistory[indx].CompareTo(arrHrbHistoryAdd[indx]))
                                                                {
                                                                    bEditHrb = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if (bEditHrb)
                                                        {
                                                            // Remove hết hrb history của nhân sự trong bảng
                                                            foreach (HrbHistory item in arrHrbHistory)
                                                            {
                                                                hrbHistoryService.Delete(item.Id);
                                                            }
                                                            // Add lại các item trong arrHrbHistoryAdd vào hrb history
                                                            foreach (HrbHistory item in arrHrbHistoryAdd)
                                                            {
                                                                hrbHistoryService.Create(item);
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                    if (bEditHrb || bEditCorporation || bEditHeadOfOrg || bEditOtherInfo)
                                                    {
                                                        string changeContent = bEditCorporation ? " Corporation" : string.Empty;
                                                        changeContent += bEditHrb ? " Hrb" : string.Empty;
                                                        changeContent += bEditHeadOfOrg ? " HeadOfOrg" : string.Empty;
                                                        changeContent += bEditLeaveType ? " LeaveType" : string.Empty;
                                                        changeContent += bEditOtherInfo ? " OtherInfo" : string.Empty;
                                                        WriteLogBody(sw, header, statementInfo, string.Format("Update User {0}", changeContent));
                                                    }

                                                }
                                                transaction.Commit();
                                            }
                                            catch (Exception ex)
                                            {
                                                err.Add((statementInfo.StatementEmployeeCode, ex.Message + ex.StackTrace));
                                                transaction.Rollback();
                                            }
                                        }
                                    }
                                }
                                if (data.Item1.Count < take)
                                {
                                    break;
                                }
                                skip += take;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    err.Add(("EXCEPTION ALL: ", string.Format("Message: {0} Trace: {1}", e.Message, e.StackTrace)));

                }

                if (err.Count > 0)
                {
                    StringBuilder errorMessage = new StringBuilder();
                    foreach ((string userId, string message) in err)
                    {
                        errorMessage.AppendLine(string.Format("{0} - {1} <br>", userId, message));
                    }
                    await mailProcess.ErrorService(this.GetType().Name + ".<br>UserIds: " + errorMessage.ToString());
                }

                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }

        private List<Tuple<DateTime, DateTime, HrbStatus>> GetHrbHistories(List<HKTStatementInfo> statements)
        {
            // Tạo danh sách HrbHistory thêm mới
            List<Tuple<DateTime, DateTime, HrbStatus>> hrbHistories = new List<Tuple<DateTime, DateTime, HrbStatus>>();

            // Sắp xếp theo validDate tăng dần.
            foreach (HKTStatementInfo statementInfoItem in statements.OrderBy(st => st.StatementValidDate))
            {
                // Lấy ra ngày hiệu lực
                DateTime validDate = DateTime.ParseExact(statementInfoItem.StatementValidDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, CultureInfo.InvariantCulture);

                // Xử lý khi tìm thấy 1 quyết định tuyển mới (ZA/ZB)
                if (statementInfoItem.StatementType.Contains(ZA) || statementInfoItem.StatementType.Contains(ZB))
                {
                    // Thêm hrb history vào tuple
                    // Kiểm tra tuple cuối cùng nếu đang không có ngày kết thúc
                    // => thêm statement kết thúc
                    Tuple<DateTime, DateTime, HrbStatus> hrbTuple = hrbHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    if (hrbTuple != null)
                    {
                        hrbHistories.Remove(hrbTuple);
                        hrbTuple = new Tuple<DateTime, DateTime, HrbStatus>(hrbTuple.Item1, validDate.AddDays(-1), hrbTuple.Item3);
                        hrbHistories.Add(hrbTuple);
                    }
                    // Thêm tuple ở Pháp nhân mới
                    hrbHistories.Add(new Tuple<DateTime, DateTime, HrbStatus>(validDate, DateTime.MaxValue.Date, HrbStatus.Active));
                }
                // Xử lý khi tìm thấy 1 quyết định nghỉ việc (ZG)
                else if (statementInfoItem.StatementType.Contains(ZG))
                {
                    // Lấy ra Hrb Tuple
                    Tuple<DateTime, DateTime, HrbStatus> hrbTuple = hrbHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    if (hrbTuple != null)
                    {
                        hrbHistories.Remove(hrbTuple);
                        hrbTuple = new Tuple<DateTime, DateTime, HrbStatus>(hrbTuple.Item1, validDate.AddDays(-1), hrbTuple.Item3);
                        hrbHistories.Add(hrbTuple);
                    }
                }
                // Xử lý khi thấy 1 qđ điều chuyển sang THX , tạm hoãn hđ
                else if ((statementInfoItem.HRB?.Contains(HRB1200) ?? true) && ((statementInfoItem.StatementType.Contains(ZC) && statementInfoItem.CDT.Contains(THX)) || (statementInfoItem.StatementType.Contains(ZF) && statementInfoItem.StatementName.Contains(FB))))
                {
                    // Lấy ra Hrb Tuple
                    Tuple<DateTime, DateTime, HrbStatus> hrbTuple = hrbHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    if (hrbTuple != null && hrbTuple.Item3 == HrbStatus.Active)
                    {
                        hrbHistories.Remove(hrbTuple);
                        // Kết thúc khoảng thời gian trạng thái active
                        hrbTuple = new Tuple<DateTime, DateTime, HrbStatus>(hrbTuple.Item1, validDate.AddDays(-1), hrbTuple.Item3);
                        hrbHistories.Add(hrbTuple);
                        // Tạo tuple thêm khoảng thời gian trạng thái pending
                        hrbHistories.Add(new Tuple<DateTime, DateTime, HrbStatus>(validDate, DateTime.MaxValue.Date, HrbStatus.Pending));
                    }
                }
                // Xử lý khi thấy 1 qđ điều chuyển về phòng ban khác THX, gia hạn hđ
                else if (!(statementInfoItem.HRB?.Contains(HRB1200) ?? false) && !(statementInfoItem.HRB?.Contains(HRB9000) ?? false) && ((statementInfoItem.StatementType.Contains(ZC) && !statementInfoItem.CDT.Contains(THX)) || (statementInfoItem.StatementType.Contains(ZF) && statementInfoItem.StatementName.Contains(FA))))
                {
                    // Lấy ra Hrb Tuple
                    Tuple<DateTime, DateTime, HrbStatus> hrbTuple = hrbHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    if (hrbTuple != null && hrbTuple.Item3 == HrbStatus.Pending)
                    {
                        hrbHistories.Remove(hrbTuple);
                        // Kết thúc khoảng thời gian trạng thái pending
                        hrbTuple = new Tuple<DateTime, DateTime, HrbStatus>(hrbTuple.Item1, validDate.AddDays(-1), hrbTuple.Item3);
                        hrbHistories.Add(hrbTuple);
                        // Tạo tuple thêm khoảng thời gian trạng thái active
                        hrbHistories.Add(new Tuple<DateTime, DateTime, HrbStatus>(validDate, DateTime.MaxValue.Date, HrbStatus.Active));
                    }
                }

            }
            return hrbHistories;
        }

        private List<Tuple<DateTime, DateTime, string, string>> GetCorporationHistories(List<HKTStatementInfo> statements)
        {
            // Tạo danh sách CorporationHistory thêm mới
            List<Tuple<DateTime, DateTime, string, string>> corporationHistories = new List<Tuple<DateTime, DateTime, string, string>>();

            // Sắp xếp theo validDate tăng dần.
            foreach (HKTStatementInfo statementInfoItem in statements.OrderBy(st => st.StatementValidDate))
            {
                // Lấy ra ngày hiệu lực
                DateTime validDate = DateTime.ParseExact(statementInfoItem.StatementValidDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, CultureInfo.InvariantCulture);

                // Xử lý khi tìm thấy 1 quyết định tuyển mới (ZA/ZB)
                if (statementInfoItem.StatementType.Contains(ZA) || statementInfoItem.StatementType.Contains(ZB))
                {
                    // Thêm corporation history vào tuple
                    // Kiểm tra tuple cuối cùng nếu đang không có ngày kết thúc
                    // => thêm statement kết thúc
                    Tuple<DateTime, DateTime, string, string> corporationTuple = corporationHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    if (corporationTuple != null)
                    {
                        corporationHistories.Remove(corporationTuple);
                        corporationTuple = new Tuple<DateTime, DateTime, string, string>(corporationTuple.Item1, validDate.AddDays(-1), corporationTuple.Item3, corporationTuple.Item4);
                        corporationHistories.Add(corporationTuple);
                    }
                    // Thêm tuple ở Pháp nhân mới
                    corporationHistories.Add(new Tuple<DateTime, DateTime, string, string>(validDate, DateTime.MaxValue.Date, statementInfoItem.StatementEmployeePernr, statementInfoItem.PT));
                }
                // Xử lý khi tìm thấy 1 quyết định nghỉ việc (ZG)
                else if (statementInfoItem.StatementType.Contains(ZG))
                {
                    // Thêm corporation history vào tuple
                    Tuple<DateTime, DateTime, string, string> corporationTuple = corporationHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    if (corporationTuple != null)
                    {
                        corporationHistories.Remove(corporationTuple);
                        corporationTuple = new Tuple<DateTime, DateTime, string, string>(corporationTuple.Item1, validDate.AddDays(-1), corporationTuple.Item3, corporationTuple.Item4);
                        corporationHistories.Add(corporationTuple);
                    }
                }
            }
            return corporationHistories;
        }

        private List<Tuple<DateTime, DateTime, int>> GetScriptHistories(List<HKTStatementInfo> statements, string corporationType)
        {
            // Tạo danh sách ScriptHistory thêm mới
            List<Tuple<DateTime, DateTime, int>> scriptHistories = new List<Tuple<DateTime, DateTime, int>>();

            // Sắp xếp theo validDate tăng dần.
            foreach (HKTStatementInfo statementInfoItem in statements.OrderBy(st => st.StatementValidDate))
            {
                // Lấy ra ngày hiệu lực
                DateTime validDate = DateTime.ParseExact(statementInfoItem.StatementValidDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, CultureInfo.InvariantCulture);

                // Xử lý khi tìm thấy 1 quyết định tuyển mới (ZA/ZB)
                if (statementInfoItem.StatementType.Contains(ZA) || statementInfoItem.StatementType.Contains(ZB))
                {
                    // Thêm scripthistory vào tuple
                    // Tìm lịch sử có chứa ngày hiệu lực của quyết định tiếp nhận
                    Tuple<DateTime, DateTime, int> history = scriptHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    // Nếu chưa có => thêm mới
                    // Nếu có rồi thì bỏ qua và duyệt tiếp
                    if (history == null)
                    {
                        int scriptIdDefault = (int)(corporationType == CorporationType.CorporationVI ? (statementInfoItem.CB.Contains("LX") ? ScriptId.D : ScriptId.B) : ScriptId.T);
                        scriptHistories.Add(new Tuple<DateTime, DateTime, int>(validDate, DateTime.MaxValue.Date, scriptIdDefault));
                    }
                }
                // Xử lý khi tìm thấy 1 quyết định nghỉ việc (ZG)
                else if (statementInfoItem.StatementType.Contains(ZG))
                {
                    // Thêm script history vào tuple
                    // Tìm lịch sử có chứa ngày hiệu lực của quyết định nghỉ việc
                    Tuple<DateTime, DateTime, int> history = scriptHistories.Where(h => h.Item1 <= validDate && h.Item2 >= validDate).FirstOrDefault();
                    // Nếu có => thêm mới ngày hết hạn
                    // Nếu chưa có thì bỏ qua và duyệt tiếp
                    if (history != null)
                    {
                        scriptHistories.Remove(history);
                        int scriptIdDefault = (int)(corporationType == CorporationType.CorporationVI ? (statementInfoItem.CB.Contains("LX") ? ScriptId.D : ScriptId.B) : ScriptId.T);
                        scriptHistories.Add(new Tuple<DateTime, DateTime, int>(history.Item1, validDate.AddDays(-1), scriptIdDefault));
                    }
                }
            }
            return scriptHistories;
        }

        private void WriteLogBody(StreamWriter sw, string[] header, HKTStatementInfo statementInfo, string type)
        {
            string formatText = "\"{0}\"";
            StringBuilder line = new StringBuilder();
            foreach (string collumn in header)
            {
                string text = (string)statementInfo.GetType().GetProperty(collumn).GetValue(statementInfo);
                line.Append(string.Format(formatText, text));
                line.Append(",");
            }
            line.Append(string.Format(formatText, type));
            sw.WriteLine(line.ToString());
        }

        private void WriteLogHeader(StreamWriter sw, string[] header)
        {
            string formatText = "\"{0}\"";
            StringBuilder line = new StringBuilder();
            foreach (string collumn in header)
            {
                line.Append(string.Format(formatText, collumn));
                line.Append(",");
            }
            line.Append(string.Format(formatText, "Type"));
            sw.WriteLine(line.ToString());

        }
    }
}