﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Service
{
    public enum ScheduleType : int
    {
        SECOND = 0,
        MINUTE = 1,
        HOUR = 2,
        DAY = 3,
        MONTH = 4,
        YEAR = 5,
        NONE = 6
    }

    /// Copyright(c) .NET Foundation.Licensed under the Apache License, Version 2.0.
    /// <summary>
    /// Base class for implementing a long running <see cref="IHostedService"/>.
    /// </summary>
    public abstract class BackgroundService : IHostedService, IDisposable
    {
        protected readonly IServiceScope scope;
        private Task _executingTask;
        private readonly CancellationTokenSource _stoppingCts =
                                                       new CancellationTokenSource();
        //protected readonly IMailService mailService;
        protected readonly IMailProcess mailProcess;
        //protected readonly ApplicationDbContext context;
        protected readonly IFireBaseMesageService notificationService;

        public BackgroundService(IServiceScopeFactory scopeFactory)
        {
            scope = scopeFactory.CreateScope();
            mailProcess = scope.ServiceProvider.GetRequiredService<IMailProcess>();
            //context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            notificationService = scope.ServiceProvider.GetRequiredService<IFireBaseMesageService>();
        }

        protected abstract Task ExecuteAsync(CancellationToken stoppingToken);

        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            // Store the task we're executing
            _executingTask = ExecuteAsync(_stoppingCts.Token);
            // If the task is completed then return it,
            // this will bubble cancellation and failure to the caller
            if (_executingTask.IsCompleted)
            {
                return _executingTask;
            }
            // Otherwise it's running
            return Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop called without start
            if (_executingTask == null)
            {
                return;
            }

            try
            {
                // Signal cancellation to the executing method
                _stoppingCts.Cancel();
            }
            finally
            {
                // Wait until the task completes or the stop token triggers
                await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite,
                                                              cancellationToken));
            }
        }

        public virtual void Dispose()
        {
            _stoppingCts.Cancel();
        }

        public (DateTime RunTime, ScheduleType Type) GetRunTime(string schedule)
        {
            // * * * * * *
            string[] array = schedule.Trim().Split(" ");
            CultureInfo customCulture = new System.Globalization.CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);

            int year = array[0] != "*" ? int.Parse(array[0]) : DateTime.Now.Year;
            int month = array[1] != "*" ? int.Parse(array[1]) : DateTime.Now.Month;
            int day = array[2] != "*" ? int.Parse(array[2]) : DateTime.Now.Day;
            int hour = array[3] != "*" ? int.Parse(array[3]) : DateTime.Now.Hour;
            int min = array[4] != "*" ? int.Parse(array[4]) : DateTime.Now.Minute;
            int second = array[5] != "*" ? int.Parse(array[5]) : DateTime.Now.Second;

            DateTime runTime = new DateTime(year, month, day, hour, min, second);

            ScheduleType type = ScheduleType.NONE;
            foreach (string item in array)
            {
                if (item == "*")
                {
                    type--;
                }
                else
                {
                    break;
                }
            }
            return (runTime, type);
        }

        public DateTime AddScheduleTime(DateTime startTime, ScheduleType type)
        {
            DateTime nextTime = startTime;
            switch (type)
            {
                case ScheduleType.SECOND:
                    nextTime = nextTime.AddSeconds(1);
                    break;
                case ScheduleType.MINUTE:
                    nextTime = nextTime.AddMinutes(1);
                    break;
                case ScheduleType.HOUR:
                    nextTime = nextTime.AddHours(1);
                    break;
                case ScheduleType.DAY:
                    nextTime = nextTime.AddDays(1);
                    break;
                case ScheduleType.MONTH:
                    nextTime = nextTime.AddMonths(1);
                    break;
                case ScheduleType.YEAR:
                    nextTime = nextTime.AddYears(1);
                    break;
            }
            return nextTime;
        }

        public double GetPeriod(string schedule)
        {
            double period = 0;
            // * * * * * *
            string[] array = schedule.Trim().Split(" ");
            CultureInfo customCulture = new System.Globalization.CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);

            int year = array[0] != "*" ? int.Parse(array[0]) : DateTime.Now.Year;
            int month = array[1] != "*" ? int.Parse(array[1]) : DateTime.Now.Month;
            int day = array[2] != "*" ? int.Parse(array[2]) : DateTime.Now.Day;
            int hour = array[3] != "*" ? int.Parse(array[3]) : DateTime.Now.Hour;
            int min = array[4] != "*" ? int.Parse(array[4]) : DateTime.Now.Minute;
            int second = array[5] != "*" ? int.Parse(array[5]) : DateTime.Now.Second;

            DateTime runTime = new DateTime(year, month, day, hour, min, second);

            ScheduleType type = ScheduleType.NONE;
            foreach (string item in array)
            {
                if (item == "*")
                {
                    type--;
                }
                else
                {
                    break;
                }
            }

            if (DateTime.Now != runTime)
            {
                runTime = DateTime.Now < runTime ? runTime : AddScheduleTime(runTime, type);
                period = runTime.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;

            }
            return period;
        }

        protected DateTime GetRunTime(bool isNext, int numberBeforeDay, int hour, int minute, int second, string leaveType)
        {
            using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
            {
                LeaveDateService leaveDateService = new LeaveDateService(db);
                DateTime current = isNext ? DateTime.Now.AddMonths(1) : DateTime.Now;

                DateTime timeRun = new DateTime(current.Year, current.Month, DateTime.DaysInMonth(current.Year, current.Month), hour, minute, second);
                int subDate = 0;
                while (true)
                {
                    LeaveDate leaveDateToday = leaveDateService.GetByDateAndType(timeRun.Date, leaveType);
                    if (leaveDateToday != null)
                    {
                        timeRun = timeRun.AddDays(-1);
                    }
                    else
                    {
                        if (subDate == numberBeforeDay)
                        {
                            break;
                        }
                        else
                        {
                            subDate += 1;
                            timeRun = timeRun.AddDays(-1);
                        }
                    }
                }
                return timeRun;
            }
        }

        protected async Task Delay(long delay)
        {
            while (delay > 0)
            {
                var currentDelay = delay > int.MaxValue ? int.MaxValue : (int)delay;
                await Task.Delay(currentDelay);
                delay -= currentDelay;
            }
        }
    }
}
