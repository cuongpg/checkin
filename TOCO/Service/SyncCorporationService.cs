﻿//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
//using Newtonsoft.Json;
//using NPOI.HSSF.UserModel;
//using NPOI.SS.UserModel;
//using NPOI.XSSF.UserModel;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using TOCO.API;
//using TOCO.Common;
//using TOCO.Helpers;
//using TOCO.Implements;
//using TOCO.Logic;
//using TOCO.Models;
//using TOCO.Models.APIModels;

//namespace TOCO.Service
//{
//    public class SyncCorporationService : BackgroundService
//    {
//        private const string ZA = "ZA";
//        private const string ZB = "ZB";
//        private const string ZG = "ZG";

//        private readonly string[] STATEMENT_TYPES = { ZA, ZB, ZG };

//        public SyncCorporationService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
//        {
//        }

//        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
//        {
//            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:SyncCorporationService"));
//            double period;
//            if (DateTime.Now != timeRun)
//            {
//                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
//                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//                period = period > 0 ? period : 0;
//                await Delay((long)period);
//            }
//            while (!stoppingToken.IsCancellationRequested)
//            {
//                await mailProcess.RunService(this.GetType().Name);
//                List<int> userIds = new List<int> {};
//                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
//                {
//                    UserManager userManager = new UserManager(db);
//                    userIds = userManager.GetAll();
//                }
//                try
//                {
//                    int numberInTask = 500;
//                    int count = userIds.Count();
//                    int taskNumber = count / numberInTask;
//                    if (count % numberInTask != 0)
//                    {
//                        taskNumber++;
//                    }

//                    Task[] process = new Task[taskNumber];
//                    List<(int, string)> err = new List<(int, string)>();
//                    for (int indTask = 0; indTask < taskNumber; indTask++)
//                    {
//                        int i = indTask;
//                        List<int> listUserInTask = new List<int>();
//                        listUserInTask.AddRange(userIds.Skip(i * numberInTask).Take(numberInTask).ToList());
//                        process[i] = Task.Run(async () =>
//                        {
//                            using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
//                            {
//                                CorporationHistoryService corporationHistoryService = new CorporationHistoryService(db);
//                                UserManager userManager = new UserManager(db);

//                                CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
//                                foreach (int userId in listUserInTask)
//                                {
//                                    try
//                                    {
//                                        User user = userManager.GetUserById(userId);
//                                        (List<HKTStatementInfo> statementInfos, HttpStatusCode statementStatus, string statementMessage) = await HKTDataAPI.Connection.GetStatementInfoAsync(user.EmployeeCode);
//                                        if (statementInfos?.Count > 0 && statementStatus == HttpStatusCode.OK)
//                                        {
//                                            // Lấy danh sách quyết định liên quan tới Pháp nhân, sắp xếp từ bé đến lớn theo thời gian hiệu lực
//                                            List<HKTStatementInfo> changeCorporationStatements = statementInfos.Where(s => STATEMENT_TYPES.Contains(s.StatementType.Split("-")[0].Trim())).OrderBy(s => s.StatementValidDate).ToList();
//                                            // Lấy ra các cặp giá trị (tuyển mới, nghỉ việc) của nhân sự tại các pháp nhân
//                                            List<Tuple<HKTStatementInfo, HKTStatementInfo>> tuples = new List<Tuple<HKTStatementInfo, HKTStatementInfo>>();
//                                            foreach (HKTStatementInfo statement in changeCorporationStatements)
//                                            {
//                                                // Nếu quyết định là tuyển mới hoặc tái tuyển, thêm mới 1 tuple
//                                                if (statement.StatementType.StartsWith(ZA) || statement.StatementType.StartsWith(ZB))
//                                                {
//                                                    // Kiểm tra tuple cuối cùng nếu đang không có statement kết thúc
//                                                    // => thêm statement kết thúc
//                                                    Tuple<HKTStatementInfo, HKTStatementInfo> tuple = tuples.LastOrDefault();
//                                                    if (tuple != null && tuple.Item2 == null)
//                                                    {
//                                                        tuples.Remove(tuple);
//                                                        tuple = new Tuple<HKTStatementInfo, HKTStatementInfo>(tuple.Item1, statement);
//                                                        tuples.Add(tuple);
//                                                    }
//                                                    // Thêm tuple ở Pháp nhân mới
//                                                    tuples.Add(new Tuple<HKTStatementInfo, HKTStatementInfo>(statement, null));
//                                                }
//                                                // Nếu quyết định là nghỉ việc, lấy ra tuple cuối cùng và thêm statement kết thúc vào tuple đó
//                                                else if (statement.StatementType.StartsWith(ZG))
//                                                {
//                                                    Tuple<HKTStatementInfo, HKTStatementInfo> tuple = tuples.LastOrDefault();
//                                                    if (tuple != null)
//                                                    {
//                                                        tuples.Remove(tuple);
//                                                        tuple = new Tuple<HKTStatementInfo, HKTStatementInfo>(tuple.Item1, statement);
//                                                        tuples.Add(tuple);
//                                                    }
//                                                }
//                                            }

//                                            List<CorporationHistory> corporationHistorys = corporationHistoryService.GetAllByUser(user.Id).ToList();

//                                            // Duyệt danh sách tuple
//                                            foreach (Tuple<HKTStatementInfo, HKTStatementInfo> tuple in tuples)
//                                            {
//                                                int employeeCode = int.Parse(tuple.Item1.StatementEmployeeCode);
//                                                string statementEmployeePernr = tuple.Item1.StatementEmployeePernr;
//                                                string corporation = tuple.Item1.PT;
//                                                DateTime effectiveDate = DateTime.ParseExact(tuple.Item1.StatementValidDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, customCulture);
//                                                DateTime expirationDate = tuple.Item2 != null ? DateTime.ParseExact(tuple.Item2.StatementValidDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, customCulture).AddDays(-1) : DateTime.MaxValue.Date;

//                                                CorporationHistory corporationHistory = corporationHistorys
//                                                    .Where(c => c.Corporation == corporation)
//                                                    .Where(c => c.HcmPersionalId == statementEmployeePernr)
//                                                    .Where(c => !(c.ExpirationDate < effectiveDate || c.EffectiveDate > expirationDate))
//                                                    .FirstOrDefault();

//                                                if (corporationHistory == null)
//                                                {
//                                                    corporationHistoryService.Create(new CorporationHistory
//                                                    {
//                                                        UserId = employeeCode,
//                                                        Corporation = corporation,
//                                                        HcmPersionalId = statementEmployeePernr,
//                                                        EffectiveDate = effectiveDate,
//                                                        ExpirationDate = expirationDate
//                                                    });
//                                                }
//                                                else if (corporationHistory.EffectiveDate != effectiveDate || corporationHistory.ExpirationDate != expirationDate)
//                                                {
//                                                    corporationHistory.ExpirationDate = expirationDate;
//                                                    corporationHistory.EffectiveDate = effectiveDate;
//                                                    corporationHistoryService.Update(corporationHistory);
//                                                    corporationHistorys.Remove(corporationHistory);
//                                                }
//                                                else if (corporationHistory.EffectiveDate == effectiveDate && corporationHistory.ExpirationDate == expirationDate)
//                                                {
//                                                    corporationHistorys.Remove(corporationHistory);
//                                                }
//                                            }
//                                            foreach (CorporationHistory corporationHistory in corporationHistorys)
//                                            {
//                                                corporationHistoryService.Delete(corporationHistory.Id);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            err.Add((userId, string.Format("({0}){1}", statementStatus, statementMessage)));
//                                            continue;
//                                        }
//                                    }
//                                    catch (Exception ex)
//                                    {
//                                        err.Add((userId, ex.Message));
//                                        continue;
//                                    }
//                                }
//                            }
//                        });
//                    }
//                    Task.WaitAll(process);
//                    if (err.Count > 0)
//                    {
//                        StringBuilder errorMessage = new StringBuilder();
//                        foreach ((int userId, string message) in err)
//                        {
//                            errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
//                        }
//                        await mailProcess.ErrorService(this.GetType().Name + ".<br>UserIds: " + errorMessage.ToString());
//                    }
//                }
//                catch (Exception e)
//                {
//                    string a = e.Message;
//                }

//                if (type != ScheduleType.NONE)
//                {
//                    timeRun = AddScheduleTime(timeRun, type);
//                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//                    period = period > 0 ? period : 0;
//                    await Delay((long)period);
//                }
//                else
//                {
//                    break;
//                }
//            }
//        }
//    }
//}
