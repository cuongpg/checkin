﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class SendNotificationService : BackgroundService
    {
        //private const int TURN = 100;

        public SendNotificationService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            IFireBaseMesageService fireBaseMesageService = scope.ServiceProvider.GetRequiredService<IFireBaseMesageService>();
            await mailProcess.RunService(this.GetType().Name);
            while (!stoppingToken.IsCancellationRequested)
            {
                int id = 0;
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        UserManager userManager = new UserManager(db);
                        TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                        OvertimeManager overtimeManager = new OvertimeManager(db);
                        ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);
                        List<Notification> notifications = notificationManager.GetNotificationsByStatus(NotificationStatus.UNSENT);

                        foreach (Notification notification in notifications)
                        {
                            id = notification.Id;
                            int badge = notificationManager.CountNotificationsByStatusAndEmail(NotificationStatus.WAITING_SEE, notification.Email);
                            User user = userManager.GetUserByEmail(notification.Email);
                            if (user != null)
                            {
                                int numberPendingTimeKeepingForManager = timeKeepingManager.CountPendingTimeKeeping(user, true);
                                int numberPendingApprovalForManager = approvalRecordManager.CountPendingApprovalRecord(user, true, false, false);
                                int numberPendingDestroyApprovalForManager = approvalRecordManager.CountPendingApprovalRecord(user, true, false, true);
                                int numberPendingOverTimeForManager = overtimeManager.CountPendingOverTime(user, true, false);
                                SDocHeader header = new SDocHeader
                                {
                                    XUserMail = notification.Email
                                };
                                (int countProcess, HttpStatusCode status, string message) = await SDocAPI.Connection.CountPendingProcessesAsync(header);
                                badge += numberPendingTimeKeepingForManager
                                    + numberPendingApprovalForManager
                                    + numberPendingDestroyApprovalForManager
                                    + numberPendingOverTimeForManager
                                    + countProcess;
                            }
                            List<string> listTokens = notificationManager.GetFirebaseTokensByEmail(notification.Email);
                            bool bOk = fireBaseMesageService.SendPushNotification(listTokens, notification, badge);
                            if (bOk)
                            {
                                notification.Status = NotificationStatus.WAITING_SEE;
                            }
                            else
                            {
                                notification.Status = NotificationStatus.ERROR;
                            }
                            notificationManager.UpdateStatus(notification);
                        }
                    }
                }
                catch (Exception ex)
                {
                    await mailProcess.ErrorService(this.GetType().Name + " - Id:" + id + ". <br>Error: " + ex.Message + "<br>" + ex.StackTrace);
                }
                int period = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:SendNotification"));
                await Task.Delay(period);
            }
        }
    }
}
