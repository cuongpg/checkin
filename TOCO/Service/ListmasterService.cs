﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Service
{
    public class ListmasterServicecs : BackgroundService
    {
        public ListmasterServicecs(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:RunListmasterService"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    try
                    {
                        EstimateOwnerService estimateOwnerService = new EstimateOwnerService(db);
                        CorporationService corporationService = new CorporationService(db);
                        UserService userService = new UserService(db);
                        CommonItemService commonItemService = new CommonItemService(db);
                        // Đồng bộ chủ dư toán
                        await EstimateOwnerSyncDataAsync(estimateOwnerService, userService);
                        // Đồng bộ pháp nhân
                        // await CorporationSyncDataAsync(corporationService, userService);
                        // Đồng bộ Common List
                        await CommonSyncDataAsync(commonItemService);
                    }
                    catch (Exception ex)
                    {
                        await mailProcess.ErrorService(this.GetType().Name + ". <br>Error: " + ex.Message + "<br>" + ex.StackTrace);
                    }
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }

        private async Task CommonSyncDataAsync(CommonItemService commonItemService)
        {
            (ListmasterCommonResult result, HttpStatusCode status, string message) = await ListmasterAPI.Connection.GetListCommons();
            if (status == HttpStatusCode.OK)
            {
                foreach (ListmasterCommon commonItem in result.Data)
                {
                    CommonItem item = commonItemService.GetById(commonItem.Id);
                    if (item == null)
                    {
                        item = STHelper.ConvertObject<CommonItem, ListmasterCommon>(commonItem);
                        commonItemService.Create(item);
                    }
                    else
                    {
                        if (item.Name != commonItem.Name
                            || item.Description != item.Description
                            || item.ParentId != item.ParentId
                            || item.DescriptionEn != commonItem.DescriptionEn
                            || item.Active != commonItem.Active)
                        {

                            item = STHelper.ConvertObject<CommonItem, ListmasterCommon>(commonItem);
                            commonItemService.Update(item);
                        }
                    }
                }
            }
        }

        private async Task EstimateOwnerSyncDataAsync(EstimateOwnerService estimateOwnerService, UserService userService)
        {
            int perPage = int.Parse(AppSetting.Configuration.GetValue("Meta:Perpage"));
            int currentPage = 1;
            bool bContinue = true;
            while (bContinue)
            {
                (ListmasterMSTResult result, HttpStatusCode status, string message) = await ListmasterAPI.Connection.GetListEstimateOwners(perPage, currentPage);
                if (status == HttpStatusCode.OK)
                {
                    foreach (ListmasterMST listmasterEstimateOwner in result.Data)
                    {
                        EstimateOwner estimateOwner = estimateOwnerService.GetById(listmasterEstimateOwner.Tpdata.SmdmId);
                        if (estimateOwner == null)
                        {
                            estimateOwner = new EstimateOwner
                            {
                                Id = listmasterEstimateOwner.Tpdata.SmdmId,
                                CompleteCode = listmasterEstimateOwner.Tpdata.TpOrgCodeNew,
                                ParentId = listmasterEstimateOwner.Tpdata.TpParentOrgId,
                                Level = listmasterEstimateOwner.Tpdata.TpLevel,
                                Active = listmasterEstimateOwner.Tpdata.TpCurrentStatus,
                                DivisionNameVn = listmasterEstimateOwner.Tpdata.TpOrgNameVi,
                                DivisionNameEn = listmasterEstimateOwner.Tpdata.TpOrgNameEn
                            };
                            estimateOwnerService.Create(estimateOwner);
                        }
                        else
                        {
                            if (estimateOwner.CompleteCode != listmasterEstimateOwner.Tpdata.TpOrgCodeNew
                                || estimateOwner.Level != listmasterEstimateOwner.Tpdata.TpLevel
                                || estimateOwner.ParentId != listmasterEstimateOwner.Tpdata.TpParentOrgId
                                || estimateOwner.DivisionNameEn != listmasterEstimateOwner.Tpdata.TpOrgNameEn
                                || estimateOwner.DivisionNameVn != listmasterEstimateOwner.Tpdata.TpOrgNameVi
                                || estimateOwner.Active != listmasterEstimateOwner.Tpdata.TpCurrentStatus)
                            {
                                estimateOwner.CompleteCode = listmasterEstimateOwner.Tpdata.TpOrgCodeNew;
                                estimateOwner.ParentId = listmasterEstimateOwner.Tpdata.TpParentOrgId;
                                estimateOwner.Level = listmasterEstimateOwner.Tpdata.TpLevel;
                                estimateOwner.Active = listmasterEstimateOwner.Tpdata.TpCurrentStatus;
                                estimateOwner.DivisionNameVn = listmasterEstimateOwner.Tpdata.TpOrgNameVi;
                                estimateOwner.DivisionNameEn = listmasterEstimateOwner.Tpdata.TpOrgNameEn;
                              
                                estimateOwnerService.Update(estimateOwner);
                            }
                        }
                    }
                    if (result.Meta.Pagination.CurrentPage < result.Meta.Pagination.TotalPages)
                    {
                        currentPage++;
                    }
                    else
                    {
                        bContinue = false;
                    }
                }
                else
                {
                    bContinue = false;
                }
            }
        }

        private async Task CorporationSyncDataAsync(CorporationService corporationService, UserService userService)
        {
            int perPage = int.Parse(AppSetting.Configuration.GetValue("Meta:Perpage"));
            int currentPage = 1;
            bool bContinue = true;
            while (bContinue)
            {
                (ListmasterCorporationResult result, HttpStatusCode status, string message) = await ListmasterAPI.Connection.GetListCorporations(perPage, currentPage);
                if (status == HttpStatusCode.OK)
                {
                    foreach (ListmasterCorporation listmasterCorporaton in result.Items)
                    {
                        Corporation corporation = corporationService.GetById(listmasterCorporaton.Id);
                        if (corporation == null)
                        {
                            corporation = STHelper.ConvertObject<Corporation, ListmasterCorporation>(listmasterCorporaton);
                            corporationService.Create(corporation);
                        }
                        else
                        {
                            if (corporation.CompleteCode != listmasterCorporaton.CompleteCode
                                || corporation.ShortCode != listmasterCorporaton.ShortCode
                                || corporation.ParentId != listmasterCorporaton.ParentId
                                || corporation.Level != listmasterCorporaton.Level
                                || corporation.NameEn != listmasterCorporaton.NameEn
                                || corporation.NameNative != listmasterCorporaton.NameNative
                                || corporation.TaxCode != listmasterCorporaton.TaxCode
                                || corporation.Status != listmasterCorporaton.Status
                                || (int)corporation.Active != listmasterCorporaton.Active)
                            {
                                if (corporation.ShortCode != listmasterCorporaton.ShortCode)
                                {
                                    userService.UpdateCorporation(corporation.ShortCode, listmasterCorporaton.ShortCode);
                                }
                                corporation = STHelper.ConvertObject<Corporation, ListmasterCorporation>(listmasterCorporaton);
                                corporation.Active = (CorporationStatus)listmasterCorporaton.Active;
                                corporationService.Update(corporation);
                            }
                        }
                    }
                    if (result.Meta.CurrentPage < result.Meta.PageCount)
                    {
                        currentPage++;
                    }
                    else
                    {
                        bContinue = false;
                    }
                }
                else
                {
                    bContinue = false;
                }
            }
        }
    }
}