﻿//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading;
//using System.Threading.Tasks;
//using TOCO.API;
//using TOCO.Common;
//using TOCO.Implements;
//using TOCO.Logic;
//using TOCO.Models;
//using TOCO.Models.APIModels;

//namespace TOCO.Service
//{
//    public abstract class GenerateQuotasService : BackgroundService
//    {
//        public const string HRB1100 = "HRB1100";
//        public const int PAID_LEAVE_REASON_ID_VI = 2;
//        public const int PAID_LEAVE_REASON_ID_TH = 12;

//        public GenerateQuotasService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
//        {
//        }

//        protected async Task GenerateQuotasAsync(DateTime timeRun, ScheduleType type, string corporationType, CancellationToken stoppingToken)
//        {
//            int month = DateTime.Now.AddMonths(-1).Month;
//            int year = DateTime.Now.AddMonths(-1).Year;
//            DateTime startDate = new DateTime(year, month, 1).Date;
//            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
//            double period;
//            if (DateTime.Now != timeRun)
//            {
//                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
//                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//                period = period > 0 ? period : 0;
//                await Delay((long)period);
//            }
//            while (!stoppingToken.IsCancellationRequested)
//            {
//                await mailProcess.RunService(this.GetType().Name);
//                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
//                {
//                    using (var transaction = db.Database.BeginTransaction())
//                    {
//                        try
//                        {
//                            UserManager userManager = new UserManager(db);
//                            DateTime effectiveDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
//                            (DateTime TimeRun, ScheduleType Type) expiration = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:vi:ExpirationDate"));
//                            DateTime expirationDate = AddScheduleTime(expiration.TimeRun, expiration.Type).Date;
//                            IEnumerable<int> userIds = userManager.GetListUserIncludeQuotas(corporationType, effectiveDate);

//                            foreach (int userId in userIds)
//                            {
//                                User user = userManager.GetUserById(userId);
//                                (HrDataEmployeeAPIResponse data, HttpStatusCode status, string message) = await HrDataApi.Connection.GetEmployeeInfo(user.Email);
//                                if (data.Message == ResultCode.OK && data.Data.Count != 0)
//                                {
//                                    if (data != null && data.Data[0].Statements != null && data.Data[0].Statements.Count > 0)
//                                    {
//                                        if (data.Data[0].Statements[0].HrbStatus.Contains(HRB1100))
//                                        {
//                                            AbsenderQuotas absenderQuotas = new AbsenderQuotas
//                                            {
//                                                UserId = userId,
//                                                EffectiveDate = effectiveDate,
//                                                ExpirationDate = expirationDate,
//                                                Number = 1,
//                                                ReasonTypeId = corporationType == CorporationType.CorporationTH ? PAID_LEAVE_REASON_ID_TH : PAID_LEAVE_REASON_ID_VI,
//                                                CreatedAt = DateTime.Now,
//                                                UpdatedAt = DateTime.Now
//                                            };
//                                            db.AbsenderQuotas.Add(absenderQuotas);
//                                        }
//                                    }
//                                }
//                            }
//                            db.SaveChanges();
//                            transaction.Commit();
//                        }
//                        catch (Exception ex)
//                        {
//                            await mailProcess.ErrorService(this.GetType().Name + ". \nError: " + ex.Message + "\n" + ex.StackTrace);
//                            transaction.Rollback();
//                            throw ex;
//                        }
//                    }
//                }
//                if (type != ScheduleType.NONE)
//                {
//                    timeRun = AddScheduleTime(timeRun, type);
//                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//                    period = period > 0 ? period : 0;
//                    await Delay((long)period);
//                }
//                else
//                {
//                    break;
//                }
//            }
//        }
//    }

//    public class GenerateQuotasServiceVi : GenerateQuotasService
//    {
//        public GenerateQuotasServiceVi(IServiceScopeFactory scopeFactory) : base(scopeFactory)
//        {
//        }

//        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
//        {
//            // Run something
//            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:vi:GenerateQuotas"));
//            await GenerateQuotasAsync(timeRun, type, CorporationType.CorporationVI, stoppingToken);
//        }
//    }

//    public class GenerateQuotasServiceTh : GenerateQuotasService
//    {
//        public GenerateQuotasServiceTh(IServiceScopeFactory scopeFactory) : base(scopeFactory)
//        {
//        }

//        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
//        {
//            // Run something
//            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:th:GenerateQuotas"));
//            await GenerateQuotasAsync(timeRun, type, CorporationType.CorporationTH, stoppingToken);
//        }
//    }

//}

