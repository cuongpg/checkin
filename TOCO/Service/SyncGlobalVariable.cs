﻿
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Service
{
    public class SyncGlobalVariable : BackgroundService
    {
        public SyncGlobalVariable(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:SyncGlobalVariable"));
            double period;
            //if (DateTime.Now != timeRun)
            //{
            //    timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
            //    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
            //    period = period > 0 ? period : 0;
            //    await Delay((long)period);
            //}
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        ConfirmAllocationManager confirmAllocationManager = new ConfirmAllocationManager(db);
                        MechanismService mechanismService = new MechanismService(db);
                        List<Unit> projectUnits = new List<Unit>();
                        List<Unit> orgUnits = new List<Unit>();
                        // Đồng bộ OrgUnit
                        (List<OrgUnitResult>, HttpStatusCode, string) orgs = await HKTDataAPI.Connection.GetListOrgUnit(null);
                        foreach (OrgUnitResult org in orgs.Item1)
                        {
                            //Tính theo cơ chế đơn vị
                            orgUnits.Add(new Unit
                            {
                                Id = org.OBJID,
                                Code = org.FULL_CODE,
                                ShortCode = org.SHORT,
                                Name = org.STEXT,
                                StartDate = string.Empty,
                                EndDate = string.Empty,
                                CH = org.HILFM,
                                HeadOfUnit = org.HEAD_P,
                                DepartmentType = Constants.UNIT_TYPE_ORG,
                                Level = org.LEVEL,
                                ParentId = org.PARID

                            });
                        }
                        (List<ProjectInfo>, HttpStatusCode, string) projects = await HKTDataAPI.Connection.GetListProjectInfomations();
                        foreach (ProjectInfo project in projects.Item1)
                        {
                            //Lấy cơ chế dự án
                            Mechanism departmentMechanism = mechanismService.GetByHCMCode(project.PROJTYPE);
                            string shortCode = project.PROJID.Split(".")[0];
                            string projectType = orgUnits.Any(og => og.ShortCode == shortCode) ? Constants.UNIT_TYPE_ORG : Constants.UNIT_TYPE_SMALL_PROJECT;
                            projectUnits.Add(new Unit
                            {
                                Id = project.PSPID,
                                Code = project.PROJID,
                                ShortCode = shortCode,
                                Name = project.SNAME,
                                StartDate = project.BEGDA,
                                EndDate = project.ENDDA,
                                CH = departmentMechanism?.MechanismName,
                                HeadOfUnit = project.PERNR,
                                DepartmentType = projectType
                            });
                        }
                        GlobalVariable.Data.Projects.Clear();
                        GlobalVariable.Data.OrgUnits.Clear();
                        GlobalVariable.Data.Projects.AddRange(projectUnits);
                        GlobalVariable.Data.OrgUnits.AddRange(orgUnits);
                    }
                }
                catch (Exception e)
                {
                    string a = e.Message;
                }

                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}
