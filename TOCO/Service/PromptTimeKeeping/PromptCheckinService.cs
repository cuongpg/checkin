﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class PromptCheckinService : BackgroundService
    {
        public PromptCheckinService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptCheckin"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                DateTime workDate = DateTime.Now.Date;
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        LeaveCalendarTypeHistoryService leaveCalendarTypeHistoryService = new LeaveCalendarTypeHistoryService(db);
                        // Lấy danh sách user chưa chấm công
                        List<User> users = (from u in db.Users
                                            join sh in db.ScriptHistorys
                                            on u.Id equals sh.UserId
                                            where sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now
                                            join hrb in db.HrbHistories
                                            on u.Id equals hrb.UserId
                                            where hrb.EffectiveDate <= DateTime.Now && hrb.ExpirationDate >= DateTime.Now && hrb.HrbStatus == HrbStatus.Active
                                            join t in db.TimeKeepings.Where(tk => tk.WorkDate == workDate)
                                            on u.Id equals t.UserId
                                            into a
                                            from b in a.DefaultIfEmpty()
                                            where b == null
                                            select u
                                                ).Distinct().ToList();
                        int userError = 0;
                        foreach (User user in users)
                        {
                            userError = user.EmployeeCode;
                            try
                            {
                                // Notify nhắc checkin
                                ScriptHistory scriptHistory = db.ScriptHistorys
                                    .Where(sh => sh.UserId == user.Id && sh.ExpirationDate >= workDate && sh.EffectiveDate <= workDate)
                                    .OrderByDescending(sh => sh.EffectiveDate)
                                    .FirstOrDefault();

                                Script script = db.Scripts.Where(s => s.Id == scriptHistory.ScriptId).FirstOrDefault();
                                if (script.NoNeedTimeKeeping)
                                {
                                    continue;
                                }
                                // neu user co ngay nghi le và hôm nay là ngày nghỉ lễ
                                // neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
                                // Không cần kiểm tra thông tin checkin checkout đúng giờ
                                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
                                LeaveDate leaveDate = db.LeaveDates.Where(ld => ld.Type == leaveCalendarTypeHistory.Code && ld.Date == workDate).FirstOrDefault();
                                bool checkDone = leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK));
                                if (!checkDone)
                                {
                                    ApprovalRecord approvalRecord = db.ApprovalRecords
                                       .Where(ar => ar.UserId == user.Id)
                                       .Where(ar => ar.StartTime <= DateTime.Now.Date && ar.EndTime >= DateTime.Now.Date && ar.IsMorning)
                                       .Where(ar => ar.Status != StatusApprovalRecord.Rejected && ar.StatusDestroy != StatusApprovalRecord.Approved)
                                       .FirstOrDefault();
                                    checkDone = approvalRecord != null;
                                }
                                if (!checkDone)
                                {
                                    notificationManager.PushNotification(
                                        user.Email,
                                        null,
                                        null,
                                        NotificationTemplateCode.ON019,
                                        new string[]
                                        {
                                        },
                                        user.Nationality);
                                }
                            }
                            catch (Exception ex)
                            {
                                await mailProcess.ErrorService(this.GetType().Name + "\nUserError: " + userError + ". \nError: " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    await mailProcess.ErrorService(this.GetType().Name + ". \nError: " + ex.Message + "\n" + ex.StackTrace);
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

