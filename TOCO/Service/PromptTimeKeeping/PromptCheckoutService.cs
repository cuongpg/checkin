﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class PromptCheckoutService : BackgroundService
    {
        public PromptCheckoutService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptCheckout"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                DateTime workDate = DateTime.Now.Date;
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        // Notify nhắc checkout
                        // Lấy danh sách user có chấm công và chưa checkout
                        IQueryable<User> users = from u in db.Users
                                                 join sh in db.ScriptHistorys
                                                 on u.Id equals sh.UserId
                                                 where sh.EffectiveDate <= DateTime.Now && sh.ExpirationDate >= DateTime.Now
                                                 join hrb in db.HrbHistories
                                                 on u.Id equals hrb.UserId
                                                 where hrb.EffectiveDate <= DateTime.Now && hrb.ExpirationDate >= DateTime.Now && hrb.HrbStatus == HrbStatus.Active
                                                 join t in db.TimeKeepings on u.Id equals t.UserId
                                                 where t.WorkDate == workDate
                                                 join tkd in db.TimeKeepingDetails on t.Id equals tkd.TimeKeepingId
                                                 where !tkd.CheckOutTime.HasValue
                                                 select u;

                        foreach (User user in users.Distinct().ToList())
                        {
                            notificationManager.PushNotification(
                                user.Email,
                                null,
                                null,
                                NotificationTemplateCode.ON021,
                                new string[]
                                {
                                },
                                user.Nationality);
                        }
                    }
                }
                catch (Exception ex)
                {
                    await mailProcess.ErrorService(this.GetType().Name + ". \nError: " + ex.Message + "\n" + ex.StackTrace);
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}
