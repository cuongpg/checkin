﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Service
{
    public class PromptManagerApprovalCutoffTh : PromptTimeKeeping
    {
        public PromptManagerApprovalCutoffTh(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int numberPromptManagerCheck = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptManagerCheck:Number"));

            int hour = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptManagerCheck:Hour"));
            int minute = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptManagerCheck:Minute"));
            int second = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptManagerCheck:Second"));

            // Run something
            double period;
            DateTime timeRun = GetRunTime(false, numberPromptManagerCheck, hour, minute, second, LeaveCalendarType.TH);
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : GetRunTime(true, numberPromptManagerCheck, hour, minute, second, LeaveCalendarType.TH);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                List<(string, string)> err = new List<(string, string)>();
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        EmailManager emailManager = new EmailManager(db);
                        UserManager userManager = new UserManager(db);
                        ScriptManager scriptManager = new ScriptManager(db);
                        TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                        OvertimeManager overtimeManager = new OvertimeManager(db);
                        ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);
                        EventManager eventManager = new EventManager(db);
                        EmployeeTypeService employeeTypeService = new EmployeeTypeService(db);
                        HrbHistoryService hrbHistoryService = new HrbHistoryService(db);
                        // Lấy danh sách quản lý còn đơn chưa duyệt
                        List<int> managers = userManager.GetAllManagerByCorporationType(CorporationType.CorporationTH);
                        // Lấy danh sách user
                        List<User> users = userManager.GetUsersByCorporationType(CorporationType.CorporationTH);
                        // Lấy danh sách bảng công cho nhân sự
                        List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)> employeeTimeSheet = new List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)>();

                        DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                        DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
                        // Lấy định mức ngày công trong tháng
                        foreach (User user in users)
                        {
                            (double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat) employeeData = (0, 0, 0, 0, 0);

                            // Lấy danh sách kịch bản trong tháng của nhân sự
                            List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, startDate, endDate);
                            foreach (ScriptHistory scriptHistory in scriptHistorys)
                            {
                                Script script = scriptManager.GetScriptById(scriptHistory.ScriptId);
                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                                DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                                DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;
                                // Lọc theo lịch sử hrb
                                // Lấy danh sách hrbHistory trong thời gian làm việc
                                List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                                foreach (HrbHistory hrbHistory in hrbHistorys)
                                {
                                    if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                    {
                                        continue;
                                    }
                                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                    DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                    DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;

                                    (List<ApprovalDateResult> approvalDates, double totalNumberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);

                                    double totalMorningSeconds = 0;
                                    double totalAffternoonSeconds = 0;
                                    int totalCheckinLate = 0;
                                    int totalCheckoutEarly = 0;
                                    double totalNumberOfWorkDay = 0;
                                    List<TimeKeepingResult> timeKeepingsInMonth = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endDate).Item1, user, script, startWorkDate, endWorkDate, "vi", ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                    (List<(double TotalSeconds, string Description, int Percentage)> totalOverTime, List<(double seconds, string workDate, List<(string, string, double)>)> overTimes) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(CorporationType.CorporationTH, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, "vi");

                                    if (script.NoNeedTimeKeeping)
                                    {
                                        // Lấy định mức ngày công trong tháng
                                        int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                        employeeData.NumberWork += quotasTimeKeepingInMonth;
                                    }
                                    else
                                    {
                                        employeeData.NumberWork += totalNumberOfLeaveDay + totalNumberOfWorkDay;
                                        employeeData.NumberShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                                        foreach (var (totalSeconds, description, percentage) in totalOverTime)
                                        {
                                            if (percentage == 150)
                                            {
                                                employeeData.HourOT150 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                            else if (percentage == 200)
                                            {
                                                employeeData.HourOT200 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                            else if (percentage == 300)
                                            {
                                                employeeData.HourOT300 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                        }
                                    }

                                }
                            }
                            employeeTimeSheet.Add((user, employeeData.NumberWork, employeeData.HourOT150, employeeData.HourOT200, employeeData.HourOT300, employeeData.NumberShiftEat));
                        }
                        // Gửi mail bảng công của nhân sự quản lý
                        foreach (int managerId in managers)
                        {
                            //if (managerId != 2963)
                            //{
                            //    continue;
                            //}
                            try
                            {
                                User manager = userManager.GetUserById(managerId);
                                int totalPending = timeKeepingManager.CountPendingTimeKeeping(manager, true)
                                 + approvalRecordManager.CountPendingApprovalRecord(manager, true, false, false)
                                 + approvalRecordManager.CountPendingApprovalRecord(manager, true, false, true)
                                 + overtimeManager.CountPendingOverTime(manager, true, false);
                                if (totalPending > 0)
                                {
                                    // Gửi mail nhắc duyệt đơn
                                    emailManager.PushEmail(
                                        manager.Email,
                                        null,
                                        null,
                                        EmailTemplateCode.OC031,
                                        new string[]
                                        {
                                            manager.FullName,
                                            totalPending.ToString(),
                                            "https://checkin.topica.asia/#/approve"
                                        });

                                    // Gửi notify nhắc duyệt đơn cho quản lý
                                    notificationManager.PushNotification(
                                        manager.Email,
                                        null,
                                        null,
                                        NotificationTemplateCode.ON015,
                                        new string[]
                                        {
                                            totalPending.ToString()
                                        },
                                        manager.Nationality);
                                }

                                List<(int EmployeeCode, string FullName, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)> employeeStaffs = new List<(int EmployeeCode, string FullName, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)>();
                                foreach ((User user, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeTimeSheet.Where(e => e.User.ManagerId == manager.Id).ToList())
                                {
                                    employeeStaffs.Add((user.EmployeeCode, user.FullName, numberWork, hourOT150, hourOT200, hourOT300, numberShiftEat));
                                }

                                // Gửi bảng công chốt của nhân viên cho quản lý
                                StringBuilder textData = new StringBuilder();
                                int ThaiYear = 543 + timeRun.Year;
                                int ThaiMonth = timeRun.Month;
                                if (employeeStaffs.Count > 0)
                                {
                                    foreach ((int employeeCode, string fullName, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeStaffs)
                                    {
                                        textData.AppendLine("<tr>");
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", employeeCode));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", fullName));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", numberWork));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", hourOT150));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", hourOT200));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", hourOT300));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", hourOT150 + hourOT200 + hourOT300));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", numberShiftEat));
                                        textData.AppendLine("</tr>");
                                    }
                                    emailManager.PushEmail(
                                        manager.Email,
                                        null,
                                        null,
                                        EmailTemplateCode.OC032,
                                        new string[]
                                        {
                                            manager.FullName,
                                            timeRun.ToString(Constants.FORMAT_DATE_MMYYYY),
                                            textData.ToString(),
                                            "https://checkin.topica.asia/#/attendance/lowerGrade",
                                            ThaiYear.ToString(),
                                            ThaiMonth.ToString()
                                        });
                                }
                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("MANAGER ID: {0}", managerId), ex.Message));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    err.Add(("EXCEPTION", ex.Message));
                }

                if (err.Count > 0)
                {
                    StringBuilder errorMessage = new StringBuilder();
                    foreach ((string userId, string message) in err)
                    {
                        errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
                    }
                    await mailProcess.ErrorService(this.GetType().Name + ".     " + errorMessage.ToString());
                }

                // Tính thời gian delay
                timeRun = GetRunTime(true, numberPromptManagerCheck, hour, minute, second, LeaveCalendarType.TH);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                // Delay tới tháng sau
                await Delay((long)period);
            }
        }
    }


    public class PromptEmployeeApprovalCutoffTh : PromptTimeKeeping
    {
        public PromptEmployeeApprovalCutoffTh(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Run something
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptEmployeeCheck"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                List<(string, string)> err = new List<(string, string)>();
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        EmailManager emailManager = new EmailManager(db);
                        UserManager userManager = new UserManager(db);
                        ScriptManager scriptManager = new ScriptManager(db);
                        TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                        OvertimeManager overtimeManager = new OvertimeManager(db);
                        ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);
                        EventManager eventManager = new EventManager(db);
                        EmployeeTypeService employeeTypeService = new EmployeeTypeService(db);
                        HrbHistoryService hrbHistoryService = new HrbHistoryService(db);
                     
                        // Lấy danh sách user
                        List<User> users = userManager.GetUsersByCorporationType(CorporationType.CorporationTH);
                        // Lấy danh sách bảng công cho nhân sự
                        List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)> employeeTimeSheet = new List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)>();

                        DateTime startDate = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1).Date;
                        DateTime endDate = new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month)).Date;
                        // Lấy định mức ngày công trong tháng
                        foreach (User user in users)
                        {
                            (double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat) employeeData = (0, 0, 0, 0, 0);
                            // Lấy danh sách kịch bản trong tháng của nhân sự
                            List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, startDate, endDate);
                            foreach (ScriptHistory scriptHistory in scriptHistorys)
                            {
                                Script script = scriptManager.GetScriptById(scriptHistory.ScriptId);
                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                                DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                                DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;
                                // Lọc theo lịch sử hrb
                                // Lấy danh sách hrbHistory trong thời gian làm việc
                                List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                                foreach (HrbHistory hrbHistory in hrbHistorys)
                                {
                                    if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                    {
                                        continue;
                                    }
                                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                    DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                    DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;
                                    (List<ApprovalDateResult> approvalDates, double totalNumberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);
                                    double totalMorningSeconds = 0;
                                    double totalAffternoonSeconds = 0;
                                    int totalCheckinLate = 0;
                                    int totalCheckoutEarly = 0;
                                    double totalNumberOfWorkDay = 0;
                                    List<TimeKeepingResult> timeKeepingsInMonth = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endDate).Item1, user, script, startWorkDate, endWorkDate, "vi", ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                    (List<(double TotalSeconds, string Description, int Percentage)> totalOverTime, List<(double seconds, string workDate, List<(string, string, double)>)> overTimes) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(CorporationType.CorporationTH, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, "vi");
                                    if (script.NoNeedTimeKeeping)
                                    {
                                        // Lấy định mức ngày công trong tháng
                                        int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                        employeeData.NumberWork += quotasTimeKeepingInMonth;
                                    }
                                    else
                                    {
                                        employeeData.NumberWork += totalNumberOfLeaveDay + totalNumberOfWorkDay;
                                        employeeData.NumberShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                                        foreach (var (totalSeconds, description, percentage) in totalOverTime)
                                        {
                                            if (percentage == 150)
                                            {
                                                employeeData.HourOT150 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                            else if (percentage == 200)
                                            {
                                                employeeData.HourOT200 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                            else if (percentage == 300)
                                            {
                                                employeeData.HourOT300 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                        }
                                    }

                                }
                            }
                            employeeTimeSheet.Add((user, employeeData.NumberWork, employeeData.HourOT150, employeeData.HourOT200, employeeData.HourOT300, employeeData.NumberShiftEat));
                        }
                       
                        (DateTime cutoffTime, ScheduleType cutoffType) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptCheckTimeKeepingCutoff"));
                        cutoffTime = cutoffTime.AddMonths(1);

                        // Gửi email bảng công cho nhân viên
                        foreach ((User user, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeTimeSheet)
                        {
                            //if (user.EmployeeCode != 2963)
                            //{
                            //    continue;
                            //}
                            try
                            {
                                int totalPending = timeKeepingManager.CountPendingTimeKeeping(user, false)
                                   + approvalRecordManager.CountPendingApprovalRecord(user, false, false, false)
                                   + approvalRecordManager.CountPendingApprovalRecord(user, false, false, true)
                                   + overtimeManager.CountPendingOverTime(user, false, false)
                                   + overtimeManager.CountPendingOverTime(user, false, true);
                                int ThaiYear = 543 + timeRun.Year;
                                int ThaiMonth = timeRun.Month - 1;

                                // Gửi bảng công chốt cho nhân sự là nhân viên
                                // {Ten_Nguoi_Nhan},{month/year},{employeeCode},{fullName},{totalWorkDate},{hourOT150},{hourOT200},{hourOT300},{totalHourOT},{numberShiftEat},{ngay_chot},{luong_lan_2},{link}
                                emailManager.PushEmail(
                                    user.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC033,
                                    new string[]
                                    {
                                        user.FullName,
                                        timeRun.AddMonths(-1).ToString(Constants.FORMAT_DATE_MMYYYY),
                                        user.EmployeeCode.ToString(),
                                        user.FullName,
                                        numberWork.ToString(),
                                        hourOT150.ToString(),
                                        hourOT200.ToString(),
                                        hourOT300.ToString(),
                                        (hourOT150 + hourOT200 + hourOT300).ToString(),
                                        cutoffTime.ToString(Constants.FORMAT_DATE_MMDDYYYY),
                                        numberShiftEat.ToString(),
                                        "https://checkin.topica.asia/#/timesheet",
                                        ThaiYear.ToString(),
                                        ThaiMonth.ToString()
                                    });
                                // Gửi notify kiểm tra bảng công chốt cho nhân sự nhân viên
                                notificationManager.PushNotification(
                                    user.Email,
                                    null,
                                    null,
                                    NotificationTemplateCode.ON016,
                                    new string[]
                                    {
                                        // Tháng lương
                                        timeRun.Month.ToString(),
                                        // Ngày chốt
                                        cutoffTime.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        totalPending.ToString()
                                    },
                                    user.Nationality);
                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("USER ID: {0}", user.Id), ex.Message));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    err.Add(("EXCEPTION", ex.Message));
                }

                if (err.Count > 0)
                {
                    StringBuilder errorMessage = new StringBuilder();
                    foreach ((string userId, string message) in err)
                    {
                        errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
                    }
                    await mailProcess.ErrorService(this.GetType().Name + ".     " + errorMessage.ToString());
                }

                // Tính thời gian delay
                timeRun = AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                // Delay tới tháng sau
                await Delay((long)period);
            }
        }
    }
}

