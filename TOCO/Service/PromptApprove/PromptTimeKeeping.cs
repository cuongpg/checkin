﻿using Microsoft.Extensions.DependencyInjection;
using System;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Service
{
    public abstract class PromptTimeKeeping : BackgroundService
    {
        public PromptTimeKeeping(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }
    }
}

