﻿//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Net.Http;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using TOCO.Common;
//using TOCO.Implements;
//using TOCO.Logic;
//using TOCO.Models;
//using TOCO.Models.ResultModels.WisamiResultModels;

//namespace TOCO.Service
//{
//    public class PromptCheckTimeKeepingVi : PromptTimeKeeping
//    {

//        public PromptCheckTimeKeepingVi(IServiceScopeFactory scopeFactory) : base(scopeFactory)
//        {
//        }

//        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
//        {
//            int numberCutoffTemp = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:CutoffTemp:Number"));
//            int hour = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:CutoffTemp:Hour"));
//            int minute = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:CutoffTemp:Minute"));
//            int second = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:CutoffTemp:Second"));
//            while (!stoppingToken.IsCancellationRequested)
//            {
//                // Run something
//                double period = 0;
//                DateTime timeRun = GetRunTime(false, numberCutoffTemp, hour, minute, second, LeaveCalendarType.VI);
//                if (DateTime.Now != timeRun)
//                {
//                    timeRun = DateTime.Now < timeRun ? timeRun : GetRunTime(true, numberCutoffTemp, hour, minute, second, LeaveCalendarType.VI);
//                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//                    period = period > 0 ? period : 0;
//                    await Delay((long)period);
//                }
//                await mailProcess.RunService(this.GetType().Name);
//                List<(string, string)> err = new List<(string, string)>();
//                try
//                {
//                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
//                    {
//                        NotificationManager notificationManager = new NotificationManager(db);
//                        EmailManager emailManager = new EmailManager(db);
//                        UserManager userManager = new UserManager(db);
//                        ScriptManager scriptManager = new ScriptManager(db);
//                        TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
//                        OvertimeManager overtimeManager = new OvertimeManager(db);
//                        ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);
//                        EventManager eventManager = new EventManager(db);
//                        EmployeeTypeService employeeTypeService = new EmployeeTypeService(db);
//                        HrbHistoryService hrbHistoryService = new HrbHistoryService(db);
//                        // Lấy ra EmployeeType của CTV
//                        EmployeeType employeeTypeCTV = employeeTypeService.GetByName(EmployeeType.CTV);
//                        // Lấy danh sách user
//                        List<User> users = userManager.GetUsersByCorporationType(CorporationType.CorporationVI);
//                        // Lấy danh sách bảng công cho nhân sự
//                        List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)> employeeTimeSheet = new List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)>();
//                        // Lấy danh sách bảng công cho CTV
//                        // List<(User User, double NumberWork, double TotalHour, int NumberShiftEat)> ctvTimeSheet = new List<(User User, double NumberWork, double TotalHour, int NumberShiftEat)>();
//                        DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
//                        DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
//                        // Lấy định mức ngày công trong tháng
//                        foreach (User user in users)
//                        {
//                            (double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat) employeeData = (0, 0, 0, 0, 0);
//                            bool isEmployee = false;
//                            // Lấy danh sách kịch bản trong tháng của nhân sự
//                            List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, startDate, endDate);
//                            foreach (ScriptHistory scriptHistory in scriptHistorys)
//                            {
//                                Script script = scriptManager.GetScriptById(scriptHistory.ScriptId);
//                                if (script.EmployeeTypeId == employeeTypeCTV.Id)
//                                {
//                                    continue;
//                                }
//                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
//                                DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
//                                DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;

//                                // Lọc theo lịch sử hrb
//                                // Lấy danh sách hrbHistory trong thời gian làm việc
//                                List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
//                                foreach (HrbHistory hrbHistory in hrbHistorys)
//                                {
//                                    if (hrbHistory.HrbStatus == HrbStatus.Pending)
//                                    {
//                                        continue;
//                                    }
//                                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
//                                    DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
//                                    DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;

//                                    (List<ApprovalDateResult> approvalDates, double totalNumberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);

//                                    double totalMorningSeconds = 0;
//                                    double totalAffternoonSeconds = 0;
//                                    int totalCheckinLate = 0;
//                                    int totalCheckoutEarly = 0;
//                                    double totalNumberOfWorkDay = 0;
//                                    List<TimeKeepingResult> timeKeepingsInMonth = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endDate).Item1, user, script, startWorkDate, endWorkDate, "vi", ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
//                                    (List<(double TotalSeconds, string Description, int Percentage)> totalOverTime, List<(double seconds, string workDate, List<(string, string, double)>)> overTimes) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(CorporationType.CorporationVI, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, "vi");

//                                    if (script.NoNeedTimeKeeping)
//                                    {
//                                        isEmployee = true;
//                                        // Lấy định mức ngày công trong tháng
//                                        int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
//                                        employeeData.NumberWork += quotasTimeKeepingInMonth;
//                                    }
//                                    else
//                                    {
//                                        isEmployee = true;
//                                        employeeData.NumberWork += totalNumberOfLeaveDay + totalNumberOfWorkDay;
//                                        employeeData.NumberShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
//                                        foreach (var (totalSeconds, description, percentage) in totalOverTime)
//                                        {
//                                            if (percentage == 150)
//                                            {
//                                                employeeData.HourOT150 += Math.Round(totalSeconds / 3600, 2);
//                                            }
//                                            else if (percentage == 200)
//                                            {
//                                                employeeData.HourOT200 += Math.Round(totalSeconds / 3600, 2);
//                                            }
//                                            else if (percentage == 300)
//                                            {
//                                                employeeData.HourOT300 += Math.Round(totalSeconds / 3600, 2);
//                                            }
//                                        }
//                                    }

//                                }
//                            }
//                            if (isEmployee)
//                            {
//                                employeeTimeSheet.Add((user, employeeData.NumberWork, employeeData.HourOT150, employeeData.HourOT200, employeeData.HourOT300, employeeData.NumberShiftEat));
//                            }
//                        }
//                        DateTime timeCutoff = timeRun.AddDays(-1);
//                        int timekeepingQuotas = timeKeepingManager.GetQuotasTimeKeepingInMonth(startDate, timeCutoff) - 2;

//                        // Gửi email bảng công cho nhân viên
//                        foreach ((User user, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeTimeSheet)
//                        {
//                            try
//                            {
//                                // Gửi bảng công dùng để tính tạm ứng lương cho nhân sự là nhân viên
//                                emailManager.PushEmail(
//                                    user.Email,
//                                    null,
//                                    null,
//                                    EmailTemplateCode.OC012,
//                                    new string[]
//                                    {
//                                    user.FullName,
//                                    timeCutoff.ToString(Constants.FORMAT_DATE_MMYYYY),
//                                    timeCutoff.ToString(Constants.FORMAT_DATE_DDMMYYYY),
//                                    timeCutoff.ToString(),
//                                    user.EmployeeCode.ToString(),
//                                    user.FullName,
//                                    numberWork.ToString(),
//                                    hourOT150.ToString(),
//                                    hourOT200.ToString(),
//                                    hourOT300.ToString(),
//                                    (hourOT150 + hourOT200 + hourOT300).ToString(),
//                                    numberShiftEat.ToString(),
//                                    "https://checkin.topica.vn/#/timesheet"
//                                    });
//                                // Gửi notify kiểm tra bảng công dùng để tính tạm ứng lương cho nhân sự nhân viên
//                                notificationManager.PushNotification(
//                                    user.Email,
//                                    null,
//                                    null,
//                                    NotificationTemplateCode.ON011,
//                                    new string[]
//                                    {
//                                    timeCutoff.ToString(Constants.FORMAT_DATE_DDMMYYYY)
//                                    },
//                                    user.Nationality);
//                            }
//                            catch (Exception ex)
//                            {
//                                err.Add((string.Format("USER ID: {0}", user.Id), ex.Message));
//                            }
//                        }
//                    }
//                }
//                catch (Exception ex)
//                {
//                    err.Add(("EXCEPTION", ex.Message));
//                }
//                if (err.Count > 0)
//                {
//                    StringBuilder errorMessage = new StringBuilder();
//                    foreach ((string userId, string message) in err)
//                    {
//                        errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
//                    }
//                    await mailProcess.ErrorService(this.GetType().Name + ".     " + errorMessage.ToString());
//                }
//                // Tính thời gian delay
//                timeRun = GetRunTime(true, numberCutoffTemp, hour, minute, second, LeaveCalendarType.VI);
//                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
//                period = period > 0 ? period : 0;
//                // Delay tới tháng sau
//                await Delay((long)period);
//            }
//        }
//    }
//}

