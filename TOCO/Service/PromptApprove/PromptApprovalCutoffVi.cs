﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Service
{
    public class PromptApprovalCutoffVi : PromptTimeKeeping
    {
        public PromptApprovalCutoffVi(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int numberPromptCheck = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Vi:PromptCheck:Number"));

            int hour = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Vi:PromptCheck:Hour"));
            int minute = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Vi:PromptCheck:Minute"));
            int second = int.Parse(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Vi:PromptCheck:Second"));

            // Run something
            double period;
            DateTime timeRun = GetRunTime(false, numberPromptCheck, hour, minute, second, LeaveCalendarType.VI);
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : GetRunTime(true, numberPromptCheck, hour, minute, second, LeaveCalendarType.VI);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                List<(string, string)> err = new List<(string, string)>();
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        EmailManager emailManager = new EmailManager(db);
                        UserManager userManager = new UserManager(db);
                        ScriptManager scriptManager = new ScriptManager(db);
                        TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                        OvertimeManager overtimeManager = new OvertimeManager(db);
                        ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);
                        EventManager eventManager = new EventManager(db);
                        EmployeeTypeService employeeTypeService = new EmployeeTypeService(db);
                        HrbHistoryService hrbHistoryService = new HrbHistoryService(db);
                        // Lấy ra EmployeeType của CTV
                        EmployeeType employeeTypeCTV = employeeTypeService.GetByName(EmployeeType.CTV);
                        // Lấy danh sách quản lý còn đơn chưa duyệt
                        List<int> managers = userManager.GetAllManagerByCorporationType(CorporationType.CorporationVI);
                        // Lấy danh sách user
                        List<User> users = userManager.GetUsersByCorporationType(CorporationType.CorporationVI);
                        // Lấy danh sách bảng công cho nhân sự
                        List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)> employeeTimeSheet = new List<(User User, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)>();
                        // Lấy danh sách bảng công cho CTV
                        List<(User User, double NumberWork, double TotalHour, int NumberShiftEat)> ctvTimeSheet = new List<(User User, double NumberWork, double TotalHour, int NumberShiftEat)>();
                        
                        DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                        DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
                        // Lấy định mức ngày công trong tháng
                        foreach (User user in users)
                        {
                            (double NumberWork, double TotalHour, int NumberShiftEat) ctvData = (0, 0, 0);
                            (double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat) employeeData = (0, 0, 0, 0, 0);
                            bool isCTV = false;
                            bool isEmployee = false;
                            // Lấy danh sách kịch bản trong tháng của nhân sự
                            List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, startDate, endDate);
                            foreach (ScriptHistory scriptHistory in scriptHistorys)
                            {
                                Script script = scriptManager.GetScriptById(scriptHistory.ScriptId);
                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                                DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                                DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;

                                // Lọc theo lịch sử hrb
                                // Lấy danh sách hrbHistory trong thời gian làm việc
                                List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                                foreach (HrbHistory hrbHistory in hrbHistorys)
                                {
                                    if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                    {
                                        continue;
                                    }
                                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                    DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                    DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;

                                    (List<ApprovalDateResult> approvalDates, double totalNumberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);

                                    double totalMorningSeconds = 0;
                                    double totalAffternoonSeconds = 0;
                                    int totalCheckinLate = 0;
                                    int totalCheckoutEarly = 0;
                                    double totalNumberOfWorkDay = 0;
                                    List<TimeKeepingResult> timeKeepingsInMonth = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endDate).Item1, user, script, startWorkDate, endWorkDate, "vi", ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                    (List<(double TotalSeconds, string Description, int Percentage)> totalOverTime, List<(double seconds, string workDate, List<(string, string, double)>)> overTimes) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(CorporationType.CorporationVI, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, "vi");

                                    if (script.EmployeeTypeId == employeeTypeCTV.Id)
                                    {
                                        isCTV = true;
                                        ctvData.NumberWork += timeKeepingsInMonth.Count;
                                        ctvData.TotalHour += Math.Round((totalMorningSeconds + totalAffternoonSeconds) / 3600, 2);
                                        ctvData.NumberShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                                    }
                                    else if (script.NoNeedTimeKeeping)
                                    {
                                        isEmployee = true;
                                        // Lấy định mức ngày công trong tháng
                                        int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                        employeeData.NumberWork += quotasTimeKeepingInMonth;
                                    }
                                    else
                                    {
                                        isEmployee = true;
                                        employeeData.NumberWork += totalNumberOfLeaveDay + totalNumberOfWorkDay;
                                        employeeData.NumberShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                                        foreach (var (totalSeconds, description, percentage) in totalOverTime)
                                        {
                                            if (percentage == 150)
                                            {
                                                employeeData.HourOT150 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                            else if (percentage == 200)
                                            {
                                                employeeData.HourOT200 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                            else if (percentage == 300)
                                            {
                                                employeeData.HourOT300 += Math.Round(totalSeconds / 3600, 2);
                                            }
                                        }
                                    }

                                }
                            }
                            if (isCTV)
                            {
                                ctvTimeSheet.Add((user, ctvData.NumberWork, ctvData.TotalHour, ctvData.NumberShiftEat));
                            }
                            if (isEmployee)
                            {
                                employeeTimeSheet.Add((user, employeeData.NumberWork, employeeData.HourOT150, employeeData.HourOT200, employeeData.HourOT300, employeeData.NumberShiftEat));
                            }
                        }
                        // Gửi mail bảng công của nhân sự quản lý
                        foreach (int managerId in managers)
                        {
                            try
                            {
                                User manager = userManager.GetUserById(managerId);
                                int totalPending = timeKeepingManager.CountPendingTimeKeeping(manager, true)
                                 + approvalRecordManager.CountPendingApprovalRecord(manager, true, false, false)
                                 + approvalRecordManager.CountPendingApprovalRecord(manager, true, false, true)
                                 + overtimeManager.CountPendingOverTime(manager, true, false);
                                if (totalPending > 0)
                                {
                                    // Gửi mail nhắc duyệt đơn
                                    emailManager.PushEmail(
                                        manager.Email,
                                        null,
                                        null,
                                        EmailTemplateCode.OC015,
                                        new string[]
                                        {
                                            manager.FullName,
                                            "https://checkin.topica.vn/#/approve",
                                            totalPending.ToString()
                                        });

                                    // Gửi notify nhắc duyệt đơn cho quản lý
                                    notificationManager.PushNotification(
                                        manager.Email,
                                        null,
                                        null,
                                        NotificationTemplateCode.ON015,
                                        new string[]
                                        {
                                        totalPending.ToString()
                                        },
                                        manager.Nationality);
                                }

                                List<(int EmployeeCode, string FullName, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)> employeeStaffs = new List<(int EmployeeCode, string FullName, double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat)>();
                                foreach ((User user, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeTimeSheet.Where(e => e.User.ManagerId == manager.Id).ToList())
                                {
                                    employeeStaffs.Add((user.EmployeeCode, user.FullName, numberWork, hourOT150, hourOT200, hourOT300, numberShiftEat));
                                }
                                List<(int EmployeeCode, string FullName, double NumberWork, double TotalHour, int NumberShiftEat)> ctvStaffs = new List<(int EmployeeCode, string FullName, double NumberWork, double TotalHour, int NumberShiftEat)>();
                                foreach ((User user, double numberWork, double totalHour, int numberShiftEat) in ctvTimeSheet.Where(e => e.User.ManagerId == manager.Id).ToList())
                                {
                                    ctvStaffs.Add((user.EmployeeCode, user.FullName, numberWork, totalHour, numberShiftEat));
                                }

                                // Gửi bảng công chốt của nhân viên cho quản lý
                                StringBuilder textData = new StringBuilder();
                                if (employeeStaffs.Count > 0)
                                {
                                    foreach ((int employeeCode, string fullName, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeStaffs)
                                    {
                                        textData.AppendLine("<tr>");
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", employeeCode));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", fullName));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", numberWork));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", hourOT150));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", hourOT200));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", hourOT300));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", hourOT150 + hourOT200 + hourOT300));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", numberShiftEat));
                                        textData.AppendLine("</tr>");
                                    }
                                    emailManager.PushEmail(
                                        manager.Email,
                                        null,
                                        null,
                                        EmailTemplateCode.OC013,
                                        new string[]
                                        {
                                            manager.FullName,
                                            textData.ToString(),
                                            "https://checkin.topica.vn/#/attendance/lowerGrade",
                                            timeRun.ToString(Constants.FORMAT_DATE_MMYYYY)
                                        });
                                }
                                //  Gửi bảng công chốt của CTV cho quản lý
                                textData.Clear();
                                if (ctvStaffs.Count > 0)
                                {
                                    foreach ((int employeeCode, string fullName, double numberWork, double totalHour, int numberShiftEat) in ctvStaffs)
                                    {
                                        textData.AppendLine("<tr>");
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", employeeCode));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", fullName));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", numberWork));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p>{0}</p></td>", totalHour));
                                        textData.AppendLine(string.Format("<td style=\"text-align: center; border: 1px solid\"><p><strong>{0}</strong></p></td>", numberShiftEat));
                                        textData.AppendLine("</tr>");
                                    }
                                    emailManager.PushEmail(
                                        manager.Email,
                                        null,
                                        null,
                                        EmailTemplateCode.OC014,
                                        new string[]
                                        {
                                        manager.FullName,
                                        textData.ToString(),
                                        "https://checkin.topica.vn/#/attendance/lowerGrade",
                                        timeRun.ToString(Constants.FORMAT_DATE_MMYYYY)
                                        });
                                }

                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("MANAGER ID: {0}", managerId), ex.Message));
                            }
                        }

                        (DateTime cutoffTime, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Vi:PromptCheckTimeKeepingCutoff"));
                        cutoffTime = cutoffTime.AddMonths(1);

                        // Gửi email bảng công cho CTV
                        foreach ((User user, double numberWork, double totalHour, int numberShiftEat) in ctvTimeSheet)
                        {
                            try
                            {
                                int totalPending = timeKeepingManager.CountPendingTimeKeeping(user, false)
                                    + approvalRecordManager.CountPendingApprovalRecord(user, false, false, false)
                                    + approvalRecordManager.CountPendingApprovalRecord(user, false, false, true)
                                    + overtimeManager.CountPendingOverTime(user, false, false)
                                    + overtimeManager.CountPendingOverTime(user, false, true);

                                // Gửi mail bảng công dùng để tính lương chốt cho nhân sự CTV
                                emailManager.PushEmail(
                                    user.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC016,
                                    new string[]
                                    {
                                        user.FullName,
                                        timeRun.ToString(Constants.FORMAT_DATE_MMYYYY),
                                        user.EmployeeCode.ToString(),
                                        user.FullName,
                                        numberWork.ToString(),
                                        totalHour.ToString(),
                                        numberShiftEat.ToString(),
                                        "https://checkin.topica.vn/#/timesheet",
                                        cutoffTime.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        totalPending.ToString()
                                    });
                                // Gửi notify kiểm tra bảng công chốt cho nhân sự CTV
                                notificationManager.PushNotification(
                                    user.Email,
                                    null,
                                    null,
                                    NotificationTemplateCode.ON016,
                                    new string[]
                                    {
                                        // Tháng lương
                                        timeRun.Month.ToString(),
                                        // Ngày chốt
                                        cutoffTime.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        totalPending.ToString()
                                    },
                                    user.Nationality);
                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("USER ID: {0}", user.Id), ex.Message));
                            }
                        }
                        // Gửi email bảng công cho nhân viên
                        foreach ((User user, double numberWork, double hourOT150, double hourOT200, double hourOT300, int numberShiftEat) in employeeTimeSheet)
                        {
                            try
                            {
                                int totalPending = timeKeepingManager.CountPendingTimeKeeping(user, false)
                                   + approvalRecordManager.CountPendingApprovalRecord(user, false, false, false)
                                   + approvalRecordManager.CountPendingApprovalRecord(user, false, false, true)
                                   + overtimeManager.CountPendingOverTime(user, false, false)
                                   + overtimeManager.CountPendingOverTime(user, false, true);

                                // Gửi bảng công chốt cho nhân sự là nhân viên
                                emailManager.PushEmail(
                                    user.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC017,
                                    new string[]
                                    {
                                        user.FullName,
                                        timeRun.ToString(Constants.FORMAT_DATE_MMYYYY),
                                        user.EmployeeCode.ToString(),
                                        user.FullName,
                                        numberWork.ToString(),
                                        hourOT150.ToString(),
                                        hourOT200.ToString(),
                                        hourOT300.ToString(),
                                        (hourOT150 + hourOT200 + hourOT300).ToString(),
                                        numberShiftEat.ToString(),
                                        "https://checkin.topica.vn/#/timesheet",
                                        cutoffTime.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        totalPending.ToString()
                                    });
                                // Gửi notify kiểm tra bảng công chốt cho nhân sự nhân viên
                                notificationManager.PushNotification(
                                    user.Email,
                                    null,
                                    null,
                                    NotificationTemplateCode.ON016,
                                    new string[]
                                    {
                                        // Tháng lương
                                        timeRun.Month.ToString(),
                                        // Ngày chốt
                                        cutoffTime.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        totalPending.ToString()
                                    },
                                    user.Nationality);
                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("USER ID: {0}", user.Id), ex.Message));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    err.Add(("EXCEPTION", ex.Message));
                }

                if (err.Count > 0)
                {
                    StringBuilder errorMessage = new StringBuilder();
                    foreach ((string userId, string message) in err)
                    {
                        errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
                    }
                    await mailProcess.ErrorService(this.GetType().Name + ".     " + errorMessage.ToString());
                }

                // Tính thời gian delay
                timeRun = GetRunTime(true, numberPromptCheck, hour, minute, second, LeaveCalendarType.VI);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                // Delay tới tháng sau
                await Delay((long)period);
            }
        }
    }
}

