﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Service
{
    public class PromptApproveCutoffTempTh : PromptTimeKeeping
    {

        public PromptApproveCutoffTempTh(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Run something
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:PromptCutoffTemp"));
            double period = 0;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);
                List<(string, string)> err = new List<(string, string)>();
                try
                {
                    using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                    {
                        NotificationManager notificationManager = new NotificationManager(db);
                        EmailManager emailManager = new EmailManager(db);
                        UserManager userManager = new UserManager(db);
                        ScriptManager scriptManager = new ScriptManager(db);
                        TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                        OvertimeManager overtimeManager = new OvertimeManager(db);
                        ApprovalRecordManager approvalRecordManager = new ApprovalRecordManager(db);
                        EventManager eventManager = new EventManager(db);
                        EmployeeTypeService employeeTypeService = new EmployeeTypeService(db);
                        HrbHistoryService hrbHistoryService = new HrbHistoryService(db);
                        // Lấy danh sách quản lý Thái
                        List<int> managers = userManager.GetAllManagerByCorporationType(CorporationType.CorporationTH);
                        // Lấy danh sách user
                        List<User> users = userManager.GetUsersByCorporationType(CorporationType.CorporationTH);
                        // Lấy ra EmployeeType của CTV
                        EmployeeType employeeTypeCTV = employeeTypeService.GetByName(EmployeeType.CTV);

                        // Lấy danh sách bảng công cho nhân sự
                        List<(User User, double NumberWork)> employeeTimeSheet = new List<(User User, double NumberWork)>();
                        DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                        DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
                        // Lấy định mức ngày công trong tháng
                        foreach (User user in users)
                        {
                            double numberWork = 0;
                            // Lấy danh sách kịch bản trong tháng của nhân sự
                            List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, startDate, endDate);
                            foreach (ScriptHistory scriptHistory in scriptHistorys)
                            {
                                Script script = scriptManager.GetScriptById(scriptHistory.ScriptId);
                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                                DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                                DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;
                                // Lọc theo lịch sử hrb
                                // Lấy danh sách hrbHistory trong thời gian làm việc
                                List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                                foreach (HrbHistory hrbHistory in hrbHistorys)
                                {
                                    if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                    {
                                        continue;
                                    }
                                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                    DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                    DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;
                                    (List<ApprovalDateResult> approvalDates, double totalNumberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);
                                    double totalMorningSeconds = 0;
                                    double totalAffternoonSeconds = 0;
                                    int totalCheckinLate = 0;
                                    int totalCheckoutEarly = 0;
                                    double totalNumberOfWorkDay = 0;
                                    List<TimeKeepingResult> timeKeepingsInMonth = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endDate).Item1, user, script, startWorkDate, endWorkDate, "th", ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                    if (script.NoNeedTimeKeeping)
                                    {
                                        // Lấy định mức ngày công trong tháng
                                        int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                        numberWork += quotasTimeKeepingInMonth;
                                    }
                                    else
                                    {
                                        numberWork += totalNumberOfLeaveDay + totalNumberOfWorkDay;
                                    }
                                }
                            }
                            employeeTimeSheet.Add((user, numberWork));
                        }
                        // Gửi mail nhắc duyệt đơn
                        // Gửi mail bảng công của nhân sự quản lý
                        foreach (int managerId in managers)
                        {
                            //if(managerId != 2963)
                            //{
                            //    continue;
                            //}
                            try
                            {
                                User manager = userManager.GetUserById(managerId);
                                int totalPending = timeKeepingManager.CountPendingTimeKeeping(manager, true)
                                    + approvalRecordManager.CountPendingApprovalRecord(manager, true, false, false)
                                    + approvalRecordManager.CountPendingApprovalRecord(manager, true, false, true)
                                    + overtimeManager.CountPendingOverTime(manager, true, false);
                                if (totalPending > 0)
                                {
                                    // Gửi mail nhắc duyệt đơn cho quản lý
                                    emailManager.PushEmail(
                                        manager.Email,
                                        null,
                                        null,
                                        EmailTemplateCode.OC031,
                                        new string[]
                                        {
                                            manager.FullName,
                                            totalPending.ToString(),
                                            "https://checkin.topica.asia/#/approve"
                                        });
                                    // Gửi notify nhắc duyệt đơn cho quản lý
                                    notificationManager.PushNotification(
                                        manager.Email,
                                        null,
                                        null,
                                        NotificationTemplateCode.ON015,
                                        new string[]
                                        {
                                            totalPending.ToString()
                                        },
                                        manager.Nationality);
                                }
                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("MANAGER ID: {0}", managerId), ex.Message));
                            }
                        }

                        (DateTime cutoffTemp, ScheduleType cutoffTempType) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:PromptTimeKeeping:Th:CutoffTemp")); ;

                        // Gửi email bảng công cho nhân viên
                        foreach ((User user, double numberWork) in employeeTimeSheet)
                        {
                            //if (user.EmployeeCode != 2963)
                            //{
                            //    continue;
                            //}
                            try
                            {
                                (double NumberWork, double HourOT150, double HourOT200, double HourOT300, int NumberShiftEat) employeeData = (0, 0, 0, 0, 0);
                                bool isEmployee = false;
                                // Lấy danh sách kịch bản trong tháng của nhân sự
                                List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, startDate, endDate);
                                foreach (ScriptHistory scriptHistory in scriptHistorys)
                                {
                                    Script script = scriptManager.GetScriptById(scriptHistory.ScriptId);
                                    if (script.EmployeeTypeId == employeeTypeCTV.Id)
                                    {
                                        continue;
                                    }
                                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                                    DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                                    DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;

                                    // Lọc theo lịch sử hrb
                                    // Lấy danh sách hrbHistory trong thời gian làm việc
                                    List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                                    foreach (HrbHistory hrbHistory in hrbHistorys)
                                    {
                                        if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                        {
                                            continue;
                                        }
                                        // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                        DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                        DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;

                                        (List<ApprovalDateResult> approvalDates, double totalNumberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);

                                        double totalMorningSeconds = 0;
                                        double totalAffternoonSeconds = 0;
                                        int totalCheckinLate = 0;
                                        int totalCheckoutEarly = 0;
                                        double totalNumberOfWorkDay = 0;
                                        List<TimeKeepingResult> timeKeepingsInMonth = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endDate).Item1, user, script, startWorkDate, endWorkDate, "vi", ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                        (List<(double TotalSeconds, string Description, int Percentage)> totalOverTime, List<(double seconds, string workDate, List<(string, string, double)>)> overTimes) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(CorporationType.CorporationVI, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, "vi");

                                        if (script.NoNeedTimeKeeping)
                                        {
                                            isEmployee = true;
                                            // Lấy định mức ngày công trong tháng
                                            int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                            employeeData.NumberWork += quotasTimeKeepingInMonth;
                                        }
                                        else
                                        {
                                            isEmployee = true;
                                            employeeData.NumberShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                                            
                                        }

                                    }
                                }
                                    int totalPending = timeKeepingManager.CountPendingTimeKeeping(user, false)
                                    + approvalRecordManager.CountPendingApprovalRecord(user, false, false, false)
                                    + approvalRecordManager.CountPendingApprovalRecord(user, false, false, true)
                                    + overtimeManager.CountPendingOverTime(user, false, false)
                                    + overtimeManager.CountPendingOverTime(user, false, true);
                                int ThaiYear = 543 + timeRun.Year;
                                int ThaiMonth = timeRun.Month;

                                // Gửi bảng công dùng để tính tạm ứng lương cho nhân sự là nhân viên
                                emailManager.PushEmail(
                                    user.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC030,
                                    new string[]
                                    {
                                        timeRun.ToString(Constants.FORMAT_DATE_MMYYYY),
                                        user.FullName,
                                        user.EmployeeCode.ToString(),
                                        user.FullName,
                                        numberWork.ToString(),
                                        totalPending.ToString(),
                                        "https://checkin.topica.asia/#/timesheet",
                                        employeeData.NumberShiftEat.ToString(),
                                        ThaiYear.ToString(),
                                        ThaiMonth.ToString()
                                    });
                                // Gửi notify kiểm tra bảng công dùng để tính tạm ứng lương cho nhân sự nhân viên
                                notificationManager.PushNotification(
                                    user.Email,
                                    null,
                                    null,
                                    NotificationTemplateCode.ON011,
                                    new string[]
                                    {
                                        cutoffTemp.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        totalPending.ToString()
                                    },
                                    user.Nationality);
                            }
                            catch (Exception ex)
                            {
                                err.Add((string.Format("USER ID: {0}", user.Id), ex.Message));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    err.Add(("EXCEPTION", ex.Message));
                }
                if (err.Count > 0)
                {
                    StringBuilder errorMessage = new StringBuilder();
                    foreach ((string userId, string message) in err)
                    {
                        errorMessage.AppendLine(string.Format("{0} - {1}", userId, message));
                    }
                    await mailProcess.ErrorService(this.GetType().Name + ".     " + errorMessage.ToString());
                }
                // Delay tới tháng sau
                timeRun = AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
        }
    }
}

