﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Service
{
    public class AutoCheckoutService : BackgroundService
    {
        public AutoCheckoutService(IServiceScopeFactory scopeFactory) : base(scopeFactory)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Run something
            (DateTime timeRun, ScheduleType type) = GetRunTime(AppSetting.Configuration.GetValue("BackgroundService:RunAutoCheckOutTime"));
            double period;
            if (DateTime.Now != timeRun)
            {
                timeRun = DateTime.Now < timeRun ? timeRun : AddScheduleTime(timeRun, type);
                period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                period = period > 0 ? period : 0;
                await Delay((long)period);
            }
            while (!stoppingToken.IsCancellationRequested)
            {
                await mailProcess.RunService(this.GetType().Name);

                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        int id = 0;
                        try
                        {
                            UserManager userManager = new UserManager(db);
                            ScriptManager scriptManager = new ScriptManager(db);
                            TimeKeepingManager timeKeepingManager = new TimeKeepingManager(db);
                            OvertimeManager overtimeManager = new OvertimeManager(db);
                            EmailManager emailManager = new EmailManager(db);
                            NotificationManager notificationManager = new NotificationManager(db);
                            EventManager eventManager = new EventManager(db);
                            List<(TimeKeepingDetail, Script)> result = timeKeepingManager.AutoCheckOut();
                            foreach ((TimeKeepingDetail detail, Script script) in result)
                            {
                                if (detail.Status == StatusTimeKeepingApproval.Approved || detail.Status == StatusTimeKeepingApproval.Done)
                                {
                                    User manager = userManager.GetUserById(detail.TimeKeeping.User.ManagerId);
                                    // Xử lý ăn ca
                                    timeKeepingManager.ShiftsEatProcess(detail.TimeKeeping.WorkDate, detail.TimeKeeping.User, script, detail.CheckOutTime);
                                    // Get thời gian làm thêm giờ
                                    (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(detail.TimeKeeping.User, script, detail.CheckInTime.Value, detail.CheckOutTime.Value, false, detail.Id, null);
                                    if (hasOverTime)
                                    {
                                        // Kiểm tra conflict thời gian
                                        if (overtimeManager.CheckConflictTime(detail.TimeKeeping.User.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                                        {
                                            overtimeManager.DestroyConflictOverTime(detail.TimeKeeping.User.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null);
                                        }

                                        OverTimeDetail otdResult = overtimeManager.CreateOverTime(detail.TimeKeeping.User, script, detail.TimeKeeping.WorkDate, overTimeDetail, false, string.Empty, StatusOverTime.Pending);
                                        Event eventOvertime = eventManager.GetEventById(otdResult.EventId);
                                        // Bắn mail đăng ký làm thêm giờ cho quản lý
                                        emailManager.PushEmail(
                                            manager.Email,
                                            null,
                                            null,
                                            EmailTemplateCode.OC002,
                                            new string[]
                                            {
                                                manager.FullName,
                                                detail.TimeKeeping.User.FullName,
                                                string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                                (string)eventOvertime.EventCode,
                                                detail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                otdResult.StartTime.ToString(Constants.FORMAT_TIME_HHMM) + " - " + otdResult.EndTime.ToString(Constants.FORMAT_TIME_HHMM),
                                                otdResult.Note,
                                                AppSetting.Configuration.GetValue("MailConfig:UrlFrontEnd") + "/#/attendance/overtime"
                                            });

                                        // Bắn notify đăng ký làm thêm giờ cho quản lý
                                        notificationManager.PushNotification(
                                            manager.Email,
                                            null,
                                            detail.TimeKeeping.User.ImageUrl,
                                            NotificationTemplateCode.ON002,
                                            new string[]
                                            {
                                                detail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                detail.TimeKeeping.User.FullName
                                            },
                                            manager.Nationality);
                                    }
                                }
                                if (!PilotFeatureManager.CheckUsing(detail.TimeKeeping.User.Id, PilotCode.LAST_CHECKOUT))
                                {
                                    // Bắn mail quên checkout cho nhân sự
                                    emailManager.PushEmail(
                                    detail.TimeKeeping.User.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC003,
                                    new string[]
                                    {
                                        detail.TimeKeeping.User.FullName,
                                        detail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        AppSetting.Configuration.GetValue("MailConfig:UrlFrontEnd") + "/#/approvalRecord/manuallyRecord"
                                    });
                                }
                                // Bắn notify quên checkout cho nhân sự
                                notificationManager.PushNotification(
                                    detail.TimeKeeping.User.Email,
                                    null,
                                    null,
                                    NotificationTemplateCode.ON003,
                                    new string[]
                                    {
                                        detail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY)
                                    },
                                    detail.TimeKeeping.User.Nationality);
                            }
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            await mailProcess.ErrorService(this.GetType().Name + " <br>Id: " + id + ". <br>Error: " + ex.Message + "<br>" + ex.StackTrace);
                            transaction.Rollback();
                            throw ex;
                        }
                    }
                }
                if (type != ScheduleType.NONE)
                {
                    timeRun = AddScheduleTime(timeRun, type);
                    period = timeRun.Subtract(DateTime.Now).TotalMilliseconds;
                    period = period > 0 ? period : 0;
                    await Delay((long)period);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

