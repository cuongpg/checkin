﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.RequestModels;

namespace TOCO.Logic
{
    public class EmailManager
    {
        private readonly ApplicationDbContext context;
        private readonly EmailService emailService;
        private readonly EmailTemplateService emailTemplateService;
        private readonly UserService userService;
        public EmailManager(ApplicationDbContext context)
        {
            this.context = context;
            emailService = new EmailService(context);
            emailTemplateService = new EmailTemplateService(context);
            userService = new UserService(context);
        }

        public int PushEmail(string emailTo, string ccTo, string[] fileAttachs, string templateCode, string[] parameterValues)
        {
            Email email = new Email
            {
                EmailTo = emailTo,
                CCTo = ccTo,
                Status = EmailStatus.UNSENT,
                EmailType = templateCode,
                ParameterValues = JsonConvert.SerializeObject(parameterValues)
            };
            emailService.Create(email);

            return context.SaveChanges();
        }

        public (string Subject, string Body) GetContentByEmailType(Email email)
        {
            User user = userService.GetByEmail(email.EmailTo);
            EmailTemplate template = emailTemplateService.GetTemplateByCode(MessageManager.GetMessageManager(user.Nationality).GetValue(email.EmailType));
            string body = template.Body;
            string subject = template.Subject;
            string[] parameters = template.Parameters.Split(",");
            string[] parameterValues = JsonConvert.DeserializeObject<string[]>(email.ParameterValues);
            int indx = 0;
            foreach (string parameter in parameters)
            {
                body = body.Replace(parameter, MessageManager.GetMessageManager(user.Nationality).GetValue(parameterValues[indx]));
                subject = subject.Replace(parameter, MessageManager.GetMessageManager(user.Nationality).GetValue(parameterValues[indx]));
                indx++;
            }
            return (subject, body);
        }

        public List<Email> GetEmailsByStatus(EmailStatus status)
        {
            return emailService.GetEmailsByStatus(status);
        }

        public int UpdateStatus(int id, EmailStatus status)
        {
            Email email = emailService.GetById(id);
            if (email == null)
            {
                return -1;
            }
            email.Status = status;
            emailService.Update(email);
            return context.SaveChanges();
        }
    }
}
