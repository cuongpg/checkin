﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Logic
{
    public class PilotCode 
    {
        public const string FIRST_CHECKIN = "FIRST_CHECKIN";
        public const string LAST_CHECKOUT = "LAST_CHECKOUT";
    }

    public class PilotFeatureManager
    {
        public static bool CheckUsing(int userId, string code)
        {
            bool isUsing = true;
            using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
            {
                PilotFeatureService pilotFeatureService = new PilotFeatureService(db);
                UserService userService = new UserService(db);
                PilotFeature pilotFeature = pilotFeatureService.GetByCode(code);
                if (pilotFeature?.Status == (int)PilotStatus.DESTROY)
                {
                    isUsing = false;
                }
                else if (pilotFeature?.Status == (int)PilotStatus.PILOT)
                {
                    User user = userService.GetById(userId);
                    isUsing = user?.IsPilot ?? false;
                }
            }
            return isUsing;
        }
    }
}
