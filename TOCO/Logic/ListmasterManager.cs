﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.RequestModels;

namespace TOCO.Logic
{
    public class ListmasterManager
    {
        private readonly ApplicationDbContext context;
        private readonly CorporationService corporationService;
      
        public ListmasterManager(ApplicationDbContext context)
        {
            this.context = context;
            corporationService = new CorporationService(context);
          
        }

        public List<CorporationResult> GetCorporations(out int numberOfPages, out int totalRecords, int page, int perPage)
        {
            List<CorporationResult> result = new List<CorporationResult>();
            foreach (Corporation corporation in corporationService.GetCorporation(out numberOfPages, out totalRecords, page, perPage))
            {
                CorporationResult corporationResult = STHelper.ConvertObject<CorporationResult, Corporation>(corporation);
                result.Add(corporationResult);
            }
            return result;
        }

        public Corporation GetCorporationByCompleteCode(string corporation)
        {
            return corporationService.GetCorporationByCompleteCode(corporation);
        }
    }
}
