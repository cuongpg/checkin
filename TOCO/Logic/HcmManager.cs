﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.Pagination;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public delegate string ExportFunction(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime);

    public class HcmManager : CommonManager
    {
        private readonly int[] uniIds = { 20408, 20438, 20488, 20727, 20864, 21060, 21061 };
        private readonly int[] ignoreSyncCorporation = AppSetting.Configuration.GetArrayValue<int>("HKT:IgnoreSyncCorporation");
        public const string IT0007 = "0007";
        public const string IT2001 = "2001";
        public const string IT2006 = "2006";
        public const string IT2010 = "2010";
        public const string IT2003 = "2003";
        public const string IT0267 = "0267";
        public const string IT0015 = "0015";

        private readonly UserService userService;
        private readonly ApprovalRecordService approvalRecordService;
        private readonly AbsenderQuotasService absenderQuotasService;
        private readonly OverTimeDetailService overTimeDetailService;
        private readonly ScriptHistoryService scriptHistoryService;
        private readonly EmployeeTypeService employeeTypeService;
        private readonly ScriptService scriptService;
        private readonly TimeKeepingDetailService timeKeepingDetailService;
        private readonly ReasonTypeService reasonTypeService;
        private readonly GroupEventService groupEventService;
        private readonly ApprovalRecordManager approvalRecordManager;
        private readonly TimeKeepingManager timeKeepingManager;
        private readonly OvertimeQuotasService overtimeQuotasService;
        private readonly CorporationHistoryService corporationHistoryService;
        private readonly EventService eventService;
        private readonly HrbHistoryService hrbHistoryService;

        public HcmManager(ApplicationDbContext context) : base(context)
        {
            userService = new UserService(context);
            approvalRecordService = new ApprovalRecordService(context);
            absenderQuotasService = new AbsenderQuotasService(context);
            overTimeDetailService = new OverTimeDetailService(context);
            scriptHistoryService = new ScriptHistoryService(context);
            employeeTypeService = new EmployeeTypeService(context);
            scriptService = new ScriptService(context);
            timeKeepingDetailService = new TimeKeepingDetailService(context);
            reasonTypeService = new ReasonTypeService(context);
            approvalRecordManager = new ApprovalRecordManager(context);
            timeKeepingManager = new TimeKeepingManager(context);
            groupEventService = new GroupEventService(context);
            overtimeQuotasService = new OvertimeQuotasService(context);
            corporationHistoryService = new CorporationHistoryService(context);
            eventService = new EventService(context);
            hrbHistoryService = new HrbHistoryService(context);
        }
        private string GetOTLine(string formatLine, int userId, double totalSeconds, DateTime workDate, string hcmEventId, double? payPerHour)
        {
            CorporationHistory corporationHistory = corporationHistoryService.GetCorporationInDay(userId, workDate.Date);
            string timeOvertime = totalSeconds.ToString(CultureInfo.InvariantCulture);
            string hcmId = corporationHistory?.HcmPersionalId ?? userId.ToString();
            return string.Format(formatLine,
                hcmId,
                IT2010,
                hcmEventId,
                workDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                workDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                hcmEventId,
                timeOvertime,
                payPerHour.HasValue ? (payPerHour * 1000).ToString() : string.Empty);
        }

        private double CalculationWorkSeconds(DateTime startTime, DateTime endTime, int lunchBreak, bool isManual)
        {
            double totalSeconds = 0;
            if (isManual)
            {
                totalSeconds += endTime.Subtract(startTime).TotalSeconds;
            }
            else
            {
                (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, lunchBreak);
                totalSeconds += (morningSeconds + afternoonSeconds);
            }
            return totalSeconds;
        }

        public string ExportPlannedWorkingTimeEveryDay(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime)
        {
            string endWorkDate = DateTime.MaxValue.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
            string name = string.Format("P" + IT0007 + "-{0}.csv",
               timeRun.ToString(Constants.FORMAT_DATE_HCM_YYYYMMDDHHMMSS));
            string temp_path = @"Exported\IT0007\" + name;

            int employeeTypeId = employeeTypeService.GetByName(EmployeeType.NV).Id;
            // Lấy danh sách nhân sự
            IEnumerable<(int UserId,
                string HCMWorkScheduleRule,
                int LunchBreak,
                bool HasHoliday,
                bool HasLeaveWeekend,
                bool CheckInOnly,
                bool NoNeedTimeKeeping,
                int? HourOfWorkDay,
                DateTime StartWork,
                DateTime EndWork)> result = userService.GetNewUserIds(corporationType, employeeTypeId, startTime, endTime);
            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:P0007"));
                foreach ((int userId,
                    string hcmWorkScheduleRule,
                    int lunchBreak,
                    bool hasHoliday,
                    bool hasLeaveWeekend,
                    bool checkInOnly,
                    bool noNeedTimeKeeping,
                    int? hourOfWorkDay,
                    DateTime startWork,
                    DateTime endWork) in result)
                {
                    CorporationHistory corporationHistory = corporationHistoryService.GetCorporationInDay(userId, startTime);
                    //                  "\"PERNR\",\"INFTY\",\"SUBTY\",\"OBJPS\",\"SPRPS\",\"ENDDA\",\"BEGDA\",\"SEQNR\",\"AEDTM\",\"UNAME\",\"HISTO\",\"ITXEX\",\"REFEX\",\"ORDEX\",\"ITBLD\",\"PREAS\",\"FLAG1\",\"FLAG2\",\"FLAG3\",\"FLAG4\",\"RESE1\",\"RESE2\",\"GRPVL\",\"SCHKZ\",\"ZTERF\",\"EMPCT\",\"MOSTD\",\"WOSTD\",\"ARBST\",\"WKWDY\",\"JRSTD\",\"TEILK\",\"MINTA\",\"MAXTA\",\"MINWO\",\"MAXWO\",\"MINMO\",\"MAXMO\",\"MINJA\",\"MAXJA\",\"DYSCH\",\"KZTIM\",\"WWEEK\",\"AWTYP\"",
                    string formatLine = "\"{0}\",\"{1}\",\"\",\"\",\"\",\"{2}\",\"{3}\",\"000\",\"{4}\",\"CHECKIN\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"{5}\",\"0\",\"100\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"";
                    string line = string.Format(formatLine, corporationHistory?.HcmPersionalId ?? userId.ToString(), IT0007, endWorkDate, startWork.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), hcmWorkScheduleRule);
                    sw.WriteLine(line);
                }
            }

            return temp_path;
        }

        public string ExportAbsencesEveryMonth(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime)
        {
            string startMorning = corporationType == CorporationType.CorporationVI? "08:00" : "09:00";
            string endMorning = corporationType == CorporationType.CorporationVI ? "12:00" : "14:30";
            string startAfternoon = corporationType == CorporationType.CorporationVI ? "13:30" : "14:30";
            string endAfternoon = corporationType == CorporationType.CorporationVI ? "17:30" : "18:30";
            string name = string.Format("P" + IT2001 + "-{0}.csv", timeRun.ToString(Constants.FORMAT_DATE_HCM_YYYYMMDDHHMMSS));
            string temp_path = @"Exported\IT2001\" + name;
            // Lấy HCMCode nghỉ không lương
            string hcmUnPaidLeaveCode = "2001";
            string haftWorkHour = "4";
            //                 "\"PERNR\",\"INFTY\",\"SUBTY\",\"OBJPS\",\"SPRPS\",\"ENDDA\",\"BEGDA\",\"SEQNR\",\"AEDTM\",\"UNAME\",\"HISTO\",\"ITXEX\",\"REFEX\",\"ORDEX\",\"ITBLD\",\"PREAS\",\"FLAG1\",\"FLAG2\",\"FLAG3\",\"FLAG4\",\"RESE1\",\"RESE2\",\"GRPVL\",\"BEGUZ\",\"ENDUZ\",\"VTKEN\",\"AWART\",\"ABWTG\",\"STDAZ\",\"ABRTG\",\"ABRST\",\"ANRTG\",\"LFZED\",\"KRGED\",\"KBBEG\",\"RMDDA\",\"KENN1\",\"KENN2\",\"KALTG\",\"URMAN\",\"BEGVA\",\"BWGRL\",\"AUFKZ\",\"TRFGR\",\"TRFST\",\"PRAKN\",\"PRAKZ\",\"OTYPE\",\"PLANS\",\"MLDDA\",\"MLDUZ\",\"RMDUZ\",\"VORGS\",\"UMSKD\",\"UMSCH\",\"REFNR\",\"UNFAL\",\"STKRV\",\"STUND\",\"PSARB\",\"AINFT\",\"GENER\",\"HRSIF\",\"ALLDF\",\"WAERS\",\"LOGSYS\",\"AWTYP\",\"AWREF\",\"AWORG\",\"DOCSY\",\"DOCNR\",\"PAYTY\",\"PAYID\",\"BONDT\",\"OCRSN\",\"SPPE1\",\"SPPE2\",\"SPPE3\",\"SPPIN\",\"ZKMKT\",\"FAPRS\",\"TDLANGU\",\"TDSUBLA\",\"TDTYPE\",\"NXDFL\"",
            string formatLine = "\"{0}\",\"{1}\",\"{2}\",\"\",\"\",\"{3}\",\"{4}\",\"000\",\"{5}\",\"CHECKIN\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"{6}\",\"{7}\",\"\",\"\",\"\",\"{8}\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"";
            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                // Ghi header ra file
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:P2001"));

                // Lấy ra danh sách tất cả đơn nghỉ phép trong tháng
                int employeeTypeId = employeeTypeService.GetByName(EmployeeType.NV).Id;
                List<(ApprovalRecord Record,
                    int reasonId,
                    string HCMCode)> resultApproval = approvalRecordService.GetApprovalRecordForHcm(corporationType, startTime, endTime).ToList();
                // Lấy tất cả danh sách chấm công trong tháng
                List<(int UserId,
                    TimeKeepingDetail Detail,
                    DateTime WorkDate,
                    string HCMWorkScheduleRule)> resultTimeKeeping = timeKeepingDetailService.GetTimeKeepingByTime(corporationType, employeeTypeId, startTime, endTime).ToList();
                // Lấy danh sách tất cả nhân viên và khoảng thời gian có làm việc trong tháng
                IEnumerable<(int UserId,
                    HrbStatus HrbStatus,
                    string HCMWorkScheduleRule,
                    int LunchBreak,
                    bool HasHoliday,
                    bool HasLeaveWeekend,
                    bool CheckInOnly,
                    bool NoNeedTimeKeeping,
                    int? HourOfWorkDay,
                    DateTime StartWork,
                    DateTime EndWork)> userIds = userService.GetAllUserIds(corporationType, employeeTypeId, startTime, endTime);
                List<ApprovalRecordResult> approvalResults = new List<ApprovalRecordResult>();
                // Duyệt từng nhân viên
                foreach (IGrouping<int, (int UserId,
                    HrbStatus HrbStatus,
                    string HCMWorkScheduleRule,
                    int LunchBreak,
                    bool HasHoliday,
                    bool HasLeaveWeekend,
                    bool CheckInOnly,
                    bool NoNeedTimeKeeping,
                    int? HourOfWorkDay,
                    DateTime StartWork,
                    DateTime EndWork)> group in userIds.GroupBy(u => u.UserId))
                {
                    List<(ApprovalRecord Record, int reasonId, string HCMCode)> resultUserApproval = resultApproval.Where(a => a.Record.UserId == group.Key).ToList();
                    List<(int UserId, TimeKeepingDetail Detail, DateTime WorkDate, string HCMWorkScheduleRule)> resultUserTimeKeeping = resultTimeKeeping.Where(t => t.UserId == group.Key).ToList();
                    // Remote bản ghi của user đã xử lý
                    resultApproval.RemoveAll(a => a.Record.UserId == group.Key);
                    resultTimeKeeping.RemoveAll(t => t.UserId == group.Key);
                    // Loại bỏ thầy Uni
                    if (uniIds.Contains(group.Key))
                    {
                        continue;
                    }
                    foreach ((int userId,
                        HrbStatus hrbStatus,
                        string hcmWorkScheduleRule,
                        int lunchBreak,
                        bool hasHoliday,
                        bool hasLeaveWeekend,
                        bool checkInOnly,
                        bool noNeedTimeKeeping,
                        int? hourOfWorkDay,
                        DateTime startWork,
                        DateTime endWork) in group)
                    {
                        // Nếu khoảng thời gian nhân sự có trạng thái pending (nghỉ không lương/nghỉ thai sản) => đẩy nghỉ không lương lên HCM
                        if (hrbStatus == HrbStatus.Pending)
                        {
                            // Duyệt từng ngày
                            foreach (DateTime day in STHelper.EachDay(startWork, endWork))
                            {
                                if (CheckLeaveDate(userId, true, true, day))
                                {
                                    continue;
                                }
                                CorporationHistory corporationHistory = corporationHistoryService.GetCorporationInDay(userId, day);
                                string hcmId = corporationHistory?.HcmPersionalId ?? userId.ToString();
                                string startDay = day.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
                                string endDay = day.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
                                string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                sw.WriteLine(lineMorning);
                                string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                sw.WriteLine(lineAffternoon);
                            }
                        }
                        // Nếu khoảng thời gian nhân sự có trạng thái active và không thuộc kb Không cần chấm công => đẩy thông tin lên HCM 
                        else if (!noNeedTimeKeeping)
                        {
                            // Duyệt từng ngày
                            foreach (DateTime day in STHelper.EachDay(startWork, endWork))
                            {
                                if (CheckLeaveDate(userId, true, true, day))
                                {
                                    continue;
                                }
                                CorporationHistory corporationHistory = corporationHistoryService.GetCorporationInDay(userId, day);
                                string hcmId = corporationHistory?.HcmPersionalId ?? userId.ToString();
                                string startDay = day.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
                                string endDay = day.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
                                List<(ApprovalRecord Record, int reasonId, string HCMCode)> approvalIndays = resultUserApproval.Where(a => a.Record.StartTime.Date <= day && a.Record.EndTime.Date >= day).ToList();
                                bool isLeaveMorning = false;
                                bool isLeaveAfternoon = false;
                                if (approvalIndays != null && approvalIndays.Count > 0)
                                {
                                    foreach ((ApprovalRecord record, int reasonId, string hcmCode) in approvalIndays)
                                    {
                                        (double paidLeave, double unPaidLeave) = CalcTotalLeaveInDate(userId, hasHoliday, hasLeaveWeekend, record, day);
                                        // Nếu loại nghỉ phép là có lương
                                        if (record.ReasonType.HasPaidLeave)
                                        {
                                            // Nếu cả ngày có phép
                                            if (paidLeave == 1)
                                            {
                                                isLeaveMorning = true;
                                                isLeaveAfternoon = true;
                                                string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                sw.WriteLine(lineMorning);
                                                string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                                sw.WriteLine(lineAffternoon);
                                            }
                                            // Nếu nửa ngày có phép, nửa ngày không phép
                                            else if (paidLeave == 0.5 && unPaidLeave == 0.5)
                                            {
                                                isLeaveMorning = true;
                                                isLeaveAfternoon = true;
                                                string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                sw.WriteLine(lineMorning);
                                                string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                                sw.WriteLine(lineAffternoon);
                                            }
                                            // Nếu nửa ngày có phép, nửa ngày không nghỉ
                                            else if (paidLeave == 0.5 && unPaidLeave == 0)
                                            {
                                                isLeaveMorning = isLeaveMorning || record.IsMorning;
                                                isLeaveAfternoon = isLeaveAfternoon || record.IsAfternoon;
                                                string line = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), record.IsMorning ? startMorning : startAfternoon, record.IsMorning ? endMorning : endAfternoon, haftWorkHour);
                                                sw.WriteLine(line);
                                            }
                                            // Nếu nửa ngày không phép, nửa ngày không nghỉ
                                            else if (paidLeave == 0 && unPaidLeave == 0.5)
                                            {
                                                isLeaveMorning = isLeaveMorning || record.IsMorning;
                                                isLeaveAfternoon = isLeaveAfternoon || record.IsAfternoon;
                                                string line = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), record.IsMorning ? startMorning : startAfternoon, record.IsMorning ? endMorning : endAfternoon, haftWorkHour);
                                                sw.WriteLine(line);
                                            }
                                            // Nếu cả ngày không phép
                                            else
                                            {
                                                isLeaveMorning = true;
                                                isLeaveAfternoon = true;
                                                string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                sw.WriteLine(lineMorning);
                                                string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                                sw.WriteLine(lineAffternoon);
                                            }
                                        }
                                        // Nếu loại nghỉ phép là không lương
                                        else
                                        {
                                            // Nếu xin nghỉ cả ngày
                                            if (unPaidLeave == 1)
                                            {
                                                isLeaveMorning = true;
                                                isLeaveAfternoon = true;
                                                string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                sw.WriteLine(lineMorning);
                                                string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                                sw.WriteLine(lineAffternoon);
                                            }
                                            // Nếu nghỉ nửa ngày
                                            else
                                            {
                                                isLeaveMorning = isLeaveMorning || record.IsMorning;
                                                isLeaveAfternoon = isLeaveAfternoon || record.IsAfternoon;
                                                string line = string.Format(formatLine, hcmId, IT2001, hcmCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), record.IsMorning ? startMorning : startAfternoon, record.IsMorning ? endMorning : endAfternoon, haftWorkHour);
                                                sw.WriteLine(line);
                                            }
                                        }
                                    }
                                }
                                if (!isLeaveAfternoon || !isLeaveMorning)
                                {
                                    IEnumerable<(int UserId, TimeKeepingDetail Detail, DateTime WorkDate, string HCMWorkScheduleRule)> timeKeepinginDay = resultUserTimeKeeping.Where(tk => tk.WorkDate == day);
                                    // Nếu không chấm công
                                    if (timeKeepinginDay == null || timeKeepinginDay.Count() == 0)
                                    {
                                        if (!isLeaveAfternoon && !isLeaveMorning)
                                        {
                                            string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                            sw.WriteLine(lineMorning);
                                            string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                            sw.WriteLine(lineAffternoon);
                                        }
                                        else
                                        {
                                            string line = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), !isLeaveMorning ? startMorning : startAfternoon, !isLeaveMorning ? endMorning : endAfternoon, haftWorkHour);
                                            sw.WriteLine(line);
                                        }
                                    }
                                    else // Nếu có chấm công
                                    {
                                        double totalMorning = 0;
                                        double totalAfternoon = 0;
                                        DateTime midDay = new DateTime(day.Year, day.Month, day.Day, 12, 0, 0);
                                        DateTime? checkInTime = null;
                                        foreach (TimeKeepingDetail detail in timeKeepinginDay.Select(t => t.Detail))
                                        {
                                            if (detail.CheckInTime.HasValue && detail.CheckOutTime.HasValue)
                                            {
                                                checkInTime = !checkInTime.HasValue || checkInTime.Value > detail.CheckInTime.Value ? detail.CheckInTime : checkInTime;
                                                (double morning, double afternoon) = STHelper.CalculationWorkSeconds(detail.CheckOutTime.Value, detail.CheckInTime.Value, lunchBreak);
                                                totalMorning += morning;
                                                totalAfternoon += afternoon;
                                            }
                                        }
                                        // Làm tròn công
                                        // Nếu chỉ cần check in
                                        if (checkInOnly)
                                        {
                                            // Chỉ chấm công chiều và ko có đơn nghỉ sáng
                                            if (checkInTime >= midDay && !isLeaveMorning)
                                            {
                                                string line = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                sw.WriteLine(line);
                                            }
                                        }
                                        // Nếu cần đủ giờ
                                        if (hourOfWorkDay != null)
                                        {
                                            // Nếu không đủ 3 tiếng
                                            if ((totalMorning + totalAfternoon) < hourOfWorkDay.Value * 60 * 60 / 2)
                                            {
                                                if (!isLeaveAfternoon && !isLeaveMorning)
                                                {
                                                    string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                    sw.WriteLine(lineMorning);
                                                    string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                                    sw.WriteLine(lineAffternoon);
                                                }
                                                else
                                                {
                                                    string line = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), !isLeaveMorning ? startMorning : startAfternoon, !isLeaveMorning ? endMorning : endAfternoon, haftWorkHour);
                                                    sw.WriteLine(line);
                                                }
                                            }
                                            // Nếu không đủ 6 tiếng
                                            else if ((totalMorning + totalAfternoon) < hourOfWorkDay.Value * 60 * 60)
                                            {
                                                if (totalMorning >= totalAfternoon && !isLeaveAfternoon)
                                                {
                                                    string line = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                                                    sw.WriteLine(line);
                                                }
                                                else if (totalMorning < totalAfternoon && !isLeaveMorning)
                                                {
                                                    string line = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                                                    sw.WriteLine(line);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Xử lý đẩy đơn nghỉ ko lương cho nhân sự cùng lúc 2 pháp nhân
                foreach (int userId in ignoreSyncCorporation)
                {
                    List<CorporationHistory> corporationHistories = corporationHistoryService.GetByUserAndTime(userId, startTime, endTime).ToList();
                    DateTime startWork = corporationHistories.Min(ch => ch.EffectiveDate).Date;
                    DateTime endWork = corporationHistories.Max(ch => ch.ExpirationDate).Date;
                    startWork = startWork > startTime ? startWork : startTime;
                    endWork = endWork < endTime ? endWork : endTime;
                    // Duyệt từng ngày
                    foreach (DateTime day in STHelper.EachDay(startWork, endWork))
                    {
                        foreach (CorporationHistory corporationHistory in corporationHistories.Where(ch => ch.EffectiveDate > day || ch.ExpirationDate < day))
                        {
                            string hcmId = corporationHistory.HcmPersionalId;
                            string startDay = day.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
                            string endDay = day.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY);
                            string lineMorning = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startMorning, endMorning, haftWorkHour);
                            sw.WriteLine(lineMorning);
                            string lineAffternoon = string.Format(formatLine, hcmId, IT2001, hcmUnPaidLeaveCode, startDay, endDay, DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), startAfternoon, endAfternoon, haftWorkHour);
                            sw.WriteLine(lineAffternoon);
                        }
                    }
                }
            }
            return temp_path;
        }

        public string ExportAbsencesQuotasEveryDay(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime)
        {
            string name = string.Format("P" + IT2006 + "-{0}.csv",
                timeRun.ToString(Constants.FORMAT_DATE_HCM_YYYYMMDDHHMMSS));
            string temp_path = @"Exported\IT2006\" + name;
            IEnumerable<(AbsenderQuotas, string)> absenderQuotas = absenderQuotasService.GetAbsenderQuotas(corporationType, startTime, endTime);
            //                  "\"PERNR\",\"INFTY\",\"SUBTY\",\"OBJPS\",\"SPRPS\",\"ENDDA\",\"BEGDA\",\"SEQNR\",\"AEDTM\",\"UNAME\",\"HISTO\",\"ITXEX\",\"REFEX\",\"ORDEX\",\"ITBLD\",\"PREAS\",\"FLAG1\",\"FLAG2\",\"FLAG3\",\"FLAG4\",\"RESE1\",\"RESE2\",\"GRPVL\",\"BEGUZ\",\"ENDUZ\",\"VTKEN\",\"KTART\",\"ANZHL\",\"KVERB\",\"QUONR\",\"DESTA\",\"DEEND\",\"QUOSY\",\"TDLANGU\",\"TDSUBLA\",\"TDTYPE\"",
            string formatLine = "\"{0}\",\"{1}\",\"{2}\",\"\",\"\",\"{3}\",\"{4}\",\"000\",\"{5}\",\"CHECKIN\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"60\",\"{6}\",\"\",\"\",\"{7}\",\"{8}\",\"\",\"\",\"\",\"\"";
            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:P2006"));
                foreach ((AbsenderQuotas absenderQuota, string hcmQuotasCode) in absenderQuotas)
                {
                    DateTime endMonth = new DateTime(absenderQuota.CreatedAt.Year, absenderQuota.CreatedAt.Month, DateTime.DaysInMonth(absenderQuota.CreatedAt.Year, absenderQuota.CreatedAt.Month));
                    CorporationHistory corporationHistory = corporationHistoryService.GetCorporationInDay(absenderQuota.UserId, startTime);
                    string line = string.Format(formatLine, corporationHistory?.HcmPersionalId ?? absenderQuota.UserId.ToString(), IT2006, hcmQuotasCode, absenderQuota.CreatedAt.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), absenderQuota.CreatedAt.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), absenderQuota.Number.ToString(CultureInfo.InvariantCulture), absenderQuota.EffectiveDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), absenderQuota.ExpirationDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY));
                    sw.WriteLine(line);
                }
            }
            return temp_path;
        }

        public string ExportOvertimeEveryMonth(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime)
        {
            string name = string.Format("P" + IT2010 + "-{0}.csv",
                timeRun.ToString(Constants.FORMAT_DATE_HCM_YYYYMMDDHHMMSS));
            string temp_path = @"Exported\IT2010\" + name;
            int employeeTypeId = employeeTypeService.GetByName(EmployeeType.NV).Id;
            List<(int UserId,
                DateTime StartTime,
                DateTime EndTime,
                bool IsManual,
                int EventId,
                DateTime WorkDate,
                int GroupEventId,
                string HcmEventId,
                double? Percentage,
                double? PayPerHour,
                int LunchBreak)> result = overTimeDetailService.GetOvertimeHcm(corporationType, employeeTypeId, startTime, endTime).ToList();
            //                  "\"PERNR\",\"INFTY\",\"SUBTY\",\"OBJPS\",\"SPRPS\",\"ENDDA\",\"BEGDA\",\"SEQNR\",\"AEDTM\",\"UNAME\",\"HISTO\",\"ITXEX\",\"REFEX\",\"ORDEX\",\"ITBLD\",\"PREAS\",\"FLAG1\",\"FLAG2\",\"FLAG3\",\"FLAG4\",\"RESE1\",\"RESE2\",\"GRPVL\",\"BEGUZ\",\"ENDUZ\",\"VTKEN\",\"STDAZ\",\"LGART\",\"ANZHL\",\"ZEINH\",\"BWGRL\",\"AUFKZ\",\"BETRG\",\"ENDOF\",\"UFLD1\",\"UFLD2\",\"UFLD3\",\"KEYPR\",\"TRFGR\",\"TRFST\",\"PRAKN\",\"PRAKZ\",\"OTYPE\",\"PLANS\",\"VERSL\",\"EXBEL\",\"WAERS\",\"LOGSYS\",\"AWTYP\",\"AWREF\",\"AWORG\",\"WTART\",\"TDLANGU\",\"TDSUBLA\",\"TDTYPE\""
            string formatLine = "\"{0}\",\"{1}\",\"{2}\",\"\",\"\",\"{3}\",\"{4}\",\"000\",\"{5}\",\"CHECKIN\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"{6}\",\"{7}\",\"\",\"\",\"\",\"{8}\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"";
            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:P2010"));
                // Nếu pháp nhân Thái, lọc bỏ những đơn OT 200/300% để xử lý riêng
                if (corporationType == CorporationType.CorporationTH)
                {
                    int nt200300EventId = eventService.GetByCode(Event.NT_OT200_300).Id;
                    List<(int UserId,
                            DateTime StartTime,
                            DateTime EndTime,
                            bool IsManual,
                            int EventId,
                            DateTime WorkDate,
                            int GroupEventId,
                            string HcmEventId,
                            double? Percentage,
                            double? PayPerHour,
                            int LunchBreak)> ot200300s = result.Where(ot => ot.EventId == nt200300EventId).ToList();
                    // Gỡ bỏ những đơn làm thêm này khỏi danh sách tổng
                    result.RemoveAll(ot => ot.EventId == nt200300EventId);
                    foreach (var userIdGroup in ot200300s.GroupBy(ot => ot.UserId))
                    {
                        foreach (var wordDateGroup in userIdGroup.GroupBy(ot => ot.WorkDate))
                        {
                            // Tổng số giây làm thêm trong ngày theo giá trị
                            double totalSeconds = 0;
                            foreach ((int userId,
                                       DateTime start,
                                       DateTime end,
                                       bool isManual,
                                       int eventId,
                                       DateTime workDate,
                                       int groupEventId,
                                       string hcmEventId,
                                       double? percentage,
                                       double? payPerHour,
                                       int lunchBreak) in wordDateGroup)
                            {
                                totalSeconds += CalculationWorkSeconds(start, end, lunchBreak, isManual);
                            }
                            double ot8hourSeconds = 8 * 60 * 60;
                            double totalOvertime200 = 0;
                            double totalOvertime300 = 0;
                            if (totalSeconds < ot8hourSeconds)
                            {
                                totalOvertime200 = totalSeconds;
                            }
                            else
                            {
                                totalOvertime200 = ot8hourSeconds;
                                totalOvertime300 = totalSeconds - ot8hourSeconds;
                            }
                            var otFirt = wordDateGroup.OrderBy(ot => ot.StartTime).First();
                            if (totalOvertime200 > 0)
                            {
                                // Thêm overtime record 200%
                                result.Add((otFirt.UserId, otFirt.StartTime, otFirt.StartTime.AddSeconds(totalOvertime200), true, otFirt.EventId, otFirt.WorkDate, otFirt.GroupEventId, "3001", 200, otFirt.PayPerHour, otFirt.LunchBreak));
                            }
                            if (totalOvertime300 > 0)
                            {
                                // Thêm overtime record 300%
                                result.Add((otFirt.UserId, otFirt.StartTime, otFirt.StartTime.AddSeconds(totalOvertime300), true, otFirt.EventId, otFirt.WorkDate, otFirt.GroupEventId, "3002", 300, otFirt.PayPerHour, otFirt.LunchBreak));
                            }
                        }
                    }
                }
                foreach (var overtimeByUser in result.GroupBy(ot => ot.UserId))
                {
                    // Loại bỏ thầy Uni
                    if (uniIds.Contains(overtimeByUser.Key))
                    {
                        continue;
                    }
                    foreach (var overtimeByGroup in overtimeByUser.GroupBy(otu => otu.GroupEventId))
                    {
                        GroupEvent groupEvent = groupEventService.GetGroupEventById(overtimeByGroup.Key);
                        //if (groupEvent.HasQuotas && groupEvent.QuotasDefault.HasValue && groupEvent.PayByHour)
                        if (groupEvent.HasQuotas)
                        {
                            bool bBreak = false;
                            double totalSecondInMonth = 0;
                            double quotas = 0;
                            if (groupEvent.QuotasDefault.HasValue)
                            {
                                quotas = (double)groupEvent.QuotasDefault * 60 * 60;
                            }
                            else
                            {
                                OvertimeQuotas overtimeQuotas = overtimeQuotasService.GetByUserAndGroup(overtimeByUser.Key, overtimeByGroup.Key, startTime, endTime);
                                quotas = (double)(overtimeQuotas?.Quotas * 60 * 60 ?? 0);
                            }

                            if (quotas > 0)
                            {
                                // Group và sắp xếp giảm dần theo giá trị
                                foreach (var remunerationGroup in overtimeByGroup.GroupBy(ot => groupEvent.PayByHour ? ot.PayPerHour : ot.Percentage).OrderByDescending(g => g.Key))
                                {
                                    // Group theo ngày
                                    foreach (var wordDateGroup in remunerationGroup.GroupBy(ot => ot.WorkDate))
                                    {
                                        // Tổng số giây làm thêm trong ngày theo giá trị
                                        double totalSeconds = 0;
                                        foreach ((int userId,
                                                DateTime start,
                                                DateTime end,
                                                bool isManual,
                                                int eventId,
                                                DateTime workDate,
                                                int groupEventId,
                                                string hcmEventId,
                                                double? percentage,
                                                double? payPerHour,
                                                int lunchBreak) in wordDateGroup)
                                        {
                                            totalSeconds += CalculationWorkSeconds(start, end, lunchBreak, isManual);
                                        }

                                        if (totalSeconds > quotas - totalSecondInMonth)
                                        {
                                            totalSeconds = quotas - totalSecondInMonth;
                                            bBreak = true;
                                        }
                                        double totalHours = 0;
                                        if ((totalHours = Math.Round(totalSeconds / 3600, 3)) != 0)
                                        {
                                            string hcmEventId = wordDateGroup.First().HcmEventId;
                                            double? payPerHour = wordDateGroup.First().PayPerHour;
                                            string line = GetOTLine(formatLine, overtimeByUser.Key, totalHours, wordDateGroup.Key, hcmEventId, payPerHour);
                                            sw.WriteLine(line);
                                        }
                                        totalSecondInMonth += totalSeconds;
                                        if (bBreak)
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if (!groupEvent.HasQuotas)
                        {
                            // Group theo ngày
                            foreach (var wordDateGroup in overtimeByGroup.GroupBy(ot => ot.WorkDate))
                            {
                                foreach (var remunerationGroup in wordDateGroup.GroupBy(ot => groupEvent.PayByHour ? ot.PayPerHour : ot.Percentage))
                                {
                                    // Tổng số giây làm thêm trong ngày theo giá trị
                                    double totalSeconds = 0;
                                    foreach ((int userId,
                                        DateTime start,
                                        DateTime end,
                                        bool isManual,
                                        int eventId,
                                        DateTime workDate,
                                        int groupEventId,
                                        string hcmEventId,
                                        double? percentage,
                                        double? payPerHour,
                                        int lunchBreak) in remunerationGroup)
                                    {
                                        totalSeconds += CalculationWorkSeconds(start, end, lunchBreak, isManual);
                                    }
                                    double totalHours = 0;
                                    if ((totalHours = Math.Round(totalSeconds / 3600, 3)) != 0)
                                    {
                                        string hcmEventId = remunerationGroup.First().HcmEventId;
                                        double? payPerHour = remunerationGroup.First().PayPerHour;
                                        string line = GetOTLine(formatLine, overtimeByUser.Key, totalHours, wordDateGroup.Key, hcmEventId, payPerHour);
                                        sw.WriteLine(line);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return temp_path;
        }

        public async Task<MemoryStream> ExportSubstituons(DateTime start, DateTime end)
        {
            string name = string.Format("IMP_IT" + IT2003 + "_{ 0}{1}{2}{3}{4}{5}.csv",
               DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            string temp_path = @"Exported\" + name + ".csv";

            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:IT2003"));
                //string formatLine = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}";
                sw.Close();
            }

            var memory = new MemoryStream();
            using (var stream = new FileStream(temp_path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return memory;
        }

        public string ExportTimeKeepingsEveryMonth(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime)
        {
            string name = string.Format("P" + IT0267 + "-{0}.csv",
                timeRun.ToString(Constants.FORMAT_DATE_HCM_YYYYMMDDHHMMSS));
            string temp_path = @"Exported\IT0267\" + name;
            int employeeTypeId = employeeTypeService.GetByName(EmployeeType.NV).Id;
            // Lấy danh sách nhân sự
            IEnumerable<int> userIds = userService.GetUserIds(corporationType, employeeTypeId, startTime, endTime);
            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:IT0267"));
                //                  "PERNR","INFTY","SUBTY","OBJPS","SPRPS","ENDDA","BEGDA","SEQNR","AEDTM","UNAME","HISTO","ITXEX","REFEX","ORDEX","ITBLD","PREAS","FLAG1","FLAG2","FLAG3","FLAG4","RESE1","RESE2","GRPVL","LGART","OPKEN","BETRG","WAERS","ANZHL","ZEINH","INDBW","ZUORD","ESTDT","PABRJ","PABRP","UWDAT","PAYTY","PAYID","OCRSN"
                string formatLine = "\"{0}\",\"0267\",\"4000\",\"\",\"\",\"{1}\",\"{2}\",\"0\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"4000\",\"\",\"\",\"VND\",\"{3}\",\"010\",\"\",\"\",\"\",\"0\",\"0\",\"\",\"A\",\"\",\"0002\"";
                foreach (int userId in userIds)
                {
                    double totalNumber = 0;
                    User user = userService.GetById(userId);
                    // Lấy danh sách scriptHistory theo script
                    List<ScriptHistory> scriptHistorys = scriptHistoryService.GetByUserAndTime(userId, startTime, endTime).ToList();
                    if (scriptHistorys.Count == 0)
                    {
                        continue;
                    }
                    foreach (ScriptHistory scriptHistory in scriptHistorys)
                    {
                        Script script = scriptService.GetById(scriptHistory.ScriptId);
                        if (script.CorporationType != corporationType)
                        {
                            continue;
                        }
                        // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                        DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startTime ? scriptHistory.EffectiveDate : startTime;
                        DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endTime ? scriptHistory.ExpirationDate : endTime;
                        // Lọc theo lịch sử hrb
                        // Lấy danh sách hrbHistory trong thời gian làm việc
                        List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                        foreach (HrbHistory hrbHistory in hrbHistorys)
                        {
                            if (hrbHistory.HrbStatus == HrbStatus.Pending)
                            {
                                continue;
                            }
                            // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                            DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                            DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;
                            if (script.NoNeedTimeKeeping)
                            {
                                // Lấy định mức ngày công trong tháng
                                int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                totalNumber += quotasTimeKeepingInMonth;
                            }
                            else
                            {
                                (List<ApprovalDateResult> approvalDatesByHistory, double numberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);
                                totalNumber += numberOfLeaveDay;
                                double morningSeconds = 0;
                                double affternoonSeconds = 0;
                                double numberOfWorkDay = 0;
                                int totalCheckinLate = 0;
                                int totalCheckoutEarly = 0;
                                List<TimeKeepingResult> timeKeepings = timeKeepingManager.GetTimeKeepingsInMonth(true, approvalDatesByHistory, user, script, startWorkDate, endWorkDate, "vi", ref morningSeconds, ref affternoonSeconds, ref numberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                totalNumber += numberOfWorkDay;
                            }
                        }
                    }
                    CorporationHistory corporationHistory = corporationHistoryService.GetLastCorporation(userId);
                    if (totalNumber > 0)
                    {
                        string line = string.Format(formatLine,
                            corporationHistory?.HcmPersionalId ?? userId.ToString(),
                            endTime.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                            endTime.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                            totalNumber.ToString(CultureInfo.InvariantCulture));
                        sw.WriteLine(line);
                    }
                }
            }
            return temp_path;
        }

        public string ExportShiftEatsEveryMonth(string corporationType, DateTime timeRun, DateTime startTime, DateTime endTime)
        {
            string name = string.Format("P" + IT0015 + "-{0}.csv",
                timeRun.ToString(Constants.FORMAT_DATE_HCM_YYYYMMDDHHMMSS));
            string temp_path = @"Exported\IT0015\" + name;
            int employeeTypeId = employeeTypeService.GetByName(EmployeeType.NV).Id;
            string currency = corporationType == CorporationType.CorporationVI ? "VND" : "THB";
            double pay = corporationType == CorporationType.CorporationVI ? 20000 : 30;
            string subType = corporationType == CorporationType.CorporationVI ? "2016" : "7OTF";
            // Lấy danh sách nhân sự
            IEnumerable<int> userIds = userService.GetUserIds(corporationType, employeeTypeId, startTime, endTime);
            using (StreamWriter sw = new StreamWriter(temp_path, false))
            {
                sw.NewLine = "\n";
                sw.WriteLine(AppSetting.Configuration.GetValue("HCM:IT0015"));
                //                  "PERNR","INFTY","SUBTY","OBJPS","SPRPS","ENDDA","BEGDA","SEQNR","AEDTM","UNAME","HISTO","ITXEX","REFEX","ORDEX","ITBLD","PREAS","FLAG1","FLAG2","FLAG3","FLAG4","RESE1","RESE2","GRPVL","LGART","OPKEN","BETRG","WAERS","ANZHL","ZEINH","INDBW","ZUORD","ESTDT","PABRJ","PABRP","UWDAT","PAYTY","PAYID","OCRSN"
                string formatLine = "\"{0}\",\"0015\",\"{1}\",\"\",\"\",\"{2}\",\"{3}\",\"0\",\"{4}\",\"CHECKIN\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"{5}\",\"\",\"{6}\",\"{7}\",\"{8}\",\"010\",\"\",\"\",\"\",\"0\",\"0\",\"\",\"\"";

                foreach (int userId in userIds)
                {
                    // Loại bỏ thầy Uni
                    if (uniIds.Contains(userId))
                    {
                        continue;
                    }
                    int totalShiftEat = 0;
                    User user = userService.GetById(userId);
                    List<CorporationHistory> corporationHistories = corporationHistoryService.GetByUserAndTime(userId, startTime, endTime).OrderBy(ch => ch.EffectiveDate).ToList();
                    DateTime preEndDate = startTime;
                    foreach (CorporationHistory corporationHistory in corporationHistories)
                    {
                        totalShiftEat = 0;
                        DateTime startCorp = corporationHistory.EffectiveDate < startTime ? startTime : corporationHistory.EffectiveDate;
                        DateTime endCorp = corporationHistory.ExpirationDate > endTime ? endTime : corporationHistory.ExpirationDate;
                        // Loại bỏ khoảng thời gian đã tính ở pháp nhân cũ nếu đồng thời làm 2 pháp nhân
                        if (endCorp < preEndDate)
                        {
                            continue;
                        }
                        else
                        {
                            startCorp = startCorp > preEndDate ? startCorp : preEndDate;
                        }
                        // Lấy danh sách scriptHistory theo script
                        List<ScriptHistory> scriptHistorys = scriptHistoryService.GetByUserAndTime(userId, startCorp, endCorp).ToList();
                        if (scriptHistorys.Count == 0)
                        {
                            continue;
                        }
                        foreach (ScriptHistory scriptHistory in scriptHistorys)
                        {
                            Script script = scriptService.GetById(scriptHistory.ScriptId);
                            if (script.CorporationType != corporationType || script.EmployeeTypeId != employeeTypeId)
                            {
                                continue;
                            }
                            // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                            DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startCorp ? scriptHistory.EffectiveDate : startCorp;
                            DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endCorp ? scriptHistory.ExpirationDate : endCorp;
                            // Lọc theo lịch sử hrb
                            // Lấy danh sách hrbHistory trong thời gian làm việc
                            List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                            foreach (HrbHistory hrbHistory in hrbHistorys)
                            {
                                if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                {
                                    continue;
                                }
                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;
                                totalShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                            }
                        }
                        if (totalShiftEat > 0)
                        {
                            double money = corporationType == CorporationType.CorporationVI ? pay : pay * totalShiftEat;
                            string hcmId = corporationHistory?.HcmPersionalId ?? userId.ToString();
                            string line = string.Format(formatLine,
                                hcmId,
                                subType,
                                endCorp.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                                startCorp.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                                DateTime.Now.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY),
                                subType,
                                money.ToString(CultureInfo.InvariantCulture),
                                currency,
                                totalShiftEat.ToString(CultureInfo.InvariantCulture));
                            sw.WriteLine(line);
                        }
                        preEndDate = endCorp.AddDays(1);
                    }
                }
            }
            return temp_path;
        }



    }
}
