﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Logic
{
    public class ScriptManager
    {
        private readonly UserService userService;
        private readonly ScriptService scriptService;
        private readonly ScriptHistoryService scriptHistoryService;
        private readonly LeaveDateService leaveDateService;
        private readonly EmployeeTypeService employeeTypeService;
        private readonly AbsenderQuotasService absenderQuotasService;

        public ScriptManager(ApplicationDbContext context)
        {
            userService = new UserService(context);
            scriptService = new ScriptService(context);
            leaveDateService = new LeaveDateService(context);
            employeeTypeService = new EmployeeTypeService(context);
            scriptHistoryService = new ScriptHistoryService(context);
            absenderQuotasService = new AbsenderQuotasService(context);
        }

        public Script GetScriptByName(string scriptName)
        {
            return scriptService.GetByName(scriptName);
        }

        public Script GetCurrentScriptByUser(int userId, DateTime workDate)
        {
            return GetScriptInDay(userId, workDate);
        }

        public Script GetScriptById(int id)
        {
            return scriptService.GetById(id);
        }

        public EmployeeType GetEmployeeTypeById(int employeeTypeId)
        {
            return employeeTypeService.GetById(employeeTypeId);
        }

        public List<Script> GetAll()
        {
            return scriptService.GetAll();
        }

        public List<Script> GetScriptByUserAndTime(int userId, DateTime startDate, DateTime endDate)
        {
            List<Script> scripts = new List<Script>();
            foreach (ScriptHistory scriptHistory in scriptHistoryService.GetByUserAndTime(userId, startDate, endDate).OrderByDescending(s => s.EffectiveDate))
            {
                if (!scripts.Exists(s => s.Id == scriptHistory.ScriptId))
                {
                    scripts.Add(scriptService.GetById(scriptHistory.ScriptId));
                }
            }
            return scripts;
        }

        public List<ScriptHistory> GetScriptHistorys(int userId, DateTime startDate, DateTime endDate)
        {
            return scriptHistoryService.GetByUserAndTime(userId, startDate, endDate).ToList();
        }

        public Script GetScriptInDay(int userId, DateTime workDate)
        {
            Script script = null;
            ScriptHistory scriptHistory = scriptHistoryService.GetScriptInDay(userId, workDate);
            if (scriptHistory != null)
            {
                script = scriptService.GetById(scriptHistory.ScriptId);
            }
            return script;
        }

        public List<ScriptHistory> GetScriptHistorys(int userId, int scriptId, DateTime startDate, DateTime endDate)
        {
            return scriptHistoryService.GetByUserAndTime(userId, startDate, endDate).Where(sh => sh.ScriptId == scriptId).ToList();
        }

        public bool UpdateScript(int userId, int scriptId, string effectiveDate)
        {
            DateTime effective = DateTime.Parse(effectiveDate);
            ScriptHistory scriptHisNow = scriptHistoryService.GetScriptInDay(userId, DateTime.Now.Date);
            if (effective <= scriptHisNow.EffectiveDate || scriptHisNow.ScriptId == scriptId) return false;

            ScriptHistory newScriptHistory = new ScriptHistory
            {
                UserId = userId,
                ScriptId = scriptId,
                EffectiveDate = effective,
                ExpirationDate = DateTime.MaxValue
            };
            
            // Xóa bỏ các lịch sử trong tương lai
            List<ScriptHistory> futureHistories = scriptHistoryService.GetFutureHistories(userId, DateTime.Now.Date).ToList();
            foreach(ScriptHistory history in futureHistories)
            {
                scriptHistoryService.Delete(history.Id);
            }

            scriptHisNow.ExpirationDate = newScriptHistory.EffectiveDate.AddDays(-1);
            scriptHistoryService.Update(scriptHisNow);
            scriptHistoryService.Create(newScriptHistory);
            return true;
        }

        public ScriptHistory GetScriptHistoryInDay(int userId, DateTime date)
        {
            ScriptHistory scriptHistory = scriptHistoryService.GetScriptInDay(userId, date);
            return scriptHistory;
        }

        public bool CreateScriptHistory(ScriptHistory scriptHistory)
        {
            return (scriptHistoryService.Create(scriptHistory) != null);
        }

        //public bool UpdateActiveDate(DateTime newActiveDate, User user)
        //{
        //    bool ok = false;
        //    ScriptHistory currentHistory = scriptHistoryService.GetScriptInDay(user.Id, DateTime.Now);
        //    if (newActiveDate > currentHistory.EffectiveDate)
        //    {
        //        List<ScriptHistory> oldHistories = scriptHistoryService.GetOldScriptHistories(currentHistory.EffectiveDate, newActiveDate, user);
        //        foreach (var item in oldHistories)
        //        {
        //            scriptHistoryService.Delete(item.Id);
        //        }
        //        currentHistory = scriptHistoryService.GetScriptInDay(user.Id, newActiveDate);
        //    }
        //    else

        //    {
        //        currentHistory = scriptHistoryService.GetScriptInDay(user.Id, curentActiveDate);
        //    }
        //    currentHistory.EffectiveDate = newActiveDate;
        //    currentHistory = scriptHistoryService.Update(currentHistory);
        //    if (currentHistory != null) ok = true;
        //    return ok;
        //}

        //public DateTime GetActivedDate(int userId)
        //{
        //    bool hasLeave = false;
        //    ScriptHistory current_sh = scriptHistoryService.GetScriptInDay(userId, DateTime.Now);
        //    while (!hasLeave)
        //    {
        //        ScriptHistory temp_sh = scriptHistoryService.GetScriptInDay(userId, current_sh.EffectiveDate.AddDays(-1));
        //        if (temp_sh is null) hasLeave = true;
        //        else
        //        {
        //            current_sh = temp_sh;
        //        }
        //    }
        //    return current_sh.EffectiveDate;
        //}
    }
}
