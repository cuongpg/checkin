﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.Pagination;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public class TimeKeepingManager : CommonManager
    {
        private readonly TimeKeepingService timeKeepingService;
        private readonly TimeKeepingDetailService timeKeepingDetailService;
        private readonly ScriptService scriptService;
        private readonly OverTimeService overTimeService;
        private readonly OverTimeDetailService overTimeDetailService;
        private readonly UserService userService;
        private readonly ShiftsEatService shiftsEatService;
        private readonly EventService eventService;
        private readonly EstimateOwnerService estimateOwnerService;
        private readonly EmployeeTypeService employeeTypeService;
        private readonly ApprovalRecordService approvalRecordService;
        private readonly ReasonTypeService reasonTypeService;
        private readonly ChangeManagerInfoService changeManagerInfoService;


        private readonly List<string> STATUS_NOT_CONFIRM = new List<string> {
            ResultCode.INVALID_CHECKIN_ONTIME,
            ResultCode.INVALID_CHECKOUT_ONTIME,
            ResultCode.INVALID_ENOUGH_HOUR
        };

        public TimeKeepingManager(ApplicationDbContext context) : base(context)
        {
            timeKeepingService = new TimeKeepingService(context);
            timeKeepingDetailService = new TimeKeepingDetailService(context);
            userService = new UserService(context);
            scriptService = new ScriptService(context);
            overTimeService = new OverTimeService(context);
            overTimeDetailService = new OverTimeDetailService(context);
            shiftsEatService = new ShiftsEatService(context);
            eventService = new EventService(context);
            estimateOwnerService = new EstimateOwnerService(context);
            approvalRecordService = new ApprovalRecordService(context);
            employeeTypeService = new EmployeeTypeService(context);
            reasonTypeService = new ReasonTypeService(context);
            changeManagerInfoService = new ChangeManagerInfoService(context);
        }

        public StatusTimeKeepingResult GetStatus(User user, Script script, DateTime time)
        {
            StatusTimeKeepingResult status = new StatusTimeKeepingResult();
            TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, time.Date);
            if (script.NoNeedTimeKeeping)
            {
                status.Status = StatusTimeKeeping.Done;
            }
            else if (timeKeeping == null)
            {
                status.Status = StatusTimeKeeping.Checkin;
            }
            else
            {
                // Lấy ra tổng số lần đã chấm công hoàn thiện và tạm thời hợp lệ (có trạng thái khác Reject và Destroy)
                List<TimeKeepingDetail> listDetails = timeKeeping.TimeKeepingDetails
                .Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy && t.CheckOutTime != null).ToList();
                // Kiểm tra giờ hiện tại đã nằm trong secsion nào không
                bool inSession = false;
                foreach (TimeKeepingDetail detail in listDetails)
                {
                    if (time < detail.CheckOutTime)
                    {
                        inSession = true;
                    }
                }
                int checkRemain = script.NumberCheckInMax - listDetails.Count;
                if (checkRemain <= 0 || inSession)
                {
                    status.Status = StatusTimeKeeping.Done;
                    if (script.CheckInOnly)
                    {
                        status.SubstractTime = 0;
                    }
                    else
                    {
                        (double totalMorning, double totalAfternoon) = GetWorkSecondsInDay(user, script, timeKeeping);
                        status.SubstractTime += (totalAfternoon + totalMorning);
                    }
                }
                else
                {
                    // Lấy lần chấm công tạm thời hợp lệ cuối cùng trong ngày
                    TimeKeepingDetail timeKeepingDetail = timeKeeping.TimeKeepingDetails
                        .Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy)
                        .OrderByDescending(tk => tk.CheckInTime).FirstOrDefault();
                    // Nếu chưa có check out -> trạng thái là check out
                    if (timeKeepingDetail == null)
                    {
                        status.Status = StatusTimeKeeping.Checkin;
                    }
                    else if (timeKeepingDetail.CheckOutTime == null)
                    {
                        status.Status = StatusTimeKeeping.Checkout;
                        if (script.CheckInOnly)
                        {
                            status.SubstractTime = 0;
                        }
                        else
                        {
                            status.SubstractTime += DateTime.Now.Subtract(timeKeepingDetail.CheckInTime.Value).TotalSeconds;
                        }
                    }
                    // Nếu đã có check out -> trạng thái là check in lần tiếp
                    else
                    {
                        status.Status = StatusTimeKeeping.Checkin;
                    }
                }
            }
            return status;
        }

        public TimeKeepingDetail Update(TimeKeepingDetail detail)
        {
            return timeKeepingDetailService.Update(detail);
        }

        public IEnumerable<TimeKeepingDetail> GetPendingTimeKeepingLastMonth(DateTime startDate, DateTime endDate, string corporationType)
        {
            IEnumerable<TimeKeepingDetail> result = timeKeepingDetailService.GetPendingTimeKeepingLastMonth(startDate, endDate, corporationType);
            return result;
        }

        public int CountPendingTimeKeeping(User user, bool isForManager)
        {
            return timeKeepingDetailService.CountPendingTimeKeeping(user, isForManager);
        }

        public List<(TimeKeepingDetail, Script)> AutoCheckOut()
        {
            List<(TimeKeepingDetail, Script)> result = new List<(TimeKeepingDetail, Script)>();
            List<TimeKeepingDetail> timeKeepingDetails = GetForgetCheckOut();
            foreach (TimeKeepingDetail detail in timeKeepingDetails)
            {
                Script script = scriptService.GetById(detail.TimeKeeping.ScriptId);
                bool bUpdate = false;
                switch (script.HandleForgetCheckout)
                {
                    case Constants.HandleForgetCheckout.Auto:
                        CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
                        DateTime checkOutDefault = DateTime.ParseExact(detail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY) + " " + script.AutoCheckoutTime, Constants.FORMAT_DATE_DDMMYYYYHHMMSS, customCulture);
                        if (detail.CheckInTime >= checkOutDefault)
                        {
                            detail.Status = StatusTimeKeepingApproval.Destroy;
                        }
                        else
                        {
                            detail.CheckOutTime = checkOutDefault;
                        }
                        bUpdate = true;
                        break;

                    case Constants.HandleForgetCheckout.Destroy:
                        detail.Status = StatusTimeKeepingApproval.Destroy;
                        bUpdate = true;
                        break;

                    case Constants.HandleForgetCheckout.DoNothing:
                    default:
                        break;
                }
                if (bUpdate)
                {
                    detail.SystemNote += string.IsNullOrEmpty(detail.SystemNote) ? "" : "/";
                    detail.SystemNote += ResultCode.FORGET_CHECKOUT;
                    timeKeepingDetailService.Update(detail);
                    result.Add((detail, script));
                }
            }
            return result;
        }

        public List<TimeKeepingDetail> GetForgetCheckOut()
        {
            return timeKeepingDetailService.GetForgetCheckOut();
        }

        public (bool bOk, string[] resultStatus) ValidateCheckIn(User user, Script script, DateTime checkInTime)
        {
            bool bOk = true;
            List<string> skipAllowStatus = new List<string>();
            TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, checkInTime.Date);
            if (script.NoNeedTimeKeeping)
            {
                bOk = false;
            }
            else if (timeKeeping != null)
            {
                // Lấy ra tổng số lần chấm công tạm thời hợp lệ (có trạng thái khác Reject và Destroy)
                List<TimeKeepingDetail> listDetail = context.TimeKeepingDetails.Where(t => t.TimeKeepingId == timeKeeping.Id && t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy && t.CheckOutTime != null).ToList();
                int checkRemain = script.NumberCheckInMax - listDetail.Count();
                // filter so lan da cham cong trong ngay
                if (checkRemain <= 0)
                {
                    bOk = false;
                }

                // Kiểm tra giờ hiện tại đã nằm trong seccsion nào ko
                foreach (TimeKeepingDetail timeKeepingDetail in listDetail)
                {
                    if (checkInTime < timeKeepingDetail.CheckOutTime)
                    {
                        bOk = false;
                    }
                }

                // Check secssion chưa hoàn thiện (Lần chấm công tạm thời hợp lệ và chưa checkout)
                TimeKeepingDetail secssionCheckin = context.TimeKeepingDetails.Where(t => t.TimeKeepingId == timeKeeping.Id && t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy && t.CheckOutTime == null).FirstOrDefault();
                if (secssionCheckin != null)
                {
                    bOk = false;
                }
            }

            if (bOk)
            {
                DateTime workDate = checkInTime.Date;
                // neu user co ngay nghi le và hôm nay là ngày nghỉ lễ
                // neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
                // Không cần kiểm tra thông tin checkin checkout đúng giờ
                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
                LeaveDate leaveDate = context.LeaveDates.Where(ld => ld.Type == leaveCalendarTypeHistory.Code && ld.Date == workDate).FirstOrDefault();
                bool checkDone = leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK));

                // không có ngày nghỉ cuối tuần hoặc hôm đó không phải t7 và cn
                if (!checkDone)
                {
                    if (script.CheckInTimeDefault != null)
                    {
                        //so sanh neu gio check in hien tai > default + 15p (sai gio)
                        if (String.Compare(checkInTime.AddMinutes(-15).ToString(Constants.FORMAT_TIME_HHMMSS), script.CheckInTimeDefault) > 0)
                        {
                            skipAllowStatus.Add(ResultCode.INVALID_CHECKIN_ONTIME);
                        }
                    }
                }
            }
            return (bOk, skipAllowStatus.ToArray());
        }

        public bool CheckConflictTime(User user, Script script, DateTime checkInTime, DateTime? checkOutTime)
        {
            // Lấy ra EmployeeType của CTV
            EmployeeType employeeTypeCTV = employeeTypeService.GetByName(EmployeeType.CTV);

            DateTime workDate = checkInTime.Date;
            TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, workDate);
            if (timeKeeping == null)
            {
                return false;
            }
            // Lấy seccsion chấm công chưa hoàn thành
            TimeKeepingDetail secssionCheckin = timeKeepingDetailService.GetTimekeepingWaitCheckout(timeKeeping.Id);
            if (secssionCheckin != null)
            {
                return true;
            }

            // Lấy ra tổng số lần chấm công tạm thời hợp lệ (có trạng thái khác Reject và Destroy)
            List<TimeKeepingDetail> timeKeepingDetails = timeKeepingDetailService.GetFinishTimeKeepingByWorkDate(timeKeeping.Id);

            if (script.EmployeeTypeId != employeeTypeCTV.Id)
            {
                // Lấy số lượng bản ghi chấm công
                int numberTimeKeeping = timeKeepingDetails.Count();
                // Check số lần chấm công
                int checkRemain = script.NumberCheckInMax - numberTimeKeeping;
                // filter so lan da cham cong trong ngay
                if (checkRemain <= 0)
                {
                    return true;
                }
            }

            // Nếu có checkout
            if (checkOutTime != null)
            {
                // Kiểm tra nếu có trùng giờ checkin checkout => báo conflict
                foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
                {
                    if (!(checkOutTime <= timeKeepingDetail.CheckInTime || checkInTime >= timeKeepingDetail.CheckOutTime))
                    {
                        return true;
                    }
                }
            }
            else // Nếu ko có checkout
            {
                // Kiểm tra nếu có checkin < checkin đã chấm => báo conflict
                foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
                {
                    if (!(checkInTime <= timeKeepingDetail.CheckOutTime))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public (bool bOk, string[] resultCodes, TimeKeepingDetail timeKeepingDetail) ValidateCheckOut(User user, Script script, DateTime checkOutTime)
        {

            DateTime workDate = checkOutTime.Date;
            TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, workDate);
            bool bOK = true;
            List<string> allowSkipStatus = new List<string>();

            //tim kiem ban ghi dang checkin cho` checkout
            TimeKeepingDetail timeKeepingDetail = timeKeepingDetailService.GetTimekeepingWaitCheckout(timeKeeping.Id);
            if (script.NoNeedTimeKeeping)
            {
                bOK = false;
            }
            else if (timeKeepingDetail == null)
            {
                bOK = false;
            }
            else
            {
                // neu user co ngay nghi le và hôm nay là ngày nghỉ lễ
                // neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
                // Không cần kiểm tra thông tin checkin checkout đúng giờ
                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
                LeaveDate leaveDate = context.LeaveDates.Where(ld => ld.Type == leaveCalendarTypeHistory.Code && ld.Date == workDate).FirstOrDefault();
                bool checkDone = leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK));
                // không có ngày nghỉ cuối tuần hoặc hôm đó không phải t7 và cn
                if (!checkDone)
                {
                    (double morningSecondsToday, double affternoonSecondsToday) = STHelper.CalculationWorkSeconds(checkOutTime, timeKeepingDetail.CheckInTime.Value, script.LunchBreak);
                    double totalSecondsToday = morningSecondsToday + affternoonSecondsToday;
                    // phải làm đủ giờ
                    if (script.EnoughHour != null)
                    {
                        // Lấy số giờ cần làm trong ngày
                        double enoughtHour = script.EnoughHour.Value * 60 * 60;
                        // Kiểm tra xem có đơn nghỉ phép hôm nay không
                        List<ApprovalRecord> approvalRecords = approvalRecordService.GetApprovalRecordByDate(user, workDate);
                        if (approvalRecords != null && approvalRecords.Count > 0)
                        {
                            foreach (ApprovalRecord record in approvalRecords)
                            {
                                if (record.IsManyDate || (record.IsMorning && record.IsAfternoon))
                                {
                                    enoughtHour = 0;
                                }
                                else if (record.IsMorning || record.IsAfternoon)
                                {
                                    enoughtHour -= enoughtHour / 2;
                                }
                            }
                        }
                        enoughtHour = enoughtHour > 0 ? enoughtHour : 0;
                        //check nếu ko đủ giờ
                        if (totalSecondsToday < enoughtHour)
                        {
                            allowSkipStatus.Add(ResultCode.INVALID_ENOUGH_HOUR);
                        }
                    }

                    // Check nếu là CTV làm quá 8 tiếng
                    EmployeeType ctvType = employeeTypeService.GetByName(EmployeeType.CTV);
                    if (script.EmployeeTypeId == ctvType.Id)
                    {
                        double totalSeconds = totalSecondsToday;
                        // Nếu số lượng chấm công trong ngày > 1
                        // Lấy danh sách bản ghi chấm công chi tiết trong ngày
                        if (script.NumberCheckInMax > 1)
                        {
                            List<TimeKeepingDetail> timeKeepingDetails = timeKeepingDetailService.GetTimeKeepingDetailInDay(timeKeeping.Id)
                                .Where(tkd => tkd.CheckOutTime.HasValue && tkd.Id != timeKeepingDetail.Id)
                                .ToList();
                            foreach (TimeKeepingDetail detail in timeKeepingDetails)
                            {
                                (double morningSeconds, double affternoonSeconds) = STHelper.CalculationWorkSeconds(detail.CheckOutTime.Value, detail.CheckInTime.Value, script.LunchBreak);
                                totalSeconds += morningSeconds + affternoonSeconds;
                            }
                        }
                        if (totalSeconds > 8 * 60 * 60)
                        {
                            allowSkipStatus.Add(ResultCode.EXCEEDED_8_HOURS);
                        }
                    }

                    // cần về đúng giờ
                    if (script.CheckOutTimeDefault != null && string.Compare(script.CheckOutTimeDefault, checkOutTime.AddMinutes(15).ToString(Constants.FORMAT_TIME_HHMMSS)) > 0)
                    {
                        // Check xem có nghỉ phép trong ngày không
                        allowSkipStatus.Add(ResultCode.INVALID_CHECKOUT_ONTIME);
                    }
                }
            }
            return (bOK, allowSkipStatus.ToArray(), timeKeepingDetail);
        }

        public TimeKeepingDetail GetTimeKeepingDetail(User user, DateTime date)
        {
            TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, date);
            return timeKeeping?.TimeKeepingDetails
                       .Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy)
                       .OrderByDescending(tk => tk.CheckInTime).FirstOrDefault() ?? null;
        }

        public void ShiftsEatProcess(DateTime workDate, User user, Script script, DateTime? checkOutTime)
        {
            string checkOutTimeText = checkOutTime.Value.ToString(Constants.FORMAT_TIME_HHMMSS);
            // neu user co ngay nghi le và hôm nay là ngày nghỉ lễ
            // neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
            LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
            LeaveDate leaveDate = context.LeaveDates.Where(ld => ld.Type == leaveCalendarTypeHistory.Code && ld.Date == workDate).FirstOrDefault();
            bool hasShiftEat = leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK));

            if (script.HasShiftsEat && (String.Compare(checkOutTimeText, script.ShiftsEatTimeLine) >= 0 || hasShiftEat))
            {
                ShiftsEat shiftsEat = shiftsEatService.GetByWorkDate(user.Email, workDate);
                if (shiftsEat == null)
                {
                    //them data vao bang an ca
                    shiftsEat = new ShiftsEat
                    {
                        User = user,
                        WorkDate = workDate
                    };

                    shiftsEatService.Create(shiftsEat);
                }
            }
        }

        public void DestroyConflictTimeKeeping(User user, Script script, DateTime workDate, TimeKeeping timeKeeping, DateTime checkInTime, DateTime? checkOutTime)
        {
            // Lấy ra EmployeeType của CTV
            EmployeeType employeeTypeCTV = employeeTypeService.GetByName(EmployeeType.CTV);

            if (timeKeeping == null)
            {
                return;
            }
            // Lấy seccsion chấm công chưa hoàn thành
            TimeKeepingDetail secssionCheckin = timeKeeping.TimeKeepingDetails.Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy && t.CheckOutTime == null).FirstOrDefault();
            if (secssionCheckin != null)
            {
                if (checkOutTime == null || secssionCheckin.CheckInTime.Value < checkOutTime.Value)
                {
                    secssionCheckin.Status = StatusTimeKeepingApproval.Destroy;
                    context.TimeKeepingDetails.Update(secssionCheckin);
                    context.SaveChanges();
                }
            }
            // Lấy danh sách chấm công tạm thời hợp lệ trong ngày
            List<TimeKeepingDetail> timeKeepingDetails = timeKeeping.TimeKeepingDetails.Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy && t.CheckOutTime != null).OrderByDescending(t => t.CreatedAt).ToList();
            int destroyNumber = 0;
            foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
            {
                bool bConflict = false;
                if (checkOutTime != null && !(checkOutTime <= timeKeepingDetail.CheckInTime || checkInTime >= timeKeepingDetail.CheckOutTime))
                {
                    bConflict = true;
                }
                else if (checkOutTime == null && checkInTime <= timeKeepingDetail.CheckOutTime)
                {
                    bConflict = true;
                }
                if (bConflict)
                {
                    destroyNumber++;
                    timeKeepingDetail.Status = StatusTimeKeepingApproval.Destroy;
                    context.TimeKeepingDetails.Update(timeKeepingDetail);
                    context.SaveChanges();
                    // Hủy tất cả các đăng ký làm thêm giờ được tạo ra bởi bản chấm công bị hủy
                    int[] autoOverTimeIds = overTimeDetailService.GetAutoOverTimeDetails(user, timeKeeping.WorkDate);
                    overTimeDetailService.DestroyOverTime(autoOverTimeIds, user);
                }
            }
            if (script.EmployeeTypeId != employeeTypeCTV.Id)
            {
                if (destroyNumber == 0)
                {
                    // Check số lần chấm công
                    int checkRemain = script.NumberCheckInMax - timeKeepingDetails.Count();
                    // filter so lan da cham cong trong ngay
                    if (checkRemain <= 0)
                    {
                        timeKeepingDetails[0].Status = StatusTimeKeepingApproval.Destroy;
                        context.TimeKeepingDetails.Update(timeKeepingDetails[0]);
                        context.SaveChanges();
                    }
                }
            }
        }

        public (bool bOk, TimeKeeping timeKeeping, TimeKeepingDetail timeKeepingDetail) TimeKeepingManually(User user, Script script, bool isAdmin, TimeKeepingRequestModel timeKeepingRequestModel, string device)
        {
            DateTime workDate = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.WorkDate).Value.Date;
            DateTime checkInTime = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.CheckInTime).Value;
            DateTime? checkOutTime = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.CheckOutTime);
            TimeKeepingDetail timeKeepingDetail = new TimeKeepingDetail
            {
                IsManual = true,
                Status = StatusTimeKeepingApproval.Pending,
                Note = timeKeepingRequestModel.Note
            };
            (bool bOk, string[] statusAllowSkips) = ValidateTimeKeepingManual(user, script, checkInTime, checkOutTime, isAdmin);
            if (!bOk)
            {
                return (false, null, null);
            }
            else
            {
                TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, workDate);
                foreach (string status in statusAllowSkips)
                {
                    timeKeepingDetail.SystemNote = (string.IsNullOrEmpty(timeKeepingDetail.SystemNote) ? "" : timeKeepingDetail.SystemNote + "/") + status;
                }
                timeKeepingDetail.CheckInTime = checkInTime;
                timeKeepingDetail.CheckOutTime = checkOutTime;
                // Xử lý tính giờ check out cho kịch bản chỉ cần checkin
                if (script.CheckInOnly)
                {
                    timeKeepingDetail.CheckOutTime = CalcCheckOutForCheckInOnly(checkInTime, script);
                }
                if (timeKeeping == null)
                {
                    timeKeeping = timeKeepingService.Create(user, script.Id, workDate);
                }
                else
                {
                    if (timeKeeping.ScriptId != script.Id)
                    {
                        timeKeeping.ScriptId = script.Id;
                        timeKeeping = timeKeepingService.Update(timeKeeping);
                    }
                    // Kiểm tra conflict thời gian
                    if (CheckConflictTime(user, script, checkInTime, checkOutTime))
                    {
                        DestroyConflictTimeKeeping(user, script, workDate, timeKeeping, checkInTime, checkOutTime);
                    }
                }
                timeKeepingDetail.TimeKeepingId = timeKeeping.Id;
                timeKeepingDetail.TimeKeeping = timeKeeping;
                if (script.CheckInOnly)
                {
                    timeKeepingDetail.CheckOutTime = CalcCheckOutForCheckInOnly(checkInTime, script);
                }
                timeKeepingDetail.CheckinPosition = Constants.NA;
                timeKeepingDetail.CheckoutPosition = Constants.NA;
                timeKeepingDetail.IsCheckinFinger = false;
                timeKeepingDetail.IsCheckoutFinger = false;
                timeKeepingDetail.CheckinDevice = device;
                timeKeepingDetail.CheckoutDevice = device;
                timeKeepingDetail = timeKeepingDetailService.Create(timeKeepingDetail);

                return (true, timeKeeping, timeKeepingDetail);
            }
        }

        public (bool bOk, string status) AddNote(string email, string note)
        {
            DateTime workDate = DateTime.Now.Date;
            TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(email, workDate);
            return timeKeepingDetailService.AddNote(email, note, timeKeeping);
        }

        public (bool bOk, string[] resultStatus) ValidateTimeKeepingManual(User user, Script script, DateTime checkInTime, DateTime? checkOutTime, bool isAdmin)
        {
            bool bOk = true;
            List<string> allowSkips = new List<string>();
            DateTime workDate = checkInTime.Date;

            // Kiểm tra nếu có checkout thời gian checkout sau checkin không
            // Nếu không có checkout kiểm tra thời gian checkin là hôm nay
            // Kiểm tra nếu có checkin check out có cùng ngày workdate
            bOk = !script.NoNeedTimeKeeping && workDate.Date == checkInTime.Date && (checkOutTime != null ? checkInTime < checkOutTime && workDate.Date == checkOutTime.Value.Date : workDate.Date == DateTime.Now.Date);


            if (bOk && !isAdmin)
            {
                // Kiểm tra chấm công trong tương lai và chấm công cho tháng trước sau ngày mồng 03 hàng tháng
                DateTime startCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                DateTime startLastMonth = startCurrentMonth.AddMonths(-1).Date;
                if ((checkInTime > DateTime.Now)
                    || ((workDate.Date < startCurrentMonth) && DateTime.Now.Day >= script.CutoffTimeKeepingDay)
                    || ((workDate.Date < startLastMonth) && DateTime.Now.Day < script.CutoffTimeKeepingDay))
                {
                    bOk = false;
                }
            //}

            ////Chỉ được chấm thủ công trong đúng ngày, nếu khác ngày thì không được chấm bù
            //if (DateTime.Now.Date > workDate.Date.AddDays(1))
            //{
            //    bOk = false;
            //}
        }

            if (bOk)
            {
                // neu user co ngay nghi le và hôm nay là ngày nghỉ lễ
                // neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
                // Không cần kiểm tra thông tin checkin checkout đúng giờ
                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
                LeaveDate leaveDate = leaveDateService.GetByDateAndType(workDate, leaveCalendarTypeHistory.Code);
                bool checkDone = leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK)
                    || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK));

                // không có ngày nghỉ cuối tuần hoặc hôm đó không phải t7 và cn
                if (!checkDone)
                {
                    if (script.CheckInTimeDefault != null)
                    {
                        //so sanh neu gio check in hien tai > default + 15p (sai gio)
                        if (String.Compare(checkInTime.AddMinutes(-15).ToString(Constants.FORMAT_TIME_HHMMSS), script.CheckInTimeDefault) > 0)
                        {
                            allowSkips.Add(ResultCode.INVALID_CHECKIN_ONTIME);
                        }
                    }

                    if (checkOutTime != null && script.CheckOutTimeDefault != null)
                    {
                        //so sanh neu gio check in hien tai > default + 15p (sai gio)
                        if (String.Compare(script.CheckOutTimeDefault, checkOutTime.Value.AddMinutes(15).ToString(Constants.FORMAT_TIME_HHMMSS)) > 0)
                        {
                            allowSkips.Add(ResultCode.INVALID_CHECKOUT_ONTIME);
                        }
                    }
                }
            }

            return (bOk, allowSkips.ToArray());
        }

        public int GetQuotasTimeKeepingInMonth(DateTime startDate, DateTime endDate)
        {
            // Lấy danh sách nghỉ cuối tuần trong tháng
            int weekendNumber = GetWeekendNumber(startDate, endDate);
            int quotas = endDate.Subtract(startDate).Days + 1 - weekendNumber;
            return quotas >= 0 ? quotas : 0;
        }

        public (double totalMorning, double totalAfternoon) GetWorkSecondsInDay(User user, Script script, TimeKeeping timeKeeping)
        {
            double totalMorning = 0;
            double totalAfternoon = 0;
            DateTime workDate = timeKeeping.WorkDate;

            // Lấy danh sách số lần chấm công tạm thời hợp lệ trong ngày -> tính tổng thời gian làm việc trong ngày
            List<TimeKeepingDetail> timeKeepingDetails = timeKeeping.TimeKeepingDetails.Where(t => t.Status != StatusTimeKeepingApproval.Rejected && t.Status != StatusTimeKeepingApproval.Destroy && t.CheckOutTime != null).ToList();
            foreach (TimeKeepingDetail detail in timeKeepingDetails)
            {
                (double morning, double afternoon) = STHelper.CalculationWorkSeconds(detail.CheckOutTime.Value, detail.CheckInTime.Value, script.LunchBreak);
                totalMorning += morning;
                totalAfternoon += afternoon;
            }
            return (totalMorning, totalAfternoon);
        }

        public bool CheckIn(User user, Script script, bool validateClient, DateTime checkInTime, string note, string device, bool isFinger = false, string position = Constants.NA)
        {
            (bool bOk, string[] statusAllowSkips) = ValidateCheckIn(user, script, checkInTime);
            List<string> listStatus = new List<string>(statusAllowSkips);
            if (bOk)
            {
                TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, checkInTime.Date);
                TimeKeepingDetail timeKeepingDetail = new TimeKeepingDetail
                {
                    IsManual = false
                };
                if (!validateClient)
                {
                    timeKeepingDetail.InvalidCheckIn = true;
                    listStatus.Add(ResultCode.CHECKIN_INVALID_CLIENT);
                }
                foreach (string status in listStatus)
                {
                    timeKeepingDetail.SystemNote = (string.IsNullOrEmpty(timeKeepingDetail.SystemNote) ? "" : timeKeepingDetail.SystemNote + "/") + status;
                }
                // Set trạng thái chấm công
                listStatus.RemoveAll(s => STATUS_NOT_CONFIRM.Contains(s));
                timeKeepingDetail.Status = listStatus.Count() == 0 ? StatusTimeKeepingApproval.Done : StatusTimeKeepingApproval.Pending;
                timeKeepingDetail.CheckInTime = checkInTime;
                if (timeKeeping == null)
                {
                    timeKeeping = timeKeepingService.Create(user, script.Id, checkInTime.Date);
                }
                else if (timeKeeping.ScriptId != script.Id)
                {
                    timeKeeping.ScriptId = script.Id;
                    timeKeeping = timeKeepingService.Update(timeKeeping);
                }
                // Xử lý tính giờ check out cho kịch bản chỉ cần checkin
                if (script.CheckInOnly)
                {
                    timeKeepingDetail.CheckOutTime = CalcCheckOutForCheckInOnly(checkInTime, script);
                }
                timeKeepingDetail.TimeKeepingId = timeKeeping.Id;
                if (!string.IsNullOrEmpty(note))
                {
                    timeKeepingDetail.Note += string.IsNullOrEmpty(timeKeepingDetail.Note) ? note : "\n" + note;
                }
                timeKeepingDetail.IsCheckinFinger = isFinger;
                timeKeepingDetail.CheckinPosition = position;
                timeKeepingDetail.CheckinDevice = device;
                timeKeepingDetail = timeKeepingDetailService.Create(timeKeepingDetail);
                return timeKeeping != null && timeKeepingDetail != null;
            }
            else
            {
                return false;
            }
        }

        public (bool bOk, TimeKeepingDetail detail, TimeKeeping timeKeeping) CheckOut(User user, Script script, bool validateClient, DateTime checkOutTime, string overTimeNode, string device, bool isFinger = false, string position = Constants.NA)
        {
            //Validate và tim kiem ban ghi dang checkin cho` checkout
            (bool bOk, string[] statusAllowSkips, TimeKeepingDetail timeKeepingDetail) = ValidateCheckOut(user, script, checkOutTime);
            List<string> listStatus = new List<string>(statusAllowSkips);
            if (bOk)
            {
                DateTime workDate = checkOutTime.Date;

                TimeKeeping timeKeeping = timeKeepingService.GetByEmailAndWorkDate(user.Email, checkOutTime.Date);
                if (!validateClient)
                {
                    timeKeepingDetail.InvalidCheckOut = true;
                    listStatus.Add(ResultCode.CHECKOUT_INVALID_CLIENT);
                }
                foreach (string status in listStatus)
                {
                    timeKeepingDetail.SystemNote = (string.IsNullOrEmpty(timeKeepingDetail.SystemNote) ? "" : timeKeepingDetail.SystemNote + "/") + status;
                }
                // Set trạng thái chấm công
                listStatus.RemoveAll(s => STATUS_NOT_CONFIRM.Contains(s));
                timeKeepingDetail.Status = listStatus.Count() == 0 ? timeKeepingDetail.Status : StatusTimeKeepingApproval.Pending;
                if (!string.IsNullOrEmpty(overTimeNode))
                {
                    timeKeepingDetail.Note += string.IsNullOrEmpty(timeKeepingDetail.Note) ? overTimeNode : "\n" + overTimeNode;
                }
                timeKeepingDetail.CheckOutTime = checkOutTime;
                timeKeepingDetail.IsCheckoutFinger = isFinger;
                timeKeepingDetail.CheckoutPosition = position;
                timeKeepingDetail.CheckoutDevice = device;
                timeKeepingDetail = timeKeepingDetailService.Update(timeKeepingDetail);
                return (true, timeKeepingDetail, timeKeeping);
            }
            else
            {
                return (false, null, null);
            }
        }

        public List<AbnormalTimeKeepingResult> GetAbnormalTimeKeeping(out int numberOfPages, out int totalRecords, STRequestModel request, User user, bool isForManager, string langugage)
        {
            List<AbnormalTimeKeepingResult> result = new List<AbnormalTimeKeepingResult>();
            foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetailService.GetAbnormalTimeKeepingDetail(out numberOfPages, out totalRecords, request, user, isForManager))
            {
                string updateAt = string.Empty;
                switch (timeKeepingDetail.Status)
                {
                    case StatusTimeKeepingApproval.Approved:
                    case StatusTimeKeepingApproval.Rejected:
                        if (timeKeepingDetail.ApprovalTime.HasValue)
                        {
                            updateAt = timeKeepingDetail.ApprovalTime.Value.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS);
                        }
                        break;
                    case StatusTimeKeepingApproval.Destroy:
                    case StatusTimeKeepingApproval.Expired:
                    case StatusTimeKeepingApproval.Pending:
                    default:
                        break;
                }
                // Lấy ra thông tin quản lý
                User manager = null;
                if (timeKeepingDetail.Status == StatusTimeKeepingApproval.Pending)
                {
                    manager = userService.GetById(timeKeepingDetail.TimeKeeping.User.ManagerId);
                }
                else
                {
                    if (!string.IsNullOrEmpty(timeKeepingDetail.ApproverEmail))
                    {
                        manager = userService.GetByEmail(timeKeepingDetail.ApproverEmail);
                    }
                    else
                    {
                        ChangeManagerInfo changeManagerInfo = changeManagerInfoService.GetLastApprovalChangeManagerInfo(timeKeepingDetail.TimeKeeping.UserId, timeKeepingDetail.CreatedAt.Date);
                        if (changeManagerInfo != null)
                        {
                            manager = userService.GetById(changeManagerInfo.NewManagerId);
                        }
                    }
                }
                result.Add(new AbnormalTimeKeepingResult()
                {
                    Id = timeKeepingDetail.Id,
                    Name = timeKeepingDetail.TimeKeeping.User.FullName,
                    Email = timeKeepingDetail.TimeKeeping.User.Email,
                    ImageUrl = timeKeepingDetail.TimeKeeping.User.ImageUrl,
                    Status = (int)timeKeepingDetail.Status,
                    WorkDate = timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                    CheckInTime = timeKeepingDetail.CheckInTime != null ? timeKeepingDetail.CheckInTime.Value.ToString(Constants.FORMAT_TIME_HHMMSS) : string.Empty,
                    CheckOutTime = timeKeepingDetail.CheckOutTime != null ? timeKeepingDetail.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMMSS) : string.Empty,
                    Note = timeKeepingDetail.Note,
                    SystemNote = ConvertSystemNote(timeKeepingDetail.SystemNote, langugage),
                    ManagerName = manager?.FullName ?? Constants.NA,
                    ManagerEmail = manager?.Email ?? Constants.NA,
                    CreatedAt = timeKeepingDetail.CreatedAt.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS),
                    UpdatedAt = updateAt,
                    RejectReason = timeKeepingDetail.RejectReason
                });
            }
            return result;
        }

        private string ConvertSystemNote(string code, string language)
        {
            string systemNote = string.Empty;
            if (!string.IsNullOrEmpty(code))
            {
                string[] notes = code.Split('/');
                foreach (string note in notes)
                {
                    systemNote += String.Format("- {0}\n", MessageManager.GetMessageManager(language).GetValue(note));
                }
                systemNote.TrimEnd();
            }
            return systemNote;
        }

        public (bool bOk, string message, TimeKeepingDetail[] timeKeepingDetails) ConfirmAbnormalTimeKeeping(StatusTimeKeepingApproval status, int[] ids, User user, string rejectReason = null)
        {
            return timeKeepingDetailService.ConfirmAbnormalTimeKeeping(status, ids, user, rejectReason);
        }

        public int CountRejectTimeKeepingDetailsInMonth(int userId, DateTime startDate, DateTime endDate)
        {
            return timeKeepingDetailService.CountRejectTimeKeepingDetailsInMonth(userId, startDate, endDate);
        }

        public int CountShiftEatsInMonth(User user, DateTime startDate, DateTime endDate)
        {
            return shiftsEatService.GetShiftEatsInMonth(user, startDate, endDate).Count;
        }

        public DateTime? CalcCheckOutForCheckInOnly(DateTime checkInTime, Script script)
        {
            CultureInfo customCulture = new System.Globalization.CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
            DateTime startLunch = DateTime.ParseExact(checkInTime.ToString(Constants.FORMAT_DATE_DDMMYYYY) + " " + AppSetting.Configuration.GetValue("LunchBreakStart"), Constants.FORMAT_DATE_DDMMYYYYHHMMSS, customCulture);
            return checkInTime < startLunch ? checkInTime.AddHours(8).AddMinutes(script.LunchBreak) : (checkInTime < startLunch.AddMinutes(script.LunchBreak) ? checkInTime.AddHours(4).AddMinutes(script.LunchBreak) : (checkInTime.AddHours(4) < startLunch.AddHours(12) ? checkInTime.AddHours(4) : startLunch.AddHours(12)));
        }

        public List<TimeKeepingResult> GetTimeKeepingsInMonth(bool isApproved, List<ApprovalDateResult> approvalDates, User user, Script script, DateTime startDate, DateTime endDate, string language, ref double totalSecondsMorning, ref double totalSecondsAfternoon, ref double totalNumberOfWorkDay, ref int totalCheckinLate, ref int totalCheckoutEarly)
        {
            List<TimeKeepingResult> timeKeepingDetailsInMonth = new List<TimeKeepingResult>();
            List<TimeKeeping> timeKeepings = timeKeepingService.GetTimeKeepingInMonth(user, script, startDate, endDate);
            foreach (TimeKeeping timeKeeping in timeKeepings)
            {
                List<TimeKeepingDetail> result = isApproved ? timeKeepingDetailService.GetApprovedTimeKeepingDetailInDay(timeKeeping.Id) : timeKeepingDetailService.GetTimeKeepingDetailInDay(timeKeeping.Id);
                if (result.Count <= 0)
                {
                    continue;
                }
                TimeKeepingResult timeKeepingResult = new TimeKeepingResult
                {
                    NumberOfWorkDay = 0,
                    TotalSeconds = 0,
                    WorkDate = timeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                    Details = new List<TimeKeepingDetailResult>(),
                    IsAfternoon = false,
                    IsMorning = false,
                };
                (double morningSecondInDay, double afternoonSecondInDay) = GetWorkSecondsInDay(user, script, timeKeeping);
                timeKeepingResult.TotalSeconds += (morningSecondInDay + afternoonSecondInDay);
                DateTime? checkInTime = null;
                DateTime? checkOutTime = null;
                foreach (TimeKeepingDetail detail in result)
                {
                    if (!string.IsNullOrEmpty(detail.SystemNote))
                    {
                        if (detail.SystemNote.Contains(ResultCode.INVALID_CHECKIN_ONTIME))
                        {
                            totalCheckinLate++;
                        }
                        if (detail.SystemNote.Contains(ResultCode.INVALID_CHECKOUT_ONTIME))
                        {
                            totalCheckoutEarly++;
                        }
                    }
                    TimeKeepingDetailResult detailResult = new TimeKeepingDetailResult
                    {
                        CheckInTime = detail.CheckInTime?.ToString(Constants.FORMAT_TIME_HHMMSS),
                        CheckOutTime = detail.CheckOutTime?.ToString(Constants.FORMAT_TIME_HHMMSS),
                        Status = (int)detail.Status,
                        SystemNote = ConvertSystemNote(detail.SystemNote, language),
                        Note = detail.Note
                    };
                    timeKeepingResult.Details.Add(detailResult);
                    checkInTime = !checkInTime.HasValue || checkInTime.Value > detail.CheckInTime.Value ? detail.CheckInTime : checkInTime;
                    checkOutTime = !checkOutTime.HasValue || (detail.CheckOutTime.HasValue && checkOutTime.Value < detail.CheckOutTime.Value) ? detail.CheckOutTime : checkOutTime;
                }
                bool isLeave = CheckLeaveDate(user.Id, script.HasHoliday, script.HasLeaveWeekend, timeKeeping.WorkDate);
                if (!isLeave)
                {
                    DateTime midDay = new DateTime(timeKeeping.WorkDate.Year, timeKeeping.WorkDate.Month, timeKeeping.WorkDate.Day, 12, 0, 0);
                    // Làm tròn công
                    // Nếu chỉ cần check in
                    if (script.CheckInOnly)
                    {
                        timeKeepingResult.IsAfternoon = true;
                        timeKeepingResult.IsMorning = checkInTime < midDay ? true : false;
                    }
                    // Nếu cần đủ giờ
                    if (script.HourOfWorkDay != null && timeKeepingResult.TotalSeconds >= script.HourOfHalfWorkDay.Value * 60 * 60)
                    {
                        DateTime startAfternoon = midDay.AddMinutes(script.LunchBreak);
                        if (timeKeepingResult.TotalSeconds < script.HourOfWorkDay.Value * 60 * 60)
                        {
                            double morningSeconds = midDay.Subtract(checkInTime.Value).TotalSeconds;
                            double affternoonSeconds = checkOutTime.Value.Subtract(startAfternoon).TotalSeconds;
                            if (morningSeconds > affternoonSeconds)
                            {
                                timeKeepingResult.IsMorning = true;
                            }
                            else
                            {
                                timeKeepingResult.IsAfternoon = true;
                            }
                        }
                        else
                        {
                            timeKeepingResult.IsAfternoon = true;
                            timeKeepingResult.IsMorning = true;
                        }
                    }

                    List<ApprovalDateResult> approvalDateResults = approvalDates.Where(ap => ap.WorkDate == timeKeepingResult.WorkDate && ap.Status != (int)StatusApprovalRecord.Expired).ToList();
                    if (approvalDateResults != null && approvalDateResults.Count > 0)
                    {
                        foreach (ApprovalDateResult record in approvalDateResults)
                        {
                            timeKeepingResult.IsAfternoon = (record.IsAfternoon == timeKeepingResult.IsAfternoon) ? false : timeKeepingResult.IsAfternoon;
                            timeKeepingResult.IsMorning = (record.IsMorning == timeKeepingResult.IsMorning) ? false : timeKeepingResult.IsMorning;
                        }
                    }
                    timeKeepingResult.NumberOfWorkDay += timeKeepingResult.IsAfternoon ? 0.5 : 0;
                    timeKeepingResult.NumberOfWorkDay += timeKeepingResult.IsMorning ? 0.5 : 0;
                }
                if (timeKeepingResult.Details.All(d => d.Status == (int)StatusTimeKeepingApproval.Done || d.Status == (int)StatusTimeKeepingApproval.Approved))
                {
                    totalNumberOfWorkDay += timeKeepingResult.NumberOfWorkDay;
                }
                totalSecondsMorning += morningSecondInDay;
                totalSecondsAfternoon += afternoonSecondInDay;
                timeKeepingDetailsInMonth.Add(timeKeepingResult);
            }
            return timeKeepingDetailsInMonth;
        }

        public async Task<MemoryStream> ExportTimesheet(Script script, List<TimesheetResult> listTimesheetResult, DateTime startDate, DateTime endDate)
        {
            try
            {
                // Khởi tạo thông số ghi file
                var cellNumberFormat = HSSFDataFormat.GetBuiltinFormat("#,##0");
                XSSFWorkbook hssfwb;
                string tempPath;
                int generalStartRow;
                if (script.CorporationType == CorporationType.CorporationTH)
                {
                    generalStartRow = 13;
                    tempPath = @"Templates\ExportTimesheetThailand.xlsx";
                }
                else
                {
                    generalStartRow = 2;
                    tempPath = @"Templates\ExportTimesheet.xlsx";
                }
                using (FileStream file = new FileStream(tempPath, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new XSSFWorkbook(file);
                    file.Close();
                }
                // Lấy sheet Template bảng công - sheet đầu tiên
                ISheet generalSheet = hssfwb.GetSheetAt(0);
                EmployeeType employeeTypeCTV = employeeTypeService.GetByName(EmployeeType.CTV);
                string unPaidLeaveCode = reasonTypeService.GetById((int)ReasonThailandId.UnPaidLeaveThId).ReasonExportCode;
                int numberInTask = 10;
                int count = listTimesheetResult.Count();
                int taskNumber = count / numberInTask;
                if (count % numberInTask != 0)
                {
                    taskNumber++;
                }
                List<(string, ICellStyle)[]>[] generalDataInRowsInProcess = new List<(string, ICellStyle)[]>[taskNumber];
                List<(string, ICellStyle)[]>[] durationDetailDataInRowsInProcess = new List<(string, ICellStyle)[]>[taskNumber];
                List<(string, ICellStyle)[]>[] leaveDetailDataInRowsInProcess = new List<(string, ICellStyle)[]>[taskNumber];
                List<(string, ICellStyle)[]>[] leaveRemainDataInRowsInProcess = new List<(string, ICellStyle)[]>[taskNumber];
                Task[] process = new Task[taskNumber];
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    int i = indTask;
                    List<TimesheetResult> listTimesheetResultTask = new List<TimesheetResult>();
                    listTimesheetResultTask.AddRange(listTimesheetResult.Skip(i * numberInTask).Take(numberInTask).ToList());
                    process[i] = Task.Run(() =>
                    {
                        int ii = i;
                        generalDataInRowsInProcess[ii] = new List<(string, ICellStyle)[]>();
                        durationDetailDataInRowsInProcess[ii] = new List<(string, ICellStyle)[]>();
                        leaveDetailDataInRowsInProcess[ii] = new List<(string, ICellStyle)[]>();
                        leaveRemainDataInRowsInProcess[ii] = new List<(string, ICellStyle)[]>();
                        if (script.CorporationType == CorporationType.CorporationTH)
                        {
                            generalDataInRowsInProcess[ii].AddRange(ConvertDataTLProcess(listTimesheetResultTask, startDate, endDate, hssfwb));
                        }
                        else
                        {
                            generalDataInRowsInProcess[ii].AddRange(ConvertDataVNProcess(listTimesheetResultTask, startDate, endDate, employeeTypeCTV.Id));
                        }

                        durationDetailDataInRowsInProcess[ii].AddRange(ConvertDurationProcess(listTimesheetResultTask, startDate, endDate));
                        leaveDetailDataInRowsInProcess[ii].AddRange(ConvertLeaveProcess(listTimesheetResultTask, startDate, endDate, unPaidLeaveCode, hssfwb));
                        leaveRemainDataInRowsInProcess[ii].AddRange(ConvertLeaveRemainProcess(listTimesheetResultTask, script));
                    });
                }
                Task.WaitAll(process);

                // Ghi thông tin general
                List<(string, ICellStyle)[]> generalDataInRows = new List<(string, ICellStyle)[]>();
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    generalDataInRows.AddRange(generalDataInRowsInProcess[indTask]);
                }
                // Ghi data vào sheet
                WriteToSheet(ref generalSheet, generalDataInRows, generalStartRow);


                // Ghi thông tin detail
                List<(string, ICellStyle)[]> durationDataInRows = new List<(string, ICellStyle)[]>();
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    durationDataInRows.AddRange(durationDetailDataInRowsInProcess[indTask]);
                }
                // Lấy sheet Duration 
                ISheet durationSheet = hssfwb.GetSheetAt(1);
                WriteToSheet(ref durationSheet, durationDataInRows, 9);

                // Ghi thông tin nghỉ phép
                List<(string, ICellStyle)[]> leaveDataInRows = new List<(string, ICellStyle)[]>();
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    leaveDataInRows.AddRange(leaveDetailDataInRowsInProcess[indTask]);
                }
                // Lấy sheet Leave 
                ISheet leaveSheet = hssfwb.GetSheetAt(2);
                WriteToSheet(ref leaveSheet, leaveDataInRows, 16);

                // Ghi thông tin phép tồn
                List<(string, ICellStyle)[]> leaveRemainInRows = new List<(string, ICellStyle)[]>();
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    leaveRemainInRows.AddRange(leaveRemainDataInRowsInProcess[indTask]);
                }
                // Lấy sheet Remain 
                ISheet remainSheet = hssfwb.GetSheetAt(3);
                WriteToSheet(ref remainSheet, leaveRemainInRows, 5);

                var qguid = Guid.NewGuid().ToString();
                string filePath = @"Exported\" + qguid + ".xlsx";
                using (FileStream file = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    hssfwb.Write(file);
                    file.Close();
                }
                var memory = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return memory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<MemoryStream> ExportPendingRecordAsync(List<PendingRecordResult> listPendingRecords)
        {
            try
            {
                // Khởi tạo thông số ghi file
                var cellNumberFormat = HSSFDataFormat.GetBuiltinFormat("#,##0");
                XSSFWorkbook hssfwb;
                string tempPath = @"Templates\ExportPendingPetition.xlsx";
                int generalStartRow = 2;

                using (FileStream file = new FileStream(tempPath, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new XSSFWorkbook(file);
                    file.Close();
                }
                // Lấy sheet Template bảng công - sheet đầu tiên
                ISheet generalSheet = hssfwb.GetSheetAt(0);

                int numberInTask = 10;
                int count = listPendingRecords.Count();
                int taskNumber = count / numberInTask;
                if (count % numberInTask != 0)
                {
                    taskNumber++;
                }
                List<(string, ICellStyle)[]>[] generalDataInRowsInProcess = new List<(string, ICellStyle)[]>[taskNumber];

                Task[] process = new Task[taskNumber];
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    int i = indTask;
                    List<PendingRecordResult> listPendingRecordInTask = new List<PendingRecordResult>();
                    listPendingRecordInTask.AddRange(listPendingRecords.Skip(i * numberInTask).Take(numberInTask).ToList());
                    process[i] = Task.Run(() =>
                    {
                        int ii = i;
                        generalDataInRowsInProcess[ii] = new List<(string, ICellStyle)[]>();
                        generalDataInRowsInProcess[ii].AddRange(ConvertPendingRecordProcess(listPendingRecordInTask));
                    });
                }
                Task.WaitAll(process);

                // Ghi thông tin general
                List<(string, ICellStyle)[]> generalDataInRows = new List<(string, ICellStyle)[]>();
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    generalDataInRows.AddRange(generalDataInRowsInProcess[indTask]);
                }
                // Ghi data vào sheet
                WriteToSheet(ref generalSheet, generalDataInRows, generalStartRow);

                var name = string.Format("PendingPetition_{0}{1}{2}{3}{4}{5}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                string filePath = @"Exported\" + name + ".xlsx";
                using (FileStream file = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    hssfwb.Write(file);
                    file.Close();
                }
                var memory = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return memory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IEnumerable<(string, ICellStyle)[]> ConvertPendingRecordProcess(List<PendingRecordResult> listPendingRecord)
        {
            List<(string, ICellStyle)[]> dataInRows = new List<(string, ICellStyle)[]>();
            foreach (PendingRecordResult pendingRecord in listPendingRecord)
            {
                if (pendingRecord.User != null)
                {
                    (string, ICellStyle)[] dataRow = new (string, ICellStyle)[12];
                    dataRow[0] = (pendingRecord.User.FullName, null);
                    dataRow[1] = (pendingRecord.User.EmployeeCode.ToString(), null);
                    dataRow[2] = (pendingRecord.User.Email, null);
                    dataRow[3] = (pendingRecord.User.Corporation, null);
                    dataRow[4] = (pendingRecord.User.Department, null);
                    dataRow[5] = (pendingRecord.Manager.Email, null);
                    int total = pendingRecord.NumberPendingApproval
                        + pendingRecord.NumberPendingChangeManager
                        + pendingRecord.NumberPendingDestroyApproval
                        + pendingRecord.NumberPendingOverTime
                        + pendingRecord.NumberPendingTimeKeeping;
                    dataRow[6] = (total.ToString(), null);
                    dataRow[7] = (pendingRecord.NumberPendingTimeKeeping.ToString(), null);
                    dataRow[8] = (pendingRecord.NumberPendingOverTime.ToString(), null);
                    dataRow[9] = (pendingRecord.NumberPendingApproval.ToString(), null);
                    dataRow[10] = (pendingRecord.NumberPendingDestroyApproval.ToString(), null);
                    dataRow[11] = (pendingRecord.NumberPendingChangeManager.ToString(), null);
                    dataInRows.Add(dataRow);
                }
            }
            return dataInRows;
        }

        private List<(string, ICellStyle)[]> ConvertLeaveRemainProcess(List<TimesheetResult> listTimesheetResult, Script script)
        {
            int columnOffset = 5;
            List<(string, ICellStyle)[]> dataInRows = new List<(string, ICellStyle)[]>();


            foreach (TimesheetResult timesheetResult in listTimesheetResult)
            {
                double[] remainLeaves = script.CorporationType == CorporationType.CorporationTH ? timesheetResult.ThailandRemainLeave : timesheetResult.VietnamRemainLeave;

                if (timesheetResult.User != null)
                {
                    (string, ICellStyle)[] dataRow = new (string, ICellStyle)[13];
                    dataRow[0] = (timesheetResult.User.FullName, null);
                    dataRow[1] = (timesheetResult.User.EmployeeCode.ToString(), null);
                    dataRow[2] = (timesheetResult.User.Email, null);
                    dataRow[3] = (timesheetResult.User.Corporation, null);
                    dataRow[4] = (timesheetResult.User.Department, null);
                    int count = remainLeaves.Count();
                    // ngày phép tồn
                    for (int indx = 0; indx < count; indx++)
                    {
                        dataRow[indx + columnOffset] = (remainLeaves[indx].ToString(CultureInfo.InvariantCulture), null);
                    }
                    dataInRows.Add(dataRow);

                }
            }
            return dataInRows;
        }

        private List<(string, ICellStyle)[]> ConvertDurationProcess(List<TimesheetResult> listTimesheetResult, DateTime startDate, DateTime endDate)
        {
            int dateOffset = 4;
            List<(string, ICellStyle)[]> dataInRows = new List<(string, ICellStyle)[]>();
            foreach (TimesheetResult timesheetResult in listTimesheetResult)
            {
                if (timesheetResult.User != null)
                {
                    (string, ICellStyle)[] dataRow = new (string, ICellStyle)[68];
                    dataRow[0] = (timesheetResult.User.FullName, null);
                    dataRow[1] = (timesheetResult.User.EmployeeCode.ToString(), null);
                    dataRow[2] = (timesheetResult.User.Email, null);
                    dataRow[3] = (timesheetResult.User.Corporation, null);
                    dataRow[4] = (timesheetResult.User.Department, null);
                    // ngày 1 - 31 (cell 6 - 67)(item 5 - 66)
                    // duyệt công làm hàng ngày
                    foreach (DateTime time in STHelper.EachDay(startDate, endDate))
                    {
                        int day = time.Day;
                        string checkinTime = string.Empty;
                        string checkoutTime = string.Empty;
                        TimeKeepingResult timekepeingResult = timesheetResult.TimeKeepingsInMonth.Where(tk => tk.WorkDate == time.ToString(Constants.FORMAT_DATE_DDMMYYYY)).FirstOrDefault();
                        //LeaveDate leaveDate = timesheetResult.LeaveDates.Where(ld => ld.Date == time).FirstOrDefault();
                        //if (!(leaveDate != null &&
                        //    ((timesheetResult.Script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) ||
                        //    (timesheetResult.Script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK))))
                        //{
                        if (timekepeingResult != null && timekepeingResult.Details?.Count() != 0)
                        {
                            checkinTime = timekepeingResult.Details[0].CheckInTime?.Substring(0, 5);
                            checkoutTime = timekepeingResult.Details[0].CheckOutTime?.Substring(0, 5);
                        }
                        //}

                        dataRow[day * 2 + dateOffset - 1] = (checkinTime, null);
                        dataRow[day * 2 + dateOffset] = (checkoutTime, null);
                    }

                    // Tổng giờ (cell 68)
                    dataRow[67] = (Math.Round(timesheetResult.TotalWorkSeconds / 3600, 2).ToString(CultureInfo.InvariantCulture), null);

                    dataInRows.Add(dataRow);

                }
            }

            return dataInRows;
        }

        private List<(string, ICellStyle)[]> ConvertLeaveProcess(List<TimesheetResult> listTimesheetResult, DateTime startDate, DateTime endDate, string unPaidLeaveCode, XSSFWorkbook hssfwb)
        {
            // Styling
            byte[] leaveRgb = new byte[3] { 60, 120, 216 };
            byte[] weekendRgb = new byte[3] { 147, 196, 125 };
            byte[] holidayRgb = new byte[3] { 217, 217, 217 };

            XSSFCellStyle leaveCellStyle = (XSSFCellStyle)hssfwb.CreateCellStyle();
            leaveCellStyle.SetFillForegroundColor(new XSSFColor(leaveRgb));
            leaveCellStyle.FillPattern = FillPattern.SolidForeground;

            XSSFCellStyle weekendCellStyle = (XSSFCellStyle)hssfwb.CreateCellStyle();
            weekendCellStyle.SetFillForegroundColor(new XSSFColor(weekendRgb));
            weekendCellStyle.FillPattern = FillPattern.SolidForeground;

            XSSFCellStyle holidayCellStyle = (XSSFCellStyle)hssfwb.CreateCellStyle();
            holidayCellStyle.SetFillForegroundColor(new XSSFColor(holidayRgb));
            holidayCellStyle.FillPattern = FillPattern.SolidForeground;

            int dateOffset = 5;
            List<(string, ICellStyle)[]> dataInRows = new List<(string, ICellStyle)[]>();
            foreach (TimesheetResult timesheetResult in listTimesheetResult)
            {
                if (timesheetResult.User != null)
                {

                    (string, ICellStyle)[] dataRow = new (string, ICellStyle)[38];
                    dataRow[0] = (timesheetResult.User.FullName, null);
                    dataRow[1] = (timesheetResult.User.EmployeeCode.ToString(), null);
                    dataRow[2] = (timesheetResult.User.Email, null);
                    dataRow[3] = (timesheetResult.User.Corporation, null);
                    dataRow[4] = (timesheetResult.User.Department, null);
                    // Tong ngay nghi
                    dataRow[5] = (timesheetResult.ApprovalDates.Sum(ar => ar.TotalPaidLeave).ToString(CultureInfo.InvariantCulture), null);
                    // ngày 1 - 31 (cell 7 - 38)(item 6 - 37)
                    // duyệt công làm hàng ngày
                    foreach (DateTime time in STHelper.EachDay(startDate, endDate))
                    {
                        int day = time.Day;
                        string value = string.Empty;
                        ICellStyle style = null;

                        List<ApprovalDateResult> approvalDates = timesheetResult.ApprovalDates.Where(ad => ad.WorkDate == time.ToString(Constants.FORMAT_DATE_DDMMYYYY)).ToList();
                        LeaveDate leaveDate = timesheetResult.LeaveDates.Where(ld => ld.Date == time).FirstOrDefault();
                        if (leaveDate != null && ((timesheetResult.Script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (timesheetResult.Script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK)))
                        {
                            value = leaveDate.ExportTimeKeepingCode;
                            style = value == LeaveDate.WK ? weekendCellStyle : holidayCellStyle;
                        }
                        else
                        {
                            if (approvalDates != null)
                            {
                                foreach (ApprovalDateResult record in approvalDates)
                                {
                                    if (record.TotalPaidLeave == 1)
                                    {
                                        value = record.ReasonExportCode;
                                    }
                                    else if (record.TotalPaidLeave == 0.5)
                                    {
                                        value += "0.5" + record.ReasonExportCode;
                                    }

                                    if (record.TotalUnPaidLeave == 1)
                                    {
                                        value = unPaidLeaveCode;
                                    }
                                    else if (record.TotalUnPaidLeave == 0.5)
                                    {
                                        value += string.IsNullOrEmpty(value) ? "" : "+";
                                        value += "0.5" + unPaidLeaveCode;
                                    }
                                }
                            }
                            if (string.IsNullOrEmpty(value))
                            {
                                value = "-";
                            }
                            else
                            {
                                style = leaveCellStyle;
                            }
                        }
                        dataRow[day + dateOffset] = (value, style);
                    }
                    dataInRows.Add(dataRow);

                }
            }

            return dataInRows;
        }

        private List<(string, ICellStyle)[]> ConvertDataVNProcess(List<TimesheetResult> listTimesheetResult, DateTime startDate, DateTime endDate, int employeeTypeCTV)
        {
            int dateOffset = 8;
            List<(string, ICellStyle)[]> dataInRows = new List<(string, ICellStyle)[]>();
            foreach (TimesheetResult timesheetResult in listTimesheetResult)
            {
                if (timesheetResult.User != null)
                {

                    (string, ICellStyle)[] dataRow = new (string, ICellStyle)[49];
                    dataRow[0] = (timesheetResult.User.FullName, null);
                    dataRow[1] = (timesheetResult.User.EmployeeCode.ToString(), null);
                    dataRow[2] = (timesheetResult.User.Email, null);
                    dataRow[3] = (timesheetResult.User.Corporation, null);
                    dataRow[4] = (timesheetResult.User.Department, null);
                    // Công định mức 
                    dataRow[5] = (timesheetResult.QuotasTimeKeepingInMonth.ToString(), null);
                    // Tổng công
                    dataRow[6] = ((timesheetResult.TotalNumberOfWorkDay + timesheetResult.TotalNumberOfLeaveDay).ToString(CultureInfo.InvariantCulture), null);
                    // Tổng công đi làm 
                    dataRow[7] = (timesheetResult.TotalNumberOfWorkDay.ToString(CultureInfo.InvariantCulture), null);
                    // Tổng công nghỉ phép
                    dataRow[8] = (timesheetResult.TotalNumberOfLeaveDay.ToString(CultureInfo.InvariantCulture), null);

                    // ngày 1 - 31 (cell 10 - 40)(item 9 - 39)
                    // duyệt công làm hàng ngày
                    foreach (DateTime time in STHelper.EachDay(startDate, endDate))
                    {
                        int day = time.Day;
                        string value = string.Empty;
                        List<ApprovalDateResult> approvalDates = timesheetResult.ApprovalDates.Where(ad => ad.WorkDate == time.ToString(Constants.FORMAT_DATE_DDMMYYYY)).ToList();
                        TimeKeepingResult timekepeingResult = timesheetResult.TimeKeepingsInMonth.Where(tk => tk.WorkDate == time.ToString(Constants.FORMAT_DATE_DDMMYYYY)).FirstOrDefault();
                        LeaveDate leaveDate = timesheetResult.LeaveDates.Where(ld => ld.Date == time).FirstOrDefault();
                        if (leaveDate != null && ((timesheetResult.Script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (timesheetResult.Script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK)))
                        {
                            value = leaveDate.ExportTimeKeepingCode;
                        }
                        else
                        {
                            // Nếu là ctv
                            if (employeeTypeCTV == timesheetResult.Script.EmployeeTypeId)
                            {
                                if (timekepeingResult != null)
                                {
                                    value = "x";
                                }
                            }
                            else // Nếu là nhân viên
                            {
                                if (timekepeingResult != null)
                                {
                                    if (timekepeingResult.NumberOfWorkDay == 1)
                                    {
                                        value = "x";
                                    }
                                    else if (timekepeingResult.NumberOfWorkDay == 0.5)
                                    {
                                        value = "0.5";
                                    }
                                }

                                if (approvalDates != null)
                                {
                                    foreach (ApprovalDateResult record in approvalDates)
                                    {
                                        if (record.TotalPaidLeave == 1)
                                        {
                                            value = record.ReasonExportCode;
                                        }
                                        else if (record.TotalPaidLeave == 0.5)
                                        {
                                            value += string.IsNullOrEmpty(value) ? "" : "+";
                                            value += "0.5" + record.ReasonExportCode;
                                        }
                                    }
                                }
                            }
                        }
                        if (value.Contains("+"))
                        {
                            string[] items = value.Split("+");
                            if (items.Count() == 2 && items[0] == items[1])
                            {
                                value = items[0].Replace("0.5", "");
                            }
                        }
                        dataRow[day + dateOffset] = (value, null);
                    }

                    // duyệt tổng overtime (cell 40 - 42)
                    double totalOvertime150 = 0, totalOvertime200 = 0, totalOvertime300 = 0;
                    foreach (var (totalSeconds, description, percentage) in timesheetResult.TotalOverTime)
                    {
                        if (percentage == 150)
                        {
                            totalOvertime150 += Math.Round(totalSeconds / 3600, 2);
                        }
                        else if (percentage == 200)
                        {
                            totalOvertime200 += Math.Round(totalSeconds / 3600, 2);
                        }
                        else if (percentage == 300)
                        {
                            totalOvertime300 += Math.Round(totalSeconds / 3600, 2);
                        }
                    }
                    dataRow[40] = (totalOvertime150.ToString(CultureInfo.InvariantCulture), null);
                    dataRow[41] = (totalOvertime200.ToString(CultureInfo.InvariantCulture), null);
                    dataRow[42] = (totalOvertime300.ToString(CultureInfo.InvariantCulture), null);
                    dataRow[43] = (string.Empty, null);
                    dataRow[44] = (string.Empty, null);
                    dataRow[45] = (string.Empty, null);
                    //Số ngày ăn ca (cell 47)
                    dataRow[46] = (timesheetResult.TotalShiftEat.ToString(CultureInfo.InvariantCulture), null);
                    //Kịch bản (cell 48)
                    dataRow[47] = (timesheetResult.Script.SubScript, null);
                    // Tổng giờ (cell 49)
                    dataRow[48] = (Math.Round(timesheetResult.TotalWorkSeconds / 3600, 2).ToString(CultureInfo.InvariantCulture), null);

                    dataInRows.Add(dataRow);

                }
            }

            return dataInRows;
        }

        private List<(string, ICellStyle)[]> ConvertDataTLProcess(List<TimesheetResult> listTimesheetResult, DateTime startDate, DateTime endDate, XSSFWorkbook hssfwb)
        {
            // Styling
            byte[] leaveRgb = new byte[3] { 60, 120, 216 };
            byte[] weekendRgb = new byte[3] { 147, 196, 125 };
            byte[] holidayRgb = new byte[3] { 217, 217, 217 };

            XSSFCellStyle leaveCellStyle = (XSSFCellStyle)hssfwb.CreateCellStyle();
            leaveCellStyle.SetFillForegroundColor(new XSSFColor(leaveRgb));
            leaveCellStyle.FillPattern = FillPattern.SolidForeground;

            XSSFCellStyle weekendCellStyle = (XSSFCellStyle)hssfwb.CreateCellStyle();
            weekendCellStyle.SetFillForegroundColor(new XSSFColor(weekendRgb));
            weekendCellStyle.FillPattern = FillPattern.SolidForeground;

            XSSFCellStyle holidayCellStyle = (XSSFCellStyle)hssfwb.CreateCellStyle();
            holidayCellStyle.SetFillForegroundColor(new XSSFColor(holidayRgb));
            holidayCellStyle.FillPattern = FillPattern.SolidForeground;

            int dateOffset = 8;
            List<(string, ICellStyle)[]> dataInRows = new List<(string, ICellStyle)[]>();
            foreach (TimesheetResult timesheetResult in listTimesheetResult)
            {
                if (timesheetResult.User != null)
                {

                    (string, ICellStyle)[] dataRow = new (string, ICellStyle)[51];
                    dataRow[0] = (timesheetResult.User.FullName, null);
                    dataRow[1] = (timesheetResult.User.EmployeeCode.ToString(), null);
                    dataRow[2] = (timesheetResult.User.Email, null);
                    dataRow[3] = (timesheetResult.User.Corporation, null);
                    dataRow[4] = (timesheetResult.User.Department, null);
                    // Công định mức 
                    dataRow[5] = (timesheetResult.QuotasTimeKeepingInMonth.ToString(), null);
                    // Tổng công
                    dataRow[6] = ((timesheetResult.TotalNumberOfWorkDay + timesheetResult.TotalNumberOfLeaveDay).ToString(CultureInfo.InvariantCulture), null);
                    // Tổng công đi làm 
                    dataRow[7] = (timesheetResult.TotalNumberOfWorkDay.ToString(CultureInfo.InvariantCulture), null);
                    // Tổng công nghỉ phép
                    dataRow[8] = (timesheetResult.TotalNumberOfLeaveDay.ToString(CultureInfo.InvariantCulture), null);
                    // ngày 1 - 31 (cell 10 - 40)(item 9 - 39)
                    // duyệt công làm hàng ngày
                    foreach (DateTime time in STHelper.EachDay(startDate, endDate))
                    {
                        int day = time.Day;
                        string value = string.Empty;
                        ICellStyle style = null;
                        List<ApprovalDateResult> approvalDates = timesheetResult.ApprovalDates.Where(ad => ad.WorkDate == time.ToString(Constants.FORMAT_DATE_DDMMYYYY)).ToList();
                        TimeKeepingResult timekepeingResult = timesheetResult.TimeKeepingsInMonth.Where(tk => tk.WorkDate == time.ToString(Constants.FORMAT_DATE_DDMMYYYY)).FirstOrDefault();
                        LeaveDate leaveDate = timesheetResult.LeaveDates.Where(ld => ld.Date == time).FirstOrDefault();
                        if (leaveDate != null &&
                            ((timesheetResult.Script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) ||
                            (timesheetResult.Script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK)))
                        {
                            value = leaveDate.ExportTimeKeepingCode;
                            style = value == LeaveDate.WK ? weekendCellStyle : holidayCellStyle;
                        }
                        else
                        {
                            if (timekepeingResult != null)
                            {
                                if (timekepeingResult.NumberOfWorkDay == 1)
                                {
                                    value = "1";
                                }
                                else if (timekepeingResult.NumberOfWorkDay == 0.5)
                                {
                                    value = "0.5";
                                }
                            }

                            if (approvalDates != null)
                            {
                                foreach (ApprovalDateResult record in approvalDates)
                                {
                                    if (record.TotalPaidLeave == 1)
                                    {
                                        value = record.ReasonExportCode;
                                    }
                                    else if (record.TotalPaidLeave == 0.5)
                                    {
                                        value += string.IsNullOrEmpty(value) ? "" : "+";
                                        value += "0.5" + record.ReasonExportCode;
                                    }
                                }
                                if (approvalDates.Count > 0)
                                {
                                    style = leaveCellStyle;
                                }
                            }
                        }

                        if (value.Contains("+"))
                        {
                            string[] items = value.Split("+");
                            if (items.Count() == 2 && items[0] == items[1])
                            {
                                value = items[0].Replace("0.5", "");
                            }
                        }
                        dataRow[day + dateOffset] = (value, style);
                    }

                    // duyệt tổng overtime (cell 41 - 43)
                    double totalOvertime150 = 0, totalOvertime200 = 0, totalOvertime300 = 0;
                    foreach (var (totalSeconds, description, percentage) in timesheetResult.TotalOverTime)
                    {
                        if (percentage == 150)
                        {
                            totalOvertime150 += Math.Round(totalSeconds / 3600, 2);
                        }
                        else if (percentage == 200)
                        {
                            totalOvertime200 += Math.Round(totalSeconds / 3600, 2);
                        }
                        else if (percentage == 300)
                        {
                            totalOvertime300 += Math.Round(totalSeconds / 3600, 2);
                        }
                    }
                    dataRow[40] = (totalOvertime150.ToString(CultureInfo.InvariantCulture), null);
                    dataRow[41] = (totalOvertime200.ToString(CultureInfo.InvariantCulture), null);
                    dataRow[42] = (totalOvertime300.ToString(CultureInfo.InvariantCulture), null);
                    dataRow[43] = (string.Empty, null);
                    dataRow[44] = (string.Empty, null);
                    dataRow[45] = (string.Empty, null);
                    //Số ngày ăn ca (cell 47)
                    dataRow[46] = (timesheetResult.TotalShiftEat.ToString(CultureInfo.InvariantCulture), null);
                    //Phép tồn 2018 (cell 48)
                    dataRow[47] = (timesheetResult.ThailandRemainLeave[0].ToString(CultureInfo.InvariantCulture), null);
                    //Phép tồn 2019 (cell 49)
                    dataRow[48] = (timesheetResult.ThailandRemainLeave[1].ToString(CultureInfo.InvariantCulture), null);
                    //Kịch bản (cell 50)
                    dataRow[49] = (timesheetResult.Script.SubScript, null);
                    // Tổng giờ (cell 51)
                    dataRow[50] = (Math.Round(timesheetResult.TotalWorkSeconds / 3600, 2).ToString(CultureInfo.InvariantCulture), null);

                    dataInRows.Add(dataRow);

                }
            }

            return dataInRows;
        }

    }
}
