﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.Pagination;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public class CommonManager
    {
        protected readonly ApplicationDbContext context;
        protected readonly LeaveDateService leaveDateService;
        protected readonly LeaveCalendarTypeHistoryService leaveCalendarTypeHistoryService;
        public CommonManager(ApplicationDbContext context)
        {
            this.context = context;
            leaveDateService = new LeaveDateService(context);
            leaveCalendarTypeHistoryService = new LeaveCalendarTypeHistoryService(context);
        }

        protected (double totalPaidLeave, double totalUnPaidLeave) CalcTotalLeaveInDate(int userId, bool hasHoliday, bool hasLeaveWeekend, ApprovalRecord approvalRecord, DateTime day)
        {
            double paidLeave = 0;
            double unPaidLeave = 0;
            (double leaveDateNumber, int holidayNumber, int weekendNumber) = GetLeaveDate(userId, hasHoliday, hasLeaveWeekend,
                approvalRecord.IsManyDate, approvalRecord.IsMorning, approvalRecord.IsAfternoon, approvalRecord.StartTime, day, out List<DateTime> listLeaveDate);
            // Nghỉ một ngày
            if (!approvalRecord.IsManyDate)
            {
                paidLeave = approvalRecord.TotalPaidLeave;
                unPaidLeave = approvalRecord.TotalUnpaidLeave;
            }
            else // Nghỉ nhiều ngày
            {
                if (approvalRecord.TotalPaidLeave >= leaveDateNumber)
                {
                    paidLeave = 1;
                }
                else if (approvalRecord.TotalPaidLeave == leaveDateNumber - 0.5)
                {
                    paidLeave = 0.5;
                    unPaidLeave = 0.5;
                }
                else
                {
                    unPaidLeave = 1;
                }
            }
            return (paidLeave, unPaidLeave);
        }

        public (double LeaveDateNumber, int HolidayNumber, int WeekendNumber) GetLeaveDate(int userId, bool hasHoliday, bool hasLeaveWeekend,
         bool isManyDate, bool isMorning, bool isAfternoon, DateTime startDate, DateTime endDate, out List<DateTime> listLeaveDate)
        {
            double leaveDateNumber = 0;
            (int holidayNumber, int weekendNumber) = GetNumberHoliday(userId, startDate, endDate, out listLeaveDate);
            if (isManyDate)
            {
                leaveDateNumber = endDate.Subtract(startDate).Days + 1;
            }
            else
            {
                if (isMorning && isAfternoon)
                {
                    leaveDateNumber = 1;
                }
                else if (isMorning || isAfternoon)
                {
                    leaveDateNumber = 0.5;
                }
                else
                {
                    leaveDateNumber = 0;
                }
            }
            leaveDateNumber = leaveDateNumber - holidayNumber - weekendNumber;
            return (leaveDateNumber > 0 ? leaveDateNumber : 0, holidayNumber, weekendNumber);
        }

        protected (int HolidayNumber, int WeekendNumber) GetNumberHoliday(int userId, DateTime startDate, DateTime endDate, out List<DateTime> listLeaveDate)
        {
            int holidayNumber = 0;
            int weekendNumber = 0;
            listLeaveDate = new List<DateTime>();
            // Check user có nghỉ lễ và hôm xin nghỉ là ngày lễ hoặc user có nghỉ cuối tuần và hôm xin nghỉ là cuối tuần
            foreach (DateTime day in STHelper.EachDay(startDate, endDate))
            {
                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(userId, day);
                LeaveDate leaveDate = leaveDateService.GetByDateAndType(day, leaveCalendarTypeHistory.Code);
                if ((day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday))
                {
                    weekendNumber++;
                }
                else if (leaveDate != null && leaveDate.ExportTimeKeepingCode != LeaveDate.WK)
                {
                    holidayNumber++;
                }
                else
                {
                    listLeaveDate.Add(day);
                }
            }
            return (holidayNumber, weekendNumber);
        }

        protected int GetWeekendNumber(DateTime startDate, DateTime endDate)
        {
            int weekendNumber = 0;
            foreach (DateTime day in STHelper.EachDay(startDate, endDate))
            {
                if ((day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday))
                {
                    weekendNumber++;
                }
            }
            return weekendNumber;
        }

        protected bool CheckLeaveDate(int userId, bool hasHoliday, bool hasLeaveWeekend, DateTime workDate)
        {
            // neu user co ngay nghi le và hôm nay là ngày nghỉ lễ
            // neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
            // Không cần kiểm tra thông tin checkin checkout đúng giờ
            LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(userId, workDate);
            return (hasHoliday && context.LeaveDates.Where(ld => ld.Type == leaveCalendarTypeHistory.Code && ld.Date == workDate).FirstOrDefault() != null)
                || (hasLeaveWeekend && (workDate.DayOfWeek == DayOfWeek.Saturday || workDate.DayOfWeek == DayOfWeek.Sunday));
        }


        public static void WriteToSheet(ref ISheet sheet, List<(string, ICellStyle)[]> dataInRows, int startRow)
        {
            int addedRow = 0;
            foreach ((string, ICellStyle)[] row in dataInRows)
            {
                int curRow = startRow + addedRow;
                IRow newRow = sheet.CreateRow(curRow);
                int curCell = 1;
                newRow.CreateCell(0).SetCellValue(curRow - 1);
                foreach ((string Value, ICellStyle Style) dataCell in row)
                {
                    ICell cell = newRow.CreateCell(curCell);
                    cell.SetCellValue(dataCell.Value);
                    if (dataCell.Style != null)
                    {
                        cell.CellStyle = dataCell.Style;
                    }
                    curCell++;
                }
                addedRow++;
            }
        }


    }
}
