﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public class OvertimeManager
    {
        private readonly ApplicationDbContext context;
        private readonly TimeKeepingService timeKeepingService;
        private readonly TimeKeepingDetailService timeKeepingDetailService;
        private readonly ScriptService scriptService;
        private readonly OverTimeService overTimeService;
        private readonly OverTimeDetailService overTimeDetailService;
        private readonly UserService userService;
        private readonly ShiftsEatService shiftsEatService;
        private readonly EventService eventService;
        private readonly LeaveDateService leaveDateService;
        private readonly EstimateOwnerService estimateOwnerService;
        private readonly ApprovalRecordService approvalRecordService;
        private readonly GroupEventService groupEventService;
        private readonly ChangeManagerInfoService changeManagerInfoService;
        private readonly LeaveCalendarTypeHistoryService leaveCalendarTypeHistoryService;
        public OvertimeManager(ApplicationDbContext context)
        {
            this.context = context;
            timeKeepingService = new TimeKeepingService(context);
            timeKeepingDetailService = new TimeKeepingDetailService(context);
            userService = new UserService(context);
            scriptService = new ScriptService(context);
            overTimeService = new OverTimeService(context);
            overTimeDetailService = new OverTimeDetailService(context);
            shiftsEatService = new ShiftsEatService(context);
            eventService = new EventService(context);
            leaveDateService = new LeaveDateService(context);
            estimateOwnerService = new EstimateOwnerService(context);
            approvalRecordService = new ApprovalRecordService(context);
            groupEventService = new GroupEventService(context);
            changeManagerInfoService = new ChangeManagerInfoService(context);
            leaveCalendarTypeHistoryService = new LeaveCalendarTypeHistoryService(context);
        }

        public (bool HasOverTime, OverTimeDetail OverTimeDetail) GetStartOverTime(User user, Script script, DateTime checkInTime, DateTime checkOutTime, bool isManual, int? timeKeepingDetailId, int? eventId)
        {
            bool hasOverTime = false;
            OverTimeDetail overTimeDetail = null;

            DateTime workDate = checkInTime.Date;
            if (script.HasOverTime)
            {
                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
                LeaveDate leaveDateToday = leaveDateService.GetByDateAndType(workDate, leaveCalendarTypeHistory.Code);
                // Neu user co ngay nghi le và hôm đó là ngày nghỉ lễ
                // Neu user co ngay nghi cuoi tuan và hôm đó là t7 hoặc cn
                if (leaveDateToday != null && ((script.HasHoliday && leaveDateToday.ExportTimeKeepingCode != LeaveDate.WK) || (script.HasLeaveWeekend && leaveDateToday.ExportTimeKeepingCode == LeaveDate.WK)))
                {
                    // Phải OT nhiều hơn số phút tối thiểu
                    double overTimeMinutes = CalcOverTimeMinutes(checkInTime, checkOutTime, script.LunchBreak, isManual);
                    hasOverTime = script.MinOverTimeMinutes.HasValue ? overTimeMinutes >= script.MinOverTimeMinutes.Value : true;
                    if (hasOverTime)
                    {
                        switch (script.CorporationType)
                        {
                            case CorporationType.CorporationTH:
                            case CorporationType.CorporationVI:
                            default:
                                overTimeDetail = new OverTimeDetail
                                {
                                    StartTime = checkInTime,
                                    EndTime = checkOutTime,
                                    EventId = eventId ?? leaveDateToday.EventId
                                };
                                break;
                        }
                    }
                }
                // Nếu là ngày làm việc bình thường
                else
                {
                    hasOverTime = true;
                    DateTime startOverTime = DateTime.MaxValue;
                    DateTime checkOutMax = checkOutTime;
                    // Phải làm đủ giờ 
                    if (script.EnoughHour != null)
                    {
                        TimeKeeping timeKeeping = timeKeepingService.GetTimeKeepingOfEmployeeByWorkDate(user.Id, workDate);

                        double totalSeconds = 0;
                        if (timeKeeping != null)
                        {
                            foreach (TimeKeepingDetail detail in timeKeepingDetailService.GetFinishTimeKeepingByWorkDate(timeKeeping.Id))
                            {
                                (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(detail.CheckOutTime.Value, detail.CheckInTime.Value, script.LunchBreak);
                                totalSeconds += (morningSeconds + afternoonSeconds);
                                checkOutMax = checkOutMax < detail.CheckOutTime.Value ? detail.CheckOutTime.Value : checkOutMax;
                            }
                        }
                        if (!timeKeepingDetailId.HasValue)
                        {
                            (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(checkOutTime, checkInTime, script.LunchBreak);
                            totalSeconds += (morningSeconds + afternoonSeconds);
                            checkOutMax = checkOutMax < checkOutTime ? checkOutTime : checkOutMax;
                        }

                        double enoughtHour = script.EnoughHour.Value * 60 * 60;
                        // Kiểm tra xem có đơn nghỉ phép hôm nay không
                        List<ApprovalRecord> approvalRecords = approvalRecordService.GetApprovalRecordByDate(user, workDate);

                        if (approvalRecords != null && approvalRecords.Count > 0)
                        {
                            foreach (ApprovalRecord record in approvalRecords)
                            {
                                if (record.IsManyDate || (record.IsMorning && record.IsAfternoon))
                                {
                                    enoughtHour = 0;
                                }
                                else if (record.IsMorning || record.IsAfternoon)
                                {
                                    enoughtHour -= enoughtHour / 2;
                                }
                            }
                        }
                        enoughtHour = enoughtHour > 0 ? enoughtHour : 0;
                        hasOverTime = totalSeconds > enoughtHour;
                        if (hasOverTime)
                        {
                            startOverTime = checkOutTime.AddSeconds(enoughtHour - totalSeconds);
                        }
                    }

                    // Phải qua giờ làm thêm
                    if (hasOverTime && script.OverTimeLine != null)
                    {
                        hasOverTime = string.Compare(script.OverTimeLine, checkOutMax.ToString(Constants.FORMAT_TIME_HHMMSS)) < 0;
                        if (hasOverTime)
                        {
                            string timeOverText = checkOutTime.ToString(Constants.FORMAT_DATE_DDMMYYYY) + " " + script.OverTimeLine;
                            CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
                            DateTime tempOver = DateTime.ParseExact(timeOverText, Constants.FORMAT_DATE_DDMMYYYYHHMMSS, customCulture);
                            startOverTime = (startOverTime != DateTime.MaxValue && tempOver < startOverTime) ? startOverTime : tempOver;
                        }
                    }

                    // Phải OT nhiều hơn số phút tối thiểu
                    if (hasOverTime && script.MinOverTimeMinutes.HasValue)
                    {
                        double overTimeMinutes = CalcOverTimeMinutes(startOverTime, checkOutMax, script.LunchBreak, isManual);
                        hasOverTime = overTimeMinutes >= script.MinOverTimeMinutes.Value;
                    }

                    if (hasOverTime)
                    {
                        string eventCode = script.CorporationType == CorporationType.CorporationTH ? Event.NT_OT150 : Event.NV_OTDL;

                        overTimeDetail = new OverTimeDetail
                        {
                            StartTime = startOverTime,
                            EndTime = checkOutMax,
                            EventId = eventId ?? eventService.GetByCode(eventCode).Id
                        };
                    }
                }
            }
            return (hasOverTime, overTimeDetail);
        }

        public OverTimeDetail Update(OverTimeDetail detail)
        {
            return overTimeDetailService.Update(detail);
        }

        public IEnumerable<OverTimeDetail> GetPendingOverTimeLastMonth(DateTime startDate, DateTime endDate, string corporationType)
        {
            IEnumerable<OverTimeDetail> result = overTimeDetailService.GetPendingOverTimeLastMonth(startDate, endDate, corporationType);
            return result;
        }

        public double CalcOverTimeMinutes(DateTime startOverTime, DateTime checkOutMax, int lunchBreak, bool isManual)
        {
            double overTimeMinutes = 0;
            if (isManual)
            {
                overTimeMinutes = checkOutMax.Subtract(startOverTime).TotalMinutes;
            }
            else
            {
                (double morningOverTime, double affternoonOverTime) = STHelper.CalculationWorkSeconds(checkOutMax, startOverTime, lunchBreak);
                overTimeMinutes = (morningOverTime + affternoonOverTime) / 60;
            }
            return overTimeMinutes;
        }

        public int CountPendingOverTime(User user, bool isForManager, bool isDestroy)
        {
            return overTimeDetailService.CountPendingOverTime(user, isForManager, isDestroy);
        }

        public (bool bOk, string message, OverTimeDetail[] details) ConfirmOverTime(StatusOverTime status, int[] ids, User manager, bool isDestroy, string rejectReason = null)
        {
            return overTimeDetailService.ConfirmOverTime(status, ids, manager, isDestroy, rejectReason);
        }

        public bool CheckConflictTime(string email, DateTime startTime, DateTime endTime, int? id)
        {
            DateTime workDate = startTime.Date;
            OverTime overTime = overTimeService.GetByWorkDate(email, workDate);
            if (overTime == null)
            {
                return false;
            }
            // Lấy danh sách làm thêm giờ trong ngày tạm hợp lệ (Khác reject và đã hủy)
            List<OverTimeDetail> overTimeDetails = overTimeDetailService.GetValidOverTimeDetailInDay(overTime.Id, id);

            foreach (OverTimeDetail overTimeDetail in overTimeDetails)
            {
                if (!(endTime <= overTimeDetail.StartTime || startTime >= overTimeDetail.EndTime))
                {
                    return true;
                }
            }
            return false;
        }

        public (bool bOk, string message) DestroyOverTime(int[] ids, User user)
        {
            return overTimeDetailService.DestroyOverTime(ids, user);
        }

        public async Task<MemoryStream> ExportOvertimeDetail(string nationality, DateTime start, DateTime end)
        {
            XSSFWorkbook hssfwb;

           

            Dictionary<StatusOverTime, string> statusMap = new Dictionary<StatusOverTime, string>
            {
                {StatusOverTime.Pending, "Đang chờ duyệt" },
                {StatusOverTime.Approved, "Đã chấp nhận" },
                {StatusOverTime.Rejected, "Không chấp nhận" },
                {StatusOverTime.Expired, "Quá hạn duyệt" }
            };

            using (FileStream file = new FileStream(@"Templates\ExportOvertimeDetail.xlsx", FileMode.Open, FileAccess.Read))
            {
                hssfwb = new XSSFWorkbook(file);
                file.Close();
            }
            // Lấy sheet Template bảng công - sheet đầu tiên
            ISheet sheet = hssfwb.GetSheetAt(0);
            IRow row = sheet.GetRow(0);
            row.CreateCell(0).SetCellValue("TOPICA OSSCAR CHECKIN");
            int startRow = 5;
            int addedRow = 0;

            IEnumerable<(int UserId, string Email, string FullName, string Department, string CalendarType, DateTime WorkDate, OverTimeDetail Detail, string GroupEventName, string EventCode, int? Percentage, double? PayPerHour, int lunchBreak)> result
                = overTimeDetailService.GetOvertimeForExport(start, end);
            foreach ((int userId, string email, string fullName, string department, string calendarType, DateTime workDate, OverTimeDetail detail, string groupEventName, string eventCode, int? percentage, double? payPerHour, int lunchBreak) in result)
            {
                int curRow = startRow + addedRow;
                IRow newRow = sheet.CreateRow(curRow);
                newRow.CreateCell(0).SetCellValue(curRow - 4);
                newRow.CreateCell(1).SetCellValue(fullName);
                newRow.CreateCell(2).SetCellValue(userId);
                newRow.CreateCell(3).SetCellValue(email);
                newRow.CreateCell(5).SetCellValue(department);

                // Ngày làm thêm
                newRow.CreateCell(6).SetCellValue(detail.StartTime.Date);

                // Loại làm thêm
                newRow.CreateCell(7).SetCellValue(MessageManager.GetMessageManager(nationality).GetValue(groupEventName));

                // Mã làm thêm
                newRow.CreateCell(8).SetCellValue(eventCode);

                // Số giờ làm thêm
                double totalSeconds = 0;
                if (detail.IsManual)
                {
                    totalSeconds += detail.EndTime.Subtract(detail.StartTime).TotalSeconds;
                }
                else
                {
                    (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(detail.EndTime, detail.StartTime, lunchBreak);
                    totalSeconds += (morningSeconds + afternoonSeconds);
                }

                newRow.CreateCell(9).SetCellValue(Math.Round(totalSeconds / 3600, 2));
                newRow.CreateCell(10).SetCellValue(Constants.DAYOFWEEK[workDate.DayOfWeek]);
                LeaveDate leaveDateToday = leaveDateService.GetByDateAndType(workDate, calendarType);

                newRow.CreateCell(11).SetCellValue(leaveDateToday != null && leaveDateToday.ExportTimeKeepingCode != LeaveDate.WK ? "Có" : " Không");
                newRow.CreateCell(12).SetCellValue(detail.StartTime.ToString(Constants.FORMAT_TIME_HHMM));
                newRow.CreateCell(13).SetCellValue(detail.EndTime.ToString(Constants.FORMAT_TIME_HHMM));

                // Phần trăm làm thêm
                newRow.CreateCell(14).SetCellValue(percentage.HasValue ? percentage + "%" : string.Empty);

                // Thù lao theo giờ
                newRow.CreateCell(15).SetCellValue(payPerHour.HasValue ? (payPerHour * 1000).ToString() : string.Empty);

                // Thù lao theo block
                //newRow.CreateCell(16).SetCellValue(detail.Event.Percentage.HasValue ? detail.Event.Percentage + "%" : string.Empty);

                newRow.CreateCell(17).SetCellValue(detail.Note);
                newRow.CreateCell(18).SetCellValue(statusMap[detail.Status]);
                addedRow++;
            }
            string name = string.Format("OT_{0}{1}_{2}{3}_{4}{5}{6}{7}{8}{9}",
                start.Day, start.Month, end.Day, end.Month,
                DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            string temp_path = @"Exported\" + name + ".xlsx";
            using (FileStream file = new FileStream(temp_path, FileMode.Create, FileAccess.Write))
            {
                hssfwb.Write(file);
                file.Close();
            }

            var memory = new MemoryStream();
            using (var stream = new FileStream(temp_path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return memory;
        }

        public async Task<MemoryStream> ExportOvertimeDetail(Script script, DateTime start, DateTime end)
        {
            string nationality;
            XSSFWorkbook hssfwb;
            Dictionary<DayOfWeek, string> dayOfWeekMap;
            Dictionary<StatusOverTime, string> statusMap;
            string tempPath;
            if (script.CorporationType == CorporationType.CorporationTH)
            {
                nationality = "en";
                tempPath = @"Templates\ExportOvertimeThailand.xlsx";

                dayOfWeekMap = new Dictionary<DayOfWeek, string>
                {
                    { DayOfWeek.Monday, "Monday" },
                    { DayOfWeek.Tuesday, "Tuesday" },
                    { DayOfWeek.Wednesday, "Wednesday" },
                    { DayOfWeek.Thursday, "Thursday" },
                    { DayOfWeek.Friday, "Friday" },
                    { DayOfWeek.Saturday, "Saturday" },
                    { DayOfWeek.Sunday, "Sunday" }
                };
                statusMap = new Dictionary<StatusOverTime, string>
                {
                    {StatusOverTime.Pending, "Pending" },
                    {StatusOverTime.Approved, "Approved" },
                    {StatusOverTime.Rejected, "Rejected" },
                    {StatusOverTime.Expired, "Expired" }
                };
            }
            else
            {
                nationality = "vi";
                tempPath = @"Templates\ExportOvertimeDetail.xlsx";
                dayOfWeekMap = new Dictionary<DayOfWeek, string>
                {
                    { DayOfWeek.Monday, "Thứ hai" },
                    { DayOfWeek.Tuesday, "Thứ ba" },
                    { DayOfWeek.Wednesday, "Thứ tư" },
                    { DayOfWeek.Thursday, "Thứ năm" },
                    { DayOfWeek.Friday, "Thứ sáu" },
                    { DayOfWeek.Saturday, "Thứ bẩy" },
                    { DayOfWeek.Sunday, "Chủ nhật" }
                };
                statusMap = new Dictionary<StatusOverTime, string>
                {
                    {StatusOverTime.Pending, "Đang chờ duyệt" },
                    {StatusOverTime.Approved, "Đã chấp nhận" },
                    {StatusOverTime.Rejected, "Không chấp nhận" },
                    {StatusOverTime.Expired, "Quá hạn duyệt" }
                };
            }
            using (FileStream file = new FileStream(tempPath, FileMode.Open, FileAccess.Read))
            {
                hssfwb = new XSSFWorkbook(file);
                file.Close();
            }
            // Lấy sheet Template bảng công - sheet đầu tiên
            ISheet sheet = hssfwb.GetSheetAt(0);
            IRow row = sheet.GetRow(0);
            row.CreateCell(0).SetCellValue("TOPICA OSSCAR CHECKIN - " + string.Format(MessageManager.GetMessageManager(nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(nationality).GetValue(script.Description)));
            int startRow = 5;
            int addedRow = 0;

            IEnumerable<(int UserId, string Email, string FullName, string Department, string CalendarType, DateTime WorkDate, OverTimeDetail Detail, string GroupEventName, string EventCode, int? Percentage, double? PayPerHour, int lunchBreak)> result
                = overTimeDetailService.GetOvertimeForExport(start, end, script.Id);

            foreach ((int userId, string email, string fullName, string department, string calendarType, DateTime workDate, OverTimeDetail detail, string groupEventName, string eventCode, int? percentage, double? payPerHour, int lunchBreak) in result)
            {
                int curRow = startRow + addedRow;
                IRow newRow = sheet.CreateRow(curRow);
                newRow.CreateCell(0).SetCellValue(curRow - 4);
                newRow.CreateCell(1).SetCellValue(fullName);
                newRow.CreateCell(2).SetCellValue(userId);
                newRow.CreateCell(3).SetCellValue(email);
                newRow.CreateCell(5).SetCellValue(department);

                // Ngày làm thêm
                newRow.CreateCell(6).SetCellValue(detail.StartTime.Date.ToString(Constants.FORMAT_DATE_MMDDYYYY));

                // Loại làm thêm
                newRow.CreateCell(7).SetCellValue(MessageManager.GetMessageManager(nationality).GetValue(groupEventName));

                // Mã làm thêm
                newRow.CreateCell(8).SetCellValue(eventCode);

                // Số giờ làm thêm
                double totalSeconds = 0;
                if (detail.IsManual)
                {
                    totalSeconds += detail.EndTime.Subtract(detail.StartTime).TotalSeconds;
                }
                else
                {
                    (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(detail.EndTime, detail.StartTime, lunchBreak);
                    totalSeconds += (morningSeconds + afternoonSeconds);
                }

                newRow.CreateCell(9).SetCellValue(Math.Round(totalSeconds / 3600, 2).ToString(CultureInfo.InvariantCulture));
                newRow.CreateCell(10).SetCellValue(dayOfWeekMap[workDate.DayOfWeek]);
                LeaveDate leaveDateToday = leaveDateService.GetByDateAndType(workDate, calendarType);

                newRow.CreateCell(11).SetCellValue(leaveDateToday != null && leaveDateToday.ExportTimeKeepingCode != LeaveDate.WK ? "Có" : " Không");
                newRow.CreateCell(12).SetCellValue(detail.StartTime.ToString(Constants.FORMAT_TIME_HHMM));
                newRow.CreateCell(13).SetCellValue(detail.EndTime.ToString(Constants.FORMAT_TIME_HHMM));

                // Phần trăm làm thêm
                newRow.CreateCell(14).SetCellValue(percentage.HasValue ? percentage + "%" : string.Empty);

                // Thù lao theo giờ
                newRow.CreateCell(15).SetCellValue(payPerHour.HasValue ? (payPerHour * 1000).ToString() : string.Empty);

                // Thù lao theo block
                //newRow.CreateCell(16).SetCellValue(detail.Event.Percentage.HasValue ? detail.Event.Percentage + "%" : string.Empty);

                newRow.CreateCell(17).SetCellValue(detail.Note);
                newRow.CreateCell(18).SetCellValue(statusMap[detail.Status]);
                addedRow++;
            }
            string name = string.Format("OT_{0}{1}_{2}{3}_{4}{5}{6}{7}{8}{9}",
                start.Day, start.Month, end.Day, end.Month,
                DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            string temp_path = @"Exported\" + name + ".xlsx";
            using (FileStream file = new FileStream(temp_path, FileMode.Create, FileAccess.Write))
            {
                hssfwb.Write(file);
                file.Close();
            }

            var memory = new MemoryStream();
            using (var stream = new FileStream(temp_path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return memory;
        }

        public void DestroyConflictOverTime(string email, DateTime startTime, DateTime endTime, int? currentId)
        {
            OverTime overTime = overTimeService.GetByWorkDate(email, startTime.Date);
            if (overTime == null)
            {
                return;
            }

            // Lấy danh sách làm thêm giờ trong ngày tạm hợp lệ (Khác reject và đã hủy) khác bản làm thêm hiện tại
            List<OverTimeDetail> overTimeDetails = overTimeDetailService.GetValidOverTimeDetailInDay(overTime.Id, currentId);

            foreach (OverTimeDetail overTimeDetail in overTimeDetails)
            {
                if (!(endTime <= overTimeDetail.StartTime || startTime >= overTimeDetail.EndTime))
                {
                    overTimeDetail.IsDestroy = true;
                    overTimeDetail.DestroyTime = DateTime.Now;
                    overTimeDetail.StatusDestroy = StatusOverTime.Approved;
                    overTimeDetail.ApprovalDestroyTime = DateTime.Now;
                    context.OverTimeDetails.Update(overTimeDetail);
                    context.SaveChanges();
                }
            }
        }

        public List<OverTimeResult> GetOvertime(int[] grouptEventIds, out int numberOfPages, out int totalRecords, STRequestModel request, User user, bool isForManager, bool isDestroy, string language)
        {
            List<OverTimeResult> result = new List<OverTimeResult>();
            foreach (OverTimeDetail overTimeDetail in overTimeDetailService.GetOvertime(grouptEventIds, out numberOfPages, out totalRecords, request, user, isForManager, isDestroy))
            {
                string remuneration = string.Empty;
                if (overTimeDetail.Event.EventCode == Event.NT_OT200_300)
                {
                    remuneration = "200% / 300%";
                }
                else if (overTimeDetail.Event.Percentage.HasValue)
                {
                    remuneration = overTimeDetail.Event.Percentage.ToString() + "%";
                }
                else if (overTimeDetail.Event.PayPerHour.HasValue)
                {
                    remuneration = (overTimeDetail.Event.PayPerHour * 1000).ToString() + "VND/h";
                }
                else if (overTimeDetail.Event.PayPerBlock.HasValue)
                {
                    remuneration = overTimeDetail.Event.PayPerBlock.ToString() + " Block";
                }

                string eventName = string.Empty;
                if (overTimeDetail.Event.ShowCode && !string.IsNullOrEmpty(overTimeDetail.Event.Description))
                {
                    eventName = string.Format("{0} ( {1} )", overTimeDetail.Event.EventCode, MessageManager.GetMessageManager(language).GetValue(overTimeDetail.Event.Description));
                }
                else if (overTimeDetail.Event.ShowCode)
                {
                    eventName = overTimeDetail.Event.EventCode;
                }
                else if (!string.IsNullOrEmpty(overTimeDetail.Event.Description))
                {
                    eventName = MessageManager.GetMessageManager(language).GetValue(overTimeDetail.Event.Description);
                }
                string updateAt = string.Empty;
                if (overTimeDetail.IsDestroy)
                {
                    switch (overTimeDetail.StatusDestroy)
                    {
                        case StatusOverTime.Approved:
                        case StatusOverTime.Rejected:
                            if (overTimeDetail.ApprovalDestroyTime.HasValue)
                            {
                                updateAt = overTimeDetail.ApprovalDestroyTime.Value.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS);
                            }
                            break;
                        case StatusOverTime.Expired:
                        case StatusOverTime.Pending:
                        default:
                            break;
                    }
                }
                else
                {
                    switch (overTimeDetail.Status)
                    {
                        case StatusOverTime.Approved:
                        case StatusOverTime.Rejected:
                            if (overTimeDetail.ApprovalTime.HasValue)
                            {
                                updateAt = overTimeDetail.ApprovalTime.Value.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS);
                            }
                            break;
                        case StatusOverTime.Expired:
                        case StatusOverTime.Pending:
                        default:
                            break;
                    }
                }
                // Lấy ra thông tin quản lý
                User manager = null;
                if (!overTimeDetail.IsDestroy && overTimeDetail.Status == StatusOverTime.Pending)
                {
                    manager = userService.GetById(overTimeDetail.OverTime.User.ManagerId);
                }
                else
                {
                    if (!string.IsNullOrEmpty(overTimeDetail.ApproverEmail))
                    {
                        manager = userService.GetByEmail(overTimeDetail.ApproverEmail);
                    }
                    else
                    {
                        ChangeManagerInfo changeManagerInfo = changeManagerInfoService.GetLastApprovalChangeManagerInfo(overTimeDetail.OverTime.UserId, overTimeDetail.CreatedAt.Date);
                        if (changeManagerInfo != null)
                        {
                            manager = userService.GetById(changeManagerInfo.NewManagerId);
                        }
                    }
                }

                result.Add(new OverTimeResult()
                {
                    Id = overTimeDetail.Id,
                    Name = overTimeDetail.OverTime.User.FullName,
                    Email = overTimeDetail.OverTime.User.Email,
                    ImageUrl = overTimeDetail.OverTime.User.ImageUrl,
                    Status = (int)overTimeDetail.Status,
                    WorkDate = overTimeDetail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                    StartTime = overTimeDetail.StartTime != null ? DateTime.Parse(overTimeDetail.StartTime.ToString()).ToString(Constants.FORMAT_TIME_HHMMSS) : string.Empty,
                    EndTime = overTimeDetail.EndTime != null ? DateTime.Parse(overTimeDetail.EndTime.ToString()).ToString(Constants.FORMAT_TIME_HHMMSS) : string.Empty,
                    Note = overTimeDetail.Note,
                    Event = eventName,
                    Remuneration = remuneration,
                    GroupEventName = MessageManager.GetMessageManager(language).GetValue(overTimeDetail.Event.GroupEvent.Name),
                    ManagerName = manager?.FullName ?? Constants.NA,
                    ManagerEmail = manager?.Email ?? Constants.NA,
                    CreatedAt = overTimeDetail.CreatedAt.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS),
                    ApprovalTime = overTimeDetail.ApprovalTime != null ? DateTime.Parse(overTimeDetail.ApprovalTime.ToString()).ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS) : string.Empty,
                    IsDestroy = isDestroy.ToString(),
                    StatusDestroy = (int)overTimeDetail.StatusDestroy,
                    UpdatedAt = updateAt,
                    RejectReason = overTimeDetail.RejectReason
                });
            }

            return result;
        }

        public List<OverTimeDetail> GetAllOvertimeInMonthByUser(int userId, int scriptId, DateTime startDate, DateTime endDate, string groupEventCode)
        {
            return overTimeDetailService.GetAllOvertimeInMonthByUser(userId, scriptId, startDate, endDate, groupEventCode);
        }

        public OverTimeDetail CreateOverTime(User user, Script script, DateTime workDate, OverTimeDetail overTimeDetail, bool isManual, string node, StatusOverTime status)
        {
            //check out, luu du lieu vao bang overtime
            OverTime overTime = overTimeService.GetByWorkDate(user.Email, workDate);
            if (overTime == null)
            {
                overTime = overTimeService.Create(user, script, workDate);
            }
            else if (overTime.ScriptId != script.Id)
            {
                overTime.ScriptId = script.Id;
                overTime = overTimeService.Update(overTime);
            }

            //Thêm data vào bảng OT
            overTimeDetail.Note = node;
            overTimeDetail.OverTimeId = overTime.Id;
            overTimeDetail.Status = status;
            overTimeDetail.IsManual = isManual;
            OverTimeDetail record = overTimeDetailService.Create(overTimeDetail);
            return record;
        }

        public (bool Ok, OverTimeDetail OverTimeDetails) ManualOverTime(User user, Script script, string note, DateTime workDate, DateTime startTime, DateTime endTime, int? eventId, bool isAdmin)
        {
            // Kiểm tra có lý do nghỉ không, nếu có check maxlenght
            if (string.IsNullOrEmpty(note) || note.Count() > int.Parse(AppSetting.Configuration.GetValue("MaxLenghtNote")))
            {
                return (false, null);
            }
            OverTime overTime = overTimeService.Create(user, script, workDate);
            if (overTime == null)
            {
                return (false, null);
            }
            OverTimeDetail overTimeDetail = null;
            (bool bOk, string[] statusAllowSkips) = ValidateOvertime(user.Email, workDate, eventId, startTime, endTime, script, null, isAdmin);
            if (statusAllowSkips.Contains(ResultCode.OVERTIME_CONFLICT))
            {
                DestroyConflictOverTime(user.Email, startTime, endTime, null);
            }

            if (!bOk)
            {
                return (false, null);
            }
            else
            {
                // Nếu là OT tính toán eventId
                if (eventId == null)
                {
                    LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, workDate);
                    LeaveDate leaveDate = leaveDateService.GetByDateAndType(workDate, leaveCalendarTypeHistory.Code);
                    // Làm thêm vào ngày lễ
                    // Xin làm thêm vào chủ nhật, t7
                    if (leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK) || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK)))
                    {
                        switch (script.CorporationType)
                        {
                            case CorporationType.CorporationTH:
                            case CorporationType.CorporationVI:
                            default:
                                eventId = leaveDate.EventId;
                                break;
                        }
                    }
                    // Xin làm thêm vào ngày thường
                    else
                    {
                        switch (script.CorporationType)
                        {
                            case CorporationType.CorporationTH:
                                eventId = eventService.GetByCode(Event.NT_OT150).Id;
                                break;
                            case CorporationType.CorporationVI:
                            default:
                                eventId = eventService.GetByCode(Event.NV_OTDL).Id;
                                break;
                        }
                    }
                }
            }
            overTimeDetail = new OverTimeDetail
            {
                IsManual = true,
                Note = note,
                Status = StatusOverTime.Pending,
                StartTime = startTime,
                EndTime = endTime,
                EventId = eventId.Value,
                OverTimeId = overTime.Id
            };
            OverTimeDetail result = overTimeDetailService.Create(overTimeDetail);
            if (result == null)
            {
                return (false, null);
            }

            return (true, overTimeDetail);
        }

        public (bool, string[]) ValidateOvertime(string email, DateTime workDate, int? eventId, DateTime startTime, DateTime endTime, Script script, int? id, bool isAdmin)
        {
            bool bOk = script.HasOverTime;
            List<string> allowSkips = new List<string>();

            // Kiểm tra thời gian checkout sau checkin không
            bOk = startTime < endTime && workDate.Date == startTime.Date && workDate.Date == endTime.Date;

            // Kiểm tra đăng ký ot trong tương lai và cho tháng trước sau mồng 03 hàng tháng
            DateTime startCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
            DateTime startLastMonth = startCurrentMonth.AddMonths(-1).Date;
            // Nếu validate theo ngày cutoff
            if (!isAdmin)
            {
                if ((startTime > DateTime.Now)
                    || ((workDate.Date < startCurrentMonth) && DateTime.Now.Day >= script.CutoffTimeKeepingDay)
                    || ((workDate.Date < startLastMonth) && DateTime.Now.Day < script.CutoffTimeKeepingDay))
                {
                    bOk = false;
                }
            }

            if (bOk)
            {
                OverTime overTime = overTimeService.GetByWorkDate(email, workDate);
                // Kiểm tra conflict thời gian
                if (CheckConflictTime(email, startTime, endTime, id))
                {
                    allowSkips.Add(ResultCode.OVERTIME_CONFLICT);
                }
            }

            return (bOk, allowSkips.ToArray());
        }

        public (List<(double TotalSeconds, string Description, int Percentage)>, List<(double Seconds, string WorkDate, List<(string, string, double)>)>) GetTotalOverTimeByStatusInMonthByStatus(string corporationType, StatusOverTime status, User user, Script script, DateTime startDate, DateTime endDate, string languge)
        {
            List<(double, string, int)> totalOverTimes = new List<(double, string, int)>();
            List<(double, string, List<(string, string, double)>)> overTimes = new List<(double, string, List<(string, string, double)>)>();

            if (corporationType == CorporationType.CorporationVI)
            {
                List<(int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> overTimeDetailsInMonth
                = overTimeDetailService.GetOverTimeDetailsInMonthByStatus(GroupEvent.OVER_TIME_VN_ID, status, user.Id, script.Id, startDate, endDate).Where(ot => ot.Percentage.HasValue).ToList();
                foreach (IGrouping<int, (int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> group in overTimeDetailsInMonth.GroupBy(otd => otd.Percentage.Value))
                {
                    double totalSeconds = 0;
                    string description = MessageManager.GetMessageManager(languge).GetValue("DESCRIPTION_GE_OT") + " (" + group.Key + "%)";
                    foreach ((int? percentage,
                        double? payPerHour,
                        double? payPerBlock,
                        string eventCode,
                        DateTime workDate,
                        DateTime startTime,
                        DateTime endTime,
                        bool isManual) in group)
                    {
                        if (isManual)
                        {
                            totalSeconds += endTime.Subtract(startTime).TotalSeconds;
                        }
                        else
                        {
                            (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                            totalSeconds += (morningSeconds + afternoonSeconds);
                        }
                    }
                    totalOverTimes.Add((totalSeconds, description, group.Key));
                }
                foreach (IGrouping<DateTime, (int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> group in overTimeDetailsInMonth.GroupBy(otd => otd.WorkDate))
                {

                    List<(string, string, double)> details = new List<(string, string, double)>();
                    double seconds = 0;
                    foreach ((int? percentage,
                        double? PayPerHour,
                        double? PayPerBlock,
                        string eventCode,
                        DateTime workDate,
                        DateTime startTime,
                        DateTime endTime,
                        bool isManual) in group)
                    {
                        if (isManual)
                        {
                            details.Add((startTime.ToString(Constants.FORMAT_TIME_HHMMSS), endTime.ToString(Constants.FORMAT_TIME_HHMMSS), endTime.Subtract(startTime).TotalSeconds));
                            seconds += endTime.Subtract(startTime).TotalSeconds;
                        }
                        else
                        {
                            (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                            details.Add((startTime.ToString(Constants.FORMAT_TIME_HHMMSS), endTime.ToString(Constants.FORMAT_TIME_HHMMSS), (morningSeconds + afternoonSeconds)));
                            seconds += (morningSeconds + afternoonSeconds);
                        }
                    }
                    overTimes.Add((seconds, group.Key.ToString(Constants.FORMAT_DATE_DDMMYYYY), details));
                }
            }
            else
            {
                List<(int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> overTimeDetailsInMonth
               = overTimeDetailService.GetOverTimeDetailsInMonthByStatus(GroupEvent.OVER_TIME_TH_ID, status, user.Id, script.Id, startDate, endDate).Where(ot => ot.Percentage.HasValue).ToList();

                List<(int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> additional
                    = status == StatusOverTime.Pending ? overTimeDetailService.GetOverTimeDetailsInMonthByStatus(GroupEvent.OVER_TIME_TH_ID, StatusOverTime.Approved, user.Id, script.Id, startDate, endDate).Where(ot => ot.Percentage.HasValue).ToList()
                    : new List<(int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)>();

                double totalOvertime150 = 0, totalOvertime200 = 0, totalOvertime300 = 0;

                foreach ((int? percentage,
                    double? payPerHour,
                    double? payPerBlock,
                    string eventCode,
                    DateTime workDate,
                    DateTime startTime,
                    DateTime endTime,
                    bool isManual) in overTimeDetailsInMonth.Where(otd => otd.EventCode == Event.NT_OT150))
                {
                    if (isManual)
                    {
                        totalOvertime150 += endTime.Subtract(startTime).TotalSeconds;
                    }
                    else
                    {
                        (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                        totalOvertime150 += (morningSeconds + afternoonSeconds);
                    }
                }

                List<(int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> ot200_300 = overTimeDetailsInMonth.Where(otd => otd.EventCode == Event.NT_OT200_300).ToList();

                foreach (IGrouping<DateTime, (int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> group in ot200_300.GroupBy(otd => otd.WorkDate))
                {
                    double totalSeconds = 0;
                    double totalApproval = 0;
                    foreach ((int? percentage,
                        double? payPerHour,
                        double? payPerBlock,
                        string eventCode,
                        DateTime workDate,
                        DateTime startTime,
                        DateTime endTime,
                        bool isManual) in additional.Where(otd => otd.WorkDate == group.Key && otd.EventCode == Event.NT_OT200_300))
                    {
                        if (isManual)
                        {
                            totalApproval += endTime.Subtract(startTime).TotalSeconds;
                        }
                        else
                        {
                            (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                            totalApproval += (morningSeconds + afternoonSeconds);
                        }
                    }
                    foreach ((int? percentage,
                        double? payPerHour,
                        double? payPerBlock,
                        string eventCode,
                        DateTime workDate,
                        DateTime startTime,
                        DateTime endTime,
                        bool isManual) in group)
                    {
                        if (isManual)
                        {
                            totalSeconds += endTime.Subtract(startTime).TotalSeconds;
                        }
                        else
                        {
                            (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                            totalSeconds += (morningSeconds + afternoonSeconds);
                        }
                    }
                    double ot8hourSeconds = 8 * 60 * 60 - totalApproval;
                    ot8hourSeconds = ot8hourSeconds < 0 ? 0 : ot8hourSeconds;
                    if (totalSeconds < ot8hourSeconds)
                    {
                        totalOvertime200 += totalSeconds;
                    }
                    else
                    {
                        totalOvertime200 += ot8hourSeconds;
                        totalOvertime300 += totalSeconds - ot8hourSeconds;
                    }
                }

                if (totalOvertime150 > 0)
                {
                    totalOverTimes.Add((totalOvertime150, MessageManager.GetMessageManager(languge).GetValue("DESCRIPTION_GE_OT") + " (" + 150 + "%)", 150));
                }
                if (totalOvertime200 > 0)
                {
                    totalOverTimes.Add((totalOvertime200, MessageManager.GetMessageManager(languge).GetValue("DESCRIPTION_GE_OT") + " (" + 200 + "%)", 200));
                }
                if (totalOvertime300 > 0)
                {
                    totalOverTimes.Add((totalOvertime300, MessageManager.GetMessageManager(languge).GetValue("DESCRIPTION_GE_OT") + " (" + 300 + "%)", 300));
                }

                foreach (IGrouping<DateTime, (int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> group in overTimeDetailsInMonth.GroupBy(otd => otd.WorkDate))
                {
                    List<(string, string, double)> details = new List<(string, string, double)>();
                    double seconds = 0;
                    foreach ((int? percentage,
                        double? PayPerHour,
                        double? PayPerBlock,
                        string eventCode,
                        DateTime workDate,
                        DateTime startTime,
                        DateTime endTime,
                        bool isManual) in group)
                    {
                        if (isManual)
                        {
                            details.Add((startTime.ToString(Constants.FORMAT_TIME_HHMMSS), endTime.ToString(Constants.FORMAT_TIME_HHMMSS), endTime.Subtract(startTime).TotalSeconds));
                            seconds += endTime.Subtract(startTime).TotalSeconds;
                        }
                        else
                        {
                            (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                            details.Add((startTime.ToString(Constants.FORMAT_TIME_HHMMSS), endTime.ToString(Constants.FORMAT_TIME_HHMMSS), (morningSeconds + afternoonSeconds)));
                            seconds += (morningSeconds + afternoonSeconds);
                        }
                    }
                    overTimes.Add((seconds, group.Key.ToString(Constants.FORMAT_DATE_DDMMYYYY), details));
                }
            }
            return (totalOverTimes, overTimes);
        }

        public Dictionary<string, List<EventWorkResult>> GetTotalEventWorkByStatusInMonthByStatus(string corporationType, StatusOverTime status, User user, Script script, DateTime startWorkDate, DateTime endWorkDate, string language)
        {
            Dictionary<string, List<EventWorkResult>> eventWorkResults = new Dictionary<string, List<EventWorkResult>>();
            List<GroupEvent> groupEvents = groupEventService.GetGroupEvents(corporationType);
            groupEvents.RemoveAll(ge => ge.Id == GroupEvent.OVER_TIME_VN_ID);
            groupEvents.RemoveAll(ge => ge.Id == GroupEvent.OVER_TIME_TH_ID);

            foreach (GroupEvent groupEvent in groupEvents)
            {
                List<EventWorkResult> events = new List<EventWorkResult>();
                IEnumerable<(int? Percentage,
                    double? PayPerHour,
                    double? PayPerBlock,
                    string EventCode,
                    DateTime WorkDate,
                    DateTime StartTime,
                    DateTime EndTime,
                    bool IsManual)> overTimeDetailsInMonth
                    = overTimeDetailService.GetOverTimeDetailsInMonthByStatus(groupEvent.Id, status, user.Id, script.Id, startWorkDate, endWorkDate);
                if (groupEvent.PayByBlock)
                {
                    overTimeDetailsInMonth = overTimeDetailsInMonth.OrderByDescending(o => o.PayPerBlock);
                }
                else if (groupEvent.PayByHour)
                {
                    overTimeDetailsInMonth = overTimeDetailsInMonth.OrderByDescending(o => o.PayPerHour);
                }
                else
                {
                    overTimeDetailsInMonth = overTimeDetailsInMonth.OrderByDescending(o => o.Percentage);
                }
                foreach ((int? percentage,
                    double? payPerHour,
                    double? payPerBlock,
                    string eventCode,
                    DateTime workDate,
                    DateTime startTime,
                    DateTime endTime,
                    bool isManual) in overTimeDetailsInMonth)
                {
                    double seconds = 0;
                    if (isManual)
                    {
                        seconds += endTime.Subtract(startTime).TotalSeconds;
                    }
                    else
                    {
                        (double morningSeconds, double afternoonSeconds) = STHelper.CalculationWorkSeconds(endTime, startTime, script.LunchBreak);
                        seconds += (morningSeconds + afternoonSeconds);
                    }
                    events.Add(new EventWorkResult
                    {
                        GroupEvent = MessageManager.GetMessageManager(language).GetValue(groupEvent.Name),
                        EventCode = eventCode,
                        WorkDate = workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                        Seconds = seconds,
                        PayByBlock = payPerBlock.HasValue ? string.Format("{0} Block", payPerBlock) : string.Empty,
                        PayByHour = payPerHour.HasValue ? string.Format("{0} VND", payPerHour * 1000) : string.Empty,
                        Percentage = percentage.HasValue ? string.Format("{0} %", percentage) : string.Empty,
                        StartTime = startTime.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS),
                        EndTime = endTime.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS)
                    });
                }
                eventWorkResults.Add(MessageManager.GetMessageManager(language).GetValue(groupEvent.Name), events);
            }

            return eventWorkResults;
        }
    }
}