﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.RequestModels;

namespace TOCO.Logic
{
    public class ClientManager
    {
        private readonly ApplicationDbContext context;
        private readonly LocationService locationService;
        private readonly ClientIPService clientIPService;
        public ClientManager(ApplicationDbContext context)
        {
            this.context = context;
            clientIPService = new ClientIPService(context);
            locationService = new LocationService(context);
        }

        public (Location, double) GetTopicaLocation(LocationRequestModel locationRequest)
        {
            (Location, double) result = locationService.GetTopicaLocation(locationRequest.Longitude, locationRequest.Latitude);
            return result;
        }

        public bool ValidateClient(LocationRequestModel location, string clientIP)
        {
            List<string> allowSkips = new List<string>();
            return clientIPService.CheckClientIP(clientIP) || locationService.CheckLocation(location.Longitude, location.Latitude);
        }

        public List<Location> GetAllTopicaLocation()
        {
            List<Location> locations = locationService.GetAll();
            return locations;
        }
    }
}
