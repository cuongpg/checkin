﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.RequestModels;

namespace TOCO.Logic
{
    public class NotificationManager
    {
        private readonly ApplicationDbContext context;
        private readonly NotificationService notificationService;
        private readonly FirebaseTokenService firebaseTokenService;
        private readonly NotificationTemplateService notificationTemplateService;
        public NotificationManager(ApplicationDbContext context)
        {
            this.context = context;
            notificationService = new NotificationService(context);
            firebaseTokenService = new FirebaseTokenService(context);
            notificationTemplateService = new NotificationTemplateService(context);
        }

        public int InsertFireBaseToken(string email, string token, string deviceId)
        {
            return firebaseTokenService.InsertFireBaseToken(email, token, deviceId);
        }

        public int DeleteFirebaseToken(string deviceId)
        {
            return firebaseTokenService.DeleteFireBaseToken(deviceId);
        }

        public int PushNotification(string email, string documentId, string avatar, string documentType, string[] parameterValues, string language)
        {
            NotificationTemplate template = notificationTemplateService.GetTemplateByCode(MessageManager.GetMessageManager(language).GetValue(documentType));
            Notification notification = new Notification
            {
                Email = email,
                DocumentId = documentId,
                DocumentType = template.DocumentType,
                Title = template.Title,
                Message = CreateMessage(template, parameterValues, language),
                Status = NotificationStatus.UNSENT,
                Avatar = avatar,
                ParameterValues = JsonConvert.SerializeObject(parameterValues)
            };

            return notificationService.Create(notification);
        }

        private string CreateMessage(NotificationTemplate template, string[] parameterValues, string language)
        {
            string message = template.Message;
            if (!string.IsNullOrEmpty(template.Parameters))
            {
                string[] parameters = template.Parameters.Split(",");
                int indx = 0;
                foreach (string parameter in parameters)
                {
                    if(parameterValues.Count() > indx)
                    {
                        message = message.Replace(parameter, MessageManager.GetMessageManager(language).GetValue(parameterValues[indx]));
                    }
                    indx++;
                }
            }

            return message;
        }

        public List<Notification> GetNotificationsByStatus(NotificationStatus status)
        {
            return notificationService.GetNotificationsByStatus(status);
        }

        public List<string> GetFirebaseTokensByEmail(string email)
        {
            return firebaseTokenService.GetFirebaseTokensByEmail(email).Select(d => d.Token).ToList();
        }

        public int CountNotificationsByStatusAndEmail(NotificationStatus status, string email)
        {
            return notificationService.CountNotificationsByStatusAndEmail(status, email);
        }

        public int UpdateStatus(int id, NotificationStatus status)
        {
            Notification notification = notificationService.GetById(id);
            if (notification == null)
            {
                return -1;
            }
            notification.Status = status;
            return notificationService.Update(notification);
        }

        public int UpdateStatus(Notification notification)
        {
            return notificationService.Update(notification);
        }

        public List<Notification> GetNotification(out int numberOfPages, out int totalRecords, STRequestModel request, string email, string language)
        {
            List<Notification> result = notificationService.GetNotification(out numberOfPages, out totalRecords, request, email);
            foreach (Notification notification in result)
            {
                string templateCode = MessageManager.GetMessageManager(language).GetValue(notification.DocumentType);
                NotificationTemplate template = notificationTemplateService.GetTemplateByCode(templateCode);
                if (template != null)
                {
                    notification.Title = template.Title;
                    string[] parameterValues = JsonConvert.DeserializeObject<string[]>(notification.ParameterValues);
                    notification.Message = CreateMessage(template, parameterValues, language);
                }
            }
            return result;
        }

        public int SeenStatus(string email)
        {
            int result = 0;
            List<int> notifications = notificationService.GetWaitingSeeNotificationIds(email).ToList();
            foreach (int id in notifications)
            {
                if (notificationService.UpdateStatus(id, NotificationStatus.WAITING_READ) > 0)
                {
                    result++;
                }
            }
            return result;
        }

        public int UpdateStatus(int id, int status)
        {
            return notificationService.UpdateStatus(id, (NotificationStatus)status);
        }
    }
}
