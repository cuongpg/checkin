﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Logic
{
    public class EventManager
    {
        private readonly UserService userService;
        private readonly ScriptService scriptService;
        private readonly EventService eventService;
        private readonly GroupEventService groupEventService;

        public EventManager(ApplicationDbContext context)
        {
            userService = new UserService(context);
            scriptService = new ScriptService(context);
            eventService = new EventService(context);
            groupEventService = new GroupEventService(context);
        }

        public List<GroupEventResult> GetGroupEvents(string language, string corporationType, bool hasOverTime)
        {
            List<GroupEventResult> result = new List<GroupEventResult>();
            List<GroupEvent> groupEvents = groupEventService.GetGroupEvents(corporationType);
            if (!hasOverTime)
            {
                groupEvents.RemoveAll(ge => ge.Id == GroupEvent.OVER_TIME_VN_ID);
                groupEvents.RemoveAll(ge => ge.Id == GroupEvent.OVER_TIME_TH_ID);
            }
            foreach (GroupEvent groupEvent in groupEvents)
            {
                GroupEventResult groupEventResult = STHelper.ConvertObject<GroupEventResult, GroupEvent>(groupEvent);
                groupEventResult.Name = MessageManager.GetMessageManager(language).GetValue(groupEventResult.Name);
                groupEventResult.Description = MessageManager.GetMessageManager(language).GetValue(groupEventResult.Description);
                result.Add(groupEventResult);
            }

            return result;
        }


        public List<GroupEventResult> GetOTQuotasGroupEvents(string language, string corporationType)
        {
            List<GroupEventResult> result = new List<GroupEventResult>();
            List<GroupEvent> groupEvents = groupEventService.GetOTQuotasGroupEvents(corporationType);
          
            foreach (GroupEvent groupEvent in groupEvents)
            {
                GroupEventResult groupEventResult = STHelper.ConvertObject<GroupEventResult, GroupEvent>(groupEvent);
                groupEventResult.Name = MessageManager.GetMessageManager(language).GetValue(groupEventResult.Name);
                groupEventResult.Description = MessageManager.GetMessageManager(language).GetValue(groupEventResult.Description);
                result.Add(groupEventResult);
            }

            return result;
        }

        public List<EventResult> GetEventsByEmployeeTypeAndGroup(int? groupId, string language, int employeeTypeId, int page, int perPage, string keyWord, out int totalPages)
        {
            List<EventResult> result = new List<EventResult>();
            List<Event> events = eventService.GetEventsByEmailAndGroup(groupId, employeeTypeId, page, perPage, keyWord, out totalPages);
            foreach (Event ev in events)
            {
                EventResult eventResult = STHelper.ConvertObject<EventResult, Event>(ev);
                eventResult.Description = string.IsNullOrEmpty(ev.Description)? string.Empty : MessageManager.GetMessageManager(language).GetValue(ev.Description);
                result.Add(eventResult);
            }
            return result;
        }

        public GroupEvent GetGroupEventByCode(string code)
        {
            return groupEventService.GetGroupEventByCode(code);
        }

        public bool CheckHasEventId(string corporationType, int employeeTypeId, int eventId)
        {
            Event eventType = eventService.GetByEmployeeTypeAndEventId(corporationType, employeeTypeId, eventId);
            return eventType.GroupEvent.InputEventCode && eventType != null;
        }

        public Event GetEventById(int eventId)
        {
            return eventService.GetById(eventId);
        }
    }
}
