﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public class ApprovalRecordManager : CommonManager
    {
        private readonly ApprovalRecordService approvalRecordService;
        private readonly UserService userService;
        private readonly ScriptService scriptService;
        private readonly AbsenderQuotasService absenderQuotasService;
        private readonly ReasonTypeService reasonTypeService;
        private readonly ScriptHistoryService scriptHistoryService;
        private readonly ChangeManagerInfoService changeManagerInfoService;

        public ApprovalRecordManager(ApplicationDbContext context) : base(context)
        {
            approvalRecordService = new ApprovalRecordService(context);
            userService = new UserService(context);
            scriptService = new ScriptService(context);
            absenderQuotasService = new AbsenderQuotasService(context);
            reasonTypeService = new ReasonTypeService(context);
            reasonTypeService = new ReasonTypeService(context);
            scriptHistoryService = new ScriptHistoryService(context);
            changeManagerInfoService = new ChangeManagerInfoService(context);
        }

        public (List<ApprovalDateResult>, double) GetApprovalDatesInMonth(bool isApproved, User user, Script script, DateTime startMonth, DateTime endMonth)
        {
            double totalNumberOfLeaveDay = 0;
            List<ApprovalDateResult> result = new List<ApprovalDateResult>();

            foreach (DateTime day in STHelper.EachDay(startMonth, endMonth))
            {
                LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, day);
                // Kiểm tra user có nghỉ lễ và nghỉ cuối tuần và hôm nay là ngày lễ hoặc ngày cuối tuần thì không tính là nghỉ phép
                LeaveDate leaveDate = leaveDateService.GetByDateAndType(day, leaveCalendarTypeHistory.Code);
                if (leaveDate != null && ((script.HasHoliday && leaveDate.ExportTimeKeepingCode != LeaveDate.WK)
                    || (script.HasLeaveWeekend && leaveDate.ExportTimeKeepingCode == LeaveDate.WK)))
                {
                    // Nếu ngày lễ hoặc nghỉ bù khác t7, cn thì được tính công nghỉ phép
                    if (script.HasHoliday && leaveDate != null && leaveDate.ExportTimeKeepingCode != LeaveDate.WK && day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                    {
                        totalNumberOfLeaveDay++;
                    }
                    continue;
                }
                List<ApprovalRecord> approvalRecords = isApproved ? approvalRecordService.GetApprovalApprovalRecordByDate(user, day) : approvalRecordService.GetApprovalRecordByDate(user, day);
                if (approvalRecords != null && approvalRecords.Count > 0)
                {
                    foreach (ApprovalRecord record in approvalRecords)
                    {
                        (double paidLeave, double unPaidLeave) = CalcTotalLeaveInDate(user.Id, script.HasHoliday, script.HasLeaveWeekend, record, day);
                        // Nếu đơn đã được duyệt cộng vào công nghỉ phép
                        if (record.Status == StatusApprovalRecord.Approved
                            && record.StatusDestroy != StatusApprovalRecord.Approved)
                        {
                            totalNumberOfLeaveDay += paidLeave;
                        }

                        ApprovalDateResult approvalDate = new ApprovalDateResult
                        {
                            WorkDate = day.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                            Note = record.Reason,
                            Reason = record.ReasonType.Name,
                            IsAfternoon = record.IsAfternoon,
                            IsMorning = record.IsMorning,
                            Status = (int)record.Status,
                            TotalPaidLeave = paidLeave,
                            TotalUnPaidLeave = unPaidLeave,
                            ReasonExportCode = record.ReasonType.ReasonExportCode
                        };
                        result.Add(approvalDate);
                    }
                }
            }
            return (result, totalNumberOfLeaveDay);
        }

        public ApprovalRecord Update(ApprovalRecord detail)
        {
            return approvalRecordService.Update(detail);
        }

        public IEnumerable<ApprovalRecord> GetPendingApprovalLastMonth(DateTime startDate, DateTime endDate, string corporationType)
        {
            IEnumerable<ApprovalRecord> result = approvalRecordService.GetPendingApprovalLastMonth(startDate, endDate, corporationType);
            return result;
        }

        public int CountPendingApprovalRecord(User user, bool isForManager, bool isAdmin, bool isDestroy)
        {
            return approvalRecordService.CountPendingApprovalRecord(user, isForManager, isAdmin, isDestroy);
        }

        public List<ApprovalRecordResult> GetApprovalRecords(out int numberOfPages, out int totalRecords, STRequestModel request, User user, bool isForManager, bool isDestroy, string language)
        {
            List<ApprovalRecordResult> result = new List<ApprovalRecordResult>();
            foreach (ApprovalRecord approvalRecord in approvalRecordService.GetApprovalRecords(out numberOfPages, out totalRecords, request, user, isForManager, isDestroy))
            {
                // Lấy ra thời gian nghi
                string leaveTime = string.Empty;
                if (approvalRecord.IsManyDate)
                {
                    leaveTime = approvalRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM) + " - " + approvalRecord.EndTime.ToString(Constants.FORMAT_DATE_DDMM);
                }
                else
                {
                    leaveTime = approvalRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM) + " (";
                    leaveTime += approvalRecord.IsMorning ? MessageManager.GetMessageManager(language).GetValue("MORNING") : "";
                    leaveTime += approvalRecord.IsMorning && approvalRecord.IsAfternoon ? "," : "";
                    leaveTime += approvalRecord.IsAfternoon ? MessageManager.GetMessageManager(language).GetValue("AFTERNOON") + ")" : ")";
                }
                // Lấy ra thời gian phê duyệt
                string updateAt = string.Empty;
                if (approvalRecord.IsDestroy)
                {
                    switch (approvalRecord.StatusDestroy)
                    {
                        case StatusApprovalRecord.Approved:
                        case StatusApprovalRecord.Rejected:
                            if (approvalRecord.ApprovalDestroyTime.HasValue)
                            {
                                updateAt = approvalRecord.ApprovalDestroyTime.Value.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS);
                            }
                            break;
                        case StatusApprovalRecord.Expired:
                        case StatusApprovalRecord.Pending:
                        default:
                            break;
                    }
                }
                else
                {
                    switch (approvalRecord.Status)
                    {
                        case StatusApprovalRecord.Approved:
                        case StatusApprovalRecord.Rejected:
                            if (approvalRecord.ApprovalTime.HasValue)
                            {
                                updateAt = approvalRecord.ApprovalTime.Value.ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS);
                            }
                            break;
                        case StatusApprovalRecord.Pending:
                        case StatusApprovalRecord.Expired:
                        default:
                            break;
                    }
                }
                // Lấy ra thông tin quản lý
                User manager = null;
                if ((approvalRecord.IsDestroy && approvalRecord.StatusDestroy == StatusApprovalRecord.Pending)
                     || (!approvalRecord.IsDestroy && approvalRecord.Status == StatusApprovalRecord.Pending))
                {
                    manager = userService.GetById(approvalRecord.User.ManagerId);
                }
                else
                {
                    if (!string.IsNullOrEmpty(approvalRecord.ApproverEmail))
                    {
                        manager = userService.GetByEmail(approvalRecord.ApproverEmail);
                    }
                    else
                    {
                        ChangeManagerInfo changeManagerInfo = changeManagerInfoService.GetLastApprovalChangeManagerInfo(approvalRecord.UserId, approvalRecord.CreatedAt.Date);
                        if (changeManagerInfo != null)
                        {
                            manager = userService.GetById(changeManagerInfo.NewManagerId);
                        }
                    }
                }

                result.Add(new ApprovalRecordResult()
                {
                    Id = approvalRecord.Id,
                    Name = approvalRecord.User.FullName,
                    Email = approvalRecord.User.Email,
                    ImageUrl = approvalRecord.User.ImageUrl,
                    Status = (int)approvalRecord.Status,
                    LeaveTime = leaveTime,
                    Note = approvalRecord.Reason,
                    ReasonType = MessageManager.GetMessageManager(language).GetValue(approvalRecord.ReasonType.Name),
                    TotalPaidLeave = approvalRecord.TotalPaidLeave.ToString(CultureInfo.InvariantCulture),
                    TotalLeaveDate = (approvalRecord.TotalPaidLeave + approvalRecord.TotalUnpaidLeave).ToString(CultureInfo.InvariantCulture),
                    TotalUnpaidLeave = approvalRecord.TotalUnpaidLeave.ToString(CultureInfo.InvariantCulture),
                    ManagerName = manager?.FullName ?? Constants.NA,
                    ManagerEmail = manager?.Email ?? Constants.NA,
                    ApprovalTime = approvalRecord.ApprovalTime != null ? DateTime.Parse(approvalRecord.ApprovalTime.ToString()).ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS) : string.Empty,
                    CreatedAt = approvalRecord.CreatedAt.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                    IsDestroy = isDestroy.ToString(),
                    StatusDestroy = (int)approvalRecord.StatusDestroy,
                    UpdatedAt = updateAt,
                    RejectReason = approvalRecord.RejectReason
                });
            }
            return result;
        }

        public List<ApprovalRecordResult> GetApprovalRecordsNeedConfirmByAdmin(out int numberOfPages, out int totalRecords, STRequestModel request, string language)
        {
            List<ApprovalRecordResult> result = new List<ApprovalRecordResult>();
            foreach (ApprovalRecord approvalRecord in approvalRecordService.GetApprovalRecordsNeedConfirmByAdmin(out numberOfPages, out totalRecords, request))
            {
                User manager = userService.GetById(approvalRecord.User.ManagerId);
                string leaveTime = string.Empty;
                if (approvalRecord.IsManyDate)
                {
                    leaveTime = approvalRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM) + " - " + approvalRecord.EndTime.ToString(Constants.FORMAT_DATE_DDMM);
                }
                else
                {
                    leaveTime = approvalRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM) + " (";

                    leaveTime += approvalRecord.IsMorning ? MessageManager.GetMessageManager(language).GetValue("MORNING") : "";
                    leaveTime += approvalRecord.IsMorning && approvalRecord.IsAfternoon ? "," : "";
                    leaveTime += approvalRecord.IsAfternoon ? MessageManager.GetMessageManager(language).GetValue("AFTERNOON") + ")" : ")";
                }
                result.Add(new ApprovalRecordResult()
                {
                    Id = approvalRecord.Id,
                    Name = approvalRecord.User.FullName,
                    Email = approvalRecord.User.Email,
                    ImageUrl = approvalRecord.User.ImageUrl,
                    Status = (int)approvalRecord.Status,
                    LeaveTime = leaveTime,
                    Note = approvalRecord.Reason,
                    ReasonType = MessageManager.GetMessageManager(language).GetValue(approvalRecord.ReasonType.Name),
                    TotalPaidLeave = approvalRecord.TotalPaidLeave.ToString(CultureInfo.InvariantCulture),
                    TotalLeaveDate = (approvalRecord.TotalPaidLeave + approvalRecord.TotalUnpaidLeave).ToString(CultureInfo.InvariantCulture),
                    TotalUnpaidLeave = approvalRecord.TotalUnpaidLeave.ToString(CultureInfo.InvariantCulture),
                    ManagerName = manager.FullName,
                    ManagerEmail = manager.Email,
                    ApprovalTime = approvalRecord.ApprovalTime != null ? DateTime.Parse(approvalRecord.ApprovalTime.ToString()).ToString(Constants.FORMAT_DATE_DDMMYYYYHHMMSS) : string.Empty,
                    CreatedAt = approvalRecord.CreatedAt.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                    IsDestroy = false.ToString(),
                    StatusDestroy = (int)approvalRecord.StatusDestroy,
                    UpdatedAt = string.Empty,
                    RejectReason = approvalRecord.RejectReason
                });
            }
            return result;
        }


        public (bool bOk, string message) DestroyApprovalRecord(int[] ids, User user)
        {
            (bool bOk, string message, ApprovalRecord[] records) = approvalRecordService.DestroyApprovalRecord(false, false, ids, user.Id);
            return (bOk, message);
        }

        public (bool bOk, string message, ApprovalRecord[] records) ConfirmApprovalReacord(bool isAdmin, StatusApprovalRecord status, int[] ids, User manager, bool isDestroy, string rejectReason = null)
        {
            (bool bOk, string message, ApprovalRecord[] records) = approvalRecordService.ConfirmApprovalReacord(status, ids, manager, isDestroy, rejectReason);

            // Nếu reject đơn xin hủy nghỉ phép của nhân sự
            if (status == StatusApprovalRecord.Rejected && isDestroy)
            {
                foreach (ApprovalRecord record in records)
                {
                    // Xử lý kiểm tra conflict thời gian
                    // Xử lý conflict thời gian
                    ScriptHistory scriptHistory = scriptHistoryService.GetScriptInDay(record.User.Id, DateTime.Now);
                    Script script = scriptService.GetById(scriptHistory.ScriptId);
                    DestroyConflict(true, isAdmin, record.IsManyDate, record.User, script, record.StartTime, record.EndTime, record.IsAfternoon, record.IsMorning, record.Id);
                }
            }

            return (bOk, message, records);
        }

        public (bool bOk, string message, ApprovalRecord[] records) ConfirmRecordByAdmin(StatusApprovalRecord status, int[] ids, string adminEmail, string rejectReason = null)
        {
            (bool bOk, string message, ApprovalRecord[] records) = approvalRecordService.ConfirmRecordByAdmin(status, ids, adminEmail, rejectReason);
            return (bOk, message, records);
        }



        public IEnumerable<ApprovalRecord> ApprovalDestroyConflict(bool isManyDate, User user, DateTime startTime, DateTime endTime, bool isAfternoon, bool isMorning)
        {
            List<ApprovalRecord> approvalRecords = new List<ApprovalRecord>();
            if (isManyDate)
            {
                approvalRecords.AddRange(approvalRecordService.GetDestroyPendingByDate(user.Email, startTime, endTime));
            }
            else
            {
                approvalRecords.AddRange(approvalRecordService.GetDestroyPendingByDate(user.Email, startTime, isAfternoon, isMorning));
            }
            // Approve đơn hủy xin nghỉ bị trùng thời gian đơn xin nghỉ phép được approval
            foreach (ApprovalRecord record in approvalRecords)
            {
                record.StatusDestroy = StatusApprovalRecord.Approved;
                record.ApprovalDestroyTime = DateTime.Now;
                Update(record);
                context.SaveChanges();
                yield return record;
            }
        }

        public void DestroyConflict(bool byManager, bool isAdmin, bool isManyDate, User user, Script script, DateTime startTime, DateTime endTime, bool isAfternoon, bool isMorning, int? id)
        {
            List<ApprovalRecord> approvalRecords = new List<ApprovalRecord>();
            if (isManyDate)
            {
                approvalRecords.AddRange(approvalRecordService.GetByDate(user.Email, startTime, endTime));
            }
            else
            {
                approvalRecords.AddRange(approvalRecordService.GetByDate(user.Email, startTime, isAfternoon, isMorning));
            }
            List<int> ids = approvalRecords.Select(a => a.Id).ToList();
            if (id.HasValue && ids.Contains(id.Value))
            {
                ids.Remove(id.Value);
            }
            // Hủy đơn xin nghỉ bị trùng
            (bool bOk, string message, ApprovalRecord[] records) = approvalRecordService.DestroyApprovalRecord(byManager, isAdmin, ids.ToArray(), user.Id);
        }

        public (bool, string[], string) ValidateApprovalRecord(User user, Script script, bool isManyDate, DateTime startDate, DateTime endDate,
            bool isMorning, bool isAfternoon, string note, int reasonTypeId, bool isAdmin)
        {
            bool bOk = true;
            List<string> allowSkips = new List<string>();
            string message = string.Empty;
            // Validate TH nghỉ nhiều ngày
            if (isManyDate)
            {
                // Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                bOk = startDate < endDate;
                message = !bOk ? ResultCode.CAN_NOT_APPROVAL_RECORD : string.Empty;
            }
            else
            {
                // Validate TH nghỉ một ngày
                // Ngày bắt đầu và ngày kết thúc cùng một ngày và phải có ít nhất nghỉ 1 buổi
                bOk = startDate.Date == endDate.Date && (isMorning || isAfternoon);
                message = !bOk ? ResultCode.CAN_NOT_APPROVAL_RECORD : string.Empty;
            }
            
            
            // Kiểm tra có lý do nghỉ không
            if (bOk)
            {
                bOk = !string.IsNullOrEmpty(note);
                message = !bOk ? ResultCode.REASON_CAN_NOT_NULL : string.Empty;
            }
            // Check độ dài lý do
            if (bOk)
            {
                bOk = note.Count() < int.Parse(AppSetting.Configuration.GetValue("MaxLenghtNote"));
                message = !bOk ? ResultCode.REASON_TOO_LONG : string.Empty;
            }

            if (bOk && !isAdmin)
            {
                // Chỉ được xin phép nghỉ backdate trong thời gian <= T+3 
                // và không vắt tháng (tháng nào được xin nghỉ tháng đấy, khác tháng không được phép xin nghỉ).
                DateTime startCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                DateTime endCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
                DateTime startLastMonth = startCurrentMonth.AddMonths(-1).Date;
                if (((startDate < startCurrentMonth) && DateTime.Now.Day >= script.CutoffTimeKeepingDay)
                    || ((startDate < startLastMonth) && DateTime.Now.Day < script.CutoffTimeKeepingDay))
                {
                    bOk = false;
                    message = ResultCode.LEAVE_REQUEST_OVER_3_DAYS;
                }
                //if (startDate < startCurrentMonth || DateTime.Now.Subtract(startDate).TotalDays > script.CutoffTimeKeepingDay)
                //{
                //    bOk = false;
                //    message = ResultCode.LEAVE_REQUEST_OVER_3_DAYS;
                //}
            }

            // Kiểm tra kịch bản của nhân sự có lý do nghỉ không
            List<ReasonType> reasonTypes = reasonTypeService.GetReasonTypes(script.HasPaidLeave, script.CorporationType);
            ReasonType reason = reasonTypes.Find(r => r.Id == reasonTypeId);
            if (bOk)
            {
                bOk = reason != null;
                message = !bOk ? ResultCode.DONT_HAVE_REASON_TYPE : string.Empty;
            }

            if (bOk)
            {
                // Kiểm tra quỹ nghỉ
                (bool allowed, List<(double Number, DateTime ExpirationDate, int GenerationYear)> listBuffer) = GetBufferLeave(user, reason, script, startDate, endDate);
                if (!allowed)
                {
                    bOk = false;
                    message = ResultCode.END_OF_LEAVE_BUFFER;
                }
                else
                {
                    (double leaveDateNumber, int holidayNumber, int weekendNumber) = GetLeaveDate(user.Id, script.HasHoliday, script.HasLeaveWeekend,
                        isManyDate, isMorning, isAfternoon, startDate, endDate, out List<DateTime> listLeaveDate);
                    if (leaveDateNumber <= 0)
                    {
                        bOk = false;
                        message = ResultCode.LEAVE_DATE_INVALID;
                    }
                    else
                    {
                        if (listBuffer.Count > 0)
                        {
                            if (!isManyDate)
                            {
                                double totalBuffer = listBuffer.Sum(bf => bf.Number);
                                if (totalBuffer < leaveDateNumber)
                                {
                                    allowSkips.Add(ResultCode.APPROVAL_OVER_BUFFER);
                                }
                            }
                            else
                            {
                                double totalBufferExpAffterEnd = listBuffer.Where(bf => bf.ExpirationDate >= endDate).Sum(bf => bf.Number);

                                // Lấy danh sách nghỉ phép có ngày bắt đầu trước ngày hết hạn và ngày kết thúc sau ngày hết hạn
                                List<(double Number, DateTime ExpirationDate, int GenerationYear)> surplusBuff = listBuffer.Where(bf => bf.ExpirationDate <= endDate).OrderBy(bf => bf.ExpirationDate).ToList();
                                DateTime start = startDate;
                                foreach ((double number, DateTime expirationDate, int GenerationYear) in surplusBuff)
                                {
                                    double leaveDateNumberBeforeExp = listLeaveDate.Where(d => d >= start && d <= expirationDate).Count();
                                    totalBufferExpAffterEnd += leaveDateNumberBeforeExp < number ? leaveDateNumberBeforeExp : number;
                                    start = expirationDate.AddDays(1);
                                }
                                if (leaveDateNumber > totalBufferExpAffterEnd)
                                {
                                    allowSkips.Add(ResultCode.APPROVAL_OVER_BUFFER);
                                }
                            }
                        }

                        // Kiểm tra conflict thời gian
                        if (CheckConflictTime(isManyDate, user.Email, startDate, endDate, isAfternoon, isMorning))
                        {
                            allowSkips.Add(ResultCode.APPROVAL_CONFLICT);
                        }
                    }
                }
            }
            return (bOk, allowSkips.ToArray(), message);
        }

        public bool CheckConflictTime(bool isManyDate, string email, DateTime startTime, DateTime endTime, bool isAfternoon, bool isMorning)
        {
            bool bConflict = false;
            if (isManyDate)
            {
                List<ApprovalRecord> approvalRecords = approvalRecordService.GetByDate(email, startTime, endTime);
                bConflict = approvalRecords != null && approvalRecords.Count > 0;
            }
            else
            {
                List<ApprovalRecord> approvalRecords = approvalRecordService.GetByDate(email, startTime, isAfternoon, isMorning);
                bConflict = approvalRecords != null && approvalRecords.Count > 0;
            }

            return bConflict;
        }

        /// <summary>
        /// Lấy danh sách quỹ tồn nghỉ phép từ ngày bắt đầu nghỉ đến ngày kết thúc nghỉ
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reason"></param>
        /// <param name="script"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public (bool Allowed, List<(double Number, DateTime ExpirationDate, int GenerationYear)> ListBuffer) GetBufferLeave(User user, ReasonType reason, Script script, DateTime startDate, DateTime endDate)
        {
            bool bOk = false;
            List<(double Number, DateTime ExpirationDate, int GenerationYear)> bufferLeave = new List<(double Number, DateTime ExpirationDate, int GenerationYear)>();
            // Nếu lý do nghỉ phép là loại nghỉ có lương
            if (reason.HasPaidLeave)
            {
                // Nếu lý do nghỉ có quỹ phép phát sinh
                if (reason.HasBufferDate)
                {
                    List<(double Number, DateTime ExpirationDate, int GenerationYear)> absenderGroups = absenderQuotasService.GetByExpirationDate(user.Id, reason.Id).OrderBy(ag => ag.ExpirationDate).ToList();
                    List<ApprovalRecord> approvalRecords = approvalRecordService.GetApprovalRecordByUserAndReasonType(user.Id, reason.Id);

                    // Khai báo số phép nợ 
                    double numberDebt = 0;
                    foreach ((double number, DateTime expirationDate, int GenerationYear) in absenderGroups)
                    {
                        // Lấy ra tổng số ngày phép đã nghỉ trước ngày hết hạn của quỹ
                        double totalLeaved = numberDebt + approvalRecords.Where(ar => ar.EndTime <= expirationDate).Sum(ar => ar.TotalPaidLeave);

                        // Reset giá trị phép nợ cho quỹ sau
                        numberDebt = 0;

                        // Lấy danh sách nghỉ phép có ngày bắt đầu trước ngày hết hạn và ngày kết thúc sau ngày hết hạn
                        List<ApprovalRecord> surplusAppr = approvalRecords.Where(ar => ar.StartTime <= expirationDate && ar.EndTime > expirationDate).ToList();

                        // Duyệt từng đơn nghỉ phép
                        foreach (ApprovalRecord approvalRecord in surplusAppr)
                        {
                            // Nếu số ngày nghỉ từ ngày bắt đầu tới ngày hết hạn phép nhỏ hơn số ngày nghỉ phép

                            (double leaveDateNumber, int holidayNumber, int weekendNumber) = GetLeaveDate(user.Id, script.HasHoliday, script.HasLeaveWeekend,
                                approvalRecord.IsManyDate, approvalRecord.IsMorning, approvalRecord.IsAfternoon, approvalRecord.StartTime, expirationDate, out List<DateTime> listLeaveDate);

                            if (leaveDateNumber < approvalRecord.TotalPaidLeave)
                            {
                                // Cộng số ngày nghỉ trước ngày hết hạn 
                                totalLeaved += leaveDateNumber;

                                // Cộng số ngày còn lại vào số phép nợ
                                numberDebt += (approvalRecord.TotalPaidLeave - leaveDateNumber);
                            }
                            else  // Nếu số ngày từ ngày bắt đầu tới ngày hết hạn phép lớn hơn hoặc bằng tổng số phép nghỉ thì cộng số ngày nghỉ phép trước ngày hết hạn với số ngày nghỉ phép
                            {
                                totalLeaved += approvalRecord.TotalPaidLeave;
                            }
                        }

                        // Nếu ngày hết hạn sau ngày bắt đầu xin nghỉ thêm vào list buffer
                        if (expirationDate >= startDate)
                        {
                            bufferLeave.Add((number > totalLeaved ? number - totalLeaved : 0, expirationDate, GenerationYear));
                        }

                        // Kiểm tra nếu số ngày nghỉ phép trước ngày hết hạn mà lớn hơn quỹ phép => công số ngày còn lại vào số phép nợ
                        numberDebt += number < totalLeaved ? totalLeaved - number : 0;

                        // Remove các đơn nghỉ phép đã tính toán
                        approvalRecords.RemoveAll(ar => ar.StartTime <= expirationDate);
                    }
                    bOk = bufferLeave.Sum(bf => bf.Number) > 0;
                }
                else if (reason.BufferDefault.HasValue) // Nếu lý do nghỉ có định mức phép mặc định
                {
                    bOk = true;
                    bufferLeave.Add((reason.BufferDefault.Value, DateTime.MaxValue, DateTime.Now.Year));
                }
            }
            // Nếu lý do nghỉ phép là nghỉ không lương
            else
            {
                bOk = true;
            }
            return (bOk, bufferLeave);
        }

        public string GetLeaveTime(ApprovalRecord approvalRecord, string language)
        {
            string leaveTime = string.Empty;
            if (approvalRecord.IsManyDate)
            {
                leaveTime = string.Format(MessageManager.GetMessageManager(language).GetValue(Constants.FROM_TO_DATE),
                    approvalRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM),
                    approvalRecord.EndTime.ToString(Constants.FORMAT_DATE_DDMM));
            }
            else
            {
                leaveTime += approvalRecord.IsMorning ? MessageManager.GetMessageManager(language).GetValue("MORNING_SHIFT") : "";
                leaveTime += approvalRecord.IsMorning && approvalRecord.IsAfternoon ? "/ " : "";
                leaveTime += approvalRecord.IsAfternoon ? MessageManager.GetMessageManager(language).GetValue("AFTERNOON_SHIFT") : "";
                leaveTime += MessageManager.GetMessageManager(language).GetValue("DATE") + approvalRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
            }
            return leaveTime;
        }

        public (bool bOk, ApprovalRecord Record) ApprovalRecord(User user, Script script, bool isManyDate, DateTime startDate, DateTime endDate,
            bool isMorning, bool isAfternoon, string note, int reasonTypeId, bool isAdmin)
        {

            (bool bOk, string[] statusAllowSkips, string message) = ValidateApprovalRecord(user, script, isManyDate, startDate, endDate, isMorning, isAfternoon, note, reasonTypeId, isAdmin);
            if (!bOk)
            {
                return (false, null);
            }
            ApprovalRecord approvalRecord = new ApprovalRecord();
            if (statusAllowSkips.Contains(ResultCode.APPROVAL_CONFLICT))
            {
                DestroyConflict(false, isAdmin, isManyDate, user, script, startDate, endDate, isAfternoon, isMorning, null);
            }
            ReasonType reasonType = reasonTypeService.GetReasonById(reasonTypeId);
            approvalRecord.UserId = user.Id;
            approvalRecord.User = user;
            approvalRecord.Status = reasonType.NeedAdminConfirm ? StatusApprovalRecord.AdminPending : StatusApprovalRecord.Pending;
            approvalRecord.StartTime = startDate;
            approvalRecord.EndTime = endDate;
            approvalRecord.Reason = note;
            approvalRecord.ReasonTypeId = reasonTypeId;
            approvalRecord.ReasonType = reasonType;
            approvalRecord.IsManyDate = isManyDate;
            approvalRecord.IsMorning = isManyDate ? true : isMorning;
            approvalRecord.IsAfternoon = isManyDate ? true : isAfternoon;

            (double leaveDateNumber, int numberHoliday, int numberWeekend) = GetLeaveDate(user.Id, script.HasHoliday, script.HasLeaveWeekend,
                        isManyDate, isMorning, isAfternoon, startDate, endDate, out List<DateTime> listLeaveDate);

            if (reasonType.HasPaidLeave)
            {
                if (statusAllowSkips.Contains(ResultCode.APPROVAL_OVER_BUFFER))
                {
                    // Kiểm tra quỹ nghỉ
                    List<(double Number, DateTime ExpirationDate, int GenerationYear)> listBuffer = GetBufferLeave(user, reasonType, script, startDate, endDate).ListBuffer;
                    double leaveBuffer = 0;
                    if (!isManyDate)
                    {
                        leaveBuffer = listBuffer.Sum(bf => bf.Number);
                    }
                    else
                    {
                        leaveBuffer = listBuffer.Where(bf => bf.ExpirationDate >= endDate).Sum(bf => bf.Number);
                        // Lấy danh sách nghỉ phép có ngày bắt đầu trước ngày hết hạn và ngày kết thúc sau ngày hết hạn
                        List<(double Number, DateTime ExpirationDate, int GenerationYear)> surplusBuff = listBuffer.Where(bf => bf.ExpirationDate <= endDate).OrderBy(bf => bf.ExpirationDate).ToList();
                        DateTime start = startDate;
                        foreach ((double number, DateTime expirationDate, int GenerationYear) in surplusBuff)
                        {
                            double leaveDateNumberBeforeExp = listLeaveDate.Where(d => d >= start && d <= expirationDate).Count();
                            leaveBuffer += leaveDateNumberBeforeExp < number ? leaveDateNumberBeforeExp : number;
                            start = expirationDate.AddDays(1);
                        }
                    }

                    approvalRecord.TotalPaidLeave = leaveBuffer;
                    approvalRecord.TotalUnpaidLeave = leaveDateNumber - leaveBuffer;
                }
                else
                {
                    approvalRecord.TotalPaidLeave = leaveDateNumber;
                    approvalRecord.TotalUnpaidLeave = 0;
                }
            }
            else
            {
                approvalRecord.TotalPaidLeave = 0;
                approvalRecord.TotalUnpaidLeave = leaveDateNumber;
            }
            // Tao don xin nghi phep
            approvalRecord = approvalRecordService.Create(approvalRecord);

            return (approvalRecord != null, approvalRecord);
        }
    }
}
