﻿using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Models.RequestModels;

namespace TOCO.Logic
{
    public class ConfirmAllocationManager
    {
        private readonly ApplicationDbContext context;
        //private readonly ConfirmAllocationService confirmAllocationService;
        private readonly ConfirmAllocationV2Service confirmAllocationV2Service;
        private readonly MechanismService mechanismService;
        private readonly MechanismQuotasService mechanismQuotasService;
        private readonly CommonItemService commonItemService;
        private readonly UserService userService;
        private readonly OrgUnitJoinInfoService orgUnitJoinInfoService;
        private readonly UnitRoleService unitRoleService;
        private const int CommonRoot = 0;
        public const int TrackRoot = 668;

        public ConfirmAllocationManager(ApplicationDbContext context)
        {
            this.context = context;
            //confirmAllocationService = new ConfirmAllocationService(context);
            confirmAllocationV2Service = new ConfirmAllocationV2Service(context);
            mechanismService = new MechanismService(context);
            mechanismQuotasService = new MechanismQuotasService(context);
            commonItemService = new CommonItemService(context);
            userService = new UserService(context);
            orgUnitJoinInfoService = new OrgUnitJoinInfoService(context);
            unitRoleService = new UnitRoleService(context);
        }

        //public int Update(ConfirmAllocation confirmAllocation)
        //{
        //    return confirmAllocationService.Update(confirmAllocation);
        //}

        public int UpdateV2(ConfirmAllocationV2 confirmAllocation)
        {
            return confirmAllocationV2Service.Update(confirmAllocation);
        }

        //public int AdminConfirm(int recordId, ConfirmAllocationStatus status, string rejectReason)
        //{
        //    ConfirmAllocation confirmAllocation = confirmAllocationService.GetLateReportById(recordId);
        //    if (confirmAllocation == null || confirmAllocation.Status != ConfirmAllocationStatus.Reporting)
        //    {
        //        return 0;
        //    }
        //    confirmAllocation.Status = status;
        //    confirmAllocation.RejectReason = rejectReason;
        //    return confirmAllocationService.Update(confirmAllocation);
        //}

        public int AdminConfirmV2(int recordId, ConfirmAllocationStatusV2 status, string rejectReason)
        {
            ConfirmAllocationV2 confirmAllocation = confirmAllocationV2Service.GetLateReportById(recordId);
            if (confirmAllocation == null || confirmAllocation.Status != ConfirmAllocationStatusV2.Reporting)
            {
                return 0;
            }
            confirmAllocation.Status = status;
            confirmAllocation.RejectReason = rejectReason;
            return confirmAllocationV2Service.Update(confirmAllocation);
        }

        //public ConfirmAllocation Create(ConfirmAllocation confirmAllocation)
        //{
        //    return confirmAllocationService.Create(confirmAllocation);
        //}

        public ConfirmAllocationV2 CreateV2(ConfirmAllocationV2 confirmAllocation)
        {
            return confirmAllocationV2Service.Create(confirmAllocation);
        }

        //public List<ConfirmAllocation> GetErrorReport()
        //{
        //    return confirmAllocationService.GetErrorReport();
        //}

        public List<ConfirmAllocationV2> GetErrorReportV2()
        {
            return confirmAllocationV2Service.GetErrorReport();
        }

        //public ConfirmAllocation GetWaitReportByUserId(int userId)
        //{
        //    return confirmAllocationService.GetWaitReportByUserId(userId);
        //}

        public ConfirmAllocationV2 GetWaitReportByUserIdV2(int userId, DateTime startMonth, DateTime endMonth)
        {
            return confirmAllocationV2Service.GetWaitReportByUserId(userId, startMonth, endMonth);
        }

        //public List<ConfirmAllocationV2> GetReportByUserAndTime(int userId, DateTime startTime, DateTime endTime)
        //{
        //    return confirmAllocationV2Service.GetReportByUserAndTime(userId, startTime, endTime);
        //}


        //public void DestroyRecordByStatus(int userId, ConfirmAllocationStatus status)
        //{
        //    List<ConfirmAllocation> confirmAllocations = confirmAllocationService.GetReportByUserAndStatus(userId, ConfirmAllocationStatus.Reporting);
        //    foreach (ConfirmAllocation confirmAllocation in confirmAllocations)
        //    {
        //        confirmAllocation.Status = ConfirmAllocationStatus.Destroy;
        //        confirmAllocationService.Update(confirmAllocation);
        //    }
        //}

        public void DestroyRecordByStatusV2(int userId, ConfirmAllocationStatusV2 status)
        {
            List<ConfirmAllocationV2> confirmAllocations = confirmAllocationV2Service.GetReportByUserAndStatus(userId, ConfirmAllocationStatusV2.Reporting);
            foreach (ConfirmAllocationV2 confirmAllocation in confirmAllocations)
            {
                confirmAllocation.Status = ConfirmAllocationStatusV2.Destroy;
                confirmAllocationV2Service.Update(confirmAllocation);
            }
        }


        //public int CreatedSessionAllocation(int[] userIds)
        //{
        //    int number = 0;
        //    foreach (int userId in userIds)
        //    {
        //        //DestroyRecordByStatus(userId, ConfirmAllocationStatus.Waiting);
        //        ConfirmAllocation confirmAllocation = new ConfirmAllocation
        //        {
        //            UserId = userId,
        //            Status = ConfirmAllocationStatus.Waiting
        //        };
        //        confirmAllocationService.Create(confirmAllocation);
        //        if (confirmAllocation.Id != 0)
        //        {
        //            number += 1;
        //        }
        //    }

        //    return number;
        //}

        public int CreatedSessionAllocationV2(int[] userIds, DateTime startMonth, DateTime endMonth)
        {
            int number = 0;
            foreach (int userId in userIds)
            {
                DestroyRecordByStatusV2(userId, ConfirmAllocationStatusV2.Waiting);
                ConfirmAllocationV2 confirmAllocation = new ConfirmAllocationV2
                {
                    UserId = userId,
                    Status = ConfirmAllocationStatusV2.Waiting,
                    StartDate = startMonth,
                    EndDate = endMonth
                };
                confirmAllocationV2Service.Create(confirmAllocation);
                if (confirmAllocation.Id != 0)
                {
                    number += 1;
                }
            }

            return number;
        }

        public List<Mechanism> GetAllDepartmentMechanism()
        {
            return mechanismService.GetAll();
        }

        public Mechanism GetDepartmentMechanismByHCMCode(string hcmCode)
        {
            return mechanismService.GetByHCMCode(hcmCode);
        }

        //public async Task<MechanismInformation> GetMechanismInformationByEmail(User user, DateTime startMonth, DateTime endMonth)
        //{
        //    string startMonthText = startMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
        //    string endMonthText = endMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
        //    MechanismInformation result = new MechanismInformation
        //    {
        //        Email = user.Email,
        //        EmployeeCode = string.Format("{0,0:D8}", user.EmployeeCode),
        //        StatementCheckin = new StatementCheckin(),
        //        Projects = new List<ProjectCheckin>(),
        //        Departments = new List<DepartmentCheckin>()
        //    };
        //    // Lấy thông tin nhân sự từ HrOper
        //    (HKTEmployeeInfo userInfo, HttpStatusCode userStatus, string userMessage) = await HKTDataAPI.Connection.GetEmployeeInfoAsync(user.EmployeeCode);
        //    (List<HKTStatementInfo> statementInfos, HttpStatusCode statementStatus, string statementMessage) = await HKTDataAPI.Connection.GetStatementInfoAsync(user.EmployeeCode);
        //    // Lấy danh sách thông tin cơ chế lương thưởng đã tham gia
        //    (List<JoinCOMMechanism> mechanisms, HttpStatusCode mechanismStatus, string mechanismMessage) = await HKTDataAPI.Connection.GetCOMMechanismForUser(user.EmployeeCode);
        //    // Lấy thông tin dự án đã tham gia
        //    (List<JoinProjectInfo> projects, HttpStatusCode projectStatus, string projectMessage) = await HKTDataAPI.Connection.GetProjectInformationsForUser(user.EmployeeCode);
        //    // Lấy thông tin phòng ban đang tham gia
        //    (List<JoinDepartmentInfo> departments, HttpStatusCode departmentStatus, string departmentMessage) = await HKTDataAPI.Connection.GetDepartmentInformationsForUser(user.EmployeeCode, endMonth);
        //    // Nếu 1 trong các API lấy thông tin từ HKT
        //    if (userStatus != HttpStatusCode.OK || statementStatus != HttpStatusCode.OK || mechanismStatus != HttpStatusCode.OK || projectStatus != HttpStatusCode.OK || departmentStatus != HttpStatusCode.OK)
        //    {
        //        return null;
        //    }

        //    // Lấy ra quyết định cuối cùng trong tháng
        //    HKTStatementInfo statementInfo = statementInfos?
        //        .Where(s => string.Compare(s.StatementValidDate, endMonthText) <= 0)
        //        .Where(s => string.Compare(s.StatementExpiredDate, startMonthText) >= 0)
        //        .OrderByDescending(s => s.StatementValidDate).FirstOrDefault();

        //    result.UserName = userInfo?.EmployeeUsername;
        //    result.StatementCheckin.EstimationOwner = statementInfo?.CDT;
        //    result.StatementCheckin.Product = statementInfo?.SP;
        //    result.StatementCheckin.Level = statementInfo?.CB;

        //    if (!string.IsNullOrEmpty(statementInfo.NG2))
        //    {
        //        CommonItem track2 = GetCommonItems(2, null, ConfirmAllocationManager.TrackRoot)
        //            .Where(c => c.Name == statementInfo.NG2).FirstOrDefault();
        //        if (track2 != null)
        //        {
        //            result.StatementCheckin.Track2 = string.Format("{0} - {1}", track2.Name, track2.Description);
        //        }
        //    }

        //    // Xử lý thông tin cơ chế lương thưởng
        //    if (mechanisms?.Count > 0)
        //    {
        //        // Lấy thông tin cơ chế lương thưởng cuối cùng trong tháng đang tham gia
        //        JoinCOMMechanism joinCOMMechanism = mechanisms
        //            .Where(m => !(string.Compare(m.ENDDA, startMonthText) < 0
        //            || string.Compare(m.BEGDA, endMonthText) > 0))
        //            .OrderByDescending(m => m.BEGDA)
        //            .FirstOrDefault();
        //        if (!string.IsNullOrEmpty(joinCOMMechanism?.COMMechanismCode))
        //        {
        //            COMMechanismInput inputMechanism = new COMMechanismInput
        //            {
        //                COMMechanismCode = joinCOMMechanism.COMMechanismCode
        //            };
        //            (List<COMMechanism> data, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetCOMMechanismList(inputMechanism, startMonth, endMonth);
        //            if (status == HttpStatusCode.OK && data?.Count > 0)
        //            {
        //                joinCOMMechanism.OTRegulation = data[0].OTRegulation;
        //            }
        //            result.StatementCheckin.COMMechanisms = joinCOMMechanism;
        //        }
        //    }
        //    // Xử lý thông tin dự án tham gia
        //    if (projectStatus == HttpStatusCode.OK && projects?.Count > 0)
        //    {
        //        // Duyệt tất cả các dự án có tham gia trong tháng và trong tương lai
        //        foreach (JoinProjectInfo project in projects.Where(p => p.PERCT > 0)
        //            .Where(p => string.Compare(p.ENDDA, startMonthText) >= 0)
        //            .OrderByDescending(p => p.PERCT))
        //        {
        //            //Tính theo cơ chế đơn vị
        //            Mechanism departmentMechanism = mechanismService.GetByHCMCode(project.POLICY);
        //            (List<ProjectInfo> data, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetListProjectInfomations(project.SNAME);
        //            ProjectInfo prj = data.Where(proj => proj.PSPID == project.PSPID).FirstOrDefault();
        //            result.Projects.Add(new ProjectCheckin
        //            {
        //                CH = project.POLICY,
        //                Id = project.PSPID,
        //                Name = project.SNAME,
        //                StartDate = project.BEGDA,
        //                EndDate = project.ENDDA,
        //                PERCT = project.PERCT,
        //                PROJ_TYPE_NAME = departmentMechanism?.MechanismName,
        //                PERNR = prj?.PERNR
        //            });
        //        }
        //    }
        //    // Xử lý thông tin phòng ban tham gia
        //    if (departmentStatus == HttpStatusCode.OK && departments?.Count > 0)
        //    {
        //        // Lấy danh sách thông tin phòng ban tham gia hiện tại
        //        foreach (JoinDepartmentInfo department in departments
        //            .Where(d => !(string.Compare(d.BEGDA, endMonthText) > 0
        //            || string.Compare(d.ENDDA, startMonthText) < 0))
        //            .OrderByDescending(p => p.PROZT))
        //        {
        //            DepartmentCheckin departmentCheckin = STHelper.ConvertObject<DepartmentCheckin, JoinDepartmentInfo>(department);
        //            (HKTEmployeeInfo employeeInfo, HttpStatusCode employeeStatus, string employeeMessage) = await HKTDataAPI.Connection.GetEmployeeInfoAsync(int.Parse(departmentCheckin.HEAD));

        //            departmentCheckin.HEAD_NAME = employeeInfo?.EmployeeUsername;
        //            //Tính theo cơ chế đơn vị
        //            result.Departments.Add(departmentCheckin);
        //        }
        //    }

        //    // Tính định mức OT trong tháng của nhân sự
        //    result.OvertimeQuotas = CalcCurrentOverTimeQuotas(result.Projects, result.StatementCheckin.COMMechanisms, result.Departments, result.StatementCheckin.Level, startMonth, endMonth);
        //    return result;
        //}

        public async Task<MechanismInformationV2> GetMechanismInformationV2ByEmail(User user, DateTime startMonth, DateTime endMonth)
        {
            string startMonthText = startMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            string endMonthText = endMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            MechanismInformationV2 result = new MechanismInformationV2
            {
                Email = user.Email,
                EmployeeCode = string.Format("{0,0:D8}", user.EmployeeCode),
                PositionsInformation = new PositionsInformation(),
                LongtermAllocation = new LongtermAllocation(),
                SorttermAllocation = new SorttermAllocation
                {
                    Projects = new List<JoinUnit>(),
                    Departments = new List<JoinUnit>(),
                    All = new List<JoinUnit>()
                },
                ComMechanisms = null
            };
            // Lấy thông tin nhân sự từ HrOper
            (HKTEmployeeInfo userInfo, HttpStatusCode userStatus, string userMessage) = await HKTDataAPI.Connection.GetEmployeeInfoAsync(user.EmployeeCode);
            (List<HKTStatementInfo> statementInfos, HttpStatusCode statementStatus, string statementMessage) = await HKTDataAPI.Connection.GetStatementInfoAsync(user.EmployeeCode);
            // Lấy danh sách thông tin cơ chế lương thưởng đã tham gia
            (List<JoinCOMMechanism> mechanisms, HttpStatusCode mechanismStatus, string mechanismMessage) = await HKTDataAPI.Connection.GetCOMMechanismForUser(user.EmployeeCode);
            // Lấy thông tin dự án đã tham gia
            (List<JoinProjectInfo> projects, HttpStatusCode projectStatus, string projectMessage) = await HKTDataAPI.Connection.GetProjectInformationsForUser(user.EmployeeCode);
            // Lấy thông tin phòng ban đang tham gia
            (List<JoinDepartmentInfo> departments, HttpStatusCode departmentStatus, string departmentMessage) = await HKTDataAPI.Connection.GetDepartmentInformationsForUser(user.EmployeeCode, endMonth);
            // Nếu 1 trong các API lấy thông tin từ HKT
            if (userStatus != HttpStatusCode.OK || statementStatus != HttpStatusCode.OK || mechanismStatus != HttpStatusCode.OK || projectStatus != HttpStatusCode.OK || departmentStatus != HttpStatusCode.OK)
            {
                return null;
            }

            // Lấy ra quyết định cuối cùng trong tháng
            HKTStatementInfo statementInfo = statementInfos?
                .Where(s => string.Compare(s.StatementValidDate, endMonthText) <= 0)
                //.Where(s => string.Compare(s.StatementExpiredDate, startMonthText) >= 0)
                .OrderByDescending(s => s.StatementValidDate).FirstOrDefault();

            if (statementInfo == null)
            {
                return null;
            }

            result.UserName = userInfo?.EmployeeUsername;

            // SetPosition
            if (!string.IsNullOrEmpty(statementInfo.NG1))
            {
                CommonItem track1 = GetCommonItems(1, null, ConfirmAllocationManager.TrackRoot)
                    .Where(c => c.Name == statementInfo.NG1).FirstOrDefault();
                if (track1 != null)
                {
                    result.PositionsInformation.Track1 = string.Format("{0} - {1}", track1.Name, track1.Description);
                }
            }

            if (!string.IsNullOrEmpty(statementInfo.NG2))
            {
                CommonItem track2 = GetCommonItems(2, null, ConfirmAllocationManager.TrackRoot)
                     .Where(c => c.Name == statementInfo.NG2).FirstOrDefault();
                if (track2 != null)
                {
                    result.PositionsInformation.Track2 = string.Format("{0} - {1}", track2.Name, track2.Description);
                }
            }
            result.PositionsInformation.Grade = statementInfo?.CB;
            //result.PositionsInformation.Corporation = statementInfo?.PT.Split("-").First() ?? string.Empty;

            // SetLongTerm
            // Org
            result.LongtermAllocation.OrgUnitLv2 = statementInfo?.SP;
            result.LongtermAllocation.OrgUnitLv3 = statementInfo?.Org3;
            result.LongtermAllocation.OrgUnitLv4 = statementInfo?.CDT;
            result.LongtermAllocation.OrgUnitLv5 = statementInfo?.PB;
            // Head
            result.LongtermAllocation.HeadOfOrgLv2 = statementInfo?.SpHead;
            result.LongtermAllocation.HeadOfOrgLv3 = statementInfo?.Org3Head;
            result.LongtermAllocation.HeadOfOrgLv4 = statementInfo?.CdtHead;
            result.LongtermAllocation.HeadOfOrgLv5 = statementInfo?.PbHead;
            // Loại dự án
            string departmentCode = statementInfo?.DV ?? string.Empty;
            if (!string.IsNullOrEmpty(departmentCode))
            {
                departmentCode = statementInfo.DV;
                result.LongtermAllocation.DepartmentType = GetUnitType(departmentCode);
            }
            // Cơ chế
            result.LongtermAllocation.CH = statementInfo?.CH;
            // Quản lý đơn vị
            result.LongtermAllocation.KH200 = statementInfo?.KH200;

            // Set ComMechanism
            // Xử lý thông tin cơ chế lương thưởng
            if (mechanisms?.Count > 0)
            {
                // Lấy thông tin cơ chế lương thưởng cuối cùng trong tháng đang tham gia
                JoinCOMMechanism joinCOMMechanism = mechanisms
                    .Where(m => !(string.Compare(m.ENDDA, startMonthText) < 0
                    || string.Compare(m.BEGDA, endMonthText) > 0))
                    .OrderByDescending(m => m.BEGDA)
                    .FirstOrDefault();
                if (!string.IsNullOrEmpty(joinCOMMechanism?.COMMechanismCode))
                {
                    COMMechanismInput inputMechanism = new COMMechanismInput
                    {
                        COMMechanismCode = joinCOMMechanism.COMMechanismCode
                    };
                    (List<COMMechanism> data, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetCOMMechanismList(inputMechanism, startMonth, endMonth);
                    if (status == HttpStatusCode.OK && data?.Count > 0)
                    {
                        joinCOMMechanism.OTRegulation = data[0].OTRegulation;
                    }
                    result.ComMechanisms = joinCOMMechanism;
                }
            }

            // SetSortTerm
            // Xử lý thông tin dự án tham gia
            if (projectStatus == HttpStatusCode.OK && projects?.Count > 0)
            {
                // Duyệt tất cả các dự án có tham gia trong tháng và trong tương lai
                foreach (JoinProjectInfo project in projects.Where(p => p.PERCT > 0)
                    .Where(p => string.Compare(p.ENDDA, startMonthText) >= 0)
                    .OrderByDescending(p => p.PERCT))
                {
                    Unit prj = GlobalVariable.Data.Projects.Find(proj => proj.Id == project.PSPID);
                    if (prj != null)
                    {
                        UnitRole unitRole = unitRoleService.GetByHCMCode(project.TITLE);
                        if (unitRole == null)
                        {
                            unitRole = unitRoleService.GetByHCMCode(UnitRole.HCMCodeDefault);
                        }
                        result.SorttermAllocation.Projects.Add(new JoinUnit(prj, unitRole, project.BEGDA, project.ENDDA, project.PERCT));
                        // Loại bỏ dự án trùng với Longterm
                        if (departmentCode.EndsWith(prj.ShortCode))
                        {
                            continue;
                        }
                        string begin = string.Compare(project.BEGDA, startMonthText) < 0 ? startMonthText : project.BEGDA;
                        string end = string.Compare(project.ENDDA, endMonthText) < 0 ? project.ENDDA : endMonthText;
                        result.SorttermAllocation.All.Add(new JoinUnit(prj, unitRole, begin, end, project.PERCT));
                    }
                }
            }

            // Xử lý thông tin phòng ban tham gia
            if (departmentStatus == HttpStatusCode.OK && departments?.Count > 0)
            {
                // Lấy danh sách thông tin phòng ban tham gia hiện tại
                foreach (JoinDepartmentInfo department in departments
                    .Where(d => !(string.Compare(d.BEGDA, endMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) > 0
                    || string.Compare(d.ENDDA, startMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) < 0))
                    .OrderByDescending(p => p.PROZT))
                {
                    Unit org = GlobalVariable.Data.OrgUnits.Find(or => or.Id == department.ORG_ID);
                    if (org != null)
                    {
                        // Loại bỏ dự án trùng với Longterm
                        if (departmentCode == org.Code)
                        {
                            continue;
                        }
                        // Đã có trong project thì thôi không thêm vào All
                        if (result.SorttermAllocation.All.Any(prj => prj.ShortCode == department.ORG_SHORT))
                        {
                            continue;
                        }
                        //Tính theo cơ chế đơn vị
                        string begin = string.Compare(department.BEGDA, startMonthText) < 0 ? startMonthText : department.BEGDA;
                        string end = string.Compare(department.ENDDA, endMonthText) < 0 ? department.ENDDA : endMonthText;
                        DateTime startDate = DateTime.ParseExact(begin, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, Thread.CurrentThread.CurrentCulture);
                        DateTime endDate = DateTime.ParseExact(end, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, Thread.CurrentThread.CurrentCulture);
                        OrgUnitJoinInfo orgUnitJoinInfo = orgUnitJoinInfoService.GetOrgUnitJoinInfo(user.Id, department.ORG_SHORT, startDate, endDate).FirstOrDefault();
                        double percentage = orgUnitJoinInfo?.Percentage ?? 0;
                        string hcmRole = orgUnitJoinInfo?.HcmRole ?? UnitRole.HCMCodeDefault;
                        UnitRole unitRole = unitRoleService.GetByHCMCode(hcmRole);
                        if (unitRole != null)
                        {
                            unitRole = unitRoleService.GetByHCMCode(UnitRole.HCMCodeDefault);
                        }
                        result.SorttermAllocation.Departments.Add(new JoinUnit(org, unitRole, department.BEGDA, department.ENDDA, 0));
                        result.SorttermAllocation.All.Add(new JoinUnit(org, unitRole, begin, end, orgUnitJoinInfo?.Percentage ?? 0));
                    }
                }
            }

            // Tính định mức OT trong tháng của nhân sự
            result.OvertimeQuotas = CalcCurrentOverTimeQuotasV2(result, startMonth, endMonth);
            return result;
        }

        public double CalcCurrentOverTimeQuotas(List<ProjectCheckin> joinProjectInfos, JoinCOMMechanism joinComMechanism, List<DepartmentCheckin> departments, string levelName, DateTime startMonth, DateTime endMonth)
        {
            double overtimeQuotasProject = 0;
            double overtimeQuotas = 0;

            if (departments?.Count > 0)
            {
                foreach (DepartmentCheckin department in departments)
                {
                    double mechanismQuotas = mechanismQuotasService.GetQuotas(department.HILFM, levelName);
                    if (overtimeQuotas < mechanismQuotas)
                    {
                        overtimeQuotas = mechanismQuotas;
                    }
                }
            }
            // Nếu cơ chế lương thưởng của nhân sự là Không OT thì set giá trị định mức về 0
            if (joinComMechanism != null)
            {
                if (joinComMechanism.OTRegulation?.Trim().ToLower() == COMMechanism.KHONG_OT.ToLower())
                {
                    overtimeQuotas = 0;
                }
                else if (joinComMechanism.OTRegulation?.Trim().ToLower() != COMMechanism.CO_OT.ToLower())
                {
                    overtimeQuotas = mechanismQuotasService.GetQuotas(joinComMechanism.OTRegulation, levelName);
                }
            }

            // Tính theo Project nhân sự tham giá
            if (joinProjectInfos != null && joinProjectInfos.Count > 0)
            {
                // Lấy ra project nhân sự tham gia trong tháng có % > 70 và số thời gian tham gia >= 15 ngày
                List<ProjectCheckin> projects = joinProjectInfos
                    .Where(p => !(p.StartDate.CompareTo(endMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) > 0
                    || p.EndDate.CompareTo(startMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) < 0))
                    .Where(p => p.PERCT >= 70).ToList();
                foreach (ProjectCheckin project in projects)
                {
                    DateTime startJoin = DateTime.Parse(project.StartDate);
                    startJoin = startJoin > startMonth ? startJoin : startMonth;
                    DateTime endJoin = DateTime.Parse(project.EndDate);
                    endJoin = endJoin < endMonth ? endJoin : endMonth;
                    int joinDateNumber = endJoin.Subtract(startJoin).Days;
                    if (joinDateNumber >= 15)
                    {
                        double quotas = mechanismQuotasService.GetQuotas(project.PROJ_TYPE_NAME, levelName);
                        overtimeQuotasProject = quotas > overtimeQuotasProject ? quotas : overtimeQuotasProject;
                    }
                }
            }
            overtimeQuotas = overtimeQuotas > overtimeQuotasProject ? overtimeQuotas : overtimeQuotasProject;
            return overtimeQuotas;
        }

        public double CalcCurrentOverTimeQuotasV2(MechanismInformationV2 mechanism, DateTime startMonth, DateTime endMonth)
        {
            double overtimeQuotasProject = 0;
            double overtimeQuotas = mechanismQuotasService.GetQuotas(mechanism.LongtermAllocation.CH, mechanism.PositionsInformation.Grade);

            // Nếu cơ chế lương thưởng của nhân sự là Không OT thì set giá trị định mức về 0
            if (mechanism.ComMechanisms != null)
            {
                if (mechanism.ComMechanisms.OTRegulation?.Trim().ToLower() == COMMechanism.KHONG_OT.ToLower())
                {
                    overtimeQuotas = 0;
                }
                else if (mechanism.ComMechanisms.OTRegulation?.Trim().ToLower() != COMMechanism.CO_OT.ToLower())
                {
                    overtimeQuotas = mechanismQuotasService.GetQuotas(mechanism.ComMechanisms.OTRegulation, mechanism.PositionsInformation.Grade);
                }
            }

            // Tính theo Project nhân sự tham gia
            if (mechanism.SorttermAllocation != null && mechanism.SorttermAllocation.Projects.Count > 0)
            {
                // Lấy ra project nhân sự tham gia trong tháng có % > 70 và số thời gian tham gia >= 15 ngày
                List<JoinUnit> projects = mechanism.SorttermAllocation.Projects
                    .Where(p => !(p.StartDate.CompareTo(endMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) > 0
                    || p.EndDate.CompareTo(startMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) < 0))
                    .Where(p => p.Percent >= 70).ToList();
                foreach (JoinUnit project in projects)
                {
                    DateTime startJoin = DateTime.Parse(project.StartDate);
                    startJoin = startJoin > startMonth ? startJoin : startMonth;
                    DateTime endJoin = DateTime.Parse(project.EndDate);
                    endJoin = endJoin < endMonth ? endJoin : endMonth;
                    int joinDateNumber = endJoin.Subtract(startJoin).Days;
                    if (joinDateNumber >= 15)
                    {
                        double quotas = mechanismQuotasService.GetQuotas(project.CH, mechanism.PositionsInformation.Grade);
                        overtimeQuotasProject = quotas > overtimeQuotasProject ? quotas : overtimeQuotasProject;
                    }
                }
            }
            overtimeQuotas = overtimeQuotas > overtimeQuotasProject ? overtimeQuotas : overtimeQuotasProject;
            return overtimeQuotas;
        }

        public CommonItem GetParent(int itemId)
        {
            CommonItem item = commonItemService.GetById(itemId);
            return item;
        }

        public List<CommonItem> GetCommonItems(int level, int? parentId, int rootId)
        {
            List<CommonItem> result = new List<CommonItem>();
            // Lấy ra danh sách parentId 
            List<int> parentIds = new List<int>();
            int startIndx = 1;

            if (parentId == null)
            {
                parentIds.Add(rootId);
            }
            else
            {
                CommonItem parent = commonItemService.GetById(parentId.Value);
                startIndx = GetLevel(parent);
                parentIds.Add(parent.Id);
            }
            if (level >= startIndx)
            {
                for (int indx = startIndx; indx < level; indx++)
                {
                    List<int> childIds = new List<int>();
                    foreach (int id in parentIds)
                    {
                        childIds.AddRange(GetChilds(id).Select(c => c.Id));
                    }
                    parentIds = childIds;
                }
                // Lấy danh sách Track
                foreach (int id in parentIds)
                {
                    result.AddRange(GetChilds(id));
                }
            }
            return result;
        }

        private int GetLevel(CommonItem item)
        {
            int level = 0;
            while (item.ParentId != CommonRoot)
            {
                item = commonItemService.GetById(item.ParentId);
                level++;
            }
            return level;
        }

        private List<CommonItem> GetChilds(int parentId)
        {
            List<CommonItem> childs = commonItemService.GetChilds(parentId);
            return childs;
        }

        public string GetUnitType(string unitCode)
        {
            string type = string.Empty;
            if (GlobalVariable.Data.OrgUnits.Any(og => og.Code == unitCode))
            {
                type = Constants.UNIT_TYPE_ORG;
            }
            else
            {
                type = Constants.UNIT_TYPE_SMALL_PROJECT;
            }
            return type;
        }

        //public async Task<MemoryStream> ExportAllocationRecord(DateTime startDate, DateTime endDate)
        //{
        //    try
        //    {
        //        // Khởi tạo thông số ghi file
        //        var cellNumberFormat = HSSFDataFormat.GetBuiltinFormat("#,##0");
        //        XSSFWorkbook hssfwb;
        //        string tempPath = @"Templates\ExportAllocationRecord.xlsx";
        //        int generalStartRow = 2;

        //        using (FileStream file = new FileStream(tempPath, FileMode.Open, FileAccess.Read))
        //        {
        //            hssfwb = new XSSFWorkbook(file);
        //            file.Close();
        //        }
        //        // Lấy danh sách khai báo Allocation trong tháng
        //        List<ConfirmAllocationV2> confirmAllocations = confirmAllocationV2Service.GetActiveReports(startDate, endDate);

        //        // Lấy sheet OrgUnit - sheet đầu tiên
        //        ISheet orgUnitSheet = hssfwb.GetSheetAt(0);
        //        // Lấy sheet Project - sheet thứ 2
        //        ISheet projectSheet = hssfwb.GetSheetAt(1);
        //        // Lấy sheet COM - sheet thứ 3
        //        ISheet comSheet = hssfwb.GetSheetAt(2);

        //        // Lấy thông tin tham gia OrgUnit
        //        List<(string, ICellStyle)[]> orgUnitDataInRows = new List<(string, ICellStyle)[]>();
        //        // Lấy thông tin tham gia Project
        //        List<(string, ICellStyle)[]> projectDataInRows = new List<(string, ICellStyle)[]>();
        //        // Lấy thông tin cơ chế lương thưởng
        //        List<(string, ICellStyle)[]> comDataInRows = new List<(string, ICellStyle)[]>();
        //        int errId;
        //        foreach (ConfirmAllocation confirmAllocation in confirmAllocations)
        //        {
        //            errId = confirmAllocation.Id;
        //            MechanismInformation newMechanismInfo = JsonConvert.DeserializeObject<MechanismInformation>(confirmAllocation.NewValue);
        //            foreach (DepartmentCheckin org in newMechanismInfo.Departments)
        //            {
        //                (string, ICellStyle)[] rowData = new (string, ICellStyle)[9];
        //                rowData[0] = (confirmAllocation.Id.ToString(), null);
        //                rowData[1] = (newMechanismInfo.EmployeeCode.ToString(), null);
        //                rowData[2] = (newMechanismInfo.Email, null);
        //                rowData[3] = (string.Format("{0} - {1}", org.ORG_SHORT, org.ORG_STEXT), null);
        //                rowData[4] = (org.HEAD_NAME, null);
        //                rowData[5] = (org.HILFM, null);
        //                rowData[6] = (startDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), null);
        //                rowData[7] = (startDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), null);
        //                rowData[8] = (confirmAllocation.Status.ToString(), null);
        //                orgUnitDataInRows.Add(rowData);
        //            }
        //            foreach (ProjectCheckin project in newMechanismInfo.Projects)
        //            {
        //                (string, ICellStyle)[] rowData = new (string, ICellStyle)[10];
        //                rowData[0] = (confirmAllocation.Id.ToString(), null);
        //                rowData[1] = (newMechanismInfo.EmployeeCode.ToString(), null);
        //                rowData[2] = (newMechanismInfo.Email.ToString(), null);
        //                rowData[3] = (project.Name, null);
        //                rowData[4] = (project.PERNR, null);
        //                rowData[5] = (project.PROJ_TYPE_NAME, null);
        //                rowData[6] = (project.PERCT.ToString(), null);
        //                rowData[7] = (project.StartDate, null);
        //                rowData[8] = (project.EndDate, null);
        //                rowData[9] = (confirmAllocation.Status.ToString(), null);
        //                projectDataInRows.Add(rowData);
        //            }
        //            (string, ICellStyle)[] comData = new (string, ICellStyle)[9];
        //            comData[0] = (confirmAllocation.Id.ToString(), null);
        //            comData[1] = (newMechanismInfo.EmployeeCode.ToString(), null);
        //            comData[2] = (newMechanismInfo.Email.ToString(), null);
        //            comData[3] = (newMechanismInfo.StatementCheckin.COMMechanisms?.ICNUM ?? "Không có", null);
        //            comData[4] = (newMechanismInfo.StatementCheckin.COMMechanisms?.Regulation ?? "Không có", null);
        //            comData[5] = (newMechanismInfo.StatementCheckin.COMMechanisms?.OTRegulation ?? "Không có", null);
        //            comData[6] = (startDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), null);
        //            comData[7] = (startDate.ToString(Constants.FORMAT_DATE_HCM_DDMMYYYY), null);
        //            comData[8] = (confirmAllocation.Status.ToString(), null);
        //            comDataInRows.Add(comData);
        //        }

        //        // Ghi thông tin vào file

        //        CommonManager.WriteToSheet(ref orgUnitSheet, orgUnitDataInRows, generalStartRow);
        //        CommonManager.WriteToSheet(ref projectSheet, projectDataInRows, generalStartRow);
        //        CommonManager.WriteToSheet(ref comSheet, comDataInRows, generalStartRow);

        //        var name = string.Format("AllocationRecord{0}{1}{2}{3}{4}{5}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        //        string filePath = @"Exported\" + name + ".xlsx";
        //        using (FileStream file = new FileStream(filePath, FileMode.Create, FileAccess.Write))
        //        {
        //            hssfwb.Write(file);
        //            file.Close();
        //        }
        //        var memory = new MemoryStream();
        //        using (var stream = new FileStream(filePath, FileMode.Open))
        //        {
        //            await stream.CopyToAsync(memory);
        //        }
        //        memory.Position = 0;
        //        return memory;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<UnitRole> GetUnitRoles()
        {
            return unitRoleService.GetAll();
        }

    }
}
