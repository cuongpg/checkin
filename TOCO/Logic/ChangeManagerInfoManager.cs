﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public class ChangeManagerInfoManager
    {
        private readonly ApplicationDbContext context;
        private readonly ChangeManagerInfoService changeManagerInfoService;
        private readonly UserService userService;
        public ChangeManagerInfoManager(ApplicationDbContext context)
        {
            this.context = context;
            changeManagerInfoService = new ChangeManagerInfoService(context);
            userService = new UserService(context);
        }

        public bool ChangeManager(User employee, User currentManager, User newManager, bool isManual)
        {
            // {{Validate}}
            ChangeManagerInfo pendingChange = changeManagerInfoService.GetMyPendingChangeManagerInfo(employee);
            if (pendingChange != null)
            {
                changeManagerInfoService.DestroyChangeManagerInfo(pendingChange.Id);
            }
            ChangeManagerInfo changeManagerInfo = new ChangeManagerInfo
            {
                UserId = employee.Id,
                OldManagerId = currentManager.Id,
                NewManagerId = newManager.Id
            };
            if (isManual)
            {
                if (employee.Id == currentManager.Id) changeManagerInfo.OldManagerStatus = StatusChangeManagerInfo.Approved;
                if (employee.Id == newManager.Id) changeManagerInfo.NewManagerStatus = StatusChangeManagerInfo.Approved;
            }
            else
            {
                changeManagerInfo.OldManagerStatus = StatusChangeManagerInfo.Approved;
                changeManagerInfo.NewManagerStatus = StatusChangeManagerInfo.Approved;
                changeManagerInfo.Status = StatusChangeManagerInfo.Approved;
            }
            changeManagerInfo.IsManual = isManual ? 1 : 0;
            changeManagerInfo = changeManagerInfoService.Create(changeManagerInfo);
            if (changeManagerInfo == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<ChangeManagerResult> GetMyChangeManagerInfo(User user, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            List<ChangeManagerResult> results = new List<ChangeManagerResult>();

            foreach (ChangeManagerInfo changeManagerInfo in changeManagerInfoService.GetMyChangeManagerInfo(user, out numberOfPages, out totalRecords, request))
            {
                User employee = userService.GetById(changeManagerInfo.UserId);
                User oldManager = userService.GetById(changeManagerInfo.OldManagerId);
                User newManager = userService.GetById(changeManagerInfo.NewManagerId);

                results.Add(new ChangeManagerResult()
                {
                    Id = changeManagerInfo.Id,
                    UserId = employee.Id,
                    UserEmail = employee.Email,
                    OldManagerId = oldManager.Id,
                    OldManagerEmail = oldManager.Email,
                    OldManagerStatus = (int)changeManagerInfo.OldManagerStatus,
                    NewManagerId = newManager.Id,
                    NewManagerEmail = newManager.Email,
                    NewManagerStatus = (int)changeManagerInfo.NewManagerStatus,
                    Status = (int)changeManagerInfo.Status,
                    CreatedAt = changeManagerInfo.CreatedAt,
                    UpdatedAt = changeManagerInfo.UpdatedAt,
                    RejectReason = changeManagerInfo.RejectReason,
                    MyStatus = (int)((changeManagerInfo.NewManagerStatus == StatusChangeManagerInfo.Pending && changeManagerInfo.OldManagerStatus == StatusChangeManagerInfo.Pending) ?
                    changeManagerInfo.Status : (changeManagerInfo.OldManagerStatus != StatusChangeManagerInfo.Pending ? changeManagerInfo.OldManagerStatus : changeManagerInfo.NewManagerStatus))
                });
            }
            return results;
        }

        public int CountPendingChangeManager(User user, bool isManager)
        {
            return changeManagerInfoService.CountPendingChangeManager(user, isManager);
        }


        public List<ChangeManagerResult> GetChangeManagerInfoToApprove(User manager, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            List<ChangeManagerResult> results = new List<ChangeManagerResult>();

            foreach (ChangeManagerInfo changeManagerInfo in changeManagerInfoService.GetChangeManagerInfoToApprove(manager, out numberOfPages, out totalRecords, request))
            {
                User employee = userService.GetById(changeManagerInfo.UserId);
                User oldManager = userService.GetById(changeManagerInfo.OldManagerId);
                User newManager = userService.GetById(changeManagerInfo.NewManagerId);

                results.Add(new ChangeManagerResult()
                {
                    Id = changeManagerInfo.Id,
                    UserId = employee.Id,
                    UserEmail = employee.Email,
                    OldManagerId = oldManager.Id,
                    OldManagerEmail = oldManager.Email,
                    OldManagerStatus = (int)changeManagerInfo.OldManagerStatus,
                    NewManagerId = newManager.Id,
                    NewManagerEmail = newManager.Email,
                    NewManagerStatus = (int)changeManagerInfo.NewManagerStatus,
                    Status = (int)changeManagerInfo.Status,
                    CreatedAt = changeManagerInfo.CreatedAt,
                    UpdatedAt = changeManagerInfo.UpdatedAt,
                    MyStatus = (int)(changeManagerInfo.Status != StatusChangeManagerInfo.Pending ? changeManagerInfo.Status :
                    (manager.Id == oldManager.Id ? changeManagerInfo.OldManagerStatus : changeManagerInfo.NewManagerStatus))
                });
            }
            return results;
        }

        public ChangeManagerInfo GetChangeManagerInfoById(int id)
        {
            return changeManagerInfoService.GetChangeManagerInfoById(id);
        }

        public (bool bOk, string message, ChangeManagerInfo records) ConfirmChangeManager(StatusChangeManagerInfo status, int id, User manager, string rejectReason)
        {
            (bool bOk, string message, ChangeManagerInfo records) = changeManagerInfoService.ConfirmChangeManager(status, id, manager, rejectReason);


            return (bOk, message, records);
        }

        public (bool bOk, string message, ChangeManagerInfo records) DestroyChangeManager(int id, User user)
        {
            ChangeManagerInfo pendingChange = changeManagerInfoService.GetMyPendingChangeManagerInfo(user);
            if (pendingChange.Id == id)
            {
                return changeManagerInfoService.DestroyChangeManagerInfo(id);
            }
            else
            {
                string resultcode = ResultCode.CHANGE_MANAGER_INFOMATION_NOT_MATCH;
                return (false, resultcode, null);
            }
        }

        public List<ChangeManagerResult> GetChangeManagerInfos(string nationality, User user, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            List<ChangeManagerResult> results = new List<ChangeManagerResult>();

            foreach (ChangeManagerInfo changeManagerInfo in changeManagerInfoService.GetChangeManagerInfos(nationality, user, out numberOfPages, out totalRecords, request))
            {
                User employee = userService.GetById(changeManagerInfo.UserId);
                User oldManager = userService.GetById(changeManagerInfo.OldManagerId);
                User newManager = userService.GetById(changeManagerInfo.NewManagerId);

                results.Add(new ChangeManagerResult()
                {
                    Id = changeManagerInfo.Id,
                    UserId = employee.Id,
                    UserEmail = employee.Email,
                    OldManagerId = oldManager.Id,
                    OldManagerEmail = oldManager.Email,
                    OldManagerStatus = (int)changeManagerInfo.OldManagerStatus,
                    NewManagerId = newManager.Id,
                    NewManagerEmail = newManager.Email,
                    NewManagerStatus = (int)changeManagerInfo.NewManagerStatus,
                    Status = (int)changeManagerInfo.Status,
                    CreatedAt = changeManagerInfo.CreatedAt,
                    UpdatedAt = changeManagerInfo.UpdatedAt
                });
            }
            return results;
        }
    }
}
