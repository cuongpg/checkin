﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Models;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Logic
{
    public class UserManager
    {
        private readonly UserService userService;
        private readonly ScriptService scriptService;
        private readonly LeaveDateService leaveDateService;
        private readonly ScriptHistoryService scriptHistoryService;
        private readonly GroupService groupService;
        private readonly UserGroupsService userGroupsService;
        private readonly LeaveCalendarTypeHistoryService leaveCalendarTypeHistoryService;
        private readonly HrbHistoryService hrbHistoryService;

        public UserManager(ApplicationDbContext context)
        {
            userService = new UserService(context);
            scriptService = new ScriptService(context);
            leaveDateService = new LeaveDateService(context);
            scriptHistoryService = new ScriptHistoryService(context);
            groupService = new GroupService(context);
            userGroupsService = new UserGroupsService(context);
            leaveCalendarTypeHistoryService = new LeaveCalendarTypeHistoryService(context);
            hrbHistoryService = new HrbHistoryService(context);
        }

        // Old
        public List<User> GetAll(bool? isActive, int scriptId, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            return userService.GetAll(isActive, scriptId, out numberOfPages, out totalRecords, request);
        }

        public List<User> GetAll(int? type, int scriptId, out int numberOfPages, out int totalRecords, STRequestModel request)
        {
            return userService.GetAll(type, scriptId, out numberOfPages, out totalRecords, request);
        }

        public List<int> GetAll()
        {
            return userService.GetAll();
        }

        public List<User> GetUsersByCorporationType(string corporationType)
        {
            return userService.GetUsersByCorporationType(corporationType);
        }

        public bool Create(User user)
        {
            user = userService.Create(user);
            return user != null;
        }

        public User GetUserByEmail(string email)
        {
            return userService.GetByEmail(email);
        }

        public User GetUserByUserName(string userName)
        {
            return userService.GetByUserName(userName);
        }


        public User GetUserById(int id)
        {
            return userService.GetById(id);
        }

        public List<int> GetListEmployeeHasOvertimeQuotas(string corporationType, DateTime startTime, DateTime endTime)
        {
            return userService.GetListEmployeeHasOvertimeQuotas(corporationType, startTime, endTime);
        }

        public bool Update(User user, int[] groups = null)
        {
            bool ok = false;
            var newUser = userService.Update(user);
            if (newUser != null && groups != null)
            {
                ok = userGroupsService.RemoveAllGroupByUserId(newUser.Id);
                if (ok)
                {
                    ok = CreateUserGroups(groups, newUser);
                }
            }
            return ok;
        }

        public List<LeaveDateResult> GetLeaveDateInMonthByUser(User user, int month, int year, string languge)
        {
            List<LeaveDateResult> result = new List<LeaveDateResult>();
            List<LeaveDate> leaveDates = leaveDateService.GetDateInMonthByUser(month, year, user.Id).Where(ld => ld.ExportTimeKeepingCode != LeaveDate.WK).ToList();
            foreach (LeaveDate leaveDate in leaveDates)
            {
                LeaveDateResult leaveDateResult = STHelper.ConvertObject<LeaveDateResult, LeaveDate>(leaveDate);
                leaveDateResult.Description = MessageManager.GetMessageManager(languge).GetValue(leaveDate.Description);
                result.Add(leaveDateResult);
            }
            return result;
        }

        public bool UpdateLeaveCalendarType(string calendarType, string effectiveDate, int userId)
        {
            DateTime effective = DateTime.Parse(effectiveDate);
            LeaveCalendarTypeHistory leaveCalendarTypeHistoryNow = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(userId, DateTime.Now.Date);

            if (effective <= leaveCalendarTypeHistoryNow.EffectiveDate || leaveCalendarTypeHistoryNow.Code == calendarType) return false;

            LeaveCalendarTypeHistory newLeaveCalendarTypeHistory = new LeaveCalendarTypeHistory
            {
                UserId = userId,
                Code = calendarType,
                EffectiveDate = effective,
                ExpirationDate = DateTime.MaxValue.Date
            };

            // Xóa bỏ các lịch sử trong tương lai
            List<LeaveCalendarTypeHistory> futureHistories = leaveCalendarTypeHistoryService.GetFutureHistories(userId, DateTime.Now.Date).ToList();
            foreach (LeaveCalendarTypeHistory history in futureHistories)
            {
                leaveCalendarTypeHistoryService.Delete(history.Id);
            }

            leaveCalendarTypeHistoryNow.ExpirationDate = newLeaveCalendarTypeHistory.EffectiveDate.AddDays(-1);
            leaveCalendarTypeHistoryService.Update(leaveCalendarTypeHistoryNow);
            leaveCalendarTypeHistoryService.Create(newLeaveCalendarTypeHistory);
            return true;
        }

        public List<WorkProgressResult> GetWorkProgressById(int id)
        {
            List<WorkProgressResult> listWorkProgress = new List<WorkProgressResult>();
            List<HrbHistory> hrbHistories = hrbHistoryService.GetAllByUser(id).ToList();
            List<ScriptHistory> scriptHistories = scriptHistoryService.GetAllByUser(id).ToList();
            List<LeaveCalendarTypeHistory> leaveCalendarTypeHistories = leaveCalendarTypeHistoryService.GetAllByUser(id).ToList();

            List<DateTime> scriptEffectiveTimeLine = scriptHistories.Select(sh => sh.EffectiveDate).ToList();
            scriptEffectiveTimeLine.Sort();
            scriptEffectiveTimeLine.RemoveAt(0);
            List<DateTime> scriptExpirationTimeLine = scriptHistories.Select(sh => sh.ExpirationDate).ToList();
            scriptExpirationTimeLine.Sort();
            scriptExpirationTimeLine.RemoveAt(scriptExpirationTimeLine.Count - 1);

            List<DateTime> leaveCalendarEffectiveTimeLine = leaveCalendarTypeHistories.Select(sh => sh.EffectiveDate).ToList();
            leaveCalendarEffectiveTimeLine.Sort();
            leaveCalendarEffectiveTimeLine.RemoveAt(0);
            List<DateTime> leaveCalendarExpirationTimeLine = leaveCalendarTypeHistories.Select(sh => sh.ExpirationDate).ToList();
            leaveCalendarExpirationTimeLine.Sort();
            leaveCalendarExpirationTimeLine.RemoveAt(leaveCalendarExpirationTimeLine.Count - 1);

            foreach (HrbHistory hrbHistory in hrbHistories)
            {
                List<DateTime> timeLine = new List<DateTime>();
                List<DateTime> effectiveTimeLine = new List<DateTime> { hrbHistory.EffectiveDate };
                effectiveTimeLine.AddRange(scriptEffectiveTimeLine.Where(t => t <= hrbHistory.ExpirationDate && t >= hrbHistory.EffectiveDate && !effectiveTimeLine.Contains(t)).ToList());
                effectiveTimeLine.AddRange(leaveCalendarEffectiveTimeLine.Where(t => t <= hrbHistory.ExpirationDate && t >= hrbHistory.EffectiveDate && !effectiveTimeLine.Contains(t)).ToList());

                List<DateTime> expirationTimeLine = new List<DateTime> { hrbHistory.ExpirationDate };
                expirationTimeLine.AddRange(scriptExpirationTimeLine.Where(t => t <= hrbHistory.ExpirationDate && t >= hrbHistory.EffectiveDate && !expirationTimeLine.Contains(t)).ToList());
                expirationTimeLine.AddRange(leaveCalendarExpirationTimeLine.Where(t => t <= hrbHistory.ExpirationDate && t >= hrbHistory.EffectiveDate && !expirationTimeLine.Contains(t)).ToList());
                timeLine.AddRange(effectiveTimeLine);
                timeLine.AddRange(expirationTimeLine);
                timeLine.Sort();

                for (int indx = 0; indx < timeLine.Count - 1; indx += 2)
                {
                    ScriptHistory scriptHistory = scriptHistories.Where(sh => sh.ExpirationDate >= timeLine[indx] && sh.EffectiveDate <= timeLine[indx + 1]).FirstOrDefault();
                    string script = scriptHistory != null ? scriptService.GetById(scriptHistory.ScriptId).SubScript : string.Empty;
                    LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistories.Where(sh => sh.ExpirationDate >= timeLine[indx] && sh.EffectiveDate <= timeLine[indx + 1]).FirstOrDefault();
                    string calendaType = leaveCalendarTypeHistory != null ? leaveCalendarTypeHistory.Code: string.Empty;
                    listWorkProgress.Add(new WorkProgressResult
                    {
                        EffectiveDate = timeLine[indx].ToString(Constants.FORMAT_DATE_DDMMYYYY),
                        ExpirationDate = timeLine[indx + 1].ToString(Constants.FORMAT_DATE_DDMMYYYY),
                        Script = script,
                        CalendarType = calendaType,
                        Status = (int)hrbHistory.HrbStatus
                    });
                }
            }

            return listWorkProgress;
        }

        public List<LeaveDateResult> GetWeekendInMonthByUser(User user, int month, int year, string languge)
        {
            List<LeaveDateResult> result = new List<LeaveDateResult>();
            List<LeaveDate> leaveDates = leaveDateService.GetDateInMonthByUser(month, year, user.Id).Where(ld => ld.ExportTimeKeepingCode == LeaveDate.WK).ToList();
            foreach (LeaveDate leaveDate in leaveDates)
            {
                LeaveDateResult leaveDateResult = STHelper.ConvertObject<LeaveDateResult, LeaveDate>(leaveDate);
                leaveDateResult.Description = MessageManager.GetMessageManager(languge).GetValue(leaveDate.Description);
                result.Add(leaveDateResult);
            }
            return result;
        }

        public bool Delete(int id)
        {
            return userService.Delete(id);
        }

        public List<string> GetRoutesByEmail(string email)
        {
            return userService.GetRoutesByEmail(email);
        }

        public List<UserResult> GetMyActiveStaffs(User user, out int numberOfPages, out int totalRecords, STRequestModel request = null)
        {
            List<UserResult> result = new List<UserResult>();
            List<User> myStaffs = userService.GetActiveUsersByManagerId(user.Id, out numberOfPages, out totalRecords, request);
            foreach (User staff in myStaffs)
            {
                UserResult staffResult = STHelper.ConvertObject<UserResult, User>(staff);
                ScriptHistory scriptHistoryStaff = scriptHistoryService.GetScriptInDay(staff.Id, DateTime.Now.Date);
                if (scriptHistoryStaff != null)
                {
                    Script scriptStaff = scriptService.GetById(scriptHistoryStaff.ScriptId);
                    staffResult.ScriptName = scriptStaff.SubScript;
                }
                result.Add(staffResult);
            }
            return result;
        }

        public List<UserResult> GetAllManager(int groupId, string email, int page, int perPage, string keyWord, out int totalPages)
        {
            User user = userService.GetByEmail(email);
            List<UserResult> result = new List<UserResult>();
            List<User> managers = userService.GetUserByGroups(groupId, page, perPage, keyWord, out totalPages);
            foreach (User u in managers)
            {
                UserResult usertResult = STHelper.ConvertObject<UserResult, User>(u);
                result.Add(usertResult);
            }
            return result;
        }
        public bool CheckIsManagerForStaff(string staffEmail, string managerEmail)
        {
            bool bOk = true;
            User user = GetUserByEmail(staffEmail);
            User manager = GetUserByEmail(managerEmail);
            if (user == null || manager.Id != user.ManagerId)
            {
                bOk = false;
            }
            return bOk;
        }

        public IEnumerable<int> GetUserIdsByScript(int id, DateTime startDate, DateTime endDate)
        {
            return scriptHistoryService.GetUserIdsByScript(id, startDate, endDate);
        }

        /// <summary>
        /// get all calendar type
        /// </summary>
        /// <returns>List carlendar type</returns>
        public List<LeaveCalendarType> GetAllCalendarTypes()
        {
            return userService.GetAllCalendarTypes();
        }

        public bool Deactive(User user, DateTime disableDate)
        {
            bool bOk = true;
            //user.Status = UserStatus.Deactive;
            //bOk &= Update(user);

            List<ScriptHistory> oldHistories = scriptHistoryService.GetByUserAndTime(user.Id, disableDate, DateTime.MaxValue).ToList();

            foreach (var item in oldHistories)
            {
                if (item.EffectiveDate > disableDate)
                {
                    scriptHistoryService.Delete(item.Id);
                }
                else
                {
                    item.ExpirationDate = disableDate;
                    bOk &= scriptHistoryService.Update(item) != null;
                }
            }
            return bOk;
        }

        public bool Active(User user, DateTime activeDate)
        {
            bool bOk = true;

            List<ScriptHistory> oldHistories = scriptHistoryService.GetByUserAndTime(user.Id, activeDate, DateTime.MaxValue).ToList();

            foreach (var item in oldHistories)
            {
                if (item.EffectiveDate > activeDate)
                {
                    scriptHistoryService.Delete(item.Id);
                }
                else
                {
                    item.ExpirationDate = activeDate.AddDays(-1);
                    bOk &= scriptHistoryService.Update(item) != null;
                }
            }

            ScriptHistory lastHistory = scriptHistoryService.GetAllByUser(user.Id).OrderByDescending(sh => sh.ExpirationDate).FirstOrDefault();

            ScriptHistory newHistory = new ScriptHistory
            {
                UserId = user.Id,
                EffectiveDate = activeDate,
                ExpirationDate = DateTime.MaxValue,
                ScriptId = lastHistory != null ? lastHistory.ScriptId : 2
            };

            bOk &= scriptHistoryService.Create(newHistory) != null;

            return bOk;
        }


        public List<CorporationType> GetAllCorporationTypes()
        {
            return userService.GetAllCorporationTypes();
        }

        public List<Group> GetGroupsByUserId(User user)
        {
            return groupService.GetByUserId(user);
        }

        public bool CreateUserGroups(int[] groups, User user)
        {
            List<UserGroup> userGroups = new List<UserGroup>();
            foreach (var group in groups)
            {
                UserGroup ug = new UserGroup
                {
                    UserId = user.Id,
                    GroupId = group
                };
                userGroups.Add(userGroupsService.Create(ug));
            }
            return true;
        }

        public List<User> GetUserByGroup(int groupId)
        {
            return userService.GetAllUserByGroups(groupId);
        }

        public List<int> GetAllManagerByCorporationType(string corporationType)
        {
            return userService.GetAllManagerByCorporationType(corporationType);
        }
    }
}
