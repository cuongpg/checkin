﻿using TOCO.Common;
using TOCO.Implements;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TOCO.Models;
using TOCO.Helpers;
using System.Net.Http;
using System.Threading;

namespace TOCO.API
{
    public class HKTDataAPI : BaseDataApi
    {
        public static HKTDataAPI Connection = new HKTDataAPI();

        private HKTDataAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("HKT:SystemName");
            tokenSchema = AppSetting.Configuration.GetValue("HKT:TokenSchema");
            token = AppSetting.Configuration.GetValue("HKT:AuthToken");
            urlApi = AppSetting.Configuration.GetValue("HKT:UrlApi");
        }

        public async Task<(List<COMMechanism>, HttpStatusCode, string)> GetCOMMechanismList(COMMechanismInput input, DateTime startMonth, DateTime endMonth)
        {
            string timeNow = DateTime.Now.ToString(Constants.FORMAT_DATE_YYYYMMDD);
            string urlPrefix = AppSetting.Configuration.GetValue("HKT:UrlCOMMechanismPrefix");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<COMMechanism> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<COMMechanism>>(uriBuilder.Uri, HttpMethod.Post, null, input);
            return (responseData.Where(m => !(string.Compare(m.ENDDA, startMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) < 0
            || string.Compare(m.BEGDA, endMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) > 0)).ToList(), status, message);
        }

        public async Task<(List<JoinProjectInfo>, HttpStatusCode, string)> GetProjectInformationsForUser(int employeeCode)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlProjectInfomationsForUserPrefix"), employeeCode);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<JoinProjectInfo> responseData, HttpStatusCode status, string message) =
                await GetFromAPI<List<JoinProjectInfo>>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<JoinDepartmentInfo>, HttpStatusCode, string)> GetDepartmentInformationsForUser(int employee_code, DateTime startMonth)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlDepartmentJoinPrefix"), employee_code, startMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD));
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<JoinDepartmentInfo> responseData, HttpStatusCode status, string message) =
                await GetFromAPI<List<JoinDepartmentInfo>>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<JoinCOMMechanism>, HttpStatusCode, string)> GetCOMMechanismForUser(int employee_code)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlCOMMechanismForUserPrefix"), employee_code);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<JoinCOMMechanism> responseData, HttpStatusCode status, string message) =
                await GetFromAPI<List<JoinCOMMechanism>>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<ProjectInfo>, HttpStatusCode, string)> GetListProjectInfomations(string sname = null)
        {
            string urlPrefix;
            if (string.IsNullOrEmpty(sname))
            {
                urlPrefix = AppSetting.Configuration.GetValue("HKT:UrlProjectInfomationsPrefix");
            }
            else
            {
                urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlProjectSearchPrefix"), sname);
            }
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<ProjectInfo> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<ProjectInfo>>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<OrgUnitResult>, HttpStatusCode, string)> GetListOrgUnit(int? level)
        {
            string urlPrefix = AppSetting.Configuration.GetValue("HKT:UrlOrgUnitPrefix");
            List<OrgUnitResult> result = new List<OrgUnitResult>();
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (OrgUnit responseData, HttpStatusCode status, string message) = await GetFromAPI<OrgUnit>(uriBuilder.Uri, HttpMethod.Post);
            if (status == HttpStatusCode.OK)
            {
                List<OrgUnit> listOrgUnit = new List<OrgUnit> { responseData };

                if (level.HasValue)
                {
                    for (int index = 1; index < level; index++)
                    {
                        listOrgUnit = GetOrgUnitChildren(listOrgUnit);
                    }
                    foreach (OrgUnit orgUnit in listOrgUnit)
                    {
                        OrgUnitResult orgUnitResult = STHelper.ConvertObject<OrgUnitResult, OrgUnit>(orgUnit);
                        orgUnitResult.LEVEL = level.Value;
                        result.Add(orgUnitResult);
                    }
                }
                else
                {
                    level = 1;
                    while (listOrgUnit.Count > 0)
                    {
                        foreach (OrgUnit orgUnit in listOrgUnit)
                        {
                            OrgUnitResult orgUnitResult = STHelper.ConvertObject<OrgUnitResult, OrgUnit>(orgUnit);
                            orgUnitResult.LEVEL = level.Value;
                            result.Add(orgUnitResult);
                        }
                        listOrgUnit = GetOrgUnitChildren(listOrgUnit);
                        level++;
                    }
                }
            }

            return (result, status, message);
        }

        private List<OrgUnit> GetOrgUnitChildren(List<OrgUnit> parentOrgUnits)
        {
            List<OrgUnit> childrens = new List<OrgUnit>();
            foreach (OrgUnit parentOrgUnit in parentOrgUnits)
            {
                if (parentOrgUnit.Children != null)
                {
                    foreach (OrgUnit childOrg in parentOrgUnit.Children)
                    {
                        childOrg.PARID = parentOrgUnit.OBJID;
                        childrens.Add(childOrg);
                    }
                }
            }
            return childrens;
        }

        public async Task<(HKTEmployeeInfo, HttpStatusCode, string)> GetEmployeeInfoAsync(int employeeCode)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlEmployeeInfoPrefix"), employeeCode);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (HKTEmployeeInfo responseData, HttpStatusCode status, string message) = await GetFromAPI<HKTEmployeeInfo>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<HKTStatementInfo>, HttpStatusCode, string)> GetStatementInfoAsync(int employeeCode)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlStatementInfoPrefix"), employeeCode);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<HKTStatementInfo> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<HKTStatementInfo>>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<KDSObject>, HttpStatusCode, string)> GetListLevelAsync()
        {
            string urlPrefix = AppSetting.Configuration.GetValue("HKT:UrlLevelPrefix");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<KDSObject> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<KDSObject>>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(List<KDSObject>, HttpStatusCode, string)> GetListTrackAsync(int level, string parentCode)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlTrackPrefix"), level);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<KDSObject> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<KDSObject>>(uriBuilder.Uri);
            if (string.IsNullOrEmpty(parentCode))
            {
                responseData = responseData.Where(ng => ng.Code.Substring(0, parentCode.Length) == parentCode).ToList();
            }
            return (responseData, status, message);
        }

        public async Task<(List<HKTStatementInfo>, HttpStatusCode, string)> GetListStatementsAsync(int skip, int take)
        {
            string urlPrefix = AppSetting.Configuration.GetValue("HKT:UrlStatementFullPrefix");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<HKTStatementInfo> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<HKTStatementInfo>>(uriBuilder.Uri, HttpMethod.Post, null, new { skip, take });
            return (responseData, status, message);
        }

        public async Task<(List<EmployeeInfoResult>, HttpStatusCode, string, int)> SearchEmployee(string search, int page, int perPage, string language)
        {
            int totalPages = 0;
            int skip = 0;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlSearchPrefix"), search);
            List<EmployeeInfoResult> result = new List<EmployeeInfoResult>();
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<EmployeeSearch> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<EmployeeSearch>>(uriBuilder.Uri, HttpMethod.Post, null, new
            {
                skip = 0,
                take = 9999,
                keyWord = search
            });

            if (status == HttpStatusCode.OK && responseData != null)
            {
                totalPages = responseData.Count() / perPage;
                if (responseData.Count() % perPage != 0)
                {
                    totalPages++;
                }
                skip = page * perPage;
                foreach (EmployeeSearch employee in responseData.Skip(skip).Take(perPage).ToList())
                {
                    EmployeeInfoResult employeeConvert = new EmployeeInfoResult
                    {
                        Id = employee.EmployeeCode,
                        UrlBase = "http://osscar.topica.vn/",
                        Avatar = string.Format("/public/img_nv/{0}.jpg", employee.Email?.Substring(0, employee.Email.IndexOf("@")).ToLower() ?? string.Empty),
                        Mobile = employee.Mobile,
                        Email = employee.Email?.ToLower() ?? string.Empty,
                        FullName = employee.FullName
                    };

                    result.Add(employeeConvert);
                }
            }
            return (result, HttpStatusCode.OK, message, totalPages);
        }

        public async Task<(EmployeeDetailResult data, HttpStatusCode status, string message)> EmployeeDetail(int id, string language)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HKT:UrlSearchDetailPrefix"), id);
            EmployeeDetailResult result = new EmployeeDetailResult();
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (EmployeeSearchDetail responseData, HttpStatusCode status, string message) = await GetFromAPI<EmployeeSearchDetail>(uriBuilder.Uri);
            EmployeeDetailResult employeeConvert = null;
            if (status == HttpStatusCode.OK && responseData != null)
            {
                employeeConvert = STHelper.ConvertObjectByLanguage<EmployeeDetailResult, EmployeeSearchDetail>(responseData, language);
                employeeConvert.BirthDay = DateTime.ParseExact(responseData.BirthDay, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, Thread.CurrentThread.CurrentCulture).ToString(Constants.FORMAT_DATE_DDMMYYYY) ?? string.Empty;
                employeeConvert.UpdatedAt = DateTime.ParseExact(responseData.UpdatedAt, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, Thread.CurrentThread.CurrentCulture).ToString(Constants.FORMAT_DATE_DDMMYYYY) ?? string.Empty;
                employeeConvert.Id = responseData.EmployeeCode;
                employeeConvert.Email = responseData.Email?.ToLower() ?? string.Empty;
            }
            return (employeeConvert, HttpStatusCode.OK, message);
        }

    }
}
