﻿using Newtonsoft.Json;
using TOCO.Common;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web;
using System.Reflection;

namespace TOCO.API
{
    public class BaseDataApi
    {
        public string systemName;
        protected string token = string.Empty;
        protected string tokenSchema = string.Empty;
        protected string urlApi = string.Empty;
        protected int apiTimeOut;

        public BaseDataApi()
        {
            apiTimeOut = int.Parse(AppSetting.Configuration.GetValue("APITimeout"));
        }

        protected void GetQuery<T>(T queryData, ref UriBuilder uriBuilder)
        {
            NameValueCollection query = HttpUtility.ParseQueryString(uriBuilder.Query);
            foreach(var item in GetKeyValuePairs(queryData))
            {
                query[item.Key] = (string)item.Value;
            }
            uriBuilder.Query = query.ToString();
        }

        protected Dictionary<string, object> GetKeyValuePairs<T>(T queryData)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            Dictionary<string, object> keyValuePairs = new Dictionary<string, object>();
            foreach (PropertyInfo prop in properties)
            {
                object value = string.Empty;
                if ((value is string) ? !string.IsNullOrEmpty((string)(value = prop.GetValue(queryData))) : value != null)
                {
                    string param = string.Empty;
                    foreach (object attr in prop.GetCustomAttributes(true))
                    {
                        param += (attr as JsonPropertyAttribute).PropertyName;
                    }
                    keyValuePairs[param] = value;
                }
            }
            return keyValuePairs;
        }

        protected async Task<(T, HttpStatusCode, string code)> GetFromAPI<T>(Uri uri, HttpMethod method = null, Dictionary<string, object> headers = null, object body = null)
        {
            var handler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };
            HttpClient client = new HttpClient(handler);
            client.DefaultRequestHeaders.Add("User-Agent", "Toco App");
            string resultCode = string.Empty;

            HttpStatusCode status = HttpStatusCode.OK;
            T result = default;
            if (!string.IsNullOrEmpty(tokenSchema))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenSchema, token);
            }

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = method ?? HttpMethod.Get,
                RequestUri = uri,
            };
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    if (header.Value is string)
                    {
                        httpRequestMessage.Headers.Add(header.Key, (string)header.Value);
                    }
                    if (header.Value is IEnumerable<string>)
                    {
                        httpRequestMessage.Headers.Add(header.Key, (IEnumerable<string>)header.Value);
                    }
                }
            }
            if (body != null)
            {
                httpRequestMessage.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            }
            CancellationTokenSource cts = new CancellationTokenSource(apiTimeOut);
            try
            {
                HttpResponseMessage response = await client.SendAsync(httpRequestMessage, cts.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    try
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<T>(data);
                        resultCode = ResultCode.OK;
                    }
                    catch (Exception ex)
                    {
                        resultCode = ex.StackTrace.ToString();
                    }
                    status = HttpStatusCode.OK;
                }
                else
                {
                    status = response.StatusCode;
                    resultCode = await response.Content.ReadAsStringAsync();
                }
                response.Dispose();
            }
            catch (Exception)
            {
                resultCode = ResultCode.INTERNAL_SERVER_ERROR;
                status = HttpStatusCode.InternalServerError;
            }
            finally
            {
                cts.Dispose();
                httpRequestMessage.Dispose();
                client.Dispose();
            }
            return (result, status, resultCode);
        }


        protected async Task<(dynamic, HttpStatusCode, string code)> GetFromAPI(string url)
        {
            HttpClient client = new HttpClient();
            string resultCode = string.Empty;
            dynamic result = null;
            HttpStatusCode status = HttpStatusCode.OK;


            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(url)
            };
            CancellationTokenSource cts = new CancellationTokenSource(apiTimeOut);
            try
            {
                HttpResponseMessage response = await client.SendAsync(httpRequestMessage, cts.Token);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string data = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject(data);
                    resultCode = ResultCode.OK;
                    status = HttpStatusCode.OK;
                }
                else
                {
                    status = response.StatusCode;
                    resultCode = string.Format(ResultCode.ERR_GET_INFO, this.GetType());
                }
                response.Dispose();
            }
            catch (Exception ex)
            {
                status = HttpStatusCode.InternalServerError;
                resultCode = ex.Message;
            }
            finally
            {
                cts.Dispose();
                httpRequestMessage.Dispose();
                client.Dispose();
            }
            return (result, status, resultCode);
        }
    }
}