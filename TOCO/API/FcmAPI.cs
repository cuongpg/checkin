﻿using Newtonsoft.Json;
using TOCO.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TOCO.Models;
using System.Text.RegularExpressions;
using TOCO.Helpers;

namespace TOCO.API
{
    public class FcmAPI
    {
        public static FcmAPI Connection = new FcmAPI();
        private string authToken = string.Empty;

        private FcmAPI()
        {
        }

        public bool SendPushNotification(List<string> registration_ids, Notification notification, int badge)
        {
            bool bOk;
            string str = string.Empty;
            try
            {
                string applicationID = AppSetting.Configuration.GetValue("FCM:FCMAuthorizationKey");
                string urlApi = AppSetting.Configuration.GetValue("FCM:UrlApi");
                WebRequest tRequest = WebRequest.Create(urlApi);
                tRequest.Method = Constants.POST_METHOD;
                tRequest.ContentType = Constants.APPLICATION_JSON_CONTENT_TYPE;

                var data = new
                {
                    registration_ids,
                    notification = new
                    {
                        title = notification.Title,
                        body = STHelper.RemoveHtmlTag(notification.Message),
                        sound = "default",
                        badge
                    },
                    data = new
                    {
                        notification.DocumentId,
                        notification.DocumentType,
                        notification.CreatedAt,
                        notification.UpdatedAt
                    }
                };

                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format(Constants.AUTHOR_FORMAT, applicationID));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                str = tReader.ReadToEnd();
                                dynamic response = JsonConvert.DeserializeObject(str);
                                if(response.success == 0 && response.failure > 0)
                                {
                                    bOk = false;
                                }
                                else
                                {
                                    bOk = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bOk = false;
                str = ex.Message;
            }
            return bOk;
        }
    }
}
