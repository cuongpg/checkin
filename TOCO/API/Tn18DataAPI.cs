﻿using TOCO.Common;
using TOCO.Implements;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TOCO.Models;
using TOCO.Helpers;
using System.Globalization;
using System.Reflection;

namespace TOCO.API
{
    public class Tn18DataAPI : BaseDataApi
    {
        public static Tn18DataAPI Connection = new Tn18DataAPI();
        private readonly string AuthToken = string.Empty;

        private Tn18DataAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("Tn18:SystemName");
            token = AppSetting.Configuration.GetValue("Tn18:Token");
            tokenSchema = AppSetting.Configuration.GetValue("Tn18:TokenSchema");
            urlApi = AppSetting.Configuration.GetValue("Tn18:UrlApi");
        }

        public async Task<(List<IncomeInforResult>, HttpStatusCode, string, int)> GetIncomeInformationAsync(string employeeCode, int? month, int? year, string corporation, int page, int perPage)
        {
            List<IncomeInforResult> incomeInformations = new List<IncomeInforResult>();
            int totalPages = 0;

            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Tn18:UrlEmployeeInfo"), employeeCode, page, perPage);
            if (!string.IsNullOrEmpty(corporation))
            {
                urlPrefix += "&phap_nhan=" + corporation;
            }
            if (month.HasValue)
            {
                urlPrefix += "&thang=" + month.Value;
            }
            if (year.HasValue)
            {
                urlPrefix += "&nam=" + year.Value;
            }
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (Tn18IncomeInfoResult responseData, HttpStatusCode status, string message) = await GetFromAPI<Tn18IncomeInfoResult>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK && responseData != null)
            {
                foreach (Tn18IncomeInfo incomeInfo in responseData.Data)
                {
                    IncomeInforResult incomeInfoResult = STHelper.ConvertObject<IncomeInforResult, Tn18IncomeInfo>(incomeInfo);
                    incomeInformations.Add(incomeInfoResult);
                }
                totalPages = responseData.Total / perPage;
                if (responseData.Total % perPage > 0)
                {
                    totalPages++;
                }
            }

            return (incomeInformations, status, string.Empty, totalPages);
        }

        public async Task<(Dictionary<string, Dictionary<string, string>>, HttpStatusCode, string)> GetSalaryInformationAsync(string employeeCode, int month, int year, string language)
        {
            //CultureInfo info = MessageManager.GetCultureInfo(language);
            CultureInfo info = MessageManager.GetCultureInfo("vi");
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Tn18:UrlSalaryInfo"), employeeCode, month, year);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            //float currencyConvert = await GetCurrencyConvertAsync(language);
            float currencyConvert = 1;
            Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
            (Tn18SalaryInfoResult responseData, HttpStatusCode status, string message) = await GetFromAPI<Tn18SalaryInfoResult>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK && responseData != null)
            {
                MessageManager messageManager = MessageManager.GetMessageManager(language);

                // Tạo thông tin thu nhập
                double personalIncomeTax = responseData.Data.PersonalIncomeTax.PersonalIncomeTax ?? 0;
                double taxDeductible = responseData.Data.PersonalIncomeTax.TaxDeductible ?? 0;
                Dictionary<string, string> incomeInfo = new Dictionary<string, string>
                    {
                        // Tổng thu nhập chưa tính thuế
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_NOT_TAXABLE_TITLE), String.Format(info, "{0:c0}", (responseData.Data.TotalIncome.Total ?? 0) *currencyConvert) },
                        // Tổng thu nhập được giảm trừ thuế
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_TAXABLE_DEDUCTION_TITLE), String.Format(info, "{0:c0}",(responseData.Data.PersonalIncomeTax.TotalTaxableDeduction ?? 0)*currencyConvert) },
                        // Thuế thu nhập cá nhân đúng phải nộp
                        { messageManager.GetValue(Constants.Salary.INCOME_PERSONAL_TAX_TITLE), String.Format(info, "{0:c0}", personalIncomeTax*currencyConvert) },
                        // Thuế đã trích
                        { messageManager.GetValue(Constants.Salary.INCOME_TAX_DEDUCTIBLE_TITLE), String.Format(info, "{0:c0}",taxDeductible *currencyConvert)},
                        // Thuế chênh lệch
                        { messageManager.GetValue(Constants.Salary.INCOME_TAX_DIFFERENCE_TITLE), String.Format(info, "{0:c0}",(personalIncomeTax - taxDeductible)*currencyConvert)},
                        // Tổng các khoản giảm trừ lương
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_DEDUCTION_TITLE), String.Format(info, "{0:c0}",( responseData.Data.Deductions.TotalDeduction ?? 0)*currencyConvert)},
                        // TỔNG THU NHẬP (bao gồm cả Com, thưởng, khác)
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_RECEIVED_TITLE), String.Format(info, "{0:c0}",(responseData.Data.TotalReceived.TotalReceived ?? 0)*currencyConvert) },
                        // Đã thanh toán Com, Thưởng, Khác
                        { messageManager.GetValue(Constants.Salary.INCOME_PAID_TITLE), String.Format(info, "{0:c0}",(responseData.Data.TotalReceived.Paid ?? 0)*currencyConvert) }
                    };
                result.Add(messageManager.GetValue(Constants.Salary.INCOME_INFO_TITLE), incomeInfo);

                // Tạo thông tin chi tiết thu nhập
                Dictionary<string, string> incomeDetail = new Dictionary<string, string>
                    {
                        // Lương thực nhận (theo ngày công và năng suất)
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_REALITY_WORK_SALARY_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Salary.RealityWorkSalary ?? 0)*currencyConvert) },
                        // Tổng OT
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_OT_SALARY_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Salary.TotalOTSalary?? 0)*currencyConvert) },
                        // Tiền làm ngoài giờ theo cơ chế
                        { messageManager.GetValue(Constants.Salary.INCOME_OT_ALLOWANCE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Allowance.OvertimeAllowance?? 0)*currencyConvert) },
                        // Tiền ăn trưa (Tính theo ngày công thực tế)
                        { messageManager.GetValue(Constants.Salary.INCOME_LUNCH_ALLOWANCE_TITLE),String.Format(info, "{0:c0}", (responseData.Data.Allowance.LunchAllowance?? 0)*currencyConvert) },
                        // Tiền gửi xe
                        { messageManager.GetValue(Constants.Salary.INCOME_PARKING_ALLOWANCE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Allowance.ParkingAllowance?? 0 )*currencyConvert)},
                        // Tiền điện thoại
                        { messageManager.GetValue(Constants.Salary.INCOME_MOBILE_ALLOWANCE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Allowance.MobileAllowance?? 0)*currencyConvert) },
                        // Hỗ trợ cổ phần thưởng bằng tiền
                        { messageManager.GetValue(Constants.Salary.INCOME_STOCK_ALLOWANCE_TITLE),String.Format(info, "{0:c0}", (responseData.Data.Allowance.StockAllowance?? 0)*currencyConvert) },
                        // Thưởng kỳ (Thưởng lễ tết)
                        { messageManager.GetValue(Constants.Salary.INCOME_HOLIDAY_BONUS_TITLE), String.Format(info, "{0:c0}",(responseData.Data.OtherRevenues.HolidayBonus?? 0)*currencyConvert) },
                        // Công tác phí
                        { messageManager.GetValue(Constants.Salary.INCOME_WORKING_FEE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Allowance.WorkingFee?? 0)*currencyConvert) },
                        // Tiền khác (nếu có)
                        { messageManager.GetValue(Constants.Salary.INCOME_OTHER_ALLOWANCE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Allowance.OtherAllowance?? 0)*currencyConvert) },
                        // Tiền ăn ca
                        { messageManager.GetValue(Constants.Salary.INCOME_SHIFTEAT_ALLOWANCE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.OtherRevenues.ShiftEatAllowance?? 0)*currencyConvert) },
                        // Truy lĩnh ngày phép
                        { messageManager.GetValue(Constants.Salary.INCOME_LEAVE_RETRIEVAL_TITLE), String.Format(info, "{0:c0}",(responseData.Data.OtherRevenues.LeaveRetrieval?? 0)*currencyConvert) },
                        // Truy lĩnh lương/ Khác
                        { messageManager.GetValue(Constants.Salary.INCOME_SALARY_RETRIEVAL_TITLE), String.Format(info, "{0:c0}",(responseData.Data.OtherRevenues.SalaryRetrieval?? 0)*currencyConvert) },
                        // Thu khác (Thưởng hạ cánh, dự án, các CT đặc biệt)
                        { messageManager.GetValue(Constants.Salary.INCOME_HOLIDAY_RETRIEVAL_TITLE), String.Format(info, "{0:c0}",(responseData.Data.OtherRevenues.HolidayRetrieval?? 0)*currencyConvert) },
                        // Com
                        { messageManager.GetValue(Constants.Salary.INCOME_COM_TITLE),String.Format(info, "{0:c0}", (responseData.Data.OtherRevenues.Com?? 0 )*currencyConvert)},
                        // Thưởng(Ngoài lương đã thanh toán)
                        { messageManager.GetValue(Constants.Salary.INCOME_BONUS_COM_TITLE),String.Format(info, "{0:c0}", (responseData.Data.OtherRevenues.Bonus?? 0)*currencyConvert) },
                        // Thu nhập khác (Ngoài lương đã thanh toán)
                        { messageManager.GetValue(Constants.Salary.INCOME_OTHER_BONUS_TITLE),String.Format(info, "{0:c0}", (responseData.Data.OtherRevenues.Other?? 0)*currencyConvert) },
                        // Truy thu Lương
                        { messageManager.GetValue(Constants.Salary.INCOME_SALARY_REIMBURSEMENT_TITLE),String.Format(info, "{0:c0}", (responseData.Data.Deductions.SalaryReimbursement?? 0)*currencyConvert) },
                        // Truy thu thuế TNCN sau quyết toán 2017
                        { messageManager.GetValue(Constants.Salary.INCOME_TAX_REIMBURSEMENT_TITLE),String.Format(info, "{0:c0}", (responseData.Data.Deductions.TaxReimbursement?? 0)*currencyConvert )},
                        // Hoàn ứng chi phí gửi xe
                        { messageManager.GetValue(Constants.Salary.INCOME_PARKING_REIMBURSEMENT_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Deductions.ParkingReimbursement?? 0)*currencyConvert) },
                        // Hoàn tạm ứng
                        { messageManager.GetValue(Constants.Salary.INCOME_REIMBURSEMENT_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Deductions.Reimbursement?? 0)*currencyConvert) },
                        // Hoàn tạm thừa
                        { messageManager.GetValue(Constants.Salary.INCOME_EXCEED_TEMPORARILY_TITLE),String.Format(info, "{0:c0}", (responseData.Data.Deductions.QuoteCarDeposit?? 0 )*currencyConvert)},
                        // Bảo hiểm bắt buộc
                        { messageManager.GetValue(Constants.Salary.INCOME_TOTAL_INSURRANCE_TITLE), String.Format(info, "{0:c0}",(responseData.Data.Insurrance.TotalInsurrance?? 0)*currencyConvert) },
                    };
                result.Add(messageManager.GetValue(Constants.Salary.INCOME_DETAIL_TITLE), incomeDetail);

                // Tạo thông tin chi tiết thu nhập
                Dictionary<string, string> incomeDetail2 = new Dictionary<string, string>
                    {
                        // Lương  cơ sở
                        { messageManager.GetValue(Constants.Salary.INCOME_BASE_SALARY_TITLE), String.Format(info, "{0:c0}",(responseData.Data.SalaryStructure.BaseSalary?? 0)*currencyConvert )},
                        // Phụ cấp năng suất (Lương năng suất)
                        { messageManager.GetValue(Constants.Salary.INCOME_PRODUCTIVITY_SALARY_TITLE), String.Format(info, "{0:c0}",(responseData.Data.SalaryStructure.ProductivitySalary?? 0 )*currencyConvert)},
                        // Số công định mức trong tháng
                        { messageManager.GetValue(Constants.Salary.INCOME_WORK_DATE_QUOTAS_TITLE), String.Format("{0}",(responseData.Data.WorkDate.WorkDateQuotas?? 0)) },
                        // Số ngày công thực tế
                        { messageManager.GetValue(Constants.Salary.INCOME_WORK_DATE_NUMBER_TITLE), String.Format("{0}",(responseData.Data.WorkDate.WorkDateNumber?? 0)) },
                        // Số giờ OT 150%
                        { messageManager.GetValue(Constants.Salary.INCOME_OT_150_TITLE), String.Format( "{0}h",responseData.Data.WorkDate.OT150?? 0) },
                        // Số giờ OT 200%
                        { messageManager.GetValue(Constants.Salary.INCOME_OT_200_TITLE), String.Format( "{0}h",responseData.Data.WorkDate.OT200?? 0) },
                        // Số giờ OT 300%
                        { messageManager.GetValue(Constants.Salary.INCOME_OT_300_TITLE), String.Format( "{0}h",responseData.Data.WorkDate.OT300?? 0) },
                        // Giảm trừ bản thân
                        { messageManager.GetValue(Constants.Salary.INCOME_PERSONAL_DEDUCTION_TITLE), String.Format(info, "{0:c0}",(responseData.Data.PersonalIncomeTax.PersonalDeduction?? 0)*currencyConvert )},
                        // Giảm trừ gia cảnh
                        { messageManager.GetValue(Constants.Salary.INCOME_FAMILY_DEDUCTION_TITLE), String.Format(info, "{0:c0}",(responseData.Data.PersonalIncomeTax.FamilyDeduction?? 0)*currencyConvert) }
                    };
                result.Add(messageManager.GetValue(Constants.Salary.INCOME_DETAIL_2_TITLE), incomeDetail2);
            }

            return (result, status, string.Empty);
        }

        private async Task<float> GetCurrencyConvertAsync(string language)
        {
            string convert = MessageManager.GetCurrencyInfo(AppSetting.Configuration.GetValue("Culture")) + "_" + MessageManager.GetCurrencyInfo(language);
            string url = String.Format(AppSetting.Configuration.GetValue("Tn18:CurencyConvertApi"), convert);
            (dynamic data, HttpStatusCode status, string message) = await GetFromAPI(url);
            float? value = data[convert];
            return value ?? 1;
        }

        public async Task<(FTTInfoResult data, HttpStatusCode status, string message)> GetFTTInformationAsync(string employeeCode, int month, int year)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Tn18:UrlFtt"), employeeCode, month, year);
            FTTInfoResult result = new FTTInfoResult
            {
                Com = new List<FTTInfo>(),
                Bonus = new List<FTTInfo>(),
                Other = new List<FTTInfo>(),

            };
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<Tn18FTTInfo> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<Tn18FTTInfo>>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK)
            {
                CultureInfo info = MessageManager.GetCultureInfo("vi");
                float currencyConvert = 1;
                foreach (Tn18FTTInfo detail in responseData)
                {
                    FTTInfo fTTInfo = new FTTInfo
                    {
                        Documents = detail.Documents,
                        PaymentDate = detail.PaymentDate,
                        Explain = detail.Explain,
                        TaxableIncome = String.Format(info, "{0:c0}", (detail.TaxableIncome ?? 0) * currencyConvert),
                        TaxTempDeductible = String.Format(info, "{0:c0}", (detail.TaxTempDeductible ?? 0) * currencyConvert),
                        TotalReceived = String.Format(info, "{0:c0}", (detail.TotalReceived ?? 0) * currencyConvert)
                    };
                    if (detail.Type == TN18FTTType.ComCTV || detail.Type == TN18FTTType.ComNV)
                    {
                        result.Com.Add(fTTInfo);
                    }
                    else if (detail.Type == TN18FTTType.ThuongNV || detail.Type == TN18FTTType.ThuongCTV)
                    {
                        result.Bonus.Add(fTTInfo);
                    }
                    else
                    {
                        result.Other.Add(fTTInfo);
                    }
                }
            }

            return (result, status, message);
        }
    }
}
