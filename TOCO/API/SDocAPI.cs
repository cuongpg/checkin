﻿using Newtonsoft.Json;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Models.APIModels.PaoAPIModels.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOCO.Models.APIModels.SDocAPIModels;
using System.Web;
using System.Collections.Specialized;
using Newtonsoft.Json.Serialization;
using TOCO.Models.APIModels.SDocAPIModels.InputModels;

namespace TOCO.API
{
    public class SDocQuery
    {
        [JsonProperty("search")]
        public string Search { get; set; }
        [JsonProperty("page")]
        public string Page { get; set; }
        [JsonProperty("perPage")]
        public string PerPage { get; set; }
        [JsonProperty("populate")]
        public string Populate { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("entity")]
        public string Entity { get; set; }
        [JsonProperty("includeComments")]
        public string IncludeComments { get; set; }
    }

    public class SDocHeader
    {
        [JsonProperty("X-UserEmail")]
        public string XUserMail { get; set; }
    }

    public class SDocAPI : BaseDataApi
    {
        public static SDocAPI Connection = new SDocAPI();

        private SDocAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("SDoc:SystemName");
            token = AppSetting.Configuration.GetValue("SDoc:Token");
            tokenSchema = AppSetting.Configuration.GetValue("SDoc:TokenSchema");
            urlApi = AppSetting.Configuration.GetValue("SDoc:UrlApi");
        }

        public async Task<(int count, HttpStatusCode status, string message)> CountPendingProcessesAsync(SDocHeader sDocHeader)
        {
            int count = 0;
            string urlPrefix = AppSetting.Configuration.GetValue("SDoc:UrlPrefixCountApproveProcess");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            Dictionary<string, object> header = GetKeyValuePairs(sDocHeader);
            (dynamic responseData, HttpStatusCode status, string message) = await GetFromAPI<dynamic>(uriBuilder.Uri, HttpMethod.Get, header, null);
            if (status == HttpStatusCode.OK)
            {
                count = responseData.data;
            }
            return (count, status, message);
        }

        public async Task<(object data, HttpStatusCode status, string message)> GetRisk(SDocHeader sDocHeader, int processId)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("SDoc:UrlPrefixRisk"), processId);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            Dictionary<string, object> header = GetKeyValuePairs(sDocHeader);
            (object responseData, HttpStatusCode status, string message) = await GetFromAPI<object>(uriBuilder.Uri, HttpMethod.Get, header, null);
            return (responseData, status, message);
        }

        public async Task<(object data, HttpStatusCode status, string message)> SubmitRisk(SDocHeader sDocHeader, int processId, object riskData)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("SDoc:UrlPrefixRisk"), processId);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            Dictionary<string, object> header = GetKeyValuePairs(sDocHeader);
            (object responseData, HttpStatusCode status, string message) = await GetFromAPI<object>(uriBuilder.Uri, HttpMethod.Post, header, riskData);
            return (responseData, status, message);
        }

        public async Task<(List<SDocProcessesResult>, HttpStatusCode, string, int)> GetListSDocProcesses(string urlPrefix, SDocHeader sDocHeader, SDocQuery sDocQuery)
        {
            List<SDocProcessesResult> sdocProcesses = null;
            int totalPages = 0;

            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            GetQuery(sDocQuery, ref uriBuilder);
            Dictionary<string, object> header = GetKeyValuePairs(sDocHeader);
            (ListSDocProcessResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<ListSDocProcessResponse>(uriBuilder.Uri, HttpMethod.Get, header, null);
            if (status == HttpStatusCode.OK)
            {
                sdocProcesses = new List<SDocProcessesResult>();
                int taskNumber = responseData?.Data?.Items.Count ?? 0;

                Task[] process = new Task[taskNumber];
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    int i = indTask;
                    process[i] = Task.Run(async () =>
                    {
                        SDocProcessesResult sdocProcessResult = STHelper.ConvertObject<SDocProcessesResult, SDocProcessDocumentModel>(responseData.Data.Items[i]);
                        sdocProcessResult.Avatar = responseData.Data.Items[i].Creator?.Picture;
                        sdocProcessResult.CreatorName = responseData.Data.Items[i].Creator?.FullName;
                        sdocProcessResult.CreatorEmail = responseData.Data.Items[i].Creator?.Email;

                        (List<EmployeeInfoResult> employeeData, HttpStatusCode employeeStatus, string employeeMessage, int total) = await HKTDataAPI.Connection.SearchEmployee(sdocProcessResult.CreatorEmail, 0, 1, "vi");
                        if (employeeStatus == HttpStatusCode.OK && employeeData.Count != 0)
                        {
                            sdocProcessResult.CreatorMobile = employeeData[0].Mobile;
                        }
                        sdocProcesses.Add(sdocProcessResult);
                    });
                }
                Task.WaitAll(process);
                totalPages = responseData.Data.TotalPage;
            }
            return (sdocProcesses, status, message, totalPages);
        }

        public async Task<(List<SDocKindOfDocModel> data, HttpStatusCode status, string message)> GetKindOfDocAsync()
        {
            List<SDocKindOfDocModel> entities = new List<SDocKindOfDocModel>();
            string urlPrefix = AppSetting.Configuration.GetValue("SDoc:UrlPrefixKindOfDocs");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (dynamic responseData, HttpStatusCode status, string message) = await GetFromAPI<dynamic>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK)
            {
                entities.AddRange(JsonConvert.DeserializeObject<List<SDocKindOfDocModel>>(JsonConvert.SerializeObject(responseData.data)));
            }
            return (entities, status, message);

        }

        public async Task<(List<SDocEntityModel> data, HttpStatusCode status, string message)> GetEntitiesAsync()
        {
            List<SDocEntityModel> entities = new List<SDocEntityModel>();
            string urlPrefix = AppSetting.Configuration.GetValue("SDoc:UrlPrefixEntities");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (dynamic responseData, HttpStatusCode status, string message) = await GetFromAPI<dynamic>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK)
            {
                entities.AddRange(JsonConvert.DeserializeObject<List<SDocEntityModel>>(JsonConvert.SerializeObject(responseData.data)));
            }
            return (entities, status, message);

        }

        public async Task<(SDocProcessDetailResult data, HttpStatusCode statusCode, string)> GetPaoProcessDetailAsync(int id, SDocHeader sDocHeader, SDocQuery sDocQuery)
        {
            SDocProcessDetailResult sdocProcesses = null;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("SDoc:UrlPrefixDetailProcess"), id);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            GetQuery(sDocQuery, ref uriBuilder);
            Dictionary<string, object> header = GetKeyValuePairs(sDocHeader);
            (SDocProcessResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<SDocProcessResponse>(uriBuilder.Uri, HttpMethod.Get, header);

            if (status == HttpStatusCode.OK && responseData?.Data != null)
            {
                sdocProcesses = new SDocProcessDetailResult
                {
                    ProcessName = responseData?.Data?.ProcessName,
                    Description = responseData?.Data?.Description,
                    Id = responseData?.Data?.Id.ToString(),
                    // Tạo documents
                    Documents = new List<SDocDocumentResult>(),
                    // Tạo activities
                    Activities = new List<SDocActivityResult>(),
                    // Tạo Comments
                    Comments = new List<SDocCommnentResult>(),
                    // Tạo group
                    Groups = new List<SDocReviewGroupResult>(),
                    // Tạo rejected_reasons
                    RejectedReasons = new List<SDocRejectedReasonResult>(),
                    // Đặt mặc định cho isReviewing là true
                    IsReviewing = Constants.FALSE
                };

                // Lấy thông tin documents
                foreach (SDocDocumentModel document in responseData?.Data?.Documents)
                {
                    SDocDocumentResult docResult = STHelper.ConvertObject<SDocDocumentResult, SDocDocumentModel>(document);
                    sdocProcesses.Documents.Add(docResult);
                }

                // Lấy thông tin activities
                // Lấy thông tin document trong activities
                SDocDocumentModel docSearch = responseData?.Data?.Documents?.Find(delegate (SDocDocumentModel doc)
                {
                    return doc.FileId.Equals(responseData?.Data?.MainFileId);
                });

                foreach (SDocActivityModel activity in responseData?.Data?.Activities)
                {
                    SDocActivityResult actResult = STHelper.ConvertObject<SDocActivityResult, SDocActivityModel>(activity);
                    actResult.FileName = docSearch?.Name;
                    actResult.DocLink = docSearch?.DocLink;
                    actResult.Email = activity.Creator?.Email;
                    actResult.Avatar = activity.Creator?.Picture;
                    actResult.FullName = activity.Creator?.FullName;
                    sdocProcesses.Activities.Add(actResult);
                }

                // Lấy thông tin group reviewer
                foreach (SDocReviewerModel reviewer in responseData?.Data?.Reviewers)
                {
                    if (reviewer.Email == sDocHeader.XUserMail && sdocProcesses.IsReviewing == Constants.FALSE)
                    {
                        sdocProcesses.ApprovalId = reviewer.ApprovalId;
                        sdocProcesses.IsReviewing = reviewer.Status == SDocReviewerModel.SDOC_STATUS_REVIEWING ? Constants.TRUE : Constants.FALSE;
                    }
                    SDocReviewGroupResult searchGroup = sdocProcesses.Groups?.Find(delegate (SDocReviewGroupResult rv)
                    {
                        return rv.Id.Equals(reviewer.GroupNum?.ToString());
                    });
                    if (searchGroup == null)
                    {
                        searchGroup = new SDocReviewGroupResult()
                        {
                            Id = reviewer.GroupNum?.ToString(),
                            Name = reviewer.SmallGroupName,
                            Reviewers = new List<SDocReviewerResult>()
                        };
                        sdocProcesses.Groups.Add(searchGroup);
                    }
                    SDocReviewerResult reviewerResult = STHelper.ConvertObject<SDocReviewerResult, SDocReviewerModel>(reviewer);
                    if (reviewer.Status == ProcessStatus.reviewing)
                    {
                        reviewerResult.UpdatedAt = string.Empty;
                    }
                    reviewerResult.Status = reviewer.Status?.ToString();
                    searchGroup.Reviewers.Add(reviewerResult);
                }

                // Lấy thông tin comment
                foreach (SDocCommentModel comment in responseData?.Data?.Comments)
                {
                    SDocCommnentResult sdocCommentResult = new SDocCommnentResult()
                    {
                        Avatar = comment.Author?.Picture?.Url,
                        DisplayName = comment.Author?.DisplayName,
                        CreatedDate = comment.CreatedDate,
                        Comment = comment.Content,
                        Deleted = comment.Deleted?.ToString(),
                        Replies = new List<SDocReplyResult>()
                    };
                    foreach (SDocReplyModel reply in comment.Replies)
                    {
                        sdocCommentResult.Replies.Add(new SDocReplyResult()
                        {
                            Avatar = reply.Author?.Picture?.Url,
                            DisplayName = reply.Author?.DisplayName,
                            Deleted = reply.Deleted?.ToString(),
                            CreatedDate = reply.CreatedDate,
                            Comment = reply.Content
                        });
                    }
                    sdocProcesses.Comments.Add(sdocCommentResult);
                }

                // Lấy thông tin rejected_reasons
                foreach (SDocRejectedReasonModel rejectReason in responseData?.Data?.RejectedReasons)
                {
                    SDocRejectedReasonResult rejectedResult = STHelper.ConvertObject<SDocRejectedReasonResult, SDocRejectedReasonModel>(rejectReason);
                    rejectedResult.Email = rejectReason.Reviewer?.Email;
                    sdocProcesses.RejectedReasons.Add(rejectedResult);
                }
            }

            return (sdocProcesses, status, message);
        }

        public async Task<(HttpStatusCode status, string message)> ActionProcessAsync(SDocProcessAction actionProcess, SDocHeader sDocHeader)
        {
            HttpClient client = new HttpClient();
            string urlPrefix = AppSetting.Configuration.GetValue("SDoc:UrlPrefixAction");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            Dictionary<string, object> header = GetKeyValuePairs(sDocHeader);

            (dynamic responseData, HttpStatusCode status, string message) = await GetFromAPI<dynamic>(uriBuilder.Uri, HttpMethod.Post, header, actionProcess);
            if (status == HttpStatusCode.OK)
            {
                message = responseData.message;
            }
            return (status, message);
        }

    }
}
