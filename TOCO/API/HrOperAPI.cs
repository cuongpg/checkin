﻿using TOCO.Common;
using TOCO.Implements;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using TOCO.Models;

namespace TOCO.API
{
    public class HrOperAPI : BaseDataApi
    {
        public static HrOperAPI Connection = new HrOperAPI();

        private HrOperAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("HROPER:SystemName");
            tokenSchema = AppSetting.Configuration.GetValue("HROPER:TokenSchema");
            token = AppSetting.Configuration.GetValue("HROPER:AuthToken");
            urlApi = AppSetting.Configuration.GetValue("HROPER:UrlApi");
        }

        public async Task<(string, HttpStatusCode, string)> Order(OrderMechanismInformation input)
        {
            string urlPrefix = AppSetting.Configuration.GetValue("HROPER:UrlOrderPrefix");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (string responseData, HttpStatusCode status, string message) = await GetFromAPI<string>( uriBuilder.Uri, HttpMethod.Post, null, input);
            return (responseData, status, message);
        }

        public async Task<(string, HttpStatusCode, string)> OrderV2(OrderMechanismInformationV2 input)
        {
            string urlPrefix = AppSetting.Configuration.GetValue("HROPER:UrlOrderV2Prefix");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (string responseData, HttpStatusCode status, string message) = await GetFromAPI<string>(uriBuilder.Uri, HttpMethod.Post, null, input);
            return (responseData, status, message);
        }

        //public async Task<(List<HrOperObject> levels, HttpStatusCode status, string message)> GetListByCollumn(int collumnId)
        //{
        //    string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HROPER:UrlCollumnPrefix"), collumnId);
        //    UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
        //    (HrOperAPIResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<HrOperAPIResponse>(uriBuilder.Uri);
        //    return (responseData?.Data, status, message);
        //}

        //public async Task<(List<string> track3s, HttpStatusCode status, string message)> GetListTrack3(string track2Code)
        //{
        //    string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HROPER:UrlTrack3Prefix"), track2Code);
        //    UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
        //    (HrOperTrack3APIResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<HrOperTrack3APIResponse>(uriBuilder.Uri);
        //    return (responseData?.Data, status, message);
        //}

        //public async Task<(HrOperStatement response, HttpStatusCode hrdataStatus, string hrdataMessage)> GetStatementInfo(int employeeCode)
        //{
        //    string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HROPER:UrlStatementPrefix"), employeeCode);
        //    UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
        //    (HrOperStatementAPIResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<HrOperStatementAPIResponse>(uriBuilder.Uri);
        //    return (responseData?.Data, status, message);
        //}
    }
}
