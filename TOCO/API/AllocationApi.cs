﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Common;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using TOCO.Helpers;

namespace TOCO.API
{

    public class AllocationApi : BaseDataApi
    {

        public static AllocationApi Connection = new AllocationApi();

        private AllocationApi() : base()
        {
            systemName = AppSetting.Configuration.GetValue("Allocation:SystemName");
            tokenSchema = AppSetting.Configuration.GetValue("Allocation:TokenSchema");
            urlApi = AppSetting.Configuration.GetValue("Allocation:UrlApi");
            string privateTimeOut = AppSetting.Configuration.GetValue("Allocation:ApiTimeOut");
            if (!string.IsNullOrEmpty(privateTimeOut))
            {
                apiTimeOut = int.Parse(privateTimeOut);
            }
        }


        public async Task<(AllocationStatus, HttpStatusCode, string)> GetAllocationStatus(string employeeWorkEmail)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Allocation:UrlStatusPrefix"), employeeWorkEmail);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (AllocationStatus responseData, HttpStatusCode status, string message) = await GetFromAPI<AllocationStatus>(uriBuilder.Uri);

            return (responseData, status, message);
        }

    }
}
