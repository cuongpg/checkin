﻿using Newtonsoft.Json;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.APIModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System;

namespace TOCO.API
{

    public class OsscarAPI : BaseDataApi
    {
        public static OsscarAPI Connection = new OsscarAPI();

        private OsscarAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("Osscar:SystemName");
            urlApi = AppSetting.Configuration.GetValue("Osscar:UrlApi");
        }

        public async Task<(List<EmployeeInfoResult>, HttpStatusCode, string, int)> SearchEmployee(string search, int page, int perPage, string language)
        {
            int totalPages = 0;
            int skip = 0;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Osscar:UrlPrefixSearch"), search);
            List<EmployeeInfoResult> result = new List<EmployeeInfoResult>();
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (List<EmployeeInfoResult> responseData, HttpStatusCode status, string message) = await GetFromAPI<List<EmployeeInfoResult>>(uriBuilder.Uri);

            if (status == HttpStatusCode.OK && responseData != null)
            {
                totalPages = responseData.Count() / perPage;
                if (responseData.Count() % perPage != 0)
                {
                    totalPages++;
                }
                skip = page * perPage;
                foreach (EmployeeInfoResult employee in responseData.Skip(skip).Take(perPage).ToList())
                {
                    EmployeeInfoResult employeeConvert = STHelper.ConvertObjectByLanguage<EmployeeInfoResult, EmployeeInfoResult>(employee, language);
                    employeeConvert.UrlBase = urlApi;
                    result.Add(employeeConvert);
                }
            }
            return (result, HttpStatusCode.OK, message, totalPages);
        }

        public async Task<(EmployeeDetailResult data, HttpStatusCode status, string message)> EmployeeDetail(int id, string language)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Osscar:UrlPrefixDetail"), id);
            EmployeeDetailResult result = new EmployeeDetailResult();
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (EmployeeDetailResult responseData, HttpStatusCode status, string message) = await GetFromAPI<EmployeeDetailResult>(uriBuilder.Uri);
            EmployeeDetailResult employeeConvert = null;
            if (status == HttpStatusCode.OK && responseData != null)
            {
                employeeConvert = STHelper.ConvertObjectByLanguage<EmployeeDetailResult, EmployeeDetailResult>(responseData, language);
            }
            return (employeeConvert, HttpStatusCode.OK, message);
        }
    }
}
