﻿using Newtonsoft.Json;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Models.APIModels.PaoAPIModels.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TOCO.API
{

    public class PaoDataAPI : BaseDataApi
    {
        public static PaoDataAPI Connection = new PaoDataAPI();

        private PaoDataAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("PAO:SystemName");
            token = AppSetting.Configuration.GetValue("PAO:PaoToken");
            tokenSchema = AppSetting.Configuration.GetValue("PAO:TokenSchema");
            urlApi = AppSetting.Configuration.GetValue("PAO:UrlApi");
        }

        public async Task<(List<PaoProcessesResult>, HttpStatusCode, string, int)> GetListPaoProcesses(string email, string search, int page, int perPage)
        {
            List<PaoProcessesResult> paoProcesses = null;
            int totalPages = 0;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("PAO:UrlPrefixListProcess"), email, search, page, perPage);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (ListPaoProcesses responseData, HttpStatusCode status, string message) = await GetFromAPI<ListPaoProcesses>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK)
            {
                paoProcesses = new List<PaoProcessesResult>();
                foreach (PaoProcess paoProcess in responseData.Processes)
                {
                    PaoProcessesResult paoProcessResult = STHelper.ConvertObject<PaoProcessesResult, PaoProcess>(paoProcess);
                    paoProcessResult.Avatar = paoProcess.Creator.Avatar;
                    paoProcessResult.CreatorName = paoProcess.Creator.CreatorName;
                    paoProcessResult.CreatorEmail = paoProcess.Creator.Email;
                    (HrDataEmployeeAPIResponse Data, HttpStatusCode Status, string Message) employeeInfo = await HrDataApi.Connection.GetEmployeeInfo(paoProcessResult.CreatorEmail);
                    if (employeeInfo.Data.Message == ResultCode.OK && employeeInfo.Data.Data.Count != 0)
                    {
                        paoProcessResult.CreatorMobile = employeeInfo.Data.Data[0].EmployeePhoneNumber;
                    }
                    paoProcesses.Add(paoProcessResult);
                }
                totalPages = responseData.TotalPages;
            }
            return (paoProcesses, status, message, totalPages);
        }

        public async Task<(int count, HttpStatusCode status, string message)> CountPendingProcessesAsync(string email)
        {
            int count = 0;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("PAO:UrlPrefixCountOverviewProcess"), email);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (dynamic responseData, HttpStatusCode status, string message) = await GetFromAPI<dynamic>(uriBuilder.Uri);
            if (status == HttpStatusCode.OK)
            {
                count = responseData.pending_process_num;
            }
            return (count, status, message);
        }

        public async Task<(PaoProcessDetailResult data, HttpStatusCode statusCode, string)> GetPaoProcessDetailAsync(string email, int id)
        {
            PaoProcessDetailResult paoProcesses = null;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("PAO:UrlPrefixDetailProcess"), id, email);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (PaoProcessDetail responseData, HttpStatusCode status, string message) = await GetFromAPI<PaoProcessDetail>(uriBuilder.Uri);

            if (status == HttpStatusCode.OK)
            {
                paoProcesses = new PaoProcessDetailResult
                {
                    ProcessName = responseData.ProcessDocument.ProcessName,
                    Description = responseData.ProcessDocument.Description,
                    Id = responseData.ProcessDocument.Id.ToString(),
                    // Tạo documents
                    Documents = new List<PaoDocumentResult>(),
                    // Tạo activities
                    Activities = new List<PaoActivityResult>(),
                    // Tạo Comments
                    Comments = new List<PaoCommnentResult>(),
                    // Tạo group
                    Groups = new List<PaoReviewGroupResult>(),
                    // Tạo rejected_reasons
                    RejectedReasons = new List<PaoRejectedReasonResult>(),
                    // Đặt mặc định cho isReviewing là true
                    IsReviewing = Constants.FALSE
                };

                // Lấy thông tin documents
                foreach (PaoDocument document in responseData.Documents)
                {
                    PaoDocumentResult docResult = STHelper.ConvertObject<PaoDocumentResult, PaoDocument>(document);
                    paoProcesses.Documents.Add(docResult);
                }

                // Lấy thông tin activities
                // Lấy thông tin document trong activities
                PaoDocument docSearch = responseData.Documents.Find(delegate (PaoDocument doc)
                {
                    return doc.FileId.Equals(responseData.ProcessDocument.MainFileId);
                });

                foreach ((string key, PaoActivity activity) in responseData.Activities)
                {
                    PaoActivityResult actResult = STHelper.ConvertObject<PaoActivityResult, PaoActivity>(activity);
                    actResult.FileName = docSearch?.Name;
                    actResult.DocLink = docSearch?.DocLink;
                    actResult.Email = activity.Email;
                    paoProcesses.Activities.Add(actResult);
                }

                // Lấy thông tin group reviewer
                foreach (PaoReviewer reviewer in responseData.Reviewers)
                {
                    if (reviewer.Email == email && paoProcesses.IsReviewing == Constants.FALSE)
                    {
                        paoProcesses.ApprovalId = reviewer.ApprovalId;
                        paoProcesses.IsReviewing = reviewer.Status == Constants.REVIEWING ? Constants.TRUE : Constants.FALSE;
                    }
                    PaoReviewGroupResult searchGroup = paoProcesses.Groups.Find(delegate (PaoReviewGroupResult rv)
                    {
                        return rv.Id.Equals(reviewer.GroupNum.ToString());
                    });
                    if (searchGroup == null)
                    {
                        searchGroup = new PaoReviewGroupResult()
                        {
                            Id = reviewer.GroupNum.ToString(),
                            Name = reviewer.SmallGroupName,
                            Reviewers = new List<PaoReviewerResult>()
                        };
                        paoProcesses.Groups.Add(searchGroup);
                    }
                    PaoReviewerResult reviewerResult = STHelper.ConvertObject<PaoReviewerResult, PaoReviewer>(reviewer);
                    searchGroup.Reviewers.Add(reviewerResult);
                }

                // Lấy thông tin comment
                foreach (PaoComments comment in responseData.Comments)
                {
                    PaoCommnentResult paoCommentResult = new PaoCommnentResult()
                    {
                        Avatar = comment.Author.Picture.Url,
                        DisplayName = comment.Author.DisplayName,
                        CreatedDate = comment.CreatedDate,
                        Comment = comment.Content,
                        Deleted = comment.Deleted.ToString(),
                        Replies = new List<PaoReplyResult>()
                    };
                    foreach (PaoReply reply in comment.Replies)
                    {
                        paoCommentResult.Replies.Add(new PaoReplyResult()
                        {
                            Avatar = reply.Author.Picture.Url,
                            DisplayName = reply.Author.DisplayName,
                            Deleted = reply.Deleted.ToString(),
                            CreatedDate = reply.CreatedDate,
                            Comment = reply.Content
                        });
                    }
                    paoProcesses.Comments.Add(paoCommentResult);
                }

                // Lấy thông tin rejected_reasons
                foreach ((string key, PaoRejectedReason value) in responseData.RejectedReasons)
                {
                    PaoRejectedReasonResult rejectedResult = STHelper.ConvertObject<PaoRejectedReasonResult, PaoRejectedReason>(value);
                    rejectedResult.Email = key;
                    paoProcesses.RejectedReasons.Add(rejectedResult);
                }
            }

            return (paoProcesses, status, message);
        }

        public async Task<(HttpStatusCode status, string message)> RejectProcessAsync(string email, string approvalId, string reason)
        {
            PaoProcessAction actionProcess = new PaoProcessReject()
            {
                Email = email,
                ApprovalId = approvalId,
                Reason = reason
            };
            return await ActionProcessAsync(actionProcess);
        }

        public async Task<(HttpStatusCode status, string message)> ApprovalProcessAsync(string email, string approvalId)
        {
            PaoProcessAction actionProcess = new PaoProcessApproval()
            {
                Email = email,
                ApprovalId = approvalId
            };
            return await ActionProcessAsync(actionProcess);
        }

        private async Task<(HttpStatusCode status, string message)> ActionProcessAsync(PaoProcessAction actionProcess)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenSchema, token);
            HttpStatusCode status = HttpStatusCode.OK;
            string message = string.Empty;
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Method = HttpMethod.Get;
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("PAO:UrlPrefixAction"));
            if (actionProcess is PaoProcessApproval)
            {
                httpRequestMessage.RequestUri = new Uri(urlApi + urlPrefix + "?process=" + actionProcess.Process + "&email=" + actionProcess.Email + "&approval_id=" + actionProcess.ApprovalId);
            }
            else
            {
                httpRequestMessage.RequestUri = new Uri(urlApi + urlPrefix + "?process=" + actionProcess.Process + "&email=" + actionProcess.Email + "&approval_id=" + actionProcess.ApprovalId + "&reason=" + (actionProcess as PaoProcessReject).Reason);
            }

            //string json = JsonConvert.SerializeObject(actionProcess);

            //HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            //httpRequestMessage.Content = content;

            int timeout = int.Parse(AppSetting.Configuration.GetValue("APITimeout"));
            CancellationTokenSource cts = new CancellationTokenSource(timeout);
            try
            {
                HttpResponseMessage actionResponse = await client.SendAsync(httpRequestMessage);
                status = actionResponse.StatusCode;
                string data = await actionResponse.Content.ReadAsStringAsync();
                dynamic result = JsonConvert.DeserializeObject(data);
                actionResponse.Dispose();
            }
            catch (Exception ex)
            {
                status = HttpStatusCode.InternalServerError;
                message = ex.Message;
            }
            finally
            {
                cts.Dispose();
                httpRequestMessage.Dispose();
                //content.Dispose();
                client.Dispose();
            }
            return (status, message);
        }
    }

}
