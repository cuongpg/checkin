﻿using TOCO.Common;
using TOCO.Implements;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TOCO.API
{
    public class ListmasterAPI : BaseDataApi
    {
        public static ListmasterAPI Connection = new ListmasterAPI();

        private ListmasterAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("Listmaster:SystemName");
            tokenSchema = AppSetting.Configuration.GetValue("Listmaster:TokenSchema");
            token = AppSetting.Configuration.GetValue("Listmaster:AuthToken");
            urlApi = AppSetting.Configuration.GetValue("Listmaster:UrlApi");
        }

        public async Task<(ListmasterMSTResult, HttpStatusCode, string)> GetListEstimateOwners(int perPage, int page)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Listmaster:UrlEstimateOwnerPrefix"), perPage, page);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (ListmasterMSTResult responseData, HttpStatusCode status, string message) = await GetFromAPI<ListmasterMSTResult>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(ListmasterCorporationResult, HttpStatusCode, string)> GetListCorporations(int perPage, int page)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Listmaster:UrlCorporationPrefix"), perPage, page);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (ListmasterCorporationResult responseData, HttpStatusCode status, string message) = await GetFromAPI<ListmasterCorporationResult>(uriBuilder.Uri);
            return (responseData, status, message);
        }

        public async Task<(ListmasterCommonResult, HttpStatusCode, string)> GetListCommons()
        {
            string urlPrefix = AppSetting.Configuration.GetValue("Listmaster:UrlCommonListPrefix");
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (ListmasterCommonResult responseData, HttpStatusCode status, string message) = await GetFromAPI<ListmasterCommonResult>(uriBuilder.Uri);
            return (responseData, status, message);
        }
    }
}
