﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Common;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using TOCO.Helpers;

namespace TOCO.API
{
    class UserHrData
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class HrDataApi : BaseDataApi
    {

        public static HrDataApi Connection = new HrDataApi();

        private HrDataApi() : base()
        {
            systemName = AppSetting.Configuration.GetValue("HrData:SystemName");
            tokenSchema = AppSetting.Configuration.GetValue("HrData:TokenSchema");
            urlApi = AppSetting.Configuration.GetValue("HrData:Url");
        }

        private async Task<(HttpStatusCode statusCode, string code)> UpdateToken()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent", "Toco App");
            HttpStatusCode status = HttpStatusCode.OK;
            string resultCode = string.Empty;
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post
            };
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HrData:UrlPrefixAuth"));
            httpRequestMessage.RequestUri = new Uri(urlApi + urlPrefix);

            UserHrData user = new UserHrData
            {
                Email = AppSetting.Configuration.GetValue("HrData:UserName"),
                Password = AppSetting.Configuration.GetValue("HrData:Password")
            };
            string json = JsonConvert.SerializeObject(user);

            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            httpRequestMessage.Content = content;

            int timeout = int.Parse(AppSetting.Configuration.GetValue("APITimeout"));
            CancellationTokenSource cts = new CancellationTokenSource(timeout);
            HttpResponseMessage oauthResponse = await client.SendAsync(httpRequestMessage);

            if (oauthResponse.StatusCode == HttpStatusCode.OK)
            {
                string data = await oauthResponse.Content.ReadAsStringAsync();
                dynamic result = JsonConvert.DeserializeObject(data);
                if (result.message == ResultCode.OK)
                {
                    token = result.data;
                    status = HttpStatusCode.OK;
                }
                else
                {
                    status = HttpStatusCode.Forbidden;
                }
                resultCode = result.message;
            }
            else
            {
                status = oauthResponse.StatusCode;
                resultCode = ResultCode.ERR_UPDATE_TOKEN_HRDATA;
            }

            oauthResponse.Dispose();

            cts.Dispose();
            httpRequestMessage.Dispose();
            content.Dispose();
            client.Dispose();

            return (status, resultCode);
        }

        public async Task<(HrDataEmployeeAPIResponse, HttpStatusCode, string)> GetEmployeeInfo(string employeeWorkEmail)
        {
            if (string.IsNullOrEmpty(token) || !STHelper.CheckEffectiveToken(token))
            {
                (HttpStatusCode statusUpdateToken, string messageUpdateToken) = await UpdateToken();
                if (statusUpdateToken != HttpStatusCode.OK)
                {
                    return (null, statusUpdateToken, messageUpdateToken);
                }
            }
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HrData:UrlPrefixEmployeeInfo"), employeeWorkEmail);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (HrDataEmployeeAPIResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<HrDataEmployeeAPIResponse>(uriBuilder.Uri);

            return (responseData, status, message);
        }

        public async Task<(string EmployeeCode, HttpStatusCode Status, string message)> GetEmployeeCodeByEmail(string employeeWorkEmail)
        {
            string employeeCode = string.Empty;
            if (string.IsNullOrEmpty(token) || STHelper.CheckEffectiveToken(token))
            {
                (HttpStatusCode statusUpdateToken, string messageUpdateToken) = await UpdateToken();
                if (statusUpdateToken != HttpStatusCode.OK)
                {
                    return (null, statusUpdateToken, messageUpdateToken);
                }
            }
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("HrData:UrlPrefixEmployeeCode"), employeeWorkEmail);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (HrDataEmployeeCodeAPIResponse responseData, HttpStatusCode status, string message) = await GetFromAPI<HrDataEmployeeCodeAPIResponse>(uriBuilder.Uri);
            if (responseData.Message == ResultCode.OK)
            {
                employeeCode = responseData.Data.ToString();
            }
            return (employeeCode, status, message);
        }
    }
}
