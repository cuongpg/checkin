﻿using TOCO.Common;
using TOCO.Implements;
using TOCO.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TOCO.API
{
    public class ZohoDataAPI : BaseDataApi
    {
        public static ZohoDataAPI Connection = new ZohoDataAPI();
        private readonly string authToken = string.Empty;

        private ZohoDataAPI() : base()
        {
            systemName = AppSetting.Configuration.GetValue("Zoho:SystemName");
            authToken = AppSetting.Configuration.GetValue("Zoho:AuthToken");
            urlApi = AppSetting.Configuration.GetValue("Zoho:UrlApi");
        }

        public async Task<(List<ZohoDataContracts>, HttpStatusCode, string)> GetListContract(string employee_code)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Zoho:UrlPrefixListContracts"), authToken, employee_code);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (ResponseZohoDataContracts responseData, HttpStatusCode status, string messageZoho) = await GetFromAPI<ResponseZohoDataContracts>(uriBuilder.Uri);
            return (responseData.Data_HD_new, status, messageZoho);
        }

        public async Task<(ZohoDataContracts, HttpStatusCode, string)> GetContractsById(string employee_code, string id)
        {
            string urlPrefix = string.Format(AppSetting.Configuration.GetValue("Zoho:UrlPrefixDetailContracts"), authToken, id);
            UriBuilder uriBuilder = new UriBuilder(new Uri(urlApi + urlPrefix));
            (ResponseZohoDataContracts response, HttpStatusCode status, string messageZoho) =
                await GetFromAPI<ResponseZohoDataContracts>(uriBuilder.Uri);
            return response.Data_HD_new[0].Ma_nhan_vien == employee_code ? (response.Data_HD_new[0], status, messageZoho) : (null, HttpStatusCode.Forbidden, ResultCode.ERR_FORBIDDEN_CONTRACT);
        }
    }
}
