﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using TOCO.API;
using TOCO.Common;
using TOCO.Filters;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Models.RequestModels;

namespace TOCO.Controllers
{

    [Authorize]
    [Produces("application/json")]
    [Route("api/ApprovalRecord")]
    public class ApprovalRecordController : BaseController
    {
        private readonly IConfiguration configuration;


        private readonly IHttpContextAccessor accessor;
        private readonly IDistributedCache cache;
        private readonly ApprovalRecordManager approvalRecordManager;
        private readonly UserManager userManager;
        private readonly ScriptManager scriptManager;
        private readonly ApplicationDbContext context;
        private readonly NotificationManager notificationManager;
        private readonly EmailManager emailManager;

        public ApprovalRecordController(ApplicationDbContext context, IConfiguration configuration,
            IHttpContextAccessor accessor, IDistributedCache cache)
        {
            this.context = context;
            this.configuration = configuration;
            this.accessor = accessor;
            this.cache = cache;
            approvalRecordManager = new ApprovalRecordManager(context);
            userManager = new UserManager(context);
            scriptManager = new ScriptManager(context);
            notificationManager = new NotificationManager(context);
            emailManager = new EmailManager(context);
        }

        // GET: api/<controller>/GetApprovalRecordsForManager
        [HttpGet]
        [Route("GetApprovalRecordsForManager")]
        public IActionResult GetApprovalRecordsForManager(STRequestModel request, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, true, false, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/AdminGetApprovalRecordsForManager
        [HttpGet]
        [Route("AdminGetApprovalRecordsForManager")]
        public IActionResult AdminGetApprovalRecordsForManager(STRequestModel request, [FromQuery] int employeeCode, [FromQuery] string language = "vi")
        {
            User user = userManager.GetUserById(employeeCode);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, true, false, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetDestroyApprovalRecordsForManager
        [HttpGet]
        [Route("GetDestroyApprovalRecordsForManager")]
        public IActionResult GetDestroyApprovalRecordsForManager(STRequestModel request, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, true, true, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetMyApprovalRecords
        [HttpGet]
        [Route("GetMyApprovalRecords")]
        public IActionResult GetMyApprovalRecords(STRequestModel request, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, false, false, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }



        // GET: api/<controller>/GetApprovalRecordsForAdmin
        [HttpGet]
        [Route("GetApprovalRecordsForAdmin")]
        public IActionResult GetApprovalRecordsForAdmin(STRequestModel request, [FromQuery] int employeeCode, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserById(employeeCode);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, false, false, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetMyDestroyApprovalRecords
        [HttpGet]
        [Route("GetMyDestroyApprovalRecords")]
        public IActionResult GetMyDestroyApprovalRecords(STRequestModel request, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, false, true, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetDestroyApprovalRecordsForAdmin
        [HttpGet]
        [Route("GetDestroyApprovalRecordsForAdmin")]
        public IActionResult GetDestroyApprovalRecordsForAdmin(STRequestModel request, [FromQuery] int employeeCode, [FromQuery] string language = "vi")
        {
            User user = userManager.GetUserById(employeeCode);
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecords(out int numberOfPages, out int totalRecords, request, user, true, true, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/DestroyApprovalRecord
        [HttpPost]
        [Route("DestroyApprovalRecord")]
        public IActionResult DestroyApprovalRecord([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message) = approvalRecordManager.DestroyApprovalRecord(request.Ids, user);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/ApprovalApprovalRecord
        [HttpPost]
        [Route("ApprovalApprovalRecord")]
        public IActionResult ApprovalApprovalRecord([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(false, StatusApprovalRecord.Approved, request.Ids, user, false);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            string leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC006,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.APPROVAL
                                });
                            // Gửi notify phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON006,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.APPROVAL
                               },
                               record.User.Nationality);
                        }
                        // Nếu approval đơn nghỉ phép nhân sự
                        foreach (ApprovalRecord record in records)
                        {
                            // Xử lý kiểm tra có đơn đk hủy nào conflict thời gian
                            // Xử lý approval cho các đơn hủy đó
                            IEnumerable<ApprovalRecord> destroyRecords = approvalRecordManager.ApprovalDestroyConflict(record.IsManyDate, record.User, record.StartTime, record.EndTime, record.IsAfternoon, record.IsMorning);
                            foreach (ApprovalRecord destroyRecord in destroyRecords)
                            {
                                var leaveTime = approvalRecordManager.GetLeaveTime(destroyRecord, destroyRecord.User.Nationality);
                                // Gửi mail phản hồi đơn xin hủy nghỉ phép cho nhân sự
                                emailManager.PushEmail(
                                    destroyRecord.User.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC008,
                                    new string[]
                                    {
                                        destroyRecord.User.FullName,
                                        leaveTime,
                                        Constants.APPROVAL
                                    });
                                // Gửi notify phản hồi đơn xin hủy nghỉ phép cho nhân sự
                                string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", destroyRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM), destroyRecord.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : destroyRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                                notificationManager.PushNotification(
                                    destroyRecord.User.Email,
                                    null,
                                    destroyRecord.User.ImageUrl,
                                    NotificationTemplateCode.ON008,
                                    new string[]
                                    {
                                        notifyLeaveTime,
                                        Constants.APPROVAL
                                    },
                                    destroyRecord.User.Nationality);
                            }
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/ApprovalDestroyApprovalRecord
        [HttpPost]
        [Route("ApprovalDestroyApprovalRecord")]
        public IActionResult ApprovalDestroyApprovalRecord([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(false, StatusApprovalRecord.Approved, request.Ids, user, true);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC008,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.APPROVAL
                                });
                            // Gửi notify phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                                record.User.Email,
                                null,
                                record.User.ImageUrl,
                                NotificationTemplateCode.ON008,
                                new string[]
                                {
                                    notifyLeaveTime,
                                    Constants.APPROVAL
                                },
                                record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/RejectApprovalRecord
        [HttpPost]
        [Route("RejectApprovalRecord")]
        public IActionResult RejectApprovalRecord([FromBody]ConfirmRequestModel request, [FromQuery]string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(false, StatusApprovalRecord.Rejected, request.Ids, user, false, rejectReason);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC006,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.REJECT
                                });
                            // Gửi notify phản hồi đơn xin nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON006,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.REJECT
                               },
                               record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/RejectDestroyApprovalRecord
        [HttpPost]
        [Route("RejectDestroyApprovalRecord")]
        public IActionResult RejectDestroyApprovalRecord([FromBody]ConfirmRequestModel request, [FromQuery]string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(false, StatusApprovalRecord.Rejected, request.Ids, user, true, rejectReason);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            //Send Mail
                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC008,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.REJECT
                                });
                            // Gửi notification
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON008,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.REJECT
                               },
                               record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AdminApproveLeaveRequest
        [HttpPost]
        [Route("AdminApproveLeaveRequest")]
        public IActionResult AdminApproveLeaveRequest([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(true, StatusApprovalRecord.Approved, request.Ids, isForManager ? user : manager, false);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            //Thêm mail admin duyệt đơn
                            record.ApproverEmail = email;
                            approvalRecordManager.Update(record);

                            string leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC006,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.APPROVAL
                                });
                            // Gửi notify phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON006,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.APPROVAL
                               },
                               record.User.Nationality);
                        }
                        // Nếu approval đơn nghỉ phép nhân sự
                        foreach (ApprovalRecord record in records)
                        {
                            // Xử lý kiểm tra có đơn đk hủy nào conflict thời gian
                            // Xử lý approval cho các đơn hủy đó
                            IEnumerable<ApprovalRecord> destroyRecords = approvalRecordManager.ApprovalDestroyConflict(record.IsManyDate, record.User, record.StartTime, record.EndTime, record.IsAfternoon, record.IsMorning);
                            foreach (ApprovalRecord destroyRecord in destroyRecords)
                            {
                                var leaveTime = approvalRecordManager.GetLeaveTime(destroyRecord, destroyRecord.User.Nationality);
                                // Gửi mail phản hồi đơn xin hủy nghỉ phép cho nhân sự
                                emailManager.PushEmail(
                                    destroyRecord.User.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC008,
                                    new string[]
                                    {
                                        destroyRecord.User.FullName,
                                        leaveTime,
                                        Constants.APPROVAL
                                    });
                                // Gửi notify phản hồi đơn xin hủy nghỉ phép cho nhân sự
                                string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", destroyRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM), destroyRecord.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : destroyRecord.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                                notificationManager.PushNotification(
                                    destroyRecord.User.Email,
                                    null,
                                    destroyRecord.User.ImageUrl,
                                    NotificationTemplateCode.ON008,
                                    new string[]
                                    {
                                        notifyLeaveTime,
                                        Constants.APPROVAL
                                    },
                                    destroyRecord.User.Nationality);
                            }
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AdminApproveDestroyLeaveRequest
        [HttpPost]
        [Route("AdminApproveDestroyLeaveRequest")]
        public IActionResult AdminApproveDestroyLeaveRequest([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(true, StatusApprovalRecord.Approved, request.Ids, isForManager ? user : manager, true);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            //Thêm mail admin duyệt đơn
                            record.ApproverEmail = email;
                            approvalRecordManager.Update(record);

                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC008,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.APPROVAL
                                });
                            // Gửi notify phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                                record.User.Email,
                                null,
                                record.User.ImageUrl,
                                NotificationTemplateCode.ON008,
                                new string[]
                                {
                                    notifyLeaveTime,
                                    Constants.APPROVAL
                                },
                                record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AdminRejectLeaveRequest
        [HttpPost]
        [Route("AdminRejectLeaveRequest")]
        public IActionResult AdminRejectLeaveRequest([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager, [FromQuery]string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(true, StatusApprovalRecord.Rejected, request.Ids, isForManager ? user : manager, false, rejectReason);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            //Thêm mail admin duyệt đơn
                            record.ApproverEmail = email;
                            approvalRecordManager.Update(record);

                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC006,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.REJECT
                                });
                            // Gửi notify phản hồi đơn xin nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON006,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.REJECT
                               },
                               record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AdminRejectDestroyLeaveRequest
        [HttpPost]
        [Route("AdminRejectDestroyLeaveRequest")]
        public IActionResult AdminRejectDestroyLeaveRequest([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager, [FromQuery]string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmApprovalReacord(true, StatusApprovalRecord.Rejected, request.Ids, isForManager ? user : manager, true, rejectReason);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            //Thêm mail admin duyệt đơn
                            record.ApproverEmail = email;
                            approvalRecordManager.Update(record);

                            //Send Mail
                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin hủy nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC008,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.REJECT
                                });
                            // Gửi notification
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON008,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.REJECT
                               },
                               record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
        // GET: api/<controller>/GetApprovalRecordValidCode

        [HttpPost]
        [Route("GetApprovalRecordValidCode")]
        public IActionResult GetApprovalRecordValidCode([FromBody]ApprovalRecordRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            return GetApprovalRecordValidCode(email, false, request);
        }

        [HttpPost]
        [Route("AdminGetApprovalRecordValidCode")]
        public IActionResult AdminGetApprovalRecordValidCode([FromQuery] string email, [FromBody]ApprovalRecordRequestModel request)
        {
            return GetApprovalRecordValidCode(email, true, request);
        }

        private IActionResult GetApprovalRecordValidCode(string email, bool isAdmin, ApprovalRecordRequestModel request)
        {
            // Convert Date
            DateTime startDate = STHelper.ConvertDateTimeToLocal(request.StartTime).Value.Date;
            DateTime endDate = STHelper.ConvertDateTimeToLocal(request.EndTime).Value.Date;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptByUserAndTime(user.Id, startDate, endDate).FirstOrDefault();
            if (script == null)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            (bool bOk, string[] statusAllowSkips, string message) = approvalRecordManager.ValidateApprovalRecord(user, script, request.IsManyDate,
                startDate, endDate, request.IsMorning, request.IsAfternoon, request.Note, request.ReasonTypeId, isAdmin);
            if (!bOk)
            {
                return LogicError(message);
            }
            return Ok(statusAllowSkips);
        }

        // GET: api/<controller>/CalculationDate
        [HttpPost]
        [Route("CalculationDate")]
        public IActionResult CalculationDate([FromBody]ApprovalRecordRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            return CalculationDate(email, request);
        }

        // GET: api/<controller>/AdminCalculationDate
        [HttpPost]
        [Route("AdminCalculationDate")]
        public IActionResult AdminCalculationDate([FromQuery] string email, [FromBody]ApprovalRecordRequestModel request)
        {
            return CalculationDate(email, request);
        }

        private IActionResult CalculationDate(string email, ApprovalRecordRequestModel request)
        {
            // Convert Date
            DateTime startTime = STHelper.ConvertDateTimeToLocal(request.StartTime).Value.Date;
            DateTime endTime = STHelper.ConvertDateTimeToLocal(request.EndTime).Value.Date;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptByUserAndTime(user.Id, startTime, endTime).FirstOrDefault();
        
            if (script == null)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            int numberHoliday = 0;
            int numberWeekend = 0;
            double numberLeaveDate = 0;
            (numberLeaveDate, numberHoliday, numberWeekend) = approvalRecordManager.GetLeaveDate(user.Id, script.HasHoliday, script.HasLeaveWeekend, request.IsManyDate, request.IsMorning, request.IsAfternoon, startTime, endTime, out List<DateTime> listLeaveDate);
            return Ok(new
            {
                numberHoliday,
                numberWeekend,
                numberLeaveDate
            });
        }

        // GET: api/<controller>/ApprovalRecord
        [HttpPost]
        [Route("ApprovalRecord")]
        public IActionResult ApprovalRecordAsync([FromBody]ApprovalRecordRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            return CreatedApprovalRecord(false, email, request);
        }

        // GET: api/<controller>/AdminCreateApprovalRecord
        [HttpPost]
        [Route("AdminCreateApprovalRecord")]
        public IActionResult AdminCreateApprovalRecord([FromQuery] string email, [FromBody]ApprovalRecordRequestModel request)
        {
            return CreatedApprovalRecord(true, email, request);
        }

        private IActionResult CreatedApprovalRecord(bool isAdmin, string email, ApprovalRecordRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    // Convert Date
                    DateTime startDate = STHelper.ConvertDateTimeToLocal(request.StartTime).Value.Date;
                    DateTime endDate = STHelper.ConvertDateTimeToLocal(request.EndTime).Value.Date;
                    User user = userManager.GetUserByEmail(email);

                    Script script = scriptManager.GetScriptByUserAndTime(user.Id, startDate, endDate).FirstOrDefault();
                    if (script == null)
                    {
                        return NotFound(ResultCode.NOT_FOUND_SCRIPT);
                    }
                    (bool bOk, ApprovalRecord record) = approvalRecordManager.ApprovalRecord(user, script, request.IsManyDate, startDate, endDate,
                        request.IsMorning, request.IsAfternoon, request.Note, request.ReasonTypeId, isAdmin);
                    if (bOk)
                    {
                        if (!isAdmin)
                        {
                            List<User> userToList = new List<User>();

                            if (record.Status != StatusApprovalRecord.AdminPending)
                            {
                                userToList.Add(userManager.GetUserById(user.ManagerId));
                            }
                            else
                            {
                                userToList.AddRange(userManager.GetUserByGroup(Group.AdminGroupId));
                            }

                            string notifyLeaveTime = record.IsManyDate ?
                                   string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                   : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            foreach (User userTo in userToList)
                            {
                                // Gửi mail đơn xin nghỉ phép cho quản lý hoặc admin
                                emailManager.PushEmail(
                                    userTo.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC007,
                                    new string[]
                                    {
                                        userTo.FullName,
                                        user.FullName,
                                        string.Format(MessageManager.GetMessageManager(userTo.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(userTo.Nationality).GetValue(script.Description)),
                                        approvalRecordManager.GetLeaveTime(record, userTo.Nationality),
                                        record.ReasonType.Name,
                                        record.Reason,
                                        "https://checkin.topica.vn/#/approve"
                                    });
                                // Gửi notify đơn xin nghỉ phép cho quản lý hoặc admin
                                notificationManager.PushNotification(
                                    userTo.Email,
                                    null,
                                    user.ImageUrl,
                                    NotificationTemplateCode.ON007,
                                    new string[]
                                    {
                                        notifyLeaveTime,
                                        user.FullName
                                    }, userTo.Nationality);
                            }

                        }

                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_APPROVAL_RECORD);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }


        // GET: api/<controller>/GetApprovalRecordsNeedConfirmByAdmin
        [HttpGet]
        [Route("GetApprovalRecordsNeedConfirmByAdmin")]
        public IActionResult GetApprovalRecordsNeedConfirmByAdmin(STRequestModel request, [FromQuery] string language = "vi")
        {
            List<ApprovalRecordResult> approvals = approvalRecordManager.GetApprovalRecordsNeedConfirmByAdmin(out int numberOfPages, out int totalRecords, request, language);
            return OkPagination(approvals, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/ApproveRecordNeedConfirmByAdmin
        [HttpPost]
        [Route("ApproveRecordNeedConfirmByAdmin")]
        public IActionResult ApproveRecordNeedConfirmByAdmin([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmRecordByAdmin(StatusApprovalRecord.Pending, request.Ids, email);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            User user = userManager.GetUserById(record.UserId);
                            Script script = scriptManager.GetScriptByUserAndTime(user.Id, record.StartTime, record.EndTime).FirstOrDefault();
                            if (script == null)
                            {
                                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
                            }
                            User manager = userManager.GetUserById(user.ManagerId);
                            // Gửi mail đơn xin nghỉ phép cho quản lý
                            emailManager.PushEmail(
                                manager.Email,
                                null,
                                null,
                                EmailTemplateCode.OC007,
                                new string[]
                                {
                                    manager.FullName,
                                    user.FullName,
                                    string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                    approvalRecordManager.GetLeaveTime(record, manager.Nationality),
                                    record.ReasonType.Name,
                                    record.Reason,
                                    "https://checkin.topica.vn/#/approve"
                                });
                            // Gửi notify đơn xin nghỉ phép cho quản lý
                            string notifyLeaveTime = record.IsManyDate ?
                                string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                                manager.Email,
                                null,
                                manager.ImageUrl,
                                NotificationTemplateCode.ON007,
                                new string[]
                                {
                                    notifyLeaveTime,
                                    manager.FullName
                                }, manager.Nationality);
                        }

                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/RejectRecordNeedConfirmByAdmin
        [HttpPost]
        [Route("RejectRecordNeedConfirmByAdmin")]
        public IActionResult RejectRecordNeedConfirmByAdmin([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    (bool bOk, string message, ApprovalRecord[] records) = approvalRecordManager.ConfirmRecordByAdmin(StatusApprovalRecord.Rejected, request.Ids, email);
                    if (bOk)
                    {
                        foreach (ApprovalRecord record in records)
                        {
                            var leaveTime = approvalRecordManager.GetLeaveTime(record, record.User.Nationality);
                            // Gửi mail phản hồi đơn xin nghỉ phép cho nhân sự
                            emailManager.PushEmail(
                                record.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC006,
                                new string[]
                                {
                                    record.User.FullName,
                                    leaveTime,
                                    Constants.REJECT
                                });
                            // Gửi notify phản hồi đơn xin nghỉ phép cho nhân sự
                            string notifyLeaveTime = record.IsManyDate ?
                                     string.Format("{0} - {1}", record.StartTime.ToString(Constants.FORMAT_DATE_DDMM), record.EndTime.ToString(Constants.FORMAT_DATE_DDMM))
                                     : record.StartTime.ToString(Constants.FORMAT_DATE_DDMM);
                            notificationManager.PushNotification(
                               record.User.Email,
                               null,
                               record.User.ImageUrl,
                               NotificationTemplateCode.ON006,
                               new string[]
                               {
                                    notifyLeaveTime,
                                    Constants.REJECT
                               },
                               record.User.Nationality);
                        }
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)) : LogicError(message);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

    }
}