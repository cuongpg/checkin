﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using TOCO.API;
using TOCO.Models;
using TOCO.Models.APIModels.SDocAPIModels;
using TOCO.Models.APIModels.SDocAPIModels.InputModels;

namespace TOCO.Controllers
{
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("api/SDoc")]
    public class SDocController : BaseController
    {
        private readonly IConfiguration configuration;

        public SDocController(ApplicationDbContext context, IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("CountPendingRecords")]
        public async Task<IActionResult> CountPendingProcessesAsync()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };
            (int count, HttpStatusCode status, string message) = await SDocAPI.Connection.CountPendingProcessesAsync(header);
            return Ok(new
            {
                count
            });
        }

        [HttpGet]
        [Route("FollowList")]
        public async Task<IActionResult> GetListFollowSDocProcesses([FromQuery]string search, [FromQuery] string entities, [FromQuery] string kindOfDocs, [FromQuery]int page = 0, [FromQuery]int perPage = 10)
        {
            string urlPrefix = configuration["SDoc:UrlPrefixListFollowProcess"];
            return await GetListSDocProcesses(urlPrefix, entities, kindOfDocs, search, page, perPage);
        }

        [HttpGet]
        [Route("ApproveList")]
        public async Task<IActionResult> GetListApproveSDocProcesses([FromQuery]string search, [FromQuery] string entities, [FromQuery] string kindOfDocs, [FromQuery]int page = 0, [FromQuery]int perPage = 10)
        {
            string urlPrefix = configuration["SDoc:UrlPrefixListApproveProcess"];
            return await GetListSDocProcesses(urlPrefix, entities, kindOfDocs, search, page, perPage);
        }

        [HttpGet]
        [Route("SearchList")]
        public async Task<IActionResult> GetListSearchSDocProcesses([FromQuery]string search, [FromQuery] string entities, [FromQuery] string kindOfDocs, [FromQuery]int page = 0, [FromQuery]int perPage = 10)
        {
            string urlPrefix = configuration["SDoc:UrlPrefixListSearchProcess"];
            return await GetListSDocProcesses(urlPrefix, entities, kindOfDocs, search, page, perPage);
        }

        private async Task<IActionResult> GetListSDocProcesses(string urlPrefix, string entities, string kindOfDocs, string search, int page, int perPage)
        {
            page++;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            SDocQuery query = new SDocQuery
            {
                Page = page.ToString(),
                PerPage = perPage.ToString(),
                Search = string.IsNullOrEmpty(search) ? null : search,
                Entity = entities,
                Type = kindOfDocs
            };
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };
            (List<SDocProcessesResult> data, HttpStatusCode status, string message, int totalPages) = await SDocAPI.Connection.GetListSDocProcesses(urlPrefix, header, query);
            page--;
            return status == HttpStatusCode.OK || status == HttpStatusCode.NotFound ? OkPagination(data, page, perPage, totalPages) : StatusCode((int)status, null);
        }

        [HttpGet]
        [Route("Entities")]
        public async Task<IActionResult> GetEntities()
        {
            (List<SDocEntityModel> data, HttpStatusCode status, string message) = await SDocAPI.Connection.GetEntitiesAsync();
            return Ok(data);
        }

        [HttpGet]
        [Route("KindOfDocs")]
        public async Task<IActionResult> GetKindOfDocs()
        {
            (List<SDocKindOfDocModel> data, HttpStatusCode status, string message) = await SDocAPI.Connection.GetKindOfDocAsync();
            return Ok(data);
        }

        [HttpGet]
        [Route("Detail/{id}")]
        public async Task<IActionResult> GetSDocProcessesInfo(int id)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            SDocQuery query = new SDocQuery
            {
                Populate = true.ToString(),
                IncludeComments = true.ToString()
            };
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };

            (SDocProcessDetailResult data, HttpStatusCode status, string message) = await SDocAPI.Connection.GetPaoProcessDetailAsync(id, header, query);
            return status == HttpStatusCode.OK || status == HttpStatusCode.NotFound ? Ok(data) : StatusCode((int)status, null);
        }

        [HttpPost]
        [Route("Approval")]
        public async Task<IActionResult> ApprovalProcessAsync([FromBody]ApprovalRequestModel requestBody)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };
            SDocProcessAction actionProcess = new SDocProcessAction()
            {
                ApprovalId = requestBody.ApprovalId,
                Status = SDocProcessAction.APPROVAL
            };
            (HttpStatusCode status, string message) = await SDocAPI.Connection.ActionProcessAsync(actionProcess, header);
            return StatusCode((int)status, message);
        }

        [HttpPost]
        [Route("Reject")]
        public async Task<IActionResult> RejectProcessAsync([FromBody]RejectRequestModel requestBody)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            if (string.IsNullOrEmpty(requestBody.Reason))
            {
                return BadRequest();
            }
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };
            SDocProcessAction actionProcess = new SDocProcessAction()
            {
                ApprovalId = requestBody.ApprovalId,
                Reason = requestBody.Reason,
                Status = SDocProcessAction.REJECT
            };
            (HttpStatusCode status, string message) = await SDocAPI.Connection.ActionProcessAsync(actionProcess, header);
            return StatusCode((int)status, message);
        }

        [HttpGet]
        [Route("GetRisk/{processId}")]
        public async Task<object> GetRisk(int processId)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };
           
            (object responseData, HttpStatusCode status, string message) = await SDocAPI.Connection.GetRisk(header, processId);
            return responseData;
        }

        [HttpPost]
        [Route("SubmitRisk/{processId}")]
        public async Task<object> SubmitRisk([FromBody]object data, int processId)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            SDocHeader header = new SDocHeader
            {
                XUserMail = email
            };

            (object responseData, HttpStatusCode status, string message) = await SDocAPI.Connection.SubmitRisk(header, processId, data);
            return responseData;
        }
    }
}