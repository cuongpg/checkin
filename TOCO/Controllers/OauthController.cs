﻿using Microsoft.AspNetCore.Mvc;
using TOCO.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System.Linq;
using TOCO.Implements;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using TOCO.API;
using TOCO.Models.APIModels;
using System.Resources;
using TOCO.Common;
using System.Net;
using System.Text.RegularExpressions;
using TOCO.Logic;
using TOCO.Helpers;

namespace TOCO.Controllers
{
    [Authorize]
    [Route("api/oauth")]
    public class OauthController : BaseController
    {
        private readonly IConfiguration configuration;
        private readonly UserManager userManager;
        private readonly HrbHistoryService hrbHistoryService;
        private readonly IHttpContextAccessor accessor;
        private readonly IDistributedCache cache;

        public OauthController(ApplicationDbContext context, IConfiguration configuration,
            IHttpContextAccessor accessor, IDistributedCache cache)
        {
            userManager = new UserManager(context);
            hrbHistoryService = new HrbHistoryService(context);
            this.configuration = configuration;
            this.accessor = accessor;
            this.cache = cache;
        }

        // POST api/oauth/token
        [AllowAnonymous]
        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> Token([FromBody]GoogleInfoModel request, [FromQuery] string language = "vi")
        {
            string email = string.Empty;
            if (request.IdToken != null)
            {
                HttpResponseMessage response = await STHelper.VerifiedIdToken(request.IdToken);
                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content.ReadAsStringAsync().Result;
                    GoogleInfoModel tokenInfo = STHelper.GetTokenInfo(json);
                    if (string.IsNullOrEmpty(tokenInfo.picture))
                    {
                        tokenInfo.picture = string.Empty;
                    }
                    if (tokenInfo.aud == configuration["Google:ClientId"])
                    {
                        return await CreateToken(tokenInfo.email, (employeeCode) =>
                        {
                            return STHelper.GenerateJWT(tokenInfo.email, tokenInfo.picture,
                                employeeCode.ToString());
                        });
                        //                        User user = userManager.GetUserByEmail(tokenInfo.email);
                        //                        if (user != null)
                        //                        {
                        //                            // Check trạng thái HRB
                        //                            HrbHistory hrbHistory = hrbHistoryService.GetHrbInDay(user.Id, DateTime.Now.Date);
                        //                            if (hrbHistory?.HrbStatus == HrbStatus.Active)
                        //                            {
                        //                                return Ok(STHelper.GenerateJWT(tokenInfo.email, tokenInfo.picture, user.EmployeeCode.ToString()));
                        //                            }
                        //                            else if (hrbHistory?.HrbStatus == HrbStatus.Pending)
                        //                            {
                        //                                return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.PENDING_USER_MESSAGE), new
                        //                                {
                        //                                    email
                        //                                });
                        //                            }
                        //                            else
                        //                            {
                        //                                return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.DEACTIVE_USER_MESSAGE), new
                        //                                {
                        //                                    email
                        //                                });
                        //                            }
                        //
                        //                        }
                    }
                }
            }
            return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.NOT_EXISTED_USER), new
            {
                email
            });
        }

        // POST api/oauth/token
        [AllowAnonymous]
        [HttpPost]
        [Route("ssotoken")]
        public async Task<IActionResult> Token([FromBody]SSOInfoModel request, [FromQuery] string language = "vi")
        {
            string email = string.Empty;
            if (request.AccessToken != null)
            {
                HttpResponseMessage response = await STHelper.VerifiedAccessToken(request.AccessToken);
                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content.ReadAsStringAsync().Result;
                    SSOInfoModel tokenInfo = JsonConvert.DeserializeObject<SSOInfoModel>(json);
                    if (!(tokenInfo?.IsActive ?? false))
                    {
                        return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.DEACTIVE_USER_MESSAGE), new
                        {
                            email,
                            active = false
                        });
                    }
                    if (string.IsNullOrEmpty(tokenInfo.Picture))
                    {
                        tokenInfo.Picture = string.Empty;
                    }
                    var clientIDs = AppSetting.Configuration.GetArrayValue<string>("SSO:ClientId");
                    if (tokenInfo.ClientId != null && clientIDs.Contains(tokenInfo.ClientId)) 
                    {
                        return await CreateToken(tokenInfo.Email, (employeeCode) =>
                            {
                                DateTime expiredTime;
                                try
                                {
                                    expiredTime = DateTimeOffset.FromUnixTimeMilliseconds(tokenInfo.Expired * 1000).LocalDateTime;
                                    if (expiredTime == null)
                                    {
                                        expiredTime = DateTime.Now.AddMinutes(Double.Parse(AppSetting.Configuration.GetValue("Jwt:Expires")));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    expiredTime = DateTime.Now.AddMinutes(Double.Parse(AppSetting.Configuration.GetValue("Jwt:Expires")));
                                }
                                return STHelper.GenerateJWT(tokenInfo.Email, tokenInfo.Picture,
                                    employeeCode.ToString(), expiredTime);
                            });
                        //                        User user = userManager.GetUserByEmail(tokenInfo.Email);
                        //                        if (user != null)
                        //                        {
                        //                            // Check trạng thái HRB
                        //                            HrbHistory hrbHistory = hrbHistoryService.GetHrbInDay(user.Id, DateTime.Now.Date);
                        //                            if (hrbHistory?.HrbStatus == HrbStatus.Active)
                        //                            {
                        //                                return Ok(STHelper.GenerateJWT(tokenInfo.Email, tokenInfo.Picture, user.EmployeeCode.ToString()));
                        //                            }
                        //                            else if (hrbHistory?.HrbStatus == HrbStatus.Pending)
                        //                            {
                        //                                return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.PENDING_USER_MESSAGE), new
                        //                                {
                        //                                    email
                        //                                });
                        //                            }
                        //                            else
                        //                            {
                        //                                return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.DEACTIVE_USER_MESSAGE), new
                        //                                {
                        //                                    email
                        //                                });
                        //                            }
                        //
                        //                        }
                    }
                }
            }
            return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.NOT_EXISTED_USER), new
            {
                email
            });
        }

        private async Task<IActionResult> CreateToken(string email, Func<int, string> generateTokenTask, string language = "vi")
        {
            User user = userManager.GetUserByEmail(email);
            if (user != null)
            {
                // Check trạng thái HRB
                HrbHistory hrbHistory = hrbHistoryService.GetHrbInDay(user.Id, DateTime.Now.Date);
                switch (hrbHistory?.HrbStatus)
                {
                    case HrbStatus.Active:
                        return Ok(generateTokenTask(user.EmployeeCode));
                    case HrbStatus.Pending:
                        return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.PENDING_USER_MESSAGE), new
                        {
                            email
                        });
                    default:
                        return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.DEACTIVE_USER_MESSAGE), new
                        {
                            email
                        });
                }
            }
            return BadRequest(MessageManager.GetMessageManager(language).GetValue(ResultCode.NOT_EXISTED_USER), new
            {
                email
            });
        }

        // GET: api/oauth/routes
        [HttpGet]
        [Route("routes")]
        public IActionResult Routes()
        {
            var email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            return Ok(userManager.GetRoutesByEmail(email));
        }
    }
}
