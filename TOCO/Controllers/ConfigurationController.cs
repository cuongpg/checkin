﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Controllers
{

    [Authorize]
    [Route("api/Configuration")]
    public class ConfigurationController : BaseController
    {
        private readonly AbsenderQuotasService absenderQuotasService;
        private readonly OvertimeQuotasService overtimeQuotasService;
        private readonly ApplicationDbContext context;

        public ConfigurationController(ApplicationDbContext context)
        {
            this.context = context;
            absenderQuotasService = new AbsenderQuotasService(context);
            overtimeQuotasService = new OvertimeQuotasService(context);
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("AddLeaveQuotas")]
        public IActionResult AddLeaveQuotas([FromBody] AbsenderQuotas absenderQuotas)
        {
            try
            {
                return Ok(absenderQuotasService.Create(absenderQuotas));
            }
            catch (Exception ex)
            {
                return LogicError();
            }
        }


        // GET: api/<controller>
        [HttpPost]
        [Route("AddOTQuotas")]
        public IActionResult AddOTQuotas([FromBody] OvertimeQuotas otQuotas)
        {
            try
            {
                OvertimeQuotas curQuotas;
                if((curQuotas = overtimeQuotasService.GetByUserAndGroup(otQuotas.UserId, otQuotas.EventGroupId, otQuotas.StartTime, otQuotas.EndTime)) != null)
                {
                    curQuotas.Quotas = otQuotas.Quotas;
                    return Ok(overtimeQuotasService.Update(curQuotas));
                }
                return Ok(overtimeQuotasService.Create(otQuotas));
            }
            catch (Exception ex)
            {
                return LogicError();
            }
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("ImportAbsenderQuotas")]
        public IActionResult ImportAbsenderQuotas()
        {

            List<string[]> errors = new List<string[]>();

            IFormFile file = Request.Form.Files[0];
            using (ExcelReader reader = new ExcelReader(file))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    string[][] data = reader.ReadFile("Data", 1);
                    foreach (string[] row in data)
                    {
                        try
                        {
                            absenderQuotasService.Create(new AbsenderQuotas
                            {
                                UserId = int.Parse(row[0]),
                                ReasonTypeId = int.Parse(row[1]),
                                EffectiveDate = DateTime.ParseExact(row[2], Constants.FORMAT_DATE_YYYYMMDD, CultureInfo.InvariantCulture).Date,
                                ExpirationDate = DateTime.ParseExact(row[3], Constants.FORMAT_DATE_YYYYMMDD, CultureInfo.InvariantCulture).Date,
                                Number = float.Parse(row[4])
                            });

                        }
                        catch (Exception ex)
                        {
                            errors.Add(row);
                        }
                    }
                    if (errors.Count == 0)
                    {
                        transaction.Commit();
                        return Ok(data);
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError("Import fails", errors.ToArray());
                    }
                }
            }
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("ImportOTQuotas")]
        public IActionResult ImportOTQuotas()
        {

            List<string[]> errors = new List<string[]>();

            IFormFile file = Request.Form.Files[0];
            using (ExcelReader reader = new ExcelReader(file))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    string[][] data = reader.ReadFile("Data", 1);
                    foreach (string[] row in data)
                    {
                        try
                        {
                            int userId = int.Parse(row[0]);
                            int groupEventId = int.Parse(row[1]);
                            int month = int.Parse(row[2]);
                            int year = int.Parse(row[3]);
                            DateTime startTime = new DateTime(year, month, 1);
                            DateTime endTime = new DateTime(year, month, DateTime.DaysInMonth(year,month));
                            double quotas = double.Parse(row[4]);
                            OvertimeQuotas curQuotas;
                            if ((curQuotas = overtimeQuotasService.GetByUserAndGroup(userId, groupEventId, startTime, endTime)) != null)
                            {
                                curQuotas.Quotas = quotas;
                                overtimeQuotasService.Update(curQuotas);
                            }
                            else
                            {
                                overtimeQuotasService.Create(new OvertimeQuotas
                                {
                                    UserId = userId,
                                    EventGroupId = groupEventId,
                                    StartTime = startTime,
                                    EndTime = endTime,
                                    Quotas = quotas
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.Add(row);
                        }
                    }
                    if (errors.Count == 0)
                    {
                        transaction.Commit();
                        return Ok(data);
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError("Import fails", errors.ToArray());
                    }
                }
            }
        }

    }
}
