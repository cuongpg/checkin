﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.API;
using TOCO.Common;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Contract")]
    public class ContractController : BaseController
    {
        private readonly UserManager userManager;
        public ContractController(ApplicationDbContext context)
        {
            userManager = new UserManager(context);
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> GetListZohoContracts(STRequestModel request, [FromQuery] string language = "vi")
        {
            string employeeCode = User.Claims.FirstOrDefault(c => c.Type == Constants.EMPLOYEE_CODE)?.Value;
            User user = userManager.GetUserById(int.Parse(employeeCode));
            (List<ZohoDataContracts> data, HttpStatusCode status, string message) = await ZohoDataAPI.Connection.GetListContract(employeeCode);
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), ZohoDataAPI.Connection.systemName));

        }

        [HttpGet]
        [Route("Detail/{id}")]
        public async Task<IActionResult> GetZohoContractDetail(string id, [FromQuery] string language = "vi")
        {
            string employeeCode = User.Claims.FirstOrDefault(c => c.Type == Constants.EMPLOYEE_CODE)?.Value;
            User user = userManager.GetUserById(int.Parse(employeeCode));
            (ZohoDataContracts data, HttpStatusCode status, string message) = await ZohoDataAPI.Connection.GetContractsById(employeeCode, id);
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), ZohoDataAPI.Connection.systemName));
        }
    }
}