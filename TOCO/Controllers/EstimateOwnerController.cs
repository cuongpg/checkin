﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.API;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/EstimateOwner")]
    public class EstimateOwnerController : BaseController
    {
        private EstimateOwnerService estimateOwnerService;
        private UserService userService;
        private CorporationService corporationService;
        private ConfirmAllocationManager confirmAllocationManager;
        public EstimateOwnerController(ApplicationDbContext context)
        {
            estimateOwnerService = new EstimateOwnerService(context);
            corporationService = new CorporationService(context);
            userService = new UserService(context);
            confirmAllocationManager = new ConfirmAllocationManager(context);
        }

        [HttpGet]
        [Route("api/SearchEstimateOwner")]
        public IActionResult SearchEstimateOwner(string keyWord)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);

            EstimateOwner currentEstimateOwner = estimateOwnerService.GetByCode(user.Department);

            if (string.IsNullOrEmpty(keyWord) && currentEstimateOwner != null)
            {
                keyWord = currentEstimateOwner.CompleteCode;
            }
            else
            {
                keyWord = string.Empty;
            }
            List<EstimateOwner> estimateOwners = estimateOwnerService.SearchEstimateOwner(keyWord);
            return Ok(new
            {
                currentEstimateOwner,
                estimateOwners
            });
        }

        [HttpGet]
        [Route("GetEstimateOwner")]
        public IActionResult GetEstimateOwner(string keyWord = "")
        {
            List<EstimateOwner> estimateOwners = estimateOwnerService.SearchEstimateOwner(keyWord);
            return Ok(new
            {
                estimateOwners
            });
        }

        [HttpGet]
        [Route("GetCorporations")]
        public IActionResult GetCorporations(string keyWord = "")
        {
            List<Corporation> corporations = corporationService.SearchCorporationService(keyWord);
            return Ok(new
            {
                corporations
            });
        }

        [HttpGet]
        [Route("GetOrgUnit")]
        public async Task<IActionResult> GetOrgUnit([FromQuery]int? level, [FromQuery] string language = "vi")
        {
            (List<OrgUnitResult> OrgUnitResults, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetListOrgUnit(level);
            return status == HttpStatusCode.OK ? Ok(OrgUnitResults) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("GetListOrgUnit")]
        public IActionResult GetListOrgUnit([FromQuery] int level, [FromQuery] string language = "vi")
        {
            List<Unit> orgUnitResults = GlobalVariable.Data.OrgUnits.Where(or => or.Level == level).ToList();
            return Ok(orgUnitResults);
        }

        [HttpGet]
        [Route("GetListSorttermUnit")]
        public IActionResult GetListSorttermUnit([FromQuery]string time, [FromQuery] string language = "vi")
        {
            List<Unit> orgUnitResults = new List<Unit>();
            DateTime dateTime = DateTime.ParseExact(time, Constants.FORMAT_DATE_YYYYMMDD, Thread.CurrentThread.CurrentCulture);
            DateTime startMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            orgUnitResults.AddRange(GlobalVariable.Data.OrgUnits.Where(org => org.Level == 4));
            List<Unit> activeProject = GlobalVariable.Data.Projects.Where(prj => string.Compare(prj.EndDate, startMonth.ToString(Constants.FORMAT_DATE_YYYYMMDD)) >= 0).ToList();
            orgUnitResults.RemoveAll(org => activeProject.Any(prj => prj.ShortCode == org.ShortCode));
            orgUnitResults.AddRange(activeProject);

            return Ok(orgUnitResults);
        }
    }
}
