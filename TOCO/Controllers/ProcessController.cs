﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Security.Claims;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using TOCO.API;
//using TOCO.Models;

//namespace TOCO.Controllers
//{
//    [AllowAnonymous]
//    [Produces("application/json")]
//    [Route("api/Process")]
//    public class ProcessController : BaseController
//    {

//        public ProcessController(ApplicationDbContext context)
//        {
//        }

//        // GET: api/<controller>
//        [HttpGet]
//        [Route("CountPendingRecords")]
//        public async Task<IActionResult> CountPendingProcessesAsync()
//        {
//            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
//            (int count, HttpStatusCode status, string message) = await PaoDataAPI.Connection.CountPendingProcessesAsync(email);
//            return Ok(new
//            {
//                count
//            });
//        }

//        [HttpGet]
//        [Route("List")]
//        public async Task<IActionResult> GetListPaoProcesses([FromQuery]int page = 0, [FromQuery]int perPage = 10, [FromQuery]string search = "")
//        {
//            page++;
//            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
//            (List<PaoProcessesResult> data, HttpStatusCode status, string message, int totalPages) = await PaoDataAPI.Connection.GetListPaoProcesses(email, search, page, perPage);
//            //(List<PaoProcessesResult> data, HttpStatusCode status, string message, int totalPages) = await SDocAPI.Connection.GetListSDocProcesses(email, search, page, perPage);
//            page--;
//            return status == HttpStatusCode.OK || status == HttpStatusCode.NotFound ? OkPagination(data, page, perPage, totalPages) : StatusCode((int)status, null);
//        }

//        [HttpGet]
//        [Route("Detail/{id}")]
//        public async Task<IActionResult> GetPaoProcessesInfo(int id)
//        {
//            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
//            (PaoProcessDetailResult data, HttpStatusCode status, string message) = await PaoDataAPI.Connection.GetPaoProcessDetailAsync(email, id);
//            return status == HttpStatusCode.OK || status == HttpStatusCode.NotFound ? Ok(data) : StatusCode((int)status, null);
//        }

//        [HttpPost]
//        [Route("Approval")]
//        public async Task<IActionResult> ApprovalProcessAsync([FromBody]ApprovalRequestModel requestBody)
//        {
//            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
//            (HttpStatusCode status, string message) = await PaoDataAPI.Connection.ApprovalProcessAsync(email, requestBody.ApprovalId);
//            return StatusCode((int)status, message);
//        }

//        [HttpPost]
//        [Route("Reject")]
//        public async Task<IActionResult> RejectProcessAsync([FromBody]RejectRequestModel requestBody)
//        {
//            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
//            if (string.IsNullOrEmpty(requestBody.Reason))
//            {
//                return BadRequest();
//            }
//            (HttpStatusCode status, string message) = await PaoDataAPI.Connection.RejectProcessAsync(email, requestBody.ApprovalId, requestBody.Reason);
//            return StatusCode((int)status, message);
//        }
//    }
//}