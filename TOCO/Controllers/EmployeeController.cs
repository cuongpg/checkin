﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TOCO.API;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Models.RequestModels;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Employee")]
    public class EmployeeController : BaseController
    {
        private readonly UserService userService;
        private readonly FirebaseTokenService firebaseTokenService;
        //private readonly OvertimeManager overtimeManager;
        private readonly ConfirmAllocationManager confirmAllocationManager;
        private readonly FacebookAccountService facebookAccountService;
        private readonly CorporationHistoryService corporationHistoryService;
        private readonly OrgUnitJoinInfoService orgUnitJoinInfoService;
        private readonly UserManager userManager;
        private readonly ScriptManager scriptManager;
        private readonly PopupActionService popupActionService;
        private readonly PopupConfigService popupConfigService;
        private readonly ClientManager clientManager;

        public EmployeeController(ApplicationDbContext context)
        {
            userService = new UserService(context);
            firebaseTokenService = new FirebaseTokenService(context);
            confirmAllocationManager = new ConfirmAllocationManager(context);
            corporationHistoryService = new CorporationHistoryService(context);
            orgUnitJoinInfoService = new OrgUnitJoinInfoService(context);
            userManager = new UserManager(context);
            scriptManager = new ScriptManager(context);
            popupActionService = new PopupActionService(context);
            popupConfigService = new PopupConfigService(context);
            clientManager = new ClientManager(context);
            facebookAccountService = new FacebookAccountService(context);
        }

        [HttpGet]
        [Route("PopupActions")]
        public IActionResult GetPopupActions([FromQuery]string language = "vi")
        {
            List<PopupAction> popupActions = popupActionService.GetAll();
            return Ok(popupActions);
        }

        [HttpPost]
        [Route("PopupConfig")]
        public IActionResult CreatPopupConfig([FromBody]PopupConfigRequestModel data, [FromQuery]string language = "vi")
        {
            PopupConfig popupConfig = new PopupConfig
            {
                Grades = JsonConvert.SerializeObject(data.Grades),
                Track1s = JsonConvert.SerializeObject(data.Track1s),
                Corporations = JsonConvert.SerializeObject(data.Corporations),
                Locations = JsonConvert.SerializeObject(data.Locations),
                OrgUnitLv2s = JsonConvert.SerializeObject(data.OrgUnitLv2s),
                OrgUnitLv3s = JsonConvert.SerializeObject(data.OrgUnitLv3s),
                OrgUnitLv4s = JsonConvert.SerializeObject(data.OrgUnitLv4s),
                OrgUnitLv5s = JsonConvert.SerializeObject(data.OrgUnitLv5s),
                Actions = JsonConvert.SerializeObject(data.Actions),
                Message = data.Message,
                Status = data.Status,
                EffectiveDate = data.EffectiveDate,
                ExpirationDate = data.ExpirationDate,
                CreatedAt = data.CreatedAt,
                UpdatedAt = data.UpdatedAt
            };

            popupConfig = popupConfigService.Create(popupConfig);
            return Ok(popupConfig);
        }

        [HttpPut]
        [Route("PopupConfig")]
        public IActionResult UpdatePopupConfig([FromBody]PopupConfigRequestModel data, [FromQuery]string language = "vi")
        {
            PopupConfig popupConfig = popupConfigService.GetById(data.Id);

            popupConfig.Grades = JsonConvert.SerializeObject(data.Grades);
            popupConfig.Track1s = JsonConvert.SerializeObject(data.Track1s);
            popupConfig.Corporations = JsonConvert.SerializeObject(data.Corporations);
            popupConfig.Locations = JsonConvert.SerializeObject(data.Locations);
            popupConfig.OrgUnitLv2s = JsonConvert.SerializeObject(data.OrgUnitLv2s);
            popupConfig.OrgUnitLv3s = JsonConvert.SerializeObject(data.OrgUnitLv3s);
            popupConfig.OrgUnitLv4s = JsonConvert.SerializeObject(data.OrgUnitLv4s);
            popupConfig.OrgUnitLv5s = JsonConvert.SerializeObject(data.OrgUnitLv5s);
            popupConfig.Actions = JsonConvert.SerializeObject(data.Actions);
            popupConfig.Message = data.Message;
            popupConfig.Status = data.Status;
            popupConfig.EffectiveDate = data.EffectiveDate;
            popupConfig.ExpirationDate = data.ExpirationDate;

            popupConfig = popupConfigService.Update(popupConfig);
            return Ok(popupConfig);
        }

        [HttpGet]
        [Route("PopupConfigs")]
        public IActionResult GetPopupConfigs([FromQuery]string language = "vi")
        {
            List<PopupConfig> data = popupConfigService.GetAll();
            return Ok(data);
        }

        [HttpGet]
        [Route("PopupConfig/{id}")]
        public IActionResult GetPopupConfig(int id, [FromQuery]string language = "vi")
        {
            PopupConfig data = popupConfigService.GetById(id);
            return Ok(data);
        }

        [HttpGet]
        [Route("CheckPopUpStatus")]
        public async Task<IActionResult> CheckPopUpStatus([FromQuery] LocationRequestModel request, [FromQuery] string language = "vi")
        {
            DateTime timeNow = DateTime.Now.Date;
            DateTime startMonth = new DateTime(timeNow.Year, timeNow.Month, 1);
            DateTime endMonth = new DateTime(timeNow.Year, timeNow.Month, DateTime.DaysInMonth(timeNow.Year, timeNow.Month));
            string startMonthText = startMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            string endMonthText = endMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            PopupConfig popupConfig = popupConfigService.GetConfigByDate(timeNow);
            string[] result = new string[] { };
            string message = string.Empty;
            if (popupConfig != null)
            {
                string[] grades = JsonConvert.DeserializeObject<string[]>(popupConfig.Grades);
                string[] track1s = JsonConvert.DeserializeObject<string[]>(popupConfig.Track1s);
                string[] corporations = JsonConvert.DeserializeObject<string[]>(popupConfig.Corporations);
                string[] locations = JsonConvert.DeserializeObject<string[]>(popupConfig.Locations);
                string[] orgUnitLv2s = JsonConvert.DeserializeObject<string[]>(popupConfig.OrgUnitLv2s);
                string[] orgUnitLv3s = JsonConvert.DeserializeObject<string[]>(popupConfig.OrgUnitLv3s);
                string[] orgUnitLv4s = JsonConvert.DeserializeObject<string[]>(popupConfig.OrgUnitLv4s);
                string[] orgUnitLv5s = JsonConvert.DeserializeObject<string[]>(popupConfig.OrgUnitLv5s);
                string[][] dataConfigs =
                {
                    grades,
                    track1s,
                    corporations,
                    orgUnitLv2s,
                    orgUnitLv3s,
                    orgUnitLv4s,
                    orgUnitLv5s
                };
                string[] fieldNames = { "CB", "NG1", "PT", "SP", "Org3", "CDT", "PB" };
                bool bCheck = popupConfig != null;
                (List<HKTStatementInfo> statementInfos, HttpStatusCode statementStatus, string statementMessage) = await HKTDataAPI.Connection.GetStatementInfoAsync(user.EmployeeCode);
                // Nếu không lấy thông tin từ HKT
                if (statementStatus == HttpStatusCode.OK)
                {
                    // Lấy ra quyết định cuối cùng trong tháng
                    HKTStatementInfo statementInfo = statementInfos?
                        .Where(s => string.Compare(s.StatementValidDate, endMonthText) <= 0)
                        .Where(s => string.Compare(s.StatementExpiredDate, startMonthText) >= 0)
                        .OrderByDescending(s => s.StatementValidDate).FirstOrDefault();
                    int indx = 0;
                    foreach (string[] config in dataConfigs)
                    {
                        if (config != null && config.Length > 0)
                        {
                            string fieldName = fieldNames[indx];
                            string value = ((string)statementInfo?.GetType().GetProperty(fieldName)?.GetValue(statementInfo))?.Split("-")[0].Trim() ?? string.Empty;
                            bCheck = !string.IsNullOrEmpty(value) & config.Contains(value);
                            if (!bCheck)
                            {
                                break;
                            }
                        }
                        indx++;
                    }
                }
                //if (bCheck && locations.Length > 0)
                //{
                //    (Location location, double distance) = clientManager.GetTopicaLocation(request);
                //    bCheck &= locations.Contains(location.Address);
                //}
                if (bCheck)
                    result = JsonConvert.DeserializeObject<string[]>(popupConfig.Actions);
                message = popupConfig.Message;

            }
            return Ok(new { result, message });
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> InfoAsync([FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            string imageUrl = User.Claims.FirstOrDefault(c => c.Type == Constants.IMAGE_URL)?.Value;
            HrDataEmployeeResult employeeInfo = null;
            try
            {
                User user = userService.GetByEmail(email);
                (HKTEmployeeInfo employeeData, HttpStatusCode employeeStatus, string employeeMessage) = await HKTDataAPI.Connection.GetEmployeeInfoAsync(user.EmployeeCode);
                (List<HKTStatementInfo> statementData, HttpStatusCode statementStatus, string statementMessage) = await HKTDataAPI.Connection.GetStatementInfoAsync(user.EmployeeCode);

                if (employeeStatus == HttpStatusCode.OK && statementStatus == HttpStatusCode.OK)
                {
                    employeeInfo = STHelper.ConvertObjectByLanguage<HrDataEmployeeResult, HKTEmployeeInfo>(employeeData, language);
                    employeeInfo.EmployeeAvatar = imageUrl;
                    employeeInfo.EmployeeWorkEmail = email;
                    HKTStatementInfo statementLast = statementData.OrderBy(s => s.StatementValidDate).Last();
                    HrDataStatementResult statementLastResult = STHelper.ConvertObjectByLanguage<HrDataStatementResult, HKTStatementInfo>(statementLast, language);
                    employeeInfo.Statements = new List<HrDataStatementResult> { statementLastResult };
                    user.Department = statementLast?.DV ?? string.Empty;
                    if (user.ImageUrl != imageUrl)
                    {
                        user.ImageUrl = imageUrl;
                        user = userService.Update(user);
                    }
                    employeeInfo.FireBaseTokens = firebaseTokenService.GetFirebaseTokensByEmail(employeeInfo.EmployeeWorkEmail).Select(fb => fb.Token).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return Ok(employeeInfo);
        }

        [HttpGet]
        [Route("TimeKeepingManagerInfo")]
        public IActionResult TimeKeepingManagerInfo()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            return TimeKeepingManagerInfo(email);
        }

        [HttpGet]
        [Route("AdminGetTimeKeepingManagerInfo")]
        public IActionResult AdminGetTimeKeepingManagerInfo([FromQuery] string email)
        {
            return TimeKeepingManagerInfo(email);
        }

        private IActionResult TimeKeepingManagerInfo(string email)
        {
            User user = userService.GetByEmail(email);

            if (user == null)
            {
                return NotFound();
            }
            User manager = userService.GetById(user.ManagerId);
            User HeadUnit = user.HeadOfOrgUnitId is null ? null : userService.GetById((int)user.HeadOfOrgUnitId);
            return Ok(new
            {
                UserName = user.FullName,
                ManagerEmail = manager.Email,
                ManagerName = manager.FullName,
                user.Department,
                HeadOfOrgUnitEmail = HeadUnit?.Email,
                HeadOfOrgUnitName = HeadUnit?.FullName
            });
        }

        [HttpGet]
        [Route("Search")]
        public async Task<IActionResult> SearchEmployeeAsync([FromQuery]int page = 0, [FromQuery]int perPage = 10, [FromQuery]string search = "", [FromQuery]string language = "vi")
        {
            if (perPage <= 0)
            {
                return BadRequest();
            }
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            (List<EmployeeInfoResult> data, HttpStatusCode status, string message, int totalPages) = await HKTDataAPI.Connection.SearchEmployee(search, page, perPage, language);
            return status == HttpStatusCode.OK ? OkPagination(data, page, perPage, totalPages) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("Detail")]
        public async Task<IActionResult> EmployeeDetail([FromQuery]int id, [FromQuery]string language = "vi")
        {
            (EmployeeDetailResult data, HttpStatusCode status, string message) = await HKTDataAPI.Connection.EmployeeDetail(id, language);
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("COMMechanismList")]
        public async Task<IActionResult> GetCOMMechanismList([FromQuery] string language = "vi")
        {
            DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            if (DateTime.Now.Day < script.CutoffTimeKeepingDay)
            {
                startMonth = startMonth.AddMonths(-1);
            }
            DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month)).Date;
            COMMechanismInput input = new COMMechanismInput
            {
            };
            (List<COMMechanism> data, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetCOMMechanismList(input, startMonth, endMonth);
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("ProjectInformations")]
        public async Task<IActionResult> GetProjectInformations([FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            (List<ProjectInfo> data, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetListProjectInfomations();
            foreach (ProjectInfo proj in data)
            {
                //Tính theo cơ chế đơn vị
                Mechanism departmentMechanism = confirmAllocationManager.GetDepartmentMechanismByHCMCode(proj.PROJTYPE);
                proj.PROJ_TYPE_NAME = departmentMechanism?.MechanismName;
            }
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("DepartmentMechanism")]
        public IActionResult GetDepartmentMechanism([FromQuery] string language = "vi")
        {
            List<Mechanism> departmentMechanisms = confirmAllocationManager.GetAllDepartmentMechanism();
            return Ok(departmentMechanisms);
        }

        //[HttpGet]
        //[Route("IsNeedConfirmByEmployee")]
        //public IActionResult IsNeedConfirmByEmployee()
        //{
        //    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        //    User user = userService.GetByEmail(email);
        //    ConfirmAllocation confirmAllocation = confirmAllocationManager.GetWaitReportByUserId(user.Id);
        //    return Ok(confirmAllocation != null);
        //}

        [HttpGet]
        [Route("IsNeedConfirmByEmployeeV2")]
        public IActionResult IsNeedConfirmByEmployeeV2()
        {
            DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;

            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            if (DateTime.Now.Day < script.CutoffTimeKeepingDay)
            {
                startMonth = startMonth.AddMonths(-1);
            }

            DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month)).Date;
            ConfirmAllocationV2 confirmAllocation = confirmAllocationManager.GetWaitReportByUserIdV2(user.Id, startMonth, endMonth);
            return Ok(confirmAllocation != null);
        }

        [HttpGet]
        [Route("IsNeedDeclareFacebook")]
        public IActionResult IsNeedDeclareFacebook()
        {
            bool bOk = false;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            string[] corporationTypes = AppSetting.Configuration.GetArrayValue<string>("CheckFacebook");
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            if (corporationTypes.Contains(script.CorporationType))
            {
                FacebookAccount facebookAccount = facebookAccountService.GetByUserId(user.Id);
                bOk = facebookAccount == null || string.IsNullOrEmpty(facebookAccount.Facebook);
            }

            return Ok(bOk);
        }


        [HttpPost]
        [Route("DeclareFacebook")]
        public IActionResult DeclareFacebook([FromBody] FacebookRequestModel facebook)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);

            FacebookAccount facebookAccount = facebookAccountService.GetByUserId(user.Id);
            if (facebookAccount == null)
            {
                facebookAccount = new FacebookAccount
                {
                    UserId = user.Id,
                    Facebook = facebook.Value
                };
                facebookAccount = facebookAccountService.Create(facebookAccount);
            }
            else
            {
                facebookAccount.Facebook = facebook.Value;
                facebookAccount = facebookAccountService.Update(facebookAccount);
            }

            return Ok(facebookAccount);
        }


        //[HttpPost]
        //[Route("CreatedSessionAllocation")]
        //public IActionResult CreatedSessionAllocation([FromBody] int[] userIds)
        //{
        //    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        //    int number = confirmAllocationManager.CreatedSessionAllocation(userIds);
        //    return Ok(number);
        //}

        [HttpPost]
        [Route("CreatedSessionAllocationV2")]
        public IActionResult CreatedSessionAllocationV2([FromBody] int[] userIds)
        {
            DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;

            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            if (DateTime.Now.Day < script.CutoffTimeKeepingDay)
            {
                startMonth = startMonth.AddMonths(-1);
            }
            DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month)).Date;
            int number = confirmAllocationManager.CreatedSessionAllocationV2(userIds, startMonth, endMonth);
            return Ok(number);
        }

        //[HttpGet]
        //[Route("MechanismInformation")]
        //public async Task<IActionResult> GetMechanismInformation([FromQuery] string language = "vi")
        //{
        //    DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
        //    DateTime endMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
        //    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        //    User user = userService.GetByEmail(email);
        //    MechanismInformation result = await confirmAllocationManager.GetMechanismInformationByEmail(user, startMonth, endMonth);
        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    else
        //    {
        //        return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        //    }
        //}

        [HttpGet]
        [Route("MechanismInformationV2")]
        public async Task<IActionResult> GetMechanismInformationV2([FromQuery] string language = "vi")
        {
            DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;

            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            if (DateTime.Now.Day < script.CutoffTimeKeepingDay)
            {
                startMonth = startMonth.AddMonths(-1);
            }

            DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month)).Date;
            MechanismInformationV2 result = await confirmAllocationManager.GetMechanismInformationV2ByEmail(user, startMonth, endMonth);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
            }
        }

        [HttpGet]
        [Route("MechanismInformationForAdminV2/{month}/{year}")]
        public async Task<IActionResult> GetMechanismInformationForAdminV2([FromQuery] int employeeCode, int month, int year, [FromQuery] string language = "vi")
        {
            DateTime startMonth = new DateTime(year, month, 1).Date;
            DateTime endMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
            User user = userService.GetByEmployeeCode(employeeCode);
            MechanismInformationV2 result = await confirmAllocationManager.GetMechanismInformationV2ByEmail(user, startMonth, endMonth);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
            }
        }


        //[HttpGet]
        //[Route("MechanismInformationForAdmin")]
        //public async Task<IActionResult> GetMechanismInformationForAdmin([FromQuery] int employeeCode, [FromQuery] string language = "vi")
        //{
        //    DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
        //    DateTime endMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
        //    User user = userService.GetByEmployeeCode(employeeCode);
        //    MechanismInformation result = await confirmAllocationManager.GetMechanismInformationByEmail(user, startMonth, endMonth);
        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    else
        //    {
        //        return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        //    }
        //}

        //[HttpGet]
        //[Route("MechanismInformationForAdmin/{month}/{year}")]
        //public async Task<IActionResult> GetMechanismInformationForAdmin([FromQuery] int employeeCode, int month, int year, [FromQuery] string language = "vi")
        //{
        //    DateTime startMonth = new DateTime(year, month, 1).Date;
        //    DateTime endMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
        //    User user = userService.GetByEmployeeCode(employeeCode);
        //    MechanismInformation result = await confirmAllocationManager.GetMechanismInformationByEmail(user, startMonth, endMonth);
        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    else
        //    {
        //        return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HKTDataAPI.Connection.systemName));
        //    }
        //}

        //[HttpGet]
        //[Route("ConfirmInformation")]
        //public async Task<IActionResult> ConfirmInformationAsync([FromQuery] string language = "vi")
        //{
        //    DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
        //    DateTime endMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
        //    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        //    User user = userService.GetByEmail(email);
        //    ConfirmAllocation confirmAllocation = confirmAllocationManager.GetWaitReportByUserId(user.Id);
        //    if (confirmAllocation == null)
        //    {
        //        return NotFound();
        //    }
        //    MechanismInformation changeInfo = await confirmAllocationManager.GetMechanismInformationByEmail(user, startMonth, endMonth);
        //    confirmAllocation.OldValue = JsonConvert.SerializeObject(changeInfo);
        //    confirmAllocation.Status = ConfirmAllocationStatus.ConfirmByEmployee;
        //    int confirmNumber = confirmAllocationManager.Update(confirmAllocation);
        //    return Ok(confirmNumber > 0);
        //}

        [HttpPost]
        [Route("ConfirmInformationV2")]
        public async Task<IActionResult> ConfirmInformationV2Async([FromQuery] string language = "vi")
        {
            DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userService.GetByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            if (DateTime.Now.Day < script.CutoffTimeKeepingDay)
            {
                startMonth = startMonth.AddMonths(-1);
            }
            DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month)).Date;
            ConfirmAllocationV2 confirmAllocation = confirmAllocationManager.GetWaitReportByUserIdV2(user.Id, startMonth, endMonth);
            if (confirmAllocation == null)
            {
                return NotFound(ResultCode.NOT_EXISTED_ALLOCATION_REQUEST);
            }
            confirmAllocationManager.DestroyRecordByStatusV2(user.Id, ConfirmAllocationStatusV2.Reporting);
            MechanismInformationV2 changeInfo = await confirmAllocationManager.GetMechanismInformationV2ByEmail(user, startMonth, endMonth);
            confirmAllocation.OldValue = JsonConvert.SerializeObject(changeInfo);
            confirmAllocation.Status = ConfirmAllocationStatusV2.ConfirmByEmployee;
            int confirmNumber = confirmAllocationManager.UpdateV2(confirmAllocation);
            return Ok(confirmNumber > 0);
        }


        //[HttpPost]
        //[Route("Order")]
        //public async Task<IActionResult> Order([FromBody]MechanismInformation changeInfo, [FromQuery] string language = "vi")
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest();
        //    }
        //    try
        //    {
        //        DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
        //        DateTime endMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).Date;
        //        string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        //        User user = userService.GetByEmail(email);
        //        if (int.Parse(changeInfo.EmployeeCode) != user.EmployeeCode || changeInfo.Email != email)
        //        {
        //            return Forbidden();
        //        }
        //        if (string.IsNullOrEmpty(changeInfo.UserName))
        //        {
        //            changeInfo.UserName = email.Substring(0, email.IndexOf('@'));
        //        }

        //        // Lấy thông tin cũ
        //        MechanismInformation oldValue = await confirmAllocationManager.GetMechanismInformationByEmail(user, startMonth, endMonth);

        //        // Xử lý thông tin cơ chế lương thưởng tham gia
        //        changeInfo.StatementCheckin.COMMechanisms = ComMechanismProcess(user.Id, changeInfo.StatementCheckin.COMMechanisms, oldValue?.StatementCheckin?.COMMechanisms, startMonth, endMonth);

        //        // Xử lý thông tin project tham gia
        //        ProjectProcess(user.Id, ref changeInfo);

        //        ConfirmAllocation confirmAllocation = confirmAllocationManager.GetWaitReportByUserId(user.Id);
        //        confirmAllocationManager.DestroyRecordByStatus(user.Id, ConfirmAllocationStatus.Reporting);
        //        if (confirmAllocation == null)
        //        {
        //            confirmAllocation = new ConfirmAllocation
        //            {
        //                UserId = user.EmployeeCode,
        //                OldValue = JsonConvert.SerializeObject(oldValue),
        //                NewValue = JsonConvert.SerializeObject(changeInfo),
        //                Status = ConfirmAllocationStatus.Reporting
        //            };
        //            confirmAllocation = confirmAllocationManager.Create(confirmAllocation);
        //        }
        //        else
        //        {
        //            confirmAllocation.OldValue = JsonConvert.SerializeObject(oldValue);
        //            confirmAllocation.NewValue = JsonConvert.SerializeObject(changeInfo);
        //            confirmAllocation.Status = ConfirmAllocationStatus.Reporting;
        //            confirmAllocationManager.Update(confirmAllocation);
        //        }

        //        changeInfo.RecordId = confirmAllocation.Id;
        //        (string response, HttpStatusCode status, string message) = await HrOperAPI.Connection.Order(new OrderMechanismInformation
        //        {
        //            OldValue = oldValue,
        //            NewValue = changeInfo
        //        });

        //        // Update trạng thái order HrOper
        //        confirmAllocation.HrOperResult = status.ToString();
        //        confirmAllocationManager.Update(confirmAllocation);

        //        return Ok(response);
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HrOperAPI.Connection.systemName), ex);
        //    }
        //}

        [HttpPost]
        [Route("OrderV2")]
        public async Task<IActionResult> OrderV2([FromBody]MechanismInformationV2 changeInfo, [FromQuery] string language = "vi")
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
                string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                User user = userService.GetByEmail(email);
                Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
                if (DateTime.Now.Day < script.CutoffTimeKeepingDay)
                {
                    startMonth = startMonth.AddMonths(-1);
                }
                DateTime endMonth = new DateTime(startMonth.Year, startMonth.Month, DateTime.DaysInMonth(startMonth.Year, startMonth.Month)).Date;


                if (int.Parse(changeInfo.EmployeeCode) != user.EmployeeCode || changeInfo.Email != email)
                {
                    return Forbidden();
                }
                if (string.IsNullOrEmpty(changeInfo.UserName))
                {
                    changeInfo.UserName = email.Substring(0, email.IndexOf('@'));
                }

                // Lấy thông tin cũ
                MechanismInformationV2 oldValue = await confirmAllocationManager.GetMechanismInformationV2ByEmail(user, startMonth, endMonth);

                // Xử lý thông tin cơ chế lương thưởng tham gia
                changeInfo.ComMechanisms = ComMechanismProcess(user.Id, changeInfo.ComMechanisms, oldValue?.ComMechanisms, startMonth, endMonth);

                // Xử lý thông tin project tham gia
                changeInfo.SorttermAllocation = SorttermProcessV2(user.Id, changeInfo.SorttermAllocation, oldValue.SorttermAllocation, startMonth, endMonth);

                // Tính định mức OT trong tháng của nhân sự
                changeInfo.OvertimeQuotas = confirmAllocationManager.CalcCurrentOverTimeQuotasV2(changeInfo, startMonth, endMonth);

                ConfirmAllocationV2 confirmAllocation = confirmAllocationManager.GetWaitReportByUserIdV2(user.Id, startMonth, endMonth);
                confirmAllocationManager.DestroyRecordByStatusV2(user.Id, ConfirmAllocationStatusV2.Reporting);
                if (confirmAllocation == null)
                {
                    confirmAllocation = new ConfirmAllocationV2
                    {
                        UserId = user.EmployeeCode,
                        OldValue = JsonConvert.SerializeObject(oldValue),
                        NewValue = JsonConvert.SerializeObject(changeInfo),
                        Status = ConfirmAllocationStatusV2.Reporting,
                        StartDate = startMonth,
                        EndDate = endMonth
                    };
                    confirmAllocation = confirmAllocationManager.CreateV2(confirmAllocation);
                }
                else
                {
                    confirmAllocation.OldValue = JsonConvert.SerializeObject(oldValue);
                    confirmAllocation.NewValue = JsonConvert.SerializeObject(changeInfo);
                    confirmAllocation.Status = ConfirmAllocationStatusV2.Reporting;
                    confirmAllocationManager.UpdateV2(confirmAllocation);
                }
                oldValue.RecordId = confirmAllocation.Id;
                changeInfo.RecordId = confirmAllocation.Id;
                (string response, HttpStatusCode status, string message) = await HrOperAPI.Connection.OrderV2(new OrderMechanismInformationV2
                {
                    OldValue = oldValue,
                    NewValue = changeInfo
                });

                // Update trạng thái order HrOper
                confirmAllocation.HrOperResult = status.ToString();
                confirmAllocationManager.UpdateV2(confirmAllocation);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HrOperAPI.Connection.systemName), ex);
            }
        }

        private void ProjectProcess(int userId, ref MechanismInformation changeInfo)
        {
            // Kiểm tra danh sách project, gán HCM ID cho từng project tham gia,
            // nếu có chuyển pháp nhân trong khoảng thời gian tham gia project thì tách làm nhiều giai đoạn và gán HCM ID cho từng giai đoạn
            List<ProjectCheckin> newProjects = new List<ProjectCheckin>();
            foreach (ProjectCheckin project in changeInfo.Projects)
            {
                DateTime startTime = DateTime.Parse(project.StartDate);
                DateTime endTime = DateTime.Parse(project.EndDate);
                List<CorporationHistory> corporationHistories = corporationHistoryService.GetByUserAndTime(userId, startTime, endTime).OrderBy(corp => corp.ExpirationDate).ToList();
                if (corporationHistories.Count == 0)
                {
                    project.HcmId = string.Format("{0,0:D8}", userId);
                    newProjects.Add(project);
                }
                else if (corporationHistories.Count == 1)
                {
                    project.HcmId = corporationHistories[0].HcmPersionalId;
                    newProjects.Add(project);
                }
                else // Nhiều pháp nhân
                {
                    List<DateTime> expiredTimes = corporationHistories
                        .Where(corp => corp.ExpirationDate <= endTime)
                        .Where(corp => corp.ExpirationDate >= startTime)
                        .OrderBy(corp => corp.ExpirationDate)
                        .Select(corp => corp.ExpirationDate)
                        .ToList();
                    if (expiredTimes.Count == 0)// Nếu đồng thời cùng ở nhiều pháp nhân
                    {
                        project.HcmId = corporationHistories[0].HcmPersionalId;
                        newProjects.Add(project);
                    }
                    else
                    {
                        DateTime oldStartTime = startTime.AddDays(-1);
                        foreach (DateTime exp in expiredTimes)
                        {
                            ProjectCheckin newProject = project.ShallowCopy();
                            newProject.StartDate = oldStartTime.AddDays(1).ToString(Constants.FORMAT_DATE_YYYYMMDD);
                            newProject.EndDate = exp.ToString(Constants.FORMAT_DATE_YYYYMMDD);
                            newProject.HcmId = corporationHistories.Where(corp => corp.ExpirationDate == exp).First().HcmPersionalId;
                            newProjects.Add(newProject);
                            oldStartTime = exp;
                        }
                        if (oldStartTime.AddDays(1) < endTime)
                        {
                            ProjectCheckin newProject = project.ShallowCopy();
                            newProject.StartDate = oldStartTime.AddDays(1).ToString(Constants.FORMAT_DATE_YYYYMMDD);
                            newProject.EndDate = endTime.ToString(Constants.FORMAT_DATE_YYYYMMDD);
                            newProject.HcmId = corporationHistories.Last().HcmPersionalId;
                            newProjects.Add(newProject);
                        }
                    }
                }
            }
            changeInfo.Projects = newProjects;
        }

        private SorttermAllocation SorttermProcessV2(int userId, SorttermAllocation newSortterm, SorttermAllocation oldSortterm, DateTime startMonth, DateTime endMonth)
        {
            string startMonthText = startMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            string endMonthText = endMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            string previousEndMonthText = new DateTime(endMonth.AddMonths(-1).Year, endMonth.AddMonths(-1).Month, DateTime.DaysInMonth(endMonth.AddMonths(-1).Year, endMonth.AddMonths(-1).Month)).ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS);
            // Kiểm tra danh sách project, gán HCM ID cho từng project tham gia,
            // nếu có chuyển pháp nhân trong khoảng thời gian tham gia project thì tách làm nhiều giai đoạn và gán HCM ID cho từng giai đoạn
            newSortterm.Departments = new List<JoinUnit>();
            newSortterm.Projects = new List<JoinUnit>();
            // Tìm và loại bỏ các project, org bị remove bằng cách set percent về 0
            List<JoinUnit> removeProjects = oldSortterm.Projects
                .Where(prj => newSortterm.All.All(u => u.ShortCode != prj.ShortCode)).ToList();
            List<JoinUnit> removeDepartments = oldSortterm.Departments
               .Where(org => newSortterm.All.All(u => u.ShortCode != org.ShortCode)).ToList();
            // Loại bỏ các project
            foreach (JoinUnit removeProject in removeProjects)
            {
                JoinUnit newUnit = removeProject.ShallowCopy();
                AddHcmId(userId, ref newUnit);
                // Xóa bản ghi cũ
                removeProject.Percent = 0;
                newSortterm.Projects.Add(removeProject);
                if (string.Compare(removeProject.StartDate, startMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS)) < 0)
                {
                    JoinUnit preUnit = newUnit.ShallowCopy();
                    preUnit.EndDate = previousEndMonthText;
                    newSortterm.Projects.Add(preUnit);
                }
            }
            // Loaị bỏ các department nếu có lưu trên checkin
            foreach (JoinUnit removeDepartment in removeDepartments)
            {
                RemoveOrgUnitJoinInfo(userId, removeDepartment);
            }
            // Duyệt unit hiện tại để xử lý thêm vào projects và departments
            foreach (JoinUnit unit in newSortterm.All)
            {
                JoinUnit newUnit = unit.ShallowCopy();
                AddHcmId(userId, ref newUnit);

                // Check unit là project hay org hoặc cả 2
                bool isProject = GlobalVariable.Data.Projects.Any(prj => prj.ShortCode == newUnit.ShortCode);
                bool isDepartment = GlobalVariable.Data.OrgUnits.Any(org => org.ShortCode == newUnit.ShortCode);

                if (isProject)
                {
                    JoinUnit newProject = newUnit.ShallowCopy();
                    // Lấy ra project hiện tại trên HCM
                    JoinUnit oldProject = oldSortterm.Projects.Find(prj => prj.ShortCode == newProject.ShortCode);
                    // Nếu chưa có, thêm mới
                    // Nếu có rồi và chỉ thay đổi %, không thay đổi thời gian => thay đổi %
                    if (oldProject == null || (oldProject.Percent != newProject.Percent
                        && oldProject.StartDate.Substring(0, Constants.TOTAL_NUMBER_DATE) == newProject.StartDate && oldProject.EndDate.Substring(0, Constants.TOTAL_NUMBER_DATE) == newProject.EndDate))
                    {
                        newSortterm.Projects.Add(newProject);
                    }
                    // Nếu thay đổi thời gian => Xóa bản ghi cũ, thêm 1 bản ghi trong quá khứ, thêm bản ghi mới
                    else if (oldProject.StartDate.Substring(0, Constants.TOTAL_NUMBER_DATE) != newProject.StartDate || oldProject.EndDate.Substring(0, Constants.TOTAL_NUMBER_DATE) != newProject.EndDate)
                    {
                        // Add HcmId
                        AddHcmId(userId, ref oldProject);
                        // Xóa bản ghi cũ
                        JoinUnit deleteProject = oldProject.ShallowCopy();
                        deleteProject.Percent = 0;
                        newSortterm.Projects.Add(deleteProject);
                        if (string.Compare(oldProject.StartDate, startMonth.ToString(Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS)) < 0)
                        {
                            JoinUnit preProject = oldProject.ShallowCopy();
                            preProject.EndDate = previousEndMonthText;
                            newSortterm.Projects.Add(preProject);
                        }
                        newSortterm.Projects.Add(newProject);
                    }
                }

                if (isDepartment)
                {
                    JoinUnit newDepartment = newUnit.ShallowCopy();
                    // Gán lại ID theo Org
                    Unit org = GlobalVariable.Data.OrgUnits.Find(o => o.ShortCode == newDepartment.ShortCode);
                    newDepartment.Id = org.Id;
                    newDepartment.Name = org.Name;
                    newDepartment.Code = org.Code;
                    newDepartment.ParentId = org.ParentId;
                    newDepartment.Level = org.Level;

                    // Lấy ra org hiện tại trên HCM
                    JoinUnit oldOrg = oldSortterm.Departments.Find(o => o.ShortCode == newDepartment.ShortCode);

                    // Nếu chưa có, thêm mới
                    // Nếu có rồi thì thôi
                    if (oldOrg == null)
                    {
                        newSortterm.Departments.Add(newDepartment);
                    }
                    // Nếu không phải là project lưu lại trên checkin
                    if (!isProject)
                    {
                        // Lưu percent vào db
                        CreateOrgUnitJoinInfo(userId, newDepartment);
                    }
                }
            }
            return newSortterm;
        }

        private void CreateOrgUnitJoinInfo(int userId, JoinUnit unit)
        {
            DateTime start = DateTime.ParseExact(unit.StartDate, Constants.FORMAT_DATE_YYYYMMDD, Thread.CurrentThread.CurrentCulture);
            DateTime end = DateTime.ParseExact(unit.EndDate, Constants.FORMAT_DATE_YYYYMMDD, Thread.CurrentThread.CurrentCulture);
            List<OrgUnitJoinInfo> orgUnitJoinInfos = orgUnitJoinInfoService.GetOrgUnitJoinInfo(userId, unit.ShortCode, start, end);
            if (orgUnitJoinInfos.Count == 0)
            {
                OrgUnitJoinInfo orgUnitJoinInfo = new OrgUnitJoinInfo
                {
                    UserId = userId,
                    OrgUnitId = unit.Id,
                    Code = unit.Code,
                    Name = unit.Name,
                    StartDate = start,
                    EndDate = end,
                    Percentage = unit.Percent,
                    Status = OrgUnitJoinInfoStatus.Active,
                    HcmRole = unit.HcmRole
                };
                orgUnitJoinInfoService.Create(orgUnitJoinInfo);
            }
            else if (orgUnitJoinInfos.Count == 1)
            {
                orgUnitJoinInfos[0].StartDate = start;
                orgUnitJoinInfos[0].EndDate = end;
                orgUnitJoinInfos[0].Percentage = unit.Percent;
                orgUnitJoinInfos[0].HcmRole = unit.HcmRole;
                orgUnitJoinInfoService.Update(orgUnitJoinInfos[0]);
            }
            else
            {
                RemoveOrgUnitJoinInfo(userId, unit);
                OrgUnitJoinInfo orgUnitJoinInfo = new OrgUnitJoinInfo
                {
                    UserId = userId,
                    OrgUnitId = unit.Id,
                    Code = unit.Code,
                    ShortCode = unit.ShortCode,
                    Name = unit.Name,
                    StartDate = start,
                    EndDate = end,
                    Percentage = unit.Percent,
                    Status = OrgUnitJoinInfoStatus.Active,
                    HcmRole = unit.HcmRole
                };
                orgUnitJoinInfoService.Create(orgUnitJoinInfo);
            }

        }

        private void RemoveOrgUnitJoinInfo(int userId, JoinUnit unit)
        {
            DateTime start = DateTime.ParseExact(unit.StartDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, Thread.CurrentThread.CurrentCulture);
            DateTime end = DateTime.ParseExact(unit.EndDate, Constants.FORMAT_DATE_HKT_YYYYMMDDHHMMSS, Thread.CurrentThread.CurrentCulture);
            List<OrgUnitJoinInfo> orgUnitJoinInfos = orgUnitJoinInfoService.GetOrgUnitJoinInfo(userId, unit.ShortCode, start, end);
            foreach (OrgUnitJoinInfo orgUnitJoinInfo in orgUnitJoinInfos)
            {
                orgUnitJoinInfo.Status = OrgUnitJoinInfoStatus.Deactive;
                orgUnitJoinInfoService.Update(orgUnitJoinInfo);
            }
        }

        private void AddHcmId(int userId, ref JoinUnit unit)
        {
            // Add HcmId
            DateTime startTime = DateTime.Parse(unit.StartDate);
            DateTime endTime = DateTime.Parse(unit.EndDate);
            List<CorporationHistory> corporationHistories = corporationHistoryService.GetByUserAndTime(userId, startTime, endTime).OrderBy(corp => corp.ExpirationDate).ToList();
            if (corporationHistories.Count == 0)
            {
                unit.HcmId = string.Format("{0,0:D8}", userId);

            }
            else if (corporationHistories.Count == 1)
            {
                unit.HcmId = corporationHistories[0].HcmPersionalId;
            }
            else // Nhiều pháp nhân
            {
                List<DateTime> expiredTimes = corporationHistories
                    .Where(corp => corp.ExpirationDate <= endTime)
                    .Where(corp => corp.ExpirationDate >= startTime)
                    .OrderBy(corp => corp.ExpirationDate)
                    .Select(corp => corp.ExpirationDate)
                    .ToList();
                if (expiredTimes.Count == 0)// Nếu đồng thời cùng ở nhiều pháp nhân
                {
                    unit.HcmId = corporationHistories[0].HcmPersionalId;
                }
                else
                {
                    DateTime oldStartTime = startTime.AddDays(-1);
                    foreach (DateTime exp in expiredTimes)
                    {
                        JoinUnit newProject = unit.ShallowCopy();
                        newProject.StartDate = oldStartTime.AddDays(1).ToString(Constants.FORMAT_DATE_YYYYMMDD);
                        newProject.EndDate = exp.ToString(Constants.FORMAT_DATE_YYYYMMDD);
                        newProject.HcmId = corporationHistories.Where(corp => corp.ExpirationDate == exp).First().HcmPersionalId;
                        oldStartTime = exp;
                    }
                    if (oldStartTime.AddDays(1) < endTime)
                    {
                        JoinUnit newProject = unit.ShallowCopy();
                        newProject.StartDate = oldStartTime.AddDays(1).ToString(Constants.FORMAT_DATE_YYYYMMDD);
                        newProject.EndDate = endTime.ToString(Constants.FORMAT_DATE_YYYYMMDD);
                        newProject.HcmId = corporationHistories.Last().HcmPersionalId;
                    }
                }
            }
        }

        private JoinCOMMechanism ComMechanismProcess(int userId, JoinCOMMechanism newCOMMechanism, JoinCOMMechanism oldCOMMechanism, DateTime startTime, DateTime endTime)
        {
            // Nếu đang có cơ chế chuyển thành không có cơ chế
            if (newCOMMechanism == null && oldCOMMechanism != null)
            {
                newCOMMechanism = new JoinCOMMechanism
                {
                    ICNUM = string.Empty,
                    BEGDA = DateTime.Now.ToString(Constants.FORMAT_DATE_YYYYMMDD),
                    ENDDA = DateTime.MaxValue.ToString(Constants.FORMAT_DATE_YYYYMMDD)
                };
            }
            // Nếu đang có cơ chế và không thay đổi
            else if (newCOMMechanism != null && oldCOMMechanism != null && newCOMMechanism.ICNUM == oldCOMMechanism.ICNUM)
            {
                newCOMMechanism = null;
            }
            // Nếu đổi cơ chế hoặc đang từ không có cơ chế sang có cơ chế 
            else if (newCOMMechanism != null)
            {
                // TÌm cơ chế đang chọn trong danh sách
                COMMechanismInput inputMechanism = new COMMechanismInput
                {
                    COMMechanismCode = newCOMMechanism.COMMechanismCode
                };
                Task<(List<COMMechanism> DataCOMList, HttpStatusCode StatusCOMList, string MessageCOMList)> process = HKTDataAPI.Connection.GetCOMMechanismList(inputMechanism, startTime, endTime);

                Task.WaitAll(process);

                if (process.Result.StatusCOMList == HttpStatusCode.OK && process.Result.DataCOMList?.Count > 0)
                {
                    newCOMMechanism.BEGDA = DateTime.Now.ToString(Constants.FORMAT_DATE_YYYYMMDD);
                    newCOMMechanism.ENDDA = process.Result.DataCOMList[0].ENDDA;
                }
                else
                {
                    newCOMMechanism = null;
                }
            }
            // Gán mã HCM ID cho cơ chế riêng
            CorporationHistory corporationHistory = corporationHistoryService.GetCorporationInDay(userId, DateTime.Now.Date);
            if (newCOMMechanism != null)
            {
                newCOMMechanism.HcmId = corporationHistory?.HcmPersionalId ?? string.Format("{0,0:D8}", userId);
            }
            return newCOMMechanism;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ApproveAllocationV2")]
        public IActionResult ApproveAllocationV2([FromBody] ConfirmAllocationRequestModel request, [FromHeader] string Authorization)
        {
            string[] allowTokens = AppSetting.Configuration.GetArrayValue<string>("FCM:AllowTokens");

            if (!allowTokens.Contains(Authorization))
            {
                return new ContentResult()
                {
                    Content = @"{""message"":""YOU_DONT_HAVE_PERMISSION"",""data"":null}",
                    StatusCode = 403,
                    ContentType = "application/json; charset=utf-8"
                };
            }

            int updateNumber = confirmAllocationManager.AdminConfirmV2(request.RecordId, ConfirmAllocationStatusV2.ApprovalReport, string.Empty);
            return updateNumber > 0 ? Ok() : BadRequest();
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("ApproveAllocation")]
        //public IActionResult ApproveAllocation([FromBody] ConfirmAllocationRequestModel request, [FromHeader] string Authorization)
        //{
        //    string[] allowTokens = AppSetting.Configuration.GetArrayValue<string>("FCM:AllowTokens");

        //    if (!allowTokens.Contains(Authorization))
        //    {
        //        return new ContentResult()
        //        {
        //            Content = @"{""message"":""YOU_DONT_HAVE_PERMISSION"",""data"":null}",
        //            StatusCode = 403,
        //            ContentType = "application/json; charset=utf-8"
        //        };
        //    }

        //    int updateNumber = confirmAllocationManager.AdminConfirm(request.RecordId, ConfirmAllocationStatus.ApprovalReport, string.Empty);
        //    return updateNumber > 0 ? Ok() : BadRequest();
        //}

        [HttpPost]
        [AllowAnonymous]
        [Route("RejectAllocationV2")]
        public IActionResult RejectAllocationV2([FromBody] ConfirmAllocationRequestModel request, [FromHeader] string Authorization)
        {
            string[] allowTokens = AppSetting.Configuration.GetArrayValue<string>("FCM:AllowTokens");

            if (!allowTokens.Contains(Authorization))
            {
                return new ContentResult()
                {
                    Content = @"{""message"":""YOU_DONT_HAVE_PERMISSION"",""data"":null}",
                    StatusCode = 403,
                    ContentType = "application/json; charset=utf-8"
                };
            }

            int updateNumber = confirmAllocationManager.AdminConfirmV2(request.RecordId, ConfirmAllocationStatusV2.RejectReport, request.RejectReason);
            return updateNumber > 0 ? Ok() : BadRequest();
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("RejectAllocation")]
        //public IActionResult RejectAllocation([FromBody] ConfirmAllocationRequestModel request, [FromHeader] string Authorization)
        //{
        //    string[] allowTokens = AppSetting.Configuration.GetArrayValue<string>("FCM:AllowTokens");

        //    if (!allowTokens.Contains(Authorization))
        //    {
        //        return new ContentResult()
        //        {
        //            Content = @"{""message"":""YOU_DONT_HAVE_PERMISSION"",""data"":null}",
        //            StatusCode = 403,
        //            ContentType = "application/json; charset=utf-8"
        //        };
        //    }

        //    int updateNumber = confirmAllocationManager.AdminConfirm(request.RecordId, ConfirmAllocationStatus.RejectReport, request.RejectReason);
        //    return updateNumber > 0 ? Ok() : BadRequest();
        //}

        [HttpGet]
        [Route("GetListLevel")]
        public async Task<IActionResult> GetListLevel([FromQuery] string language = "vi")
        {
            //(List<HrOperObject> objects, HttpStatusCode status, string message) = await HrOperAPI.Connection.GetListByCollumn(10);
            (List<KDSObject> objects, HttpStatusCode status, string message) = await HKTDataAPI.Connection.GetListLevelAsync();
            return status == HttpStatusCode.OK ? Ok(objects) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), HrOperAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("GetListTrack")]
        public IActionResult GetListTrack([FromQuery] int level, [FromQuery] int? parentId, [FromQuery] string language = "vi")
        {
            return Ok(confirmAllocationManager.GetCommonItems(level, parentId, ConfirmAllocationManager.TrackRoot));
        }

        [HttpGet]
        [Route("GetTrack")]
        public IActionResult GetTrack([FromQuery] int id, [FromQuery] string language = "vi")
        {
            return Ok(confirmAllocationManager.GetParent(id));
        }


        [HttpGet]
        [Route("Allocation")]
        public async Task<IActionResult> GetAllocationStatus([FromQuery] string language = "vi")
        {
            DateTime timeNow = DateTime.Now.Date;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptInDay(user.Id, DateTime.Now.Date);
            if (script.CorporationType == CorporationType.CorporationTH)
            {
                return Ok();
            }
            (AllocationStatus result, HttpStatusCode status, string message) = await AllocationApi.Connection.GetAllocationStatus(email);
            DayOfWeek[] allows = { DayOfWeek.Saturday, DayOfWeek.Sunday, DayOfWeek.Monday };
            Dictionary<DayOfWeek, int> times = new Dictionary<DayOfWeek, int>
            {
                {DayOfWeek.Tuesday, 15 },
                {DayOfWeek.Wednesday, 30 },
                { DayOfWeek.Thursday, 40 },
                {DayOfWeek.Friday, int.MaxValue },
            };
            Dictionary<DayOfWeek, string> types = new Dictionary<DayOfWeek, string>
            {
                {DayOfWeek.Tuesday, ReportType.Warning },
                {DayOfWeek.Wednesday, ReportType.Warning },
                {DayOfWeek.Thursday, ReportType.Warning },
                {DayOfWeek.Friday, ReportType.Error },
            };

            if (status == HttpStatusCode.OK && result.Success)
            {

                bool fakeAllocation = AppSetting.Configuration.GetValue<bool>("FakeAllocation");
                if (!fakeAllocation && (result.Result || allows.Contains(timeNow.DayOfWeek) || int.Parse(AppSetting.Configuration.GetValue("Allocation:CheckAllocation")) == 0))
                {
                    return Ok();
                }
                else
                {
                    int time = times[timeNow.DayOfWeek];
                    string allocationMessage = string.Empty;
                    switch (types[timeNow.DayOfWeek])
                    {
                        case ReportType.Warning:
                            allocationMessage = Constants.ALLOCATION_MESSAGE_ERROR;
                            break;
                        case ReportType.Error:
                            allocationMessage = Constants.ALLOCATION_MESSAGE_ERROR;
                            break;
                        default:
                            break;
                    }
                    return Ok(new AllocationStatusResponse
                    {
                        UserEmail = email,
                        Type = types[timeNow.DayOfWeek],
                        Times = time,
                        Title = Constants.ALLOCATION_TITLE,
                        Message = allocationMessage,
                        Contact = new ReportContact
                        {
                            Website = AppSetting.Configuration.GetValue("Allocation:Website"),
                            Mobile = AppSetting.Configuration.GetValue("Allocation:Mobile"),
                            Email = AppSetting.Configuration.GetValue("Allocation:Email"),
                        }
                    });
                }
            }
            else
            {
                return Ok(null, "Mất kết nối tới Allocation");
            }
        }

        [HttpGet]
        [Route("GetUnitRoles")]
        public IActionResult GetUnitRoles([FromQuery] string language = "vi")
        {
            return Ok(confirmAllocationManager.GetUnitRoles());
        }

        //[HttpGet]
        //[Route("ExportAllocationRecord")]
        //public IActionResult ExportAllocationRecord([FromQuery]string startTime, [FromQuery]string endTime, [FromQuery] string language = "vi")
        //{
        //    if (isExporting)
        //    {
        //        return BadRequest(ResultCode.IS_EXPORTING);
        //    }

        //    isExporting = true;

        //    CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
        //    DateTime startDate = DateTime.ParseExact(startTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date; ;
        //    DateTime endDate = DateTime.ParseExact(endTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date; ;


        //    Task<MemoryStream> stream = confirmAllocationManager.ExportAllocationRecord(startDate, endDate);
        //    var qguid = Guid.NewGuid().ToString();
        //    isExporting = false;
        //    return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qguid + ".xlsx");
        //}

        [HttpGet]
        [Route("DisableCheckinMobile")]
        public IActionResult DisableCheckinMobile([FromQuery] string language = "vi")
        {
            bool isDisable = AppSetting.Configuration.GetValue<bool>("DisableCheckinMobile:IsDisable");
            string message = string.Empty;
            if (isDisable)
            {
                string[] corporations = AppSetting.Configuration.GetArrayValue<string>("DisableCheckinMobile:Corporations");
                string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                User user = userService.GetByEmail(email);
                Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
                DateTime startDate = AppSetting.Configuration.GetValue<DateTime>("DisableCheckinMobile:StartDate");
                DateTime endDate = AppSetting.Configuration.GetValue<DateTime>("DisableCheckinMobile:EndDate");
                string start = string.Empty;
                string end = string.Empty;
                start = language == "vi" ? startDate.ToString(Constants.FORMAT_DATE_DDMM) : startDate.ToString(Constants.FORMAT_DATE_MMDDYYYY);
                end = language == "vi" ? endDate.ToString(Constants.FORMAT_DATE_DDMM) : endDate.ToString(Constants.FORMAT_DATE_MMDDYYYY);
                isDisable = corporations.Contains(script.CorporationType) && startDate <= DateTime.Now.Date && endDate >= DateTime.Now.Date;
                message = string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.DISABLE_CHECKIN_MOBILE_MESSAGE), start, end);
            }

            return Ok(isDisable, message);
        }


    }
}
