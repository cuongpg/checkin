﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Controllers
{
    [Authorize]
    [Route("api/groups")]
    public class GroupsController : BaseController
    {
        private readonly GroupService groupService;

        public GroupsController(ApplicationDbContext context)
        {
            groupService = new GroupService(context);
        }

        [HttpGet]
        public IActionResult GetAll(STRequestModel request)
        {
            return OkPagination(groupService.GetAll(out int numberOfPages, out int totalRecords, request), numberOfPages, totalRecords);
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAllNotPagination()
        {
            return Ok(groupService.GetAllNotPagination());
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var group = groupService.GetById(id);

            if (group == null)
            {
                return NotFound();
            }

            return Ok(group);
        }

        [HttpPost]
        public IActionResult Create([FromBody]Group group)
        {
            if (group == null)
            {
                return BadRequest();
            }

            return Ok(groupService.Create(group));
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]Group group)
        {
            if (group == null || group.Id != id)
            {
                return BadRequest();
            }

            var updated = groupService.Update(id, group);

            if (updated)
            {
                return Ok(updated);
            }

            return NotImplemented();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (groupService.Delete(id))
            {
                return new NoContentResult();
            }

            return NotImplemented();
        }
    }
}
