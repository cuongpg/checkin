﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.RequestModels;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Notification")]
    public class NotificationController : BaseController
    {
        private readonly NotificationManager notificationManager;
        private readonly IFireBaseMesageService notificationService;
        private readonly UserManager userManager;
        public NotificationController(IFireBaseMesageService notificationService, ApplicationDbContext context)
        {
            notificationManager = new NotificationManager(context);
            userManager = new UserManager(context);
            this.notificationService = notificationService;
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("CountWaittingSeeNotifications")]
        public IActionResult CountWaittingSeeNotifications()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            int number = notificationManager.CountNotificationsByStatusAndEmail(NotificationStatus.WAITING_SEE, email);
            return Ok(new
            {
                number
            });
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("CountWaittingReedNotifications")]
        public IActionResult CountWaittingReedNotifications()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            int number = notificationManager.CountNotificationsByStatusAndEmail(NotificationStatus.WAITING_READ, email);
            return Ok(new
            {
                number
            });
        }

        [HttpPost]
        [Route("InsertFirebaseToken")]
        public IActionResult InsertFirebaseToken([FromBody]NotificationRequestModel notification)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            int result = notificationManager.InsertFireBaseToken(email, notification.Token, notification.DeviceId);

            if (result > 0)
            {
                return Ok(notification.Token);
            }
            else
            {
                return LogicError();
            }
        }

        [HttpPost]
        [Route("DeleteFirebaseToken")]
        public IActionResult DeleteFirebaseToken([FromBody]NotificationRequestModel notification)
        {
            int result = notificationManager.DeleteFirebaseToken(notification.DeviceId);

            return Ok(notification.Token);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("PushNotification")]
        public IActionResult PushNotification([FromBody]NotificationRequestModel notificationInfo, [FromHeader] string Authorization)
        {
            string[] allowTokens = AppSetting.Configuration.GetArrayValue<string>("FCM:AllowTokens");

            if (!allowTokens.Contains(Authorization))
            {
                return new ContentResult()
                {
                    Content = @"{""message"":""YOU_DONT_HAVE_PERMISSION"",""data"":null}",
                    StatusCode = 403,
                    ContentType = "application/json; charset=utf-8"
                };
            }
            User user = userManager.GetUserByEmail(notificationInfo.Email);
            if(user == null)
            {
                return NotFound();
            }
            int result = notificationManager.PushNotification(
                notificationInfo.Email,
                notificationInfo.DocumentId,
                notificationInfo.Avatar,
                notificationInfo.DocumentType,
                notificationInfo.ParameterValues,
                user.Nationality);

            return result > 0 ? Ok(result) : LogicError();
        }
        
        // GET: api/<controller>/GetNotification
        [HttpGet]
        [Route("GetNotification")]
        public IActionResult GetNotification(STRequestModel request, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            List<Notification> notifications = notificationManager.GetNotification(out int numberOfPages, out int totalRecords, request, email, language);
            return OkPagination(notifications, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/UpdateStatus
        [HttpPost]
        [Route("UpdateStatus")]
        public IActionResult UpdateStatus([FromQuery] int id, [FromQuery] int status)
        {
            int result = notificationManager.UpdateStatus(id, status);
            if (result == 1)
            {
                return Ok(result);
            }
            else if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return BadRequest(result);
            }
        }

        // GET: api/<controller>/SeenNotifications
        [HttpPost]
        [Route("SeenNotifications")]
        public IActionResult SeenNotifications()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            int result = notificationManager.SeenStatus(email);
            return Ok(result);
        }

        // GET: api/<controller>/ReadNotification
        [HttpPost]
        [Route("ReadNotification")]
        public IActionResult ReadNotification([FromQuery] int id)
        {
            int result = notificationManager.UpdateStatus(id, NotificationStatus.WATCHED);
            if (result == 1)
            {
                return Ok(result);
            }
            else if (result == 0)
            {
                return NotFound();
            }
            else
            {
                return BadRequest(result);
            }
        }

    }
}