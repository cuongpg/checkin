﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Controllers
{
    [Authorize]
    [Route("api/emails")]
    public class EmailsController : BaseController
    {
        private readonly EmailTemplateService emailsService;

        public EmailsController(ApplicationDbContext context)
        {
            emailsService = new EmailTemplateService(context);
        }

        [HttpGet("get-all")]
        public IActionResult GetAllTemplate(STRequestModel request)
        {
            return OkPagination(emailsService.GetAllEmailTemplate(out int numberOfPages, out int totalRecords, request), numberOfPages, totalRecords);
        }

        [HttpPost("delete/{id}")]
        public IActionResult Delete(int id)
        {
            emailsService.Delete(id);
            return Ok(id);
        }

        [HttpGet("{id}")]
        public IActionResult GetTemplateById(int id)
        {
            return Ok(emailsService.GetTemplateById(id));
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody]EmailTemplate emailTemplates)
        {
            if (emailTemplates == null)
            {
                return BadRequest();
            }
            emailTemplates = emailsService.Update(emailTemplates);
            return Ok(emailTemplates.Id);
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody]EmailTemplate emailTemplates)
        {
            if (emailTemplates == null)
            {
                return BadRequest();
            }
            return Ok(emailsService.Create(emailTemplates));
        }
    }
}
