﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Controllers
{
    [Authorize]
    [Route("api/notifications")]
    public class NotificationTemplateController : BaseController
    {
        private readonly NotificationTemplateService notificationTemplateService; 

        public NotificationTemplateController(ApplicationDbContext context)
        {
            notificationTemplateService = new NotificationTemplateService(context);
        }

        [HttpGet("get-all")]
        public IActionResult GetAllNotificationTemplate(STRequestModel request)
        {
            return OkPagination(notificationTemplateService.GetAllNotificationTemplate(out int numberOfPages, out int totalRecords, request), numberOfPages, totalRecords);
        }

        [HttpPost("delete/{id}")]
        public IActionResult Delete(int id)
        {
            notificationTemplateService.Delete(id);
            return Ok(id);
        }

        [HttpGet("{id}")]
        public IActionResult GetTemplateById(int id)
        {
            return Ok(notificationTemplateService.GetTemplateById(id));
            
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody]NotificationTemplate notificationTemplate)
        {
            if (notificationTemplate == null)
            {
                return BadRequest();
            }
            notificationTemplate = notificationTemplateService.Update(notificationTemplate);
            return Ok(notificationTemplate.Id);
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody]NotificationTemplate notificationTemplate)
        {
            if (notificationTemplate == null)
            {
                return BadRequest();
            }
            return Ok(notificationTemplateService.Create(notificationTemplate));
        }
    }
}
