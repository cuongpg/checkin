﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Implements;
using TOCO.Models;

namespace TOCO.Controllers
{
    [Authorize]
    [Route("api/routes")]
    public class RoutesController : BaseController
    {
        private readonly RouteService routeService;

        public RoutesController(ApplicationDbContext context)
        {
            routeService = new RouteService(context);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAll(string query)
        {
            return Ok(routeService.GetAll(query));
        }
    }
}
