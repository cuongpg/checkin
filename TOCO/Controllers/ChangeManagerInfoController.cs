﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Common;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.ResultModels.WisamiResultModels;


namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/ChangeManagerInfo")]
    public class ChangeManagerInfoController : BaseController
    {

        private readonly UserManager userManager;
        private readonly ChangeManagerInfoManager changeManagerInfoManager;
        private readonly ApplicationDbContext context;

        public ChangeManagerInfoController(ApplicationDbContext context)
        {
            this.context = context;
            userManager = new UserManager(context);
            changeManagerInfoManager = new ChangeManagerInfoManager(context);
        }

        [HttpGet]
        [Route("GetAllManager")]
        public IActionResult GetAllManager([FromQuery]int page = 0, [FromQuery]int perPage = 5, [FromQuery] string keyWord = "")
        {

            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            keyWord = string.IsNullOrEmpty(keyWord) ? "" : keyWord;
            int groupId = Constants.User.Groups.Manager;
            List<UserResult> data = userManager.GetAllManager(groupId, email, page, perPage, keyWord, out int totalPages);
            return OkPagination(data, page, perPage, totalPages);
        }

        [HttpPost]
        [Route("ChangeManager")]
        public IActionResult ChangeManager([FromQuery]string newManagerEmail)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    User currentManager = userManager.GetUserById(user.HeadOfOrgUnitId is null ? user.ManagerId : (int)user.HeadOfOrgUnitId);
                    User newManager = userManager.GetUserByEmail(newManagerEmail);

                    bool bOk = changeManagerInfoManager.ChangeManager(user, currentManager, newManager, true);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_CHANGE_MANAGER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }




        [HttpGet]
        [Route("GetMyChangeManagerInfo")]
        public IActionResult GetMyChangeManagerInfo(STRequestModel request, [FromQuery] int? employeeId, [FromQuery] string language)
        {
            User user = null;
            if (employeeId is null)
            {
                string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                user = userManager.GetUserByEmail(email);
            }
            else
            {
                user = userManager.GetUserById((int)employeeId);
            }
            List<ChangeManagerResult> changeManagerInfos = changeManagerInfoManager.GetMyChangeManagerInfo(user, out int numberOfPages, out int totalRecords, request);
            return OkPagination(changeManagerInfos, numberOfPages, totalRecords);
        }

        [HttpGet]
        [Route("GetChangeManagerInfoToApprove")]
        public IActionResult GetChangeManagerInfoToApprove(STRequestModel request, [FromQuery] int? managerId, [FromQuery] string language)
        {
            User manager = null;
            if (managerId is null)
            {
                string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                manager = userManager.GetUserByEmail(email);
            }
            else
            {
                manager = userManager.GetUserById((int)managerId);
            }
            List<ChangeManagerResult> changeManagerInfos = changeManagerInfoManager.GetChangeManagerInfoToApprove(manager, out int numberOfPages, out int totalRecords, request);
            return OkPagination(changeManagerInfos, numberOfPages, totalRecords);
        }


        [HttpPost]
        [Route("ConfirmChangeManager")]
        public IActionResult ConfirmChangeManager([FromQuery]int id, [FromQuery] bool isConfirm, [FromQuery]string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User manager = userManager.GetUserByEmail(email);
                    StatusChangeManagerInfo confirm = isConfirm ? StatusChangeManagerInfo.Approved : StatusChangeManagerInfo.Rejected;
                    (bool bOk, string message, ChangeManagerInfo records) = changeManagerInfoManager.ConfirmChangeManager(confirm, id, manager, rejectReason);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_CHANGE_MANAGER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpPost]
        [Route("AdminConfirmChangeManager")]
        public IActionResult AdminConfirmChangeManager([FromQuery]int id, [FromQuery] bool isConfirm, [FromQuery] int managerId, [FromQuery]string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User manager = userManager.GetUserById(managerId);
                    StatusChangeManagerInfo confirm = isConfirm ? StatusChangeManagerInfo.Approved : StatusChangeManagerInfo.Rejected;
                    (bool bOk, string message, ChangeManagerInfo records) = changeManagerInfoManager.ConfirmChangeManager(confirm, id, manager, rejectReason);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_CHANGE_MANAGER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }


        [HttpPost]
        [Route("DestroyChangeManager")]
        public IActionResult DestroyChangeManager([FromQuery]int id)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);

                    (bool bOk, string message, ChangeManagerInfo records) = changeManagerInfoManager.DestroyChangeManager(id, user);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_CHANGE_MANAGER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpGet]
        [Route("GetChangeManagerInfos")]
        public IActionResult GetChangeManagerInfos(STRequestModel request, [FromQuery] string nationality, [FromQuery] int? employeeId, [FromQuery] string language)
        {
            User user = null;
            if (!(employeeId is null))
            {
                user = userManager.GetUserById((int)employeeId);
            }
            List<ChangeManagerResult> changeManagerInfos = changeManagerInfoManager.GetChangeManagerInfos(nationality, user, out int numberOfPages, out int totalRecords, request);
            return OkPagination(changeManagerInfos, numberOfPages, totalRecords);
        }

    }
}
