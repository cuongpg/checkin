﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net;
using TOCO.Models;

namespace TOCO.Controllers
{
    public class BaseController : Controller
    {
        protected static bool isExporting = false;

        protected ObjectResult Ok(object data = null, string message = "Ok")
        {
            return StatusCode(200, GetResultModel(data, message));
        }

        protected ObjectResult OkPagination(object data, int numberOfPages = 1, int totalRecords = 0)
        {
            return Ok(new STResponseModel
            {
                Data = data,
                NumberOfPages = numberOfPages,
                TotalRecords = totalRecords
            });
        }

        protected ObjectResult OkPagination(object data, int page = 1, int perPage = 10, int totalPages = 0)
        {
            var response = new
            {
                Page = page,
                PerPage = perPage,
                TotalPages = totalPages,
                Data = data,
            };
            return Ok(response);
        }

        protected ObjectResult LogicError(string message = "Logic error", object data = null)
        {
            return StatusCode(422, GetResultModel(data, message));
        }

        protected ObjectResult BadRequest(string message = "Bad Request", object data = null)
        {
            return StatusCode(400, GetResultModel(data, message));
        }

        protected ObjectResult Unauthorized(string message = "Unauthorized", object data = null)
        {
            return StatusCode(401, GetResultModel(data, message));
        }

        protected ObjectResult Forbidden(string message = "Forbidden", object data = null)
        {
            return StatusCode(403, GetResultModel(data, message));
        }

        protected ObjectResult NotFound(string message = "Not Found", object data = null)
        {
            return StatusCode(404, GetResultModel(data, message));
        }

        protected ObjectResult InternalServerError(string message = "Internal Server Error", object data = null)
        {
            return StatusCode(500, GetResultModel(data, message));
        }

        protected ObjectResult NotImplemented(string message = "Not Implemented", object data = null)
        {
            return StatusCode(501, GetResultModel(data, message));
        }

        protected ResultModel GetResultModel(object data, string message)
        {
            return new ResultModel
            {
                Message = message,
                Data = data
            };
        }

        protected JsonResult OkJsonIgnoreNullValue(object data = null, int totalRecords = 0, string message = "Ok")
        {
            return new JsonResult(new ResultAPIModel
            {
                Message = message,
                Total = totalRecords,
                Data = data
            }, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }
    }
}
