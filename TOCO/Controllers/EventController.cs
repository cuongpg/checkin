﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Event")]
    public class EventController : BaseController
    {
        private readonly EventManager eventManager;
        private readonly UserManager userManager;
        private readonly ScriptManager scriptManager;

        public EventController(ApplicationDbContext context)
        {
            eventManager = new EventManager(context);
            userManager = new UserManager(context);
            scriptManager = new ScriptManager(context);
        }

        [HttpGet]
        [Route("GetGroupEvents")]
        public IActionResult GetGroupEvents([FromQuery] string email, [FromQuery] string language = "vi")
        {
            
            email = email is null || email == "" ? User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value : email;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            List<GroupEventResult> groupEvents = eventManager.GetGroupEvents(language, script.CorporationType, script.HasOverTime);
            return Ok(groupEvents);
        }

        [HttpGet]
        [Route("GetOTQuotasGroupEvents")]
        public IActionResult GetOTQuotasGroupEvents([FromQuery] int employeeCode, [FromQuery] string language = "vi")
        {
            User user = userManager.GetUserById(employeeCode);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            List<GroupEventResult> groupEvents = eventManager.GetOTQuotasGroupEvents(language, script.CorporationType);
            return Ok(groupEvents);
        }


        [HttpGet]
        [Route("GetEvents")]
        public IActionResult GetEvents([FromQuery] string email, [FromQuery] int? groupId, [FromQuery]int page = 0, [FromQuery]int perPage = 10, [FromQuery] string keyWord = "", [FromQuery]string language = "vi")
        {
            email = email is null || email == "" ? User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value : email;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            keyWord = string.IsNullOrEmpty(keyWord) ? "" : keyWord;
            List<EventResult> data = eventManager.GetEventsByEmployeeTypeAndGroup(groupId, language, script.EmployeeTypeId, page, perPage, keyWord, out int totalPages);
            return OkPagination(data, page, perPage, totalPages);
        }
    }
}
