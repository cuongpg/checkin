﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.API;
using TOCO.Common;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.APIModels;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Salary")]
    public class SalaryController : BaseController
    {
        private readonly ListmasterManager dm4cManager;
        private readonly UserManager userManager;
        public SalaryController(ApplicationDbContext context)
        {
            userManager = new UserManager(context);
            dm4cManager = new ListmasterManager(context);
        }

        [HttpGet]
        [Route("Corporations")]
        public IActionResult GetCorporations([FromQuery]int page = 0, [FromQuery]int perPage = 10)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<CorporationResult> items = dm4cManager.GetCorporations(out int numberOdPages, out int totalRecords, page, perPage);

            return OkPagination(new { items, user.Corporation}, numberOdPages, totalRecords);
        }

        [HttpGet]
        [Route("List")]
        public async Task<IActionResult> GetIncomeInformationAsync([FromQuery]int? month, [FromQuery]int? year, [FromQuery]string corporation, [FromQuery]int page = 0, [FromQuery]int perPage = 10, [FromQuery] string language = "vi")
        {
            page++;
            string employeeCode = User.Claims.FirstOrDefault(c => c.Type == Constants.EMPLOYEE_CODE)?.Value;
            User user = userManager.GetUserById(int.Parse(employeeCode));
            (List<IncomeInforResult> items, HttpStatusCode status, string message, int totalPages) = await Tn18DataAPI.Connection.GetIncomeInformationAsync(employeeCode, month, year, corporation, page, perPage);
            page--;
            return status == HttpStatusCode.OK ? OkPagination(items, page, perPage, totalPages) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), OsscarAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("FTTInfomation")]
        public async Task<IActionResult> GetFTTInformationAsync([FromQuery]int month, [FromQuery]int year, [FromQuery]string language = "vi")
        {
            string employeeCode = User.Claims.FirstOrDefault(c => c.Type == Constants.EMPLOYEE_CODE)?.Value;
            User user = userManager.GetUserById(int.Parse(employeeCode));
            (FTTInfoResult data, HttpStatusCode status, string message) = await Tn18DataAPI.Connection.GetFTTInformationAsync(employeeCode, month, year);
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), OsscarAPI.Connection.systemName));
        }

        [HttpGet]
        [Route("Detail")]
        public async Task<IActionResult> GetSalaryInformationAsync([FromQuery]int month, [FromQuery]int year, [FromQuery]string language = "vi")
        {
            string employeeCode = User.Claims.FirstOrDefault(c => c.Type == Constants.EMPLOYEE_CODE)?.Value;
            User user = userManager.GetUserById(int.Parse(employeeCode));
            (Dictionary<string, Dictionary<string, string>> data, HttpStatusCode status, string message) = await Tn18DataAPI.Connection.GetSalaryInformationAsync(employeeCode, month, year, language);
            return status == HttpStatusCode.OK ? Ok(data) : InternalServerError(string.Format(MessageManager.GetMessageManager(language).GetValue(ResultCode.FORMAT_DISCONNECT), OsscarAPI.Connection.systemName));
        }
    }
}