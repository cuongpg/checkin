﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Script")]
    public class ScriptController : BaseController
    {
        private readonly ScriptManager scriptManager;
        private readonly UserManager userManager;
        private readonly HrbHistoryService hrbHistoryService;
        private readonly ApplicationDbContext context;

        public ScriptController(ApplicationDbContext context)
        {
            this.context = context;
            scriptManager = new ScriptManager(context);
            userManager = new UserManager(context);
            hrbHistoryService = new HrbHistoryService(context);
        }

        [HttpGet]
        [Route("GetCutoffTimeKeepingDay")]
        public IActionResult GetCutoffTimeKeepingDay()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            return Ok(script.CutoffTimeKeepingDay);
        }

        [HttpGet]
        [Route("GetMinDate")]
        public IActionResult GetMinDate()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            DateTime startCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
            DateTime startLastMonth = startCurrentMonth.AddMonths(-1).Date;
            return Ok((DateTime.Now.Day < script.CutoffTimeKeepingDay ? startLastMonth : startCurrentMonth)
                .Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds);
        }

        [HttpGet]
        [Route("GetScriptsInMonth/{month}/{year}")]
        public IActionResult GetScriptInMonth(int month, int year)
        {
            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;

            User user = userManager.GetUserByEmail(email);
            List<Script> scripts = scriptManager.GetScriptByUserAndTime(user.Id, startDate, endDate);
            if (scripts.Count == 0)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            return Ok(scripts);
        }

        [HttpGet]
        [Route("GetScriptsOfStaffInMonth/{month}/{year}/{email}")]
        public IActionResult GetScriptsOfStaffInMonth(int month, int year, string email, [FromQuery] string language = "vi")
        {
            string emailManager = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            if (month > 12 || month < 1 || year < 1 || !userManager.CheckIsManagerForStaff(email, emailManager))
            {
                return BadRequest();
            }
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;

            User user = userManager.GetUserByEmail(email);
            List<Script> scripts = scriptManager.GetScriptByUserAndTime(user.Id, startDate, endDate);
            if (scripts.Count == 0)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            return Ok(scripts);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<Script> scripts = scriptManager.GetAll();
            return Ok(scripts);
        }

        [HttpPost]
        [Route("UpdateScript")]
        public IActionResult UpdateScript([FromQuery]int scriptId, string effectiveDate, int userId)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    bool bOk = scriptManager.UpdateScript(userId, scriptId, effectiveDate);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_UPDATE_SCRIPT);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpGet]
        [Route("GetScriptHistoryNow")]
        public IActionResult GetScriptHistoryNow([FromQuery]int userId)
        {
            User user = userManager.GetUserById(userId);
            ScriptHistory scriptHistory = scriptManager.GetScriptHistoryInDay(user.Id, DateTime.Now.Date);
            return Ok(scriptHistory);
        }

        [HttpGet]
        [Route("GetHrbHistoryNow")]
        public IActionResult GetHrbHistoryNow([FromQuery]int userId)
        {
            User user = userManager.GetUserById(userId);
            HrbHistory hrbHistory = hrbHistoryService.GetHrbInDay(user.Id, DateTime.Now.Date);
            return Ok(hrbHistory);
        }

        [HttpGet]
        [Route("GetScriptsOfEmployeeInMonth/{month}/{year}/{email}")]
        public IActionResult GetScriptsOfEmployeeInMonth(int month, int year, string email, [FromQuery] string language = "vi")
        {
            string emailManager = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;

            User user = userManager.GetUserByEmail(email);
            List<Script> scripts = scriptManager.GetScriptByUserAndTime(user.Id, startDate, endDate);
            if (scripts.Count == 0)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            return Ok(scripts);
        }


    }
}
