﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using TOCO.Common;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Controllers
{

    [Authorize]
    [Produces("application/json")]
    [Route("api/ReasonType")]
    public class ReasonTypeController : BaseController
    {
        private readonly IConfiguration configuration;
        private readonly ReasonTypeService reasonTypeService;
        private readonly UserManager userManager;
        private readonly ScriptManager scriptManager;
        private readonly ApprovalRecordManager approvalRecordManager;
        private readonly IHttpContextAccessor accessor;
        private readonly IDistributedCache cache;

        public ReasonTypeController(ApplicationDbContext context, IConfiguration configuration,
            IHttpContextAccessor accessor, IDistributedCache cache)
        {
            reasonTypeService = new ReasonTypeService(context);
            userManager = new UserManager(context);
            scriptManager = new ScriptManager(context);
            approvalRecordManager = new ApprovalRecordManager(context);
            this.configuration = configuration;
            this.accessor = accessor;
            this.cache = cache;
        }

        //GET: api/<controller>/HasBufferReasons
        [HttpGet]
        [Route("HasBufferReasons")]
        public IActionResult GetHasBufferReasons([FromQuery]int? id, [FromQuery]string language = "vi")
        {
            User user = id.HasValue ? userManager.GetUserById(id.Value) : null;
            Script script = id.HasValue ? scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date) : null;
            List<ReasonType> reasonTypes = reasonTypeService.GetHasBufferReasonTypes();
            List<ReasonTypeResult> result = new List<ReasonTypeResult>();

            foreach (ReasonType reasonType in reasonTypes)
            {
                List<(double Number, DateTime ExpirationDate, int GenerationYear)> buffer = id.HasValue ? approvalRecordManager.GetBufferLeave(user, reasonType, script, DateTime.Now, DateTime.Now).ListBuffer : new List<(double Number, DateTime ExpirationDate, int GenerationYear)>();
                List<(double Number, string ExpirationDate, int GenerationYear)> data = new List<(double Number, string ExpirationDate, int GenerationYear)>();
                foreach ((double number, DateTime expirationDate, int generationYear) in buffer)
                {
                    data.Add((number, expirationDate.ToString(Constants.FORMAT_DATE_DDMMYYYY), generationYear));
                }
                result.Add(new ReasonTypeResult()
                {
                    Id = reasonType.Id,
                    Name = MessageManager.GetMessageManager(language).GetValue(reasonType.Name),
                    HasPaidLeave = reasonType.HasPaidLeave,
                    Buffer = data,
                    NeedAdminConfirm = reasonType.NeedAdminConfirm
                });
            }
            return Ok(result);
        }

        //GET: api/<controller>/LeaveReasons
        [HttpGet]
        [Route("LeaveReasons")]
        public IActionResult GetLeaveReasons([FromQuery] string startDate, [FromQuery] string endDate, [FromQuery]string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            return GetLeaveReasons(email, startDate, endDate, language);
        }

        //GET: api/<controller>/HasQuotasReasonsByUser
        [HttpGet]
        [Route("HasQuotasReasonsByUser")]
        public IActionResult GetHasQuotasReasonsByUser([FromQuery] int employeeCode, [FromQuery]string language = "vi")
        {
            User user = userManager.GetUserById(employeeCode);
            Script script = scriptManager.GetCurrentScriptByUser(employeeCode, DateTime.Now.Date);
            List<ReasonType> reasonTypes = reasonTypeService.GetHasQuotasReasonsByUser(script.CorporationType);
            List<ReasonTypeResult> result = new List<ReasonTypeResult>();
            foreach (ReasonType reasonType in reasonTypes)
            {
                List<(double Number, DateTime ExpirationDate, int GenerationYear)> buffer = approvalRecordManager.GetBufferLeave(user, reasonType, script, DateTime.Now, DateTime.Now).ListBuffer;
                List<(double Number, string ExpirationDate, int GenerationYear)> data = new List<(double Number, string ExpirationDate, int GenerationYear)>();
                foreach ((double number, DateTime expirationDate, int generationYear) in buffer)
                {
                    data.Add((number, expirationDate.ToString(Constants.FORMAT_DATE_DDMMYYYY), generationYear));
                }
                result.Add(new ReasonTypeResult
                {
                    Id = reasonType.Id,
                    Name = MessageManager.GetMessageManager(language).GetValue(reasonType.Name),
                    HasPaidLeave = reasonType.HasPaidLeave,
                    Buffer = data,
                    NeedAdminConfirm = reasonType.NeedAdminConfirm,
                });
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("LeaveUserReasons")]
        public IActionResult GetLeaveUserReasons([FromQuery] string email, [FromQuery] string startDate, [FromQuery] string endDate, [FromQuery]string language = "vi")
        {
            return GetLeaveReasons(email, startDate, endDate, language);
        }

        private IActionResult GetLeaveReasons(string email, string startDate, string endDate, string language)
        {
            User user = userManager.GetUserByEmail(email);
            DateTime start = DateTime.Parse(startDate).Date;
            DateTime end = DateTime.Parse(endDate).Date;
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, end.Date);
            List<ReasonType> reasonTypes = reasonTypeService.GetReasonTypes(script.HasPaidLeave, script.CorporationType);
            List<ReasonTypeResult> result = new List<ReasonTypeResult>();
            // Add parent 
            foreach (ReasonType reasonType in reasonTypes.Where(r => r.ParentId == r.Id))
            {
                List<(double Number, DateTime ExpirationDate, int GenerationYear)> buffer = approvalRecordManager.GetBufferLeave(user, reasonType, script, start, end).ListBuffer;
                List<(double Number, string ExpirationDate, int GenerationYear)> data = new List<(double Number, string ExpirationDate, int GenerationYear)>();
                foreach ((double number, DateTime expirationDate, int generationYear) in buffer)
                {
                    data.Add((number, expirationDate.ToString(Constants.FORMAT_DATE_DDMMYYYY), generationYear));
                }
                result.Add(new ReasonTypeResult()
                {
                    Id = reasonType.Id,
                    Name = MessageManager.GetMessageManager(language).GetValue(reasonType.Name),
                    HasPaidLeave = reasonType.HasPaidLeave,
                    Buffer = data,
                    NeedAdminConfirm = reasonType.NeedAdminConfirm,
                    Childs = new List<ReasonTypeResult>()
                });
            }
            // Add childs
            foreach (ReasonType reasonType in reasonTypes.Where(r => r.ParentId != r.Id))
            {
                ReasonTypeResult parent = result.Where(r => r.Id == reasonType.ParentId).FirstOrDefault();
                List<(double Number, DateTime ExpirationDate, int GenerationYear)> buffer = approvalRecordManager.GetBufferLeave(user, reasonType, script, start, end).ListBuffer;
                List<(double Number, string ExpirationDate, int GenerationYear)> data = new List<(double Number, string ExpirationDate, int GenerationYear)>();
                foreach ((double number, DateTime expirationDate, int generationYear) in buffer)
                {
                    data.Add((number, expirationDate.ToString(Constants.FORMAT_DATE_DDMMYYYY), generationYear));
                }
                parent.Childs.Add(new ReasonTypeResult()
                {
                    Id = reasonType.Id,
                    Name = MessageManager.GetMessageManager(language).GetValue(reasonType.Name),
                    HasPaidLeave = reasonType.HasPaidLeave,
                    NeedAdminConfirm = reasonType.NeedAdminConfirm,
                    Buffer = data
                });
            }

            return Ok(result);
        }

    }
}