﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;


namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Hcm")]
    public class HcmController : BaseController
    {
        private readonly HcmManager hcmManager;
        private readonly LeaveDateService leaveDateService;
        private readonly SendHCMDataHistoryService sendHCMDataHistoryService;

        public HcmController(ApplicationDbContext context)
        {
            hcmManager = new HcmManager(context);
            sendHCMDataHistoryService = new SendHCMDataHistoryService(context);
            leaveDateService = new LeaveDateService(context);
        }

        private FileStreamResult Export(string corporationType, string startDate, string endDate, ExportFunction exportFunction)
        {
            DateTime start = DateTime.Parse(startDate).Date;
            DateTime end = DateTime.Parse(endDate).Date;
            string temp_path = exportFunction(corporationType, DateTime.Now, start, end);

            var memory = new MemoryStream();
            using (var stream = new FileStream(temp_path, FileMode.Open))
            {
                stream.CopyTo(memory);
            }
            memory.Position = 0;

            var qguid = Guid.NewGuid().ToString();
            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qguid + ".csv");
        }

        [HttpGet]
        [Route("ExportPlannedWorkingTime")]
        public IActionResult ExportPlannedWorkingTime(string corporationType, string startDate, string endDate)
        {
            return Export(corporationType, startDate, endDate, hcmManager.ExportPlannedWorkingTimeEveryDay);
        }

        [HttpGet]
        [Route("ExportAbsences")]
        public IActionResult ExportAbsences(string corporationType, string startDate, string endDate)
        {
            return Export(corporationType, startDate, endDate, hcmManager.ExportAbsencesEveryMonth);
        }

        [HttpGet]
        [Route("ExportAbsencesQuatas")]
        public IActionResult ExportAbsencesQuatas(string corporationType, string startDate, string endDate)
        {
            return Export(corporationType, startDate, endDate, hcmManager.ExportAbsencesQuotasEveryDay);
        }

        [HttpGet]
        [Route("ExportOvertime")]
        public IActionResult ExportOvertime(string corporationType, string startDate, string endDate)
        {
            return Export(corporationType, startDate, endDate, hcmManager.ExportOvertimeEveryMonth);
        }

        [HttpGet]
        [Route("ExportSubstituons")]
        public IActionResult ExportSubstituons(string startDate, string endDate)
        {
            DateTime start = DateTime.Parse(startDate).Date;
            DateTime end = DateTime.Parse(endDate).Date;
            Task<MemoryStream> stream = hcmManager.ExportSubstituons(start, end);
            var qguid = Guid.NewGuid().ToString();
            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qguid + ".csv");
        }

        [HttpGet]
        [Route("ExportTimeKeepings")]
        public IActionResult ExportTimeKeepings(string corporationType, string startDate, string endDate)
        {
            return Export(corporationType, startDate, endDate, hcmManager.ExportTimeKeepingsEveryMonth);
        }

        [HttpGet]
        [Route("ExportShiftEats")]
        public IActionResult ExportShiftEats(string corporationType, string startDate, string endDate)
        {
            return Export(corporationType, startDate, endDate, hcmManager.ExportShiftEatsEveryMonth);
        }

        [HttpGet]
        [Route("UploadToHCM")]
        public IActionResult UploadToHCM(string infoType, string corporationType, string startDate, string endDate)
        {
            DateTime start = DateTime.Parse(startDate).Date;
            DateTime end = DateTime.Parse(endDate).Date;
            ExportHCMService exportHCMService = new ExportHCMService();
            ExportFunction exportFunction = null;
            switch (infoType)
            {
                case HcmManager.IT0007:
                    exportFunction = hcmManager.ExportPlannedWorkingTimeEveryDay;
                    break;
                case HcmManager.IT0015:
                    exportFunction = hcmManager.ExportShiftEatsEveryMonth;
                    break;
                case HcmManager.IT0267:
                    string calendarType = corporationType == CorporationType.CorporationVI ? LeaveCalendarType.VI : LeaveCalendarType.TH;
                    while (leaveDateService.GetByDateAndType(end, calendarType) != null)
                    {
                        end = end.AddDays(-1);
                    }
                    exportFunction = hcmManager.ExportTimeKeepingsEveryMonth;
                    break;
                case HcmManager.IT2001:
                    exportFunction = hcmManager.ExportAbsencesEveryMonth;
                    break;
                case HcmManager.IT2003:
                    //TODO
                    break;
                case HcmManager.IT2006:
                    exportFunction = hcmManager.ExportAbsencesQuotasEveryDay;
                    break;
                case HcmManager.IT2010:
                    exportFunction = hcmManager.ExportOvertimeEveryMonth;
                    break;
                default:
                    break;
            }
            var data = sendHCMDataHistoryService.Create(exportHCMService.ExportHCMData(CorporationType.CorporationVI, HcmManager.IT0007, DateTime.Now, start, end, exportFunction));
            return Ok(data);
        }
    }
}