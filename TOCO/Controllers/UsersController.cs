﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels.WisamiResultModels;

namespace TOCO.Controllers
{

    [Authorize]
    [Route("api/users")]
    public class UsersController : BaseController
    {
        private readonly UserManager userManager;
        private readonly ChangeManagerInfoManager changeManagerInfoManager;
        private readonly ScriptManager scriptManager;
        private readonly ApplicationDbContext context;
        private readonly ScriptHistoryService scriptHistoryService;
        private readonly LeaveCalendarTypeHistoryService leaveCalendarTypeHistoryService;
        private readonly HrbHistoryService hrbHistoryService;

        public UsersController(ApplicationDbContext context)
        {
            this.context = context;
            userManager = new UserManager(context);
            changeManagerInfoManager = new ChangeManagerInfoManager(context);
            scriptManager = new ScriptManager(context);
            scriptHistoryService = new ScriptHistoryService(context);
            leaveCalendarTypeHistoryService = new LeaveCalendarTypeHistoryService(context);
            hrbHistoryService = new HrbHistoryService(context);
        }

        [HttpGet]
        [Route("GetAllEmployee")]
        public IActionResult GetAllEmployee([FromQuery]int page = 0, [FromQuery]int perPage = 5, [FromQuery] string keyWord = "")
        {

            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            keyWord = string.IsNullOrEmpty(keyWord) ? "" : keyWord;
            int groupId = Constants.User.Groups.Employee;
            List<UserResult> data = userManager.GetAllManager(groupId, email, page, perPage, keyWord, out int totalPages);
            return OkPagination(data, page, perPage, totalPages);
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("GetUsersByType")]
        public IActionResult GetAll(int? type, int scriptId, STRequestModel request)
        {
            List<User> users = userManager.GetAll(type, scriptId, out int numberOfPages, out int totalRecords, request);
            return OkPagination(users, numberOfPages, totalRecords);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAll(bool? isActive, int scriptId, STRequestModel request)
        {
            List<User> users = userManager.GetAll(isActive, scriptId, out int numberOfPages, out int totalRecords, request);
            return OkPagination(users, numberOfPages, totalRecords);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = userManager.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }
            LeaveCalendarTypeHistory leaveCalendarTypeHistory = leaveCalendarTypeHistoryService.GetLeaveCalendarTypeHistoryInDay(user.Id, DateTime.Now.Date);
            dynamic userResult = new ExpandoObject();
            var dictionary = (IDictionary<string, object>)userResult;
            foreach (var property in user.GetType().GetProperties())
                dictionary.Add(property.Name, property.GetValue(user));
            userResult.CalendarType = leaveCalendarTypeHistory?.Code??string.Empty;
            return OkJsonIgnoreNullValue(userResult);
        }

        // GET api/<controller>/5
        [HttpGet("WorkProgress/{id}")]
        public IActionResult GetWorkProgressById(int id)
        {
            var user = userManager.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }

            List<WorkProgressResult> listWorkProgress = userManager.GetWorkProgressById(id);

            return Ok(listWorkProgress);
        }

        [HttpPost]
        [Route("UpdateLeaveCalendarType")]
        public IActionResult UpdateLeaveCalendarType([FromQuery]string calendarType, string effectiveDate, int userId)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    bool bOk = userManager.UpdateLeaveCalendarType( calendarType,  effectiveDate,  userId);
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_UPDATE_SCRIPT);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        //POST api/<controller>
        [HttpPost]
        public IActionResult Create([FromBody]UserRequestModel userRequest)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    if (userRequest == null)
                    {
                        return BadRequest();
                    }
                    User user = STHelper.ConvertObject<User, UserRequestModel>(userRequest);
                    DateTime activeDate = STHelper.ConvertDateTimeToLocal(userRequest.ActiveDate).Value.Date;
                    bool bOk = userManager.Create(user);
                    if (bOk)
                    {
                        bOk = userManager.CreateUserGroups(userRequest.Groups, user);
                    }
                    if (bOk)
                    {
                        LeaveCalendarTypeHistory leaveCalendarTypeHistory = new LeaveCalendarTypeHistory
                        {
                            UserId = user.Id,
                            Code = userRequest.CalendarType,
                            EffectiveDate = activeDate,
                            ExpirationDate = DateTime.MaxValue
                        };

                        ScriptHistory scriptHistory = new ScriptHistory
                        {
                            UserId = user.Id,
                            ScriptId = userRequest.ScriptId,
                            EffectiveDate = activeDate,
                            ExpirationDate = DateTime.MaxValue
                        };

                        HrbHistory hrbHistory = new HrbHistory
                        {
                            UserId = user.Id,
                            HrbStatus = HrbStatus.Active,
                            EffectiveDate = activeDate,
                            ExpirationDate = DateTime.MaxValue
                        };

                        bOk = scriptManager.CreateScriptHistory(scriptHistory) 
                            && leaveCalendarTypeHistoryService.Create(leaveCalendarTypeHistory) != null 
                            && hrbHistoryService.Create(hrbHistory) != null;
                    }
                    if (bOk)
                    {
                        bOk = changeManagerInfoManager.ChangeManager(user, userManager.GetUserById((int)user.HeadOfOrgUnitId), userManager.GetUserById(user.ManagerId), false);
                    }
                    if (bOk)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_CREATE_USER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UserRequestModel userRequest)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    bool ok = true;
                    if (userRequest == null || userRequest.Id != id)
                    {
                        return BadRequest();
                    }
                    User user = userManager.GetUserById(id);
                    user.Email = userRequest.Email;
                    user.FullName = userRequest.FullName;
                    user.Department = userRequest.Department;
                    user.Corporation = userRequest.Corporation;
                    //
                    if (userRequest.ManagerId != user.ManagerId)
                    {
                        user.ManagerId = userRequest.ManagerId;
                        User currentManager = userManager.GetUserById((userRequest.HeadOfOrgUnitId is null) ? user.ManagerId : (int)userRequest.HeadOfOrgUnitId);
                        User newManager = userManager.GetUserById(userRequest.ManagerId);
                        ok = changeManagerInfoManager.ChangeManager(user, currentManager, newManager, false);
                    }
                    
                    user.Nationality = userRequest.Nationality;
                    user.Email = userRequest.Email;
                    user.HeadOfOrgUnitId = userRequest.HeadOfOrgUnitId;
                    if (!(userRequest.ActiveDate is null))
                    {
                        CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
                        DateTime effectiveDate = DateTime.ParseExact(userRequest.ActiveDate, Constants.FORMAT_DATE_DDMMYYYYHHMMSS, customCulture).Date;
                        ScriptHistory scriptNow = scriptManager.GetScriptHistoryInDay(id, DateTime.Now.Date);
                        if (effectiveDate != scriptNow.EffectiveDate)
                        {
                            scriptNow.EffectiveDate = effectiveDate;
                            scriptNow = scriptHistoryService.Update(scriptNow);
                            ok = scriptNow is null ? false : true;
                        }
                    }
                    if (ok)
                    {
                        ok = userManager.Update(user, userRequest.Groups);
                    }
                    if (ok)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return ok ? Ok() : LogicError(ResultCode.CAN_NOT_UPDATE_USER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // PUT api/<controller>/5
        [HttpPut("Deactive/{id}")]
        public IActionResult Deactive([FromBody]UserRequestModel userRequest)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    User user = userManager.GetUserById(userRequest.Id);
                    if (user == null)
                    {
                        return NotFound();
                    }
                    DateTime disableDate = STHelper.ConvertDateTimeToLocal(userRequest.DisableDate).Value.Date;
                    bool bOK = userManager.Deactive(user, disableDate);
                    if (bOK)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOK ? Ok() : LogicError(ResultCode.CAN_NOT_UPDATE_USER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // PUT api/<controller>/5
        [HttpPut("Active/{id}")]
        public IActionResult Active([FromBody]UserRequestModel userRequest)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    User user = userManager.GetUserById(userRequest.Id);
                    if (user == null)
                    {
                        return NotFound();
                    }
                    DateTime activeDate = STHelper.ConvertDateTimeToLocal(userRequest.ActiveDate).Value.Date;
                    bool bOK = userManager.Active(user, activeDate);
                    if (bOK)
                    {
                        transaction.Commit();
                    }
                    else
                    {
                        transaction.Rollback();
                    }
                    return bOK ? Ok() : LogicError(ResultCode.CAN_NOT_UPDATE_USER);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (userManager.Delete(id))
            {
                return new NoContentResult();
            }

            return NotImplemented();
        }

        [HttpGet]
        [Route("MyStaffs")]
        public IActionResult GetMyStaffs(STRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, DateTime.Now.Date);
            List<UserResult> data = userManager.GetMyActiveStaffs(user, out int numberOfPages, out int totalRecords);
            return OkPagination(data, numberOfPages, totalRecords);
        }

        [HttpGet]
        [Route("GetStaffsOfEmployee")]
        public IActionResult GetStaffsOfEmployee([FromQuery]int managerId, STRequestModel request)
        {
            User manager = userManager.GetUserById(managerId);
            Script script = scriptManager.GetCurrentScriptByUser(manager.Id, DateTime.Now.Date);
            List<UserResult> data = userManager.GetMyActiveStaffs(manager, out int numberOfPages, out int totalRecords);
            return OkPagination(data, numberOfPages, totalRecords);
        }

        [HttpGet]
        [Route("GetAllCalendarTypes")]
        public IActionResult GetAllCalendarTypes()
        {
            List<LeaveCalendarType> calendarTypes = userManager.GetAllCalendarTypes();
            return Ok(calendarTypes);
        }

        [HttpGet]
        [Route("GetAllCorporationTypes")]
        public IActionResult GetAllCorporationTypes()
        {
            List<CorporationType> corporationTypes = userManager.GetAllCorporationTypes();
            return Ok(corporationTypes);
        }

        [HttpGet]
        [Route("GetGroupsByUserId")]
        public IActionResult GetGroupsByUserId([FromQuery]int userId)
        {
            User user = userManager.GetUserById(userId);
            List<Group> groups = userManager.GetGroupsByUserId(user);
            return Ok(groups);
        }

        [HttpGet]
        [Route("GetMyUser")]
        public IActionResult GetMyUser()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
    }
}
