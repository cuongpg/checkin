﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.RequestModels;
using TOCO.Models.ResultModels.WisamiResultModels;
using UAParser;

namespace TOCO.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/TimeKeeping")]
    public class TimeKeepingController : BaseController
    {
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor accessor;
        private readonly IDistributedCache cache;
        private readonly TimeKeepingManager timeKeepingManager;
        private readonly ClientManager clientManager;
        private readonly UserManager userManager;
        private readonly OvertimeManager overtimeManager;
        private readonly ApprovalRecordManager approvalRecordManager;
        private readonly ScriptManager scriptManager;
        private readonly EventManager eventManager;
        private readonly ApplicationDbContext context;
        private readonly NotificationManager notificationManager;
        private readonly EmailManager emailManager;
        private readonly ChangeManagerInfoManager changeManagerInfoManager;
        private readonly HrbHistoryService hrbHistoryService;

        public TimeKeepingController(ApplicationDbContext context, IConfiguration configuration,
            IHttpContextAccessor accessor, IDistributedCache cache)
        {
            this.context = context;
            this.configuration = configuration;
            this.accessor = accessor;
            this.cache = cache;
            timeKeepingManager = new TimeKeepingManager(context);
            clientManager = new ClientManager(context);
            userManager = new UserManager(context);
            overtimeManager = new OvertimeManager(context);
            approvalRecordManager = new ApprovalRecordManager(context);
            scriptManager = new ScriptManager(context);
            eventManager = new EventManager(context);
            notificationManager = new NotificationManager(context);
            emailManager = new EmailManager(context);
            changeManagerInfoManager = new ChangeManagerInfoManager(context);
            hrbHistoryService = new HrbHistoryService(context);
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("CountPendingRecords")]
        public IActionResult CountPendingRecords()
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            int numberPendingTimeKeeping = timeKeepingManager.CountPendingTimeKeeping(user, false);
            int numberPendingTimeKeepingForManager = timeKeepingManager.CountPendingTimeKeeping(user, true);
            int numberPendingApproval = approvalRecordManager.CountPendingApprovalRecord(user, false, false, false);
            int numberPendingApprovalForManager = approvalRecordManager.CountPendingApprovalRecord(user, true, false, false);
            int numberPendingApprovalForAdmin = approvalRecordManager.CountPendingApprovalRecord(user, true, true, false);
            int numberPendingDestroyApproval = approvalRecordManager.CountPendingApprovalRecord(user, false, false, true);
            int numberPendingDestroyApprovalForManager = approvalRecordManager.CountPendingApprovalRecord(user, true, false, true);
            int numberPendingOverTime = overtimeManager.CountPendingOverTime(user, false, false);
            int numberPendingOverTimeForManager = overtimeManager.CountPendingOverTime(user, true, false);
            int numberPendingUnOverTime = overtimeManager.CountPendingOverTime(user, false, true);
            int numberPendingChangeManager = changeManagerInfoManager.CountPendingChangeManager(user, false);
            int numberPendingChangeManagerForManager = changeManagerInfoManager.CountPendingChangeManager(user, true);
            return Ok(new
            {
                numberPendingTimeKeeping,
                numberPendingTimeKeepingForManager,
                numberPendingApproval,
                numberPendingApprovalForManager,
                numberPendingApprovalForAdmin,
                numberPendingDestroyApproval,
                numberPendingDestroyApprovalForManager,
                numberPendingOverTime,
                numberPendingOverTimeForManager,
                numberPendingUnOverTime,
                numberPendingChangeManager,
                numberPendingChangeManagerForManager
            });
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("GetTopicaLocation")]
        public IActionResult GetTopicaLocation([FromBody]LocationRequestModel request)
        {
            int radius = int.Parse(configuration["TopicaRadius"]);
            (Location location, double distance) = clientManager.GetTopicaLocation(request);
            return Ok(new
            {
                location,
                distance,
                radius
            });
        }

        // GET: api/<controller>
        [HttpGet]
        [HttpPost]
        [Route("GetAllLocation")]
        public IActionResult GetAllTopicaLocation()
        {
            int radius = int.Parse(configuration["TopicaRadius"]);
            List<Location> locations = clientManager.GetAllTopicaLocation();
            return Ok(new
            {
                locations,
                radius
            });
        }

        // GET: api/<controller>
        [HttpPost]
        [Route("GetStatusTimeKeeping")]
        public IActionResult GetStatusTimeKeeping([FromBody]LocationRequestModel request, [FromQuery] string language = "vi")
        {
            DateTime time = DateTime.Now;
            string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            bool bOk = clientManager.ValidateClient(request, clientIP);
            User user = userManager.GetUserByEmail(email);
            if (user.IsPilot) { bOk = true; };
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, time.Date);
            StatusTimeKeepingResult status = timeKeepingManager.GetStatus(user, script, time);
            int radius = int.Parse(configuration["TopicaRadius"]);
            (Location location, double distance) = clientManager.GetTopicaLocation(request);

            return Ok(new
            {
                clientIP,
                verifyClient = bOk,
                statusTimeKeeping = status,
                scriptName = script.Name,
                scriptDescription = MessageManager.GetMessageManager(language).GetValue(script.Description),
                subScript = script.SubScript,
                radius,
                location,
                distance
            });
        }

        // POST: api/<controller>/GetCheckInValidCode
        [HttpPost]
        [Route("GetCheckInValidCode")]
        public IActionResult GetCheckInValidCode([FromBody]LocationRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            DateTime checkInTime = DateTime.Now;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, checkInTime.Date);
            List<string> statusAllowSkips = new List<string>();
            string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            if (!clientManager.ValidateClient(request, clientIP))
            {
                statusAllowSkips.Add(ResultCode.CHECKIN_INVALID_CLIENT);
            }
            (bool bOk, string[] resultStatus) = timeKeepingManager.ValidateCheckIn(user, script, checkInTime);

            statusAllowSkips.AddRange(resultStatus);
            if (!bOk)
            {
                return LogicError(ResultCode.CAN_NOT_TIMEKEEPING);
            }
            return Ok(statusAllowSkips);
        }

        // POST: api/<controller>/GetCheckOutValidCode
        [HttpPost]
        [Route("GetCheckOutValidCode")]
        public IActionResult GetCheckOutValidCode([FromBody]LocationRequestModel request)
        {
            DateTime checkOutTime = DateTime.Now;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptInDay(user.Id, checkOutTime.Date);
            if (script == null)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            List<string> statusAllowSkips = new List<string>();
            if (!clientManager.ValidateClient(request, clientIP))
            {
                statusAllowSkips.Add(ResultCode.CHECKOUT_INVALID_CLIENT);
            }
            (bool bOk, string[] resultCodes, TimeKeepingDetail timeKeepingDetail) = timeKeepingManager.ValidateCheckOut(user, script, checkOutTime);
            if (!bOk)
            {
                return LogicError(ResultCode.CAN_NOT_TIMEKEEPING);
            }
            else
            {
                statusAllowSkips.AddRange(resultCodes);
            }

            // Get thời gian làm thêm giờ
            (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(user, script, timeKeepingDetail.CheckInTime.Value, checkOutTime, true, null, null);

            // Tổng số giây làm thêm 
            double overTimeSeconds = 0;
            if (hasOverTime)
            {
                // Kiểm tra conflict thời gian
                if (overtimeManager.CheckConflictTime(email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                {
                    statusAllowSkips.Add(ResultCode.TIMEKEEPING_HAS_OVER_TIME_CONFLICT);
                }
                else
                {
                    statusAllowSkips.Add(ResultCode.TIMEKEEPING_HAS_OVER_TIME);
                }

                (double morningOverTime, double affternoonOverTime) = STHelper.CalculationWorkSeconds(overTimeDetail.EndTime, overTimeDetail.StartTime, script.LunchBreak);
                overTimeSeconds = morningOverTime + affternoonOverTime;
            }

            return Ok(new
            {
                statusAllowSkips,
                hasOverTime,
                overTimeSeconds
            });
        }

        // POST: api/<controller>/GetManualValidCode
        [HttpPost]
        [Route("GetManualValidCode")]
        public IActionResult GetManualValidCode([FromBody]TimeKeepingRequestModel timeKeepingRequestModel)
        {
            DateTime workDate = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.WorkDate).Value.Date;
            DateTime checkInTime = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.CheckInTime).Value;
            DateTime? checkOutTime = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.CheckOutTime);
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptInDay(user.Id, workDate);
            if (script == null)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            List<string> statusAllowSkips = new List<string>();
            (bool bOk, string[] resultStatus) = timeKeepingManager.ValidateTimeKeepingManual(user, script, checkInTime, checkOutTime, false);
            if (!bOk)
            {
                return LogicError(ResultCode.CAN_NOT_MANUAL_TIMEKEEPING);
            }
            statusAllowSkips.AddRange(resultStatus);

            // Kiểm tra conflict thời gian
            if (timeKeepingManager.CheckConflictTime(user, script, checkInTime, checkOutTime))
            {
                statusAllowSkips.Add(ResultCode.TIMEKEEPING_CONFLICT);
            }
            // Kiểm tra conflict thời gian overTime
            if (checkOutTime != null)
            {
                (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(user, script, checkInTime, checkOutTime.Value, true, null, null);
                if (hasOverTime)
                {
                    // Kiểm tra conflict thời gian
                    if (overtimeManager.CheckConflictTime(email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                    {
                        statusAllowSkips.Add(ResultCode.TIMEKEEPING_HAS_OVER_TIME_CONFLICT);
                    }
                }
            }
            return Ok(statusAllowSkips);
        }


        // POST: api/<controller>/AdminGetManualValidCode
        [HttpPost]
        [AllowAnonymous]
        [Route("AttendenceManagementImport")]
        public IActionResult AttendenceManagementImport([FromBody]AttendenceManagementModel attendenceManagementModel, [FromHeader] string Authorization)
        {
            string[] allowTokens = AppSetting.Configuration.GetArrayValue<string>("FCM:AllowTokens");
            string userAgent = Request.Headers["User-Agent"];
            if (!allowTokens.Contains(Authorization))
            {
                return new ContentResult()
                {
                    Content = @"{""message"":""YOU_DONT_HAVE_PERMISSION"",""data"":null}",
                    StatusCode = 403,
                    ContentType = "application/json; charset=utf-8"
                };
            }
            List<AMRecordModel> success = new List<AMRecordModel>();
            List<AMRecordModel> error = new List<AMRecordModel>();
            foreach (AMRecordModel record in attendenceManagementModel.Data)
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    bool bOK = true;
                    try
                    {
                        DateTime time = STHelper.ConvertDateTimeToLocal(record.Time).Value;
                        User user = userManager.GetUserById(record.Id);
                        User manager = userManager.GetUserById(user.ManagerId);
                        Script script = scriptManager.GetScriptInDay(user.Id, time.Date);
                        TimeKeepingDetail currentTimeKeeping;
                        StatusTimeKeepingResult status = timeKeepingManager.GetStatus(user, script, time);

                        switch (status.Status)
                        {
                            case StatusTimeKeeping.Checkin:
                                // Thực hiện checkin
                                if (bOK = !GlobalVariable.Data.ListCheckin.Contains(user.Id))
                                {
                                    GlobalVariable.Data.ListCheckin.Add(user.Id);
                                    string device = GetDevice(userAgent);
                                    bOK = timeKeepingManager.CheckIn(user, script, true, time, MessageManager.GetMessageManager(user.Nationality).GetValue(Constants.ATTENDENCE_MANAGEMENT_CHECKIN), device, true, record.Position);
                                    if (bOK)
                                    {
                                        PushFirstCheckin(time, user, device, record.Position);
                                    }
                                    GlobalVariable.Data.ListCheckin.Remove(user.Id);
                                }
                                break;
                            case StatusTimeKeeping.Checkout:
                                int minWork = int.Parse(configuration["MinWork"]);
                                currentTimeKeeping = timeKeepingManager.GetTimeKeepingDetail(user, time.Date);
                                // Nếu giờ chấm nhỏ hơn giờ checkin
                                if (time < currentTimeKeeping.CheckInTime)
                                {
                                    currentTimeKeeping.CheckInTime = time;
                                    currentTimeKeeping.IsCheckinFinger = true;
                                    currentTimeKeeping.CheckinPosition = record.Position;
                                    timeKeepingManager.Update(currentTimeKeeping);
                                }

                                else if (bOK = time.Subtract(currentTimeKeeping.CheckInTime.Value).TotalSeconds > minWork)
                                {
                                    // Nếu giờ checkout - giờ checkin < minWork seconds thì bỏ qua
                                    // Thực hiện checkout
                                    TimeKeeping timeKeeping;
                                    (bOK, currentTimeKeeping, timeKeeping) = timeKeepingManager.CheckOut(user, script, true, time, MessageManager.GetMessageManager(user.Nationality).GetValue(Constants.ATTENDENCE_MANAGEMENT_CHECKOUT), GetDevice(userAgent), true, record.Position);
                                    if (bOK)
                                    {
                                        if (currentTimeKeeping.Status == StatusTimeKeepingApproval.Done || currentTimeKeeping.Status == StatusTimeKeepingApproval.Approved)
                                        {
                                            // Xử lý ăn ca
                                            timeKeepingManager.ShiftsEatProcess(timeKeeping.WorkDate, user, script, currentTimeKeeping.CheckOutTime);
                                            // Get thời gian làm thêm giờ
                                            (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(user, script, currentTimeKeeping.CheckInTime.Value, currentTimeKeeping.CheckOutTime.Value, false, currentTimeKeeping.Id, null);
                                            if (hasOverTime)
                                            {
                                                // Kiểm tra conflict thời gian
                                                if (overtimeManager.CheckConflictTime(user.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                                                {
                                                    overtimeManager.DestroyConflictOverTime(user.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null);
                                                }
                                                overtimeManager.CreateOverTime(user, script, currentTimeKeeping.TimeKeeping.WorkDate, overTimeDetail, false, MessageManager.GetMessageManager(user.Nationality).GetValue(Constants.ATTENDENCE_MANAGEMENT_CHECKOUT), StatusOverTime.Pending);
                                                // Gửi mail thông báo làm thêm giờ cho quản lý
                                                Event eventOvertime = eventManager.GetEventById(overTimeDetail.EventId);
                                                emailManager.PushEmail(
                                                    manager.Email,
                                                    null,
                                                    null,
                                                    EmailTemplateCode.OC002,
                                                    new string[]
                                                    {
                                                        manager.FullName,
                                                        user.FullName,
                                                        string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                                        (string)eventOvertime.EventCode,
                                                        time.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                        overTimeDetail.StartTime.ToString(Constants.FORMAT_TIME_HHMM) + " - " + overTimeDetail.EndTime.ToString(Constants.FORMAT_TIME_HHMM),
                                                        overTimeDetail.Note,
                                                        configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/overtime"
                                                    });
                                                // Gửi notify thông báo làm thêm giờ cho quản lý
                                                notificationManager.PushNotification(
                                                    manager.Email,
                                                    null,
                                                    user.ImageUrl,
                                                    NotificationTemplateCode.ON002,
                                                    new string[]
                                                    {
                                                        time.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                        user.FullName
                                                    },
                                                    manager.Nationality);
                                            }
                                        }
                                        if (currentTimeKeeping.Status == StatusTimeKeepingApproval.Pending)
                                        {
                                            // Bắn mail đăng ký chấm công bất thường cho quản lý
                                            emailManager.PushEmail(
                                                manager.Email,
                                                null,
                                                null,
                                                EmailTemplateCode.OC005,
                                                new string[]
                                                {
                                                    manager.FullName,
                                                    user.FullName,
                                                    string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                                    timeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                    currentTimeKeeping.CheckInTime.HasValue? currentTimeKeeping.CheckInTime.Value.ToString(Constants.FORMAT_TIME_HHMM) : string.Empty,
                                                    currentTimeKeeping.CheckOutTime.HasValue? currentTimeKeeping.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMM) : string.Empty,
                                                    currentTimeKeeping.Note,
                                                    configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/irregular"
                                                });
                                            // Bắn notify đăng ký chấm công bất thường cho quản lý
                                            notificationManager.PushNotification(
                                                manager.Email,
                                                null,
                                                user.ImageUrl,
                                                NotificationTemplateCode.ON005,
                                                new string[]
                                                {
                                                    time.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                    user.FullName
                                                },
                                                manager.Nationality);
                                        }
                                    }
                                }
                                break;
                            default:
                                currentTimeKeeping = timeKeepingManager.GetTimeKeepingDetail(user, time.Date);
                                // Nếu giờ checkout lớn hơn giờ checkout cũ
                                if (bOK = time > currentTimeKeeping.CheckOutTime)
                                {
                                    currentTimeKeeping.CheckOutTime = time;
                                    currentTimeKeeping.IsCheckoutFinger = true;
                                    currentTimeKeeping.CheckoutPosition = record.Position;
                                    timeKeepingManager.Update(currentTimeKeeping);
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        bOK = false;
                        if (GlobalVariable.Data.ListCheckin.Contains(record.Id))
                        {
                            GlobalVariable.Data.ListCheckin.Remove(record.Id);
                        }
                    }
                    if (bOK)
                    {
                        success.Add(record);
                        transaction.Commit();
                    }
                    else
                    {
                        error.Add(record);
                        transaction.Rollback();
                    }
                }
            }
            return Ok(new
            {
                success,
                error
            });
        }

        // POST: api/<controller>/AdminGetManualValidCode
        [HttpPost]
        [Route("AdminGetManualValidCode")]
        public IActionResult AdminGetManualValidCode([FromQuery]string email, [FromBody]TimeKeepingRequestModel timeKeepingRequestModel)
        {
            DateTime workDate = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.WorkDate).Value.Date;
            DateTime checkInTime = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.CheckInTime).Value;
            DateTime? checkOutTime = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.CheckOutTime);
            //string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptInDay(user.Id, workDate);
            if (script == null)
            {
                return NotFound(ResultCode.NOT_FOUND_SCRIPT);
            }
            List<string> statusAllowSkips = new List<string>();
            (bool bOk, string[] resultStatus) = timeKeepingManager.ValidateTimeKeepingManual(user, script, checkInTime, checkOutTime, true);
            if (!bOk)
            {
                return LogicError(ResultCode.CAN_NOT_MANUAL_TIMEKEEPING);
            }
            statusAllowSkips.AddRange(resultStatus);

            // Kiểm tra conflict thời gian
            if (timeKeepingManager.CheckConflictTime(user, script, checkInTime, checkOutTime))
            {
                statusAllowSkips.Add(ResultCode.TIMEKEEPING_CONFLICT);
            }
            // Kiểm tra conflict thời gian overTime
            if (checkOutTime != null)
            {
                (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(user, script, checkInTime, checkOutTime.Value, true, null, null);
                if (hasOverTime)
                {
                    // Kiểm tra conflict thời gian
                    if (overtimeManager.CheckConflictTime(email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                    {
                        statusAllowSkips.Add(ResultCode.TIMEKEEPING_HAS_OVER_TIME_CONFLICT);
                    }
                }
            }
            return Ok(statusAllowSkips);
        }


        private string GetDevice(string userAgent)
        {
            string device = string.Empty;
            switch (userAgent)
            {
                case Constants.UserAgent.ANDROID:
                    device = Constants.Device.ANDROID;
                    break;
                case Constants.UserAgent.IOS:
                    device = Constants.Device.IOS;
                    break;
                case Constants.UserAgent.CHECKIN_SERVICE:
                    device = Constants.Device.FINGERPRINT_MACHINE;
                    break;
                default:
                    var uaParser = Parser.GetDefault();
                    ClientInfo c = uaParser.Parse(userAgent);
                    device = string.Format("{0} on {1}", c.UserAgent.ToString(), c.OS.ToString());
                        break;
            }
            return device;
        }

        private string GetTimeKeepingChannel(string device)
        {
            string channel = string.Empty;
            switch (device)
            {
                case Constants.Device.ANDROID:
                case Constants.Device.IOS:
                    channel = Constants.TimekeepingChannel.MOBLIE;
                    break;
                case Constants.Device.FINGERPRINT_MACHINE:
                    channel = Constants.TimekeepingChannel.FINGERPRINT;
                    break;
                default: 
                    channel = Constants.TimekeepingChannel.WEB;
                    break;
            }
            return channel;
        }


        private void PushFirstCheckin(DateTime checkInTime, User user, string device, string position = Constants.NA)
        {
            if (PilotFeatureManager.CheckUsing(user.Id, PilotCode.FIRST_CHECKIN))
            {
                string time = STHelper.TimeToString(checkInTime, user.Nationality);
                string channel = GetTimeKeepingChannel(device);
                emailManager.PushEmail(
                    user.Email,
                    null,
                    null,
                    EmailTemplateCode.OC020,
                    new string[]
                    {
                        user.Email.Substring(0, user.Email.IndexOf('@')).ToUpper(),
                        time,
                        MessageManager.GetMessageManager(user.Nationality).GetValue(channel),
                        MessageManager.GetMessageManager(user.Nationality).GetValue(device),
                        position
                    });
            }
        }

        [HttpPost]
        [Route("CheckIn")]
        public IActionResult CheckIn([FromBody]LocationRequestModel request, [FromQuery]string checkinNote)
        {
            bool bOk = false;
            DateTime checkInTime = DateTime.Now;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            string userAgent = Request.Headers["User-Agent"];
            using (var transaction = context.Database.BeginTransaction())
            {
                User user = userManager.GetUserByEmail(email);
                try
                {
                    if (!GlobalVariable.Data.ListCheckin.Contains(user.Id))
                    {
                        GlobalVariable.Data.ListCheckin.Add(user.Id);
                        Script script = scriptManager.GetCurrentScriptByUser(user.Id, checkInTime.Date);
                        string device = GetDevice(userAgent);
                        if (timeKeepingManager.CheckIn(user, script, clientManager.ValidateClient(request, clientIP), checkInTime, checkinNote, device))
                        {
                            PushFirstCheckin(checkInTime, user, device);
                            transaction.Commit();
                            bOk = true;
                        }
                        else
                        {
                            transaction.Rollback();
                        }
                        GlobalVariable.Data.ListCheckin.Remove(user.Id);
                    }
                }
                catch (Exception ex)
                {
                    GlobalVariable.Data.ListCheckin.Remove(user.Id);
                    transaction.Rollback();
                    throw ex;
                }
            }
            return bOk ? Ok() : LogicError(ResultCode.CAN_NOT_TIMEKEEPING);
        }

        [HttpPost]
        [Route("CheckOut")]
        public IActionResult CheckOut([FromBody]LocationRequestModel request, [FromQuery]int? eventId, [FromQuery]string overTimeNode)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    DateTime checkOutTime = DateTime.Now;
                    DateTime workDate = DateTime.Now.Date;
                    string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    string userAgent = Request.Headers["User-Agent"];
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    User manager = userManager.GetUserById(user.ManagerId);
                    Script script = scriptManager.GetScriptInDay(user.Id, workDate);
                    if (script == null)
                    {
                        return NotFound(ResultCode.NOT_FOUND_SCRIPT);
                    }
                    if (eventId.HasValue && !eventManager.CheckHasEventId(script.CorporationType, script.EmployeeTypeId, eventId.Value))
                    {
                        return BadRequest(ResultCode.CAN_NOT_TIMEKEEPING);
                    }

                    (bool bOK, TimeKeepingDetail timeKeepingDetail, TimeKeeping timeKeeping) = timeKeepingManager.CheckOut(user, script, clientManager.ValidateClient(request, clientIP), checkOutTime, overTimeNode, GetDevice(userAgent));
                    if (bOK)
                    {
                        if (timeKeepingDetail.Status == StatusTimeKeepingApproval.Done || timeKeepingDetail.Status == StatusTimeKeepingApproval.Approved)
                        {
                            // Xử lý ăn ca
                            timeKeepingManager.ShiftsEatProcess(timeKeeping.WorkDate, user, script, timeKeepingDetail.CheckOutTime);
                            // Get thời gian làm thêm giờ
                            (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(user, script, timeKeepingDetail.CheckInTime.Value, timeKeepingDetail.CheckOutTime.Value, false, timeKeepingDetail.Id, eventId);
                            if (hasOverTime)
                            {
                                // Kiểm tra conflict thời gian
                                if (overtimeManager.CheckConflictTime(user.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                                {
                                    overtimeManager.DestroyConflictOverTime(user.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null);
                                }
                                overtimeManager.CreateOverTime(user, script, timeKeepingDetail.TimeKeeping.WorkDate, overTimeDetail, false, overTimeNode, StatusOverTime.Pending);
                                // Gửi mail thông báo làm thêm giờ cho quản lý
                                Event eventOvertime = eventManager.GetEventById(overTimeDetail.EventId);
                                emailManager.PushEmail(
                                    manager.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC002,
                                    new string[]
                                    {
                                        manager.FullName,
                                        user.FullName,
                                        string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                        (string)eventOvertime.EventCode,
                                        workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        overTimeDetail.StartTime.ToString(Constants.FORMAT_TIME_HHMM) + " - " + overTimeDetail.EndTime.ToString(Constants.FORMAT_TIME_HHMM),
                                        overTimeDetail.Note,
                                        configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/overtime"
                                    });
                                // Gửi notify thông báo làm thêm giờ cho quản lý
                                notificationManager.PushNotification(
                                    manager.Email,
                                    null,
                                    user.ImageUrl,
                                    NotificationTemplateCode.ON002,
                                    new string[]
                                    {
                                        workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        user.FullName
                                    },
                                    manager.Nationality);
                            }
                        }
                        if (timeKeepingDetail.Status == StatusTimeKeepingApproval.Pending)
                        {
                            // Bắn mail đăng ký chấm công bất thường cho quản lý
                            emailManager.PushEmail(
                                      manager.Email,
                                      null,
                                      null,
                                      EmailTemplateCode.OC005,
                                      new string[]
                                      {
                                            manager.FullName,
                                            user.FullName,
                                            string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                            timeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                            timeKeepingDetail.CheckInTime.HasValue? timeKeepingDetail.CheckInTime.Value.ToString(Constants.FORMAT_TIME_HHMM) : string.Empty,
                                            timeKeepingDetail.CheckOutTime.HasValue? timeKeepingDetail.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMM) : string.Empty,
                                            timeKeepingDetail.Note,
                                            configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/irregular"
                                      });
                            // Bắn notify đăng ký chấm công bất thường cho quản lý
                            notificationManager.PushNotification(
                                  manager.Email,
                                  null,
                                  user.ImageUrl,
                                  NotificationTemplateCode.ON005,
                                  new string[]
                                  {
                                        workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        user.FullName
                                  },
                                  manager.Nationality);
                        }
                        transaction.Commit();
                        return Ok();
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(ResultCode.CAN_NOT_TIMEKEEPING);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpPost]
        [Route("TimeKeepingManually")]
        public IActionResult TimeKeepingManually([FromBody]TimeKeepingRequestModel timeKeepingRequestModel)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    string userAgent = Request.Headers["User-Agent"];
                    // Kiểm tra có lý do chấm thủ công không, nếu có check maxlenght
                    if (string.IsNullOrEmpty(timeKeepingRequestModel.Note) || timeKeepingRequestModel.Note.Count() > int.Parse(AppSetting.Configuration.GetValue("MaxLenghtNote")))
                    {
                        return LogicError(ResultCode.CAN_NOT_MANUAL_TIMEKEEPING);
                    }

                    User user = userManager.GetUserByEmail(email);
                    DateTime workDate = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.WorkDate).Value.Date;
                    Script script = scriptManager.GetScriptInDay(user.Id, workDate);
                    if (script == null)
                    {
                        return NotFound(ResultCode.NOT_FOUND_SCRIPT);
                    }
                    (bool bOk, TimeKeeping timeKeeping, TimeKeepingDetail timeKeepingDetail) = timeKeepingManager.TimeKeepingManually(user, script, false, timeKeepingRequestModel, GetDevice(userAgent));

                    if (bOk)
                    {
                        if (timeKeepingDetail.Status == StatusTimeKeepingApproval.Pending)
                        {
                            var manager = userManager.GetUserById(user.ManagerId);
                            // Bắn mail đăng ký chấm công bất thường cho quản lý
                            emailManager.PushEmail(
                                    manager.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC005,
                                    new string[]
                                    {
                                        manager.FullName,
                                        user.FullName,
                                        string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                        timeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        timeKeepingDetail.CheckInTime.HasValue? timeKeepingDetail.CheckInTime.Value.ToString(Constants.FORMAT_TIME_HHMM) : string.Empty,
                                        timeKeepingDetail.CheckOutTime.HasValue? timeKeepingDetail.CheckOutTime.Value.ToString(Constants.FORMAT_TIME_HHMM) : string.Empty,
                                        timeKeepingDetail.Note,
                                        configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/irregular"
                                    });
                            // Bắn notify đăng ký chấm công bất thường cho quản lý
                            notificationManager.PushNotification(
                                manager.Email,
                                null,
                                user.ImageUrl,
                                NotificationTemplateCode.ON005,
                                new string[]
                                {
                                        workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        user.FullName
                                },
                                manager.Nationality);
                        }
                        transaction.Commit();
                        return Ok();
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError(ResultCode.CAN_NOT_MANUAL_TIMEKEEPING);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpPost]
        [Route("AdminCreateTimeKeepingManually")]
        public IActionResult AdminCreateTimeKeepingManually([FromQuery]string email, [FromBody]TimeKeepingRequestModel timeKeepingRequestModel)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string userAgent = Request.Headers["User-Agent"];
                    // Kiểm tra có lý do chấm thủ công không, nếu có check maxlenght
                    if (string.IsNullOrEmpty(timeKeepingRequestModel.Note) || timeKeepingRequestModel.Note.Count() > int.Parse(AppSetting.Configuration.GetValue("MaxLenghtNote")))
                    {
                        return LogicError(ResultCode.CAN_NOT_MANUAL_TIMEKEEPING);
                    }

                    User user = userManager.GetUserByEmail(email);
                    DateTime workDate = STHelper.ConvertDateTimeToLocal(timeKeepingRequestModel.WorkDate).Value.Date;
                    Script script = scriptManager.GetScriptInDay(user.Id, workDate);
                    if (script == null)
                    {
                        return NotFound(ResultCode.NOT_FOUND_SCRIPT);
                    }
                    (bool bOk, TimeKeeping timeKeeping, TimeKeepingDetail timeKeepingDetail) = timeKeepingManager.TimeKeepingManually(user, script, true, timeKeepingRequestModel, GetDevice(userAgent));

                    if (bOk)
                    {
                        transaction.Commit();
                        return Ok();
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError(ResultCode.CAN_NOT_MANUAL_TIMEKEEPING);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/GetAbnormalTimeKeepingForManager
        [HttpGet]
        [Route("GetAbnormalTimeKeepingForManager")]
        public IActionResult GetAbnormalTimeKeepingForManager(STRequestModel request, [FromQuery] string language)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<AbnormalTimeKeepingResult> abnormalTimeKeepings = timeKeepingManager.GetAbnormalTimeKeeping(out int numberOfPages, out int totalRecords, request, user, true, language);
            return OkPagination(abnormalTimeKeepings, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/AdminGetAbnormalTimeKeepingForManager
        [HttpGet]
        [Route("AdminGetAbnormalTimeKeepingForManager")]
        public IActionResult AdminGetAbnormalTimeKeepingForManager(STRequestModel request, [FromQuery] int employeeCode, [FromQuery] string language)
        {
            User user = userManager.GetUserById(employeeCode);
            List<AbnormalTimeKeepingResult> abnormalTimeKeepings = timeKeepingManager.GetAbnormalTimeKeeping(out int numberOfPages, out int totalRecords, request, user, true, language);
            return OkPagination(abnormalTimeKeepings, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetMyAbnormalTimeKeeping
        [HttpGet]
        [Route("GetMyAbnormalTimeKeeping")]
        public IActionResult GetMyAbnormalTimeKeeping(STRequestModel request, [FromQuery] string language)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<AbnormalTimeKeepingResult> abnormalTimeKeepings = timeKeepingManager.GetAbnormalTimeKeeping(out int numberOfPages, out int totalRecords, request, user, false, language);
            return OkPagination(abnormalTimeKeepings, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetAbnormalTimeKeepingForAdmin
        [HttpGet]
        [Route("GetAbnormalTimeKeepingForAdmin")]
        public IActionResult GetAbnormalTimeKeepingForAdmin(STRequestModel request, [FromQuery] string language, [FromQuery] int employeeCode)
        {
            User user = userManager.GetUserById(employeeCode);
            List<AbnormalTimeKeepingResult> abnormalTimeKeepings = timeKeepingManager.GetAbnormalTimeKeeping(out int numberOfPages, out int totalRecords, request, user, false, language);
            return OkPagination(abnormalTimeKeepings, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/ApprovalAbnormalTimeKeeping
        [HttpPost]
        [Route("ApprovalAbnormalTimeKeeping")]
        public IActionResult ApprovalAbnormalTimeKeeping([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User manager = userManager.GetUserByEmail(email);
                    (bool bOk, string message, TimeKeepingDetail[] timeKeepingDetails) = timeKeepingManager.ConfirmAbnormalTimeKeeping(StatusTimeKeepingApproval.Approved, request.Ids, manager);

                    if (bOk)
                    {
                        foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
                        {
                            if (timeKeepingDetail.CheckOutTime.HasValue)
                            {
                                User employee = timeKeepingDetail.TimeKeeping.User;
                                Script script = scriptManager.GetScriptById(timeKeepingDetail.TimeKeeping.ScriptId);
                                // Xử lý ăn ca, làm thêm cho nhân viên
                                timeKeepingManager.ShiftsEatProcess(timeKeepingDetail.TimeKeeping.WorkDate, employee, script, timeKeepingDetail.CheckOutTime);

                                // Get thời gian làm thêm giờ
                                (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(employee, script, timeKeepingDetail.CheckInTime.Value, timeKeepingDetail.CheckOutTime.Value, false, timeKeepingDetail.Id, null);
                                if (hasOverTime)
                                {
                                    // Kiểm tra conflict thời gian
                                    if (overtimeManager.CheckConflictTime(employee.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                                    {
                                        overtimeManager.DestroyConflictOverTime(employee.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null);
                                    }
                                    overTimeDetail = overtimeManager.CreateOverTime(employee, script, timeKeepingDetail.TimeKeeping.WorkDate, overTimeDetail, false, timeKeepingDetail.Note, StatusOverTime.Pending);
                                    Event ev = eventManager.GetEventById(overTimeDetail.EventId);
                                    string eventName = string.Empty;
                                    if (ev.ShowCode && !string.IsNullOrEmpty(ev.Description))
                                    {
                                        eventName = string.Format("{0} ( {1} )", ev.EventCode, MessageManager.GetMessageManager(manager.Nationality).GetValue(ev.Description));
                                    }
                                    else if (ev.ShowCode)
                                    {
                                        eventName = ev.EventCode;
                                    }
                                    else if (!string.IsNullOrEmpty(ev.Description))
                                    {
                                        eventName = MessageManager.GetMessageManager(manager.Nationality).GetValue(ev.Description);
                                    }
                                    // Bắn mail đăng ký làm thêm giờ cho quản lý
                                    emailManager.PushEmail(
                                            manager.Email,
                                            null,
                                            null,
                                            EmailTemplateCode.OC002,
                                            new string[]
                                            {
                                                manager.FullName,
                                                employee.FullName,
                                                string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                                eventName,
                                                overTimeDetail.StartTime.Date.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                overTimeDetail.StartTime.ToString(Constants.FORMAT_TIME_HHMM) + " - " + overTimeDetail.EndTime.ToString(Constants.FORMAT_TIME_HHMM),
                                                overTimeDetail.Note,
                                                configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/overtime"
                                            });
                                    // Bắn notify đăng ký làm thêm giờ cho quản lý
                                    notificationManager.PushNotification(
                                           manager.Email,
                                           null,
                                           manager.ImageUrl,
                                           NotificationTemplateCode.ON002,
                                           new string[]
                                           {
                                                overTimeDetail.StartTime.Date.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                                employee.FullName
                                           },
                                           manager.Nationality);
                                }
                            }
                            // Bắn mail phản hồi chấm công bất thường cho nhân sự
                            emailManager.PushEmail(
                                    timeKeepingDetail.TimeKeeping.User.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC004,
                                    new string[]
                                    {
                                        timeKeepingDetail.TimeKeeping.User.FullName,
                                        timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        Constants.APPROVAL
                                    });
                            // Bắn notify phản hồi chấm công bất thường cho nhân sự
                            notificationManager.PushNotification(
                                timeKeepingDetail.TimeKeeping.User.Email,
                                null,
                                timeKeepingDetail.TimeKeeping.User.ImageUrl,
                                NotificationTemplateCode.ON004,
                                new string[]
                                {
                                        timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        Constants.APPROVAL
                                },
                                timeKeepingDetail.TimeKeeping.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpPost]
        [Route("AdminApproveAbnormalTimeKeeping")]
        public IActionResult AdminApproveAbnormalTimeKeeping([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, TimeKeepingDetail[] timeKeepingDetails) = timeKeepingManager.ConfirmAbnormalTimeKeeping(StatusTimeKeepingApproval.Approved, request.Ids, isForManager ? user : manager);

                    if (bOk)
                    {
                        foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
                        {
                            if (timeKeepingDetail.CheckOutTime.HasValue)
                            {
                                User employee = timeKeepingDetail.TimeKeeping.User;
                                Script script = scriptManager.GetScriptById(timeKeepingDetail.TimeKeeping.ScriptId);
                                // Xử lý ăn ca, làm thêm cho nhân viên
                                timeKeepingManager.ShiftsEatProcess(timeKeepingDetail.TimeKeeping.WorkDate, employee, script, timeKeepingDetail.CheckOutTime);

                                // Get thời gian làm thêm giờ
                                (bool hasOverTime, OverTimeDetail overTimeDetail) = overtimeManager.GetStartOverTime(employee, script, timeKeepingDetail.CheckInTime.Value, timeKeepingDetail.CheckOutTime.Value, false, timeKeepingDetail.Id, null);
                                if (hasOverTime)
                                {
                                    // Kiểm tra conflict thời gian
                                    if (overtimeManager.CheckConflictTime(employee.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null))
                                    {
                                        overtimeManager.DestroyConflictOverTime(employee.Email, overTimeDetail.StartTime, overTimeDetail.EndTime, null);
                                    }
                                    overtimeManager.CreateOverTime(employee, script, timeKeepingDetail.TimeKeeping.WorkDate, overTimeDetail, false, string.Empty, StatusOverTime.Pending);
                                }
                            }
                            timeKeepingDetail.ApproverEmail = email;
                            var timeKeeping = timeKeepingManager.Update(timeKeepingDetail);
                            // Bắn mail phản hồi chấm công bất thường cho nhân sự
                            emailManager.PushEmail(
                                    timeKeepingDetail.TimeKeeping.User.Email,
                                    null,
                                    null,
                                    EmailTemplateCode.OC004,
                                    new string[]
                                    {
                                        timeKeepingDetail.TimeKeeping.User.FullName,
                                        timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        Constants.APPROVAL
                                    });
                            // Bắn notify phản hồi chấm công bất thường cho nhân sự
                            notificationManager.PushNotification(
                                timeKeepingDetail.TimeKeeping.User.Email,
                                null,
                                timeKeepingDetail.TimeKeeping.User.ImageUrl,
                                NotificationTemplateCode.ON004,
                                new string[]
                                {
                                        timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        Constants.APPROVAL
                                },
                                timeKeepingDetail.TimeKeeping.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // POST: api/<controller>/RejectAbnormalTimeKeeping
        [HttpPost]
        [Route("RejectAbnormalTimeKeeping")]
        public IActionResult RejectAbnormalTimeKeeping([FromBody]ConfirmRequestModel request, [FromQuery] string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);

                    (bool bOk, string message, TimeKeepingDetail[] timeKeepingDetails) = timeKeepingManager.ConfirmAbnormalTimeKeeping(StatusTimeKeepingApproval.Rejected, request.Ids, user, rejectReason);
                    if (bOk)
                    {
                        foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
                        {
                            // Bắn mail phản hồi chấm công bất thường cho nhân sự
                            emailManager.PushEmail(
                                timeKeepingDetail.TimeKeeping.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC004,
                                new string[]
                                {
                                    timeKeepingDetail.TimeKeeping.User.FullName,
                                    timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.REJECT
                                });
                            // Bắn notify phản hồi chấm công bất thường cho nhân sự
                            notificationManager.PushNotification(
                                timeKeepingDetail.TimeKeeping.User.Email,
                                null,
                                timeKeepingDetail.TimeKeeping.User.ImageUrl,
                                NotificationTemplateCode.ON004,
                                new string[]
                                {
                                        timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        Constants.REJECT
                                },
                                timeKeepingDetail.TimeKeeping.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // POST: api/<controller>/AdminRejectAbnormalTimeKeeping
        [HttpPost]
        [Route("AdminRejectAbnormalTimeKeeping")]
        public IActionResult AdminRejectAbnormalTimeKeeping([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager, [FromQuery] string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);

                    (bool bOk, string message, TimeKeepingDetail[] timeKeepingDetails) = timeKeepingManager.ConfirmAbnormalTimeKeeping(StatusTimeKeepingApproval.Rejected, request.Ids, isForManager ? user : manager, rejectReason);
                    if (bOk)
                    {
                        foreach (TimeKeepingDetail timeKeepingDetail in timeKeepingDetails)
                        {
                            timeKeepingDetail.ApproverEmail = email;
                            var timeKeeping = timeKeepingManager.Update(timeKeepingDetail);
                            // Bắn mail phản hồi chấm công bất thường cho nhân sự
                            emailManager.PushEmail(
                                timeKeepingDetail.TimeKeeping.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC004,
                                new string[]
                                {
                                    timeKeepingDetail.TimeKeeping.User.FullName,
                                    timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.REJECT
                                });
                            // Bắn notify phản hồi chấm công bất thường cho nhân sự
                            notificationManager.PushNotification(
                                timeKeepingDetail.TimeKeeping.User.Email,
                                null,
                                timeKeepingDetail.TimeKeeping.User.ImageUrl,
                                NotificationTemplateCode.ON004,
                                new string[]
                                {
                                        timeKeepingDetail.TimeKeeping.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                        Constants.REJECT
                                },
                                timeKeepingDetail.TimeKeeping.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AddNote
        [HttpPost]
        [Route("AddNote")]
        public IActionResult AddNote([FromBody]NoteTimeKeepingRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            (bool bOk, string status) = timeKeepingManager.AddNote(email, request.Note);
            return bOk ? Ok(status) : LogicError();
        }

        // GET: api/<controller>/GetTimeKeeping
        [HttpGet]
        [Route("GetTimeKeeping/{month}/{year}/{scriptId}")]
        public IActionResult GetTimeKeeping(int month, int year, int scriptId, [FromQuery] string language = "vi")
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;

            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }

            return GetTimeKeepingForUser(month, year, scriptId, email, language);
        }

        // GET: api/<controller>/GetTimeKeepingOfStaff
        [HttpGet]
        [Route("GetTimeKeepingOfStaff/{month}/{year}/{scriptId}/{email}")]
        public IActionResult GetTimeKeepingOfStaff(int month, int year, string email, int scriptId, [FromQuery] string language = "vi")
        {
            string emailManager = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;

            if (month > 12 || month < 1 || year < 1 || !userManager.CheckIsManagerForStaff(email, emailManager))
            {
                return BadRequest();
            }

            return GetTimeKeepingForUser(month, year, scriptId, email, language);
        }

        private IActionResult GetTimeKeepingForUser(int month, int year, int scriptId, string email, string language)
        {
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetScriptById(scriptId);
            // Lấy định mức ngày công trong tháng
            int quotasTimeKeepingInMonth = timeKeepingManager.GetQuotasTimeKeepingInMonth(startDate, endDate);
            // Lấy danh sách ngày nghỉ lễ của nhân sự trong tháng
            List<LeaveDateResult> holidayDatesInMonth = script.HasHoliday ? userManager.GetLeaveDateInMonthByUser(user, month, year, language) : new List<LeaveDateResult>();
            // Lấy danh sách ngày nghỉ lễ của nhân sự trong tháng
            List<LeaveDateResult> weekendInMonth = script.HasLeaveWeekend ? userManager.GetWeekendInMonthByUser(user, month, year, language) : new List<LeaveDateResult>();
            double totalWorkSeconds = 0;
            int totalRejects = 0;
            int totalShiftEat = 0;
            double totalNumberOfWorkDay = 0;
            double totalNumberOfLeaveDay = 0;
            double totalMorningSeconds = 0;
            double totalAffternoonSeconds = 0;
            int totalCheckinLate = 0;
            int totalCheckoutEarly = 0;
            List<(double TotalSeconds, string Description, int Percentage)> totalOverTime = new List<(double TotalSeconds, string Description, int Percentage)>();
            List<(double Seconds, string WorkDate, List<(string, string, double)>)> overTimes = new List<(double Seconds, string WorkDate, List<(string, string, double)>)>();
            List<(double TotalSeconds, string Description, int Percentage)> totalPendingOverTime = new List<(double TotalSeconds, string Description, int Percentage)>();
            List<(double Seconds, string WorkDate, List<(string, string, double)>)> pendingOverTimes = new List<(double Seconds, string WorkDate, List<(string, string, double)>)>();
            List<(double Seconds, string WorkDate, List<(string, string, double)>)> expiredOverTimes = new List<(double Seconds, string WorkDate, List<(string, string, double)>)>();
            List<TimeKeepingResult> timeKeepingsInMonth = new List<TimeKeepingResult>();
            List<ApprovalDateResult> approvalDates = new List<ApprovalDateResult>();
            Dictionary<string, List<EventWorkResult>> eventWorksApproval = new Dictionary<string, List<EventWorkResult>>();
            Dictionary<string, List<EventWorkResult>> eventWorksPending = new Dictionary<string, List<EventWorkResult>>();
            Dictionary<string, List<EventWorkResult>> eventWorksExpired = new Dictionary<string, List<EventWorkResult>>();
            EmployeeType employeeType = scriptManager.GetEmployeeTypeById(script.EmployeeTypeId);
            // Lấy danh sách lịch sử script có script truyền vào
            List<ScriptHistory> scriptHistorys = scriptManager.GetScriptHistorys(user.Id, script.Id, startDate, endDate);
            if (scriptHistorys.Count == 0)
            {
                return NotFound();
            }
            foreach (ScriptHistory scriptHistory in scriptHistorys)
            {
                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;
                // Lọc theo lịch sử hrb
                // Lấy danh sách hrbHistory trong thời gian làm việc
                List<HrbHistory> hrbHistorys = hrbHistoryService.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                foreach (HrbHistory hrbHistory in hrbHistorys)
                {
                    if (hrbHistory.HrbStatus == HrbStatus.Pending)
                    {
                        continue;
                    }
                    // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                    DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                    DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;
                    // Lấy định mức ngày công trong lịch sử
                    int quotasTimeKeeping = timeKeepingManager.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                    // Lấy danh sách ngày xin nghỉ phép trong tháng
                    (List<ApprovalDateResult> approvalDatesByHistory, double numberOfLeaveDay) = approvalRecordManager.GetApprovalDatesInMonth(false, user, script, startWorkDate, endWorkDate);
                    foreach (ApprovalDateResult approval in approvalDatesByHistory)
                    {
                        approval.Reason = MessageManager.GetMessageManager(language).GetValue(approval.Reason);
                    }
                    approvalDates.AddRange(approvalDatesByHistory);
                    // Cộng thêm số công nghỉ phép
                    totalNumberOfLeaveDay += numberOfLeaveDay;
                    if (!script.NoNeedTimeKeeping)
                    {
                        // Lấy tổng số bản chấm công bị reject
                        totalRejects += timeKeepingManager.CountRejectTimeKeepingDetailsInMonth(user.Id, startWorkDate, endWorkDate);
                        // Lấy danh sách chấm công trong tháng
                        List<TimeKeepingResult> timeKeepings = timeKeepingManager.GetTimeKeepingsInMonth(false, approvalDates, user, script, startWorkDate, endWorkDate, language, ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                        // Add thông tin chấm công
                        timeKeepingsInMonth.AddRange(timeKeepings);
                    }
                    else
                    {
                        totalNumberOfWorkDay += quotasTimeKeeping - totalNumberOfLeaveDay;
                    }
                    if (script.HasShiftsEat)
                    {
                        // Lấy tổng số ăn ca
                        totalShiftEat += timeKeepingManager.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                    }
                    if (script.HasOverTime)
                    {
                        // Lấy thông tin làm thêm giờ
                        // Lấy thông tin làm thêm giờ đã duyệt
                        (List<(double TotalSeconds, string Description, int Percentage)> overTimeByPercentage, List<(double Seconds, string WorkDate, List<(string, string, double)>)> overTimeByWorkDay) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, language);
                        totalOverTime.AddRange(overTimeByPercentage);
                        overTimes.AddRange(overTimeByWorkDay);
                        // Lấy thông tin làm thêm giờ chờ duyệt
                        (List<(double TotalSeconds, string Description, int Percentage)> pendingOverTimeByPercentage, List<(double Seconds, string WorkDate, List<(string, string, double)>)> pendingOverTimeByWorkDay) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Pending, user, script, startWorkDate, endWorkDate, language);
                        totalPendingOverTime.AddRange(pendingOverTimeByPercentage);
                        pendingOverTimes.AddRange(pendingOverTimeByWorkDay);
                        //Lấy thông tin làm thêm giờ quá hạn
                        (List<(double TotalSeconds, string Description, int Percentage)> expiredOverTimeByPercentage, List<(double Seconds, string WorkDate, List<(string, string, double)>)> expiredOverTimeByWorkDay) = overtimeManager.GetTotalOverTimeByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Expired, user, script, startWorkDate, endWorkDate, language);
                        expiredOverTimes.AddRange(expiredOverTimeByWorkDay);
                    }
                    // Lấy thông tin làm thêm theo cơ chế
                    // Lấy thông tin làm thêm theo cơ chế đã duyệt
                    Dictionary<string, List<EventWorkResult>> eventApproval = overtimeManager.GetTotalEventWorkByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, language);
                    foreach (string key in eventApproval.Keys)
                    {
                        if (eventWorksApproval.ContainsKey(key))
                        {
                            eventWorksApproval[key].AddRange(eventApproval[key]);
                        }
                        else
                        {
                            eventWorksApproval.Add(key, eventApproval[key]);
                        }
                    }
                    // Lấy thông tin làm thêm theo cơ chế đợi duyệt
                    Dictionary<string, List<EventWorkResult>> eventPending = overtimeManager.GetTotalEventWorkByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Pending, user, script, startWorkDate, endWorkDate, language);
                    foreach (string key in eventPending.Keys)
                    {
                        if (eventWorksPending.ContainsKey(key))
                        {
                            eventWorksPending[key].AddRange(eventPending[key]);
                        }
                        else
                        {
                            eventWorksPending.Add(key, eventPending[key]);
                        }
                    }
                    //Lấy thông tin làm thêm giờ theo cơ chế bị quá hạn
                    Dictionary<string, List<EventWorkResult>> eventExpired = overtimeManager.GetTotalEventWorkByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Expired, user, script, startWorkDate, endWorkDate, language);
                    foreach (string key in eventExpired.Keys)
                    {
                        if (eventWorksExpired.ContainsKey(key))
                        {
                            eventWorksExpired[key].AddRange(eventExpired[key]);
                        }
                        else
                        {
                            eventWorksExpired.Add(key, eventExpired[key]);
                        }
                    }
                }
            }
            // Cộng tổng thời gian làm trong tháng
            totalWorkSeconds += totalMorningSeconds + totalAffternoonSeconds;

            return Ok(new
            {
                totalNumberOfWorkDay,
                totalNumberOfLeaveDay,
                totalMorningSeconds,
                totalAffternoonSeconds,
                totalCheckinLate,
                totalCheckoutEarly,
                totalRejects,
                totalShiftEat,
                totalWorkSeconds,
                totalOverTime,
                overTimes,
                totalPendingOverTime,
                pendingOverTimes,
                holidayDatesInMonth,
                weekendInMonth,
                timeKeepingsInMonth,
                approvalDates,
                quotasTimeKeepingInMonth,
                employeeType = employeeType.Name,
                script.SubScript,
                eventWorksApproval,
                eventWorksPending,
                expiredOverTimes,
                eventWorksExpired
            });
        }

        [HttpGet]
        [Route("ExportTimesheet/{month}/{year}/{scriptId}")]
        public IActionResult ExportTimesheet(int month, int year, int scriptId, [FromQuery] string language = "vi")
        {
            if (isExporting)
            {
                return BadRequest(ResultCode.IS_EXPORTING);
            }
            // Set trạng thái là đang export
            isExporting = true;
            DateTime startRun = DateTime.Now;
            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;

            Script script = scriptManager.GetScriptById(scriptId);
            IEnumerable<int> listUserIds = userManager.GetUserIdsByScript(script.Id, startDate, endDate);
            List<TimesheetResult> listTimesheetResults = new List<TimesheetResult>();

            try
            {
                int numberInTask = 10;
                int count = listUserIds.Count();
                int taskNumber = count / numberInTask;
                if (count % numberInTask != 0)
                {
                    taskNumber++;
                }

                List<TimesheetResult>[] listResult = new List<TimesheetResult>[taskNumber];
                Task[] process = new Task[taskNumber];
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    int i = indTask;
                    List<int> listUserIdInTask = new List<int>();
                    listUserIdInTask.AddRange(listUserIds.Skip(i * numberInTask).Take(numberInTask).ToList());
                    process[i] = Task.Run(() =>
                    {
                        listResult[i] = new List<TimesheetResult>();
                        listResult[i].AddRange(TimeKeepingProcess(listUserIdInTask, script, startDate, endDate, language));
                    });
                }

                Task.WaitAll(process);

                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    listTimesheetResults.AddRange(listResult[indTask]);
                }
            }
            catch (Exception ex)
            {
                isExporting = false;
                throw ex;
            }

            Task<MemoryStream> stream = timeKeepingManager.ExportTimesheet(script, listTimesheetResults, startDate, endDate);
            var qguid = Guid.NewGuid().ToString();
            int seconds = DateTime.Now.Subtract(startRun).Seconds;
            using (StreamWriter file = new StreamWriter(new FileStream(@"Templates\New.log", FileMode.Append, FileAccess.Write)))
            {
                file.WriteLine(seconds);
            }
            isExporting = false;
            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qguid + ".xlsx");
        }

        [HttpGet]
        [Route("ExportTimesheet/{scriptId}")]
        public IActionResult ExportTimesheet([FromQuery]string startTime, [FromQuery]string endTime, int scriptId, [FromQuery] string language = "vi")
        {
            if (isExporting)
            {
                return BadRequest(ResultCode.IS_EXPORTING);
            }
            // Set trạng thái là đang export
            isExporting = true;

            DateTime startRun = DateTime.Now;
            Script script = scriptManager.GetScriptById(scriptId);
            CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
            DateTime startDate = DateTime.ParseExact(startTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date; ;
            DateTime endDate = DateTime.ParseExact(endTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date; ;
            IEnumerable<int> listUserIds = userManager.GetUserIdsByScript(script.Id, startDate, endDate);
            List<TimesheetResult> listTimesheetResults = new List<TimesheetResult>();

            try
            {
                int numberInTask = 10;
                int count = listUserIds.Count();
                int taskNumber = count / numberInTask;
                if (count % numberInTask != 0)
                {
                    taskNumber++;
                }

                List<TimesheetResult>[] listResult = new List<TimesheetResult>[taskNumber];
                Task[] process = new Task[taskNumber];
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    int i = indTask;
                    List<int> listUserIdInTask = new List<int>();
                    listUserIdInTask.AddRange(listUserIds.Skip(i * numberInTask).Take(numberInTask).ToList());
                    process[i] = Task.Run(() =>
                    {
                        listResult[i] = new List<TimesheetResult>();
                        listResult[i].AddRange(TimeKeepingProcess(listUserIdInTask, script, startDate, endDate, language));
                    });
                }

                Task.WaitAll(process);

                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    listTimesheetResults.AddRange(listResult[indTask]);
                }
            }
            catch (Exception ex)
            {
                isExporting = false;
                throw ex;
            }

            Task<MemoryStream> stream = timeKeepingManager.ExportTimesheet(script, listTimesheetResults, startDate, endDate);
            var qguid = Guid.NewGuid().ToString();
            int seconds = DateTime.Now.Subtract(startRun).Seconds;
            using (StreamWriter file = new StreamWriter(new FileStream(@"Templates\New.log", FileMode.Append, FileAccess.Write)))
            {
                file.WriteLine(seconds);
            }

            isExporting = false;
            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qguid + ".xlsx");
        }

        [HttpGet]
        [Route("GetTimeKeepingOfEmployee/{month}/{year}/{scriptId}/{email}")]
        public IActionResult GetTimeKeepingOfEmployee(int month, int year, string email, int scriptId, [FromQuery] string language = "vi")
        {
            string emailManager = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;

            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }

            return GetTimeKeepingForUser(month, year, scriptId, email, language);
        }

        private List<TimesheetResult> TimeKeepingProcess(List<int> listUserIds, Script script, DateTime startDate, DateTime endDate, string language)
        {
            List<TimesheetResult> listTimesheetResults = new List<TimesheetResult>();
            int currentId = 0;
            try
            {
                using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                {
                    ApprovalRecordManager approvalRecordManagerTask = new ApprovalRecordManager(db);
                    TimeKeepingManager timeKeepingManagerTask = new TimeKeepingManager(db);
                    UserManager userManagerTask = new UserManager(db);
                    ScriptManager scriptManagerTask = new ScriptManager(db);
                    HrbHistoryService hrbHistoryServiceTask = new HrbHistoryService(db);
                    OvertimeManager overtimeManagerTask = new OvertimeManager(db);
                    LeaveDateService leaveDateServiceInTask = new LeaveDateService(db);
                    ReasonTypeService reasonTypeServiceInTask = new ReasonTypeService(db);
                    foreach (int userId in listUserIds)
                    {
                        currentId = userId;
                        User user = userManagerTask.GetUserById(userId);
                        // Lấy định mức ngày công trong tháng
                        int quotasTimeKeepingInMonth = timeKeepingManagerTask.GetQuotasTimeKeepingInMonth(startDate, endDate);
                        // Lấy ngày phép tồn cho Thái
                        double[] thailandRemainLeave = new double[8];
                        double[] vietnamRemainLeave = new double[2];
                        if (script.CorporationType == CorporationType.CorporationTH)
                        {
                            ReasonThailandId[] listReasonLeaveThId = {
                                ReasonThailandId.SickLeaveThId,
                                ReasonThailandId.RenewVisaLeaveThId,
                                ReasonThailandId.BereavementLeaveThId,
                                ReasonThailandId.MilitaryLeaveThId,
                                ReasonThailandId.MaternityLeaveThId,
                                ReasonThailandId.NecessaryBusinessLeaveThId
                            };
                            ReasonType annualReason = reasonTypeServiceInTask.GetById((int)ReasonThailandId.AnnualLeaveThId);
                            (bool allowed, List<(double Number, DateTime ExpirationDate, int GenerationYear)> listBuffer) = approvalRecordManagerTask.GetBufferLeave(user, annualReason, script, endDate.Date, endDate.Date);
                            thailandRemainLeave[0] = allowed ? listBuffer.Where(bf => bf.GenerationYear == DateTime.Now.Year - 1).Sum(bf => bf.Number) : 0;
                            thailandRemainLeave[1] = allowed ? listBuffer.Where(bf => bf.GenerationYear == DateTime.Now.Year).Sum(bf => bf.Number) : 0;
                            int indx = 2;
                            foreach (ReasonThailandId id in listReasonLeaveThId)
                            {
                                ReasonType reason = reasonTypeServiceInTask.GetById((int)id);
                                (bool Allowed, List<(double Number, DateTime ExpirationDate, int GenerationYear)> ListBuffer) buffer = approvalRecordManagerTask.GetBufferLeave(user, reason, script, endDate.Date, endDate.Date);
                                thailandRemainLeave[indx] = buffer.Allowed ? buffer.ListBuffer.Sum(bf => bf.Number) : 0;
                                indx++;
                            }
                        }
                        else
                        {
                            ReasonType annualReason = reasonTypeServiceInTask.GetById((int)ReasonVietnamId.AnnualLeaveViId);
                            (bool allowed, List<(double Number, DateTime ExpirationDate, int GenerationYear)> listBuffer) = approvalRecordManagerTask.GetBufferLeave(user, annualReason, script, endDate.Date, endDate.Date);
                            vietnamRemainLeave[0] = allowed ? listBuffer.Where(bf => bf.GenerationYear == DateTime.Now.Year - 1).Sum(bf => bf.Number) : 0;
                            vietnamRemainLeave[1] = allowed ? listBuffer.Where(bf => bf.GenerationYear == DateTime.Now.Year).Sum(bf => bf.Number) : 0;
                        }
                        double totalNumberOfWorkDay = 0;
                        double totalNumberOfLeaveDay = 0;
                        double totalMorningSeconds = 0;
                        double totalAffternoonSeconds = 0;
                        int totalCheckinLate = 0;
                        int totalCheckoutEarly = 0;
                        int totalShiftEat = 0;
                        List<(double TotalSeconds, string Description, int Percentage)> totalOverTime = new List<(double TotalSeconds, string Description, int Percentage)>();
                        List<(double Seconds, string WorkDate)> overTimes = new List<(double Seconds, string WorkDate)>();
                        List<(double TotalSeconds, string Description, int Percentage)> totalPendingOverTime = new List<(double TotalSeconds, string Description, int Percentage)>();
                        List<(double Seconds, string WorkDate)> pendingOverTimes = new List<(double Seconds, string WorkDate)>();
                        List<TimeKeepingResult> timeKeepingsInMonth = new List<TimeKeepingResult>();
                        List<ApprovalDateResult> approvalDates = new List<ApprovalDateResult>();
                        List<LeaveDate> leaveDates = leaveDateServiceInTask.GetLeaveDateByUserAndTime(startDate, endDate, user.Id);
                        // Lọc theo lịch sử kịch bản
                        // Lấy danh sách scriptHistory theo script
                        List<ScriptHistory> scriptHistorys = scriptManagerTask.GetScriptHistorys(user.Id, script.Id, startDate, endDate);
                        if (scriptHistorys.Count == 0)
                        {
                            continue;
                        }
                        foreach (ScriptHistory scriptHistory in scriptHistorys)
                        {
                            // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian đổi kịch bản
                            DateTime startFilterScriptDate = scriptHistory.EffectiveDate > startDate ? scriptHistory.EffectiveDate : startDate;
                            DateTime endFilterScriptDate = scriptHistory.ExpirationDate < endDate ? scriptHistory.ExpirationDate : endDate;
                            // Lọc theo lịch sử hrb
                            // Lấy danh sách hrbHistory trong thời gian làm việc
                            List<HrbHistory> hrbHistorys = hrbHistoryServiceTask.GetByUserAndTime(user.Id, startFilterScriptDate, endFilterScriptDate).ToList();
                            foreach (HrbHistory hrbHistory in hrbHistorys)
                            {
                                if (hrbHistory.HrbStatus == HrbStatus.Pending)
                                {
                                    continue;
                                }
                                // Kiểm tra ngày bắt đầu và kết thúc tháng với thời gian hrb
                                DateTime startWorkDate = hrbHistory.EffectiveDate > startFilterScriptDate ? hrbHistory.EffectiveDate : startFilterScriptDate;
                                DateTime endWorkDate = hrbHistory.ExpirationDate < endFilterScriptDate ? hrbHistory.ExpirationDate : endFilterScriptDate;
                                // Lấy định mức ngày công trong lịch sử
                                int quotasTimeKeeping = timeKeepingManagerTask.GetQuotasTimeKeepingInMonth(startWorkDate, endWorkDate);
                                (List<ApprovalDateResult> approvalDatesByHistory, double numberOfLeaveDay) = approvalRecordManagerTask.GetApprovalDatesInMonth(true, user, script, startWorkDate, endWorkDate);
                                approvalDates.AddRange(approvalDatesByHistory);
                                totalNumberOfLeaveDay += numberOfLeaveDay;
                                if (!script.NoNeedTimeKeeping)
                                {
                                    List<TimeKeepingResult> timeKeepings = timeKeepingManagerTask.GetTimeKeepingsInMonth(true, approvalDates, user, script, startWorkDate, endWorkDate, language, ref totalMorningSeconds, ref totalAffternoonSeconds, ref totalNumberOfWorkDay, ref totalCheckinLate, ref totalCheckoutEarly);
                                    timeKeepingsInMonth.AddRange(timeKeepings);
                                }
                                else
                                {
                                    totalNumberOfWorkDay += quotasTimeKeeping - numberOfLeaveDay;
                                }
                                if (script.HasOverTime)
                                {
                                    (List<(double TotalSeconds, string Description, int Percentage)> overTimeByPercentage, List<(double Seconds, string WorkDate, List<(string, string, double)>)> overTimeByWorkDates) = overtimeManagerTask.GetTotalOverTimeByStatusInMonthByStatus(script.CorporationType, StatusOverTime.Approved, user, script, startWorkDate, endWorkDate, language);
                                    totalOverTime.AddRange(overTimeByPercentage);
                                }
                                if (script.HasShiftsEat)
                                {
                                    totalShiftEat += timeKeepingManagerTask.CountShiftEatsInMonth(user, startWorkDate, endWorkDate);
                                }
                            }
                        }
                        listTimesheetResults.Add(new TimesheetResult
                        {
                            User = user,
                            TotalNumberOfWorkDay = totalNumberOfWorkDay,
                            TotalNumberOfLeaveDay = totalNumberOfLeaveDay,
                            TotalShiftEat = totalShiftEat,
                            TimeKeepingsInMonth = timeKeepingsInMonth,
                            ApprovalDates = approvalDates,
                            TotalOverTime = totalOverTime,
                            Script = script,
                            TotalWorkSeconds = totalMorningSeconds + totalAffternoonSeconds,
                            LeaveDates = leaveDates,
                            QuotasTimeKeepingInMonth = quotasTimeKeepingInMonth,
                            ThailandRemainLeave = thailandRemainLeave,
                            VietnamRemainLeave = vietnamRemainLeave
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("User error: {0}", currentId), ex);
            }

            return listTimesheetResults;
        }


        [HttpGet]
        [Route("ExportPendingRecord/{scriptId}")]
        public IActionResult ExportPendingRecord([FromQuery]string startTime, [FromQuery]string endTime, int scriptId, [FromQuery] string language = "vi")
        {
            if (isExporting)
            {
                return BadRequest(ResultCode.IS_EXPORTING);
            }

            isExporting = true;

            Script script = scriptManager.GetScriptById(scriptId);
            CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
            DateTime startDate = DateTime.ParseExact(startTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date; ;
            DateTime endDate = DateTime.ParseExact(endTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date; ;
            IEnumerable<int> listUserIds = userManager.GetUserIdsByScript(script.Id, startDate, endDate);
            List<PendingRecordResult> listPendingRecords = new List<PendingRecordResult>();

            try
            {
                int numberInTask = 10;
                int count = listUserIds.Count();
                int taskNumber = count / numberInTask;
                if (count % numberInTask != 0)
                {
                    taskNumber++;
                }
                List<PendingRecordResult>[] listResult = new List<PendingRecordResult>[taskNumber];
                Task[] process = new Task[taskNumber];
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    int i = indTask;
                    List<int> listUserIdInTask = new List<int>();
                    listUserIdInTask.AddRange(listUserIds.Skip(i * numberInTask).Take(numberInTask).ToList());
                    process[i] = Task.Run(() =>
                    {
                        using (ApplicationDbContext db = ApplicationDbContext.CreateInstance())
                        {
                            ApprovalRecordManager approvalRecordManagerTask = new ApprovalRecordManager(db);
                            TimeKeepingManager timeKeepingManagerTask = new TimeKeepingManager(db);
                            UserManager userManagerTask = new UserManager(db);
                            OvertimeManager overtimeManagerTask = new OvertimeManager(db);
                            ChangeManagerInfoManager changeManagerInfoManagerTask = new ChangeManagerInfoManager(db);

                            listResult[i] = new List<PendingRecordResult>();
                            foreach (int userId in listUserIdInTask)
                            {
                                User user = userManagerTask.GetUserById(userId);
                                User manager = userManagerTask.GetUserById(user.ManagerId);
                                PendingRecordResult recordResult = new PendingRecordResult
                                {
                                    User = user,
                                    Manager = manager,
                                    NumberPendingApproval = approvalRecordManagerTask.CountPendingApprovalRecord(user, false, false, false),
                                    NumberPendingTimeKeeping = timeKeepingManagerTask.CountPendingTimeKeeping(user, false),
                                    NumberPendingOverTime = overtimeManagerTask.CountPendingOverTime(user, false, false),
                                    NumberPendingDestroyApproval = approvalRecordManagerTask.CountPendingApprovalRecord(user, false, false, true),
                                    NumberPendingChangeManager = changeManagerInfoManagerTask.CountPendingChangeManager(user, false)
                                };
                                listResult[i].Add(recordResult);
                            }
                        }
                    });
                }
                Task.WaitAll(process);
                for (int indTask = 0; indTask < taskNumber; indTask++)
                {
                    listPendingRecords.AddRange(listResult[indTask]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            Task<MemoryStream> stream = timeKeepingManager.ExportPendingRecordAsync(listPendingRecords);
            var qguid = Guid.NewGuid().ToString();
            isExporting = false;
            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qguid + ".xlsx");
        }
    }
}