﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Claims;
using TOCO.Common;
using TOCO.Helpers;
using TOCO.Implements;
using TOCO.Logic;
using TOCO.Models;
using TOCO.Models.RequestModels;

namespace TOCO.Controllers
{

    [Authorize]
    [Produces("application/json")]
    [Route("api/OverTime")]
    public class OverTimeController : BaseController
    {
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor accessor;
        private readonly IDistributedCache cache;
        private readonly ApplicationDbContext context;
        private readonly UserManager userManager;
        private readonly OvertimeManager overtimeManager;
        private readonly TimeKeepingManager timeKeepingManager;
        private readonly ScriptManager scriptManager;
        private readonly EventManager eventManager;
        private readonly NotificationManager notificationManager;
        private readonly EmailManager emailManager;
        private readonly OvertimeQuotasService overtimeQuotasService;

        public OverTimeController(ApplicationDbContext context, IConfiguration configuration,
            IHttpContextAccessor accessor, IDistributedCache cache)
        {
            this.context = context;
            this.configuration = configuration;
            this.accessor = accessor;
            this.cache = cache;
            userManager = new UserManager(context);
            overtimeManager = new OvertimeManager(context);
            timeKeepingManager = new TimeKeepingManager(context);
            scriptManager = new ScriptManager(context);
            eventManager = new EventManager(context);
            notificationManager = new NotificationManager(context);
            emailManager = new EmailManager(context);
            overtimeQuotasService = new OvertimeQuotasService(context);
        }

        [HttpGet]
        [Route("GetOTQuotas")]
        public IActionResult GetOTQuotas([FromQuery] int employeeCode, [FromQuery] int groupEventId, [FromQuery] int month, int year)
        {
            DateTime startDate = new DateTime(year, month, 1);
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            OvertimeQuotas quotas = overtimeQuotasService.GetByUserAndGroup(employeeCode, groupEventId, startDate, endDate);
            return Ok(quotas);
        }

        // GET: api/<controller>/GetOvertimeForManager
        [HttpGet]
        [Route("GetOvertimeForManager")]
        public IActionResult GetOvertimeForManager(STRequestModel request, [FromQuery] int[] groupEventIds, [FromQuery] string language)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<OverTimeResult> overTimes = overtimeManager.GetOvertime(groupEventIds, out int numberOfPages, out int totalRecords, request, user, true, false, language);
            return OkPagination(overTimes, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/AdminGetOvertimeForManager
        [HttpGet]
        [Route("AdminGetOvertimeForManager")]
        public IActionResult AdminGetOvertimeForManager(STRequestModel request, [FromQuery] int employeeCode, [FromQuery] int[] groupEventIds, [FromQuery] string language)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserById(employeeCode);
            List<OverTimeResult> overTimes = overtimeManager.GetOvertime(groupEventIds, out int numberOfPages, out int totalRecords, request, user, true, false, language);
            return OkPagination(overTimes, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetMyOvertime
        [HttpGet]
        [Route("GetMyOvertime")]
        public IActionResult GetMyOvertime(STRequestModel request, [FromQuery] int[] groupEventIds, [FromQuery] string language)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<OverTimeResult> overTimes = overtimeManager.GetOvertime(groupEventIds, out int numberOfPages, out int totalRecords, request, user, false, false, language);
            return OkPagination(overTimes, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetOvertimeForAdmin
        [HttpGet]
        [Route("GetOvertimeForAdmin")]
        public IActionResult GetOvertimeForAdmin(STRequestModel request, [FromQuery] int[] groupEventIds, [FromQuery] string language, [FromQuery] int employeeCode)
        {
            User user = userManager.GetUserById(employeeCode);
            List<OverTimeResult> overTimes = overtimeManager.GetOvertime(groupEventIds, out int numberOfPages, out int totalRecords, request, user, false, false, language);
            return OkPagination(overTimes, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/GetMyDestroyOvertime
        [HttpGet]
        [Route("GetMyDestroyOvertime")]
        public IActionResult GetMyDestroyOvertime(STRequestModel request, [FromQuery] int[] groupEventIds, [FromQuery] string language)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            User user = userManager.GetUserByEmail(email);
            List<OverTimeResult> overTimes = overtimeManager.GetOvertime(groupEventIds, out int numberOfPages, out int totalRecords, request, user, false, true, language);
            return OkPagination(overTimes, numberOfPages, totalRecords);
        }

        // GET: api/<controller>/ApprovalOverTime
        [HttpPost]
        [Route("ApprovalOverTime")]
        public IActionResult ApprovalOverTime([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message, OverTimeDetail[] details) = overtimeManager.ConfirmOverTime(StatusOverTime.Approved, request.Ids, user, false);
                    if (bOk)
                    {
                        foreach (OverTimeDetail detail in details)
                        {
                            User employee = detail.OverTime.User;
                            Script script = scriptManager.GetScriptById(detail.OverTime.ScriptId);
                            // Xử lý ăn ca
                            timeKeepingManager.ShiftsEatProcess(detail.OverTime.WorkDate, employee, script, detail.EndTime);
                            // Bắn mail phản hồi đăng ký làm thêm giờ cho nhân sự
                            emailManager.PushEmail(
                                detail.OverTime.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC001,
                                new string[]
                                {
                                    detail.OverTime.User.FullName,
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.APPROVAL
                                });
                            // Bắn notify phản hồi đăng ký làm thêm giờ cho nhân sự
                            notificationManager.PushNotification(
                                detail.OverTime.User.Email,
                                null,
                                detail.OverTime.User.ImageUrl,
                                NotificationTemplateCode.ON001,
                                new string[]
                                {
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.APPROVAL
                                },
                                detail.OverTime.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AdminApproveOverTime
        [HttpPost]
        [Route("AdminApproveOverTime")]
        public IActionResult AdminApproveOverTime([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, OverTimeDetail[] details) = overtimeManager.ConfirmOverTime(StatusOverTime.Approved, request.Ids, isForManager ? user : manager, false);
                    if (bOk)
                    {
                        foreach (OverTimeDetail detail in details)
                        {
                            User employee = detail.OverTime.User;
                            Script script = scriptManager.GetScriptById(detail.OverTime.ScriptId);
                            // Xử lý ăn ca
                            timeKeepingManager.ShiftsEatProcess(detail.OverTime.WorkDate, employee, script, detail.EndTime);
                            //Thêm mail admin duyệt
                            detail.ApproverEmail = email;
                            overtimeManager.Update(detail);
                            // Bắn mail phản hồi đăng ký làm thêm giờ cho nhân sự
                            emailManager.PushEmail(
                                detail.OverTime.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC001,
                                new string[]
                                {
                                    detail.OverTime.User.FullName,
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.APPROVAL
                                });
                            // Bắn notify phản hồi đăng ký làm thêm giờ cho nhân sự
                            notificationManager.PushNotification(
                                detail.OverTime.User.Email,
                                null,
                                detail.OverTime.User.ImageUrl,
                                NotificationTemplateCode.ON001,
                                new string[]
                                {
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.APPROVAL
                                },
                                detail.OverTime.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/RejectOverTime
        [HttpPost]
        [Route("RejectOverTime")]
        public IActionResult RejectOverTime([FromBody]ConfirmRequestModel request, [FromQuery] string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message, OverTimeDetail[] details) = overtimeManager.ConfirmOverTime(StatusOverTime.Rejected, request.Ids, user, false, rejectReason);
                    if (bOk)
                    {
                        foreach (OverTimeDetail detail in details)
                        {
                            // Bắn mail phản hồi đăng ký làm thêm giờ cho nhân sự
                            emailManager.PushEmail(
                                detail.OverTime.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC001,
                                new string[]
                                {
                                    detail.OverTime.User.FullName,
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.REJECT
                                });
                            // Bắn notify phản hồi đăng ký làm thêm giờ cho nhân sự
                            notificationManager.PushNotification(
                                detail.OverTime.User.Email,
                                null,
                                detail.OverTime.User.ImageUrl,
                                NotificationTemplateCode.ON001,
                                new string[]
                                {
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.REJECT
                                },
                                detail.OverTime.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/RejectOverTime
        [HttpPost]
        [Route("AdminRejectOverTime")]
        public IActionResult AdminRejectOverTime([FromBody]ConfirmRequestModel request, [FromQuery] int employeeCode, [FromQuery] bool isForManager, [FromQuery] string rejectReason)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserById(employeeCode);
                    User manager = userManager.GetUserById(user.ManagerId);
                    (bool bOk, string message, OverTimeDetail[] details) = overtimeManager.ConfirmOverTime(StatusOverTime.Rejected, request.Ids, isForManager ? user : manager, false, rejectReason);
                    if (bOk)
                    {
                        foreach (OverTimeDetail detail in details)
                        {
                            //Thêm mail admin duyệt
                            detail.ApproverEmail = email;
                            overtimeManager.Update(detail);
                            // Bắn mail phản hồi đăng ký làm thêm giờ cho nhân sự
                            emailManager.PushEmail(
                                detail.OverTime.User.Email,
                                null,
                                null,
                                EmailTemplateCode.OC001,
                                new string[]
                                {
                                    detail.OverTime.User.FullName,
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.REJECT
                                });
                            // Bắn notify phản hồi đăng ký làm thêm giờ cho nhân sự
                            notificationManager.PushNotification(
                                detail.OverTime.User.Email,
                                null,
                                detail.OverTime.User.ImageUrl,
                                NotificationTemplateCode.ON001,
                                new string[]
                                {
                                    detail.OverTime.WorkDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    Constants.REJECT
                                },
                                detail.OverTime.User.Nationality);
                        }
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message));
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/DestroyOverTime
        [HttpPost]
        [Route("DestroyOverTime")]
        public IActionResult DestroyOverTime([FromBody]ConfirmRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);
                    (bool bOk, string message) = overtimeManager.DestroyOverTime(request.Ids, user);
                    if (bOk)
                    {
                        transaction.Commit();
                        return StatusCode((int)HttpStatusCode.OK, GetResultModel(bOk, message)); ;
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError(message);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/GetOverTimeValidCode
        [HttpPost]
        [Route("GetOverTimeValidCode")]
        public IActionResult GetOverTimeValidCode([FromBody]OverTimeRequestModel request)
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            DateTime workDate = STHelper.ConvertDateTimeToLocal(request.WorkDate).Value.Date;
            DateTime startTime = STHelper.ConvertDateTimeToLocal(request.StartTime).Value;
            DateTime endTime = STHelper.ConvertDateTimeToLocal(request.EndTime).Value;
            int? eventId = request.EventId;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, workDate);
            if (eventId.HasValue && !eventManager.CheckHasEventId(script.CorporationType, script.EmployeeTypeId, eventId.Value))
            {
                return LogicError(ResultCode.CAN_NOT_OVERTIME);
            }
            (bool bOk, string[] statusAllowSkips) = overtimeManager.ValidateOvertime(email, workDate, eventId, startTime, endTime, script, null, false);
            if (!bOk)
            {
                return LogicError(ResultCode.CAN_NOT_OVERTIME);
            }
            return Ok(statusAllowSkips);
        }

        // GET: api/<controller>/AdminGetOverTimeValidCode
        [HttpPost]
        [Route("AdminGetOverTimeValidCode")]
        public IActionResult AdminGetOverTimeValidCode([FromQuery]string email, [FromBody]OverTimeRequestModel request)
        {
            //string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            string clientIP = accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            DateTime workDate = STHelper.ConvertDateTimeToLocal(request.WorkDate).Value.Date;
            DateTime startTime = STHelper.ConvertDateTimeToLocal(request.StartTime).Value;
            DateTime endTime = STHelper.ConvertDateTimeToLocal(request.EndTime).Value;
            int? eventId = request.EventId;
            User user = userManager.GetUserByEmail(email);
            Script script = scriptManager.GetCurrentScriptByUser(user.Id, workDate);
            if (eventId.HasValue && !eventManager.CheckHasEventId(script.CorporationType, script.EmployeeTypeId, eventId.Value))
            {
                return LogicError(ResultCode.CAN_NOT_OVERTIME);
            }
            (bool bOk, string[] statusAllowSkips) = overtimeManager.ValidateOvertime(email, workDate, eventId, startTime, endTime, script, null, true);
            if (!bOk)
            {
                return LogicError(ResultCode.CAN_NOT_OVERTIME);
            }
            return Ok(statusAllowSkips);
        }

        // GET: api/<controller>/ManualOverTime
        [HttpPost]
        [Route("ManualOverTime")]
        public IActionResult ManualOverTime([FromBody]OverTimeRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    DateTime workDate = STHelper.ConvertDateTimeToLocal(request.WorkDate).Value.Date;
                    DateTime startTime = STHelper.ConvertDateTimeToLocal(request.StartTime).Value;
                    DateTime endTime = STHelper.ConvertDateTimeToLocal(request.EndTime).Value;
                    int? eventId = request.EventId;
                    string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                    User user = userManager.GetUserByEmail(email);

                    Script script = scriptManager.GetCurrentScriptByUser(user.Id, workDate);
                    if (eventId.HasValue && !eventManager.CheckHasEventId(script.CorporationType, script.EmployeeTypeId, eventId.Value))
                    {
                        return LogicError(ResultCode.CAN_NOT_OVERTIME);
                    }
                    (bool bOk, OverTimeDetail overTimeDetail) = overtimeManager.ManualOverTime(user, script, request.Note, workDate, startTime, endTime, eventId, false);
                    if (bOk)
                    {
                        User manager = userManager.GetUserById(user.ManagerId);
                        Event ev = eventManager.GetEventById(overTimeDetail.EventId);
                        string eventName = string.Empty;
                        if (ev.ShowCode && !string.IsNullOrEmpty(ev.Description))
                        {
                            eventName = string.Format("{0} ( {1} )", ev.EventCode, MessageManager.GetMessageManager(manager.Nationality).GetValue(ev.Description));
                        }
                        else if (ev.ShowCode)
                        {
                            eventName = ev.EventCode;
                        }
                        else if (!string.IsNullOrEmpty(ev.Description))
                        {
                            eventName = MessageManager.GetMessageManager(manager.Nationality).GetValue(ev.Description);
                        }
                        // Bắn mail đăng ký làm thêm giờ cho quản lý
                        emailManager.PushEmail(
                                manager.Email,
                                null,
                                null,
                                EmailTemplateCode.OC002,
                                new string[]
                                {
                                    manager.FullName,
                                    user.FullName,
                                    string.Format(MessageManager.GetMessageManager(manager.Nationality).GetValue(Constants.SCRIPT), script.Name, MessageManager.GetMessageManager(manager.Nationality).GetValue(script.Description)),
                                    eventName,
                                    workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    overTimeDetail.StartTime.ToString(Constants.FORMAT_TIME_HHMM) + " - " + overTimeDetail.EndTime.ToString(Constants.FORMAT_TIME_HHMM),
                                    overTimeDetail.Note,
                                    configuration["MailConfig:UrlFrontEnd"] + "/#/attendance/overtime"
                                });
                        // Bắn notify đăng ký làm thêm giờ cho quản lý
                        notificationManager.PushNotification(
                               manager.Email,
                               null,
                               user.ImageUrl,
                               NotificationTemplateCode.ON002,
                               new string[]
                               {
                                    workDate.ToString(Constants.FORMAT_DATE_DDMMYYYY),
                                    user.FullName
                               },
                               manager.Nationality);
                        transaction.Commit();
                        return Ok();
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError(ResultCode.CAN_NOT_OVERTIME);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        // GET: api/<controller>/AdminCreateManualOverTime
        [HttpPost]
        [Route("AdminCreateManualOverTime")]
        public IActionResult AdminCreateManualOverTime([FromQuery]string email, [FromBody]OverTimeRequestModel request)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    DateTime workDate = STHelper.ConvertDateTimeToLocal(request.WorkDate).Value.Date;
                    DateTime startTime = STHelper.ConvertDateTimeToLocal(request.StartTime).Value;
                    DateTime endTime = STHelper.ConvertDateTimeToLocal(request.EndTime).Value;
                    int? eventId = request.EventId;
                    User user = userManager.GetUserByEmail(email);

                    Script script = scriptManager.GetCurrentScriptByUser(user.Id, workDate);
                    if (eventId.HasValue && !eventManager.CheckHasEventId(script.CorporationType, script.EmployeeTypeId, eventId.Value))
                    {
                        return LogicError(ResultCode.CAN_NOT_OVERTIME);
                    }
                    (bool bOk, OverTimeDetail overTimeDetail) = overtimeManager.ManualOverTime(user, script, request.Note, workDate, startTime, endTime, eventId, true);
                    if (bOk)
                    {
                        transaction.Commit();
                        return Ok();
                    }
                    else
                    {
                        transaction.Rollback();
                        return LogicError(ResultCode.CAN_NOT_OVERTIME);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        [HttpGet]
        [Route("ExportOverTime/{month}/{year}")]
        public IActionResult ExportOverTime(int month, int year)
        {
            User admin = userManager.GetUserByEmail(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value);

            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;

            var stream = overtimeManager.ExportOvertimeDetail(admin.Nationality, startDate, endDate);

            string name = string.Format("OT_{0}{1}_{2}{3}_{4}{5}{6}{7}{8}{9}",
                             startDate.Day, startDate.Month, endDate.Day, endDate.Month,
                             DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name + ".xlsx");
        }

        [HttpGet]
        [Route("ExportOverTime/{month}/{year}/{scriptId}")]
        public IActionResult ExportOverTime(int month, int year, int scriptId)
        {
            if (isExporting)
            {
                return BadRequest(ResultCode.IS_EXPORTING);
            }

            isExporting = true;

            User admin = userManager.GetUserByEmail(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value);
            if (month > 12 || month < 1 || year < 1)
            {
                return BadRequest();
            }
            DateTime startDate = new DateTime(year, month, 1).Date;
            DateTime endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).Date;
            Script script = scriptManager.GetScriptById(scriptId);
            var stream = overtimeManager.ExportOvertimeDetail(script, startDate, endDate);

            string name = string.Format("OT_{0}{1}_{2}{3}_{4}{5}{6}{7}{8}{9}",
                             startDate.Day, startDate.Month, endDate.Day, endDate.Month,
                             DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            isExporting = false;

            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name + ".xlsx");
        }

        [HttpGet]
        [Route("ExportOverTime/{scriptId}")]
        public IActionResult ExportOverTime([FromQuery]string startTime, [FromQuery]string endTime, int scriptId, [FromQuery] string language = "vi")
        {
            if (isExporting)
            {
                return BadRequest(ResultCode.IS_EXPORTING);
            }

            isExporting = true;
            CultureInfo customCulture = new CultureInfo(AppSetting.Configuration.GetValue("Culture"), true);
            DateTime startDate = DateTime.ParseExact(startTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date;
            DateTime endDate = DateTime.ParseExact(endTime, Constants.FORMAT_DATE_YYYYMMDD, customCulture).Date;
            Script script = scriptManager.GetScriptById(scriptId);
            var stream = overtimeManager.ExportOvertimeDetail(script, startDate, endDate);

            string name = string.Format("OT_{0}{1}_{2}{3}_{4}{5}{6}{7}{8}{9}",
                             startDate.Day, startDate.Month, endDate.Day, endDate.Month,
                             DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            isExporting = false;
            return File(stream.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name + ".xlsx");
        }
    }
}