﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Common
{
    public enum EmailEnum
    {
        NhanSu_ChamCongBatThuong = 1,
        NhanSu_DonXinNghiPhep1Ngay = 2,
        NhanSu_DonXinNghiPhepDaiNgay = 3,
        NhanSu_LamThemGio = 4,
        NhanSu_ThongBaoBangChamCong = 5,
        NhanSu_ThongBaoHeThongLoi = 6,

        QuanLy_ChamCongBatThuong = 7,
        QuanLy_DonXinNghiPhep1Ngay = 8,
        QuanLy_DonXinNghiPhepDaiNgay = 9,
        QuanLy_LamThemGio = 10,
        QuanLy_ThongBaoBangChamCong = 11,
        QuanLy_ThongBaoThayDoiQuanLyChamCong = 12
    }
}
