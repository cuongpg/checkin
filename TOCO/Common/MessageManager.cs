﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;

namespace TOCO.Common
{
    public class MessageManager
    {
        private ResourceManager rm;

        public static MessageManager GetMessageManager(string languge)
        {
            return new MessageManager(languge);
        }

        private MessageManager(string language)
        {
            switch (language)
            {
                case "vi-VN":
                case "vi":
                case "VN":
                case "Việt Nam":
                    rm = new ResourceManager("TOCO.ResourceVN",
                Assembly.GetExecutingAssembly());
                    break;
                case "th-TL":
                case "th":
                case "TL":
                case "Thái Lan":
                    rm = new ResourceManager("TOCO.ResourceTL",
               Assembly.GetExecutingAssembly());
                    break;
                case "en-EN":
                case "en":
                case "EN":
                case "English":
                default:
                    rm = new ResourceManager("TOCO.ResourceEN",
                Assembly.GetExecutingAssembly());
                    break;
            }
        }

        public string GetValue(string key)
        {
            string value = string.Empty;
            if (!string.IsNullOrEmpty(key))
            {
                value = rm.GetString(key);
            }
            return string.IsNullOrEmpty(value) ? key : value;
        }

        public static CultureInfo GetCultureInfo(string language)
        {
            CultureInfo culture;
            switch (language)
            {
                case "th-TL":
                case "th":
                case "TL":
                case "Thái Lan":
                    culture = CultureInfo.GetCultureInfo("th-TH");
                    break;
                case "vi-VN":
                case "vi":
                case "VN":
                case "Việt Nam":
                    culture = CultureInfo.GetCultureInfo("vi-VN");
                    break;
                case "en-EN":
                case "en":
                case "EN":
                case "English":
                default:
                    culture = CultureInfo.GetCultureInfo("en-US");
                    break;
            }
            return culture;
        }

        public static string GetCurrencyInfo(string language)
        {
            string currency;
            switch (language)
            {
                case "th-TL":
                case "th":
                case "TL":
                case "Thái Lan":
                    currency = "THB";
                    break;
                case "vi-VN":
                case "vi":
                case "VN":
                case "Việt Nam":
                    currency = "VND";
                    break;
                case "en-EN":
                case "en":
                case "EN":
                case "English":
                default:
                    currency = "USD";
                    break;
            }
            return currency;
        }
    }
}
