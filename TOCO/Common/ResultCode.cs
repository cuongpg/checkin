﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Common
{
    public class ResultCode
    {
        public const string OK = "Ok";
        public const string BAD_REQUEST = "Bad Request";
        public const string UNAUTHORIZED = "Unauthorized";
        public const string FORBIDDEN = "Forbidden";
        public const string NOT_FOUND = "Not Found";
        public const string INTERNAL_SERVER_ERROR = "Internal Server Error";
        public const string NOT_IMPLEMENTED = "Not Implemented";

        // Result code
        public const string YOU_DONT_HAVE_PERMISSION = "YOU_DONT_HAVE_PERMISSION";
        public const string NOT_EXISTED_USER = "NOT_EXISTED_USER";
        public const string PENDING_USER_MESSAGE = "PENDING_USER_MESSAGE";
        public const string DEACTIVE_USER_MESSAGE = "DEACTIVE_USER_MESSAGE";
        public const string APPROVAL_NOT_FOUND = "APPROVAL_NOT_FOUND";
        public const string DB_EXCEPTION = "DB_EXCEPTION";
        public const string OTHER_EXCEPTION = "OTHER_EXCEPTION";
        public const string TIME_KEEPING_NOT_FOUND = "TIME_KEEPING_NOT_FOUND";
        public const string OVERTIME_NOT_FOUND = "OVERTIME_NOT_FOUND";
        public const string ERR_UPDATE_TOKEN_HRDATA = "ERR_UPDATE_TOKEN_HRDATA";
        public const string ERR_GET_INFO = "ERR_GET_INFO {0}";
        public const string ERR_TOPICA_EMAIL_WRONG = "ERR_TOPICA_EMAIL_WRONG";
        public const string ERR_FORBIDDEN_CONTRACT = "ERR_FORBIDDEN_CONTRACT";
        public const string IS_EXPORTING = "IS_EXPORTING";
        public const string CAN_NOT_DESTROY = "CAN_NOT_DESTROY";
        public const string NOT_EXISTED_ALLOCATION_REQUEST = "NOT_EXISTED_ALLOCATION_REQUEST";

        // Result code validate check in, check out
        public const string CAN_NOT_TIMEKEEPING = "CAN_NOT_TIMEKEEPING";
        public const string INVALID_CLIENT = "INVALID_CLIENT";
        public const string CHECKIN_INVALID_CLIENT = "CHECKIN_INVALID_CLIENT";
        public const string CHECKOUT_INVALID_CLIENT = "CHECKOUT_INVALID_CLIENT";
        public const string INVALID_CHECKIN_ONTIME = "INVALID_CHECKIN_ONTIME";
        public const string INVALID_CHECKOUT_ONTIME = "INVALID_CHECKOUT_ONTIME";
        public const string TIMEKEEPING_CONFLICT = "TIMEKEEPING_CONFLICT";
        public const string SESSION_CHECKIN_NOT_EXIST = "SESSION_CHECKIN_NOT_EXIST";
        public const string INVALID_ENOUGH_HOUR = "INVALID_ENOUGH_HOUR";
        public const string EXCEEDED_8_HOURS = "EXCEEDED_8_HOURS";

        public const string TIMEKEEPING_HAS_OVER_TIME = "TIMEKEEPING_HAS_OVER_TIME";
        public const string TIMEKEEPING_HAS_OVER_TIME_CONFLICT = "TIMEKEEPING_HAS_OVER_TIME_CONFLICT";
        public const string CHECKIN_NOT_YET = "CHECKIN_NOT_YET";
        public const string ADDNOTE_SUCCESS = "ADDNOTE_SUCCESS";
        public const string INVALID_ESTIMATEOWNER = "INVALID_ESTIMATEOWNER";

        public const string NOT_FOUND_SCRIPT = "NOT_FOUND_SCRIPT";
        // Result code validate overtime
        public const string OVERTIME_CONFLICT = "OVERTIME_CONFLICT";

        // Result code validate approval record
        public const string APPROVAL_CONFLICT = "APPROVAL_CONFLICT";
        public const string APPROVAL_OVER_BUFFER = "APPROVAL_OVER_BUFFER";
        public const string LEAVE_DATE_INVALID = "LEAVE_DATE_INVALID";

        public const string FORGET_CHECKOUT = "FORGET_CHECKOUT";
        public const string BUFFERDATE_INVALID = "BUFFERDATE_INVALID";

        public const string CAN_NOT_MANUAL_TIMEKEEPING = "CAN_NOT_MANUAL_TIMEKEEPING";
        public const string CAN_NOT_OVERTIME = "CAN_NOT_OVERTIME";
        public const string CAN_NOT_APPROVAL_RECORD = "CAN_NOT_APPROVAL_RECORD";

        public const string REASON_CAN_NOT_NULL = "REASON_CAN_NOT_NULL";
        public const string REASON_TOO_LONG = "REASON_TOO_LONG";
        public const string LEAVE_REQUEST_OVER_3_DAYS = "LEAVE_REQUEST_OVER_3_DAYS";
        public const string DONT_HAVE_REASON_TYPE = "DONT_HAVE_REASON_TYPE";
        public const string END_OF_LEAVE_BUFFER = "END_OF_LEAVE_BUFFER";

        // Result code for change manager
        public const string CAN_NOT_CHANGE_MANAGER = "CAN_NOT_CHANGE_MANAGER";
        public const string CHANGE_MANAGER_NOT_FOUND = "CHANGE_MANAGER_NOT_FOUND";
        public const string MANAGER_NOT_FOUND = "MANAGER_NOT_FOUND";
        public const string CHANGE_MANAGER_INFOMATION_NOT_MATCH = "CHANGE_MANAGER_INFOMATION_NOT_MATCH";

        // Result code for Update script
        public const string CAN_NOT_UPDATE_SCRIPT = "CAN_NOT_UPDATE_SCRIPT";

        //Result code for CRUD user
        public const string CAN_NOT_CREATE_USER = "CAN_NOT_CREATE_USER";
        public const string CAN_NOT_UPDATE_USER = "CAN_NOT_UPDATE_USER";

        // Connect error
        public const string FORMAT_DISCONNECT = "FORMAT_DISCONNECT";

        //
        public const string DISABLE_CHECKIN_MOBILE_MESSAGE = "DISABLE_CHECKIN_MOBILE_MESSAGE";
    }
}
