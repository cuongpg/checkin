﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using TOCO.Models;
using TOCO.Models.APIModels;
using TOCO.Common;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using TOCO.Helpers;

namespace TOCO.Common
{

    public class GlobalVariable
    {
        public static GlobalVariable Data = new GlobalVariable();

        public List<Unit> OrgUnits = new List<Unit>();
        public List<Unit> Projects = new List<Unit>();
        public ISet<int> ListCheckin = new HashSet<int>();

        private GlobalVariable()
        {

        }
    }
}
