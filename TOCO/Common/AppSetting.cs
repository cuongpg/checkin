﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TOCO.Common
{
    public class AppSetting
    {
        private readonly IConfigurationRoot configuration;

        public static AppSetting Configuration = new AppSetting();

        private AppSetting()
        {
            configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        }

        public string GetValue(string key)
        {
            return configuration[key];
        }

        public T GetValue<T>(string key)
        {
            return configuration.GetSection(key).Get<T>();
        }

        public T[] GetArrayValue<T>(string key)
        {
            T[] value = configuration.GetSection(key).Get<T[]>();
            return value??new T[] { };
        }

        public Dictionary<string, object> GetDictionaryValue(string key)
        {
            Dictionary<string, object> value = configuration.GetSection(key).Get<Dictionary<string, object>>();
            return value;
        }
    }
}
